/*LINEA BASE*/
delete from roles_personas where id_rol in (4, 6, 7, 8, 9, 15, 18, 19, 101, 103, 105, 301, 302);
delete from roles where id in (4, 6, 7, 8, 9, 15, 18, 19, 101, 103, 105, 301, 302);

/*SCREI*/
--delete from roles_personas where id_rol in (4, 6, 7, 8, 9, 15, 18, 19, 101, 103, 105, 302);
--delete from roles where id in (4, 6, 7, 8, 9, 15, 18, 19, 101, 103, 105, 302);

update observaciones set id_tipo_observacion = 1 where id_tipo_observacion is null;

update tipo_observacion set descripcion = 'OBSERVACION NORMAL' where id = 1;
update tipo_observacion set descripcion = 'OBSERVACION SISTEMA' where id = 2;
update tipo_observacion set descripcion = 'OBSERVACION DEVOLUCION' where id = 3;
update tipo_observacion set descripcion = 'OBSERVACION RESOLUCION' where id = 4;

alter table documentos rename column ACEPTADO_DIA_ADMINISTRATIVO to ACEPTADO_DIA_ADM;
alter table documentos rename column ACEPTADO_DIA_ADMINISTRATIVO_RRHH to ACEPTADO_DIA_ADM_RRHH;
alter table documentos rename column RECHAZADO_DIA_ADMINISTRATIVO to RECHAZADO_DIA_ADM;
alter table documentos rename column DIA_ADMINISTRATIVO to DIA_ADM;
alter table documentos rename column CANT_MEDIOS_DIAS_SOLICITADOS to CANT_MEDIOS_DIAS_SOLIC;

alter table expedientes drop column acuse_recibo;
alter table expedientes drop column eliminado;
alter table expedientes drop column desierto;
alter table expedientes drop column destinatario_con_recepcion;

alter table HISTORIAL_ACTUALIZACION_TRANSPARENCIA rename column ID_ULTIMA_SOLICITUD_OBTENIDA to ID_ULTIMA_SOLIC_OBTENIDA;
alter table name HISTORIAL_ACTUALIZACION_TRANSPARENCIA to HIST_ACT_TRANSPARENCIA
alter sequence SEQ_HISTORIAL_ACTUALIZACION_TRANSPARENCIA rename to SEQ_HIST_ACT_TRANSPARENCIA;

alter table SOLICITUD_DIA_ADMINISTRATIVO rename column CANT_DIAS_ADM_SOLICITADOS to CANT_DIAS_ADM_SOLIC;
alter table SOLICITUD_DIA_ADMINISTRATIVO rename column CANT_FERIADOS_LEGALES_SOLICITADOS to CANT_FER_LEG_SOLIC;
alter table SOLICITUD_DIA_ADMINISTRATIVO rename column CANT_MEDIOS_DIAS_ADM_SOLICITADOS to CANT_MEDIOS_DIAS_ADM_SOLIC;
alter table SOLICITUD_DIA_ADMINISTRATIVO rename column CANT_MEDIOS_FERIADOS_LEGALES_SOLICITADOS to CANT_MEDIOS_FER_LEG_SOLIC;
alter table SOLICITUD_DIA_ADMINISTRATIVO rename column DIA_ADMINISTRATIVO_SOLICITUD to DIA_ADMIN_SOLIC;
alter table SOLICITUD_DIA_ADMINISTRATIVO rename to SOLICITUD_DIA_ADM;
alter sequence SEQ_SOLICITUD_DIA_ADMINISTRATIVO rename to SEQ_SOLICITUD_DIA_ADM;

--en desuso

drop table revisardocumentoshistorial;
drop table revisardocumentos;
drop table instrucciones_expedientes;
drop table instrucciones;
drop table solicitudes respuesta_exp;
drop table solicitudes_respuesta;
drop table estados_solicitud_de_respuesta;
drop table info_documento;
