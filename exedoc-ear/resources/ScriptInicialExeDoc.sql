/*Organizacion Inicial*/
insert into ORGANIZACIONES (ID,DESCRIPCION) values (0,'Organizacion Inicial');

/*Division Inicial*/
insert into DIVISIONES (ID,DESCRIPCION,CON_CARGO,ID_ORGANIZACION) values (0,'Division Inicial',false,0);

/*Departamento Inicial*/
insert into DEPARTAMENTOS (ID,DESCRIPCION,CON_CARGO,VIRTUAL,ID_DIVISION) values (0,'Departamento Inicial',false,false,0);

/*Unidad Inicial*/
insert into UNIDADES (ID,DESCRIPCION,VIRTUAL,ID_DEPARTAMENTO) values (0,'Unidad Inicial',false,0);

/*Cargo Inicial*/
insert into CARGOS (ID,DESCRIPCION,CARGO_DEFAULT,ID_UNIDAD_ORGANIZACIONAL) values (0,'Cargo Inicial',false,0);

/*Rol Inicial*/
insert into ROLES (ID,DESCRIPCION) values (1, 'INGRESO_DOCUMENTOS');
insert into ROLES (ID,DESCRIPCION) values (2, 'BANDEJA_ENTRADA_DOCUMENTOS');
insert into ROLES (ID,DESCRIPCION) values (3, 'BANDEJA_SALIDA_DOCUMENTOS');
insert into ROLES (ID,DESCRIPCION) values (5, 'ADMINISTRADOR');
insert into ROLES (ID,DESCRIPCION) values (102, 'REPORTES');
insert into ROLES (ID,DESCRIPCION) values (104, 'GESTION');
insert into ROLES (ID,DESCRIPCION) values (300, 'VISADOR');
insert into ROLES (ID,DESCRIPCION) values (303, 'TRANSFORMA_PAPEL_A_DIGITAL');

/*eliminados hasta nuevo aviso*/
--insert into ROLES (ID,DESCRIPCION) values (4, 'FIRMA_DOCUMENTOS');
--insert into ROLES (ID,DESCRIPCION) values (6, 'INGRESAR_VISA_FIRMA');
--insert into ROLES (ID,DESCRIPCION) values (7, 'ROL_FOLIADOR');
--insert into ROLES (ID,DESCRIPCION) values (8, 'ROL_ANULADOR');
--insert into ROLES (ID,DESCRIPCION) values (9, 'JEFE_RRHH');
--insert into ROLES (ID,DESCRIPCION) values (15, 'JEFE');
--insert into ROLES (ID,DESCRIPCION) values (18, 'FISCAL');
--insert into ROLES (ID,DESCRIPCION) values (19, 'ABASTECIMIENTO');
--insert into ROLES (ID,DESCRIPCION) values (101, 'MENU_PRINCIPAL');
--insert into ROLES (ID,DESCRIPCION) values (103, 'MENU_CIERRE_DOCUMENTOS');
--insert into ROLES (ID,DESCRIPCION) values (105, 'SOLICITANTE_DE_RESPUESTA');
--insert into ROLES (ID,DESCRIPCION) values (301, 'REVISOR');
--insert into ROLES (ID,DESCRIPCION) values (302, 'REPORTE_REVISION');

/*Usuario inicial*/
insert into PERSONAS (ID,ID_PERSONA,NOMBRES,APELLIDO_PATERNO,INICIALES,EMAIL,RUT,VIGENTE,USUARIO,APELLIDO_MATERNO,PERSONA_DEFAULT,KEY_STORE_ALIAS,KEY_STORE_PASSWORD,FECHA_NO_VIGENTE,EXTERNO,ID_TIPO_FIRMA,ID_CARGO) 
values (1,0,'TEST','TEST','TEST','test@exedoc.cl','1-9',true,'test','TEST',false,'123','123',null,false,null,0);
select pg_catalog.setval('seq_personas', 2, true);

/*Asociar Usuario Inicial*/

insert into ROLES_PERSONAS(ID_PERSONA, ID_ROL) values (1, 5);

/*Nivel de Urgencia*/
insert into ALERTAS (ID,NOMBRE,PLAZO) values (1,'Normal',2);
insert into ALERTAS (ID,NOMBRE,PLAZO) values (2,'Urgente',4);
insert into ALERTAS (ID,NOMBRE,PLAZO) values (3,'Critica',8);
select pg_catalog.setval('seq_alertas', 4, true);

/*Tipo Documentos*/
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (1, 'Oficio Ordinario', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (2, 'Oficio Circular', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (3, 'Oficio Reservado', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (6, 'Resolución', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (7, 'Memorándum', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (8, 'Providencia', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (9, 'Decreto', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (10, 'Carta', true, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (11, 'Factura', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (15, 'Solicitud Día Administrativo', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (16, 'Solicitud Vacaciones', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (22, 'Convenio', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (23, 'Contrato', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (98, 'Anulación Resolución', false, true, true);
insert into TIPOS_DOCUMENTOS(id, descripcion, visible, creacion, electronico) values (99, 'Solicitud Folio', false, true, true);
select pg_catalog.setval('seq_tipos_documentos', 100, true);

/*Numero de Folio Expediente*/
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (1, 0, 0, null);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (2, 0, 0, 1);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (3, 0, 0, 2);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (4, 0, 0, 3);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (5, 0, 0, 6);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (6, 0, 0, 7);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (7, 0, 0, 8);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (8, 0, 0, 9);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (9, 0, 0, 10);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (10, 0, 0, 11);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (11, 0, 0, 15);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (12, 0, 0, 16);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (13, 0, 0, 22);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (14, 0, 0, 23);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (15, 0, 0, 98);
insert into FOLIOS(id, agno_actual, numero_actual, id_tipo_documento) values (16, 0, 0, 99);

/*Estados de Documento*/
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (1,'GUARDADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (2,'VISADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (3,'FIRMADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (4,'DESPACHADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (5,'PLAZO CERRADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (6,'CREADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (7,'AUTORIZADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (8,'RECHAZADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (9,'ENVIADO A CONTRALORIA');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (10,'APROBADO EN CONTRALORIA');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (11,'RECHAZADO EN CONTRALORIA');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (12,'BORRADOR APROBADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (13,'BORRADOR');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (14,'BORRADOR RECHAZADO');
insert into ESTADOS_DOCUMENTOS (ID,DESCRIPCION) values (15,'ANULADO');

/*Formato Documentos*/
insert into FORMATOS_DOCUMENTOS (ID,DESCRIPCION) values (1,'PAPEL');
insert into FORMATOS_DOCUMENTOS (ID,DESCRIPCION) values (2,'DIGITAL');
insert into FORMATOS_DOCUMENTOS (ID,DESCRIPCION) values (3,'ELECTRONICO');

/*Tipo Documentos Expedientes*/
insert into TIPOS_DOCUMENTOS_EXPEDIENTES (ID,DESCRIPCION) values (1,'ORIGINAL');
insert into TIPOS_DOCUMENTOS_EXPEDIENTES (ID,DESCRIPCION) values (2,'RESPUESTA');
insert into TIPOS_DOCUMENTOS_EXPEDIENTES (ID,DESCRIPCION) values (3,'ANEXO');

/*Tipo Firmas*/
insert into TIPOS_FIRMA (ID,DESCRIPCION) values (1,'SIMPLE');
insert into TIPOS_FIRMA (ID,DESCRIPCION) values (2,'AVANZADA');
insert into TIPOS_FIRMA (ID,DESCRIPCION) values (3,'AMBAS');

/*Tipos de Razon*/
insert into TIPOS_RAZON (ID, DESCRIPCION) values (1,'EXENTO');
insert into TIPOS_RAZON (ID, DESCRIPCION) values (2,'TOMA DE RAZON');
insert into TIPOS_RAZON (ID, DESCRIPCION) values (3,'EXENTO CON REGISTRO');

insert into FERIADOS (ID, DIA, MES, AGNO, DESCRIPCION) values (1, 1, 1, 0, 'AÑO NUEVO');
insert into FERIADOS (ID, DIA, MES, AGNO, DESCRIPCION) values (2, 15, 8, 0, 'ASUNCION DE LA VIRGEN');
insert into FERIADOS (ID, DIA, MES, AGNO, DESCRIPCION) values (3, 18, 9, 0, 'FIESTAS PATRIAS');
insert into FERIADOS (ID, DIA, MES, AGNO, DESCRIPCION) values (4, 19, 9, 0, 'DIA DEL EJERCITO');
insert into FERIADOS (ID, DIA, MES, AGNO, DESCRIPCION) values (5, 25, 12, 0, 'NAVIDAD');
select pg_catalog.setval('seq_feriados', 6, true);

/*Tipos de Observacion*/
insert into TIPO_OBSERVACION (ID, DESCRIPCION) values (1,'OBSERVACION NORMAL');
insert into TIPO_OBSERVACION (ID, DESCRIPCION) values (2,'OBSERVACION SISTEMA');
insert into TIPO_OBSERVACION (ID, DESCRIPCION) values (3,'OBSERVACION DEVOLUCION');
insert into TIPO_OBSERVACION (ID, DESCRIPCION) values (4,'OBSERVACION RESOLUCION');

insert into tipos_plantillas (id, descripcion) values (1,'RESOLUCION/DECRETO');

insert into tipo_contenido (id, contenido) values (1, 'TIPO');
insert into tipo_contenido (id, contenido) values (2,'MATERIA');
insert into tipo_contenido (id, contenido) values (3,'VISTOS');
insert into tipo_contenido (id, contenido) values (4,'CONSIDERANDO');
insert into tipo_contenido (id, contenido) values (5,'RESUELVO');
insert into tipo_contenido (id, contenido) values (6,'INDICACION');
