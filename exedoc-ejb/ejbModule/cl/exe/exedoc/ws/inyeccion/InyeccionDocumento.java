package cl.exe.exedoc.ws.inyeccion;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jboss.seam.contexts.Lifecycle;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.dto.Carta;
import cl.exe.exedoc.ws.inyeccion.dto.Decreto;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.Digital;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Memorandum;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Oficio;
import cl.exe.exedoc.ws.inyeccion.dto.Papel;
import cl.exe.exedoc.ws.inyeccion.dto.Resolucion;

@Stateless
@WebService
public class InyeccionDocumento implements InyeccionDocumentoInterface {
	
	@EJB
	private InyeccionService service;
    
	@Override
	@WebMethod
	public InyeccionResponse InyectarCarta(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="carta") Carta carta) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarCarta(emisor, destinatario, observacion, carta);
		Lifecycle.endCall();
		return response;
	}
	
	@Override
	@WebMethod
	public InyeccionResponse InyectarMemorandum(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="memorandum") Memorandum memorandum) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarMemorandum(emisor, destinatario, observacion, memorandum);
		Lifecycle.endCall();
		return response;
	}
	
	@Override
	@WebMethod
	public InyeccionResponse InyectarOficio(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="oficio") Oficio oficio) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarOficio(emisor, destinatario, observacion, oficio);
		Lifecycle.endCall();
		return response;
	}
	
	@Override
	@WebMethod
	public InyeccionResponse InyectarResolucion(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="resolucion") Resolucion resolucion) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarResolucion(emisor, destinatario, observacion, resolucion);
		Lifecycle.endCall();
		return response;
	}
	
	@Override
	@WebMethod
	public InyeccionResponse InyectarDecreto(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="decreto") Decreto decreto) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarDecreto(emisor, destinatario, observacion, decreto);
		Lifecycle.endCall();
		return response;
	}

	@Override
	@WebMethod
	public InyeccionResponse InyectarDigital(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="digital") Digital digital) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarDigital(emisor, destinatario, observacion, digital);
		Lifecycle.endCall();
		return response;
	}

	@Override
	@WebMethod
	public InyeccionResponse InyectarPapel(
			@WebParam(name="emisor") String emisor,
			@WebParam(name="destinatario") List<Destinatario> destinatario,
			@WebParam(name="observacion") List<Observacion> observacion,
			@WebParam(name="papel") Papel papel) throws DocumentNotUploadedException, InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		Lifecycle.beginCall();
		response = service.inyectarPapel(emisor, destinatario, observacion, papel);
		Lifecycle.endCall();
		return response;
	}
}
