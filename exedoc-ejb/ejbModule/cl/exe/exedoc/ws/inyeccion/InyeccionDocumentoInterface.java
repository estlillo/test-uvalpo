package cl.exe.exedoc.ws.inyeccion;

import java.util.List;

import javax.ejb.Local;
import javax.jws.WebMethod;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.dto.Carta;
import cl.exe.exedoc.ws.inyeccion.dto.Decreto;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.Digital;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Memorandum;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Oficio;
import cl.exe.exedoc.ws.inyeccion.dto.Papel;
import cl.exe.exedoc.ws.inyeccion.dto.Resolucion;

@Local
public interface InyeccionDocumentoInterface {

	@WebMethod
	InyeccionResponse InyectarCarta(String emisor,
			List<Destinatario> destinatario, 
			List<Observacion> observacion,
			Carta carta) throws DocumentNotUploadedException, InyeccionError;

	@WebMethod
	InyeccionResponse InyectarMemorandum(String emisor,
			List<Destinatario> destinatario, 
			List<Observacion> observacion,
			Memorandum memorandum) throws DocumentNotUploadedException, InyeccionError;

	@WebMethod
	InyeccionResponse InyectarOficio(String emisor,
			List<Destinatario> destinatario, 
			List<Observacion> observacion,
			Oficio oficio) throws DocumentNotUploadedException, InyeccionError;
	
	@WebMethod
	InyeccionResponse InyectarResolucion(String emisor,
			List<Destinatario> destinatario, 
			List<Observacion> observacion,
			Resolucion resolucion) throws DocumentNotUploadedException, InyeccionError;

	@WebMethod
	InyeccionResponse InyectarDecreto(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Decreto decreto) throws DocumentNotUploadedException, InyeccionError;
	
	@WebMethod
	InyeccionResponse InyectarDigital(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Digital digital) throws DocumentNotUploadedException, InyeccionError;
	
	@WebMethod
	InyeccionResponse InyectarPapel(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Papel papel) throws DocumentNotUploadedException, InyeccionError;

}
