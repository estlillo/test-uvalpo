package cl.exe.exedoc.ws.inyeccion;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.dto.Carta;
import cl.exe.exedoc.ws.inyeccion.dto.Decreto;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.Digital;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Memorandum;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Oficio;
import cl.exe.exedoc.ws.inyeccion.dto.Papel;
import cl.exe.exedoc.ws.inyeccion.dto.Resolucion;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionCarta;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionDecreto;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionDigital;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionMemorandum;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionOficio;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionPapel;
import cl.exe.exedoc.ws.inyeccion.util.InyeccionResolucion;

@Stateless
@Name(InyeccionServiceBean.NAME)
public class InyeccionServiceBean implements InyeccionService {
	
	public final static String NAME = "inyeccionService";
	
	@EJB
	private InyeccionCarta cartaService;
	@EJB
	private InyeccionDecreto decretoService;
	@EJB
	private InyeccionDigital digitalService;
	@EJB
	private InyeccionMemorandum memorandumService;
	@EJB
	private InyeccionOficio oficioService;
	@EJB
	private InyeccionPapel papelService;
	@EJB
	private InyeccionResolucion resolucionService;
	
	@Override
	public InyeccionResponse inyectarCarta(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Carta carta) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = cartaService.inyectarCarta(emisor, destinatario, observacion, carta);
		return response;
	}
	
	@Override
	public InyeccionResponse inyectarDecreto(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Decreto decreto) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = decretoService.inyectarDecreto(emisor, destinatario, observacion, decreto);
		return response;
	}

	@Override
	public InyeccionResponse inyectarDigital(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Digital digital) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = digitalService.inyectarDigital(emisor, destinatario, observacion, digital);
		return response;
	}

	@Override
	public InyeccionResponse inyectarMemorandum(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Memorandum memorandum) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = memorandumService.inyectarMemorandum(emisor, destinatario, observacion, memorandum);
		return response;
	}

	@Override
	public InyeccionResponse inyectarOficio(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Oficio oficio) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = oficioService.inyectarOficio(emisor, destinatario, observacion, oficio);
		return response;
	}
	
	@Override
	public InyeccionResponse inyectarPapel(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Papel papel) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = papelService.inyectarPapel(emisor, destinatario, observacion, papel);
		return response;
	}

	@Override
	public InyeccionResponse inyectarResolucion(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Resolucion resolucion) throws DocumentNotUploadedException,
			InyeccionError {
		InyeccionResponse response = new InyeccionResponse();
		response = resolucionService.inyectarResolucion(emisor, destinatario, observacion, resolucion);
		return response;
	}

}
