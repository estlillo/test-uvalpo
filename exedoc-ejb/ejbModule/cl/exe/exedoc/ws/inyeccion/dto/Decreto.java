package cl.exe.exedoc.ws.inyeccion.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Decreto {
	private String numero;
	private Date fecha;
	@XmlElement(required = true, nillable = false)
	private String autor;
	@XmlElement(required = true, nillable = false)
	private String ciudad;
	@XmlElement(required = true, nillable = false)
	private TipoRazon tipoRazon;
	@XmlElement(required = true, nillable = false)
	private String materia;
	@XmlElement(required = true, nillable = false)
	private String vistos;
	@XmlElement(required = true, nillable = false)
	private String considerando;
	@XmlElement(required = true, nillable = false)
	private String decreto;
	@XmlElement(required = true, nillable = false)
	private String indicacion;
	@XmlElement(required = false, nillable = false)
	private List<String> revisor;
	@XmlElement(required = false, nillable = false)
	private List<String> visador;
	@XmlElement(required = false, nillable = false)
	private List<String> firmante;
	@XmlElement(required = false, nillable = false)
	private List<String> distribucion;
	@XmlElement(required = false, nillable = false)
	private List<Antecedente> antecedente;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public TipoRazon getTipoRazon() {
		return tipoRazon;
	}
	public void setTipoRazon(TipoRazon tipoRazon) {
		this.tipoRazon = tipoRazon;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public String getVistos() {
		return vistos;
	}
	public void setVistos(String vistos) {
		this.vistos = vistos;
	}
	public String getConsiderando() {
		return considerando;
	}
	public void setConsiderando(String considerando) {
		this.considerando = considerando;
	}
	public String getDecreto() {
		return decreto;
	}
	public void setDecreto(String decreto) {
		this.decreto = decreto;
	}
	public String getIndicacion() {
		return indicacion;
	}
	public void setIndicacion(String indicacion) {
		this.indicacion = indicacion;
	}
	public List<String> getRevisor() {
		return revisor;
	}
	public void setRevisor(List<String> revisor) {
		this.revisor = revisor;
	}
	public List<String> getVisador() {
		return visador;
	}
	public void setVisador(List<String> visador) {
		this.visador = visador;
	}
	public List<String> getFirmante() {
		return firmante;
	}
	public void setFirmante(List<String> firmante) {
		this.firmante = firmante;
	}
	public List<String> getDistribucion() {
		return distribucion;
	}
	public void setDistribucion(List<String> distribucion) {
		this.distribucion = distribucion;
	}
	public List<Antecedente> getAntecedente() {
		return antecedente;
	}
	public void setAntecedente(List<Antecedente> antecedente) {
		this.antecedente = antecedente;
	}
}
