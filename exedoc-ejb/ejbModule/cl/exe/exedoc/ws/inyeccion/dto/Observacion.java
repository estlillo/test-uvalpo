package cl.exe.exedoc.ws.inyeccion.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "observacion", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/expediente"
)
public class Observacion {
	
	@XmlElement(required = true, nillable = false)
	private String texto;
	
	@XmlElement(nillable = false)
	private Archivo archivo;
	
	@XmlElement(required = false, nillable = false)
	private String adjuntadoPor;
	
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Archivo getArchivo() {
		return archivo;
	}
	public void setArchivo(Archivo archivo) {
		this.archivo = archivo;
	}
	public String getAdjuntadoPor() {
		return adjuntadoPor;
	}
	public void setAdjuntadoPor(String adjuntadoPor) {
		this.adjuntadoPor = adjuntadoPor;
	}
}
