package cl.exe.exedoc.ws.inyeccion.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Memorandum {
	private String numero;
	private Date fecha;
	@XmlElement(required = true, nillable = false)
	private String autor;
	@XmlElement(required = true, nillable = false)
	private String emisor;
	@XmlElement(required = true, nillable = false)
	private String materia;
	@XmlElement(required = false, nillable = false)
	private Date plazo;
	@XmlElement(required = true, nillable = false)
	private String cuerpo;
	@XmlElement(required = true, nillable = false)
	private List<String> destinatario;
	@XmlElement(required = false, nillable = false)
	private List<String> distribucion;
	@XmlElement(required = false, nillable = false)
	private List<Antecedente> antecedente;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public Date getPlazo() {
		return plazo;
	}
	public void setPlazo(Date plazo) {
		this.plazo = plazo;
	}
	public String getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
	public List<String> getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(List<String> destinatario) {
		this.destinatario = destinatario;
	}
	public List<String> getDistribucion() {
		return distribucion;
	}
	public void setDistribucion(List<String> distribucion) {
		this.distribucion = distribucion;
	}
	public List<Antecedente> getAntecedente() {
		return antecedente;
	}
	public void setAntecedente(List<Antecedente> antecedente) {
		this.antecedente = antecedente;
	}
}
