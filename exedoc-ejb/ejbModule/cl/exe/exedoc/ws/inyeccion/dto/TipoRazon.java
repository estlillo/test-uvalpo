package cl.exe.exedoc.ws.inyeccion.dto;

public enum TipoRazon {
	EXENTO, 
	TOMA_RAZON, 
	EXENTO_CON_REGISTRO
}
