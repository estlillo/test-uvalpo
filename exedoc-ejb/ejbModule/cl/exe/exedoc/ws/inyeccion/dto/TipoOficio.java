package cl.exe.exedoc.ws.inyeccion.dto;

public enum TipoOficio {
	CIRCULAR, 
	ORDINARIO, 
	RESERVADO
}
