package cl.exe.exedoc.ws.inyeccion.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlSeeAlso({Carta.class, Memorandum.class, Oficio.class, Resolucion.class})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "expediente", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/expediente"
)
public class Expediente {
	
	@XmlElement(required = true, nillable = false)
	private DocumentoElectronico documento;
	
	@XmlElement(required = false, nillable = false)
	private List<Observacion> observacion;
	
	@XmlElement(required = true, nillable = false)
	private String emisor;
	
	@XmlElement(required = true, nillable = false)
	private List<Destinatario> destinatario;
	
	public DocumentoElectronico getDocumento() {
		return documento;
	}
	public void setDocumento(DocumentoElectronico documento) {
		this.documento = documento;
	}
	public List<Observacion> getObservacion() {
		return observacion;
	}
	public void setObservacion(List<Observacion> observacion) {
		this.observacion = observacion;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public List<Destinatario> getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(List<Destinatario> destinatario) {
		this.destinatario = destinatario;
	}
}
