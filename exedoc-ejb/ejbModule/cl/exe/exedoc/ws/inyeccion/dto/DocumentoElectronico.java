package cl.exe.exedoc.ws.inyeccion.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "documentoElectronico", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/documento"
)
public class DocumentoElectronico {
	@XmlElement(required = true, nillable = false)
	private String numero;
	
	@XmlElement(required = true, nillable = false)
	private Date fecha;
	
	@XmlElement(required = true, nillable = false)
	private String autor;
	
	@XmlElement(required = false, nillable = false)
	private List<Antecedente> antecedentes;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public List<Antecedente> getAntecedentes() {
		return antecedentes;
	}
	public void setAntecedentes(List<Antecedente> antecedentes) {
		this.antecedentes = antecedentes;
	}
}
