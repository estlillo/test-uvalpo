package cl.exe.exedoc.ws.inyeccion.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "archivo", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/expediente"
)
public class Archivo {
	@XmlElement(required = true, nillable = false)
	private byte[] data;
	@XmlElement(required = true, nillable = false)
	private String nombreArchivo;
	@XmlElement(required = true, nillable = false)
	private String contentType;
	
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
