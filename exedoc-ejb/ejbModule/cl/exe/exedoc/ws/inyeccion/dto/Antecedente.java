package cl.exe.exedoc.ws.inyeccion.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "antecedente", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/antecedente"
)
public class Antecedente {

	@XmlElement(required = true, nillable = false)
	private String materia;
	
	@XmlElement(required = true, nillable = false)
	private byte[] data;
	
	@XmlElement(required = true, nillable = false)
	private String nombreArchivo;
	
	@XmlElement(required = false, nillable = false)
	private String adjuntadoPor;
	
	@XmlElement(required = true, nillable = false)
	private String contentType;
	
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getAdjuntadoPor() {
		return adjuntadoPor;
	}
	public void setAdjuntadoPor(String adjuntadoPor) {
		this.adjuntadoPor = adjuntadoPor;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
