package cl.exe.exedoc.ws.inyeccion.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Digital {
	@XmlElement(required = true, nillable = false)
	private String numero;
	@XmlElement(required = true, nillable = false)
	private Date fecha;
	@XmlElement(required = true, nillable = false)
	private String autor;
	@XmlElement(required = true, nillable = false)
	private String tipoDocumento;
	private boolean reservado;
	private String antecedentes;
	@XmlElement(required = true, nillable = false)
	private String materia;
	@XmlElement(required = true, nillable = false)
	private Date plazo;
	private String nivelUrgencia;
	@XmlElement(required = true, nillable = false)
	private String emisor;
	@XmlElement(required = true, nillable = false)
	private List<String> destinatario;
	@XmlElement(required = true, nillable = false)
	private byte[] dataArchivo;
	@XmlElement(required = true, nillable = false)
	private String nombreArchivo;
	@XmlElement(required = true, nillable = false)
	private String contentType;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public boolean isReservado() {
		return reservado;
	}
	public void setReservado(boolean reservado) {
		this.reservado = reservado;
	}
	public String getAntecedentes() {
		return antecedentes;
	}
	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public Date getPlazo() {
		return plazo;
	}
	public void setPlazo(Date plazo) {
		this.plazo = plazo;
	}
	public String getNivelUrgencia() {
		return nivelUrgencia;
	}
	public void setNivelUrgencia(String nivelUrgencia) {
		this.nivelUrgencia = nivelUrgencia;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public List<String> getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(List<String> destinatario) {
		this.destinatario = destinatario;
	}
	public byte[] getDataArchivo() {
		return dataArchivo;
	}
	public void setDataArchivo(byte[] dataArchivo) {
		this.dataArchivo = dataArchivo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
