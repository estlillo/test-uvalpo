package cl.exe.exedoc.ws.inyeccion.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
	name = "destinatario", 
	namespace = "http://inyeccion.ws.exedoc.exe.cl/destinatario"
)
public class Destinatario {
	
	@XmlElement(required = true, nillable = false)
	private String usuario;
	
	@XmlElement(required = false, nillable = false)
	private Boolean copia;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Boolean getCopia() {
		return copia;
	}
	public void setCopia(Boolean copia) {
		this.copia = copia;
	}
}
