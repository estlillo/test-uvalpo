package cl.exe.exedoc.ws.inyeccion;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class InyeccionException extends Exception {

	private static final long serialVersionUID = 2642759827778596198L;

	public InyeccionException() {
        super("Falla en servicio de inyeccion");
    }

    public InyeccionException(final String message) {
        
    }

    public InyeccionException(final Throwable cause) {
    }

    public InyeccionException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
