package cl.exe.exedoc.ws.inyeccion;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class InyeccionError extends Exception {
	
	private static final long serialVersionUID = -3629846703044059472L;

	public InyeccionError() {
		super();
	}
	
	public InyeccionError(String message) {
		super(message);
	}
}
