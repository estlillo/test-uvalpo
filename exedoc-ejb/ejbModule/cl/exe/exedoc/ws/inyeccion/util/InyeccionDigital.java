package cl.exe.exedoc.ws.inyeccion.util;

import java.util.List;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.Digital;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;

public interface InyeccionDigital {

	InyeccionResponse inyectarDigital(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Digital digital) throws DocumentNotUploadedException,
			InyeccionError;

}
