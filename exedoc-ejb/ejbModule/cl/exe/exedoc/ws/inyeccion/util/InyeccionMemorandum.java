package cl.exe.exedoc.ws.inyeccion.util;

import java.util.List;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Memorandum;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;

public interface InyeccionMemorandum {

	InyeccionResponse inyectarMemorandum(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Memorandum memorandum) throws DocumentNotUploadedException,
			InyeccionError;

}
