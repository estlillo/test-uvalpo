package cl.exe.exedoc.ws.inyeccion.util;

import java.util.List;

import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Carta;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;

public interface InyeccionCarta {

	InyeccionResponse inyectarCarta(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Carta carta) throws DocumentNotUploadedException,
			InyeccionError;

}
