package cl.exe.exedoc.ws.inyeccion.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.Messages;

import cl.exe.exedoc.entity.Contenido;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Antecedente;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Oficio;
import cl.exe.exedoc.ws.inyeccion.dto.TipoOficio;

@Stateless
@Name(InyeccionOficioBean.NAME)
public class InyeccionOficioBean implements InyeccionOficio {
	
	public static final String NAME = "inyeccionOficio";
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private ManejarExpedienteInterface me;
	
	@EJB
	private RepositorioLocal repositorio;
	
	@Override
	@SuppressWarnings("unchecked")
	public InyeccionResponse inyectarOficio(
			String emisor,
			List<Destinatario> destinatario,
			List<Observacion> observacion,
			Oficio oficio) throws DocumentNotUploadedException, InyeccionError {
		Date ahora = new Date();
		InyeccionResponse response = new InyeccionResponse();
		if (emisor == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido"));
		}
		Query query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", emisor);
		query.setParameter("vigente", true);
		Persona emisorObj;
		List<Persona> result = (List<Persona>)query.getResultList();
		if (result == null || result.size() != 1) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido") + " [" + emisor + "]");
		}
		emisorObj = result.get(0);
		//usuario = emisorObj;
		if (destinatario == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido"));
		}
		List<Persona> destinatariosObj = new ArrayList<Persona>();
		for (Destinatario d : destinatario) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d.getUsuario());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido") + " [" + d.getUsuario() + "]");
			}
			Persona p = result.get(0);
			p.setDestinatarioConCopia(d.getCopia() == null ? Boolean.FALSE : d.getCopia());
			destinatariosObj.add(p);
		}
		
		//ingreso oficio
		if (oficio == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_no_valido"));
		}
		
		cl.exe.exedoc.entity.Oficio documento = new cl.exe.exedoc.entity.Oficio();
		if (oficio.getAutor() == null) {
			documento.setAutor(emisorObj);
		} else {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", oficio.getAutor());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_autor_no_valido") + " [" + oficio.getAutor() + "]");
			}
			documento.setAutor(result.get(0));
		}
		
		if (oficio.getEmisor() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_emisor_no_valido"));
		}
		query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", oficio.getEmisor());
		query.setParameter("vigente", true);
		result = (List<Persona>)query.getResultList();
		if (result == null || result.size() != 1) {
			response.setCodigoError(2);
			response.setMensaje(Messages.instance().get("ws_inyeccion_documento_emisor_no_valido") + " [" + oficio.getEmisor() + "]");
			return response;
		}
		documento.setIdEmisor(result.get(0).getId());
		documento.setEmisor(InyeccionCommon.getPersonaCargo(result.get(0)));

		if (oficio.getDestinatario() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido"));
		}
		List<Persona> destinatariosDocumento = new ArrayList<Persona>();
		for (String d : oficio.getDestinatario()) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d);
			query.setParameter("vigente", true);
			result = query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido") + " [" + d + "]");
			}
			destinatariosDocumento.add(result.get(0));
		}
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (Persona item : destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(InyeccionCommon.getPersonaCargo(item));
			dd.setDestinatarioPersona(item);
			dd.setDocumento(documento);
			ddList.add(dd);
		}
		documento.setDestinatarios(ddList);
		
		List<Persona> distribucionDocumento = new ArrayList<Persona>();
		if (oficio.getDistribucion() != null) {
			for (String d : oficio.getDistribucion()) {
				query = em.createNamedQuery("Persona.findByUsuarioVigente");
				query.setParameter("usuario", d);
				query.setParameter("vigente", true);
				result = query.getResultList();
				if (result == null || result.size() != 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_distribucion_no_valido") + " [" + d + "]");
				}
				distribucionDocumento.add(result.get(0));
			}
		}
		ddList = new LinkedList<ListaPersonasDocumento>();
		for (Persona item : distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(InyeccionCommon.getPersonaCargo(item));
			dd.setDestinatarioPersona(item);
			dd.setDocumento(documento);
			ddList.add(dd);
		}
		documento.setDistribucion(ddList);
		
		if (oficio.getTipo() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_oficio_tipo_no_valido"));
		}
		documento.setReservado(false);
		if (oficio.getTipo().equals(TipoOficio.ORDINARIO)) {
			documento.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.OFICIO_ORDINARIO));
		} else if (oficio.getTipo().equals(TipoOficio.CIRCULAR)) {
			documento.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.OFICIO_CIRCULAR));
		} else if (oficio.getTipo().equals(TipoOficio.RESERVADO)) {
			documento.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.OFICIO_RESERVADO));
			documento.setReservado(true);
		} else {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_oficio_tipo_no_valido"));
		}
		
		//insertar antecedentes
		List<cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico> listAdjuntos = new ArrayList<cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico>();
		if (oficio.getAntecedente() != null) {
			for (Antecedente a : oficio.getAntecedente()) {
				if (a == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_no_valido"));
				} else {
					response.setCodigoError(4);
					if (a.getMateria() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_materia_no_valido"));
					}
					if (a.getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_nombre_archivo_no_valido"));
					}
					if (a.getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_content_type_archivo_no_valido"));
					}
					if (a.getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_data_archivo_no_valido"));
					}
					Persona p;
					if (a.getAdjuntadoPor() == null) {
						p = documento.getAutor();
					} else {
						query = em.createNamedQuery("Persona.findByUsuarioVigente");
						query.setParameter("usuario", a.getAdjuntadoPor());
						query.setParameter("vigente", true);
						result = query.getResultList();
						if (result == null || result.size() != 1) {
							throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_adjuntado_por_no_valido") + " [" + a.getAdjuntadoPor() + "]");
						}
						p = result.get(0);
					}
					response.setCodigoError(null);
					
					cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico aade = new cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico();
					aade.setAdjuntadoPor(p);
					aade.setFecha(ahora);
					aade.setContentType(a.getContentType());
					aade.setNombreArchivo(a.getNombreArchivo());
					aade.setMateria(a.getMateria());
					aade.setArchivo(a.getData());
					aade.setDocumentoElectronico(documento);
					listAdjuntos.add(aade);
				}
			}
		}
		documento.setArchivosAdjuntos(listAdjuntos);
		//validar observaciones
		List<cl.exe.exedoc.entity.Observacion> listObservaciones = new ArrayList<cl.exe.exedoc.entity.Observacion>();
		if (observacion != null) {
			for (Observacion o : observacion) {
				response.setCodigoError(3);
				if (o == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_no_valido"));
				}
				cl.exe.exedoc.entity.Observacion obs = new cl.exe.exedoc.entity.Observacion();
				if (o.getTexto() == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_texto_no_valido"));
				}
				obs.setObservacion(o.getTexto());
				Persona p;
				if (o.getAdjuntadoPor() == null) {
					p = emisorObj;
				} else {
					query = em.createNamedQuery("Persona.findByUsuarioVigente");
					query.setParameter("usuario", o.getAdjuntadoPor());
					query.setParameter("vigente", true);
					result = query.getResultList();
					if (result == null || result.size() != 1) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_adjuntado_por_no_valido") + " [" + o.getAdjuntadoPor() + "]");
					}
					p = result.get(0);
				}
				obs.setAutor(p);
				obs.setFecha(ahora);
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				//obs.setExpediente(expediente); //mas adelante
				if (o.getArchivo() != null) {
					response.setCodigoError(3);
					if (o.getArchivo().getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_nombre_archivo_no_valido"));
					}
					if (o.getArchivo().getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_content_type_no_valido"));
					}
					if (o.getArchivo().getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_data_archivo_no_valido"));
					}
					cl.exe.exedoc.entity.ObservacionArchivo oa = new cl.exe.exedoc.entity.ObservacionArchivo();
					oa.setFecha(ahora);
					oa.setNombreArchivo(o.getArchivo().getNombreArchivo());
					oa.setContentType(o.getArchivo().getContentType());
					oa.setArchivo(o.getArchivo().getData());
					obs.setObservacionArchivo(oa);
				}
				response.setCodigoError(null);
				listObservaciones.add(obs);
			}
		}
		//insertar observaciones
		for (cl.exe.exedoc.entity.Observacion obs : listObservaciones) {
			em.persist(obs);
			ObservacionArchivo oa = obs.getObservacionArchivo();
			if (oa != null) {
					em.persist(oa);
				final String nombreArchivoObservacion = InyeccionCommon.getNombreArchivo(
						oa.getNombreArchivo(), oa.getId().toString());
				Documento d = new Documento();
				d.setAutor(obs.getAutor());
				d.setFormatoDocumento(new FormatoDocumento(0));
				String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
				obs.getObservacionArchivo().setCmsId(cmsId);
				em.merge(oa);
			}
			em.merge(obs);
		}
		
		documento.setFechaCreacion(ahora);
		documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.ORIGINAL));
		documento.setNumeroDocumento(oficio.getNumero());
		documento.setFechaDocumentoOrigen(oficio.getFecha());
		documento.setMateria(oficio.getMateria());
		documento.setPlazo(oficio.getPlazo());
		documento.setAntecedentes(oficio.getAntecedenteTexto());
		documento.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		documento.setCompletado(false);

		List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
		Contenido parrafo = new Contenido();
		parrafo.setNumero(1L);
		parrafo.setDocumento(documento);
		parrafo.setCuerpo(oficio.getCuerpo());
		listaParrafos.add(parrafo);
		documento.setContenido(listaParrafos);
		
		cl.exe.exedoc.entity.Expediente expediente = new cl.exe.exedoc.entity.Expediente();
		expediente.setEmisor(emisorObj);
		expediente.setFechaIngreso(ahora);
		
		me.crearExpediente(expediente);
		me.agregarOriginalAExpediente(expediente, documento, true);
		for (cl.exe.exedoc.entity.Observacion o : listObservaciones) {
			o.setExpediente(expediente);
		}
		expediente.setObservaciones(listObservaciones);
		em.merge(expediente);
		
		for (Persona p : destinatariosObj) {
			me.agregarDestinatarioAExpediente(
					expediente, 
					emisorObj, 
					new cl.exe.exedoc.session.utils.Destinatario(p, p.getDestinatarioConCopia()), 
					new Date());
		}
		me.despacharExpediente(expediente);
        List<cl.exe.exedoc.entity.Expediente> expedientes = me.buscarExpedientesHijos(expediente);
        for (cl.exe.exedoc.entity.Expediente e : expedientes) {
            me.despacharExpediente(e);
        }
        response.setNumero(expediente.getNumeroExpediente());
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setIdDocumento(documento.getId());
		return response;
	}
}
