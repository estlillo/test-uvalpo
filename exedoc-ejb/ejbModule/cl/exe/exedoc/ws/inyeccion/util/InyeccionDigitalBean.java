package cl.exe.exedoc.ws.inyeccion.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.Messages;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.Digital;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;

@Stateless
@Name(InyeccionDigitalBean.NAME)
public class InyeccionDigitalBean implements InyeccionDigital {
	
	public static final String NAME = "inyeccionDigital";
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private ManejarExpedienteInterface me;
	
	@EJB
	private RepositorioLocal repositorio;
	
	@Override
	@SuppressWarnings("unchecked")
	public InyeccionResponse inyectarDigital(
			String emisor,
			List<Destinatario> destinatario,
			List<Observacion> observacion,
			Digital digital) throws DocumentNotUploadedException, InyeccionError {
		Date ahora = new Date();
		InyeccionResponse response = new InyeccionResponse();
		if (emisor == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido"));
		}
		Query query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", emisor);
		query.setParameter("vigente", true);
		Persona emisorObj;
		List<Persona> result = (List<Persona>)query.getResultList();
		if (result == null || result.size() != 1) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido") + " [" + emisor + "]");
		}
		emisorObj = result.get(0);
		//usuario = emisorObj;
		if (destinatario == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido"));
		}
		List<Persona> destinatariosObj = new ArrayList<Persona>();
		for (Destinatario d : destinatario) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d.getUsuario());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido") + " [" + d.getUsuario() + "]");
			}
			Persona p = result.get(0);
			p.setDestinatarioConCopia(d.getCopia() == null ? Boolean.FALSE : d.getCopia());
			destinatariosObj.add(p);
		}
		
		//ingreso digital
		if (digital == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_no_valido"));
		}
		
		cl.exe.exedoc.entity.DocumentoBinario documento = new cl.exe.exedoc.entity.DocumentoBinario();
		if (digital.getAutor() == null) {
			documento.setAutor(emisorObj);
		} else {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", digital.getAutor());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_autor_no_valido") + " [" + digital.getAutor() + "]");
			}
			documento.setAutor(result.get(0));
		}
		
		if (digital.getEmisor() == null) {
			response.setCodigoError(2);
			response.setMensaje(Messages.instance().get("ws_inyeccion_documento_emisor_no_valido"));
			return response;
		}
		query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", digital.getEmisor());
		query.setParameter("vigente", true);
		result = (List<Persona>)query.getResultList();
		if (result == null || result.size() == 0) {
			documento.setEmisor(digital.getEmisor());
		} else {
			documento.setIdEmisor(result.get(0).getId());
			documento.setEmisor(InyeccionCommon.getPersonaCargo(result.get(0)));
		}

		if (digital.getDestinatario() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido"));
		}
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (String d : digital.getDestinatario()) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d);
			query.setParameter("vigente", true);
			result = query.getResultList();
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(d);
			if (result != null && result.size() > 0) {
				if (result.size() > 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido") + " [" + d + "]");
				}
				dd.setDestinatarioPersona(result.get(0));
				dd.setDestinatario(InyeccionCommon.getPersonaCargo(result.get(0)));
			}
			dd.setDocumento(documento);
			ddList.add(dd);
		}
		documento.setDestinatarios(ddList);
		
		if (digital.getTipoDocumento() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido"));
		}
		TipoDocumento tipoDocumento = null;
		query = em.createNamedQuery("tipoDocumentoActivo.yNoEsElectronico");
		List<TipoDocumento> resultTipo = query.getResultList();
		if (resultTipo == null || resultTipo.size() == 0) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido") + " [" + digital.getTipoDocumento() + "]");
		}
		for (TipoDocumento td : resultTipo) {
			if (td.getDescripcion().equalsIgnoreCase(digital.getTipoDocumento())) {
				tipoDocumento = td;
				break;
			}
		}
		if (tipoDocumento == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido") + " [" + digital.getTipoDocumento() + "]");
		}
		Alerta alerta = null;
		if (digital.getNivelUrgencia() != null) {
			query = em.createNamedQuery("Alerta.selectAll");
			List<Alerta> resultAlerta = query.getResultList();
			if (resultAlerta == null || resultAlerta.size() == 0) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_alerta_no_valido") + " [" + digital.getNivelUrgencia() + "]");
			}
			for (Alerta a : resultAlerta) {
				if (a.getNombre().equalsIgnoreCase(digital.getNivelUrgencia())) {
					alerta = a;
					break;
				}
			}
			if (alerta == null) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_alerta_no_valido") + " [" + digital.getNivelUrgencia() + "]");
			}
		}
		if (digital.getFecha() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_fecha_no_valido"));
		}
		if (digital.getMateria() == null) {
			response.setCodigoError(2);
			response.setMensaje(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
			return response;
		}
		digital.setMateria(digital.getMateria().trim());
		if (digital.getMateria().length() == 0) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
		}
		if (digital.getNombreArchivo() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_digital_nombre_archivo_no_valido") + " [" + digital.getNombreArchivo() + "]");
		}
		if (digital.getContentType() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_digital_content_type_archivo_no_valido") + " [" + digital.getTipoDocumento() + "]");
		}
		if (digital.getDataArchivo() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_digital_data_archivo_no_valido") + " [" + digital.getTipoDocumento() + "]");
		}
		cl.exe.exedoc.entity.ArchivoDocumentoBinario archivo = new cl.exe.exedoc.entity.ArchivoDocumentoBinario();
		archivo.setFecha(ahora);
		archivo.setNombreArchivo(digital.getNombreArchivo());
		archivo.setContentType(digital.getContentType());
		archivo.setArchivo(digital.getDataArchivo());
		
		//validar observaciones
		List<cl.exe.exedoc.entity.Observacion> listObservaciones = new ArrayList<cl.exe.exedoc.entity.Observacion>();
		if (observacion != null) {
			for (Observacion o : observacion) {
				response.setCodigoError(3);
				if (o == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_no_valido"));
				}
				cl.exe.exedoc.entity.Observacion obs = new cl.exe.exedoc.entity.Observacion();
				if (o.getTexto() == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_texto_no_valido"));
				}
				obs.setObservacion(o.getTexto());
				Persona p;
				if (o.getAdjuntadoPor() == null) {
					p = emisorObj;
				} else {
					query = em.createNamedQuery("Persona.findByUsuarioVigente");
					query.setParameter("usuario", o.getAdjuntadoPor());
					query.setParameter("vigente", true);
					result = query.getResultList();
					if (result == null || result.size() != 1) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_adjuntado_por_no_valido") + " [" + o.getAdjuntadoPor() + "]");
					}
					p = result.get(0);
				}
				obs.setAutor(p);
				obs.setFecha(ahora);
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				//obs.setExpediente(expediente); //mas adelante
				if (o.getArchivo() != null) {
					response.setCodigoError(3);
					if (o.getArchivo().getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_nombre_archivo_no_valido"));
					}
					if (o.getArchivo().getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_content_type_no_valido"));
					}
					if (o.getArchivo().getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_data_archivo_no_valido"));
					}
					cl.exe.exedoc.entity.ObservacionArchivo oa = new cl.exe.exedoc.entity.ObservacionArchivo();
					oa.setFecha(ahora);
					oa.setNombreArchivo(o.getArchivo().getNombreArchivo());
					oa.setContentType(o.getArchivo().getContentType());
					oa.setArchivo(o.getArchivo().getData());
					obs.setObservacionArchivo(oa);
				}
				response.setCodigoError(null);
				listObservaciones.add(obs);
			}
		}
		//insertar observaciones
		for (cl.exe.exedoc.entity.Observacion obs : listObservaciones) {
			em.persist(obs);
			ObservacionArchivo oa = obs.getObservacionArchivo();
			if (oa != null) {
					em.persist(oa);
				final String nombreArchivoObservacion = InyeccionCommon.getNombreArchivo(
						oa.getNombreArchivo(), oa.getId().toString());
				Documento d = new Documento();
				d.setAutor(obs.getAutor());
				d.setFormatoDocumento(new FormatoDocumento(0));
				String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
				obs.getObservacionArchivo().setCmsId(cmsId);
				em.merge(oa);
			}
			em.merge(obs);
		}
		
		
		documento.setFechaCreacion(ahora);
		documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.ORIGINAL));
		documento.setNumeroDocumento(digital.getNumero());
		documento.setFechaDocumentoOrigen(digital.getFecha());
		documento.setMateria(digital.getMateria() != null ? (digital.getMateria().length() <= 2000 ? digital.getMateria() : digital.getMateria().substring(0, 2000)) : null);
		documento.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.DIGITAL));
		documento.setTipoDocumento(tipoDocumento);
		documento.setCompletado(false);
		documento.setReservado(digital.isReservado());
		documento.setAntecedentes(digital.getAntecedentes() != null ? (digital.getAntecedentes().length() <= 2000? digital.getAntecedentes() : digital.getAntecedentes().substring(0, 2000)) : null);
		documento.setPlazo(digital.getPlazo());
		documento.setAlerta(alerta);
		documento.setArchivo(archivo);

		cl.exe.exedoc.entity.Expediente expediente = new cl.exe.exedoc.entity.Expediente();
		expediente.setEmisor(emisorObj);
		expediente.setFechaIngreso(ahora);
		
		me.crearExpediente(expediente);
		me.agregarOriginalAExpediente(expediente, documento, true);
		for (cl.exe.exedoc.entity.Observacion o : listObservaciones) {
			o.setExpediente(expediente);
		}
		expediente.setObservaciones(listObservaciones);
		em.merge(expediente);
		
		for (Persona p : destinatariosObj) {
			me.agregarDestinatarioAExpediente(
					expediente, 
					emisorObj, 
					new cl.exe.exedoc.session.utils.Destinatario(p, p.getDestinatarioConCopia()), 
					new Date());
		}
		me.despacharExpediente(expediente);
        List<cl.exe.exedoc.entity.Expediente> expedientes = me.buscarExpedientesHijos(expediente);
        for (cl.exe.exedoc.entity.Expediente e : expedientes) {
            me.despacharExpediente(e);
        }
        response.setNumero(expediente.getNumeroExpediente());
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setIdDocumento(documento.getId());
		return response;
	}
}
