package cl.exe.exedoc.ws.inyeccion.util;

import java.util.StringTokenizer;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;

public class InyeccionCommon {
	private static final String GUION = " - ";
	
	public static String getPersonaCargo(Persona per) {
		if (per != null) {
			String value = per.getNombres() + " " + per.getApellidoPaterno();
			Cargo cargo = per.getCargo();
			value += GUION + cargo.getDescripcion();
			UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
			value += GUION + uo.getDescripcion();
			return value;
		}
		return null;
	}
	
	public static String getNombreArchivo(String nombreArchivo, String id) {
		String extension = "";
		StringTokenizer st = new StringTokenizer(nombreArchivo, ".");
		while (st.hasMoreElements()) {
			extension = st.nextToken();
		}
		String nombre = id + "." + extension.toLowerCase();
		return nombre;
	}
}
