package cl.exe.exedoc.ws.inyeccion.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.Messages;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Papel;

@Stateless
@Name(InyeccionPapelBean.NAME)
public class InyeccionPapelBean implements InyeccionPapel {

	public static final String NAME = "inyeccionPapel";
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private ManejarExpedienteInterface me;
	
	@EJB
	private RepositorioLocal repositorio;
	
	@Override
	@SuppressWarnings("unchecked")
	public InyeccionResponse inyectarPapel(String emisor,
			List<Destinatario> destinatario, List<Observacion> observacion,
			Papel papel) throws DocumentNotUploadedException, InyeccionError {
		Date ahora = new Date();
		InyeccionResponse response = new InyeccionResponse();
		if (emisor == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido"));
		}
		Query query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", emisor);
		query.setParameter("vigente", true);
		Persona emisorObj;
		List<Persona> result = (List<Persona>)query.getResultList();
		if (result == null || result.size() != 1) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido") + " [" + emisor + "]");
		}
		emisorObj = result.get(0);
		//usuario = emisorObj;
		if (destinatario == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido"));
		}
		List<Persona> destinatariosObj = new ArrayList<Persona>();
		for (Destinatario d : destinatario) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d.getUsuario());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido") + " [" + d.getUsuario() + "]");
			}
			Persona p = result.get(0);
			p.setDestinatarioConCopia(d.getCopia() == null ? Boolean.FALSE : d.getCopia());
			destinatariosObj.add(p);
		}
		
		//ingreso papel
		if (papel == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_no_valido"));
		}
		
		cl.exe.exedoc.entity.DocumentoPapel documento = new cl.exe.exedoc.entity.DocumentoPapel();
		if (papel.getAutor() == null) {
			documento.setAutor(emisorObj);
		} else {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", papel.getAutor());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_autor_no_valido") + " [" + papel.getAutor() + "]");
			}
			documento.setAutor(result.get(0));
		}
		
		if (papel.getEmisor() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_emisor_no_valido"));
		}
		query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", papel.getEmisor());
		query.setParameter("vigente", true);
		result = (List<Persona>)query.getResultList();
		if (result == null || result.size() == 0) {
			documento.setEmisor(papel.getEmisor());
		} else {
			documento.setIdEmisor(result.get(0).getId());
			documento.setEmisor(InyeccionCommon.getPersonaCargo(result.get(0)));
		}

		if (papel.getDestinatario() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido"));
		}
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (String d : papel.getDestinatario()) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d);
			query.setParameter("vigente", true);
			result = query.getResultList();
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(d);
			if (result != null && result.size() > 0) {
				if (result.size() > 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_destinatario_no_valido") + " [" + d + "]");
				}
				dd.setDestinatarioPersona(result.get(0));
				dd.setDestinatario(InyeccionCommon.getPersonaCargo(result.get(0)));
			}
			dd.setDocumento(documento);
			ddList.add(dd);
		}
		documento.setDestinatarios(ddList);
		
		if (papel.getTipoDocumento() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido"));
		}
		TipoDocumento tipoDocumento = null;
		query = em.createNamedQuery("tipoDocumentoActivo.yNoEsElectronico");
		List<TipoDocumento> resultTipo = query.getResultList();
		if (resultTipo == null || resultTipo.size() == 0) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido") + " [" + papel.getTipoDocumento() + "]");
		}
		for (TipoDocumento td : resultTipo) {
			if (td.getDescripcion().equalsIgnoreCase(papel.getTipoDocumento())) {
				tipoDocumento = td;
				break;
			}
		}
		if (tipoDocumento == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_tipo_no_valido") + " [" + papel.getTipoDocumento() + "]");
		}
		Alerta alerta = null;
		if (papel.getNivelUrgencia() != null) {
			query = em.createNamedQuery("Alerta.selectAll");
			List<Alerta> resultAlerta = query.getResultList();
			if (resultAlerta == null || resultAlerta.size() == 0) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_alerta_no_valido") + " [" + papel.getNivelUrgencia() + "]");
			}
			for (Alerta a : resultAlerta) {
				if (a.getNombre().equalsIgnoreCase(papel.getNivelUrgencia())) {
					alerta = a;
					break;
				}
			}
			if (alerta == null) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_alerta_no_valido") + " [" + papel.getNivelUrgencia() + "]");
			}
		}
		if (papel.getFecha() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_fecha_no_valido"));
		}
		if (papel.getMateria() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
		}
		papel.setMateria(papel.getMateria().trim());
		if (papel.getMateria().length() == 0) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
		}
		
		//validar observaciones
		List<cl.exe.exedoc.entity.Observacion> listObservaciones = new ArrayList<cl.exe.exedoc.entity.Observacion>();
		if (observacion != null) {
			for (Observacion o : observacion) {
				response.setCodigoError(3);
				if (o == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_no_valido"));
				}
				cl.exe.exedoc.entity.Observacion obs = new cl.exe.exedoc.entity.Observacion();
				if (o.getTexto() == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_texto_no_valido"));
				}
				obs.setObservacion(o.getTexto());
				Persona p;
				if (o.getAdjuntadoPor() == null) {
					p = emisorObj;
				} else {
					query = em.createNamedQuery("Persona.findByUsuarioVigente");
					query.setParameter("usuario", o.getAdjuntadoPor());
					query.setParameter("vigente", true);
					result = query.getResultList();
					if (result == null || result.size() != 1) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_adjuntado_por_no_valido") + " [" + o.getAdjuntadoPor() + "]");
					}
					p = result.get(0);
				}
				obs.setAutor(p);
				obs.setFecha(ahora);
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				//obs.setExpediente(expediente); //mas adelante
				if (o.getArchivo() != null) {
					response.setCodigoError(3);
					if (o.getArchivo().getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_nombre_archivo_no_valido"));
					}
					if (o.getArchivo().getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_content_type_no_valido"));
					}
					if (o.getArchivo().getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_data_archivo_no_valido"));
					}
					cl.exe.exedoc.entity.ObservacionArchivo oa = new cl.exe.exedoc.entity.ObservacionArchivo();
					oa.setFecha(ahora);
					oa.setNombreArchivo(o.getArchivo().getNombreArchivo());
					oa.setContentType(o.getArchivo().getContentType());
					oa.setArchivo(o.getArchivo().getData());
					obs.setObservacionArchivo(oa);
				}
				response.setCodigoError(null);
				listObservaciones.add(obs);
			}
		}
		//insertar observaciones
		for (cl.exe.exedoc.entity.Observacion obs : listObservaciones) {
			em.persist(obs);
			ObservacionArchivo oa = obs.getObservacionArchivo();
			if (oa != null) {
					em.persist(oa);
				final String nombreArchivoObservacion = InyeccionCommon.getNombreArchivo(
						oa.getNombreArchivo(), oa.getId().toString());
				Documento d = new Documento();
				d.setAutor(obs.getAutor());
				d.setFormatoDocumento(new FormatoDocumento(0));
				String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
				obs.getObservacionArchivo().setCmsId(cmsId);
				em.merge(oa);
			}
			em.merge(obs);
		}
		
		
		documento.setFechaCreacion(ahora);
		documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.ORIGINAL));
		documento.setNumeroDocumento(papel.getNumero());
		documento.setFechaDocumentoOrigen(papel.getFecha());
		documento.setMateria(papel.getMateria());
		documento.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.PAPEL));
		documento.setTipoDocumento(tipoDocumento);
		documento.setCompletado(false);
		documento.setReservado(papel.isReservado());
		documento.setAntecedentes(papel.getAntecedentes());
		documento.setPlazo(papel.getPlazo());
		documento.setNumeroHojas(papel.getNumeroHojas());
		documento.setAlerta(alerta);

		cl.exe.exedoc.entity.Expediente expediente = new cl.exe.exedoc.entity.Expediente();
		expediente.setEmisor(emisorObj);
		expediente.setFechaIngreso(ahora);
		
		me.crearExpediente(expediente);
		me.agregarOriginalAExpediente(expediente, documento, true);
		for (cl.exe.exedoc.entity.Observacion o : listObservaciones) {
			o.setExpediente(expediente);
		}
		expediente.setObservaciones(listObservaciones);
		em.merge(expediente);
		
		for (Persona p : destinatariosObj) {
			me.agregarDestinatarioAExpediente(
					expediente, 
					emisorObj, 
					new cl.exe.exedoc.session.utils.Destinatario(p, p.getDestinatarioConCopia()), 
					new Date());
		}
		me.despacharExpediente(expediente);
        List<cl.exe.exedoc.entity.Expediente> expedientes = me.buscarExpedientesHijos(expediente);
        for (cl.exe.exedoc.entity.Expediente e : expedientes) {
            me.despacharExpediente(e);
        }
        response.setNumero(expediente.getNumeroExpediente());
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setIdDocumento(documento.getId());
		return response;
	}
}
