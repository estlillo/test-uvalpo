package cl.exe.exedoc.ws.inyeccion.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.international.Messages;

import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.ws.inyeccion.InyeccionError;
import cl.exe.exedoc.ws.inyeccion.dto.Antecedente;
import cl.exe.exedoc.ws.inyeccion.dto.Destinatario;
import cl.exe.exedoc.ws.inyeccion.dto.InyeccionResponse;
import cl.exe.exedoc.ws.inyeccion.dto.Observacion;
import cl.exe.exedoc.ws.inyeccion.dto.Resolucion;
import cl.exe.exedoc.ws.inyeccion.dto.TipoRazon;

@Stateless
@Name(InyeccionResolucionBean.NAME)
public class InyeccionResolucionBean implements InyeccionResolucion {
	
	public static final String NAME = "inyeccionResolucion";
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	private ManejarExpedienteInterface me;
	
	@EJB
	private RepositorioLocal repositorio;
	
	@Override
	@SuppressWarnings("unchecked")
	public InyeccionResponse inyectarResolucion(
			String emisor,
			List<Destinatario> destinatario,
			List<Observacion> observacion,
			Resolucion resolucion) throws DocumentNotUploadedException, InyeccionError {
		Date ahora = new Date();
		InyeccionResponse response = new InyeccionResponse();
		if (emisor == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido"));
		}
		Query query = em.createNamedQuery("Persona.findByUsuarioVigente");
		query.setParameter("usuario", emisor);
		query.setParameter("vigente", true);
		Persona emisorObj;
		List<Persona> result = (List<Persona>)query.getResultList();
		if (result == null || result.size() != 1) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_emisor_no_valido") + " [" + emisor + "]");
		}
		emisorObj = result.get(0);
		//usuario = emisorObj;
		if (destinatario == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido"));
		}
		List<Persona> destinatariosObj = new ArrayList<Persona>();
		for (Destinatario d : destinatario) {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", d.getUsuario());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_expediente_destinatario_no_valido") + " [" + d.getUsuario() + "]");
			}
			Persona p = result.get(0);
			p.setDestinatarioConCopia(d.getCopia() == null ? Boolean.FALSE : d.getCopia());
			destinatariosObj.add(p);
		}
		//implementar observaciones
		
		//ingreso resolucion
		if (resolucion == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_no_valido"));
		}
		
		cl.exe.exedoc.entity.Resolucion documento = new cl.exe.exedoc.entity.Resolucion();
		if (resolucion.getAutor() == null) {
			documento.setAutor(emisorObj);
		} else {
			query = em.createNamedQuery("Persona.findByUsuarioVigente");
			query.setParameter("usuario", resolucion.getAutor());
			query.setParameter("vigente", true);
			result = (List<Persona>)query.getResultList();
			if (result == null || result.size() != 1) {
				throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_autor_no_valido") + " [" + resolucion.getAutor() + "]");
			}
			documento.setAutor(result.get(0));
		}
		
		if (resolucion.getTipoRazon() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_resolucion_tipo_razon_no_valido"));
		}
		if (resolucion.getTipoRazon().equals(TipoRazon.EXENTO)) {
			documento.setTipo(new cl.exe.exedoc.entity.TipoRazon(cl.exe.exedoc.entity.TipoRazon.EXENTO));
		} else if (resolucion.getTipoRazon().equals(TipoRazon.EXENTO_CON_REGISTRO)) {
			documento.setTipo(new cl.exe.exedoc.entity.TipoRazon(cl.exe.exedoc.entity.TipoRazon.EXENTO_CON_REGISTRO));
		} else if (resolucion.getTipoRazon().equals(TipoRazon.TOMA_RAZON)) {
			documento.setTipo(new cl.exe.exedoc.entity.TipoRazon(cl.exe.exedoc.entity.TipoRazon.TOMA_RAZON));
		} else {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_resolucion_tipo_razon_no_valido"));
		}
		
		if (resolucion.getMateria() == null) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
		}
		resolucion.setMateria(resolucion.getMateria().trim());
		if (resolucion.getMateria().length() == 0) {
			throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_materia_no_valido"));
		}
		
		List<Persona> distribucionDocumento = new ArrayList<Persona>();
		if (resolucion.getDistribucion() != null) {
			for (String d : resolucion.getDistribucion()) {
				query = em.createNamedQuery("Persona.findByUsuarioVigente");
				query.setParameter("usuario", d);
				query.setParameter("vigente", true);
				result = query.getResultList();
				if (result == null || result.size() != 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_distribucion_no_valido") + " [" + d + "]");
				}
				distribucionDocumento.add(result.get(0));
			}
		}
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (Persona item : distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(InyeccionCommon.getPersonaCargo(item));
			dd.setDestinatarioPersona(item);
			dd.setDocumento(documento);
			ddList.add(dd);
		}
		documento.setDistribucion(ddList);
		
		//revisores
		Integer orden = 1;
		List<RevisarEstructuradaDocumento> revisorDocumento = new ArrayList<RevisarEstructuradaDocumento>();
		if (resolucion.getRevisor() != null) {
			for (String r : resolucion.getRevisor()) {
				query = em.createNamedQuery("Persona.findByUsuarioVigente");
				query.setParameter("usuario", r);
				query.setParameter("vigente", true);
				result = query.getResultList();
				if (result == null || result.size() != 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_revisor_no_valido") + " [" + r + "]");
				}
				RevisarEstructuradaDocumento revisor = new RevisarEstructuradaDocumento();
				revisor.setDocumento(documento);
				revisor.setOrden(orden++);
				revisor.setPersona(result.get(0));
				revisorDocumento.add(revisor);
			}
		}
		documento.setRevisarEstructuradas(revisorDocumento);
		
		//visadores
		orden = 1;
		List<VisacionEstructuradaDocumento> visadorDocumento = new ArrayList<VisacionEstructuradaDocumento>();
		if (resolucion.getVisador() != null) {
			for (String v : resolucion.getVisador()) {
				query = em.createNamedQuery("Persona.findByUsuarioVigente");
				query.setParameter("usuario", v);
				query.setParameter("vigente", true);
				result = query.getResultList();
				if (result == null || result.size() != 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_visador_no_valido") + " [" + v + "]");
				}
				VisacionEstructuradaDocumento visador = new VisacionEstructuradaDocumento();
				visador.setDocumento(documento);
				visador.setOrden(orden++);
				visador.setPersona(result.get(0));
				visadorDocumento.add(visador);
			}
		}
		documento.setVisacionesEstructuradas(visadorDocumento);
		
		//firmantes
		orden = 1;
		List<FirmaEstructuradaDocumento> firmanteDocumento = new ArrayList<FirmaEstructuradaDocumento>();
		if (resolucion.getFirmante() != null) {
			for (String f : resolucion.getFirmante()) {
				query = em.createNamedQuery("Persona.findByUsuarioVigente");
				query.setParameter("usuario", f);
				query.setParameter("vigente", true);
				result = query.getResultList();
				if (result == null || result.size() != 1) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_documento_firmante_no_valido") + " [" + f + "]");
				}
				FirmaEstructuradaDocumento firmante = new FirmaEstructuradaDocumento();
				firmante.setDocumento(documento);
				firmante.setOrden(orden++);
				firmante.setPersona(result.get(0));
				firmanteDocumento.add(firmante);
			}
		}
		documento.setFirmasEstructuradas(firmanteDocumento);
		
		//insertar antecedentes
		List<cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico> listAdjuntos = new ArrayList<cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico>();
		if (resolucion.getAntecedente() != null) {
			for (Antecedente a : resolucion.getAntecedente()) {
				if (a == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_no_valido"));
				} else {
					response.setCodigoError(4);
					if (a.getMateria() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_materia_no_valido"));
					}
					if (a.getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_nombre_archivo_no_valido"));
					}
					if (a.getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_content_type_archivo_no_valido"));
					}
					if (a.getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_data_archivo_no_valido"));
					}
					Persona p;
					if (a.getAdjuntadoPor() == null) {
						p = documento.getAutor();
					} else {
						query = em.createNamedQuery("Persona.findByUsuarioVigente");
						query.setParameter("usuario", a.getAdjuntadoPor());
						query.setParameter("vigente", true);
						result = query.getResultList();
						if (result == null || result.size() != 1) {
							throw new InyeccionError(Messages.instance().get("ws_inyeccion_antecedente_adjuntado_por_no_valido") + " [" + a.getAdjuntadoPor() + "]");
						}
						p = result.get(0);
					}
					response.setCodigoError(null);
					
					cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico aade = new cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico();
					aade.setAdjuntadoPor(p);
					aade.setFecha(ahora);
					aade.setContentType(a.getContentType());
					aade.setNombreArchivo(a.getNombreArchivo());
					aade.setMateria(a.getMateria());
					aade.setArchivo(a.getData());
					aade.setDocumentoElectronico(documento);
					listAdjuntos.add(aade);
				}
			}
		}
		documento.setArchivosAdjuntos(listAdjuntos);
		//validar observaciones
		List<cl.exe.exedoc.entity.Observacion> listObservaciones = new ArrayList<cl.exe.exedoc.entity.Observacion>();
		if (observacion != null) {
			for (Observacion o : observacion) {
				response.setCodigoError(3);
				if (o == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_no_valido"));
				}
				cl.exe.exedoc.entity.Observacion obs = new cl.exe.exedoc.entity.Observacion();
				if (o.getTexto() == null) {
					throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_texto_no_valido"));
				}
				obs.setObservacion(o.getTexto());
				Persona p;
				if (o.getAdjuntadoPor() == null) {
					p = emisorObj;
				} else {
					query = em.createNamedQuery("Persona.findByUsuarioVigente");
					query.setParameter("usuario", o.getAdjuntadoPor());
					query.setParameter("vigente", true);
					result = query.getResultList();
					if (result == null || result.size() != 1) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_adjuntado_por_no_valido") + " [" + o.getAdjuntadoPor() + "]");
					}
					p = result.get(0);
				}
				obs.setAutor(p);
				obs.setFecha(ahora);
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				//obs.setExpediente(expediente); //mas adelante
				if (o.getArchivo() != null) {
					response.setCodigoError(3);
					if (o.getArchivo().getNombreArchivo() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_nombre_archivo_no_valido"));
					}
					if (o.getArchivo().getContentType() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_content_type_no_valido"));
					}
					if (o.getArchivo().getData() == null) {
						throw new InyeccionError(Messages.instance().get("ws_inyeccion_observacion_data_archivo_no_valido"));
					}
					cl.exe.exedoc.entity.ObservacionArchivo oa = new cl.exe.exedoc.entity.ObservacionArchivo();
					oa.setFecha(ahora);
					oa.setNombreArchivo(o.getArchivo().getNombreArchivo());
					oa.setContentType(o.getArchivo().getContentType());
					oa.setArchivo(o.getArchivo().getData());
					obs.setObservacionArchivo(oa);
				}
				response.setCodigoError(null);
				listObservaciones.add(obs);
			}
		}
		//insertar observaciones
		for (cl.exe.exedoc.entity.Observacion obs : listObservaciones) {
			em.persist(obs);
			ObservacionArchivo oa = obs.getObservacionArchivo();
			if (oa != null) {
					em.persist(oa);
				final String nombreArchivoObservacion = InyeccionCommon.getNombreArchivo(
						oa.getNombreArchivo(), oa.getId().toString());
				Documento d = new Documento();
				d.setAutor(obs.getAutor());
				d.setFormatoDocumento(new FormatoDocumento(0));
				String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
				obs.getObservacionArchivo().setCmsId(cmsId);
				em.merge(oa);
			}
			em.merge(obs);
		}
		
		
		documento.setFechaCreacion(ahora);
		documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.ORIGINAL));
		documento.setNumeroDocumento(resolucion.getNumero());
		documento.setFechaDocumentoOrigen(resolucion.getFecha());
		documento.setMateria(resolucion.getMateria());
		documento.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		documento.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.RESOLUCION));
		//documento.setCompletado(false);
		documento.setReservado(false);
		documento.setCiudad(resolucion.getCiudad());
		documento.setEmisor(InyeccionCommon.getPersonaCargo(documento.getAutor()));
		documento.setIdEmisor(documento.getAutor().getId());

		//vistos
		List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
		Vistos vistos = new Vistos();
		vistos.setNumero(1L);
		vistos.setDocumento(documento);
		vistos.setCuerpo(resolucion.getVistos());
		listaParrafos.add(vistos);
		documento.setVistos(listaParrafos);
		
		//considerandos
		listaParrafos = new ArrayList<Parrafo>();
		Considerandos considerandos = new Considerandos();
		considerandos.setNumero(1L);
		considerandos.setDocumento(documento);
		considerandos.setCuerpo(resolucion.getConsiderando());
		listaParrafos.add(considerandos);
		documento.setConsiderandos(listaParrafos);
		
		//resuelvo
		listaParrafos = new ArrayList<Parrafo>();
		Resuelvo resuelvo = new Resuelvo();
		resuelvo.setNumero(1L);
		resuelvo.setDocumento(documento);
		resuelvo.setCuerpo(resolucion.getResuelvo());
		listaParrafos.add(resuelvo);
		documento.setResuelvo(listaParrafos);
		
		//indicacion
		documento.setIndicaciones(resolucion.getIndicacion());
		
		cl.exe.exedoc.entity.Expediente expediente = new cl.exe.exedoc.entity.Expediente();
		expediente.setEmisor(emisorObj);
		expediente.setFechaIngreso(ahora);
		
		me.crearExpediente(expediente);
		me.agregarOriginalAExpediente(expediente, documento, true);
		for (cl.exe.exedoc.entity.Observacion o : listObservaciones) {
			o.setExpediente(expediente);
		}
		expediente.setObservaciones(listObservaciones);
		em.merge(expediente);
		
		for (Persona p : destinatariosObj) {
			me.agregarDestinatarioAExpediente(
					expediente, 
					emisorObj, 
					new cl.exe.exedoc.session.utils.Destinatario(p, p.getDestinatarioConCopia()), 
					new Date());
		}
		me.despacharExpediente(expediente);
        List<cl.exe.exedoc.entity.Expediente> expedientes = me.buscarExpedientesHijos(expediente);
        for (cl.exe.exedoc.entity.Expediente e : expedientes) {
            me.despacharExpediente(e);
        }
        response.setNumero(expediente.getNumeroExpediente());
        response.setCodigoError(0);
        response.setMensaje("OK");
        response.setIdDocumento(documento.getId());
		return response;
	}
}
