package cl.exe.exedoc.util;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.faces.FacesMessages;

@Name("datesValidator")
@BypassInterceptors
@Validator
public class DatesValidator implements javax.faces.validator.Validator {

	private static final String EL_PLAZO_NO_PUEDE_SER_MENOR = 
			"El plazo no puede ser menor a la fecha de creación del documento";

	public void validate(final FacesContext context, final UIComponent component,
			final Object value) throws ValidatorException {
		final String fechaInicioComponent = (String) component.getAttributes().get(
				"cierre");
		final UIInput fechaInicioInput = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent);

		if (fechaInicioInput.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Apertura Técnica debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSummary("La Fecha de Apertura Técnica debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
	}

	public void validate2(final FacesContext context, final UIComponent component,
			final Object value) throws ValidatorException {
		final String fechaInicioComponent = (String) component.getAttributes().get(
				"aperturaTecnica");
		final String fechaInicioComponent2 = (String) component.getAttributes().get(
				"cierre");
		final UIInput fechaInicioInput = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent);
		final UIInput fechaInicioInput2 = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent2);

		if (fechaInicioInput.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Inicio de Preguntas debe ser posterior "
						+ "a la Fecha de Apertura Técnica");
				message.setSummary("La Fecha de Inicio de Preguntas debe ser posterior "
						+ "a la Fecha de Apertura Técnica");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
		if (fechaInicioInput2.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput2.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Inicio de Preguntas debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSummary("La Fecha de Inicio de Preguntas debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
	}

	public void validate3(final FacesContext context, final UIComponent component,
			final Object value) throws ValidatorException {
		final String fechaInicioComponent = (String) component.getAttributes().get(
				"inicioPreguntas");
		final String fechaInicioComponent2 = (String) component.getAttributes().get(
				"aperturaTecnica");
		final String fechaInicioComponent3 = (String) component.getAttributes().get(
				"cierre");
		final UIInput fechaInicioInput = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent);
		final UIInput fechaInicioInput2 = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent2);
		final UIInput fechaInicioInput3 = (UIInput) context.getViewRoot()
				.findComponent(fechaInicioComponent3);

		if (fechaInicioInput.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Inicio de Preguntas");
				message.setSummary("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Inicio de Preguntas");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
		if (fechaInicioInput2.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput2.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Apertura Técnica");
				message.setSummary("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Apertura Técnica");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
		if (fechaInicioInput3.getValue() != null) {
			final Date fechaInicio = (Date) fechaInicioInput3.getValue();
			final Date fechaTermino = (Date) value;

			if (fechaInicio.after(fechaTermino)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSummary("La Fecha de Fin de Preguntas debe ser posterior "
						+ "a la Fecha de Cierre de Recepción de Ofertas");
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
	}

	/**
	 * @param context {@link FacesContext}
	 * @param component {@link UIComponent}
	 * @param value {@link Object}
	 */
	@SuppressWarnings("deprecation")
	public void validateFechaDocumentoContraPlazo(final FacesContext context,
			final UIComponent component, final Object value) {
		
		final String fechaDocumentoComponent = component.getAttributes()
				.get("fechaCreacion").toString();
		final UIInput fechaDocumentoInput = (UIInput) context.getViewRoot()
				.findComponent(fechaDocumentoComponent);

		if (fechaDocumentoInput.getValue() != null) {
			final Date fechaDocumento = (Date) fechaDocumentoInput.getValue();
			final Date plazo = (Date) value;

			if (plazo.before(fechaDocumento)) {
				final FacesMessage message = new FacesMessage();
				message.setDetail(EL_PLAZO_NO_PUEDE_SER_MENOR);
				message.setSummary(EL_PLAZO_NO_PUEDE_SER_MENOR);
				message.setSeverity(FacesMessage.SEVERITY_ERROR);

				FacesMessages.instance().add(message);
				throw new ValidatorException(message);
			}
		}
	}
}
