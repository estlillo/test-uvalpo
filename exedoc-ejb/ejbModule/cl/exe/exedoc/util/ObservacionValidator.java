package cl.exe.exedoc.util;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("observacionValidator")
@BypassInterceptors
@Validator
public class ObservacionValidator implements javax.faces.validator.Validator {

	public static final int LARGO_MAXIMO = 255;

	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String valor = value.toString();

		if (valor != null && !valor.trim().isEmpty() && valor.trim().length() >= LARGO_MAXIMO) {
			String detail = "No puede ingresar más de " + LARGO_MAXIMO + " carácteres";
			String summary = "Observación muy extensa";
			throw new ValidatorException(makeMessage(detail, summary));
		}
	}

	private FacesMessage makeMessage(String detail, String summary) {
		if (summary == null) {
			summary = detail;
		}
		FacesMessage message = new FacesMessage();
		message.setDetail(detail);
		message.setSummary(summary);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		return message;
	}

}
