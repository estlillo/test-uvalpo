package cl.exe.exedoc.util.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.jboss.seam.annotations.faces.Validator;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.faces.FacesMessages;

@Name("campoMaxLargoValidator")
@BypassInterceptors
@Validator
public class CampoMaxLargoValidator implements javax.faces.validator.Validator {

	public static final int LARGO_MAXIMO = 255;

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		String valor = value.toString();
		int largo = valor.trim().length();
		if (valor != null && !valor.trim().isEmpty() && largo >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres.", LARGO_MAXIMO);
			throw new ValidatorException(makeMessage(detalle, ""));
		}
	}

	/**
	 * @param detail {@link String}
	 * @param summary {@link String}
	 * @return {@link FacesMessages}
	 */
	private FacesMessage makeMessage(String detail, String summary) {
		if (summary == null) {
			summary = detail;
		}
		FacesMessage message = new FacesMessage();
		message.setDetail(detail);
		message.setSummary(summary);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		return message;
	}

}
