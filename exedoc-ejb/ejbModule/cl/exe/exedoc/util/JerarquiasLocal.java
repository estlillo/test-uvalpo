package cl.exe.exedoc.util;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;

/**
 * @author Administrator
 */
@Local
public interface JerarquiasLocal {

	/**
	 * INICIO -1.
	 */
	Long INICIO = new Long(-1);

	/**
	 * TEXTO_INICIAL <<Seleccionar>>.
	 */
	String TEXTO_INICIAL = "<<Seleccionar>>";

	/**
	 * TODOS -2.
	 */
	Long TODOS = new Long(-2);

	/**
	 * TEXTO_TODOS "<<Todos>>".
	 */
	String TEXTO_TODOS = "<<Todos>>";

	/**
	 * CUALQUIERA -4.
	 */
	Long CUALQUIERA = new Long(-4);
	
	/**
	 * NO TIENE PERSONA -4.
	 */
	Long NOTIENEPERSONA = new Long(-1);
	
	/**
	 * TEXTO_CUALQUIERA "<<El Primero>>".
	 */
	String TEXTO_CUALQUIERA = "<<El Primero>>";
	
	/**
	 * VISUALIZAR = "Visualizar Documento.".
	 */
	String VISUALIZAR = "Visualizar Documento";
	
	/**
	 * VER_ARCHIVO = "Descargar Archivo.".
	 */
	String DESCARGAR_ARCHIVO = "Descargar Archivo";
	
	String VER_ARCHIVO_ORIGINAL = "Ver Archivo Original"; 
	/**
	 * @param textoInicial {@link String}
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getOrganizaciones(final String textoInicial);

	/**
	 * @param textoInicial String
	 * @return List<SelectItem>
	 */
	List<SelectItem> getDivisiones(String textoInicial);

	/**
	 * Metodo que crea lista de divisiones, segun su organizacion.
	 * 
	 * @param textoInicial {@link String}
	 * @param idOrganizacion {@link Long}
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getDivisiones(final String textoInicial, final Long idOrganizacion);

	/**
	 * @param textoInicial String
	 * @param idDivision Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getDepartamentos(String textoInicial, Long idDivision);

	/**
	 * @param textoInicial String
	 * @return List<SelectItem>
	 */
	List<SelectItem> getUnidadesOrganzacionales(String textoInicial);

	/**
	 * @param textoInicial String
	 * @param idDepartamento Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getUnidadesOrganzacionales(String textoInicial, Long idDepartamento);

	/**
	 * @param textoInicial String
	 * @param idUnidad Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getCargos(String textoInicial, Long idUnidad);

	/**
	 * @param textoInicial String
	 * @param idInstitucion Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getPersonaExterna(String textoInicial, Long idInstitucion);

	/**
	 * @param textoInicial String
	 * @param idCargo Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getPersonas(String textoInicial, Long idCargo);

	/**
	 * @param textoInicial String
	 * @param idCargo Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getPersonasReasignar(String textoInicial, Long idCargo);

	/**
	 * @param id Long
	 * @return Persona
	 */
	Persona getPersona(Long id);

	/**
	 * @param idDivision Long
	 * @return Long
	 */
	Long getIdUnidadVirtualDivision(Long idDivision);

	/**
	 * @param textoInicial String
	 * @param idDivision Long
	 * @return List<SelectItem>
	 */
	List<SelectItem> getIdUnidadesDepartamento(String textoInicial, Long idDivision);

	/**
	 * @param idDivision Long
	 * @param idDepartamento Long
	 * @return Long
	 */
	Long getIdUnidadVirtualDepartamento(Long idDivision, Long idDepartamento);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getOpciones();

	/**
	 * @param subordinado Persona
	 * @return List<Persona>
	 */
	List<Persona> getJefe(Persona subordinado);

	/**
	 * @param subordinado Persona
	 * @param textoInicial String
	 * @return List<SelectItem>
	 */
	List<SelectItem> getJefe(Persona subordinado, String textoInicial);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean esJefe(Persona persona);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean isJefeUnidad(Persona persona);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean isJefeDepartamento(Persona persona);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean isJefeDivision(Persona persona);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean isSubsecretario(Persona persona);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean esJefe(Long idPersona);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean isJefeUnidad(Long idPersona);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean isJefeDepartamento(Long idPersona);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean isJefeDivision(Long idPersona);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean isSubsecretario(Long idPersona);

	/**
	 * @param textoInicial String
	 * @return List<SelectItem>
	 */
	List<SelectItem> getDepartamentos(String textoInicial);

	/**
	 * @param textoInicial String
	 * @return List<SelectItem>
	 */
	List<SelectItem> getUnidades(String textoInicial);

	/**
	 * @param idPersona Long
	 * @return boolean
	 */
	boolean isJefeGabinete(Long idPersona);

	/**
	 * @param persona Persona
	 * @return boolean
	 */
	boolean isJefeGabinete(Persona persona);

	/**
	 * @param idDepartamento Long
	 * @return Long
	 */
	Long getIdUnidadVirtual(Long idDepartamento);

	/**
	 * @param subordinado Persona
	 * @param emisor Persona
	 * @return boolean
	 */
	boolean revisaJerarquia(Persona subordinado, Persona emisor);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getFiscalias();

	List<Persona> obtenerPersonasPorRol(Rol rol);

	List<Persona> buscarJefeRRHH();

	List<SelectItem> getAllDepartamentos(String textoInicial);

}
