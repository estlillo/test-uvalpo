package cl.exe.exedoc.util;

import java.util.Date;
import java.util.List;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.Persona;

public class DatosSolicitud {
	
	private Persona destinatario;
	private Date fechaIngreso;
	private String tipoSolicitud;
	private String estadoSolicitud;
	private Integer diasSolicitados;
	private Integer medioDiasSolicitados;
	private Date fechaInicioDias;
	private Date fechaInicioMedioDias;
	private Date fechaTerminoDias;
	private Date fechaTerminoMedioDias;
	private List<DetalleDias> listDetalleDias;
	private DetalleDiasVacaciones detalleDiasVacaciones;
	private Double diasPendientes;
	private String calidadJuridica;
	private String grado;
	private byte[] resolucionPDF;
	
	public String getCalidadJuridica() {
		return calidadJuridica;
	}
	public void setCalidadJuridica(String calidadJuridica) {
		this.calidadJuridica = calidadJuridica;
	}
	public String getGrado() {
		return grado;
	}
	public void setGrado(String grado) {
		this.grado = grado;
	}
	public Double getDiasPendientes() {
		return diasPendientes;
	}
	public void setDiasPendientes(Double diasPendientes) {
		this.diasPendientes = diasPendientes;
	}
	public String getTipoSolicitud() {
		return tipoSolicitud;
	}
	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}
	public String getEstadoSolicitud() {
		return estadoSolicitud;
	}
	public void setEstadoSolicitud(String estadoSolicitud) {
		this.estadoSolicitud = estadoSolicitud;
	}
	public Integer getDiasSolicitados() {
		return diasSolicitados;
	}
	public void setDiasSolicitados(Integer diasSolicitados) {
		this.diasSolicitados = diasSolicitados;
	}
	public Integer getMedioDiasSolicitados() {
		return medioDiasSolicitados;
	}
	public void setMedioDiasSolicitados(Integer medioDiasSolicitados) {
		this.medioDiasSolicitados = medioDiasSolicitados;
	}
	public Date getFechaInicioDias() {
		return fechaInicioDias;
	}
	public void setFechaInicioDias(Date fechaInicioDias) {
		this.fechaInicioDias = fechaInicioDias;
	}
	public Date getFechaInicioMedioDias() {
		return fechaInicioMedioDias;
	}
	public void setFechaInicioMedioDias(Date fechaInicioMedioDias) {
		this.fechaInicioMedioDias = fechaInicioMedioDias;
	}
	public Date getFechaTerminoDias() {
		return fechaTerminoDias;
	}
	public void setFechaTerminoDias(Date fechaTerminoDias) {
		this.fechaTerminoDias = fechaTerminoDias;
	}
	public Date getFechaTerminoMedioDias() {
		return fechaTerminoMedioDias;
	}
	public void setFechaTerminoMedioDias(Date fechaTerminoMedioDias) {
		this.fechaTerminoMedioDias = fechaTerminoMedioDias;
	}
	public Persona getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(Persona destinatario) {
		this.destinatario = destinatario;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public List<DetalleDias> getListDetalleDias() {
		return listDetalleDias;
	}
	public void setListDetalleDias(List<DetalleDias> listDetalleDias) {
		this.listDetalleDias = listDetalleDias;
	}
	public DetalleDiasVacaciones getDetalleDiasVacaciones() {
		return detalleDiasVacaciones;
	}
	public void setDetalleDiasVacaciones(DetalleDiasVacaciones detalleDiasVacaciones) {
		this.detalleDiasVacaciones = detalleDiasVacaciones;
	}
	public byte[] getResolucionPDF() {
		return resolucionPDF;
	}
	public void setResolucionPDF(byte[] resolucionPDF) {
		this.resolucionPDF = resolucionPDF;
	}
	
}
