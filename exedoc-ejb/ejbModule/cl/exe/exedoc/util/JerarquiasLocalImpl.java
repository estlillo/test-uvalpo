package cl.exe.exedoc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.UnidadOrganizacional;

/**
 * @author Administrator Implementacion de {@link JerarquiasLocal}
 */
@Stateless
@Name("jerarquias")
public class JerarquiasLocalImpl implements JerarquiasLocal {

	private static final String SELECT_PERSONA = "select p from Persona p where p.cargo.id= ?";
	private static final String JEFE = "JEFE";
	private static final String EMPTY = " ";
	private static final String CARGO_FIND_BY_ID = "Cargo.findById";
	private static final String SELECT_U_ID_FROM_UNIDAD_ORGANIZACIONAL_U = "SELECT u.id FROM UnidadOrganizacional u ";
	private static final String ID = "id";

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	private Persona usuario;

	/**
	 * Constructor.
	 */
	public JerarquiasLocalImpl() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getOrganizaciones(final String textoInicial) {
		final List<SelectItem> organizaciones = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			organizaciones.add(new SelectItem(INICIO, textoInicial));
		}

		final List<Organizacion> lista = em.createNamedQuery(
				"Organizacion.findByAll").getResultList();

		if (lista != null) {
			for (Organizacion d : lista) {
				organizaciones
						.add(new SelectItem(d.getId(), d.getDescripcion()));
			}
		}
		Collections.sort(organizaciones, ComparatorUtils.compararItems);
		return organizaciones;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getDivisiones(final String textoInicial) {
		final List<SelectItem> divisiones = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			divisiones.add(new SelectItem(INICIO, textoInicial));
		}

		final List<Division> lista = em.createNamedQuery("Division.findByAll")
				.getResultList();

		if (lista != null) {
			for (Division d : lista) {
				divisiones.add(new SelectItem(d.getId(), d.getDescripcion()));
			}
		}
		Collections.sort(divisiones, ComparatorUtils.compararItems);
		return divisiones;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getDivisiones(final String textoInicial,
			final Long idOrganizacion) {
		final List<SelectItem> divisiones = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			divisiones.add(new SelectItem(INICIO, textoInicial));
		}
		final Query query = em.createNamedQuery("Organizacion.findById");
		query.setParameter(ID, idOrganizacion);
		final List<Organizacion> organizaciones = query.getResultList();
		if (organizaciones != null && organizaciones.size() == 1) {
			final Organizacion organizacion = organizaciones.get(0);
			final List<Division> lista = organizacion.getDivisiones();
			for (Division d : lista) {
				divisiones.add(new SelectItem(d.getId(), d.getDescripcion()));
			}
		}
		Collections.sort(divisiones, ComparatorUtils.compararItems);
		return divisiones;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getDepartamentos(final String textoInicial,
			final Long idDivision) {
		final List<SelectItem> departamentos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			departamentos.add(new SelectItem(INICIO, textoInicial));
		}
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, idDivision);
		final List<Division> divisiones = query.getResultList();
		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			final List<Departamento> lista = division.getDepartamentos();
			for (Departamento d : lista) {
				if (!d.getVirtual()) {
					departamentos.add(new SelectItem(d.getId(), d
							.getDescripcion()));
				}
			}
		}
		Collections.sort(departamentos, ComparatorUtils.compararItems);
		return departamentos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getDepartamentos(final String textoInicial) {
		final List<SelectItem> departamentos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			departamentos.add(new SelectItem(INICIO, textoInicial));
		}
		final Query query = em
				.createQuery("SELECT d FROM Departamento d WHERE d.virtual = false ");
		
		final List<Departamento> dptos = query.getResultList();
		for (Departamento d : dptos) {
			departamentos.add(new SelectItem(d.getId(), d.getDescripcion()));
		}
		Collections.sort(departamentos, ComparatorUtils.compararItems);
		return departamentos;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<SelectItem> getFiscalias() {
		final List<SelectItem> fiscalias = new ArrayList<SelectItem>();
		
		fiscalias.add(new SelectItem(INICIO, "<< Seleccionar >>"));
		fiscalias.add(new SelectItem(INICIO - 1, "Todos"));
		
		final Query query = em
				.createQuery("SELECT d FROM Departamento d WHERE d.virtual = false ");
		final List<Departamento> dptos = query.getResultList();
		for (Departamento d : dptos) {
			fiscalias.add(new SelectItem(d.getId(), d.getDescripcion()));
		}
		Collections.sort(fiscalias, ComparatorUtils.compararItems);
		return fiscalias;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getUnidadesOrganzacionales(final String textoInicial) {
		final List<SelectItem> unidades = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			unidades.add(new SelectItem(INICIO, textoInicial));
		}

		final Query query = em
				.createQuery("select u from UnidadOrganizacional u where u.departamento is null");
		final List<UnidadOrganizacional> lista = query.getResultList();

		if (lista != null) {
			for (UnidadOrganizacional d : lista) {
				unidades.add(new SelectItem(d.getId(), d.getDescripcion()));
			}
		}
		Collections.sort(unidades, ComparatorUtils.compararItems);
		return unidades;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getUnidades(final String textoInicial) {
		final List<SelectItem> unidades = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			unidades.add(new SelectItem(INICIO, textoInicial));
		}
		final Query query = em
				.createQuery("SELECT u FROM UnidadOrganizacional u WHERE u.virtual = false");
		final List<UnidadOrganizacional> unidadList = query.getResultList();
		for (UnidadOrganizacional u : unidadList) {
			unidades.add(new SelectItem(u.getId(), u.getDescripcion()));
		}
		Collections.sort(unidades, ComparatorUtils.compararItems);
		return unidades;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getUnidadesOrganzacionales(
			final String textoInicial, final Long idDepartamento) {
		final List<SelectItem> unidadesOrganizacionales = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			unidadesOrganizacionales.add(new SelectItem(INICIO, textoInicial));
		}

		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, idDepartamento);
		final List<Departamento> departamentos = query.getResultList();
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento departamento = departamentos.get(0);
			final List<UnidadOrganizacional> lista = departamento.getUnidades();
			if (lista != null) {
				for (UnidadOrganizacional u : lista) {
					if (!u.getVirtual()) {
						unidadesOrganizacionales.add(new SelectItem(u.getId(),
								u.getDescripcion()));
					}
				}
			}
		}
		Collections.sort(unidadesOrganizacionales, ComparatorUtils.compararItems);
		return unidadesOrganizacionales;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getIdUnidadVirtualDivision(final Long idDivision) {
		final StringBuilder sb = new StringBuilder(
				SELECT_U_ID_FROM_UNIDAD_ORGANIZACIONAL_U);
		sb.append("WHERE  u.virtual = ? and u.departamento.virtual = ? and u.departamento.division.id = ?");
		final Query query = em.createQuery(sb.toString());
		query.setParameter(1, true);
		query.setParameter(2, true);
		query.setParameter(3, idDivision);
		final List<Long> ids = query.getResultList();
		if (ids != null && ids.size() == 1) {
			return ids.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getIdUnidadesDepartamento(
			final String textoInicial, final Long idDivision) {
		final StringBuilder sb = new StringBuilder(
				"SELECT u FROM UnidadOrganizacional u ");
		sb.append("WHERE  u.virtual = ? and u.departamento.division.id = ?");
		final Query query = em.createQuery(sb.toString());
		query.setParameter(1, false);
		query.setParameter(2, idDivision);
		final List<UnidadOrganizacional> lista = query.getResultList();

		final List<SelectItem> unidadesOrganizacionales = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			unidadesOrganizacionales.add(new SelectItem(INICIO, textoInicial));
		}
		if (lista != null) {
			for (UnidadOrganizacional u : lista) {
				if (u.getDepartamento().getVirtual() && !u.getVirtual()) {
					unidadesOrganizacionales.add(new SelectItem(u.getId(), u
							.getDescripcion()));
				}
			}
		}
		Collections.sort(unidadesOrganizacionales, ComparatorUtils.compararItems);
		return unidadesOrganizacionales;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getIdUnidadVirtualDepartamento(final Long idDivision,
			final Long idDepartamento) {
		final StringBuilder sb = new StringBuilder(
				SELECT_U_ID_FROM_UNIDAD_ORGANIZACIONAL_U);
		sb.append("WHERE  u.virtual = ? and u.departamento.id = ? and u.departamento.division.id = ?");
		final Query query = em.createQuery(sb.toString());
		query.setParameter(1, true);
		query.setParameter(2, idDepartamento);
		query.setParameter(3, idDivision);
		final List<Long> ids = query.getResultList();
		if (ids != null && ids.size() == 1) {
			return ids.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getIdUnidadVirtual(final Long idDepartamento) {
		final StringBuilder sb = new StringBuilder(
				SELECT_U_ID_FROM_UNIDAD_ORGANIZACIONAL_U);
		sb.append("WHERE  u.virtual = ? and u.departamento.id = ?");
		final Query query = em.createQuery(sb.toString());
		query.setParameter(1, true);
		query.setParameter(2, idDepartamento);
		final List<Long> ids = query.getResultList();
		if (ids != null && ids.size() == 1) {
			return ids.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getCargos(final String textoInicial,
			final Long idUnidad) {
		final List<SelectItem> cargos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			cargos.add(new SelectItem(INICIO, textoInicial));
		}

		final Query query = em
				.createNamedQuery("UnidadOrganizacional.findById");
		query.setParameter(ID, idUnidad);
		final List<UnidadOrganizacional> unidades = query.getResultList();
		if (unidades != null && unidades.size() == 1) {
			final UnidadOrganizacional unidadOrganizacional = unidades.get(0);
			final List<Cargo> lista = unidadOrganizacional.getCargos();

			if (lista != null) {
				for (Cargo c : lista) {
					cargos.add(new SelectItem(c.getId(), c.getDescripcion()));
				}
			}
		}
		Collections.sort(cargos, ComparatorUtils.compararItems);
		return cargos;
	}

	@Override
	public List<SelectItem> getPersonaExterna(final String textoInicial,
			final Long idInstitucion) {
		final List<SelectItem> personas = new ArrayList<SelectItem>();

		// Query query = em.createNamedQuery("Institucion.findById");
		// query.setParameter("id", idInstitucion.intValue());
		// List<Institucion> instituciones = query.getResultList();

		// if (instituciones != null && instituciones.size() == 1) {
		// Institucion institucion = instituciones.get(0);
		// Query queryExterno =
		// em.createQuery("select p from Persona p where p.externo= ? and p.institucion.id= ?");
		// queryExterno.setParameter(1, true);
		// queryExterno.setParameter(2, institucion.getId());
		//
		// List<Persona> personasExternos = queryExterno.getResultList();
		//
		// if (personasExternos != null) {
		// if (personasExternos.size() != 1) {
		// if (textoInicial != null) {
		// personas.add(new SelectItem(INICIO, textoInicial));
		// }
		// if (personasExternos.size() > 1) {
		// personas.addAll(getOpciones());
		// }
		// }
		//
		// for (Persona p : personasExternos) {
		// if (p.getVigente()) {
		// personas.add(new SelectItem(p.getId(), p.getNombres() + " " +
		// p.getApellidoPaterno()));
		// }
		// }
		// }
		// } else {
		// personas.add(new SelectItem(INICIO, textoInicial));
		// }
		return personas;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getPersonas(final String textoInicial,
			final Long idCargo) {
		final List<SelectItem> personas = new ArrayList<SelectItem>();

		final Query query = em.createNamedQuery(CARGO_FIND_BY_ID);
		query.setParameter(ID, idCargo);
		final List<Cargo> cargos = query.getResultList();
		if (cargos != null && cargos.size() == 1) {
			final Cargo cargo = cargos.get(0);
			final List<Persona> lista = cargo.getPersonas();

			if (lista != null) {
				if (lista.size() != 1) {
					if (textoInicial != null) {
						personas.add(new SelectItem(INICIO, textoInicial));
					}
					if (lista.size() > 1) {
						personas.addAll(this.getOpciones());
					}
				}

				for (Persona p : lista) {
					try {
						if (p.getVigente()) {
							personas.add(new SelectItem(p.getId(), p
									.getNombres()
									+ EMPTY
									+ p.getApellidoPaterno()));
						}
					} catch (NullPointerException e) { }
				}
			}
		}
		Collections.sort(personas, ComparatorUtils.compararItems);
		return personas;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getPersonasReasignar(final String textoInicial,
			final Long idCargo) {
		final List<SelectItem> personas = new ArrayList<SelectItem>();

		final Query query = em.createNamedQuery(CARGO_FIND_BY_ID);
		query.setParameter(ID, idCargo);
		final List<Cargo> cargos = query.getResultList();
		if (cargos != null && cargos.size() == 1) {
			final Cargo cargo = cargos.get(0);
			final List<Persona> lista = cargo.getPersonas();

			if (lista != null) {
				if (lista.size() != 1) {
					if (textoInicial != null) {
						personas.add(new SelectItem(INICIO, textoInicial));
					}
					if (lista.size() > 1) {
						personas.addAll(this.getOpciones());
					}
				}

				for (Persona p : lista) {
					if (p.getVigente()) {
						personas.add(new SelectItem(p.getId(), p.getNombres()
								+ EMPTY + p.getApellidoPaterno()));
					} else if (usuario.getRoles().contains(Rol.ADMINISTRADOR)) {
						personas.add(new SelectItem(p.getId(), p.getNombres()
								+ EMPTY + p.getApellidoPaterno()));
					}
				}
			}
		}
		Collections.sort(personas, ComparatorUtils.compararItems);
		return personas;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Persona getPersona(final Long id) {

		final StringBuilder sql = new StringBuilder();
		sql.append("select p.nombres, p.apellidoPaterno, p.apellidoMaterno, ");
		sql.append("c.descripcion as desc_cargo, u.descripcion as desc_uni ");
		sql.append(" from Persona p, Cargo c, Unidad u ");
		sql.append(" where p.cargo.id= c.id and c.unidadOrganizacional.id= u.id ");
		sql.append(" and p.id = ").append(id);
		final Query query = em.createQuery(sql.toString());
		final List<Object[]> arrayObject = query.getResultList();
		Persona persona = null;

		if (arrayObject != null && arrayObject.size() == 1) {
			persona = new Persona();
			final Cargo cargo = new Cargo();
			final UnidadOrganizacional unidad = new UnidadOrganizacional();
			final Object[] object = arrayObject.get(0);
			persona.setNombres((String) object[0]);
			persona.setApellidoPaterno((String) object[1]);
			persona.setApellidoMaterno((String) object[2]);
			cargo.setDescripcion((String) object[3]);
			unidad.setDescripcion((String) object[4]);
			cargo.setUnidadOrganizacional(unidad);
			persona.setCargo(cargo);
		}
		return persona;
	}

	@Override
	public List<SelectItem> getOpciones() {
		final List<SelectItem> opciones = new ArrayList<SelectItem>();
		return opciones;
	}

	@Override
	public List<SelectItem> getJefe(final Persona subordinado,
			final String textoInicial) {
		final List<Persona> jefes = this.getJefe(subordinado);
		final List<SelectItem> listaJefes = new ArrayList<SelectItem>();
		if (!textoInicial.isEmpty()) {
			listaJefes.add(new SelectItem(INICIO, textoInicial));
		}
		for (Persona p : jefes) {
			listaJefes.add(new SelectItem(p.getId(), p.getNombreApellido()));
		}
		return listaJefes;
	}

	@Override
	public List<Persona> getJefe(final Persona subordinado) {
		final List<Persona> jefes = new ArrayList<Persona>();
		this.obtenerJefatura(subordinado, jefes);

		return jefes;
	}

	@Override
	public boolean esJefe(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.esJefe(p);
	}

	@Override
	public boolean esJefe(final Persona persona) {

		final boolean isJefeUnidad = this.isJefeUnidad(persona);
		final boolean isJefeDepartamento = this.isJefeDepartamento(persona);
		final boolean isJefeDivision = this.isJefeDivision(persona);
		final boolean isSubsecretario = this.isSubsecretario(persona);
		final boolean isJefeGabinete = this.isJefeGabinete(persona);

		final boolean isJefe = (isJefeUnidad || isJefeDepartamento
				|| isJefeDivision || isSubsecretario || isJefeGabinete);
		return isJefe;
	}

	@Override
	public boolean isJefeUnidad(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.isJefeUnidad(p);
	}

	@Override
	public boolean isJefeUnidad(final Persona persona) {
		final Cargo cargo = persona.getCargo();
		final boolean isJefeUnidad = (cargo.getDescripcion() != null
				&& cargo.getDescripcion().toUpperCase().startsWith(JEFE) && cargo
				.getDescripcion().toUpperCase().contains("UNIDAD")) ? true
				: false;
		return isJefeUnidad;
	}

	@Override
	public boolean isJefeDepartamento(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.isJefeDepartamento(p);
	}

	@Override
	public boolean isJefeDepartamento(final Persona persona) {
		final Cargo cargo = persona.getCargo();
		final boolean isJefeDepartamento = (cargo.getDescripcion() != null
				&& cargo.getDescripcion().toUpperCase().startsWith(JEFE) && cargo
				.getDescripcion().toUpperCase().contains("DEPARTAMENTO")) ? true
				: false;
		return isJefeDepartamento;
	}

	@Override
	public boolean isJefeDivision(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.isJefeDivision(p);
	}

	@Override
	public boolean isJefeDivision(final Persona persona) {
		final Cargo cargo = persona.getCargo();
		final boolean isJefeDivision = (cargo.getDescripcion() != null
				&& cargo.getDescripcion().toUpperCase().startsWith(JEFE) && cargo
				.getDescripcion().toUpperCase().contains("DIVISION")) ? true
				: false;
		return isJefeDivision;
	}

	@Override
	public boolean isJefeGabinete(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.isJefeGabinete(p);
	}

	@Override
	public boolean isJefeGabinete(final Persona persona) {
		final Cargo cargo = persona.getCargo();
		final boolean isJefeGabinete = (cargo.getDescripcion() != null
				&& cargo.getDescripcion().toUpperCase().startsWith(JEFE) && cargo
				.getDescripcion().toUpperCase().contains("GABINETE")) ? true
				: false;
		return isJefeGabinete;
	}

	@Override
	public boolean isSubsecretario(final Long idPersona) {
		final Persona p = em.find(Persona.class, idPersona);
		return this.isSubsecretario(p);
	}

	@Override
	public boolean isSubsecretario(final Persona persona) {
		final Cargo cargo = persona.getCargo();
		final boolean isSubsecretario = (cargo.getDescripcion() != null && cargo
				.getDescripcion().toUpperCase().startsWith("SUBSECRETARIO")) ? true
				: false;
		return isSubsecretario;
	}

	/**
	 * @param personaSolicitante
	 *            Persona
	 * @param jefes
	 *            List<Persona>
	 */
	private void obtenerJefatura(final Persona personaSolicitante,
			final List<Persona> jefes) {

		final Persona solicitante = em.find(Persona.class,
				personaSolicitante.getId());

		Division division;
		Departamento departamento;
		UnidadOrganizacional unidadOrganizacional;
		Cargo cargo;

		cargo = solicitante.getCargo();

		final boolean isJefeUnidad = this.isJefeUnidad(solicitante);
		final boolean isJefeDepartamento = this.isJefeDepartamento(solicitante);
		final boolean isJefeDivision = this.isJefeDivision(solicitante);
		final boolean isSubsecretario = this.isSubsecretario(solicitante);
		final boolean isJefeGabinete = this.isJefeGabinete(solicitante);

		final boolean isJefe = (isJefeUnidad || isJefeDepartamento
				|| isJefeDivision || isSubsecretario || isJefeGabinete);

		if (cargo != null) {
			unidadOrganizacional = cargo.getUnidadOrganizacional();
			departamento = unidadOrganizacional.getDepartamento();

			if (isJefeUnidad) {
				this.obtenerJefe(unidadOrganizacional, jefes);
			}
			if (isJefeDepartamento || !isJefe) {
				this.obtenerJefe(departamento, jefes, isJefe);
			}
			if (isJefeDivision || isJefeGabinete) {
				division = departamento.getDivision();
				this.obtenerJefe(division, jefes);
			}
		}
	}

	/**
	 * @param division
	 *            Division
	 * @param jefes
	 *            List<Persona>
	 */
	private void obtenerJefe(final Division division, final List<Persona> jefes) {

		if (division != null) {
			// Jefe División Adm. y Finanzas
			jefes.addAll(this.obtenerJefeDivisionAdmFinanzas());
		}

		if (jefes.isEmpty()) {
			jefes.addAll(this.obtenerJefeDivisionAdmFinanzas());
		}
	}

	/**
	 * @param unidad
	 *            UnidadOrganizacional
	 * @param jefes
	 *            List<Persona>
	 */
	private void obtenerJefe(final UnidadOrganizacional unidad,
			final List<Persona> jefes) {

		UnidadOrganizacional unidadObtenida = null;
		Departamento deptoObtenido = null;
		Division divisionObtenida = null;
		List<Cargo> listCargosObtenidos = null;

		deptoObtenido = unidad.getDepartamento();

		if (deptoObtenido.getVirtual() != null && deptoObtenido.getVirtual()) {
			divisionObtenida = deptoObtenido.getDivision();
			deptoObtenido = this.obtenerDepartamentoAsociado(divisionObtenida);
		}

		unidadObtenida = this.obtenerUnidadAsociada(deptoObtenido);

		listCargosObtenidos = this.obtenerCargosJefe(unidadObtenida);

		jefes.addAll(this.obtenerJefeAsociado(listCargosObtenidos));

		if (jefes.isEmpty()) {
			jefes.addAll(this.obtenerJefeDivisionAdmFinanzas());
		}
	}

	/**
	 * @param depto
	 *            Departamento
	 * @param jefes
	 *            List<Persona>
	 * @param isJefe
	 *            boolean
	 */
	private void obtenerJefe(final Departamento depto,
			final List<Persona> jefes, final boolean isJefe) {

		UnidadOrganizacional unidadObtenida = null;
		Departamento deptoObtenido = null;
		Division divisionObtenida = null;
		List<Cargo> listCargosObtenidos = null;
		if (isJefe) {
			if (depto.getVirtual() != null && depto.getVirtual()) {
				divisionObtenida = depto.getDivision();
				deptoObtenido = this.obtenerDepartamentoAsociado(divisionObtenida);
			}

			unidadObtenida = this.obtenerUnidadAsociada(deptoObtenido != null ? deptoObtenido
							: depto);

			listCargosObtenidos = this.obtenerCargosJefe(unidadObtenida);

			jefes.addAll(this.obtenerJefeAsociado(listCargosObtenidos));

			// Si alguna jerarquia no tiene jefe, va directo donde el jefe
			// de la division
			if (jefes.isEmpty()) {
				jefes.addAll(this.obtenerJefeDivisionUsuario());
			}
		} else {

			divisionObtenida = depto.getDivision();

			deptoObtenido = this.obtenerDepartamentoAsociado(divisionObtenida);

			unidadObtenida = this.obtenerUnidadAsociada(deptoObtenido);

			listCargosObtenidos = this.obtenerCargosJefe(unidadObtenida);

			jefes.addAll(this.obtenerJefeAsociado(listCargosObtenidos));
		}
		if (jefes.isEmpty()) {
			jefes.addAll(this.obtenerJefeDivisionAdmFinanzas());
		}
	}

	/**
	 * @return List<Persona>
	 */
	private List<Persona> obtenerJefeDivisionUsuario() {
		final Division division = usuario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getDivision();
		final Departamento dpto = this.obtenerDepartamentoAsociado(division);
		final UnidadOrganizacional unidad = this.obtenerUnidadAsociada(dpto);

		return this.obtenerJefeAsociado(this.obtenerCargosJefe(unidad));

	}

	/**
	 * @return List<Persona>
	 */
	@SuppressWarnings("unchecked")
	private List<Persona> obtenerJefeDivisionAdmFinanzas() {
		final long idCargoJefatura = 3;

		final String hql = SELECT_PERSONA;
		final Query query = em.createQuery(hql);
		query.setParameter(1, idCargoJefatura);
		final List<Persona> jefesDivisionAdmFinanzas = query.getResultList();
		return jefesDivisionAdmFinanzas;
	}

	/**
	 * @param divisionObtenida
	 *            Division
	 * @return Departamento
	 */
	@SuppressWarnings("unchecked")
	private Departamento obtenerDepartamentoAsociado(final Division divisionObtenida) {
		List<Departamento> listDepartamentoObtenidos = null;
		Departamento deptoObtenido = null;
		String hqlDepto = null;
		hqlDepto = "select dep from Departamento dep where dep.division.id= ?";

		if (divisionObtenida != null) {
			final Query queryDepto = em.createQuery(hqlDepto);
			queryDepto.setParameter(1, divisionObtenida.getId());
			listDepartamentoObtenidos = queryDepto.getResultList();

			for (Departamento d : listDepartamentoObtenidos) {
				if (d != null && d.getVirtual()) {
					if (d.getDescripcion().toUpperCase().contains("STAFF")) {
						deptoObtenido = d;
						break;
					}
				}
			}
		}
		return deptoObtenido;
	}

	/**
	 * @param deptoObtenido
	 *            Departamento
	 * @return UnidadOrganizacional
	 */
	@SuppressWarnings("unchecked")
	private UnidadOrganizacional obtenerUnidadAsociada(
			final Departamento deptoObtenido) {
		UnidadOrganizacional unidadObtenida = null;
		List<UnidadOrganizacional> listUnidadesObtenidas;

		String hqlUnidad = null;
		hqlUnidad = "select u from UnidadOrganizacional u where u.departamento.id= ?";

		if (deptoObtenido != null) {
			final Query queryUnidad = em.createQuery(hqlUnidad);
			queryUnidad.setParameter(1, deptoObtenido.getId());
			listUnidadesObtenidas = queryUnidad.getResultList();

			for (UnidadOrganizacional u : listUnidadesObtenidas) {
				if (u.getVirtual() != null && u.getVirtual()) {
					unidadObtenida = u;
					break;
				}
			}
		}
		return unidadObtenida;
	}

	/**
	 * @param unidadObtenida
	 *            UnidadOrganizacional
	 * @return List<Cargo>
	 */
	@SuppressWarnings("unchecked")
	private List<Cargo> obtenerCargosJefe(
			final UnidadOrganizacional unidadObtenida) {
		List<Cargo> listCargosObtenidos = null;
		String hqlCargo = null;
		hqlCargo = "select c from Cargo c where c.unidadOrganizacional.id= ?";

		if (unidadObtenida != null) {
			final Query queryCargo = em.createQuery(hqlCargo);
			queryCargo.setParameter(1, unidadObtenida.getId());
			final List<Cargo> listCargosAux = queryCargo.getResultList();

			for (Cargo c : listCargosAux) {
				if (c.getDescripcion().toUpperCase().startsWith(JEFE)) {
					if (listCargosObtenidos == null) {
						listCargosObtenidos = new ArrayList<Cargo>();
					}
					listCargosObtenidos.add(c);
				}
			}
		}
		return listCargosObtenidos;
	}

	/**
	 * @param listCargosObtenidos
	 *            List<Cargo>
	 * @return List<Persona>
	 */
	@SuppressWarnings("unchecked")
	private List<Persona> obtenerJefeAsociado(
			final List<Cargo> listCargosObtenidos) {
		final List<Persona> listPersonas = new ArrayList<Persona>();
		String hqlPersona = null;
		hqlPersona = SELECT_PERSONA;

		if (listCargosObtenidos != null && listCargosObtenidos.size() > 0) {
			for (Cargo cargoObtenido : listCargosObtenidos) {
				final Query queryPersona = em.createQuery(hqlPersona);
				queryPersona.setParameter(1, cargoObtenido.getId());
				final List<Persona> tmp = queryPersona.getResultList();
				if (tmp != null && tmp.size() != 0) {
					listPersonas.addAll(tmp);
				}
			}
		}
		return listPersonas;
	}

	/**
	 * @param subordinado
	 *            Persona
	 * @param emisor
	 *            Persona
	 * @return boolean
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean revisaJerarquia(final Persona subordinado,
			final Persona emisor) {

		final boolean isJefeUnidad = this.isJefeUnidad(emisor);
		final boolean isJefeDepartamento = this.isJefeDepartamento(emisor);
		final boolean isJefeDivision = this.isJefeDivision(emisor);
		final boolean isSubsecretario = this.isSubsecretario(emisor);
		final boolean isJefeGabinete = this.isJefeGabinete(emisor);

		final StringBuilder sb = new StringBuilder();

		if (isJefeUnidad) {
			sb.append("select u.id from Unidad u , Cargo c , Persona p where p.id = ");
			sb.append(subordinado.getId());
			sb.append(" and p.cargo.id = c.id and c.unidadOrganizacional.id = u.id and u.virtual is false ");
		} else if (isJefeDepartamento) {
			sb.append("select d.id from Departamento d, Unidad u , Cargo c , Persona p where p.id = ");
			sb.append(subordinado.getId());
			sb.append(" and p.cargo.id = c.id and c.unidadOrganizacional.id = u.id ");
			sb.append("and u.departamento.id = d.id and d.virtual is false ");
		} else if (isJefeDivision || isSubsecretario || isJefeGabinete) {
			sb.append("select di.id from  Division di, Departamento d, Unidad u , Cargo c , Persona p where p.id = ");
			sb.append(subordinado.getId());
			sb.append(" and p.cargo.id = c.id ");
			sb.append("and c.unidadOrganizacional.id = u.id ");
			sb.append("and u.departamento.id = d.id ");
			sb.append("and d.division.id = di.id ");
		}

		final Query query = em.createQuery(sb.toString());
		final List<Object> resultado = query.getResultList();
		if (resultado != null && !resultado.isEmpty()) {
			return true;
		}

		return false;
	}

	public List<Persona> buscarJefeRRHH() {
		List<Persona> data = obtenerPersonasPorRol(new Rol(9, "JEFE_RRHH"));
		if (data == null || data.isEmpty()) {
			System.out.println("No existe Jefe de RRHH vigente");
		}
		return data;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Persona> obtenerPersonasPorRol(Rol rol){
		String hql = "select p from Persona p where p.vigente=true and ? in elements(p.roles)";
		Query query = em.createQuery(hql);
		query.setParameter(1, rol);
		List<Persona> personas = query.getResultList();
		return personas;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SelectItem> getAllDepartamentos(final String textoInicial) {
		final List<SelectItem> departamentos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			departamentos.add(new SelectItem(INICIO, textoInicial));
		}
		final Query query = em.createNamedQuery("Departamento.findByAll");
		
		final List<Departamento> dptos = query.getResultList();
		for (Departamento d : dptos) {
			departamentos.add(new SelectItem(d.getId(), d.getDescripcion()));
		}
		Collections.sort(departamentos, ComparatorUtils.compararItems);
		return departamentos;
	}
}
