package cl.exe.exedoc.util;

import javax.ejb.Local;

@Local
public interface VisualizarImagenFirmaLocal {

	public byte[] getImagenFirma(String rut);
}
