package cl.exe.exedoc.util;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;

@Stateless
@Name("visualizarImagenFirma")
public class VisualizarImagenFirma implements VisualizarImagenFirmaLocal {

	@PersistenceContext
	EntityManager entityManager;

	public byte[] getImagenFirma(String rut) {
		byte[] imagenFirma = null;
		if (rut != null && !rut.trim().isEmpty()) {
			Query query = entityManager.createNamedQuery("Persona.findImagenByRut");
			query.setParameter("rut", rut);
			imagenFirma = (byte[]) query.getSingleResult();
		}
		return imagenFirma;
	}

}
