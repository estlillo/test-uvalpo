package cl.exe.exedoc.util;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Persona;

@Local
public interface GeneradorResolucionRespuestaInterface {

	String generarResolucion(DocumentoElectronico doc, Integer idPlantilla, Persona persona);
}
