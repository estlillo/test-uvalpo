package cl.exe.exedoc.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.Logger;
import org.jboss.seam.Component;

import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.pojo.documento.ManejarDocumento;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.repositorio.Foliador;
import cl.exe.exedoc.repositorio.FoliadorLocal;
import cl.exe.exedoc.repositorio.Repositorio;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.Doc2Xml;
import cl.exe.exedoc.session.Doc2XmlBean;
import cl.exe.exedoc.session.exception.FoliadorException;

/**
 * class ServletUtils.
 * 
 * @author
 */
public class ServletUtils {

	private static Logger logger = Logger.getLogger(ServletUtils.class.getName());
	
	/**
	 * constructor.
	 */
	protected ServletUtils() {
		super();
	}

	/**
	 * Funciones para documentos electronicos, sin considerar archivos adjuntos.
	 * 
	 * @param idDocumento {@link Long}
	 * @param idFirmador {@link Long}
	 * @return byte []
	 */
	public static byte[] obtenerXml(final Long idDocumento, final Long idFirmador) {
		final DocumentoElectronico doc = getDocumento(idDocumento);
		byte[] result = null;

		if (preparaDocumentoAFirma(doc, idFirmador)) {
			result = getXmlData(doc);
		}
		return result;
	}

	/**
	 * @param idDoc {@link Long}
	 * @return {@link DocumentoElectronico}
	 */
	public static DocumentoElectronico getDocumento(final Long idDoc) {
		final Doc2Xml doc2Xml = (Doc2Xml) Component.getInstance(Doc2XmlBean.class);
		final DocumentoElectronico doc = (DocumentoElectronico) doc2Xml.getDocumento(idDoc);

		return doc;
	}

	/**
	 * @param doc {@link DocumentoElectronico}
	 * @return byte[]
	 */
	public static byte[] getXmlData(final DocumentoElectronico doc) {
		final Doc2Xml doc2Xml = (Doc2Xml) Component.getInstance(Doc2XmlBean.class);
		final byte[] data = doc2Xml.getXmlData(doc);

		return data;
	}

	/**
	 * @param doc {@link DocumentoElectronico}
	 * @param idFirmador {@link Long}
	 * @return {@link Boolean}
	 * @throws FoliadorException 
	 */
	public static boolean preparaDocumentoAFirma(final DocumentoElectronico doc, final Long idFirmador) {
		// boolean preparado = true;
		final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance(Repositorio.class);
		final FoliadorLocal foliador = (FoliadorLocal) Component.getInstance(Foliador.class);
		if (doc instanceof Resolucion || doc instanceof Decreto || doc instanceof Oficio || doc instanceof Convenio
				|| doc instanceof Contrato || doc instanceof Carta || doc instanceof Memorandum) {
			repositorio.generarFechaFirma(doc);
		}
		repositorio.firmar(doc, idFirmador);
		try {
			final String numeroDocumento = foliador.generarFolio(doc.getTipoDocumento(), null);
			doc.setNumeroDocumento(numeroDocumento);
			foliador.addFolioEnFirma(doc);
		} catch (FoliadorException f) {
			return false;
		}
		// try {
		// if (!(doc instanceof RespuestaConsultaCiudadana)) {
		// if (goMinInterior(doc)) {
		// // TODO cambiar en produccion
		// String result[] = TomarNumeroMinInterior.tomarNumero("", doc.getMateria());
		// //String result[] = { "a", "2222222", "b", "c" };
		// Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		// String numeroDocumento = result[1]/* + "/" + agnoActual */;
		// doc.setNumeroDocumento(numeroDocumento);
		// if (doc instanceof Resolucion) {
		// ((Resolucion) doc).setAgnoNumeroDocumento(agnoActual.toString());
		// ((Resolucion) doc).setIdDocumentoMI(result[3]);
		// }
		// if (doc instanceof Decreto) {
		// ((Decreto) doc).setAgnoNumeroDocumento(agnoActual.toString());
		// ((Decreto) doc).setIdDocumentoMI(result[3]);
		// }
		// } else {
		/*
		 * final String numeroDocumento = foliador.generarFolio(doc.getTipoDocumento(), null);
		 * doc.setNumeroDocumento(numeroDocumento);
		 */
		// }
		// }

		/*
		 * foliador.addFolioEnFirma(doc); } catch (FoliadorException e) { preparado = false; e.printStackTrace(); }
		 */
		return true;
	}

	/**
	 * @param documento {@link Documento}
	 * @return {@link Boolean}
	 */
	@SuppressWarnings("unused")
	private static boolean goMinInterior(final Documento documento) {
		return ((documento instanceof Resolucion) 
				&& ((Resolucion) documento).getTipo() != null 
				&& (((Resolucion) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON) 
						|| ((Resolucion) documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)))
				|| ((documento instanceof Decreto) 
						&& ((Decreto) documento).getTipo() != null 
						&& (((Decreto) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON) 
								|| ((Decreto) documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)));
	}

	/**
	 * @param idDoc {@link Long}
	 * @param idFirmador {@link Long}
	 * @param archivo {@link Byte}
	 * @throws Throwable 
	 */
	public static void guardaDocumentoFirmado(final Long idDoc, final Long idFirmador, final byte[] archivo) throws Throwable {
		final FoliadorLocal foliador = (FoliadorLocal) Component.getInstance(Foliador.class);
		final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance(Repositorio.class);
		final Doc2Xml doc2Xml = (Doc2Xml) Component.getInstance(Doc2XmlBean.class);
		final DocumentoElectronico documento = (DocumentoElectronico) doc2Xml.getDocumento(idDoc);

		repositorio.firmar(documento, idFirmador);
		repositorio.eliminarFirmas(documento, idFirmador);
		foliador.usarFolioEnFirma(documento);

		final ManejarDocumentoInterface md = (ManejarDocumentoInterface) Component.getInstance(ManejarDocumento.class);
		md.persistXML(documento, archivo);
		
	}

	/**
	 * Funciones para los archivos adjuntos de los documentos electronicos.
	 * 
	 * @param cmsId {@link String}
	 * @return byte
	 */
	public static byte[] recuperarArchivo(final String cmsId) {
		final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance(Repositorio.class);
		// byte[] data = repositorio.recuperarArchivo(cmsId);
		byte[] data = null;
		try {
			data = repositorio.getFile(cmsId);
		} catch (XPathExpressionException e) {
			logger.error("Error, XPathExpressionException", e);
		} catch (IOException e) {
			logger.error("Error, IOException", e);
		}

		return data;
	}

	/**
	 * @param cmsIdString {@link String}
	 * @param contentType {@link String}
	 * @param cmsId byte[]
	 * @param inputStream {@link ServletInputStream}
	 * @throws IOException IOException
	 */
	public static void actualizarArchivo(final String cmsIdString, final String contentType, final byte[] cmsId,
			final ServletInputStream inputStream) throws IOException {
		final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance(Repositorio.class);
		final byte[] documento = new byte[4096];
		int largo;
		final ByteArrayOutputStream bstream = new ByteArrayOutputStream();

		while ((largo = inputStream.read(documento)) > 0) {
			bstream.write(documento, 0, largo);
		}

		final byte[] newFileVersion = bstream.toByteArray();

		repositorio.actualizarArchivo(cmsIdString, contentType, newFileVersion, cmsId);
	}

	/**
	 * actualizarArchivoWS.
	 * 
	 * @param cmsIdString {@link String}
	 * @param contentType {@link String}
	 * @param file byte[]
	 * @param cmsId byte[]
	 */
	public static void actualizarArchivoWS(final String cmsIdString, final String contentType, final byte[] file,
			final byte[] cmsId) {
		final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance(Repositorio.class);
		repositorio.actualizarArchivo(cmsIdString, contentType, file, cmsId);
	}

}
