/**
 * 
 */
package cl.exe.exedoc.util;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.Component;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.repositorio.RepositorioLocal;

/**
 * @author jnova
 *
 */
public final class DocUtils {

    /**
     * 
     */
    private DocUtils() {
        super();
    }

    /**
     * Metodo para descargar un archivo desde el repositorio documental.
     * 
     * @param idArchivo el id del archivo en BD.
     * @param em instancia del EntityManager para consultar la BD.
     * @return true si el archivo fue descargado exitosamente. false en caso contrario.
     * @throws IOException si ocurre un problema ejecutando el HTTP GET.
     * @throws XPathExpressionException si ocurre un problema obteniendo el ticket.
     */
    public static boolean getFile(final Long idArchivo, final EntityManager em)
            throws XPathExpressionException, IOException {
        final Archivo archivo = em.find(Archivo.class, idArchivo);
        final RepositorioLocal repositorio = (RepositorioLocal) Component.getInstance("repositorio");
        boolean result = true;
        final byte[] data = repositorio.getFile(archivo.getCmsId());
        if (data != null) {
            final FacesContext facesContext = FacesContext.getCurrentInstance();
            try {
                final HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext()
                        .getResponse();
                response.setContentType(archivo.getContentType());
                response.setHeader("Content-disposition", "attachment; filename=\"" 
                        + archivo.getNombreArchivo() + "\"");
                response.getOutputStream().write(data);
                response.getOutputStream().flush();
                facesContext.responseComplete();
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }
        } else {
            result = false;
        }
        
        return result;
    }

}
