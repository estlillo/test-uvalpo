package cl.exe.exedoc.util;

/**
 * class DecretoElectronicoException.
 * 
 * @author
 */
public class DecretoElectronicoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2150166764988794482L;

	private static String STRING_ERROR = "-10000";

	private int errorCode;

	/**
	 * Constructor.
	 */
	public DecretoElectronicoException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param s {@link String}
	 * @param throwable {@link Throwable}
	 */
	public DecretoElectronicoException(final String s, final Throwable throwable) {
		super(s, throwable);
	}

	/**
	 * Constructor.
	 * 
	 * @param s {@link String}
	 */
	public DecretoElectronicoException(final String s) {
		super(s);
	}

	/**
	 * Constructor.
	 * 
	 * @param throwable {@link Throwable}
	 */
	public DecretoElectronicoException(final Throwable throwable) {
		super(throwable);
	}

	/**
	 * @return the errorCode
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(final int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @param sERROR 
	 */
	public static void setsERROR(final String sERROR) {
		DecretoElectronicoException.STRING_ERROR = sERROR;
	}

	@Override
	public String getMessage() {
		return "SISPER NOK";
	}

}
