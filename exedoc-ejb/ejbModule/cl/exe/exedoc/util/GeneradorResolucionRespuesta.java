package cl.exe.exedoc.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.Vistos;

@Stateless
@Name("GeneradorResolucionRespuesta")
public class GeneradorResolucionRespuesta implements GeneradorResolucionRespuestaInterface {

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;

	@In(required = true)
	private Persona usuario;

	// @In(required = false, value = "documento", scope =
	// ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Resolucion resolucion;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Integer tipoResolucion;

	@PersistenceContext
	private EntityManager em;

	public GeneradorResolucionRespuesta() {
	}

	public String generarResolucion(DocumentoElectronico doc, Integer idPlantilla, Persona persona) {
		if (resolucion == null) {
			this.inicializarResolucion();
		}
		this.idPlantillaSelec = idPlantilla;
		this.aplicarPlantilla(doc, idPlantilla);
		// if (doc instanceof Convenio) {
		this.adjuntarComoArchivoXML(doc, persona);
		// }
		return "crearResolucion";
	}

	private void inicializarResolucion() {
		resolucion = new Resolucion();
		Calendar ahora = new GregorianCalendar();
		resolucion.setFechaCreacion(ahora.getTime());
		ahora.add(Calendar.DATE, 5);
		resolucion.setPlazo(ahora.getTime());
		resolucion.setAutor(usuario);
		// tipo temporal, Siempre se toma como original el primero.
		resolucion.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
		// id temporal para borrar un documento
		resolucion.setIdNuevoDocumento(Integer.MAX_VALUE);
		resolucion.setReservado(false);
		resolucion.setEnEdicion(true);
	}

	private void aplicarPlantilla(DocumentoElectronico doc, Integer idPlantilla) {
		Plantilla plantilla;
		try {
			plantilla = (Plantilla) em.createNamedQuery("Plantilla.findById").setParameter("id", idPlantilla).getSingleResult();
		} catch (NoResultException nre) {
			plantilla = null;
		}

		if (plantilla != null) {
			for (CamposPlantilla cp : plantilla.getCamposPlantilla()) {
				Integer tipoContenido = cp.getTipoContenido().getId();
				if (tipoContenido.equals(TipoContenido.TIPO)) {
					String tipo = cp.getContenido();
					if (tipo.equals("EXENTO")) {
						tipoResolucion = 1;
					} else if (tipo.equals("TOMA DE RAZON")) {
						tipoResolucion = 2;
					} else {
						tipoResolucion = 3;
					}
				}
				if (tipoContenido.equals(TipoContenido.MATERIA)) {
					String materia = cp.getContenido();
					this.resolucion.setMateria(materia);
				}
				if (tipoContenido.equals(TipoContenido.VISTOS)) {
					String vistos = cp.getContenido();
					if (((Resolucion) this.resolucion).getVistos() != null && !((Resolucion) this.resolucion).getVistos().isEmpty()) {
						((Resolucion) this.resolucion).getVistos().get(0).setCuerpo(vistos);
					} else {
						Vistos parrafo = new Vistos();
						parrafo.setCuerpo(vistos);
						parrafo.setDocumento(((Resolucion) this.resolucion));
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						((Resolucion) this.resolucion).setVistos(parrafos);
					}
				}
				if (tipoContenido.equals(TipoContenido.CONSIDERANDO)) {
					String considerando = cp.getContenido();
					if (((Resolucion) this.resolucion).getConsiderandos() != null && !((Resolucion) this.resolucion).getConsiderandos().isEmpty()) {
						((Resolucion) this.resolucion).getConsiderandos().get(0).setCuerpo(considerando);
					} else {
						Considerandos parrafo = new Considerandos();
						parrafo.setCuerpo(considerando);
						parrafo.setDocumento(((Resolucion) this.resolucion));
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						((Resolucion) this.resolucion).setConsiderandos(parrafos);
					}
				}
				if (tipoContenido.equals(TipoContenido.RESUELVO)) {
					String resuelvo = this.setearResuelvo(doc);
					if (((Resolucion) this.resolucion).getResuelvo() != null && !((Resolucion) this.resolucion).getResuelvo().isEmpty()) {
						((Resolucion) this.resolucion).getResuelvo().get(0).setCuerpo(resuelvo);
					} else {
						Resuelvo parrafo = new Resuelvo();
						parrafo.setCuerpo(resuelvo);
						parrafo.setDocumento(((Resolucion) this.resolucion));
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						((Resolucion) this.resolucion).setResuelvo(parrafos);
					}
				}
				if (cp.getTipoContenido().getId().equals(TipoContenido.INDICACION)) {
					String indicaciones = cp.getContenido();
					((Resolucion) this.resolucion).setIndicaciones(indicaciones);
				}
			}
		}
	}

	private String setearResuelvo(DocumentoElectronico doc) {
		if (doc instanceof Contrato) {
			String puntos = ((Contrato) doc).getPuntos().get(0).getCuerpo();
			return puntos;
		} else if (doc instanceof Convenio) {
			String cuerpoConvenio = this.armarCuerpoConvenio(((Convenio) doc));
			return cuerpoConvenio;
		}
		return null;
	}

	private String armarCuerpoConvenio(Convenio conv) {
		StringBuilder cuerpoConvenio = new StringBuilder("CONSIDERANDO:\n");
		cuerpoConvenio.append(conv.getConsiderandos().get(0).getCuerpo() + "\n\n");
		cuerpoConvenio.append("TENIENDO PRESENTE:\n");
		cuerpoConvenio.append(conv.getTeniendoPresente().get(0).getCuerpo() + "\n\n");
		cuerpoConvenio.append("CONVIENEN:\n");
		cuerpoConvenio.append(conv.getConvienen().get(0).getCuerpo());

		return cuerpoConvenio.toString();
	}

	private void adjuntarComoArchivoXML(DocumentoElectronico doc, Persona persona) {
		if (doc.getDocumentoXML() != null && !doc.getDocumentoXML().isEmpty()) {
			if (resolucion.getArchivosAdjuntos() == null) {
				resolucion.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
			}
			ArchivoAdjuntoDocumentoElectronico archivo = new ArchivoAdjuntoDocumentoElectronico();
			archivo.setCmsId(doc.getCmsId());
			archivo.setFecha(doc.getFechaDocumentoOrigen());
			String nombreArchivo = doc.getTipoDocumento().getDescripcion().replace(' ', '_');
			archivo.setNombreArchivo(nombreArchivo + "_" + doc.getNumeroNumeroDocumento() + "-" + doc.getAgnoNumeroDocumento() + ".xml");
			archivo.setContentType("text/xml");
			archivo.setMateria(doc.getTipoDocumento().getDescripcion() + doc.getNumeroDocumento());
			archivo.setDocumentoElectronico(doc);
			archivo.setAdjuntadoPor(persona);
			archivo.setXml(true);
			em.persist(archivo);
			resolucion.getArchivosAdjuntos().add(archivo);
		}

	}
}
