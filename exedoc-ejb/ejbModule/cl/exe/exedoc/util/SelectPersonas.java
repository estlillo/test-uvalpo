package cl.exe.exedoc.util;

import cl.exe.exedoc.entity.Persona;

public class SelectPersonas {

	private String descripcion;
	private Persona persona;

	public SelectPersonas() {

	}

	public SelectPersonas(String descripcion) {
		this.descripcion = descripcion;
	}

	public SelectPersonas(String descripcion, Persona persona) {
		this.descripcion = descripcion;
		this.persona = persona;
	}

	public String getDescripcion() {
		
		if(descripcion == null) return "";
		if (descripcion.trim().length() > 50) {
			return descripcion.substring(0, 50).concat("...");
		} else {
			return descripcion;
		}
		
		
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof SelectPersonas) {
			return descripcion.equals(((SelectPersonas) obj).getDescripcion());
		}
		return false;
	}

}
