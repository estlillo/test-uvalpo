package cl.exe.exedoc.util;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import cl.exe.exedoc.entity.FormatoDocumento;

public class FormatoDocumentosList {

	private static FormatoDocumentosList instance;
	private static List<FormatoDocumento> formatoDocumentoList;

	public static final String TXT_PAPEL = "Papel";
	public static final String TXT_DIGITALIZADO = "Digitalizado";
	public static final Integer PAPEL = 100;
	public static final Integer DIGITALIZADO = 200;

	private FormatoDocumentosList(EntityManager em) {
		restartTipoDocumentosList(em);
	}

	@SuppressWarnings("unchecked")
	public void restartTipoDocumentosList(EntityManager em) {
		Query query = em.createQuery("SELECT td FROM FormatoDocumento td");
		formatoDocumentoList = query.getResultList();
	}

	public static FormatoDocumentosList getInstance(EntityManager em) {
		if (instance == null) {
			instance = new FormatoDocumentosList(em);
		}
		return instance;
	}

	public List<SelectItem> getFormatosDocumentoList(String textoInicial) {
		List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (FormatoDocumento td : formatoDocumentoList) {
			tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
		}
		return tipos;
	}

}
