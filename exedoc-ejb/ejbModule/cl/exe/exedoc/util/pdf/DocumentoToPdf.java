package cl.exe.exedoc.util.pdf;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.output.ByteArrayOutputStream;

import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.entity.VisacionDocumento;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * DocumentoToPdf.
 * 
 * @author
 */
public class DocumentoToPdf {

	/**
	 * Constructor.
	 */
	protected DocumentoToPdf() {
		super();
	}

	public static byte[] documentoToPdf(Documento documento) {
		if (documento instanceof Resolucion) {
			return documentoResolucionToPdf((Resolucion) documento);
		} else if (documento instanceof Decreto) {
			return documentoDecretoToPdf((Decreto) documento);
		} else if (documento instanceof Convenio) {
			return documentoConvenioToPdf((Convenio) documento);
		} else if (documento instanceof Contrato) { return documentoContratoToPdf((Contrato) documento); }
		return null;
	}

	private static byte[] documentoResolucionToPdf(Resolucion resolucion) {
		Document documentoPDF = new Document(PageSize.LETTER);
		String tipoResolucionStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osPDF = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(documentoPDF, osPDF);
			documentoPDF.open();
			PdfPTable encabezado = new PdfPTable(2);
			encabezado.setWidthPercentage(100);
			encabezado.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			PdfPCell logoCell = new PdfPCell(logo, false);
			logoCell.setBorder(Rectangle.NO_BORDER);
			encabezado.addCell(logoCell);
			PdfPTable nested1 = new PdfPTable(1);
			nested1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			nested1.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
			nested1.addCell(resolucion.getMateria());
			Integer tipoResolucion = resolucion.getTipo().getId();
			if (tipoResolucion.equals(TipoRazon.EXENTO)) {
				tipoResolucionStr = "RESOLUCION EXENTA Nº: ";
			} else if (tipoResolucion.equals(TipoRazon.TOMA_RAZON)) {
				tipoResolucionStr = "RESOLUCION TOMA DE RAZON Nº: ";
			} else if (tipoResolucion.equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				tipoResolucionStr = "RESOLUCION EXENTA CON REGISTRO Nº: ";
			}
			tipoResolucionStr += resolucion.getNumeroDocumento();
			nested1.addCell(tipoResolucionStr);
			Date fechaRes = resolucion.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaResStr = "SANTIAGO, " + simpleDateFormat.format(fechaRes);
			nested1.addCell(fechaResStr);
			encabezado.addCell(nested1);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoPDF.add(encabezado);

			Paragraph vistosPar = new Paragraph("VISTOS:\n" + resolucion.getVistos().get(0).getCuerpo());
			vistosPar.setSpacingBefore(15f);
			vistosPar.setSpacingAfter(15f);
			vistosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			vistosPar.setFont(font);
			documentoPDF.add(vistosPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n"
					+ resolucion.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoPDF.add(considerandoPar);

			Paragraph resuelvoPar = new Paragraph("RESUELVO:\n" + resolucion.getResuelvo().get(0).getCuerpo());
			resuelvoPar.setSpacingAfter(15f);
			resuelvoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			resuelvoPar.setFont(font);
			documentoPDF.add(resuelvoPar);

			Paragraph indicacionesPar = new Paragraph(resolucion.getIndicaciones());
			indicacionesPar.setSpacingAfter(15f);
			indicacionesPar.setAlignment(Element.ALIGN_LEFT);
			indicacionesPar.setFont(font);
			documentoPDF.add(indicacionesPar);

			if (resolucion.getFirmas() != null && !resolucion.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = resolucion.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = null;
				if (firmante.getExterno() == null || !firmante.getExterno()) {
					datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
					if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
						datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
					} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
					} else {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
										.getDescripcion();
					}
				} else {
					datosFirmanteStr = firmante.getNombreApellido();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoPDF.add(datosFirmante);
			}

			if (resolucion.getVisaciones() != null && !resolucion.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = resolucion.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(resolucion);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += resolucion.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoPDF.add(datosVisadores);
				}
			}
			if (!resolucion.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : resolucion.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoPDF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoPDF.close();
		return osPDF.toByteArray();

	}

	private static byte[] documentoDecretoToPdf(Decreto decreto) {
		Document documentoPDF = new Document(PageSize.LETTER);
		String tipoDecretoStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osPDF = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(documentoPDF, osPDF);
			documentoPDF.open();
			PdfPTable encabezado = new PdfPTable(2);
			encabezado.setWidthPercentage(100);
			encabezado.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			PdfPCell logoCell = new PdfPCell(logo, false);
			logoCell.setBorder(Rectangle.NO_BORDER);
			encabezado.addCell(logoCell);
			PdfPTable nested1 = new PdfPTable(1);
			nested1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			nested1.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
			nested1.addCell(decreto.getMateria());
			Integer tipoDecreto = decreto.getTipo().getId();
			if (tipoDecreto.equals(TipoRazon.EXENTO)) {
				tipoDecretoStr = "DECRETO EXENTO Nº: ";
			} else if (tipoDecreto.equals(TipoRazon.TOMA_RAZON)) {
				tipoDecretoStr = "DECRETO TOMA DE RAZON Nº: ";
			} else if (tipoDecreto.equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				tipoDecretoStr = "DECRETO EXENTO CON REGISTRO Nº: ";
			}
			tipoDecretoStr += decreto.getNumeroDocumento();
			nested1.addCell(tipoDecretoStr);
			Date fechaDec = decreto.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaDecStr = "SANTIAGO, " + simpleDateFormat.format(fechaDec);
			nested1.addCell(fechaDecStr);
			encabezado.addCell(nested1);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoPDF.add(encabezado);

			Paragraph vistosPar = new Paragraph("VISTOS:\n" + decreto.getVistos().get(0).getCuerpo());
			vistosPar.setSpacingBefore(15f);
			vistosPar.setSpacingAfter(15f);
			vistosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			vistosPar.setFont(font);
			documentoPDF.add(vistosPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n" + decreto.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoPDF.add(considerandoPar);

			Paragraph decretoPar = new Paragraph("DECRETO:\n" + decreto.getResuelvo().get(0).getCuerpo());
			decretoPar.setSpacingAfter(15f);
			decretoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			decretoPar.setFont(font);
			documentoPDF.add(decretoPar);

			Paragraph indicacionesPar = new Paragraph(decreto.getIndicaciones());
			indicacionesPar.setSpacingAfter(15f);
			indicacionesPar.setAlignment(Element.ALIGN_LEFT);
			indicacionesPar.setFont(font);
			documentoPDF.add(indicacionesPar);

			if (decreto.getFirmas() != null && !decreto.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = decreto.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = null;
				if (firmante.getExterno() == null || !firmante.getExterno()) {
					datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
					if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
						datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
					} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
					} else {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
										.getDescripcion();
					}
				} else {
					datosFirmanteStr = firmante.getNombreApellido();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoPDF.add(datosFirmante);
			}

			if (decreto.getVisaciones() != null && !decreto.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = decreto.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(decreto);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += decreto.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoPDF.add(datosVisadores);
				}
			}
			if (!decreto.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : decreto.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoPDF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoPDF.close();
		return osPDF.toByteArray();

	}

	private static byte[] documentoConvenioToPdf(Convenio convenio) {
		Document documentoPDF = new Document(PageSize.LETTER);
		String tipoConvenioStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osPDF = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(documentoPDF, osPDF);
			documentoPDF.open();
			PdfPTable encabezado = new PdfPTable(2);
			encabezado.setWidthPercentage(100);
			encabezado.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			PdfPCell logoCell = new PdfPCell(logo, false);
			logoCell.setBorder(Rectangle.NO_BORDER);
			encabezado.addCell(logoCell);
			PdfPTable nested1 = new PdfPTable(1);
			nested1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			nested1.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
			nested1.addCell(convenio.getMateria());
			tipoConvenioStr = "CONVENIO Nº: ";
			String numDoc = convenio.getNumeroDocumento();
			if (numDoc != null) {
				tipoConvenioStr += convenio.getNumeroDocumento();
			}
			nested1.addCell(tipoConvenioStr);
			Date fechaDec = convenio.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaDecStr = "SANTIAGO, " + simpleDateFormat.format(fechaDec);
			nested1.addCell(fechaDecStr);
			encabezado.addCell(nested1);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoPDF.add(encabezado);

			Paragraph tituloPar = new Paragraph("SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (SUBDERE)\nY\n"
					+ convenio.getTitulo());
			tituloPar.setSpacingBefore(15f);
			tituloPar.setSpacingAfter(15f);
			tituloPar.setAlignment(Element.ALIGN_CENTER);
			tituloPar.setFont(new Font(Font.TIMES_ROMAN, 14, Font.BOLD));
			documentoPDF.add(tituloPar);

			Paragraph prologoPar = new Paragraph(convenio.getPrologo());
			prologoPar.setSpacingBefore(15f);
			prologoPar.setSpacingAfter(15f);
			prologoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			prologoPar.setFont(font);
			documentoPDF.add(prologoPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n"
					+ convenio.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoPDF.add(considerandoPar);

			Paragraph teniendoPresentePar = new Paragraph("TENIENDO PRESENTE:\n"
					+ convenio.getTeniendoPresente().get(0).getCuerpo());
			teniendoPresentePar.setSpacingAfter(15f);
			teniendoPresentePar.setAlignment(Element.ALIGN_JUSTIFIED);
			teniendoPresentePar.setFont(font);
			documentoPDF.add(teniendoPresentePar);

			Paragraph convienenPar = new Paragraph("CONVIENEN:\n" + convenio.getConvienen().get(0).getCuerpo());
			convienenPar.setSpacingAfter(15f);
			convienenPar.setAlignment(Element.ALIGN_JUSTIFIED);
			convienenPar.setFont(font);
			documentoPDF.add(convienenPar);

			if (convenio.getFirmas() != null && !convenio.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = convenio.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = null;
				if (firmante.getExterno() == null || !firmante.getExterno()) {
					datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
					if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
						datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
					} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
					} else {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
										.getDescripcion();
					}
				} else {
					datosFirmanteStr = firmante.getNombreApellido();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoPDF.add(datosFirmante);
			}

			if (convenio.getVisaciones() != null && !convenio.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = convenio.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(convenio);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += convenio.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoPDF.add(datosVisadores);
				}
			}
			if (!convenio.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : convenio.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoPDF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoPDF.close();
		return osPDF.toByteArray();

	}

	private static byte[] documentoContratoToPdf(Contrato contrato) {
		Document documentoPDF = new Document(PageSize.LETTER);
		String tipoContratoStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osPDF = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(documentoPDF, osPDF);
			documentoPDF.open();
			PdfPTable encabezado = new PdfPTable(2);
			encabezado.setWidthPercentage(100);
			encabezado.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			PdfPCell logoCell = new PdfPCell(logo, false);
			logoCell.setBorder(Rectangle.NO_BORDER);
			encabezado.addCell(logoCell);
			PdfPTable nested1 = new PdfPTable(1);
			nested1.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			nested1.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
			nested1.addCell(contrato.getMateria());
			tipoContratoStr = "CONTRATO Nº: ";
			String numDoc = contrato.getNumeroDocumento();
			if (numDoc != null) {
				tipoContratoStr += contrato.getNumeroDocumento();
			}
			nested1.addCell(tipoContratoStr);
			Date fechaDec = contrato.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaDecStr = "SANTIAGO, " + simpleDateFormat.format(fechaDec);
			nested1.addCell(fechaDecStr);
			encabezado.addCell(nested1);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoPDF.add(encabezado);

			Paragraph tituloPar = new Paragraph("CONTRATO DE PRESTACION DE SERVICIOS\n "
					+ "SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (SUBDERE)\nY\n" + contrato.getTitulo());
			tituloPar.setSpacingBefore(15f);
			tituloPar.setSpacingAfter(15f);
			tituloPar.setAlignment(Element.ALIGN_CENTER);
			tituloPar.setFont(new Font(Font.TIMES_ROMAN, 14, Font.BOLD));
			documentoPDF.add(tituloPar);

			Paragraph prologoPar = new Paragraph(contrato.getPrologo());
			prologoPar.setSpacingBefore(15f);
			prologoPar.setSpacingAfter(15f);
			prologoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			prologoPar.setFont(font);
			documentoPDF.add(prologoPar);

			Paragraph puntosPar = new Paragraph("SE HA CONVENIDO LO SIGUIENTE:\n"
					+ contrato.getPuntos().get(0).getCuerpo());
			puntosPar.setSpacingAfter(15f);
			puntosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			puntosPar.setFont(font);
			documentoPDF.add(puntosPar);

			if (contrato.getFirmas() != null && !contrato.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = contrato.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = null;
				if (firmante.getExterno() == null || !firmante.getExterno()) {
					datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
					if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
						datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
					} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
					} else {
						datosFirmanteStr += "\n"
								+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
										.getDescripcion();
					}
				} else {
					datosFirmanteStr = firmante.getNombreApellido();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoPDF.add(datosFirmante);
			}

			if (contrato.getVisaciones() != null && !contrato.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = contrato.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(contrato);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += contrato.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoPDF.add(datosVisadores);
				}
			}
			if (!contrato.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : contrato.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoPDF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoPDF.close();
		return osPDF.toByteArray();

	}

	private static List<VisacionDocumento> getVisacionesV(Documento documento) {
		List<VisacionDocumento> visaciones = null;
		if (documento != null && documento.getVisaciones() != null) {
			visaciones = documento.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public static String getUrlFirma() {
		String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}

}
