package cl.exe.exedoc.util;

import java.util.Date;

import cl.exe.exedoc.entity.Ciudadano;

public class DatosSolicitudTransparencia {
	
	private String folio;
	private Long idUltimaSolicitudObtenida;
	private String emisor;
	private String consulta;
	private Long idFormaRecepcion;
	private Long idFormatoEntrega;
	private Date fechaFormulacionConsulta;
	private Date fechaTerminoConsulta;
	private Ciudadano ciudadano;

	public Ciudadano getCiudadano() {
		return ciudadano;
	}

	public void setCiudadano(Ciudadano ciudadano) {
		this.ciudadano = ciudadano;
	}

	public Date getFechaFormulacionConsulta() {
		return fechaFormulacionConsulta;
	}

	public void setFechaFormulacionConsulta(Date fechaFormulacionConsulta) {
		this.fechaFormulacionConsulta = fechaFormulacionConsulta;
	}

	public Date getFechaTerminoConsulta() {
		return fechaTerminoConsulta;
	}
	
	public void setFechaTerminoConsulta(Date fechaTerminoConsulta) {
		this.fechaTerminoConsulta = fechaTerminoConsulta;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getEmisor() {
		return emisor;
	}

	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Long getIdUltimaSolicitudObtenida() {
		return idUltimaSolicitudObtenida;
	}

	public void setIdUltimaSolicitudObtenida(Long idUltimaSolicitudObtenida) {
		this.idUltimaSolicitudObtenida = idUltimaSolicitudObtenida;
	}

	public Long getIdFormaRecepcion() {
		return idFormaRecepcion;
	}

	public void setIdFormaRecepcion(Long idFormaRecepcion) {
		this.idFormaRecepcion = idFormaRecepcion;
	}

	public Long getIdFormatoEntrega() {
		return idFormatoEntrega;
	}

	public void setIdFormatoEntrega(Long idFormatoEntrega) {
		this.idFormatoEntrega = idFormatoEntrega;
	}
}