package cl.exe.exedoc.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Class ReadProperties.
 * 
 * @author Ricardo Fuentes
 */
public class ReadProperties {

	private static Logger logger = Logger.getLogger(ReadProperties.class);

	/**
	 * Constructor.
	 */
	protected ReadProperties() {
		super();
	}

	/**
	 * Metodo utilizado para cargar propiedades.
	 * 
	 * @param name {@link String}, nombre de la propiedad.
	 * @return {@link Properties}
	 */
	public static Properties read(final String name) {

		final Properties properties = new Properties();
		try {
			final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
			if (inputStream != null) {
				try {
					properties.load(inputStream);
				} catch (IOException e) {
					logger.error("Ocurrio un error al leer el archivo.");
				}
				return properties;
			} else {
				logger.error("Archivo de configuración no encontrado.");
			}
		} catch (IllegalArgumentException e) {
			logger.error("Encoding del del archivo incorrecto.");
		}

		return null;
	}
}
