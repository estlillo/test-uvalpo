package cl.exe.exedoc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

/**
 * @author Administrator
 *
 */
@Name("regexValidator")
@BypassInterceptors
@Validator
public class RegexValidator implements javax.faces.validator.Validator {

	private static final String SOLOESPACIOS = "No se aceptan espacios en blanco.";

	/**
	 * Constructor.
	 */
	public RegexValidator() {
		super();
	}

	@Override
	public void validate(final FacesContext context, final UIComponent component,
			final Object value) {

		final String expresionRegular = (String) component.getAttributes().get(
				"regexExcl");
		String svalue = (String) value;

		svalue = svalue.trim();

		if (svalue.compareTo("") == 0) {

			final FacesMessage message = new FacesMessage();
			message.setDetail(SOLOESPACIOS);
			message.setSummary(SOLOESPACIOS);
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			
			throw new ValidatorException(message);
		}

		if (expresionRegular != null) {
			final Pattern p = Pattern.compile(expresionRegular);
			final Matcher m = p.matcher(svalue);

			if (expresionRegular.compareTo("") != 0) {
				if (m.find()) {
					final String detail = "Valor ingresado no válido.";
					final FacesMessage message = new FacesMessage();
					message.setDetail(detail);
					message.setSummary("No se acepta " + expresionRegular);
					message.setSeverity(FacesMessage.SEVERITY_ERROR);
					
					throw new ValidatorException(message);
				}
			}

		}
	}
}
