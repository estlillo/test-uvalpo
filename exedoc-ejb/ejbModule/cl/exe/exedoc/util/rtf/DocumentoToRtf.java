package cl.exe.exedoc.util.rtf;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.output.ByteArrayOutputStream;

import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.entity.VisacionDocumento;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.table.RtfCell;

/**
 * Class DocumentoToRtf.
 * 
 * @author Ricardo Fuentes
 */
public class DocumentoToRtf {

	/**
	 * Constructor.
	 */
	protected DocumentoToRtf() {
		super();
	}

	public static byte[] documentoToRtf(Documento documento) {
		if (documento instanceof Resolucion) {
			return documentoResolucionToRtf((Resolucion) documento);
		} else if (documento instanceof Decreto) {
			return documentoDecretoToRtf((Decreto) documento);
		} else if (documento instanceof Convenio) {
			return documentoConvenioToRtf((Convenio) documento);
		} else if (documento instanceof Contrato) { return documentoContratoToRtf((Contrato) documento); }
		return null;
	}

	private static byte[] documentoResolucionToRtf(Resolucion resolucion) {
		Document documentoRTF = new Document(PageSize.LETTER);
		String tipoResolucionStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osRTF = new ByteArrayOutputStream();

		try {
			RtfWriter2.getInstance(documentoRTF, osRTF);
			documentoRTF.open();

			Table encabezado = new Table(2);

			encabezado.setWidth(100);
			encabezado.getDefaultLayout().setBorder(Rectangle.NO_BORDER);

			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			RtfCell logoCell = new RtfCell(logo);
			logoCell.setBorder(Rectangle.NO_BORDER);

			encabezado.addCell(logoCell);

			Paragraph encabezadoDerecho = new Paragraph();
			encabezadoDerecho.setAlignment(Element.ALIGN_JUSTIFIED);
			encabezadoDerecho.setFont(font);
			Chunk textoEncabezadoDerecho = new Chunk();
			textoEncabezadoDerecho.append(resolucion.getMateria() + "\n");
			Integer tipoResolucion = resolucion.getTipo().getId();
			if (tipoResolucion.equals(TipoRazon.EXENTO)) {
				tipoResolucionStr = "RESOLUCION EXENTA Nº: ";
			} else if (tipoResolucion.equals(TipoRazon.TOMA_RAZON)) {
				tipoResolucionStr = "RESOLUCION TOMA DE RAZON Nº: ";
			} else if (tipoResolucion.equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				tipoResolucionStr = "RESOLUCION EXENTA CON REGISTRO Nº: ";
			}
			String numDoc = resolucion.getNumeroDocumento();
			if (numDoc != null) {
				tipoResolucionStr += resolucion.getNumeroDocumento();
			}
			textoEncabezadoDerecho.append(tipoResolucionStr + "\n");
			Date fechaRes = resolucion.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaResStr = "SANTIAGO, ";
			if (fechaRes != null) {
				fechaResStr += simpleDateFormat.format(fechaRes);
			}
			textoEncabezadoDerecho.append(fechaResStr);
			encabezadoDerecho.add(textoEncabezadoDerecho);

			encabezado.addCell(encabezadoDerecho);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoRTF.add(encabezado);

			Paragraph vistosPar = new Paragraph("VISTOS:\n" + resolucion.getVistos().get(0).getCuerpo());
			vistosPar.setSpacingBefore(15f);
			vistosPar.setSpacingAfter(15f);
			vistosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			vistosPar.setFont(font);
			documentoRTF.add(vistosPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n"
					+ resolucion.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoRTF.add(considerandoPar);

			Paragraph resuelvoPar = new Paragraph("RESUELVO:\n" + resolucion.getResuelvo().get(0).getCuerpo());
			resuelvoPar.setSpacingAfter(15f);
			resuelvoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			resuelvoPar.setFont(font);
			documentoRTF.add(resuelvoPar);

			Paragraph indicacionesPar = new Paragraph(resolucion.getIndicaciones());
			indicacionesPar.setSpacingAfter(15f);
			indicacionesPar.setAlignment(Element.ALIGN_LEFT);
			indicacionesPar.setFont(font);
			documentoRTF.add(indicacionesPar);

			if (resolucion.getFirmas() != null && !resolucion.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = resolucion.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
				if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
					datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
				} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
				} else {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
									.getDescripcion();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoRTF.add(datosFirmante);
			}

			if (resolucion.getVisaciones() != null && !resolucion.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = resolucion.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(resolucion);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += resolucion.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoRTF.add(datosVisadores);
				}
			}
			if (!resolucion.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : resolucion.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoRTF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoRTF.close();
		return osRTF.toByteArray();
	}

	private static byte[] documentoDecretoToRtf(Decreto decreto) {
		Document documentoRTF = new Document(PageSize.LETTER);
		String tipoDecretoStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osRTF = new ByteArrayOutputStream();

		try {
			RtfWriter2.getInstance(documentoRTF, osRTF);
			documentoRTF.open();

			Table encabezado = new Table(2);

			encabezado.setWidth(100);
			encabezado.getDefaultLayout().setBorder(Rectangle.NO_BORDER);

			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			RtfCell logoCell = new RtfCell(logo);
			logoCell.setBorder(Rectangle.NO_BORDER);

			encabezado.addCell(logoCell);

			Paragraph encabezadoDerecho = new Paragraph();
			encabezadoDerecho.setAlignment(Element.ALIGN_JUSTIFIED);
			encabezadoDerecho.setFont(font);
			Chunk textoEncabezadoDerecho = new Chunk();
			textoEncabezadoDerecho.append(decreto.getMateria() + "\n");
			Integer tipoDecreto = decreto.getTipo().getId();
			if (tipoDecreto.equals(TipoRazon.EXENTO)) {
				tipoDecretoStr = "DECRETO EXENT0 Nº: ";
			} else if (tipoDecreto.equals(TipoRazon.TOMA_RAZON)) {
				tipoDecretoStr = "DECRETO TOMA DE RAZON Nº: ";
			} else if (tipoDecreto.equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				tipoDecretoStr = "DECRETO EXENTO CON REGISTRO Nº: ";
			}
			String numDoc = decreto.getNumeroDocumento();
			if (numDoc != null) {
				tipoDecretoStr += decreto.getNumeroDocumento();
			}
			textoEncabezadoDerecho.append(tipoDecretoStr + "\n");
			Date fechaDec = decreto.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaDecStr = "SANTIAGO, ";
			if (fechaDec != null) {
				fechaDecStr += simpleDateFormat.format(fechaDec);
			}
			textoEncabezadoDerecho.append(fechaDecStr);
			encabezadoDerecho.add(textoEncabezadoDerecho);

			encabezado.addCell(encabezadoDerecho);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoRTF.add(encabezado);

			Paragraph vistosPar = new Paragraph("VISTOS:\n" + decreto.getVistos().get(0).getCuerpo());
			vistosPar.setSpacingBefore(15f);
			vistosPar.setSpacingAfter(15f);
			vistosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			vistosPar.setFont(font);
			documentoRTF.add(vistosPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n" + decreto.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoRTF.add(considerandoPar);

			Paragraph decretoPar = new Paragraph("DECRETO:\n" + decreto.getResuelvo().get(0).getCuerpo());
			decretoPar.setSpacingAfter(15f);
			decretoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			decretoPar.setFont(font);
			documentoRTF.add(decretoPar);

			Paragraph indicacionesPar = new Paragraph(decreto.getIndicaciones());
			indicacionesPar.setSpacingAfter(15f);
			indicacionesPar.setAlignment(Element.ALIGN_LEFT);
			indicacionesPar.setFont(font);
			documentoRTF.add(indicacionesPar);

			if (decreto.getFirmas() != null && !decreto.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = decreto.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
				if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
					datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
				} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
				} else {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
									.getDescripcion();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoRTF.add(datosFirmante);
			}

			if (decreto.getVisaciones() != null && !decreto.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = decreto.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(decreto);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += decreto.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoRTF.add(datosVisadores);
				}
			}
			if (!decreto.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : decreto.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoRTF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoRTF.close();
		return osRTF.toByteArray();
	}

	private static byte[] documentoConvenioToRtf(Convenio convenio) {
		Document documentoRTF = new Document(PageSize.LETTER);
		String tipoConvenioStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osRTF = new ByteArrayOutputStream();

		try {
			RtfWriter2.getInstance(documentoRTF, osRTF);
			documentoRTF.open();

			Table encabezado = new Table(2);

			encabezado.setWidth(100);
			encabezado.getDefaultLayout().setBorder(Rectangle.NO_BORDER);

			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			RtfCell logoCell = new RtfCell(logo);
			logoCell.setBorder(Rectangle.NO_BORDER);

			encabezado.addCell(logoCell);

			Paragraph encabezadoDerecho = new Paragraph();
			encabezadoDerecho.setAlignment(Element.ALIGN_JUSTIFIED);
			encabezadoDerecho.setFont(font);
			Chunk textoEncabezadoDerecho = new Chunk();
			textoEncabezadoDerecho.append(convenio.getMateria() + "\n");
			tipoConvenioStr = "CONVENIO Nº: ";
			String numDoc = convenio.getNumeroDocumento();
			if (numDoc != null) {
				tipoConvenioStr += convenio.getNumeroDocumento();
			}
			textoEncabezadoDerecho.append(tipoConvenioStr + "\n");
			Date fechaConv = convenio.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaConvStr = "SANTIAGO, ";
			if (fechaConv != null) {
				fechaConvStr += simpleDateFormat.format(fechaConv);
			}
			textoEncabezadoDerecho.append(fechaConvStr);
			encabezadoDerecho.add(textoEncabezadoDerecho);

			encabezado.addCell(encabezadoDerecho);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoRTF.add(encabezado);

			Paragraph tituloPar = new Paragraph("SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (SUBDERE)\nY\n"
					+ convenio.getTitulo());
			tituloPar.setSpacingBefore(15f);
			tituloPar.setSpacingAfter(15f);
			tituloPar.setAlignment(Element.ALIGN_CENTER);
			tituloPar.setFont(new Font(Font.TIMES_ROMAN, 14, Font.BOLD));
			documentoRTF.add(tituloPar);

			Paragraph prologoPar = new Paragraph(convenio.getPrologo());
			prologoPar.setSpacingBefore(15f);
			prologoPar.setSpacingAfter(15f);
			prologoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			prologoPar.setFont(font);
			documentoRTF.add(prologoPar);

			Paragraph considerandoPar = new Paragraph("CONSIDERANDO:\n"
					+ convenio.getConsiderandos().get(0).getCuerpo());
			considerandoPar.setSpacingAfter(15f);
			considerandoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			considerandoPar.setFont(font);
			documentoRTF.add(considerandoPar);

			Paragraph teniendoPresentePar = new Paragraph("TENIENDO PRESENTE:\n"
					+ convenio.getTeniendoPresente().get(0).getCuerpo());
			teniendoPresentePar.setSpacingAfter(15f);
			teniendoPresentePar.setAlignment(Element.ALIGN_JUSTIFIED);
			teniendoPresentePar.setFont(font);
			documentoRTF.add(teniendoPresentePar);

			Paragraph convienenPar = new Paragraph("CONVIENEN:\n" + convenio.getConvienen().get(0).getCuerpo());
			convienenPar.setSpacingAfter(15f);
			convienenPar.setAlignment(Element.ALIGN_JUSTIFIED);
			convienenPar.setFont(font);
			documentoRTF.add(convienenPar);

			if (convenio.getFirmas() != null && !convenio.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = convenio.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
				if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
					datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
				} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
				} else {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
									.getDescripcion();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoRTF.add(datosFirmante);
			}

			if (convenio.getVisaciones() != null && !convenio.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = convenio.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(convenio);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += convenio.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoRTF.add(datosVisadores);
				}
			}
			if (!convenio.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : convenio.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoRTF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoRTF.close();
		return osRTF.toByteArray();
	}

	private static byte[] documentoContratoToRtf(Contrato contrato) {
		Document documentoRTF = new Document(PageSize.LETTER);
		String tipoContratoStr = "";
		Font font = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);
		ByteArrayOutputStream osRTF = new ByteArrayOutputStream();

		try {
			RtfWriter2.getInstance(documentoRTF, osRTF);
			documentoRTF.open();

			Table encabezado = new Table(2);

			encabezado.setWidth(100);
			encabezado.getDefaultLayout().setBorder(Rectangle.NO_BORDER);

			Image logo = Image.getInstance(getUrlFirma() + "exedoc/img/logo_gobierno_01.gif");
			RtfCell logoCell = new RtfCell(logo);
			logoCell.setBorder(Rectangle.NO_BORDER);

			encabezado.addCell(logoCell);

			Paragraph encabezadoDerecho = new Paragraph();
			encabezadoDerecho.setAlignment(Element.ALIGN_JUSTIFIED);
			encabezadoDerecho.setFont(font);
			Chunk textoEncabezadoDerecho = new Chunk();
			textoEncabezadoDerecho.append(contrato.getMateria() + "\n");
			tipoContratoStr = "CONTRATO Nº: ";
			String numDoc = contrato.getNumeroDocumento();
			if (numDoc != null) {
				tipoContratoStr += contrato.getNumeroDocumento();
			}
			textoEncabezadoDerecho.append(tipoContratoStr + "\n");
			Date fechaConv = contrato.getFechaDocumentoOrigen();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String fechaConvStr = "SANTIAGO, ";
			if (fechaConv != null) {
				fechaConvStr += simpleDateFormat.format(fechaConv);
			}
			textoEncabezadoDerecho.append(fechaConvStr);
			encabezadoDerecho.add(textoEncabezadoDerecho);

			encabezado.addCell(encabezadoDerecho);
			encabezado.addCell("DOCUMENTO ELECTRONICO");
			encabezado.addCell(" ");
			documentoRTF.add(encabezado);

			Paragraph tituloPar = new Paragraph("CONTRATO DE PRESTACION DE SERVICIOS\n "
					+ "SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (SUBDERE)\nY\n" + contrato.getTitulo());
			tituloPar.setSpacingBefore(15f);
			tituloPar.setSpacingAfter(15f);
			tituloPar.setAlignment(Element.ALIGN_CENTER);
			tituloPar.setFont(new Font(Font.TIMES_ROMAN, 14, Font.BOLD));
			documentoRTF.add(tituloPar);

			Paragraph prologoPar = new Paragraph(contrato.getPrologo());
			prologoPar.setSpacingBefore(15f);
			prologoPar.setSpacingAfter(15f);
			prologoPar.setAlignment(Element.ALIGN_JUSTIFIED);
			prologoPar.setFont(font);
			documentoRTF.add(prologoPar);

			Paragraph puntosPar = new Paragraph("SE HA CONVENIDO LO SIGUIENTE:\n"
					+ contrato.getPuntos().get(0).getCuerpo());
			puntosPar.setSpacingAfter(15f);
			puntosPar.setAlignment(Element.ALIGN_JUSTIFIED);
			puntosPar.setFont(font);
			documentoRTF.add(puntosPar);

			if (contrato.getFirmas() != null && !contrato.getFirmas().isEmpty()) {
				List<FirmaDocumento> firmasRes = contrato.getFirmas();
				FirmaDocumento firmaRes = firmasRes.get(0);
				Persona firmante = firmaRes.getPersona();
				String datosFirmanteStr = firmante.getNombreApellido() + "\n" + firmante.getCargo().getDescripcion();
				if (!firmante.getCargo().getUnidadOrganizacional().getVirtual()) {
					datosFirmanteStr += "\n" + firmante.getCargo().getUnidadOrganizacional().getDescripcion();
				} else if (!firmante.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion();
				} else {
					datosFirmanteStr += "\n"
							+ firmante.getCargo().getUnidadOrganizacional().getDepartamento().getDivision()
									.getDescripcion();
				}
				Paragraph datosFirmante = new Paragraph(datosFirmanteStr);
				datosFirmante.setSpacingAfter(15f);
				datosFirmante.setAlignment(Element.ALIGN_RIGHT);
				datosFirmante.setFont(font);
				documentoRTF.add(datosFirmante);
			}

			if (contrato.getVisaciones() != null && !contrato.getVisaciones().isEmpty()) {
				{
					String inicialesVisador = contrato.getFirmas().get(0).getPersona().getIniciales() + " / ";
					List<VisacionDocumento> visasRes = getVisacionesV(contrato);
					for (VisacionDocumento visaRes : visasRes) {
						inicialesVisador += visaRes.getPersona().getIniciales();
						inicialesVisador += " / ";
					}
					inicialesVisador += contrato.getAutor().getIniciales().toLowerCase();
					Paragraph datosVisadores = new Paragraph(inicialesVisador);
					datosVisadores.setSpacingAfter(15f);
					datosVisadores.setAlignment(Element.ALIGN_LEFT);
					datosVisadores.setFont(font);
					documentoRTF.add(datosVisadores);
				}
			}
			if (!contrato.getDistribucionDocumento().isEmpty()) {
				String datosDistribucion = "DISTRIBUCION:\n";
				for (ListaPersonasDocumento persona : contrato.getDistribucionDocumento()) {
					if (datosDistribucion.isEmpty()) {
						datosDistribucion = persona.getDestinatario() + "\n";
					} else {
						datosDistribucion += persona.getDestinatario() + "\n";
					}
				}
				Paragraph distribucion = new Paragraph(datosDistribucion);
				distribucion.setSpacingAfter(15f);
				distribucion.setAlignment(Element.ALIGN_LEFT);
				distribucion.setFont(font);
				documentoRTF.add(distribucion);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		documentoRTF.close();
		return osRTF.toByteArray();
	}

	private static List<VisacionDocumento> getVisacionesV(Documento documento) {
		List<VisacionDocumento> visaciones = null;
		if (documento != null && documento.getVisaciones() != null) {
			visaciones = documento.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public static String getUrlFirma() {
		String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}
}
