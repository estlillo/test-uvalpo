package cl.exe.exedoc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class FileRestrictions {
	
	/**
	 * NAME, nombre de la clase.
	 */
	public static final String NAME = "FileRestrictions";

	private static List<String> restrictions;
	private static String nombreArchivoFileRestrictions = "fileRestrictions.properties";
	private static String separator = ",";

	static {
		try {
			restrictions = new ArrayList<String>();
			final InputStream is = getArchivo(nombreArchivoFileRestrictions);
			final BufferedReader bufRdr = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = bufRdr.readLine()) != null) {
				final StringTokenizer st = new StringTokenizer(line, separator);
				while (st.hasMoreTokens()) {
					restrictions.add(st.nextToken());
				}
			}
			bufRdr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructor.
	 */
	protected FileRestrictions() {
		super();
	}

	private static InputStream getArchivo(final String nombre) {
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		return loader.getResourceAsStream("exedoc/" + nombre);
	}

	public static boolean existeRestriccion(final String fileName) {
		String ext = "";
		final StringTokenizer st = new StringTokenizer(fileName, ".");
		while (st.hasMoreElements()) {
			ext = st.nextToken();
		}
		return restrictions.contains(ext);
	}

	public static List<String> getRestrictions() {
		return restrictions;
	}

}
