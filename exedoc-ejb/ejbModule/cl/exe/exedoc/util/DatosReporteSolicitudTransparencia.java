package cl.exe.exedoc.util;

import java.util.Date;

public class DatosReporteSolicitudTransparencia {
	
	private Long idDocumento;
	private String numeroExpediente;
	private String numeroDocumento;
	private Date fechaDocumento;
	private String autor;
	private String consulta;
	private String respuesta;
	private String responsableRespuesta;
	private Date fechaRespuesta;
	private String frecuente;
	private Long cantidad;
	private String estado;
	private String tipoTransparencia;
	
	public String getTipoTransparencia() {
		return tipoTransparencia;
	}
	public void setTipoTransparencia(String tipoTransparencia) {
		this.tipoTransparencia = tipoTransparencia;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getIdDocumento() {
		return idDocumento;
	}
	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public Date getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getResponsableRespuesta() {
		return responsableRespuesta;
	}
	public void setResponsableRespuesta(String responsableRespuesta) {
		this.responsableRespuesta = responsableRespuesta;
	}
	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	public String getFrecuente() {
		return frecuente;
	}
	public void setFrecuente(String frecuente) {
		this.frecuente = frecuente;
	}
	public String getConsulta() {
		return consulta;
	}
	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}	
}
