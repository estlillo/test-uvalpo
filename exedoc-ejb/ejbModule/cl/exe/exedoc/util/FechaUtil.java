package cl.exe.exedoc.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.GregorianCalendar;

/**
 * Utilidades para manejo de fechas.
 */
public final class FechaUtil {

	/**
	 * hasta.
	 */
	public static final String HASTA = "hasta";

	/**
	 * desde.
	 */
	public static final String DESDE = "desde";

	/**
	 * Valor de un dia {@link Long}.
	 */
	public static final Long DIA = 86400000L;
	
	/**
	 * Valor de una hora {@link Long}.
	 */
	public static final Long HORA = 3600000L;
	
	/**
	 * Valor de un minuto {@link Long}.
	 */
	public static final Long MINUTO = 60000L;
	
	/**
	 * Valor de un segundo {@link Long}.
	 */
	public static final Long SEGUNDO = 1000L;

	/**
	 * Formato "dd/MM/yyyy".
	 */
	public static final SimpleDateFormat EXTSDF = new SimpleDateFormat(
			"dd/MM/yyyy");
	/**
	 * Formato "dd/MM/yyyy HH:mm:ss".
	 */
	public static final SimpleDateFormat EXTCDF = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");
	/**
	 * Formato "yyyy-MM-dd".
	 */
	public static final SimpleDateFormat SDF = new SimpleDateFormat(
			"yyyy-MM-dd");
	/**
	 * Formato "HH:mm".
	 */
	public static final SimpleDateFormat HDF = new SimpleDateFormat("HH:mm");
	/**
	 * Formato "HH:mm:ss".
	 */
	public static final SimpleDateFormat HMSDF = new SimpleDateFormat(
			"HH:mm:ss");
	/**
	 * Formato "yyyy-MM-dd HH:mm".
	 */
	public static final SimpleDateFormat CDF = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm");
	/**
	 * Formato "yyyy-MM-dd HH:mm:ss".
	 */
	public static final SimpleDateFormat CDFDF = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	/**
	 * Formato "dd-MM-yyyy HH:mm:ss".
	 */
	public static final SimpleDateFormat BDSDF = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm:ss");
	/**
	 * Formato "dd-MM-yyyy HH:mm".
	 */
	public static final SimpleDateFormat SDFDMAH = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm");
	/**
	 * Formato "dd/MM/yyyy HH:mm".
	 */
	public static final SimpleDateFormat SDFCDMAH = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm");
	/**
	 * Formato "dd-MM-yyyy".
	 */
	public static final SimpleDateFormat SDFDMA = new SimpleDateFormat(
			"dd-MM-yyyy");
	/**
	 * Formato "yyyy-MM-dd'T'HH:mm:ss".
	 */
	public static final SimpleDateFormat CDFDFT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss");
	/**
	 * Formato "yyyy-MM".
	 */
	public static final SimpleDateFormat ANIOMES = new SimpleDateFormat(
			"yyyy-MM");
	/**
	 * Formato "yyyy".
	 */
	public static final SimpleDateFormat ANIO = new SimpleDateFormat("yyyy");
	/**
	 * Formato "MM".
	 */
	public static final SimpleDateFormat MES = new SimpleDateFormat("MM");

	/**
	 * LOCALE "es", "CL".
	 */
	public static final Locale LOCALE = new Locale("es", "CL");

	/**
	 * Constructor.
	 */
	private FechaUtil() {
		super();
	}

	/**
	 * Enum con los tipos de parametros para Query.
	 * 
	 * @author
	 */
	public enum TipoParametro {
		/**
		 * fecha desde.
		 */
		desde,
		/**
		 * fecha hasta.
		 */
		hasta
	}

	/**
	 * Método que retorna un fecha igual que la entregada con la hora, minutos, segundos y milisegundos en cero.
	 * 
	 * @param date Fecha
	 * @return Fecha
	 */
	public static Date inicio(final Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * Método que setea la hora, minutos, segundos y milisegundos en cero.
	 * 
	 * @param calendar El calendario.
	 */
	public static void inicio(final Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Método que retorna un fecha igual que la entregada con la hora, minutos y segundos en 23, 59 y 59
	 * respectivamente.
	 * 
	 * @param date Fecha
	 * @return Fecha
	 */
	public static Date fin(final Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTime();
	}

	/**
	 * Método para setear la hora, minutos y segundos en 23, 59 y 59 respectivamente.
	 * 
	 * @param calendar El calendario.
	 */
	public static void fin(final Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
	}

	/**
	 * Método que retorna un fecha equivalente al primer día del mes con la hora, minutos y segundos en cero.
	 * 
	 * @param date Fecha
	 * @return {@link Date}
	 */
	public static Date inicioMes(final Date date) {
		final Date day = FechaUtil.inicio(date);

		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		calendar.set(Calendar.DATE, 1);

		return calendar.getTime();
	}

	/**
	 * Método que retorna un fecha equivalente al último día del mes con la hora, minutos y segundos en 23, 59 y 59.
	 * 
	 * @param date Fecha
	 * @return Fecha
	 */
	public static Date finMes(final Date date) {
		final Date day = FechaUtil.inicioMes(date);

		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.SECOND, -1);

		return calendar.getTime();
	}

	/**
	 * Metodo que transforma un String a Date segun el SimpleDateFormat, el boolean es para validar si retorna la
	 * exception.
	 * 
	 * @param format {@link SimpleDateFormat}
	 * @param formato {@link String}
	 * @param isQuiet {@link Boolean}
	 * @return {@link Date}
	 */
	public static Date parse(final SimpleDateFormat format,
			final String formato, final boolean isQuiet) {
		try {
			return format.parse(formato);
		} catch (ParseException e) {
			if (isQuiet) {
				return null;
			} else {
				throw new RuntimeException("El formato de la fecha no es valido", e);
			}

		}
	}

	/**
	 * Metodo que transforma un String a Date segun el SimpleDateFormat.
	 * 
	 * @param format {@link SimpleDateFormat}
	 * @param formato {@link String}
	 * @return {@link Date}
	 */
	public static Date parse(final SimpleDateFormat format, final String formato) {
		return FechaUtil.parse(format, formato, false);
	}

	/**
	 * Metodo que permite Evaluar si el Año es Bisiesto.
	 * 
	 * @param anno Año a Evaluar
	 * @return {@link Boolean}
	 */
	public static Boolean annoEsBisiesto(final Integer anno) {

		if ((anno % 4 == 0) && ((anno % 100 != 0) || (anno % 400 == 0))) { return true; }

		return false;
	}

	/**
	 * Metodo retorna la diferencia en dias de dos fechas.
	 * 
	 * @param fechaInicio
	 *            {@link Date}
	 * @param fechaFin
	 *            {@link Date}
	 * @return {@link int}
	 */
	public static int diasDiferencia(final Date fechaInicio, final Date fechaFin) {

		if (fechaInicio != null && fechaFin != null) {
			final Calendar date1 = Calendar.getInstance();
			date1.setTime(fechaInicio);
			final Calendar date2 = Calendar.getInstance();
			date2.setTime(fechaFin);
			if (date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR)) {
				return date2.get(Calendar.DAY_OF_YEAR)
						- date1.get(Calendar.DAY_OF_YEAR);
			} else {

				final int diasAnio = ((GregorianCalendar) date1)
						.isLeapYear(date1.get(Calendar.YEAR)) ? 366 : 365;

				final int rangoAnios = date2.get(Calendar.YEAR)
						- date1.get(Calendar.YEAR);

				return (diasAnio * rangoAnios)
						+ (date2.get(Calendar.DAY_OF_YEAR) - date1
								.get(Calendar.DAY_OF_YEAR));
			}
		} else
			return 0;
	}
	
	/**
	 * Permite sumar y restar dias a una fecha determinada
	 * @return
	 */
	public static Date obtenerDia(Date fechaInicio, int i) {
		
		Calendar fechaCalendar = Calendar.getInstance();		
		
		fechaCalendar.setTime(fechaInicio);
		
		fechaCalendar.add(Calendar.DATE, i);		
		
		return fechaCalendar.getTime();	
	}
	
	/**
	 * Determina si la fecha cae dia habil
	 * @return
	 */
	public static boolean esdiaHabil(Date fecha) {
		long time = fecha.getTime();
		Calendar cale = Calendar.getInstance();

		cale.setTimeInMillis(time);
		int day = cale.get(Calendar.DAY_OF_WEEK);
		if (day != Calendar.SATURDAY && day != Calendar.SUNDAY) {
			return true;
		}
		return false;
	}
}
