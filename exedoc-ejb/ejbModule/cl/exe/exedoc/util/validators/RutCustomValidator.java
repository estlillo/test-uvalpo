package cl.exe.exedoc.util.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

import cl.exe.exedoc.session.exception.RUTException;
import cl.exe.exedoc.util.RutUtil;

@Name("rutCustomValidator")
@BypassInterceptors
@Validator
public class RutCustomValidator implements javax.faces.validator.Validator {
	
	private static final String DETAIL_NULL = "Debe ingresar el RUT.";
	private static final String SUMARY = "El RUT no es válido.";
	private static final String DETAIL = "RUT ingresado no válido.";

	@Override
	public void validate(final FacesContext context, final UIComponent component, final Object value)
			throws ValidatorException {

		try {
			if (!"-".equals(value)) {
				new RutUtil((String) value);
			} else {
				final String detail = DETAIL;
				final String summary = SUMARY;
				throw new ValidatorException(this.makeMessage(detail, summary));
			}
		} catch (RUTException e) {
			final String detail = DETAIL;
			final String summary = SUMARY;
			throw new ValidatorException(this.makeMessage(detail, summary));
		} catch (NullPointerException e) {
			final String detail = DETAIL_NULL;
			final String summary = DETAIL_NULL;
			throw new ValidatorException(this.makeMessage(detail, summary));
		}
	}

	/**
	 * @param detail {@link String}
	 * @param summary {@link String}
	 * @return {@link FacesMessage}
	 */
	private FacesMessage makeMessage(final String detail, final String summary) {
		final FacesMessage message = new FacesMessage();
		message.setDetail(detail);
		message.setSummary(summary);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		return message;
	}

}
