package cl.exe.exedoc.util.validators;

import java.util.StringTokenizer;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("rutConverter")
@BypassInterceptors
@Converter
public class RutConverter implements javax.faces.convert.Converter {

	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		String digitos = "";
		String verificador = "";
		try {
			String rut = value.toString();
			StringTokenizer st = new StringTokenizer(rut, "-");
			digitos = st.nextToken();
			verificador = st.nextToken();
		} catch (Exception e) {
		}
		String rut = "";
		if (digitos.isEmpty() || verificador.isEmpty()) {
			rut = value.toString();
		} else {
			rut = digitos + "-" + verificador.toUpperCase();
		}
		return rut;
	}

	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		String digitos = "";
		String verificador = "";
		try {
			String rut = value.toString();
			StringTokenizer st = new StringTokenizer(rut, "-");
			digitos = st.nextToken();
			verificador = st.nextToken();
		} catch (Exception e) {
		}
		String rut = "";
		if (digitos.isEmpty() || verificador.isEmpty()) {
			rut = value.toString();
		} else {
			rut = digitos + "-" + verificador.toUpperCase();
		}
		return rut;
	}

}
