package cl.exe.exedoc.util.validators;

import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("rutValidator")
@BypassInterceptors
@Validator
public class RutValidator implements javax.faces.validator.Validator {

	private static final long serialVersionUID = 4942004636826109270L;

	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		int digitos = 0;
		char verificador = '0';
		String rut = value.toString();
		try {
			StringTokenizer st = new StringTokenizer(rut, "-");
			digitos = Integer.parseInt(st.nextToken());
			String verificadores = st.nextToken();
			if (verificadores.length() == 1) {
				verificador = verificadores.charAt(0);
			} else {
				String detail = "Rut ingresado no válido. No puede tener más de un caracter el Dígito Verificador";
				String summary = "El Rut no es válido. Ej: 12123123-1";
				throw new ValidatorException(makeMessage(detail, summary));
			}
		} catch (Exception e) {
			throw new ValidatorException(makeMessage("El Rut no es válido. Ej: 12123123-1", null));
		}
		char dv = validaRut(digitos);
		if (Character.toUpperCase(dv) != Character.toUpperCase(verificador)) {
			throw new ValidatorException(makeMessage("El Rut ingresado no es válido", null));
		}
	}

	public char validaRut(int rut) {
		int M = 0, S = 1, T = rut;
		for (; T != 0; T /= 10) {
			S = (S + T % 10 * (9 - M++ % 6)) % 11;
		}
		char dv = (char) (S != 0 ? S + 47 : 75);
		return dv;
	}

	private FacesMessage makeMessage(String detail, String summary) {
		if (summary == null) {
			summary = detail;
		}
		FacesMessage message = new FacesMessage();
		message.setDetail(detail);
		message.setSummary(summary);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		return message;
	}

}
