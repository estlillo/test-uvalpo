package cl.exe.exedoc.util.ws;

import javax.ejb.Remote;
import javax.persistence.EntityManager;

@Remote
public interface Usuario {

	Boolean validaUsuario(String usuario);

	EntityManager getEntityManager(String persistenceUnit);

}
