package cl.exe.exedoc.util.ws;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;

@Name(UsuarioBean.NAME)
public @Stateless class UsuarioBean implements Usuario {

	public final static String NAME = "UsuarioBean";
	
	@PersistenceContext
	private EntityManager em;
	
	public EntityManagerFactory emf;
	
	@Override
	public Boolean validaUsuario(String usuario){
		
		
		String sql = "Select count(p.id) FROM Persona p WHERE p.usuario=:usuario";
		
		Query query = em.createQuery(sql);
		query.setParameter("usuario", usuario);
		
		Long cont = (Long) query.getSingleResult();
		
		if (cont > 0)
			return true;
		return false;
	}

	/**
     * @return {@link EntityManager}
     */
	@Override
    public EntityManager getEntityManager(String persistenceUnit) {
        if (emf == null)
            emf = Persistence.createEntityManagerFactory(persistenceUnit);
        em = emf.createEntityManager();
        return em;
    }

}
