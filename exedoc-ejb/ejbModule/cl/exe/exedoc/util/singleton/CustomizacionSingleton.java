package cl.exe.exedoc.util.singleton;

import java.util.Properties;

import org.apache.log4j.Logger;

import cl.exe.exedoc.util.ReadProperties;

/**
 * @author Pablo
 *
 */
public class CustomizacionSingleton {
	
	private static final String AMERICA = "America";

	private static Logger logger = Logger.getLogger(CustomizacionSingleton.class);
	
	private static CustomizacionSingleton instance;
	private static Properties properties;
	
	private String zonahoraria;
	private Integer horainicio;
	private Integer intervalo;
	
	private Long defaultTimeout;
	
	private boolean enviadorCorreo;
	private String notificacionEnviadorCorreo;
	
	static {
		instance = new CustomizacionSingleton();
		instance.setEnviadorCorreo(true);
		instance.setNotificacionEnviadorCorreo(null);
		instance.setHorainicio(7);
		instance.setIntervalo(24);
		try {
			properties = ReadProperties.read("exedoc/customizacion.properties");
			instance.setZonahoraria(properties.getProperty("timezone", AMERICA));
			
			if (properties.getProperty("email_activo") != null && 
					!properties.getProperty("email_activo").equalsIgnoreCase("y")) {
	        	  instance.setEnviadorCorreo(false);
			}
			if (properties.getProperty("email_notificacion") != null && 
					properties.getProperty("email_notificacion").trim().length() > 0) {
				instance.setNotificacionEnviadorCorreo(properties.getProperty("email_notificacion").trim());
			}
			
			try {
				instance.setHorainicio(new Integer(properties.getProperty("horainicio", "7")));
			} catch (NumberFormatException e) {
				logger.error("El valor \"horainicio\" debe ser numerico.");
				instance.setHorainicio(7);
			}
			try {
				instance.setIntervalo(new Integer(properties.getProperty("intervalo", "24")));
			} catch (NumberFormatException e) {
				logger.error("El valor \"intervalo\" debe ser numerico.");
				instance.setIntervalo(24);
			}

		} catch (NullPointerException e) {
			logger.error("El archivo no se encontro o no se pudo leer");
			instance.setZonahoraria(AMERICA);
		}
		
		
	}
	
	/**
	 * Constructor.
	 */
	public CustomizacionSingleton() {
		super();
	}
	
	/**
	 * @return {@link CustomizacionSingleton}
	 */
	public static CustomizacionSingleton getInstance() {
		return instance;
	}

	/**
	 * @param key {@link String}
	 * @return {@link String}
	 */
	public String getPropiedad(final String key) {
		return properties.getProperty(key);
	}
	
	/**
	 * @return {@link String}
	 */
	public String getZonahoraria() {
		return zonahoraria;
	}
	/**
	 * @param zonahoraria {@link String}
	 */
	public void setZonahoraria(final String zonahoraria) {
		this.zonahoraria = zonahoraria;
	}
	
	/**
	 * @return {@link Integer}
	 */
	public Integer getHorainicio() {
		return horainicio;
	}
	/**
	 * @param horainicio {@link Integer}
	 */
	public void setHorainicio(final Integer horainicio) {
		this.horainicio = horainicio;
	}
	
	public Long getDefaultTimeout() {
		return defaultTimeout;
	}

	public void setDefaultTimeout(Long defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
	}
	
	public boolean getEnviadorCorreo() {
		return enviadorCorreo;
	}
	
	public void setEnviadorCorreo(boolean enviadorCorreo) {
		this.enviadorCorreo = enviadorCorreo;
	}
	
	public String getNotificacionEnviadorCorreo() {
		return notificacionEnviadorCorreo;
	}
	
	public void setNotificacionEnviadorCorreo(String notificacionEnviadorCorreo) {
		this.notificacionEnviadorCorreo = notificacionEnviadorCorreo;
	}
	
	public Integer getIntervalo() {
		return intervalo;
	}
	
	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}
	
}
