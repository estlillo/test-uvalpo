package cl.exe.exedoc.util.singleton;

import java.util.Properties;

import org.apache.log4j.Logger;

import cl.exe.exedoc.util.ReadProperties;

/**
 * @author Administrator
 */
public class AlfrescoSingleton {
	
	/**
	 * USERNAME.
	 */
	public static final String USERNAME;
	
	/**
	 * PASSWORD.
	 */
	public static final String PASSWORD;
	/**
	 * SERVICE_URL.
	 */
	public static final String SERVICE_URL;
	/**
	 * DIRECTORIOBASEBINARIO.
	 */
	public static final String DIRECTORIOBASEBINARIO;
	/**
	 * DIRECTORIOBASELECTRONICO.
	 */
	public static final String DIRECTORIOBASELECTRONICO;
	/**
	 * DIRECTORIOBASEANEXOS.
	 */
	public static final String DIRECTORIOBASEANEXOS;

	private static Logger logger = Logger.getLogger(AlfrescoSingleton.class);

	private static AlfrescoSingleton instance;
	private static Properties properties;

	

	static {
		instance = new AlfrescoSingleton();
		try {
			properties = ReadProperties.read("alfresco/alfresco.properties");

			USERNAME = properties.getProperty("username");
			PASSWORD = properties.getProperty("password");
			SERVICE_URL = properties.getProperty("serviceURL");
			DIRECTORIOBASEBINARIO = properties.getProperty("directorioBaseBinarios");
			DIRECTORIOBASELECTRONICO = properties.getProperty("directorioBaseElectronicos");
			DIRECTORIOBASEANEXOS = properties.getProperty("directorioBaseAnexos");

			logger.info("Acceso Alfresco: username = " + USERNAME + ", password = "
					+ PASSWORD + ", serviceURL = " + SERVICE_URL);
		} catch (NullPointerException e) {
			logger.error("No se encontro archivo de configuracion de Alfresco.");
			throw new RuntimeException();
		}
	}

	/**
	 * Constructor.
	 */
	public AlfrescoSingleton() {
		super();
	}

	/**
	 * @return {@link AlfrescoSingleton}
	 */
	public static AlfrescoSingleton getInstance() {
		return instance;
	}

	/**
	 * @return {@link Properties}
	 */
	public Properties getProperties() {
		return properties;
	}

}
