package cl.exe.exedoc.util;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import cl.exe.exedoc.entity.TipoDocumento;

public class TipoDocumentosList {

	private static TipoDocumentosList tipoDocumentosList;
	private static TipoDocumentosList tipoDocumentosElectronico;
	private static TipoDocumentosList tipoDocumentosNoElectronico;
	private static List<TipoDocumento> listaTipoDocumentoList;
	private static List<TipoDocumento> listaTipoDocumentoElectronico;
	private static List<TipoDocumento> listaTipoDocumentoElectronicoComboCreacion;
	private static List<TipoDocumento> listaTipoDocumentoNoElectronico;

	public static final String TXT_PAPEL = "Papel";
	public static final String TXT_DIGITALIZADO = "Digitalizado";
	public static final String TXT_ELECTRONICO = "Electr\u00f3nico";
	public static final Integer PAPEL = 100;
	public static final Integer DIGITALIZADO = 200;

	/**
	 * Constructor.
	 */
	public TipoDocumentosList() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param em {@link EntityManager}
	 */
	private TipoDocumentosList(final EntityManager em) {
		this.restartTipoDocumentosList(em);
		this.restartTipoDocumentoElectronico(em);
		this.restartTipoDocumentoNoElectronico(em);
		this.restartTipoDocumentoElectronicoComboCreacion(em);
	}

	/**
	 * @param em {@link EntityManager}
	 */
	@SuppressWarnings("unchecked")
	public void restartTipoDocumentosList(final EntityManager em) {
		final Query query = em.createNamedQuery("tipoDocumentoActivo.tipoDocumento");
		TipoDocumentosList.listaTipoDocumentoList = query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public void restartTipoDocumentoElectronico(final EntityManager em) {
		final Query query = em.createNamedQuery("tipoDocumentoActivo.yEsElectronico");
		TipoDocumentosList.listaTipoDocumentoElectronico = query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public void restartTipoDocumentoElectronicoComboCreacion(final EntityManager em) {
		final Query query = em.createNamedQuery("tipoDocumentoActivo.yEsElectronicoCreacion");
		TipoDocumentosList.listaTipoDocumentoElectronicoComboCreacion = query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public void restartTipoDocumentoNoElectronico(final EntityManager em) {
		final Query query = em.createNamedQuery("tipoDocumentoActivo.yNoEsElectronico");
		TipoDocumentosList.listaTipoDocumentoNoElectronico = query.getResultList();
	}

	public static TipoDocumentosList getInstanceTipoDocumentoList(final EntityManager em) {
		if (TipoDocumentosList.tipoDocumentosList == null) {
			TipoDocumentosList.tipoDocumentosList = new TipoDocumentosList(em);
		}
		return TipoDocumentosList.tipoDocumentosList;
	}

	public static TipoDocumentosList getInstanceTipoDocumentoElectronico(final EntityManager em) {
		if (TipoDocumentosList.tipoDocumentosElectronico == null) {
			TipoDocumentosList.tipoDocumentosElectronico = new TipoDocumentosList(em);
		}
		return TipoDocumentosList.tipoDocumentosElectronico;
	}

	public List<TipoDocumento> getTiposDocumento() {
		return listaTipoDocumentoList;
	}

	public List<SelectItem> getTiposDocumentoRespuestaList() {
		List<SelectItem> ltd = new ArrayList<SelectItem>();
		ltd.add(new SelectItem(PAPEL, TXT_PAPEL));
		ltd.add(new SelectItem(DIGITALIZADO, TXT_DIGITALIZADO));
		ltd.add(new SelectItem(JerarquiasLocal.INICIO, TXT_ELECTRONICO));
		for (SelectItem e : this.getTiposDocumentoElectronicoList(null)) {
			ltd.add(new SelectItem(e.getValue(), " - " + e.getLabel()));
		}
		return ltd;
	}

	public List<TipoDocumento> getTiposDocumentoElectronico() {
		List<TipoDocumento> ltd = new ArrayList<TipoDocumento>();
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getElectronico().equals(true)) {
				ltd.add(td);
			}
		}
		return ltd;
	}

	public List<TipoDocumento> getTiposDocumentoPapel() {
		List<TipoDocumento> ltd = new ArrayList<TipoDocumento>();
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getElectronico().equals(false)) {
				ltd.add(td);
			}
		}
		return ltd;
	}

	/**
	 * Metodo que retorna lista con los tipos de documentos.
	 * 
	 * @param textoInicial {@link String}
	 * @return {@link List} of {@link SelectItem}
	 */
	public List<SelectItem> getTiposDocumentoList(final String textoInicial) {
		final List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem(JerarquiasLocal.INICIO, textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (!td.getElectronico()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}

		return tipos;
	}

	public List<SelectItem> getTiposDocumentoListVisible(String textoInicial) {
		List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getVisible()
			// &&
			// !td.getId().equals(TipoDocumento.CONSULTA) && !td.getId().equals(TipoDocumento.RESPUESTA_CONSULTA)
			) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;
	}

	public List<SelectItem> getTiposDocumentoElectronicoVisible(String textoInicial) {
		List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoElectronico) {
			if (td.getVisible()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;
	}

	public List<SelectItem> getTiposDocumentoElectronicoVisibleComboCreacion(String textoInicial) {
		final List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}

		for (TipoDocumento td : listaTipoDocumentoElectronicoComboCreacion) {
			if (td.getVisible()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;
	}
	public List<SelectItem> getTiposDocumentoNoElectronicoVisible(String textoInicial) {
		List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem(JerarquiasLocal.INICIO, textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoNoElectronico) {
			if (td.getVisible()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;
	}

	/**
	 * Metodo para cargar tipos de documentos electronicos.
	 * 
	 * @param textoInicial {@link String}
	 * @return {@link List} of {@link SelectItem}
	 */
	public List<SelectItem> getTiposDocumentoElectronicoList(final String textoInicial) {
		final List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getElectronico() != null && td.getElectronico()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;

	}

	public List<SelectItem> getTiposDocumentoElectronicoListTab(String textoInicial) {
		return getTiposDocumentoElectronicoListTab(textoInicial, true);
	}

	/**
	 * Metodo que construye la lista de documentos electronicos.
	 * 
	 * @param textoInicial {@link String}
	 * @param verSolicitudes {@link Boolean}
	 * @return {@link List} of {@link SelectItem}
	 */	public List<SelectItem> getTiposDocumentoElectronicoListTab(final String textoInicial, final boolean verSolicitudes) {

		final List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getElectronico() != null && td.getElectronico()) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion().toString()));
			}
		}
		return tipos;

	}

	public List<SelectItem> getTiposDocumentoPapelList(String textoInicial) {
		List<SelectItem> tipos = new ArrayList<SelectItem>();
		if (textoInicial != null) {
			tipos.add(new SelectItem("0", textoInicial));
		}
		for (TipoDocumento td : listaTipoDocumentoList) {
			if (td.getElectronico().equals(false)) {
				tipos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return tipos;
	}

}
