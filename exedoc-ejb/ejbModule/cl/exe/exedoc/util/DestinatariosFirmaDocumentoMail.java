package cl.exe.exedoc.util;

import java.util.Set;

import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.Persona;

public class DestinatariosFirmaDocumentoMail {
	
	DocumentoExpediente docExp;
	Set<Persona> destinatarios;
	FirmaDocumento firma;
	
	public FirmaDocumento getFirma() {
		return firma;
	}
	public void setFirma(FirmaDocumento firma) {
		this.firma = firma;
	}
	public DocumentoExpediente getDocExp() {
		return docExp;
	}
	public void setDocExp(DocumentoExpediente docExp) {
		this.docExp = docExp;
	}
	public Set<Persona> getDestinatarios() {
		return destinatarios;
	}
	public void setDestinatarios(Set<Persona> destinatarios) {
		this.destinatarios = destinatarios;
	}
}
