package cl.exe.exedoc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Validator;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("mailValidator")
@BypassInterceptors
@Validator
public class MailValidator implements javax.faces.validator.Validator {

	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String email = value.toString().trim();
//&& !email.trim().isEmpty()
		if (email != null ) {
			Pattern p = Pattern.compile("^[a-zA-Z0-9._%+-]+\\@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$");
			Matcher m = p.matcher(email);
			if (!m.find()) {
				String detail = "E-Mail ingresado no válido.";
				String summary = "El correo electrónico no es válido.";
				throw new ValidatorException(makeMessage(detail, summary));
			}
		}
	}

	private FacesMessage makeMessage(String detail, String summary) {
		if (summary == null) {
			summary = detail;
		}
		FacesMessage message = new FacesMessage();
		message.setDetail(detail);
		message.setSummary(summary);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		return message;
	}

}
