package cl.exe.exedoc.util;

import cl.exe.exedoc.session.exception.RUTException;

public class RutUtil {

	private int[] numero;
	private char digito;

	public final short RutUtil_LENGTH = 9;

	/**
	 * Crea un nuevo RutUtil vacio
	 */
	public RutUtil() {
		numero = new int[RutUtil_LENGTH];
	}

	/**
	 * Genera un RutUtil a partir de un string en formato xxxxxxxx-x o formato xx.xxx.xxx-x
	 * 
	 * @param rut El String representativo del RutUtil
	 * @throws RutUtilException En caso que el String del RutUtil no esta bien formado o el digito verificador no
	 *         corresponda
	 */
	public RutUtil(String rut) throws RUTException {
		this();

		int posFinal = 2;

		char dig = rut.charAt(rut.length() - 2);
		if (dig != '-') posFinal = 1;

		dig = rut.charAt(rut.length() - 1);

		if (!Character.isDigit(dig) && Character.toUpperCase(dig) != 'K') throw (new RUTException(
				"El RutUtil no está bien formado"));

		digito = dig;

		rut = rut.substring(0, rut.length() - posFinal);

		int sl = rut.length();

		if (sl > RutUtil_LENGTH + (int) (RutUtil_LENGTH / 3)) throw (new RUTException(
				"La cantidad de dígitos del RutUtil no es valido"));

		int j = 0;
		for (int i = 0; i < sl; i++) {
			char ch = rut.charAt(sl - i - 1);
			if (j != 0 && (j % 3) == 0 && ch == '.') continue;
			if (!Character.isDigit(ch)) throw (new RUTException("El RutUtil no está bien formado"));
			numero[RutUtil_LENGTH - 1 - j++] = Character.digit(ch, Character.LETTER_NUMBER);
		}

		if (Character.toUpperCase(digito) != getDigito()) throw (new RUTException(
				"El dígito verificador no corresponde:" + digito + " <> " + getDigito()));
	}

	/**
	 * Obtiene el RutUtil en formato xx.xxx.xxx-x
	 * 
	 * @return El String del RutUtil
	 */
	public String getFormated() {
		String value = new String();
		boolean escribir = false;
		int point = 0;
		for (int i = 0; i < RutUtil_LENGTH; i++) {
			if (numero[i] != 0) escribir = true;
			if (point != 0 && (point % 3) == 0) value += ".";
			if (escribir) value += Character.toUpperCase(Character.forDigit(numero[i], Character.LETTER_NUMBER));
			point++;
		}
		return (value + "-" + digito);

	}

	/**
	 * Obtiene el RutUtil en formato xxxxxxxx-x
	 * 
	 * @return El String del RutUtil
	 */
	public String toString() {
		String value = new String();
		boolean escribir = false;
		for (int i = 0; i < RutUtil_LENGTH; i++) {
			if (numero[i] != 0) escribir = true;
			if (escribir) value += Character.toUpperCase(Character.forDigit(numero[i], Character.LETTER_NUMBER));
		}
		return (value + "-" + digito);
	}

	/**
	 * Obtiene el RutUtil del contribuyente sin puntos, guiones ni digito verificador
	 * 
	 * @return el RutUtil del contribuyente sin puntos, guiones ni digito verificador
	 */
	public String getPure() {
		String value = new String();
		boolean escribir = false;
		for (int i = 0; i < RutUtil_LENGTH; i++) {
			if (numero[i] != 0) escribir = true;
			if (escribir) value += Character.toUpperCase(Character.forDigit(numero[i], Character.LETTER_NUMBER));
		}
		return (value);
	}

	/**
	 * Calcula el digito verificador del RutUtil
	 * 
	 * @return El digito verificador
	 */
	public char getDigito() {
		int mult = 2;
		int suma = 0;

		for (int i = numero.length - 1; i >= 0; i--) {
			suma += mult * numero[i];
			if (mult == 7) mult = 2;
			else mult++;
		}
		suma = suma % 11;
		switch (suma) {
			case 0:
				return ('0');
			case 1:
				return ('K');
			default:
				return (Character.toUpperCase(Character.forDigit(11 - suma, Character.LETTER_NUMBER)));
		}
	}

	/**
	 * Compara que dos RutUtils son iguales
	 * 
	 * @param obj El RutUtil contra el cual se desea comprar
	 * @return true en caso que sean iguales o false en caso contrario
	 */
	public boolean equals(Object obj) {
		if (!(obj instanceof RutUtil)) return false;
		RutUtil rutUtil = (RutUtil) obj;
		if (rutUtil.digito != digito) return false;
		for (int i = 0; i < RutUtil_LENGTH; i++)
			if (rutUtil.numero[i] != numero[i]) return false;
		return true;
	}
}
