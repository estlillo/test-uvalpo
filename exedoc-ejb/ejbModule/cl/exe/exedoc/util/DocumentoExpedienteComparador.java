package cl.exe.exedoc.util;

import java.util.Comparator;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;

public class DocumentoExpedienteComparador implements Comparator<DocumentoExpediente> {

	public int compare(DocumentoExpediente documentoExpediente1, DocumentoExpediente documentoExpediente2) {
		if (documentoExpediente1 == null && documentoExpediente2 != null) {
			return 1;
		} else if (documentoExpediente1 != null && documentoExpediente2 == null) {
			return -1;
		} else if (documentoExpediente1 == null && documentoExpediente2 == null) {
			return 0;
		} else {
			if (documentoExpediente1.getDocumento() == null && documentoExpediente2.getDocumento() != null) {
				return 1;
			} else if (documentoExpediente1.getDocumento() != null && documentoExpediente2.getDocumento() == null) {
				return -1;
			} else if (documentoExpediente1.getDocumento() == null && documentoExpediente2.getDocumento() == null) {
				return 0;
			} else { 
				Documento documento1 = documentoExpediente1.getDocumento();
				Documento documento2 = documentoExpediente2.getDocumento();
				if (documento1.getFechaCreacion() == null && documento2.getFechaCreacion() != null) {
					return 1;
				} else if (documento1.getFechaCreacion() != null && documento2.getFechaCreacion() == null) {
					return -1;
				} else if (documento1.getFechaCreacion() == null && documento2.getFechaCreacion() == null) {
					return 0;
				} else {
					return (-1 * documento1.getFechaCreacion().compareTo(documento2.getFechaCreacion()));
				}
			}	
		}
	}
}
