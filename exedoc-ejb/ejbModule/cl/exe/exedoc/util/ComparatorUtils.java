package cl.exe.exedoc.util;

import java.util.Comparator;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.RevisarDocumentos;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

/**
 * Class Comparators Utils.
 * @author Ricardo Fuentes
 *
 */
public class ComparatorUtils {

	public static Comparator<Documento> docIdComparator = new Comparator<Documento>() {

		@Override
		public int compare(final Documento d1, final Documento d2) {
			return d1.getId().intValue() - d2.getId().intValue();
		}
	};

	public static Comparator<VisacionEstructuradaDocumento> visasComparator = new Comparator<VisacionEstructuradaDocumento>() {

		@Override
		public int compare(final VisacionEstructuradaDocumento ve1, final VisacionEstructuradaDocumento ve2) {
			return ve1.getOrden().intValue() - ve2.getOrden().intValue();
		}
	};

	public static Comparator<FirmaEstructuradaDocumento> firmasComparator = new Comparator<FirmaEstructuradaDocumento>() {

		@Override
		public int compare(final FirmaEstructuradaDocumento fe1, final FirmaEstructuradaDocumento fe2) {
			return fe1.getOrden().intValue() - fe2.getOrden().intValue();
		}
	};
	
	public static Comparator<RevisarEstructuradaDocumento> revisarComparator = new Comparator<RevisarEstructuradaDocumento>() {

		@Override
		public int compare(final RevisarEstructuradaDocumento fe1, final RevisarEstructuradaDocumento fe2) {
			return fe1.getOrden().intValue() - fe2.getOrden().intValue();
		}
	};
	
	public static Comparator<RevisarDocumentos> revisarDocumentosComparatorByDate = new Comparator<RevisarDocumentos>() {

		@Override
		public int compare(final RevisarDocumentos fe1, final RevisarDocumentos fe2) {
			return fe1.getFechaFirma().compareTo(fe2.getFechaFirma());
		}
	};

	public static Comparator<ListaPersonasDocumento> compararListaPersonas = new Comparator<ListaPersonasDocumento>() {

		@Override
		public int compare(final ListaPersonasDocumento o1, final ListaPersonasDocumento o2) {
			return o1.getDestinatarioPersona().getId().compareTo(o2.getDestinatarioPersona().getId());
		}

	};

	public static Comparator<ExpedienteBandejaSalida> compararListaBandejaSalida = new Comparator<ExpedienteBandejaSalida>() {

		@Override
		public int compare(final ExpedienteBandejaSalida o1, final ExpedienteBandejaSalida o2) {
			return o2.getFechaIngreso().compareTo(o1.getFechaIngreso());
		}
	};

	public static Comparator<SelectPersonas> compararListaSelectPersonas = new Comparator<SelectPersonas>() {

		@Override
		public int compare(final SelectPersonas sp1, final SelectPersonas sp2) {
			return sp1.getPersona().getId().compareTo(sp2.getPersona().getId());
		}
	};
	
	public static Comparator<SelectItem> compararItems = new Comparator<SelectItem>() {
		public int compare(final SelectItem selectUno, final SelectItem selectDos) {
			return selectUno.getLabel().compareTo(selectDos.getLabel());
		}
	};
	public static Comparator<VisacionDocumento> visasDocumentosComparatorByDate = new Comparator<VisacionDocumento>() {
		@Override
		public int compare(final VisacionDocumento ve1, final VisacionDocumento ve2) {
			return ve1.getFechaVisacion().compareTo(ve2.getFechaVisacion());
		}
	};
	public static Comparator<VisacionDocumento> visasDocumentosComparatorByDateInvert = new Comparator<VisacionDocumento>() {
		@Override
		public int compare(final VisacionDocumento ve1, final VisacionDocumento ve2) {
			return ve2.getFechaVisacion().compareTo(ve1.getFechaVisacion());
		}
	};
	public static Comparator<FirmaDocumento> firmasDocumentosComparatorByDate = new Comparator<FirmaDocumento>() {
		@Override
		public int compare(final FirmaDocumento fe1, final FirmaDocumento fe2) {
			return fe1.getFechaFirma().compareTo(fe2.getFechaFirma());
		}
	};

	 public static Comparator<DetalleDias> detalleDiasComparator = new Comparator<DetalleDias>() {
		 public int compare(DetalleDias dd1, DetalleDias dd2) {
			 if (dd2.getFechaDia().before(dd1.getFechaDia())) {
				 return 1;
			 } else {
				 if (dd2.getFechaDia().equals(dd1.getFechaDia())) {
					 if (!dd2.getParteDia().equals(dd1.getParteDia())) {
						 if (dd2.getParteDia().equals(DetalleDias.AM) && !dd1.getParteDia().equals(DetalleDias.TODO)) {
							 return 1;
						 }
					 }
					 if ( (dd2.getParteDia().equals(dd1.getParteDia()))
							 || (dd2.getParteDia().equals(DetalleDias.TODO))
							 || (dd1.getParteDia().equals(DetalleDias.TODO)) ) {
						 return -1;
					 }
				 }
			 }
			 return 0;
		 }
	 };

	// public static Comparator<DetalleDiasVacaciones> detalleDiasVacacionesComparator = new
	// Comparator<DetalleDiasVacaciones>() {
	// public int compare(DetalleDiasVacaciones dd1, DetalleDiasVacaciones dd2) {
	// if(dd2.getFechaInicio().before(dd1.getFechaInicio())){
	// return 1;
	// }else{
	// if(dd2.getFechaInicio().equals(dd1.getFechaInicio())){
	// return -1;
	// }
	// }
	// return 0;
	// }
	// };
}
