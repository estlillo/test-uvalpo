package cl.exe.exedoc.util.bandeja;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.DatosUsuarioBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;
import cl.exe.exedoc.util.DocumentoExpedienteComparador;

@Stateless
@Name("bandeja")
public class BandejaImpl implements Bandeja {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void obtenerDatosUltimoDocumento(Expediente expediente, ExpedienteBandeja bandejaSalida,
			List<DocumentoExpediente> respuestas, List<DocumentoExpediente> originales) {

		DocumentoExpediente documentoAuxiliar = new DocumentoExpediente();

		StringBuilder jpQL = new StringBuilder(
				"select de.id as id_doc_exp, de.tipoDocumentoExpediente.id, td.descripcion as desc_td, ");
		jpQL.append("d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, d.id as id_doc, ");
		jpQL.append("fd.descripcion as desc_td, d.cmsId ");
		jpQL.append("from DocumentoExpediente de, Expediente e, Documento d, ");
		jpQL.append("TipoDocumento td, FormatoDocumento fd ");
		jpQL.append("where e.id= de.expediente.id and de.documento.id= d.id ");
		jpQL.append("and d.tipoDocumento.id= td.id and d.formatoDocumento.id= fd.id ");
		jpQL.append("and e.numeroExpediente= '" + expediente.getNumeroExpediente() + "' ");

		// logger.info("[obtenerDatosUltimoDocumento] La consulta a ejecutar es:
		// " + jpQL.toString());
		List<Object[]> arrayObject = (List<Object[]>) em.createQuery(jpQL.toString()).getResultList();
		Long idDocExp;
		Integer idTipoDocExp = null;
		String descTd = null;
		String numeroDoc = null;
		Date fechaDocOrigen = null;
		String materia = null;
		Long idDoc = null;
		String descFd = null;
		String cmsId = null;
		DocumentoExpediente docExp = null;
		Documento doc = null;

		for (Object[] o : arrayObject) {
			docExp = new DocumentoExpediente();
			doc = new Documento();
			// idDocExp = (((Long) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			idDocExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			idTipoDocExp = (Integer) o[1];
			docExp.setId((long) idDocExp);
			descTd = (String) o[2];
			numeroDoc = (String) o[3];
			fechaDocOrigen = (Date) o[4];
			materia = (String) o[5];
			// idDoc = (((BigInteger) o[6] != null) ? ((BigInteger) o[6]).longValue() : null);
			idDoc = (((Long) o[6] != null) ? ((Long) o[6]).longValue() : null);
			descFd = (String) o[7];
			cmsId = (String) o[8];

			TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
			tipoDocExp.setId(idTipoDocExp);
			docExp.setTipoDocumentoExpediente(tipoDocExp);
			TipoDocumento tipoDoc = new TipoDocumento();
			tipoDoc.setDescripcion(descTd);
			doc.setTipoDocumento(tipoDoc);
			doc.setNumeroDocumento(numeroDoc);
			doc.setFechaDocumentoOrigen(fechaDocOrigen);
			doc.setMateria(materia);
			doc.setId(idDoc);
			FormatoDocumento formatoDoc = new FormatoDocumento();
			formatoDoc.setDescripcion(descFd);
			doc.setFormatoDocumento(formatoDoc);
			doc.setCmsId(cmsId);
			// (cmsId != null) ? new byte[1] : null);
			docExp.setDocumento(doc);

			if (idTipoDocExp.equals(TipoDocumentoExpediente.RESPUESTA)) {
				respuestas.add(docExp);
			}
		}
		if (respuestas.size() == 0) {
			for (Object[] o : arrayObject) {
				docExp = new DocumentoExpediente();
				doc = new Documento();
				// idDocExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
				idDocExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
				idTipoDocExp = (Integer) o[1];
				docExp.setId((long) idDocExp);
				descTd = (String) o[2];
				numeroDoc = (String) o[3];
				fechaDocOrigen = (Date) o[4];
				materia = (String) o[5];
				// idDoc = (((BigInteger) o[6] != null) ? ((BigInteger) o[6]).longValue() : null);
				idDoc = (((Long) o[6] != null) ? ((Long) o[6]).longValue() : null);
				descFd = (String) o[7];
				cmsId = (String) o[8];

				TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
				tipoDocExp.setId(idTipoDocExp);
				docExp.setTipoDocumentoExpediente(tipoDocExp);
				TipoDocumento tipoDoc = new TipoDocumento();
				tipoDoc.setDescripcion(descTd);
				doc.setTipoDocumento(tipoDoc);
				doc.setNumeroDocumento(numeroDoc);
				doc.setFechaDocumentoOrigen(fechaDocOrigen);
				doc.setMateria(materia);
				doc.setId(idDoc);
				FormatoDocumento formatoDoc = new FormatoDocumento();
				formatoDoc.setDescripcion(descFd);
				doc.setFormatoDocumento(formatoDoc);
				doc.setCmsId(cmsId);
				// (cmsId != null) ? new byte[1] : null);
				docExp.setDocumento(doc);

				if (idTipoDocExp.equals(TipoDocumentoExpediente.ORIGINAL)) {
					originales.add(docExp);
				}
			}
			if (originales.size() != 0) {
				Collections.sort(originales, new DocumentoExpedienteComparador());
				documentoAuxiliar = originales.get(0);
				if(bandejaSalida != null && bandejaSalida.getIdDocumento() == null){ 
					bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
					bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
					bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
					bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
					bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
					bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento()
							.getDescripcion());
					bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
				}
			}
		} else {
			Collections.sort(respuestas, new DocumentoExpedienteComparador());
			documentoAuxiliar = respuestas.get(0);
			if(bandejaSalida != null && bandejaSalida.getIdDocumento() == null){
				bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
				bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
				bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
				bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
				bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
				bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento().getDescripcion());
				bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
			}
		}
		respuestas = null;
		originales = null;
	}
	
	@Override
	public List<Expediente> obtenerExpedientesHijos(final Long idPadre) {
		List<Expediente> listExpHijos = new ArrayList<Expediente>();
		Expediente exp = null;
		StringBuilder sql = new StringBuilder("select e.id, e.numeroExpediente, e.destinatario.id, ");
		sql.append("p.nombres as nombres_dest, p.apellidoPaterno as ap_pat_des, p.apellidoMaterno as ap_mat_dest, ");
		sql.append("c.descripcion as desc_cargo_dest, u.descripcion as desc_unidad_dest, ");
		sql.append("e.destinatarioHistorico.id, ");
		sql.append("p2.nombres as nombres_hist, p2.apellidoPaterno as ap_pat_hist, p2.apellidoMaterno as ap_mat_hist, ");
		sql.append("c2.descripcion as desc_cargo_hist, u2.descripcion as desc_unidad_hist, ");
		sql.append("e.fechaAcuseRecibo, e.fechaDespachoHistorico, e.fechaDespacho, e.fechaIngreso ");
		sql.append("from Expediente e ");
		sql.append("left join e.destinatario p ");
		sql.append("left join p.cargo c ");
		sql.append("left join c.unidadOrganizacional u ");
		sql.append("left join e.destinatarioHistorico p2  ");
		sql.append("left join p2.cargo c2 ");
		sql.append("left join c2.unidadOrganizacional u2  ");
		sql.append("where e.versionPadre.id= " + idPadre);

		// logger.info("[obtenerExpedientesHijos] La consulta a ejecutar es: " +
		// sql.toString());
		List<Object[]> arrayObject = em.createQuery(sql.toString()).getResultList();

		for (Object[] o : arrayObject) {
			exp = new Expediente();
			// Long idExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			Long idExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			String numeroExp = (String) o[1];
			// Long idDest = (((BigInteger) o[2] != null) ? ((BigInteger) o[2]).longValue() : null);
			Long idDest = (((Long) o[2] != null) ? ((Long) o[2]).longValue() : null);
			String nombresDest = (String) o[3];
			String apellidoPatDest = (String) o[4];
			String apellidoMatDest = (String) o[5];
			String descCargo = (String) o[6];
			String descUnidad = (String) o[7];
			// Long idDestHist = (((BigInteger) o[8] != null) ? ((BigInteger) o[8]).longValue() : null);
			Long idDestHist = (((Long) o[8] != null) ? ((Long) o[8]).longValue() : null);
			String nombresDestHist = (String) o[9];
			String apellidoPatDestHist = (String) o[10];
			String apellidoMatDestHist = (String) o[11];
			String descCargoHist = (String) o[12];
			String descUnidadHist = (String) o[13];
			Date fechaAcuseRecibo = (Date) o[14];
			Date fechaDespachoHist = (Date) o[15];
			Date fechaDespacho = (Date) o[16];
			Date fechaIngreso = (Date) o[17];

			exp.setId(idExp);
			exp.setNumeroExpediente(numeroExp);

			if (idDest != null) {
				Persona destinatario = new Persona();
				destinatario.setId(idDest);
				destinatario.setNombres(nombresDest);
				destinatario.setApellidoPaterno(apellidoPatDest);
				destinatario.setApellidoMaterno(apellidoMatDest);
				Cargo cargoDest = new Cargo();
				UnidadOrganizacional unidadDest = new UnidadOrganizacional();
				unidadDest.setDescripcion(descUnidad);
				cargoDest.setDescripcion(descCargo);
				cargoDest.setUnidadOrganizacional(unidadDest);
				destinatario.setCargo(cargoDest);
				exp.setDestinatario(destinatario);
			} else {
				Persona destinatarioHist = new Persona();
				destinatarioHist.setId(idDestHist);
				destinatarioHist.setNombres(nombresDestHist);
				destinatarioHist.setApellidoPaterno(apellidoPatDestHist);
				destinatarioHist.setApellidoMaterno(apellidoMatDestHist);
				Cargo cargoDestHist = new Cargo();
				UnidadOrganizacional unidadDestHist = new UnidadOrganizacional();
				unidadDestHist.setDescripcion(descUnidadHist);
				cargoDestHist.setDescripcion(descCargoHist);
				cargoDestHist.setUnidadOrganizacional(unidadDestHist);
				destinatarioHist.setCargo(cargoDestHist);
				exp.setDestinatarioHistorico(destinatarioHist);
			}

			exp.setFechaAcuseRecibo(fechaAcuseRecibo);
			exp.setFechaDespachoHistorico(fechaDespachoHist);
			exp.setFechaDespacho(fechaDespacho);
			exp.setFechaIngreso(fechaIngreso);

			listExpHijos.add(exp);
		}
		return listExpHijos;
	}
	
	@Override
	public List<DatosUsuarioBandejaSalida> obtenerDatosUsuariosAsociados(final List<Expediente> expedientes,
			final String codigoExpedienteActual) {
		final List<DatosUsuarioBandejaSalida> datosUsuarios = new ArrayList<DatosUsuarioBandejaSalida>();
		DatosUsuarioBandejaSalida usuario = null;
		for (Expediente expediente : expedientes) {
			if (expediente.getNumeroExpediente().equalsIgnoreCase(codigoExpedienteActual)) {
				usuario = new DatosUsuarioBandejaSalida();
				// if (!expediente.getFechaAcuseRecibo() == null) {
				if (expediente.getDestinatario() != null) {
					usuario.setDestinatarioCargo(expediente.getDestinatario().getCargo().getDescripcion());
					usuario.setDestinatarioUnidadOrganizacional(expediente.getDestinatario().getCargo()
							.getUnidadOrganizacional().getDescripcion());
					usuario.setDestinatarioUsuario(expediente.getDestinatario().getNombreApellido());
					usuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
					usuario.setFechaDespacho(expediente.getFechaDespachoHistorico());
					usuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
				} else {
					if (expediente.getDestinatarioHistorico() != null) {
						usuario.setDestinatarioCargo(expediente.getDestinatarioHistorico().getCargo().getDescripcion());
						usuario.setDestinatarioUnidadOrganizacional(expediente.getDestinatarioHistorico().getCargo()
								.getUnidadOrganizacional().getDescripcion());
						usuario.setDestinatarioUsuario(expediente.getDestinatarioHistorico().getNombreApellido());
					}
					usuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
					usuario.setFechaDespacho(expediente.getFechaDespachoHistorico());
					usuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
				}
				if (expediente.getCopia() == null || !expediente.getCopia()) {
					usuario.setCopia("DIRECTO");
				} else {
					usuario.setCopia("COPIA");
				}
				datosUsuarios.add(usuario);
			}
		}
		return datosUsuarios;
	}

}
