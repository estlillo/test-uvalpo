package cl.exe.exedoc.util.bandeja;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.pojo.expediente.DatosUsuarioBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;

@Local
public interface Bandeja {

	/**
	 * Metodo que obtiene datos del ultimo documento.
	 * 
	 * @param expediente {@link Expediente}
	 * @param bandejaSalida {@link ExpedienteBandeja}
	 * @param respuestas {@link ArrayList} of {@link DocumentoExpediente}
	 * @param originales {@link ArrayList} of {@link DocumentoExpediente}
	 */
	void obtenerDatosUltimoDocumento(Expediente expediente, ExpedienteBandeja bandejaSalida,
			List<DocumentoExpediente> respuestas, List<DocumentoExpediente> originales);

	/**
	 * Metodo que obtiene expedientes hijos.
	 * 
	 * @param idPadre {@link Long}
	 * @return {@link List} of {@link Expediente}
	 */
	List<Expediente> obtenerExpedientesHijos(final Long idPadre);

	/**
	 * Metodo que obtiene datos de los usuarios asociados.
	 * 
	 * @param expedientes {@link List} of {@link Expediente}
	 * @param codigoExpedienteActual {@link String}
	 * @return {@link List} of {@link DatosUsuarioBandejaSalida}
	 */
	List<DatosUsuarioBandejaSalida> obtenerDatosUsuariosAsociados(final List<Expediente> expedientes,
			final String codigoExpedienteActual);
}
