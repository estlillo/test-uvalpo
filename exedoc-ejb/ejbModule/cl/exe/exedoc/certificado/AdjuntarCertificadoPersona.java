package cl.exe.exedoc.certificado;

import javax.ejb.Local;

/**
 * @author Administrator
 */
@Local
public interface AdjuntarCertificadoPersona {

	/**
	 * 
	 */
	void destroy();
	
	/**
	 * @return String
	 */
	String begin();
	
	/**
	 * 
	 */
	void end();
	
	/**
	 * 
	 */
	void agregarCertificado();
	
	/**
	 * @return Long
	 */
	Long getIdPersona();
	
	/**
	 * @param idPersona Long
	 */
	void setIdPersona(Long idPersona);
	
	/**
	 * @return String
	 */
	String getKeyStorePassword();
	
	/**
	 * @param keyStorePassword String
	 */
	void setKeyStorePassword(String keyStorePassword);
	
	/**
	 * @return byte[]
	 */
	byte[] getKeyStore();
	
	/**
	 * @param keyStore byte[]
	 */
	void setKeyStore(byte[] keyStore);
	
	/**
	 * @return String
	 */
	String getKeyStoreName();
	
	/**
	 * @param keyStoreName String
	 */
	void setKeyStoreName(String keyStoreName);
	
	/**
	 * @return String
	 */
	String getContentType();
	
	/**
	 * @param contentType String
	 */
	void setContentType(String contentType);
	
	/**
	 * @return String
	 */
	String getKeyStoreAlias();
	
	/**
	 * @param keyStoreAlias String
	 */
	void setKeyStoreAlias(String keyStoreAlias);
}
