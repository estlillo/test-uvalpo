package cl.exe.exedoc.certificado;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Persona;

/**
 * Implementación de {@link AdjuntarCertificadoPersona}.
 */
@Stateful
@Name("adjuntarCertificadoPersona")
public class AdjuntarCertificadoPersonaBean implements
		AdjuntarCertificadoPersona {
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;
	
	@In
	private FacesMessages facesMessages;

	private Long idPersona;
	private String keyStorePassword;
	private byte[] keyStore;
	private String keyStoreName;
	private String contentType;
	private String keyStoreAlias;

	/**
	 * Constructor.
	 */
	public AdjuntarCertificadoPersonaBean() {
		super();
	}

	@Begin
	@Override
	public String begin() {
		return "adjuntarCertificadoPersona";
	}

	@End
	@Override
	public void agregarCertificado() {
		final Query query = em.createQuery("SELECT p FROM Persona p WHERE p.id = ?");
		query.setParameter(1, idPersona);
		final Persona persona = (Persona) query.getSingleResult();
		if (persona != null) {
			log.info(persona.getUsuario());
			if (keyStorePassword != null) {
				if (keyStoreName != null) {
					if (keyStore != null) {
						persona.setKeyStore(keyStore);
						persona.setKeyStorePassword(keyStorePassword);
						persona.setKeyStoreAlias(keyStoreAlias);
						em.merge(persona);
					} else {
						facesMessages.add("Debe agregar un archivo");
					}
				} else {
					facesMessages.add("Debe agregar un nombre de archivo");
				}
			} else {
				facesMessages.add("Debe ingresar el password");
			}
		} else {
			facesMessages.add("La Persona no Existe");
		}
		this.limpiar();
	}

	/**
	 * 
	 */
	private void limpiar() {
		idPersona = null;
		keyStorePassword = null;
		keyStore = null;
		keyStoreName = null;
		contentType = null;
	}

	@Override
	@End
	public void end() {

	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public Long getIdPersona() {
		return idPersona;
	}

	@Override
	public void setIdPersona(final Long idPersona) {
		this.idPersona = idPersona;
	}

	@Override
	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	@Override
	public void setKeyStorePassword(final String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	@Override
	public byte[] getKeyStore() {
		return keyStore;
	}

	@Override
	public void setKeyStore(final byte[] keyStore) {
		this.keyStore = keyStore;
	}

	@Override
	public String getKeyStoreName() {
		return keyStoreName;
	}

	@Override
	public void setKeyStoreName(final String keyStoreName) {
		this.keyStoreName = keyStoreName;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String getKeyStoreAlias() {
		return keyStoreAlias;
	}

	@Override
	public void setKeyStoreAlias(final String keyStoreAlias) {
		this.keyStoreAlias = keyStoreAlias;
	}

}
