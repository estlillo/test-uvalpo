package cl.exe.exedoc.dao;

import java.util.List;

import cl.exe.externos.DatosRespuestaSolicitudExterno;
import cl.exe.externos.DatosSolicitudTransparenciaManager;
import cl.exe.exedoc.entity.HistorialActualizacionTransparencia;
import cl.exe.exedoc.util.DatosSolicitudTransparencia;

public class DatosSolicitudTransparenciaDAO implements DatosSolicitudTransparenciaManager {

	@Override
	public List<DatosSolicitudTransparencia> getSolicitudesTransparenciaPendientes(HistorialActualizacionTransparencia hat) {
		InterfacesExternasTransparenciaDAO interfazDAO= new InterfacesExternasTransparenciaDAO();
		return interfazDAO.obtenerSolicitudesPendientes(hat);
	}
	
	@Override
	public Boolean setSolicitudUsuarioExterno(DatosRespuestaSolicitudExterno drse) {
		InterfacesExternasTransparenciaDAO interfazDAO= new InterfacesExternasTransparenciaDAO();
		return interfazDAO.responderSolicitudUsuarioExterno(drse);
	}
}