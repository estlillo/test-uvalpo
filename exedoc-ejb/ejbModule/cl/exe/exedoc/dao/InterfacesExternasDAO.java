package cl.exe.exedoc.dao;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.InformacionDocumento;
import cl.exe.exedoc.entity.InformacionFuncionario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.DatosSolicitud;

public class InterfacesExternasDAO /* extends AbstractPostgreDao */{
	// private static Logger m_logger =
	// LogMgr.instance().getLogger(InterfacesExternasDAO.class);
	// @Logger
	// private static Log log;

	@Logger
	private static Log log;

	private static String URL/* = "jdbc:jtds:sqlserver://192.168.3.24:1433/personal"*/;
	private static String user/* = "sgdoc"*/;
	private static String pass/* = "sgdoc"*/;
	private static String driver/* = "net.sourceforge.jtds.jdbc.Driver"*/;
	private static boolean active;
	public static final int CURSORTYPE = -10;

	// private final SessionFactory sessionFactory = getSessionFactory();
	//	
	// protected SessionFactory getSessionFactory() {
	// try {
	// return (SessionFactory) new InitialContext().lookup("SessionFactory");
	// } catch (Exception e) {
	// log.error("Could not locate SessionFactory in JNDI", e);
	// throw new IllegalStateException("Could not locate SessionFactory in
	// JNDI");
	// }
	// }
	private InputStream getArchivoStream(String nombre) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResourceAsStream(nombre);
	}
	public InterfacesExternasDAO () {
		try {
			//InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("conexion_personal.properties");
			//Properties properties = new Properties();
			//properties.load(inputStream);
			//inputStream.close();
			
			//TODO COnfigurar archivo de Propiedades
			 Properties defaultProps = new Properties();
		            defaultProps.load(getArchivoStream("customizacion.properties"));

				final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
				URL = defaultProps.getProperty("URL");
				user = defaultProps.getProperty("user");
				pass = defaultProps.getProperty("pass");
				driver = defaultProps.getProperty("driver");
				active=defaultProps.getProperty("active","true").equals("true");
			
			
			//ANtes
			
//			Properties properties=ReadProperties.read("conexion_personal.properties");
			
//			URL = properties.getProperty("URL");
//			user = properties.getProperty("user");
//			pass = properties.getProperty("pass");
//			driver = properties.getProperty("driver");
//			active=properties.getProperty("active","true").equals("true");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	public DatosSolicitud obtenerDiasAdministrativos(long idPersona) {
		if(!active) return null;
		DatosSolicitud datosDias = null;
		long init = System.currentTimeMillis();
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet ors = null;
		
		/*try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("conexion_personal.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			inputStream.close();
			
			URL = properties.getProperty("URL");
			user = properties.getProperty("user");
			pass = properties.getProperty("pass");
			driver = properties.getProperty("driver");
		} catch (IOException e1) {
			URL = "jdbc:jtds:sqlserver://192.168.3.24:1433/personal";
			user = "sgdoc";
			pass = "sgdoc";
			driver = "net.sourceforge.jtds.jdbc.Driver";
			e1.printStackTrace();
		}*/

		try {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(URL, user, pass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			String command = "{call mromo.sp_diasadm_sgdoc(?)}";
			stmt = conn.prepareCall(command);
			stmt.setLong(1, idPersona);
			ors = stmt.executeQuery();

			int idP;
			String nombrePersona;
			String apellidoPaterno;
			String apellidoMaterno;
			String calidad;
			String grado;
			// Date desde;
			// Date hasta;
			double numeroDias;
			// String opcion;
			String diagObs;
			String glosaTasig;

			while (ors.next()) {
				datosDias = new DatosSolicitud();
				idP = ors.getInt("ID_PER");
				Persona destinatario = new Persona();
				// destinatario.setIdPersona(idP);
				nombrePersona = ors.getString("NOMBRES");
				destinatario.setNombres(nombrePersona);
				apellidoPaterno = ors.getString("APAT");
				destinatario.setApellidoPaterno(apellidoPaterno);
				apellidoMaterno = ors.getString("AMAT");
				destinatario.setApellidoMaterno(apellidoMaterno);
				calidad = ors.getString("CALIDAD");
				datosDias.setCalidadJuridica(calidad.trim());
				grado = ors.getString("GRADO");
				datosDias.setGrado(grado);
				// desde= ors.getTimestamp("FEC_DESDE");
				// hasta= ors.getTimestamp("FEC_HASTA");
				numeroDias = ors.getDouble("NRO_DIAS");
				datosDias.setDiasPendientes(numeroDias);
				diagObs = ors.getString("DIAG_OBS");
				glosaTasig = ors.getString("GLOSA_TASIG");
				datosDias.setDestinatario(destinatario);
				break;
			}
			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
		} catch (SQLException ex) {
			if (log!=null && log.isDebugEnabled()) {
				log.debug("[buscarDatosMatch] SQLException: " + ex);
			}
			System.out.println(ex.getMessage());
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return datosDias;
	}

	public DatosSolicitud obtenerDiasVacaciones(long idPersona) {
		if(!active) return null;
		DatosSolicitud datosDias = null;
		long init = System.currentTimeMillis();
		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet ors = null;
		
		/*try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("conexion_personal.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			inputStream.close();
			
			URL = properties.getProperty("URL");
			user = properties.getProperty("user");
			pass = properties.getProperty("pass");
			driver = properties.getProperty("driver");
		} catch (IOException e1) {
			URL = "jdbc:jtds:sqlserver://192.168.3.24:1433/personal";
			user = "sgdoc";
			pass = "sgdoc";
			driver = "net.sourceforge.jtds.jdbc.Driver";
			e1.printStackTrace();
		}*/

		try {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(URL, user, pass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			String command = "{call mromo.sp_diavacaciones_sgdoc(?)}";
			stmt = conn.prepareCall(command);
			stmt.setLong(1, idPersona);
			ors = stmt.executeQuery();

			int idP;
			String nombrePersona;
			String apellidoPaterno;
			String apellidoMaterno;
			String calidad;
			String grado;
			// Date desde;
			// Date hasta;
			double numeroDias;
			// String opcion;
			// String diagObs;
			String glosaTasig;

			while (ors.next()) {
				datosDias = new DatosSolicitud();
				idP = ors.getInt("ID_PER");
				Persona destinatario = new Persona();
				// destinatario.setIdPersona(idP);
				nombrePersona = ors.getString("NOMBRES");
				destinatario.setNombres(nombrePersona);
				apellidoPaterno = ors.getString("APAT");
				destinatario.setApellidoPaterno(apellidoPaterno);
				apellidoMaterno = ors.getString("AMAT");
				destinatario.setApellidoMaterno(apellidoMaterno);
				calidad = ors.getString("CALIDAD");
				datosDias.setCalidadJuridica(calidad.trim());
				grado = ors.getString("GRADO");
				datosDias.setGrado(grado);
				// desde= ors.getTimestamp("FEC_DESDE");
				// hasta= ors.getTimestamp("FEC_HASTA");
				numeroDias = ors.getDouble("NRO_DIAS");
				datosDias.setDiasPendientes(numeroDias);
				// diagObs= ors.getString("DIAG_OBS");
				glosaTasig = ors.getString("GLOSA_TASIG");
				datosDias.setDestinatario(destinatario);
				break;
			}
			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
		} catch (SQLException ex) {
			if (log!=null && log.isDebugEnabled()) {
				log.debug("[buscarDatosMatch] SQLException: " + ex);
			}
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			try {
				ors.close();
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return datosDias;
	}

	public boolean setearDiasVacaciones(long idPersona, DetalleDiasVacaciones detalleDiasVacaciones, String numeroResolucion) {
		
		System.out.println("Vacaciones, ID Persona:"+idPersona+",Resolucion:"+numeroResolucion);
		System.out.println(detalleDiasVacaciones);
		
		long init = System.currentTimeMillis();
		Connection conn = null;
		CallableStatement stmt = null;
		StringTokenizer st = new StringTokenizer(numeroResolucion, "/");
		long numeroDocumentoResolucion = new Long(st.nextToken()).longValue();

		try {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(URL, user, pass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			String command = "{call mromo.sp_setvacaciones_sgdoc(?,?,?,?)}";
			stmt = conn.prepareCall(command);
			stmt.setLong(1, idPersona);
			stmt.setDate(2, new java.sql.Date(detalleDiasVacaciones.getFechaInicio().getTime()));
			stmt.setDate(3, new java.sql.Date(detalleDiasVacaciones.getFechaTermino().getTime()));
			stmt.setLong(4, numeroDocumentoResolucion);
			int i = stmt.executeUpdate();

			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init) + " status: " + i);
		} catch (SQLException ex) {
			System.out.println(ex);
			if (log!=null && log.isDebugEnabled()) {
				log.debug("[buscarDatosMatch] SQLException: " + ex);
			}
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			try {
				stmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean setearDiasAdministrativos(long idPersona, List<DetalleDias> listDetalleDias, String numeroResolucion) {
		
		System.out.println("Administrativo, ID Persona:"+idPersona+",Resolucion:"+numeroResolucion);
		System.out.println(listDetalleDias);
		
		long init = System.currentTimeMillis();
		Connection conn = null;
		CallableStatement stmt = null;
		StringTokenizer st = new StringTokenizer(numeroResolucion, "/");
		long numeroDocumentoResolucion = new Long(st.nextToken()).longValue();

		try {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(URL, user, pass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			for (DetalleDias ddv : listDetalleDias) {
				String command = "{call mromo.sp_setdiasadm_sgdoc(?,?,?,?)}";
				stmt = conn.prepareCall(command);
				stmt.setLong(1, idPersona);
				stmt.setDate(2, new java.sql.Date(ddv.getFechaDia().getTime()));
				stmt.setString(3, DetalleDias.getParteDia(ddv.getParteDia()));
				stmt.setLong(4, numeroDocumentoResolucion);
				stmt.executeUpdate();
			}
			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
		} catch (SQLException ex) {
			System.out.println(ex+":"+ex.getMessage());
			if (log!=null && log.isDebugEnabled()) {
				log.debug("[buscarDatosMatch] SQLException: " + ex);
			}
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean informarLicencias(List<InformacionDocumento> listInfo) {
		long init = System.currentTimeMillis();
		Connection conn = null;
		CallableStatement stmt = null;
		
		

		try {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(URL, user, pass);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			for(InformacionDocumento info:listInfo){
				InformacionFuncionario inf=(InformacionFuncionario)info;
				
				
				
				Date desde = inf.getDesde();
				Date hasta = inf.getHasta();
				String numeroResolucion=inf.getDocumento().getNumeroDocumento();
				StringTokenizer st = new StringTokenizer(numeroResolucion, "/");
				long numeroDocumentoResolucion = new Long(st.nextToken()).longValue();
				
				int tipoLicencia=inf.getTipoLicencia();
				Long idPersona = inf.getFuncionario().getIdPersona();
				
				String nombreMedico=inf.getNombreMedico();
				String rutMedico=inf.getRutMedico();
				
				
				String command = "{call mromo.sp_setlicencia_sgdoc(?,?,?,?,?,?,?)}";
				stmt = conn.prepareCall(command);
				stmt.setLong(1, idPersona);
				stmt.setLong(2, tipoLicencia);
				stmt.setDate(3, new java.sql.Date(desde.getTime()));
				stmt.setDate(4, new java.sql.Date(hasta.getTime()));
				stmt.setLong(5, numeroDocumentoResolucion);
				stmt.setString(6, nombreMedico);
				stmt.setString(7, rutMedico);
				stmt.executeUpdate();
			}
			
		}catch (SQLException ex) {
			System.out.println(ex+":"+ex.getMessage());
			if (log!=null && log.isDebugEnabled()) {
				log.debug("[buscarDatosMatch] SQLException: " + ex);
			}
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
}