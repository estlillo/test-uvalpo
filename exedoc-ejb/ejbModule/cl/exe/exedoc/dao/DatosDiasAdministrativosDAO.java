package cl.exe.exedoc.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.Feriados;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.externos.DatosDiasAdministrativosManager;

@Stateless
@Name("datosDiasAdministrativos")
public class DatosDiasAdministrativosDAO implements DatosDiasAdministrativosManager {

	@PersistenceContext
	private EntityManager em;

	@Override
	public boolean esDiaHabil(Date fecha) {
		long time = fecha.getTime();
		Calendar cale = Calendar.getInstance();

		cale.setTimeInMillis(time);
		int day = cale.get(Calendar.DAY_OF_WEEK);
		if (day != Calendar.SATURDAY && day != Calendar.SUNDAY && !esFeriado(fecha)) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean esFeriado(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		Query query = em.createNamedQuery("Feriados.esFeriado");
		query.setParameter("dia", cal.get(Calendar.DAY_OF_MONTH));
		query.setParameter("mes", cal.get(Calendar.MONTH) + 1);
		query.setParameter("agno", cal.get(Calendar.YEAR));
		List<Feriados> feriados = (List<Feriados>)query.getResultList();
		return (feriados.size() > 0) ? true : false;
	}
	
	@Override
	public DatosSolicitud getDiasAdministrativosPendientes(String rut) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DatosSolicitud getDiasAdministrativosPendientes(Long idPersona) {
		InterfacesExternasDAO interfazDAO= new InterfacesExternasDAO();
		return interfazDAO.obtenerDiasAdministrativos(idPersona);
	}
	
	@Override
	public DatosSolicitud getDiasVacacionesPendientes(Long idPersona) {
		InterfacesExternasDAO interfazDAO= new InterfacesExternasDAO();
		return interfazDAO.obtenerDiasVacaciones(idPersona);
	}
	
	@Override
	public boolean setDiasVacacionesPendientes(long idPersona, DetalleDiasVacaciones detalleDiasVacaciones, String numeroResolucion) {
		InterfacesExternasDAO interfazDAO= new InterfacesExternasDAO();
		return interfazDAO.setearDiasVacaciones(idPersona, detalleDiasVacaciones, numeroResolucion);
	}
	
	@Override
	public boolean setDiasAdministrativosPendientes(long idPersona, List<DetalleDias> listDetalleDias, String numeroResolucion) {
		InterfacesExternasDAO interfazDAO= new InterfacesExternasDAO();
		return interfazDAO.setearDiasAdministrativos(idPersona, listDetalleDias, numeroResolucion);
	}

	@Override
	public Integer getDiasFeriadosLegalesPendientes(String rut) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getDiasFeriadosLegalesPendientes(Long idPersona) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMediosDiasAdministrativosPendientes(String rut) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMediosDiasAdministrativosPendientes(Long idPersona) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMediosDiasFeriadosLegalesPendientes(String rut) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMediosDiasFeriadosLegalesPendientes(Long idPersona) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setDiasAdministrativosPendientes(String rut, Double diasAdministrativosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setDiasAdministrativosPendientes(Integer idPersona, Double diasAdministrativosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setDiasFeriadosLegalesPendientes(String rut, Double diasFeriadosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setDiasFeriadosLegalesPendientes(Long idPersona, Double diasFeriadosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setMediosDiasAdministrativosPendientes(String rut, Double diasAdministrativosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setMediosDiasAdministrativosPendientes(Long idPersona, Double diasAdministrativosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setMediosDiasFeriadosLegalesPendientes(String rut, Double diasFeriadosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setMediosDiasFeriadosLegalesPendientes(Long idPersona, Double diasFeriadosTomados) {
		// TODO Auto-generated method stub
		return null;
	}

}
