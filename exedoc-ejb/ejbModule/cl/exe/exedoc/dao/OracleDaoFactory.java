package cl.exe.exedoc.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class OracleDaoFactory extends DaoFactory {
	
	private DataSource dataSource = null;

	// private OracleDaoFactory odf = null;

	private OracleDaoFactory() {
		InitialContext context;
		try {
			context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:/exedocDatasource");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static OracleDaoFactory getOracleDaoFactory() {
		// if (odf == null) {
		OracleDaoFactory odf = new OracleDaoFactory();
		// }
		return odf;
	}

	public Connection getConnection() {
		if (dataSource != null) {
			try {
				return dataSource.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
