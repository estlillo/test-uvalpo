package cl.exe.exedoc.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConexionTransparenciaBD {
	private static String URL;
	private static String user;
	private static String pass;
	private static String driver;
	private static ConexionTransparenciaBD conexionTransparenciaBD;
	private static Connection connection;

	private void getDatosConeccionBDTransparencia() {
		try {
			//InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("transparencia_bd.properties");
			//Properties properties = new Properties();
			//properties.load(inputStream);
			
			//TODO Crear coneccion y properties
			Properties defaultProps = new Properties();
            defaultProps.load(getArchivoStream("customizacion.properties"));
			user = defaultProps.getProperty("username");
			pass = defaultProps.getProperty("password");
			driver = defaultProps.getProperty("driver");
			URL = defaultProps.getProperty("serviceURL");

			
//			Properties properties=ReadProperties.read("transparencia_bd.properties");
//			user = properties.getProperty("username");
//			pass = properties.getProperty("password");
//			driver = properties.getProperty("driver");
//			URL = properties.getProperty("serviceURL");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private InputStream getArchivoStream(String nombre) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResourceAsStream(nombre);
	}

	private ConexionTransparenciaBD() {
		getDatosConeccionBDTransparencia();
		try {
			creaConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void creaConnection() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		connection = DriverManager.getConnection(URL, user, pass);
	}

	public synchronized static ConexionTransparenciaBD getConexionTransparenciaBD() {
		if (conexionTransparenciaBD == null) {
			conexionTransparenciaBD = new ConexionTransparenciaBD();
		}
		return conexionTransparenciaBD;
	}

	public Connection getConnection() {
		try {
			if (connection.isClosed())
				creaConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public void cerrar() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
