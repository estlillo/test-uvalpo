package cl.exe.exedoc.dao;

import java.sql.Connection;

public class DaoFactory {
	
	public static Connection getDaoFactoryConnection() {
		return OracleDaoFactory.getOracleDaoFactory().getConnection();
	}
	
}
