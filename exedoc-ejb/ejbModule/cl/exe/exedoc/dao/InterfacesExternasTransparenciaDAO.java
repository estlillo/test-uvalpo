package cl.exe.exedoc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

import cl.exe.externos.DatosRespuestaSolicitudExterno;
import cl.exe.exedoc.entity.Ciudadano;
import cl.exe.exedoc.entity.HistorialActualizacionTransparencia;
import cl.exe.exedoc.util.DatosSolicitudTransparencia;
import cl.exe.exedoc.util.EstadoConsulta;

public class InterfacesExternasTransparenciaDAO {
	@Logger
	private static Log log;

	private Connection connection;
	private static InterfacesExternasTransparenciaDAO iet;

	public InterfacesExternasTransparenciaDAO() {
		ConexionTransparenciaBD.getConexionTransparenciaBD();
	}
	
	public static InterfacesExternasTransparenciaDAO getInterfacesExternasTransparenciaDAO(){
		if (iet == null){
			iet = new InterfacesExternasTransparenciaDAO();
		}
		return iet;
	}

	public List<DatosSolicitudTransparencia> obtenerSolicitudesPendientes(HistorialActualizacionTransparencia hat) {
		List<DatosSolicitudTransparencia> listSolicitudesTransparencia = new ArrayList<DatosSolicitudTransparencia>();
		DatosSolicitudTransparencia datosSolicitudTransparencia = null;
		long init = System.currentTimeMillis();
		connection = ConexionTransparenciaBD.getConexionTransparenciaBD().getConnection();
		java.sql.Statement stmt = null;
		ResultSet ors = null;
		Locale locale = new Locale("es", "CL");
		String sql = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);

		try {
			if (hat != null) {
				String fe = simpleDateFormat.format(hat.getFechaActualizacion());
				sql = "select ssa.folio, ssa.id_solicitud_acceso, ssa.fecha_formulacion, ssa.fecha_termino, ssa.identificacion_documentos, ";
				sql += "ssa.id_forma_recepcion, ssa.id_formato_entrega, ";
				sql += "u.nombre, u.paterno, u.materno, u.id_usuario, u.email, u.rut ";
				sql += "from sgs_solicitud_acceso ssa, usuario u ";
				sql += "where ssa.id_usuario= u.id_usuario ";
				sql += "and ssa.fecha_formulacion >= " + fe + " ";
				sql += "and ssa.id_solicitud_acceso > " + hat.getIdUltimaSolicitudObtenida() + " ";
				sql += "order by ssa.id_solicitud_acceso";

				stmt = connection.createStatement();
				ors = stmt.executeQuery(sql);
			} else {
				sql = "select ssa.folio, ssa.id_solicitud_acceso, ssa.fecha_formulacion, ssa.fecha_termino, ssa.identificacion_documentos, ";
				sql += "ssa.id_forma_recepcion, ssa.id_formato_entrega, ";
				sql += "u.nombre, u.paterno, u.materno, u.id_usuario, u.email, u.rut ";
				sql += "from sgs_solicitud_acceso ssa, usuario u ";
				sql += "where ssa.id_usuario= u.id_usuario ";
				sql += "order by ssa.id_solicitud_acceso";

				stmt = connection.createStatement();
				ors = stmt.executeQuery(sql);
			}

			while (ors.next()) {
				datosSolicitudTransparencia = new DatosSolicitudTransparencia();
				Ciudadano ciudadano = new Ciudadano();
				String folio = ors.getString("FOLIO");
				Long idSol = ors.getLong("ID_SOLICITUD_ACCESO");
				Date fechaFormulacionConsulta = ors.getTimestamp("FECHA_FORMULACION");
				Date fechaTerminoConsulta = ors.getTimestamp("FECHA_TERMINO");
				String consulta = ors.getString("IDENTIFICACION_DOCUMENTOS");
				Long idFormaRecepcion = ors.getLong("ID_FORMA_RECEPCION");
				Long idFormatoEntrega = ors.getLong("ID_FORMATO_ENTREGA");
				String nombresPersona = ors.getString("NOMBRE");
				String apellidoPaternoPersona = ors.getString("PATERNO");
				String apellidoMaternoPersona = ors.getString("MATERNO");
				Long idusuarioSgs = ors.getLong("ID_USUARIO");
				String email = ors.getString("EMAIL");
				String run = ors.getString("RUT");

				ciudadano.setNombres((nombresPersona != null) ? nombresPersona : "");
				ciudadano.setApellidoPaterno((apellidoPaternoPersona != null) ? apellidoPaternoPersona : "");
				ciudadano.setApellidoMaterno((apellidoMaternoPersona != null) ? apellidoMaternoPersona : "");
				ciudadano.setEmail(email);
				ciudadano.setIdUsuarioSgs(idusuarioSgs);
				ciudadano.setRun(run);
				String nombreCompleto = (nombresPersona != null) ? nombresPersona + " " : "";
				nombreCompleto += (apellidoPaternoPersona != null) ? apellidoPaternoPersona + " " : "";
				nombreCompleto += (apellidoMaternoPersona != null) ? apellidoMaternoPersona : "";
				datosSolicitudTransparencia.setFolio(folio);
				datosSolicitudTransparencia.setIdUltimaSolicitudObtenida(idSol);
				datosSolicitudTransparencia.setEmisor(nombreCompleto);
				datosSolicitudTransparencia.setConsulta(consulta);
				datosSolicitudTransparencia.setFechaFormulacionConsulta(fechaFormulacionConsulta);
				datosSolicitudTransparencia.setFechaTerminoConsulta(fechaTerminoConsulta);
				datosSolicitudTransparencia.setIdFormaRecepcion(idFormaRecepcion);
				datosSolicitudTransparencia.setIdFormatoEntrega(idFormatoEntrega);
				datosSolicitudTransparencia.setCiudadano(ciudadano);
				listSolicitudesTransparencia.add(datosSolicitudTransparencia);
			}
			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
		} catch (SQLException ex) {
			log.debug("[obtenerSolicitudesPendientes] SQLException: " + ex);
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			ConexionTransparenciaBD.getConexionTransparenciaBD().cerrar();
		}
		return listSolicitudesTransparencia;
	}

	public Long obtenerNumDiasFeriados(Date fechaInicio, Date fechaTermino) {
		Statement stmt = null;
		Locale locale = new Locale("es", "CL");
		String sql = null;
		ResultSet resultSet = null;
		Long numDiasFeriados = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
		long init = System.currentTimeMillis();
		connection = ConexionTransparenciaBD.getConexionTransparenciaBD().getConnection();
		System.out.println("Calculando cantidad de dias feriados...");

		try {

			if (fechaInicio != null && fechaTermino != null) {
				String fi = dateFormat.format(fechaInicio);
				String ft = dateFormat.format(fechaTermino);

				sql = "SELECT COUNT(*) FROM no_habil WHERE ";
				sql += "no_habil BETWEEN '" + fi + "' AND '" + ft + "'";

				stmt = connection.createStatement();
				resultSet = stmt.executeQuery(sql);

				resultSet.next();
				numDiasFeriados = (long) resultSet.getInt(1);

				System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
			}
		} catch (SQLException ex) {
			log.debug("[obtenerNumDiasFeriados] SQLException: " + ex);
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			ConexionTransparenciaBD.getConexionTransparenciaBD().cerrar();
		}

		return numDiasFeriados;
	}

	public Boolean responderSolicitudUsuarioExterno(DatosRespuestaSolicitudExterno drse) {
		long init = System.currentTimeMillis();
		connection = ConexionTransparenciaBD.getConexionTransparenciaBD().getConnection();
		java.sql.Statement stmt = null;
		Locale locale = new Locale("es", "CL");
		String sql = null;
		String sqlUpdate = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);

		try {
			if (drse != null) {
				String fe = simpleDateFormat.format(drse.getFechaRespuesta());

				sql = "insert into sgs_flujo_estados_solicitud ";
				sql += "(folio, id_estado_solicitud, id_estado_respuestas, fecha, id_usuario, observacion) ";
				sql += "values ('" + drse.getFolio() + "', " + drse.getIdEstadoSolicitud() + ", " + drse.getIdEstadoRespuesta() + ", ";
				sql += "'" + fe + "', " + drse.getIdUsuario() + ", '" + drse.getObservacion() + "'); ";

				sqlUpdate = "update sgs_solicitud_acceso ssa ";
				sqlUpdate += "set id_estado_solicitud = " + 13 + ", id_sub_estado_solicitud = " + 16 + " ";
				sqlUpdate += "where folio = '" + drse.getFolio() + "';";

				stmt = connection.createStatement();
				boolean a = stmt.execute(sql);
				boolean b = stmt.execute(sqlUpdate);
			}
			System.out.println("Tiempo DAO: " + (System.currentTimeMillis() - init));
		} catch (SQLException ex) {
			log.debug("[responderSolicitudUsuarioExterno] SQLException: " + ex);
			throw new RuntimeException(": " + ex.getMessage());
		} finally {
			ConexionTransparenciaBD.getConexionTransparenciaBD().cerrar();
		}
		return true;
	}

	public List<EstadoConsulta> getEstadosConsulta() {
		connection = ConexionTransparenciaBD.getConexionTransparenciaBD().getConnection();

		String sql = "SELECT s.id_estado_solicitud, s.estado_solicitud FROM sgs_estado_solicitudes s";
		Statement stmt = null;
		ResultSet rs = null;
		List<EstadoConsulta> estadosConsulta = new ArrayList<EstadoConsulta>();
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				EstadoConsulta ec = new EstadoConsulta();
				ec.setId(rs.getInt("id_estado_solicitud"));
				ec.setDescripcion(rs.getString("estado_solicitud"));
				estadosConsulta.add(ec);
			}
		} catch (SQLException e) {
			log.debug("[responderSolicitudUsuarioExterno] SQLException: " + e);
		}
		return estadosConsulta;
	}
	
	public EstadoConsulta getEstadoConsulta(int idEstado){
		connection = ConexionTransparenciaBD.getConexionTransparenciaBD().getConnection();

		String sql = "SELECT s.estado_solicitud FROM sgs_estado_solicitudes s WHERE s.id_estado_solicitud = " + idEstado;
		Statement stmt = null;
		ResultSet rs = null;
		
		EstadoConsulta ec = new EstadoConsulta();
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			if (rs.next()) {
				
				ec.setId(idEstado);
				ec.setDescripcion(rs.getString("estado_solicitud"));
				
			}
		} catch (SQLException e) {
			log.debug("[responderSolicitudUsuarioExterno] SQLException: " + e);
		}
		return ec;
	}

}