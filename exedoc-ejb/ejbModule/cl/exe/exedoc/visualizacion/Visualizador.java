package cl.exe.exedoc.visualizacion;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.repositorio.Repositorio;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.util.DocUtils;

@Stateless
@Name("visualizador")
public class Visualizador implements VisualizadorInterface {

	@EJB
	private RepositorioLocal repositorio;

	@Logger
	private Log logger;

	@PersistenceContext
	protected EntityManager em;

	public void verDocumento(Long id) {
		logger.info("visualizador busca documento #0", id);

		Documento doc = em.find(Documento.class, id);
		doc.getFirmas().size();
		doc.getVisaciones().size();
		logger.info("documento encontrado: #0", doc.getId());

		if (doc != null) verDocumento(doc);

	}

	public void verDocumento(Documento doc) {
		logger.info("visualizador busca documento #0", doc.getId());
		if (doc.getFirmas() != null) {
			doc.getFirmas().size();
		}
		if (doc.getVisaciones() != null) {
			doc.getVisaciones().size();
		}
		String contentType;
		String nombreArchivo;
		String cmsId = null;
		if (doc instanceof DocumentoBinario) {
			logger.info("es documento binario");
			ArchivoDocumentoBinario archivo = ((DocumentoBinario) doc).getArchivo();
			if (archivo != null) {
				contentType = archivo.getContentType();
				nombreArchivo = archivo.getNombreArchivo();
				cmsId = archivo.getCmsId();
				visualizarDesdeRepositorio(cmsId, contentType, nombreArchivo, false);
				return;
			}
		} else if (doc instanceof DocumentoElectronico) {
			logger.info("es documento electronico");
			contentType = "text/html"; // <- porque usamos xslt
			nombreArchivo = "documento_electronico";

			// TODO revisar pq. pregunta si es resolucion...
			if (doc instanceof Resolucion) {
				if ((((Resolucion) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON))
						|| (((Resolucion) doc).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO))
						//&& ((Resolucion) doc).getAntigua() != null && !((Resolucion) doc).getAntigua()
						&& doc.getFirmas() != null && !doc.getFirmas().isEmpty()) {
					cmsId = doc.getCmsId();
				} else {
					cmsId = null;
				}

			} else if (doc instanceof Decreto) {
				if ((((Decreto) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON))
						|| (((Decreto) doc).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO))
						&& doc.getFirmas() != null && !doc.getFirmas().isEmpty()) {
					cmsId = doc.getCmsId();
				} else {
					cmsId = null;
				}

			} else {
				cmsId = doc.getCmsId();
			}
			// cmsId = doc.getCmsId();

			if (cmsId != null) {
				boolean esContraloria = false;
				if (doc instanceof Resolucion) {
					if (((Resolucion) doc).getTipo() != null
							&& (((Resolucion) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON) || ((Resolucion) doc)
									.getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)) 
							//&& ((Resolucion) doc).getAntigua() != null && !((Resolucion) doc).getAntigua()
							) {
						esContraloria = true;
					}
				} else if (doc instanceof Decreto) {
					if (((Decreto) doc).getTipo() != null
							&& (((Decreto) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON) || ((Decreto) doc)
									.getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO))
							//&& ((Decreto) doc).getAntigua() != null && !((Decreto) doc).getAntigua()
							) {
						esContraloria = true;
					}
				}
				if (doc instanceof Resolucion) {
					visualizarDesdeRepositorio(cmsId, contentType, nombreArchivo, true, esContraloria, true, false);
				} else if (doc instanceof Decreto) {
					visualizarDesdeRepositorio(cmsId, contentType, nombreArchivo, true, esContraloria, false, true);
				} else {
					visualizarDesdeRepositorio(cmsId, contentType, nombreArchivo, true, esContraloria, false, false);
				}

			} else visualizarXmlTemporal((DocumentoElectronico) doc, nombreArchivo);
			return;
		}
		logger.info("no fue posible visualizar documento");
		FacesMessages.instance().add("no fue posible visualizar documento");
	}

	private void visualizarXmlTemporal(DocumentoElectronico doc, String nombreArchivo) {
		Repositorio repositorio = new Repositorio();
		byte[] data = repositorio.transformarDocumentoElectronico(doc, false);

		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			System.out.println(new String(data));
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.setContentType("text/html");
			response.setCharacterEncoding("utf-8");
			response.setHeader("Content-disposition", "inline; filename=\"" + nombreArchivo + "\"");
			// .replace(' ',
			// '_'));
			boolean contraloria = false;
			if (doc instanceof Resolucion) {
				if (((Resolucion) doc).getTipo() != null
						&& (((Resolucion) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON))
						|| ((Resolucion) doc).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)) {
					contraloria = true;
				}
			} else if (doc instanceof Decreto) {
				if (((Decreto) doc).getTipo() != null
						&& (((Decreto) doc).getTipo().getId().equals(TipoRazon.TOMA_RAZON))
						|| ((Decreto) doc).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)) {
					contraloria = true;
				}
			}

			if (doc instanceof Resolucion) {
				transform(data, response.getOutputStream(), contraloria, true, false);
			} else if (doc instanceof Decreto) {
				transform(data, response.getOutputStream(), contraloria, false, true);
			} else {
				transform(data, response.getOutputStream(), contraloria, false, false);
			}
			facesContext.responseComplete();
			return;
		} catch (IOException e) {
			FacesMessages.instance().add(
					"hubo problemas al tratar de visualizar documento, contactese con el administrador");
			e.printStackTrace();
		}
	}

	private void visualizarDesdeRepositorio(String cmsId, String contentType, String nombreArchivo, boolean esXML) {
		visualizarDesdeRepositorio(cmsId, contentType, nombreArchivo, esXML, false, false, false);
	}

	private void visualizarDesdeRepositorio(String cmsId, String contentType, String nombreArchivo, boolean esXML,
			boolean esContraloria, boolean esResolucion, boolean esDecreto) {
		logger.info("busca cmsId = #0", new String(cmsId));
		if (cmsId == null) {
			logger.info("cmsId es null");
			FacesMessages.instance().add("archivo no tiene id de repositorio");
			return;
		}

		// byte[] data = repositorio.recuperarArchivo(cmsId);
		byte[] data = null;
		try {
			data = repositorio.getFile(cmsId);
			//System.out.println(new String(data));
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (data == null) {
			logger.info("data es null al recuperar documento");
			FacesMessages.instance().add("no fue posible recuperar el archivo desde repositorio");
			return;
		}

		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

			response.setContentType(contentType);

			if (esXML) {
				response.setCharacterEncoding("utf-8");
				response.setHeader("Content-disposition", "inline; filename=\"" + nombreArchivo + "\"");
				// .replace(' ',
				// '_'));
				if (esResolucion) {
					transform(data, response.getOutputStream(), esContraloria, true, false);
				} else if (esDecreto) {
					transform(data, response.getOutputStream(), esContraloria, false, true);
				} else {
					transform(data, response.getOutputStream(), esContraloria, false, false);
				}
			} else {
				response.setHeader("Content-disposition", "attachment; filename=\"" + nombreArchivo + "\"");
				// .replace(' ',
				// '_'));
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
			}
			facesContext.responseComplete();
			return;
		} catch (IOException e) {
			FacesMessages.instance().add(
					"hubo problemas al tratar de visualizar documento, contactese con el administrador");
			e.printStackTrace();
		}
	}

	private void transform(byte[] xml, OutputStream out) {
		transform(xml, out, false, false, false);
	}

	private void transform(byte[] xml, OutputStream out, boolean contraloria, boolean esResolucion, boolean esDecreto) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = null;
			if (contraloria) {
				transformer = factory.newTransformer(getTemplateSource(true, esResolucion, esDecreto));
			} else {
				transformer = factory.newTransformer(getTemplateSource());
			}
			StreamSource source = new StreamSource(new ByteArrayInputStream(xml));
			StreamResult result = new StreamResult(out);
			transformer.transform(source, result);
		} catch (Exception ex) {
			FacesMessages.instance().add("no fue posible transformar xml");
			ex.printStackTrace();
		}
	}

	/**
	 * Plantilla default estandar
	 */
	private static final String xsltSource = "xslt/documentoElectronico.xsl";
	private static final String xsltSourceResolucionContraloria = "xslt/resolucionContraloria.xsl";
	private static final String xsltSourceDecretoContraloria = "xslt/documentoElectronico.xsl";

	// private static final String xsltSourceDecretoContraloria = "xslt/decretoContraloria.xsl";

	/**
	 * Desde donde leer la plantilla XSL
	 * 
	 * @return la plantilla como un Source
	 */
	private Source getTemplateSource() {
		return getTemplateSource(false, false, false);
	}

	private Source getTemplateSource(boolean contraloria, boolean esResolucion, boolean esDecreto) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ServletContext ctx = (ServletContext) facesContext.getExternalContext().getContext();
		String templatePath = null;
		if (contraloria) {
			if (esResolucion) {
				System.out.print("Plantilla contraloria false :  " + xsltSource);
				templatePath = ctx.getRealPath(xsltSourceDecretoContraloria);
			}
			if (esDecreto) {
				templatePath = ctx.getRealPath(xsltSourceDecretoContraloria);
			}
		} else {
			System.out.print("Plantilla contraloria false :  " + xsltSource);
			templatePath = ctx.getRealPath(xsltSourceDecretoContraloria);
		}
		return new StreamSource(new File(templatePath));
	}
	
	@Override
    public void verArchivoTemporal(Archivo archivo) throws IOException {
    	if (archivo.getArchivo() == null) {
    		return;
    	}
    	FacesContext facesContext = FacesContext.getCurrentInstance();
    	HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		response.setContentType(archivo.getContentType());
		response.setHeader("Content-disposition", "attachment; filename=\"" + archivo.getNombreArchivo() + "\"");
		response.getOutputStream().write(archivo.getArchivo());
		response.getOutputStream().flush();
		facesContext.responseComplete();
    }
	
	@Override
	public void verArchivoRepositorio(Archivo archivo) throws XPathExpressionException, IOException {
		if (archivo.getId() == null) {
			return;
		}
		if (!DocUtils.getFile(archivo.getId(), em)) {
			FacesMessages.instance().add(
					"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
							+ archivo.getId() + ")");
		}
	}
}
