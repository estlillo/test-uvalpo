package cl.exe.exedoc.visualizacion;

import java.io.IOException;

import javax.xml.xpath.XPathExpressionException;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;

public interface VisualizadorInterface {

	public void verDocumento(Long id);
	public void verDocumento(Documento doc);
	void verArchivoTemporal(Archivo archivo) throws IOException;
	void verArchivoRepositorio(Archivo archivo)	throws XPathExpressionException, IOException;
	
}
