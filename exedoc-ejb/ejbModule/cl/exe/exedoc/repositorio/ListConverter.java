package cl.exe.exedoc.repositorio;

import java.util.Iterator;

import org.hibernate.collection.PersistentBag;

import cl.exe.exedoc.entity.VisacionDocumento;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ListConverter extends ExeBaseConverter implements Converter {

	@SuppressWarnings("rawtypes")
	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		PersistentBag bag = (PersistentBag) obj;
		Iterator iter = bag.iterator();
		while (iter.hasNext()) {
			Object ob = iter.next();
			context.convertAnother(ob);
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class clss) {
		return clss.equals(PersistentBag.class);
	}
}
