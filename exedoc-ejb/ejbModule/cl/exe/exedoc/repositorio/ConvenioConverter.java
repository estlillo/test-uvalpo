package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.Convenio;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ConvenioConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		Convenio convenio = (Convenio) obj;
		if (convenio == null)
			return;

		convertAnotherNode("numeroDocumento", convenio.getNumeroDocumento(), writer, context);
		if (convenio.getFechaDocumentoOrigen() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			convertAnotherNode("fecha", sdf.format(convenio.getFechaDocumentoOrigen()), writer, context);
		}
		convertAnotherNode("materia", convenio.getMateria(), writer, context);
		convertAnotherNode("autor", convenio.getAutor(), writer, context);
		convertAnotherNode("distribucion", convenio.getDistribucionDocumento(), writer, context);
		convertAnotherNode("titulo", convenio.getTitulo(), writer, context);
		convertAnotherNode("prologo", convenio.getPrologo(), writer, context);

		if (convenio.getConsiderandos() != null)
			context.convertAnother(convenio.getConsiderandos());

		if (convenio.getTeniendoPresente() != null)
			context.convertAnother(convenio.getTeniendoPresente());

		if (convenio.getConvienen() != null)
			context.convertAnother(convenio.getConvienen());

		convertAnotherNode("visadoPor", convenio.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", convenio.getFirmas(), writer, context);

	}

	public boolean canConvert(Class cls) {
		return cls.equals(Convenio.class);
	}

}
