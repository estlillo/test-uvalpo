package cl.exe.exedoc.repositorio;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.session.exception.FoliadorException;

@Stateless
@Name("foliadorDocumento")
public class FoliadorDocumento implements FoliadorDocumentoLocal {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;

	public String generarFolio(Documento documento, Long numeroReservado) throws FoliadorException {
		return FoliadorDocumentoSingleton.getFoliadorDocumentoSingleton().generarFolio(documento.getTipoDocumento(), numeroReservado, em);
	}

	public String generarFolio(TipoDocumento tipoDocumento, Long numeroReservado) throws FoliadorException {
		return FoliadorDocumentoSingleton.getFoliadorDocumentoSingleton().generarFolio(tipoDocumento, numeroReservado, em);
	}

	public String generarFolioExpediente() {
		return FoliadorDocumentoSingleton.getFoliadorDocumentoSingleton().generarFolioExpediente(em);
	}

}
