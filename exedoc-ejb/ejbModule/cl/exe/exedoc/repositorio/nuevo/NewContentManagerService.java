package cl.exe.exedoc.repositorio.nuevo;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.xpath.XPathExpressionException;

import cl.exe.alfresco.client.nuevo.AbstractNewGenericAlfrescoAdapter;
import cl.exe.alfresco.client.nuevo.NewAlfrescoAdapter;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.util.singleton.AlfrescoSingleton;

/**
 * @author jnova
 *
 */
public class NewContentManagerService implements NewContentManagerServiceInterface {

    private static String username;
    private static String password;
    private static String serviceURL;
    private static String directorioBaseBinarios;
    private static String directorioBaseElectronicos;
    private static String directorioBaseAnexos;

    private NewAlfrescoAdapter alfresco;

    static {
		username = AlfrescoSingleton.USERNAME;
		password = AlfrescoSingleton.PASSWORD;
		serviceURL = AlfrescoSingleton.SERVICE_URL;
		directorioBaseBinarios = AlfrescoSingleton.DIRECTORIOBASEBINARIO;
		directorioBaseElectronicos = AlfrescoSingleton.DIRECTORIOBASELECTRONICO;
		directorioBaseAnexos = AlfrescoSingleton.DIRECTORIOBASEANEXOS;
    }

    /**
     * Constructor.
     */
    public NewContentManagerService() {
        this.alfresco = AbstractNewGenericAlfrescoAdapter.getAlfrescoAdapter(username, password, serviceURL, 
                directorioBaseBinarios, directorioBaseElectronicos, directorioBaseAnexos);
    }

    @Override
    public String uploadContent(final String filename, final Archivo file, final Documento doc) throws RemoteException {
    	String ret = null;
    	if (doc.getCmsId() == null) {
    		ret = alfresco.upload(filename, file, doc);
    	}
    	else {
    		alfresco.update(file, doc);
    		ret = doc.getCmsId();
    	}
    	return ret;
    }

    @Override
    public String uploadContent(final String filename, final byte[] file, final Documento doc) throws RemoteException {
    	String ret = null;
    	if (doc.getCmsId() == null) {
    		ret = alfresco.upload(filename, file, doc);
    	}
    	else {
    		alfresco.update(file, doc);
    		ret = doc.getCmsId();
    	}
    	return ret;
    }

    @Override
    public byte[] donwloadContent(final String url) throws IOException, XPathExpressionException {
        return alfresco.download(url);
    }

}
