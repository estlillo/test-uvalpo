/*
 * EXE Ingenieria y Software Ltda.
 * 
 */
package cl.exe.exedoc.repositorio.nuevo;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.xpath.XPathExpressionException;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;

/**
 * Interfaz para el nuevo ContentManagerService, para la nueva implementacion de
 * subida y bajada de documentos a Alfresco.
 * 
 * @author Jose Nova
 * @version 1.0 23 Mar 2011
 */
public interface NewContentManagerServiceInterface {

    /**
     * Metodo para la subida de archivos a Alfresco.
     * 
     * @param filename
     *            el nombre del archivo a subir a Alfresco.
     * @param file
     *            la instancia del Archivo en el sistema.
     * @param doc
     *            el documento asociado al archivo.
     * @return la URL.
     * 
     * @throws RemoteException
     *             si hay problemas al obtener la URL del archivo en Alfresco.
     */
    String uploadContent(String filename, Archivo file, Documento doc) throws RemoteException;
    
    /**
     * Metodo para la subida de archivos a Alfresco.
     * 
     * @param filename
     *            el nombre del archivo a subir a Alfresco.
     * @param file
     *            el archivo a subir como arreglo de bytes.
     * @param doc
     *            el documento asociado al archivo.
     * @return la URL.
     * 
     * @throws RemoteException
     *             si hay problemas al obtener la URL del archivo en Alfresco.
     */
    String uploadContent(String filename, byte[] file, Documento doc) throws RemoteException;
    
    /**
     * Metodo para la descarga de archivos desde Alfresco.
     * 
     * @param url
     *            la URL del archivo en Alfresco.
     * @return el archivo descargado, como arreglo de bytes.
     * @throws IOException
     *             si ocurre un problema ejecutando el HTTP GET.
     * @throws XPathExpressionException
     *             si ocurre un problema obteniendo el ticket.
     */
    byte[] donwloadContent(String url) throws IOException, XPathExpressionException;
}
