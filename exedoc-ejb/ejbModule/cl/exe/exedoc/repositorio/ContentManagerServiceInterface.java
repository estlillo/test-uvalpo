package cl.exe.exedoc.repositorio;

public interface ContentManagerServiceInterface {

	String storeContentBinary(String name, byte[] content, String contentType);

	String storeVersionableContentBinary(String name, byte[] content, String contentType);

	void updateContentBinary(String name, String contentType, byte[] content, byte[] id);

	byte[] recoverContentBinary(String id);

	String storeContentXml(String name, byte[] content);

	/**
	 * @param name {@link String}
	 * @param content {@link Byte}
	 * @return {@link String}
	 */
	String storeVersionableContentXML(final String name, final byte[] content);

	byte[] recoverContentXml(byte[] id);

	byte[] storeContentCierre(String name, byte[] content);

	byte[] storeContentCometidos(String name, byte[] content);

	void updateContentXML(String name, byte[] content, String id);
}
