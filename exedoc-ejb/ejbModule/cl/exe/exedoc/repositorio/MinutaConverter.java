package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Minuta;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class MinutaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		Minuta minuta = (Minuta) obj;
		if (minuta == null)
			return;
		convertAnotherNode("numeroDocumento", minuta.getNumeroDocumento(), writer, context);
		convertAnotherNode("fecha", minuta.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", minuta.getCiudad(), writer, context);
		convertAnotherNode("materia", minuta.getMateria(), writer, context);
		convertAnotherNode("reservado", minuta.getReservado(), writer, context);
		convertAnotherNode("autor", minuta.getAutor(), writer, context);
		convertAnotherNode("emisor", minuta.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", minuta.getDestinatarios(), writer, context);
		context.convertAnother(minuta.getContenido());
		convertAnotherNode("visadoPor", minuta.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", minuta.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", minuta.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", minuta.getId(), writer, context);
		convertAnotherNode("tipoDocumento", minuta.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", minuta.getTipoDocumento().getId(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Minuta.class);
	}
}
