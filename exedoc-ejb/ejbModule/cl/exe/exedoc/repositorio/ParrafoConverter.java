package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Contenido;
import cl.exe.exedoc.entity.Convienen;
import cl.exe.exedoc.entity.CuerpoActa;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Puntos;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.TeniendoPresente;
import cl.exe.exedoc.entity.Vistos;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ParrafoConverter extends ExeBaseConverter implements Converter {

		public void marshal(Object obj, HierarchicalStreamWriter writer,
				MarshallingContext context) {
			Parrafo parrafo = (Parrafo) obj;
			if (parrafo != null)
				setNodeValue("parrafo", parrafo.getCuerpo(), writer);
		}


		@SuppressWarnings("unchecked")
		public boolean canConvert(Class cls) {
			return cls.equals(Parrafo.class) || cls.equals(Contenido.class)
				|| cls.equals(Vistos.class) || cls.equals(Considerandos.class)
				|| cls.equals(TeniendoPresente.class) || cls.equals(Resuelvo.class) 
				|| cls.equals(Convienen.class) || cls.equals(Puntos.class) || cls.equals(CuerpoActa.class);
		}
}
