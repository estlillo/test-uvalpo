package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ArchivoAdjuntoDocumentoElectronicoConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		ArchivoAdjuntoDocumentoElectronico archivo = (ArchivoAdjuntoDocumentoElectronico) obj;
		if (archivo == null)
			return;
		convertAnotherNode("adjuntadoPor", archivo.getAdjuntadoPor(), writer, context);
		convertAnotherNode("contentType", archivo.getContentType(), writer, context);
		convertAnotherNode("fecha", archivo.getFecha(), writer, context);
		convertAnotherNode("materia", archivo.getMateria(), writer, context);
		convertAnotherNode("nombreArchivo", archivo.getNombreArchivo(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(ArchivoAdjuntoDocumentoElectronico.class);
	}
}
