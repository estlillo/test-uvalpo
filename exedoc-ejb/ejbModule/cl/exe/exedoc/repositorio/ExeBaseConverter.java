package cl.exe.exedoc.repositorio;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public abstract class ExeBaseConverter {

	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	public void convertAnotherNode(String nodeName, Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		writer.startNode(nodeName);
		if (obj != null) {
			context.convertAnother(obj);
		}
		else {
			context.convertAnother("");
		}
		writer.endNode();
	}

	public void setNodeValue(String nodeName, String value, HierarchicalStreamWriter writer) {
		writer.startNode(nodeName);
		if (value != null) writer.setValue(value);
		else writer.setValue("");
		writer.endNode();
	}
}
