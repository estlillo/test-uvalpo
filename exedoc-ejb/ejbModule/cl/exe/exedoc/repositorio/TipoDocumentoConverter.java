package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.TipoDocumento;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class TipoDocumentoConverter extends ExeBaseConverter implements Converter {

	@SuppressWarnings("unchecked")
	public boolean canConvert(Class clazz) {
        return clazz.equals(TipoDocumento.class);
	}

	public void marshal(Object value, HierarchicalStreamWriter writer,
                MarshallingContext context) {
       TipoDocumento tipoDoc = (TipoDocumento) value;
       if (tipoDoc != null)
    	   writer.setValue(tipoDoc.getDescripcion());
	}

	

}
