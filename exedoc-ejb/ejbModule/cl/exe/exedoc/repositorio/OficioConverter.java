package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Oficio;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class OficioConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		Oficio oficio = (Oficio) obj;
		if (oficio == null)
			return;
		convertAnotherNode("numeroDocumento", oficio.getNumeroDocumento(), writer, context);
		convertAnotherNode("fecha", oficio.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", oficio.getCiudad(), writer, context);
		convertAnotherNode("materia", oficio.getMateria(), writer, context);
		convertAnotherNode("reservado", oficio.getReservado(), writer, context);
		convertAnotherNode("autor", oficio.getAutor(), writer, context);
		convertAnotherNode("emisor", oficio.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", oficio.getDestinatarios(), writer, context);
		convertAnotherNode("distribucion", oficio.getDistribucionDocumento(), writer, context);
		convertAnotherNode("antecedentes", oficio.getAntecedentes(), writer, context);
		context.convertAnother(oficio.getContenido());
		convertAnotherNode("visadoPor", oficio.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", oficio.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", oficio.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", oficio.getId(), writer, context);
		convertAnotherNode("tipoDocumento", oficio.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", oficio.getTipoDocumento().getId(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Oficio.class);
	}
}
