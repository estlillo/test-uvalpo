package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.Resolucion;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ResolucionConverter extends ExeBaseConverter implements Converter {
	
	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Resolucion resolucion  = (Resolucion) obj;
		if (resolucion == null)
			return;
		
		convertAnotherNode("numeroDocumento", resolucion.getNumeroDocumento(), writer, context);
		convertAnotherNode("tipoRazon", resolucion.getTipo().getDescripcion(), writer, context);
		convertAnotherNode("fecha", resolucion.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", resolucion.getCiudad(), writer, context);
		convertAnotherNode("materia", resolucion.getMateria(), writer, context);
		convertAnotherNode("reservado", resolucion.getReservado(), writer, context);
		convertAnotherNode("autor", resolucion.getAutor(), writer, context);
		convertAnotherNode("emisor", resolucion.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", resolucion.getDestinatarios(), writer, context);
		convertAnotherNode("distribucion", resolucion.getDistribucionDocumentoAll(), writer, context);
		convertAnotherNode("antecedentes", resolucion.getAntecedentes(), writer, context);
		context.convertAnother(resolucion.getVistos());
		context.convertAnother(resolucion.getConsiderandos());
		context.convertAnother(resolucion.getTeniendoPresente());
		context.convertAnother(resolucion.getResuelvo());
		convertAnotherNode("indicacion", resolucion.getIndicaciones(), writer, context);
		convertAnotherNode("visadoPor",  resolucion.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", resolucion.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", resolucion.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", resolucion.getId(), writer, context);
		convertAnotherNode("tipoDocumento", resolucion.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", resolucion.getTipoDocumento().getId(), writer, context);
	}
	
	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Resolucion.class);
	}
}
