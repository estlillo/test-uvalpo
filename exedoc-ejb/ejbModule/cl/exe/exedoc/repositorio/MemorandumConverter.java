package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Memorandum;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class MemorandumConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Memorandum memo = (Memorandum) obj;
		if (memo == null)
			return;
		convertAnotherNode("numeroDocumento", memo.getNumeroDocumento(), writer, context);
		convertAnotherNode("fecha", memo.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", memo.getCiudad(), writer, context);
		convertAnotherNode("materia", memo.getMateria(), writer, context);
		convertAnotherNode("reservado", memo.getReservado(), writer, context);
		convertAnotherNode("autor", memo.getAutor(), writer, context);
		convertAnotherNode("emisor", memo.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", memo.getDestinatarios(), writer, context);
		convertAnotherNode("distribucion", memo.getDistribucionDocumento(), writer, context);
		convertAnotherNode("antecedentes", memo.getAntecedentes(), writer, context);
		context.convertAnother(memo.getContenido());
		convertAnotherNode("visadoPor",  memo.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", memo.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", memo.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", memo.getId(), writer, context);
		convertAnotherNode("tipoDocumento", memo.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", memo.getTipoDocumento().getId(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Memorandum.class);
	}
}
