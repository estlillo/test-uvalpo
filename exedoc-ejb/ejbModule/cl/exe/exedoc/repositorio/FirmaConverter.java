package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.FirmaDocumento;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class FirmaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {

		FirmaDocumento firma = (FirmaDocumento) obj;
		if (firma != null) {
			writer.startNode("firma");
			if (firma.getFechaFirma() != null) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				setNodeValue("fecha", sdf.format(firma.getFechaFirma()), writer);
			}
			convertAnotherNode("firmante", firma.getPersona(), writer, context);
			writer.endNode();
		}
	}

	@SuppressWarnings("unchecked")
	public boolean canConvert(Class cls) {
		return cls.equals(FirmaDocumento.class);
	}

}
