package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Decreto;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DecretoConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		Decreto decreto = (Decreto) obj;
		if (decreto == null)
			return;
		convertAnotherNode("numeroDocumento", decreto.getNumeroDocumento(), writer, context);
		convertAnotherNode("tipoRazon", decreto.getTipo().getDescripcion(), writer, context);
		convertAnotherNode("fecha", decreto.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", decreto.getCiudad(), writer, context);
		convertAnotherNode("materia", decreto.getMateria(), writer, context);
		convertAnotherNode("reservado", decreto.getReservado(), writer, context);
		convertAnotherNode("autor", decreto.getAutor(), writer, context);
		convertAnotherNode("emisor", decreto.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", decreto.getDestinatarios(), writer, context);
		convertAnotherNode("distribucion", decreto.getDistribucionDocumento(), writer, context);
		convertAnotherNode("antecedentes", decreto.getAntecedentes(), writer, context);
		context.convertAnother(decreto.getVistos());
		context.convertAnother(decreto.getConsiderandos());
		context.convertAnother(decreto.getTeniendoPresente());
		context.convertAnother(decreto.getResuelvo());
		convertAnotherNode("indicacion", decreto.getIndicaciones(), writer, context);
		convertAnotherNode("visadoPor", decreto.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", decreto.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", decreto.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", decreto.getId(), writer, context);
		convertAnotherNode("tipoDocumento", decreto.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", decreto.getTipoDocumento().getId(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Decreto.class);
	}
}
