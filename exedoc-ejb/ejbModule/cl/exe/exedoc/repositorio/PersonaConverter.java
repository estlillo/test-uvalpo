package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Persona;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class PersonaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Persona persona = (Persona) obj;
		if (persona == null)
			return;
		
		setNodeValue("RUT", persona.getRut(), writer);
		
		
		StringBuffer sbuf = new StringBuffer();
		if (persona.getNombres() != null)
			sbuf.append(persona.getNombres());
		if (persona.getApellidoPaterno() != null) {
			sbuf.append(' ');
			sbuf.append(persona.getApellidoPaterno());
		}
		if (persona.getApellidoMaterno() != null) {
			sbuf.append(' ');
			sbuf.append(persona.getApellidoMaterno());
		}
		setNodeValue("nombre", sbuf.toString(), writer);
		setNodeValue("iniciales", persona.getIniciales(), writer);
		setNodeValue("email", persona.getEmail(), writer);
		
		
		if (persona.getCargo() != null) {
			setNodeValue("cargo",  persona.getCargo().getDescripcion(), writer);
			if (persona.getCargo().getUnidadOrganizacional() != null) {
				if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					setNodeValue("unidadOrganizacional",  persona.getCargo().getUnidadOrganizacional().getDescripcion(), writer);
				} else if (persona.getCargo().getUnidadOrganizacional().getDepartamento() != null) {
					if (!persona.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
						setNodeValue("unidadOrganizacional",  persona.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion(), writer);
					} else {
						setNodeValue("unidadOrganizacional",  persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getDescripcion(), writer);
					}
				}
			}
		}
	}


	@SuppressWarnings("unchecked")
	public boolean canConvert(Class clazz) {
		return clazz.equals(Persona.class);
	}

}
