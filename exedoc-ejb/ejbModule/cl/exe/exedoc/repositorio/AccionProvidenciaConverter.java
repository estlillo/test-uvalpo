package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.AccionProvidencia;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class AccionProvidenciaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		AccionProvidencia accion = (AccionProvidencia) obj;
		if (accion != null)
			setNodeValue("accion", accion.getDescripcion(), writer);
	}

	
	public boolean canConvert(final Class cls) {
		return cls.equals(AccionProvidencia.class);
	}

}
