package cl.exe.exedoc.repositorio;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Folio;
import cl.exe.exedoc.entity.FolioEnFirma;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.session.exception.FoliadorException;

public class FoliadorSingleton {

	private static FoliadorSingleton foliador;
	private long numeroExpedienteAnterior;

	private FoliadorSingleton() {
	}

	public synchronized static FoliadorSingleton getFoliadorSingleton() {
		if (foliador == null) {
			foliador = new FoliadorSingleton();
		}
		return foliador;
	}

	public synchronized String generarFolio(Documento documento, Long numeroReservado, EntityManager em) throws FoliadorException {
		return generarFolio(documento.getTipoDocumento(), numeroReservado, em);
	}

	public synchronized String generarFolio(TipoDocumento tipoDocumento, Long numeroReservado, EntityManager em) throws FoliadorException {
		String numeroDocumento = "";
		if (numeroReservado == null) {
			Query query = em.createNamedQuery("Folio.findByIdTipoDocumento");
			query.setParameter("id", tipoDocumento.getId());
			Folio folio = (Folio) query.getSingleResult();
			Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
			if (!folio.getAgnoActual().equals(agnoActual)) {
				this.reiniciarFolio(folio, em);
			}
			folio.setNumeroActual(folio.getNumeroActual() + 1);
			em.merge(folio);
			numeroDocumento = folio.getNumeroActual() + "/" + folio.getAgnoActual();
		} else {
			// TODO buscar si existe folio reservado
			throw new FoliadorException("El Numero de Folio Reservado Ingresado No Existe");
		}
		return numeroDocumento;
	}

	/**
	 * Genera un codigo para el expediente.
	 * 
	 * @param em {@link EntityManager}
	 * @return codigo del folio.
	 */
	public synchronized String generarFolioExpediente(final EntityManager em) {
		final Folio folio = em.find(Folio.class, 1L);
		final Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		if (!folio.getAgnoActual().equals(agnoActual)) {
			this.reiniciarFolio(folio, em);
		}
//		if (folio.getNumeroActual().longValue() <= numeroExpedienteAnterior) {
//			folio.setNumeroActual(numeroExpedienteAnterior + 1);
//		}
//		numeroExpedienteAnterior = folio.getNumeroActual();
		folio.setNumeroActual(folio.getNumeroActual() + 1);
		em.merge(folio);
		final String numeroExpediente = "E" + folio.getNumeroActual() + "/" + folio.getAgnoActual();
		return numeroExpediente;

	}

	/**
	 * Reinicia el numero del departamento actual del folio en 1.
	 * 
	 * @param departamento
	 */
	private synchronized void reiniciarFolio(Folio folio, EntityManager em) {
		Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		folio.setAgnoActual(agnoActual);
		folio.setNumeroActual(0L);
		em.merge(folio);
	}

	public synchronized void addFolioEnFirma(Documento documento, EntityManager em) {
		FolioEnFirma fef = new FolioEnFirma();
		fef.setNumeroFolio(documento.getNumeroDocumento());
		fef.setFechaReserva(new Date());
		fef.setEnUso(false);
		fef.setIdDocumento(documento.getId());
		fef.setTipoDocumento(documento.getTipoDocumento());
		fef.setTomaRazon(Boolean.FALSE);
		if ((documento instanceof Resolucion) && ((Resolucion) documento).getTipo() != null) {
			if (((Resolucion) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON)) {
				fef.setTomaRazon(Boolean.TRUE);
			} else if (((Resolucion) documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				fef.setExentoConRegistro(Boolean.TRUE);
			}
			fef.setIdDocumentoMI(((Resolucion) documento).getIdDocumentoMI());
			fef.setAgnoNumeroFolio(((Resolucion) documento).getAgnoNumeroDocumento());
		} else if ((documento instanceof Decreto) && ((Decreto) documento).getTipo() != null) {
			if (((Decreto) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON)) {
				fef.setTomaRazon(Boolean.TRUE);
			} else if (((Decreto) documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO)) {
				fef.setExentoConRegistro(Boolean.TRUE);
			}
			fef.setIdDocumentoMI(((Decreto) documento).getIdDocumentoMI());
			fef.setAgnoNumeroFolio(((Decreto) documento).getAgnoNumeroDocumento());
		}
		em.persist(fef);
	}

	@SuppressWarnings("unchecked")
	public synchronized void usarFolioEnFirma(Documento documento, EntityManager em) {
		Query query = em.createNamedQuery("FolioEnFirma.findByIdDocumento");
		query.setParameter("idDocumento", documento.getId());
		List<FolioEnFirma> fefs = query.getResultList();
		if (fefs != null && fefs.size() != 0) {
			Collections.sort(fefs, new Comparator<FolioEnFirma>() {
				public int compare(FolioEnFirma o1, FolioEnFirma o2) {
					return o1.getId().compareTo(o2.getId());
				}
			});
			FolioEnFirma fef = fefs.get(fefs.size() - 1);
			fef.setEnUso(true);
			fef.setFechaUso(new Date());
			em.merge(fef);
			documento.setNumeroDocumento(fef.getNumeroFolio());
			if ((documento instanceof Resolucion) && ((Resolucion) documento).getTipo() != null && ((Resolucion) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON)) {
				((Resolucion) documento).setIdDocumentoMI(fef.getIdDocumentoMI());
				((Resolucion) documento).setAgnoNumeroDocumento(fef.getAgnoNumeroFolio());
			}
			if ((documento instanceof Decreto) && ((Decreto) documento).getTipo() != null && ((Decreto) documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON)) {
				((Decreto) documento).setIdDocumentoMI(fef.getIdDocumentoMI());
				((Decreto) documento).setAgnoNumeroDocumento(fef.getAgnoNumeroFolio());
			}
		}
	}

}
