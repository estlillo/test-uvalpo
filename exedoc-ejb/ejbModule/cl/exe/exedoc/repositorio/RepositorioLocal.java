package cl.exe.exedoc.repositorio;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Local;
import javax.xml.xpath.XPathExpressionException;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.Persona;

@Local
public interface RepositorioLocal {

	/**
	 * Genera y almacena en el repositorio de datos un documento electronico. Este metodo genera la representacion XML
	 * del documento, y si tiene firmas agrega las firmas electronicas al XML generado. Retorna el identificador del
	 * documento que devuelve el CMS. Opcionalmente, dependiendo del parametro copiarXML el texto XML queda almacenado
	 * en la propiedad documentoXML del documento electronico. El documento una vez almacenado en el CMS no puede ser
	 * modificado. Esta funcion no persiste el documento.
	 * 
	 * @param doc {@link DocumentoElectronico} , es el documento electronico.
	 * @param copiarXML {@link Boolean} si es true deposita en la propiedad documentoXML el texto XML generado
	 * @return {@link String} el identificador entregado por el Content Manager
	 */
	String almacenarXmlFirmado(final DocumentoElectronico doc, final boolean copiarXML);

	/**
	 * Metodo para subir un documento electronico firmado a Alfresco.
	 * 
	 * @param archivo {@link Byte} el documento XML firmado, como arreglo de bytes.
	 * @param doc {@link DocumentoElectronico} el documento electronico asociado al XML a subir a Alfresco.
	 * @param copiarXML {@link Boolean} booleano para decidir si el documento XML debe ser guardado en base de datos
	 *        aparte de subir a Alfresco.
	 * @return la URL del archivo subido en Alfresco.
	 * @throws RemoteException 
	 */
	String almacenarXmlFirmado(byte[] archivo, DocumentoElectronico doc, boolean copiarXML) throws RemoteException;

	/**
	 * Transforma un documento electronico a XML, opcionalmente firmandolo digitalmente
	 * 
	 * @param doc {@link DocumentoElectronico} es el documento a firmar
	 * @param firmar {@link Boolean} indica si va a firmar el documento
	 * @return {@link Byte} el documento xml firmado
	 */
	byte[] transformarDocumentoElectronico(DocumentoElectronico doc, boolean firmar);

	public byte[] transformarDocumentoElectronicoWS(DocumentoElectronico doc, boolean firmar);

	public String almacenarArchivo(String nombre, byte[] archivo, String contentType);

	public String almacenarArchivoVersionable(String nombre, byte[] archivo, String contentType);

	/**
	 * Metodo para almacenar un archivo en Alfresco.
	 * 
	 * @param filename el nombre del archivo.
	 * @param file la instancia de la estructura del archivo en el sistema.
	 * @param doc la instancia del documento asociado al archivo.
	 * @return la URL del archivo en Alfresco.
	 */
	String almacenarArchivo(String filename, Archivo file, Documento doc);

	public void actualizarArchivo(String nombre, String contentType, byte[] archivo, byte[] cmsId);

	public byte[] recuperarArchivo(String cmsId);

	/**
	 * Metodo para obtener un archivo desde Alfresco.
	 * 
	 * @param cmsId el cmsId de la BD.
	 * @return un arreglo de bytes, representando el archivo descargado.
	 * @throws XPathExpressionException si ocurre un problema obteniendo el ticket.
	 * @throws IOException si ocurre un problema ejecutando el HTTP GET.
	 */
	byte[] getFile(String cmsId) throws XPathExpressionException, IOException;

	public boolean visar(Documento documento, Persona visador);

	public boolean firmar(Documento documento, Persona firmador);

	public boolean firmar(Documento documento, Long idFirmador);

	public boolean editarDocumento(Documento documento);

	public boolean borrarVisaciones(Documento documento);

	public boolean borrarFirmas(Documento documento);

	// public boolean firmarFoliar(Documento documento, Persona firmador);

	public void eliminarFirmas(Documento documento, Long idFirmador);

	public byte[] almacenarDocumentosCerrados(byte[] archivo, String nombreArchivo);

	public byte[] almacenarResolucionCometido(byte[] archivo, String nombreArchivo);

	public byte[] almacenarDecretoCometido(byte[] archivo, String nombreArchivo);

	public void generarFechaFirma(Documento documento);

	public void almacenarXmlFirmaXML(DocumentoElectronico doc);

	public List<FirmaDocumento> getFirmas(Documento documento);

	boolean revisar(Documento documento, Persona aprobador, boolean aprobado, String comentario);

}
