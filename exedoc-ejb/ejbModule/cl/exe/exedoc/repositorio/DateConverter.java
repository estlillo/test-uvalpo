package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DateConverter extends ExeBaseConverter implements Converter {

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	public void marshal(Object value, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Date fecha = (Date) value;
		if (fecha != null) {
			writer.setValue(sdf.format(fecha));
		}
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Date.class);
	}
}
