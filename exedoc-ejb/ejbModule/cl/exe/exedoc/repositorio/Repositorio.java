package cl.exe.exedoc.repositorio;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.print.attribute.standard.Severity;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.RevisarDocumentos;
import cl.exe.exedoc.entity.RevisarDocumentosHistorial;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.repositorio.nuevo.NewContentManagerService;
import cl.exe.exedoc.repositorio.nuevo.NewContentManagerServiceInterface;

@Stateless
@Name("repositorio")
public class Repositorio implements RepositorioLocal {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

	@Logger
	private Log log;

	@Override
	public String almacenarXmlFirmado(final DocumentoElectronico doc,
			final boolean copiarXML) {
		final byte[] archivo = this.transformarDocumentoElectronico(doc, true);
		if (copiarXML) {
			doc.setDocumentoXML(new String(archivo));
		}

		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {
			return cms.storeVersionableContentXML(doc.getId() + "", archivo);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	@Override
	public String almacenarXmlFirmado(final byte[] archivo,
			final DocumentoElectronico doc, final boolean copiarXML) throws RemoteException {
		if (copiarXML) {
			doc.setDocumentoXML(new String(archivo));
		}
		final NewContentManagerServiceInterface cms = new NewContentManagerService();
		return cms.uploadContent(doc.getId() + "", archivo, doc);

		/*
		 * ContentManagerServiceInterface cms = new ContentManagerService(); try
		 * { return cms.storeContentXml(doc.getId() + "", archivo); } catch
		 * (Exception ex) { log.error("error al almacenarArchivo: " +
		 * ex.toString());
		 * log.info("excepcion almacenarArchivo repositorio, excepcion:",
		 * ex.getMessage()); }
		 */
//		try {
//			final NewContentManagerServiceInterface cms = new NewContentManagerService();
//			return cms.uploadContent(doc.getId() + "", archivo, doc);
//		} catch (RemoteException ex) {
//			log.error("error al almacenarArchivo: #0" + ex.getMessage());
//			FacesMessages
//					.instance()
//					.add("Documento firmado, pero no pudo subirse "
//							+ "al repositorio documental. Contacte a su administrador.",
//							Severity.ERROR);
//			ex.printStackTrace();
//		}
//		return null;
	}

	public byte[] almacenarDocumentosCerrados(final byte[] archivo,
			final String nombreArchivo) {

		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {

			return cms.storeContentCierre(nombreArchivo, archivo);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	public byte[] almacenarResolucionCometido(final byte[] archivo,
			final String nombreArchivo) {

		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {

			return cms.storeContentCometidos(nombreArchivo, archivo);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	public byte[] almacenarDecretoCometido(final byte[] archivo,
			final String nombreArchivo) {

		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {

			return cms.storeContentCometidos(nombreArchivo, archivo);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	@Override
	public byte[] transformarDocumentoElectronico(
			final DocumentoElectronico docum, final boolean firmar) {
		DocumentoElectronico doc = docum;
		try {
			doc.getFirmas();
		} catch (Exception e) {
			doc = em.find(DocumentoElectronico.class, doc.getId());
		}
		final ProcesadorXML xmlProc = new ProcesadorXML();
		xmlProc.setDocumento(doc);
		xmlProc.serializar();
		if (firmar) {
			if (doc.getFirmas() != null) {
				for (FirmaDocumento firma : doc.getFirmas()) {
					final Persona persona = firma.getPersona();
					xmlProc.agregarKeyStore(persona.getKeyStore(),
							persona.getKeyStoreAlias(),
							persona.getKeyStorePassword());
				}
			}
			xmlProc.firmar();
		}
		xmlProc.finalizar();
		return xmlProc.getRawXML();
	}

	public void almacenarXmlFirmaXML(final DocumentoElectronico doc) {
		final byte[] archivo = firmarDocumentoElectronicoXML(doc);

		doc.setDocumentoXML(new String(archivo));

		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {
			cms.updateContentXML(doc.getId() + "", archivo, doc.getCmsId());
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
	}

	private byte[] firmarDocumentoElectronicoXML(final DocumentoElectronico doc) {
		final ProcesadorXML xmlProc = new ProcesadorXML();
		xmlProc.setDocumento(doc);
		xmlProc.obtieneXMLDocumento();
		if (doc.getFirmas() != null) {
			for (FirmaDocumento firma : doc.getFirmas()) {
				final Persona persona = firma.getPersona();
				xmlProc.agregarKeyStore(persona.getKeyStore(),
						persona.getKeyStoreAlias(),
						persona.getKeyStorePassword());
			}
		}
		xmlProc.firmar();
		xmlProc.finalizar();
		return xmlProc.getRawXML();
	}

	/**
	 * Transforma un documento electronico a XML para WS, opcionalmente
	 * firmandolo digitalmente
	 * 
	 * @param doc
	 *            es el documento a firmar
	 * @param firmar
	 *            indica si va a firmar el documento
	 * @return el documento xml firmado
	 */
	public byte[] transformarDocumentoElectronicoWS(DocumentoElectronico doc,
			final boolean firmar) {
		try {
			doc.getFirmas();
			doc.getVisaciones();
		} catch (Exception e) {
			doc = em.find(DocumentoElectronico.class, doc.getId());
		}
		final ProcesadorXML xmlProc = new ProcesadorXML();
		xmlProc.setDocumento(doc);
		xmlProc.serializarWS();
		if (firmar) {
			if (doc.getFirmas() != null) {
				for (FirmaDocumento firma : doc.getFirmas()) {
					final Persona persona = firma.getPersona();
					xmlProc.agregarKeyStore(persona.getKeyStore(),
							persona.getKeyStoreAlias(),
							persona.getKeyStorePassword());
				}
			}
			xmlProc.firmar();
		}
		xmlProc.finalizar();
		return xmlProc.getRawXML();
	}

	/**
	 * Almacena un archivo en el content manager (CMS)
	 * 
	 * @param nombre
	 *            : nombre con que sera almacenado en el repositorio
	 * @param archivo
	 *            : el archivo como un arreglo de bytes
	 * @param contentType
	 *            : el tipo de contenido
	 * @return el uuid (identificador unico) del archivo
	 */
	public String almacenarArchivo(final String nombre, final byte[] archivo,
			final String contentType) {
		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {
			return cms.storeContentBinary(nombre, archivo, contentType);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	/**
	 * Almacena un archivo en el content manager (CMS), versionándolo
	 * 
	 * @param nombre
	 *            : nombre con que sera almacenado en el repositorio
	 * @param archivo
	 *            : el archivo como un arreglo de bytes
	 * @param contentType
	 *            : el tipo de contenido
	 * @return el uuid (identificador unico) del archivo
	 */
	public String almacenarArchivoVersionable(final String nombre,
			final byte[] archivo, final String contentType) {
		final ContentManagerServiceInterface cms = new ContentManagerService();
		try {
			return cms.storeVersionableContentBinary(nombre, archivo,
					contentType);
		} catch (Exception ex) {
			log.error("error al almacenarArchivo: " + ex.toString());
			log.info("excepcion almacenarArchivo repositorio, excepcion:",
					ex.getMessage());
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cl.exe.exedoc.repositorio.RepositorioLocal#almacenarArchivo(java.lang
	 * .String, cl.exe.exedoc.entity.Archivo, cl.exe.exedoc.entity.Documento)
	 */
	@Override
	public String almacenarArchivo(final String filename, final Archivo file,
			final Documento doc) {
		try {
			final NewContentManagerServiceInterface cms = new NewContentManagerService();
			return cms.uploadContent(filename, file, doc);
		} catch (RemoteException e) {
			log.error("Excepcion invocando metodo almacenarArchivo: #0",
					e.getMessage());
			FacesMessages
					.instance()
					.add("Expediente creado, pero el archivo "
							+ "no pudo subirse al repositorio documental. Contáctese con su administrador.",
							Severity.ERROR);
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Actualiza el registro de un archivo del content manager (CMS).
	 * 
	 * @param cmsId
	 *            : el uuid (identificador unico) del archivo.
	 */
	public void actualizarArchivo(final String nombre,
			final String contentType, final byte[] archivo, final byte[] cmsId) {
		try {
			final ContentManagerServiceInterface cms = new ContentManagerService();
			cms.updateContentBinary(nombre, contentType, archivo, cmsId);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	}

	/**
	 * Recupera un archivo del content manager (CMS)
	 * 
	 * @param cmsId
	 *            : el uuid (identificador unico) del archivo
	 * @return archivo almacenado
	 */
	public byte[] recuperarArchivo(final String cmsId) {
		final ContentManagerServiceInterface cms = new ContentManagerService();
		return cms.recoverContentBinary(cmsId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cl.exe.exedoc.repositorio.RepositorioLocal#getFile(java.lang.String)
	 */
	@Override
	public byte[] getFile(final String cmsId) throws XPathExpressionException,
			IOException {
		final NewContentManagerServiceInterface nCms = new NewContentManagerService();
		return nCms.donwloadContent(cmsId);
	}

	/**
	 * Visa un documento. No se chequea la autoridad de la persona para firmar.
	 * No persisten los cambios, es responsabilidad del que lo invoca.
	 * 
	 * @param documento
	 *            el documento a visar.
	 * @param visador
	 *            la persona que desea visar el documento.
	 * @return true si visó, false si la persona ya había visado el documento o
	 *         si el estado esta firmado (ya no se puede visar).
	 */
	public boolean visar(final Documento documento, final Persona visador) {
		List<VisacionDocumento> visas = this.getVisaciones(documento);

		final EstadoDocumento estado = documento.getEstado();
		if (estado != null && EstadoDocumento.FIRMADO.equals(estado.getId())) {
			return false;
		}

		if (visas == null) {
			visas = new ArrayList<VisacionDocumento>();
		}
		documento.setVisaciones(visas);

		for (VisacionDocumento vdoc : visas) {
			if (vdoc.getPersona().equals(visador)) {
				return false;
			}
		}

		final VisacionDocumento visa = new VisacionDocumento();
		visa.setDocumento(documento);
		visa.setFechaVisacion(new Date());
		visa.setPersona(visador);

		visas.add(visa);

		documento.setEstado(new EstadoDocumento(EstadoDocumento.VISADO));

		return true;
	}

	/**
	 * Firma electronicamente un documento. No se chequea la autoridad de la
	 * persona para firmar. No persisten los cambios, es responsabilidad del que
	 * lo invoca
	 * 
	 * @param documento
	 * @param firmador
	 * @return true si firmó, false si la persona ya había firmado el documento
	 *         o si el documento fue despachado.
	 */
	public boolean firmar(final Documento documento, final Persona firmador) {

		// List<FirmaDocumento> firmas = this.getFirmas(documento);

		final EstadoDocumento estado = documento.getEstado();
		if (estado != null && estado.getId().equals(EstadoDocumento.DESPACHADO)) {
			return false;
		}

		if (documento.getFirmas() == null) {
			documento.setFirmas(new ArrayList<FirmaDocumento>());
		}
		/*
		 * if (firmas == null) { firmas = new ArrayList<FirmaDocumento>(); }
		 */
		// documento.setFirmas(firmas);
		for (FirmaDocumento fdoc : documento.getFirmas())
			if (fdoc.getPersona().equals(firmador)) {
				return false;
			}
		
		Date fechaFirma=new Date();
        if(documento.getFechaDocumentoOrigen()!=null) {
            fechaFirma=documento.getFechaDocumentoOrigen();
        }

		final FirmaDocumento firma = new FirmaDocumento();
		firma.setFechaFirma(fechaFirma);
		firma.setDocumento(documento);
		firma.setPersona(firmador);
		documento.getFirmas().add(firma);

		documento.setEstado(new EstadoDocumento(EstadoDocumento.FIRMADO));

		return true;
	}

	public boolean firmar(final Documento documento, final Long idFirmador) {
		final Persona firmador = em.find(Persona.class, idFirmador);
		return firmar(documento, firmador);
	}

	//TODO CAMBIAR POR NAMED QUERY
	public void eliminarFirmas(final Documento documento, final Long idFirmador) {
		final Query query = em
				.createQuery("DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = :id and fe.persona.id = :peid");
		// Query query =
		// em.createNativeQuery("DELETE FROM FIRMAS_ESTRUCTURADAS WHERE ID_DOCUMENTO = ? and ID_PERSONA = ?");
		query.setParameter("id", documento.getId());
		query.setParameter("peid", idFirmador);
		query.executeUpdate();
	}

	public void generarFechaFirma(final Documento documento) {
		documento.setFechaDocumentoOrigen(new Date());
		em.merge(documento);
	}

	/**
	 * Retorna TRUE si firmador tiene los permisos para firmar el documento
	 * (firmador pertenece a la cadena de responsabilidad)
	 * 
	 * @param documento
	 * @param firmador
	 * @return
	 */
	public boolean autorizarFirma(final Documento documento,
			final Persona firmador) {
		return true;
	}

	/**
	 * Retorna TRUE si visador tiene los permisos para visar el documento
	 * (firmador pertenece a la cadena de responsabilidad)
	 * 
	 * @param documento
	 * @param visador
	 * @return
	 */
	public boolean autorizarVisacion(final Documento documento,
			final Persona visador) {
		return true;
	}

	/**
	 * Prepara un documento para ser editado, esta funcion debe ser invocada
	 * antes de editar un documento para saber si se puede modificar el
	 * documento. Esta funcion retorna false cuando el documento no puede ser
	 * editado más (es decir cuando ya fue firmado).
	 * 
	 * @param documento
	 * @return true si el documento es editable, false si el documento no es
	 *         editable
	 */
	public boolean editarDocumento(final Documento documento) {
		final List<FirmaDocumento> firmas = documento.getFirmas();
		if (firmas != null && firmas.size() > 0) {
			return false;
		}

		if (documento.getEstado() == null) {
			documento.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
		}
		return true;
	}

	/**
	 * elimina las visaciones de un documento. No se pueden eliminar visaciones
	 * si el documento tiene firmas.
	 * 
	 * @param documento
	 * @return true si pudo eliminar las visaciones o no tiene visaciones, si
	 *         retorna false el documento tiene firmas.
	 */
	public boolean borrarVisaciones(final Documento documento) {
		if (!editarDocumento(documento)) {
			return false;
		}

		final List<VisacionDocumento> visas = documento.getVisaciones();
		if (visas == null) {
			return true;
		}

		documento.setVisaciones(null);
		for (VisacionDocumento vdoc : visas) {
			vdoc.setDocumento(null);
			em.remove(vdoc);
		}
		documento.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
		em.persist(documento);
		return true;
	}

	/**
	 * Elimina las firmas de un documento. No elimina las visaciones si es que
	 * las hubiera. Sólo se puede eliminar las firmas si el documento no ha sido
	 * despachado.
	 * 
	 * @param documento
	 * @return true si pudo eliminar las firmas, si retorna false es porque el
	 *         documento ya fue despachado y por lo tanto no se puede eliminar
	 *         la firma.
	 */
	public boolean borrarFirmas(final Documento documento) {

		final EstadoDocumento estado = documento.getEstado();
		if (estado != null && estado.getId() == EstadoDocumento.DESPACHADO) {
			return false;
		}

		final List<FirmaDocumento> firmas = documento.getFirmas();
		if (firmas == null) {
			return true;
		}

		documento.setFirmas(null);
		for (FirmaDocumento fdoc : firmas) {
			fdoc.setDocumento(null);
			em.remove(fdoc);
		}

		final List<VisacionDocumento> visas = documento.getVisaciones();
		if (visas != null && visas.size() > 0) {
			documento.setEstado(new EstadoDocumento(EstadoDocumento.VISADO));
		} else {
			documento.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
		}

		em.merge(documento);
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<FirmaDocumento> getFirmas(final Documento documento) {
		if (documento.getId() != null) {
			final Query query = em
					.createQuery("select f from FirmaDocumento f where f.documento = ?");
			query.setParameter(1, documento);
			return query.getResultList();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private List<VisacionDocumento> getVisaciones(final Documento documento) {
		if (documento.getId() != null) {
			final Query query = em
					.createQuery("select v from VisacionDocumento v where v.documento = ?");
			query.setParameter(1, documento);
			return query.getResultList();
		}
		return null;
	}

	/**
	 * @param documento
	 *            {@link Documento}
	 * @return {@link List} of {@link RevisarDocumentos}
	 */
	@SuppressWarnings("unchecked")
	private List<RevisarDocumentos> getRevisar(final Documento documento) {
		if (documento.getId() != null) {
			final Query query = em
					.createQuery("select v from RevisarDocumentos v where v.documento = ?");
			query.setParameter(1, documento);
			return query.getResultList();
		}
		return null;
	}

	private void guardarHistorialRevisor(RevisarDocumentos revisar) {

		RevisarDocumentosHistorial revisarHistorial = new RevisarDocumentosHistorial(
				revisar);
		try {
			em.persist(revisarHistorial);
		} catch (Exception e) {
			log.error("Error al persistir el historial de revision.", e);
		}
	}

	@Override
	public boolean revisar(final Documento documento, final Persona aprobador,
			final boolean aprobado, final String comentario) {

		List<RevisarDocumentos> revisar = this.getRevisar(documento);

		final EstadoDocumento estado = documento.getEstado();
		if (estado != null
				&& EstadoDocumento.BORRADOR_APROBADO.equals(estado.getId())) {
			return false;
		}

		if (revisar == null) {
			revisar = new ArrayList<RevisarDocumentos>();
		}
		// documento.setRevisar(revisar);
		RevisarDocumentos rev = new RevisarDocumentos();

		for (RevisarDocumentos rdoc : revisar) {
			if (rdoc.getPersona().equals(aprobador)) {
				rev = rdoc;
			}
		}

		if (rev.getId() != null) {
			guardarHistorialRevisor(rev);
			rev.setFechaFirma(new Date());
			rev.setAprobado(aprobado);
			rev.setComentario(comentario);
		} else {
			rev.setDocumento(documento);
			rev.setFechaFirma(new Date());
			rev.setPersona(aprobador);
			rev.setAprobado(aprobado);
			rev.setComentario(comentario);
		}

		revisar.add(rev);

		documento.setRevisar(revisar);

		return true;
	}

}
