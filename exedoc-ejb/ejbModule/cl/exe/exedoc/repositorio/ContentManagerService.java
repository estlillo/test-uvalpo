package cl.exe.exedoc.repositorio;

import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.PASSWORD;
import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.SERVICE_URL;
import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.USERNAME;

import org.apache.log4j.Logger;

import cl.exe.alfresco.client.GenericAlfrescoAdapter;
import cl.exe.alfresco.client.support.AlfrescoAdapterException;

/**
 * @author Administrator
 */
public class ContentManagerService implements ContentManagerServiceInterface {

	private static final String NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS = "No se puede guardar el documentos ";
	private static final String NO_SE_PUEDE_LEER_EL_CONTENIDO_DEL_ID = "No se puede leer el contenido del id ";
	private static final String TEXT_XML = "text/xml";
	private static final String UTF_8 = "UTF-8";
	private static final String BINARY_SPACE = "Binarios";
	private static final String XML_SPACE = "Electronicos";
	private static final String CIERRE_SPACE = "Cierres";
	private static final String COMETIDOS_SPACE = "Cometidos";
	
	private static Logger logger = Logger.getLogger(ContentManagerService.class);

	private GenericAlfrescoAdapter alfresco;

	/**
	 * Constructor.
	 */
	public ContentManagerService() {
		alfresco = GenericAlfrescoAdapter.getAlfrescoAdapter(USERNAME, PASSWORD, SERVICE_URL);
	}

	@Override
	public byte[] recoverContentBinary(final String id) {
		try {
			final String uuid = new String(id);
			return alfresco.readContent(uuid);
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_LEER_EL_CONTENIDO_DEL_ID + id, ex);
		}
		return null;
	}

	@Override
	public String storeContentBinary(final String name, final byte[] content, final String contentType) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, BINARY_SPACE, contentType, UTF_8, content);
			return uuid;
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public String storeVersionableContentBinary(final String name, final byte[] content, final String contentType) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, BINARY_SPACE, contentType, UTF_8, content);
			alfresco.makeVersionableContent(uuid);
			return uuid;
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public void updateContentBinary(final String name, final String contentType, 
			final byte[] content, final byte[] id) {
		try {
			final String uuid = new String(id);
			alfresco.updateContent(contentType, UTF_8, content, uuid);
		} catch (AlfrescoAdapterException ex) {
			logger.error("No se puede actualizar el documentos " + name, ex);
		}
	}

	@Override
	public String storeContentXml(final String name, final byte[] content) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, XML_SPACE, TEXT_XML, UTF_8, content);
			return uuid;
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public void updateContentXML(final String name, final byte[] content, final String id) {
		try {
			final String uuid = new String(id);
			alfresco.updateContent(TEXT_XML, UTF_8, content, uuid);
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
	}

	@Override
	public String storeVersionableContentXML(final String name, final byte[] content) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, XML_SPACE, TEXT_XML, UTF_8, content);
			alfresco.makeVersionableContent(uuid);
			return uuid;
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public byte[] storeContentCierre(final String name, final byte[] content) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, CIERRE_SPACE, TEXT_XML, UTF_8, content);
			return uuid.getBytes();
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public byte[] storeContentCometidos(final String name, final byte[] content) {
		try {
			final String uuid = alfresco.writeContentInSpace(name, COMETIDOS_SPACE, TEXT_XML, UTF_8, content);
			return uuid.getBytes();
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_GUARDAR_EL_DOCUMENTOS + name, ex);
		}
		return null;
	}

	@Override
	public byte[] recoverContentXml(final byte[] id) {
		try {
			final String uuid = new String(id);
			return alfresco.readContent(uuid);
		} catch (AlfrescoAdapterException ex) {
			logger.error(NO_SE_PUEDE_LEER_EL_CONTENIDO_DEL_ID + id, ex);
		}
		return null;
	}
}
