package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.Carta;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class CartaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Carta carta = (Carta) obj;
		if (carta == null)
			return;
		convertAnotherNode("numeroDocumento", carta.getNumeroDocumento(), writer, context);
		convertAnotherNode("fecha", carta.getFechaDocumentoOrigen(), writer, context);
		convertAnotherNode("ciudad", carta.getCiudad(), writer, context);
		convertAnotherNode("materia", carta.getMateria(), writer, context);
		convertAnotherNode("reservado", carta.getReservado(), writer, context);
		convertAnotherNode("autor", carta.getAutor(), writer, context);
		convertAnotherNode("emisor", carta.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", carta.getDestinatarios(), writer, context);
		convertAnotherNode("distribucion", carta.getDistribucionDocumento(), writer, context);
		convertAnotherNode("antecedentes", carta.getAntecedentes(), writer, context);
		context.convertAnother(carta.getContenido());
		convertAnotherNode("visadoPor",  carta.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", carta.getFirmas(), writer, context);
		convertAnotherNode("archivosAdjuntos", carta.getArchivosAdjuntos(), writer, context);
		convertAnotherNode("id", carta.getId(), writer, context);
		convertAnotherNode("tipoDocumento", carta.getTipoDocumento(), writer, context);
		convertAnotherNode("idTipoDocumento", carta.getTipoDocumento().getId(), writer, context);
	}

	@SuppressWarnings("rawtypes")
	public boolean canConvert(Class cls) {
		return cls.equals(Carta.class);
	}
}
