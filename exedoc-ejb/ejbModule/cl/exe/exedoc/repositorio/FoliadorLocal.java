package cl.exe.exedoc.repositorio;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.session.exception.FoliadorException;

/**
 * Interface Foliador Local.
 * 
 * @author 
 * 
 */
@Local
public interface FoliadorLocal {

	/**
	 * Metodo que retorna el numero de folio para un expediente.
	 * 
	 * @return {@link String}
	 */
	String generarFolioExpediente();

	void addFolioEnFirma(final Documento documento);

	void usarFolioEnFirma(final Documento documento);

	String generarFolio(final Documento documento, final Long numeroReservado)
			throws FoliadorException;

	String generarFolio(final TipoDocumento tipoDocumento,
			final Long numeroReservado) throws FoliadorException;
}
