package cl.exe.exedoc.repositorio;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.session.exception.FoliadorException;

@Stateless
@Name("foliador")
public class Foliador implements FoliadorLocal {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)
	protected EntityManager em;

	public String generarFolio(Documento documento, Long numeroReservado) throws FoliadorException {
		return FoliadorSingleton.getFoliadorSingleton().generarFolio(documento.getTipoDocumento(), numeroReservado, em);
	}

	public String generarFolio(TipoDocumento tipoDocumento, Long numeroReservado) throws FoliadorException {
		return FoliadorSingleton.getFoliadorSingleton().generarFolio(tipoDocumento, numeroReservado, em);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String generarFolioExpediente() {
		return FoliadorSingleton.getFoliadorSingleton().generarFolioExpediente(em);
	}

	public void addFolioEnFirma(Documento documento) {
		FoliadorSingleton.getFoliadorSingleton().addFolioEnFirma(documento, em);
	}

	public void usarFolioEnFirma(Documento documento) {
		FoliadorSingleton.getFoliadorSingleton().usarFolioEnFirma(documento, em);
	}

}
