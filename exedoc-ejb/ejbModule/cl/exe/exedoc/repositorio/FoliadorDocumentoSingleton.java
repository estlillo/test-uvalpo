package cl.exe.exedoc.repositorio;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FolioDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.session.exception.FoliadorException;

public class FoliadorDocumentoSingleton {

	private static FoliadorDocumentoSingleton foliador;
	private long numeroExpedienteAnterior;

	private FoliadorDocumentoSingleton() {
	}

	public synchronized static FoliadorDocumentoSingleton getFoliadorDocumentoSingleton() {
		if (foliador == null) {
			foliador = new FoliadorDocumentoSingleton();

		}
		return foliador;
	}

	public synchronized String generarFolio(Documento documento, Long numeroReservado, EntityManager em) throws FoliadorException {
		return generarFolio(documento.getTipoDocumento(), numeroReservado, em);
	}

	public synchronized String generarFolio(TipoDocumento tipoDocumento, Long numeroReservado, EntityManager em) throws FoliadorException {
		String numeroDocumento = "";
		if (numeroReservado == null) {
			Query query = em.createNamedQuery("FolioDocumento.findByIdTipoDocumento");
			query.setParameter("id", tipoDocumento.getId());
			FolioDocumento folio = (FolioDocumento) query.getSingleResult();
			Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
			if (!folio.getAgnoActual().equals(agnoActual)) {
				this.reiniciarFolio(folio, em);
			}
			folio.setNumeroActual(folio.getNumeroActual() + 1);
			em.merge(folio);
			numeroDocumento = folio.getNumeroActual() + "/" + folio.getAgnoActual();
		} else {
			// TODO buscar si existe folio reservado
			throw new FoliadorException("El Numero de Folio Reservado Ingresado No Existe");
		}
		return numeroDocumento;
	}
	
	/**
	 * Reinicia el numero del departamento actual del folio en 1.
	 * 
	 * @param departamento
	 */
	private synchronized void reiniciarFolio(FolioDocumento folio, EntityManager em) {
		Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		folio.setAgnoActual(agnoActual);
		folio.setNumeroActual(0L);
		em.merge(folio);
	}

	public synchronized FolioDocumento guardaNuevoFolio(UnidadOrganizacional unidad, TipoDocumento tipoDocumento, boolean electronico, boolean interno, EntityManager em) {
		FolioDocumento folio = new FolioDocumento();
		Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		folio.setUnidad(unidad);
		folio.setNumeroActual(0L);
		folio.setAgnoActual(agnoActual);
		folio.setTipoDocumento(tipoDocumento);
		em.persist(folio);
		return folio;
	}

	public synchronized String generarFolioExpediente(EntityManager em) {
		Query query = em.createQuery("SELECT f FROM FolioDocumento f WHERE f.unidad = null ");
		FolioDocumento folio = (FolioDocumento) query.getSingleResult();
		Integer agnoActual = (new GregorianCalendar()).get(Calendar.YEAR);
		if (!folio.getAgnoActual().equals(agnoActual)) {
			this.reiniciarFolio(folio, em);
		}
		if (folio.getNumeroActual().longValue() <= numeroExpedienteAnterior) {
			folio.setNumeroActual(numeroExpedienteAnterior + 1);
		}
		numeroExpedienteAnterior = folio.getNumeroActual();
		folio.setNumeroActual(folio.getNumeroActual() + 1);
		em.merge(folio);
		String numeroExpediente = folio.getNumeroActual() + "/" + folio.getAgnoActual();
		return numeroExpediente;
	}
	
}
