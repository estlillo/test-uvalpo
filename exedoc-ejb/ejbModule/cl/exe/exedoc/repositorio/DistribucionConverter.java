package cl.exe.exedoc.repositorio;

import cl.exe.exedoc.entity.DistribucionDocumento;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class DistribucionConverter  extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		DistribucionDocumento  persona = (DistribucionDocumento) obj;
		writer.setValue(persona.getDestinatario());
	}

	
	@SuppressWarnings("unchecked")
	public boolean canConvert(Class clazz) {
		return clazz.equals(DistribucionDocumento.class);
	}


}
