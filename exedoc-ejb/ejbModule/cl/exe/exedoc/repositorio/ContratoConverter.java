package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.Contrato;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ContratoConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		Contrato contrato = (Contrato) obj;
		if (contrato == null)
			return;

		convertAnotherNode("numeroDocumento", contrato.getNumeroDocumento(), writer, context);
		if (contrato.getFechaDocumentoOrigen() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			convertAnotherNode("fecha", sdf.format(contrato.getFechaDocumentoOrigen()), writer, context);
		}
		convertAnotherNode("materia", contrato.getMateria(), writer, context);
		convertAnotherNode("autor", contrato.getAutor(), writer, context);
		convertAnotherNode("distribucion", contrato.getDistribucionDocumento(), writer, context);
		convertAnotherNode("titulo", contrato.getTitulo(), writer, context);
		convertAnotherNode("prologo", contrato.getPrologo(), writer, context);

		if (contrato.getPuntos() != null)
			context.convertAnother(contrato.getPuntos());

		convertAnotherNode("visadoPor", contrato.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", contrato.getFirmas(), writer, context);
	}

	@SuppressWarnings("unchecked")
	public boolean canConvert(Class cls) {
		return cls.equals(Contrato.class);
	}

}
