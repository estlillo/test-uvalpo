package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.VisacionDocumento;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class VisaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer, MarshallingContext context) {
		VisacionDocumento visa = (VisacionDocumento) obj;
		writer.startNode("visacion");
		if (visa.getFechaVisacion() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			setNodeValue("fecha", sdf.format(visa.getFechaVisacion()), writer);
		}
		convertAnotherNode("visador", visa.getPersona(), writer, context);
		writer.endNode();
	}

	@SuppressWarnings("unchecked")
	public boolean canConvert(Class cls) {
		return cls.equals(VisacionDocumento.class);
	}

}
