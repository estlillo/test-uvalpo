package cl.exe.exedoc.repositorio;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.AccionProvidencia;
import cl.exe.exedoc.entity.AntecedentesDelOficio;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Contenido;
import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Convienen;
import cl.exe.exedoc.entity.CuerpoActa;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Minuta;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.Puntos;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.TeniendoPresente;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.firma.servicios.Estilo;
import cl.exe.firma.servicios.Operacion;
import cl.exe.firma.servicios.XMLDsigSvcImpl;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;

public class ProcesadorXML {

	public static final String XmlHeader = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>\n";

	private DocumentoElectronico documento;
	private ArrayList<KeyStore> keyStores = new ArrayList<KeyStore>();
	private ArrayList<String> keyStoresPasswords = new ArrayList<String>();
	private ArrayList<String> keyStoresAliases = new ArrayList<String>();

	private String xmlString;
	private byte[] xmlData;
	XMLDsigSvcImpl xmlDsigSvc = new XMLDsigSvcImpl();
	@Logger
	private Log log;

	/**
	 * Retorna la representacion XML del documento electonico
	 * 
	 * @param header
	 *            es un string a agregar como header del documento
	 * @return un string con el XML representando el documento
	 */
	public String getXML() {
		return xmlString;
	}

	/**
	 * Retorna los bytes que representan el archivo XML
	 * 
	 * @return
	 */
	public byte[] getRawXML() {
		return xmlData;
	}

	/**
	 * Define el documento a procesar
	 * 
	 * @param doc
	 */
	public void setDocumento(DocumentoElectronico doc) {
		this.documento = doc;
	}

	public boolean agregarKeyStore(byte[] rawKS, String ksAlias,
			String ksPassword) {
		try {
			if (keyStores.add(xmlDsigSvc.getKeyStore(rawKS, "PKCS12",
					ksPassword))) {
				keyStoresPasswords.add(ksPassword);
				keyStoresAliases.add(ksAlias);
			}
			return true;
		} catch (Exception ex) {
			log.error("ProcesadorXML Excepcion al agregarKeyStore : "
					+ ex.toString());
			log.error("ProcesadorXML mensaje: " + ex.getMessage());
			return false;
		}
	}

	/**
	 * Convierte el documento a XML
	 */
	public void serializar() {
		if (documento == null)
			return;

		try {
			XStream xstrm = createXmlStream();
			ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
			OutputStreamWriter writer = new OutputStreamWriter(outBytes,
					Charset.forName("UTF-8"));
			// if (documento instanceof Resolucion && ((Resolucion)
			// documento).getTipo() != null
			// && (((Resolucion)
			// documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON) ||
			// ((Resolucion)
			// documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO))
			// && ((Resolucion) documento).getAntigua() != null &&
			// !((Resolucion) documento).getAntigua()) {
			// ToXml.createXML((Resolucion) documento, writer);
			// } else if (documento instanceof Decreto && ((Decreto)
			// documento).getTipo() != null
			// && (((Decreto)
			// documento).getTipo().getId().equals(TipoRazon.TOMA_RAZON) ||
			// ((Decreto)
			// documento).getTipo().getId().equals(TipoRazon.EXENTO_CON_REGISTRO))
			// && ((Decreto) documento).getAntigua() != null && !((Decreto)
			// documento).getAntigua()) {
			// ToXml.createXML((Decreto) documento, writer);
			// } else {
			xstrm.toXML(documento, writer);
			// }
			xmlData = outBytes.toByteArray();
			xmlString = outBytes.toString("UTF-8");
		} catch (UnsupportedEncodingException ex) {
			log.error("No se pudo codificar a UTF-8");
			log.error(ex.getMessage());
		}
	}

	public void obtieneXMLDocumento() {
		if (documento == null)
			return;

		xmlString = ((DocumentoElectronico) documento).getDocumentoXML();
		xmlData = xmlString.getBytes();
	}

	/**
	 * Convierte el documento a XML para WS
	 */
	public void serializarWS() {
		if (documento == null)
			return;

		try {
			XStream xstrm = createXmlStream();
			ByteArrayOutputStream outBytes = new ByteArrayOutputStream();
			OutputStreamWriter writer = new OutputStreamWriter(outBytes,
					Charset.forName("UTF-8"));
			xstrm.toXML(documento, writer);
			xmlData = outBytes.toByteArray();
			xmlString = outBytes.toString("UTF-8");
		} catch (UnsupportedEncodingException ex) {
			log.error("No se pudo codificar a UTF-8");
			log.error(ex.getMessage());
		}
	}

	/**
	 * Deja el documento listo para ser almacenado, este paso es necesario para
	 * agregar los headers de version y encoding del doumento xml
	 */
	public void finalizar() {

	}

	/**
	 * Firma usando XMLDsig el documento xml, previamente se debe serializar();
	 * 
	 * @return la cantidad de firmas aplicadas
	 */
	public int firmar() {
		if (keyStores.size() == 0)
			return 0;

		int count = 0;
		byte[] xmlFirmar = getRawXML();
		for (int i = 0; i < keyStores.size(); i++) {
			X509Certificate cert = getCertificado(i);
			PrivateKey pk = getLlavePrivada(i);
			byte[] result = firmar(cert, pk, xmlFirmar);
			if (result != null)
				xmlFirmar = result;
			count++;
		}
		xmlData = xmlFirmar;
		xmlString = new String(xmlData);
		return count;
	}

	private byte[] firmar(X509Certificate cert, PrivateKey pk, byte[] xml) {
		try {
			return xmlDsigSvc.firmar(Operacion.FIRMA, xml, null, null, cert,
					pk, null, Estilo.ENVUELTA, null);
		} catch (Exception ex) {
			log.error("ProcesadorXML: Error al firmar " + ex.toString());
			log.error("Procesador XML Excepcion: " + ex.getMessage());
			return null;
		}
	}

	private X509Certificate getCertificado(int i) {
		X509Certificate cert;
		try {
			cert = xmlDsigSvc.getCertificado(keyStores.get(i),
					keyStoresAliases.get(i));
		} catch (Exception ex) {
			log.error("ProcesadorXML: no pudo obtener certificado, excepcion: "
					+ ex.toString());
			log.error("ProcesadorXML: excepcion " + ex.getMessage());
			cert = null;

		}
		return cert;
	}

	private PrivateKey getLlavePrivada(int i) {
		PrivateKey pk;
		try {
			pk = xmlDsigSvc.getLlavePrivada(keyStores.get(i),
					keyStoresAliases.get(i), keyStoresPasswords.get(i));
		} catch (Exception ex) {
			log.error("ProcesadorXML: no pudo obtener llave privada, excepcion: "
					+ ex.toString());
			log.error("ProcesadorXML: excepcion " + ex.getMessage());
			pk = null;
		}
		return pk;
	}

	private XStream createXmlStream() {
		XStream xstrm = new XStream();

		// Registrar nuestros documentos aca:
		xstrm.alias("documento_electronico", DocumentoElectronico.class);
		// xstrm.alias("respuestaConsultaCiudadana",
		// RespuestaConsultaCiudadana.class);
		xstrm.alias("carta", Carta.class);
		xstrm.alias("minuta", Minuta.class);
		xstrm.alias("oficio", Oficio.class);
		xstrm.alias("providencia", Providencia.class);
		xstrm.alias("resolucion", Resolucion.class);
		xstrm.alias("decreto", Decreto.class);
		xstrm.alias("convenio", Convenio.class);
		xstrm.alias("contrato", Contrato.class);
		// xstrm.alias("actaAdjudicacion", ActaAdjudicacion.class);
		xstrm.alias("memorandum", Memorandum.class);
		xstrm.alias("contenido", Contenido.class);
		xstrm.alias("vistos", Vistos.class);
		xstrm.alias("considerandos", Considerandos.class);
		xstrm.alias("resuelvo", Resuelvo.class);
		xstrm.alias("teniendoPresente", TeniendoPresente.class);
		xstrm.alias("convienen", Convienen.class);
		xstrm.alias("puntos", Puntos.class);
		xstrm.alias("cuerpoActa", CuerpoActa.class);
		xstrm.alias("parrafo", Parrafo.class);
		xstrm.alias("antecedente", AntecedentesDelOficio.class);
		xstrm.alias("destinatario", DestinatarioDocumento.class);
		xstrm.alias("distribucion", DistribucionDocumento.class);
		xstrm.alias("archivo", ArchivoAdjuntoDocumentoElectronico.class);

		// registrar elementos superfluos:
		xstrm.omitField(Documento.class, "id");
		xstrm.omitField(Documento.class, "formatoDocumento");
		xstrm.omitField(Documento.class, "bitacoras");
		xstrm.omitField(Documento.class, "estado");
		xstrm.omitField(Documento.class, "fechaDocumentoOrigen");
		xstrm.omitField(Documento.class, "fechaCreacion");
		xstrm.omitField(Documento.class, "acciones");
		xstrm.omitField(Documento.class, "codigoDescarga");
		// xstrm.omitField(Documento.class, "visacionesEstructuradas");
		// xstrm.omitField(Documento.class, "firmasEstructuradas");
		xstrm.omitField(Documento.class, "listaPersonas");
		// xstrm.omitField(Documento.class, "visaciones");
		xstrm.omitField(Documento.class, "firmas");
		xstrm.omitField(ListaPersonasDocumento.class, "documento");
		xstrm.omitField(VisacionDocumento.class, "documento");
		xstrm.omitField(FirmaDocumento.class, "documento");
		xstrm.omitField(AccionProvidencia.class, "providencias");
		xstrm.omitField(DocumentoElectronico.class, "archivosAdjuntos");
		xstrm.omitField(Resolucion.class, "indicaciones");
		xstrm.omitField(Decreto.class, "indicaciones");
		// xstrm.omitField(RespuestaConsultaCiudadana.class, "archivos");
		// xstrm.omitField(RespuestaConsultaCiudadana.class,
		// "idDocumentoConsultaCiudadana");
		// xstrm.omitField(RespuestaConsultaCiudadana.class, "tipoRespuesta");

		// sin referencias internas
		xstrm.setMode(XStream.NO_REFERENCES);

		// converters
		xstrm.registerConverter(new BooleanConverter("SI", "NO", false));
		xstrm.registerConverter(new TipoDocumentoConverter());
		xstrm.registerConverter(new PersonaConverter());
		xstrm.registerConverter(new ListConverter());
		xstrm.registerConverter(new FirmaConverter());
		xstrm.registerConverter(new VisaConverter());
		xstrm.registerConverter(new ParrafoConverter());
		xstrm.registerConverter(new AccionProvidenciaConverter());
		xstrm.registerConverter(new DestinatarioConverter());
		xstrm.registerConverter(new ArchivoAdjuntoDocumentoElectronicoConverter());
		xstrm.registerConverter(new DateConverter());
		xstrm.registerConverter(new MinutaConverter());
		xstrm.registerConverter(new OficioConverter());
		xstrm.registerConverter(new ResolucionConverter());
		xstrm.registerConverter(new DecretoConverter());
		xstrm.registerConverter(new ConvenioConverter());
		xstrm.registerConverter(new ContratoConverter());
		xstrm.registerConverter(new DistribucionConverter());
		xstrm.registerConverter(new ProvidenciaConverter());
		xstrm.registerConverter(new MemorandumConverter());
		xstrm.registerConverter(new CartaConverter());
		// xstrm.registerConverter(new RespuestaDocumentoConsultaConverter());
		// xstrm.registerConverter(new ActaAdjudicacionConverter());

		return xstrm;
	}
}
