package cl.exe.exedoc.repositorio;

import java.text.SimpleDateFormat;

import cl.exe.exedoc.entity.Providencia;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ProvidenciaConverter extends ExeBaseConverter implements Converter {

	public void marshal(Object obj, HierarchicalStreamWriter writer,
			MarshallingContext context) {
		Providencia providencia = (Providencia) obj;
		if (providencia == null)
			return;
		convertAnotherNode("numeroDocumento", providencia.getNumeroDocumento(), writer, context);
		if (providencia.getFechaDocumentoOrigen() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			convertAnotherNode("fecha", sdf.format(providencia.getFechaDocumentoOrigen()), writer, context);
		}
		if (providencia.getPlazo() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			convertAnotherNode("fecha", sdf.format(providencia.getPlazo()), writer, context);
		}
		convertAnotherNode("materia", providencia.getMateria(), writer, context);
		convertAnotherNode("reservado", providencia.getReservado(), writer, context);
		convertAnotherNode("autor", providencia.getAutor(), writer, context);
		convertAnotherNode("emisor", providencia.getEmisor(), writer, context);
		convertAnotherNode("destinatarios", providencia.getDestinatarios(), writer, context);
		convertAnotherNode("observaciones", providencia.getObservaciones(), writer, context);
		convertAnotherNode("providencias", providencia.getAcciones(), writer, context);
		convertAnotherNode("visadoPor", providencia.getVisaciones(), writer, context);
		convertAnotherNode("firmadoPor", providencia.getFirmas(), writer, context);
	}

	

	@SuppressWarnings("unchecked")
	public boolean canConvert(Class cls) {
		return cls.equals(Providencia.class);
	}
}
