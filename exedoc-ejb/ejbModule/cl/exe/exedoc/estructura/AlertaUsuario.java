package cl.exe.exedoc.estructura;

import java.util.List;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Persona;

/**
 * @author Pablo
 *
 */
public class AlertaUsuario {
	
	private Persona persona;
	private List<Documento> documentos;
	private List<Documento> documentosCompartidos;

	/**
	 * Constructor.
	 */
	public AlertaUsuario() {
		super();
	}

	/**
	 * @return {@link Persona}
	 */
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona {@link Persona}
	 */
	public void setPersona(final Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return {@link List} of {@link Documento}
	 */
	public List<Documento> getDocumentos() {
		return documentos;
	}

	/**
	 * @param documentos {@link List} of {@link Documento}
	 */
	public void setDocumentos(final List<Documento> documentos) {
		this.documentos = documentos;
	}

	public List<Documento> getDocumentosCompartidos() {
		return documentosCompartidos;
	}

	public void setDocumentosCompartidos(List<Documento> documentosCompartidos) {
		this.documentosCompartidos = documentosCompartidos;
	}
	
}
