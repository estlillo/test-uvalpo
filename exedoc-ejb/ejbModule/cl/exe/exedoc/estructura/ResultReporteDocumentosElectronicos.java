package cl.exe.exedoc.estructura;

/**
 * @author Pablo
 *
 */
public class ResultReporteDocumentosElectronicos {
	
	private String rango;
	private String tipoDocumento;
	private Long iduo;
	private String uo;
	private Long idusuario;
	private String usuario;
	private Long cantidad;

	/**
	 * Constructor.
	 */
	public ResultReporteDocumentosElectronicos() {
		super();
	}

	/**
	 * @return {@link String}
	 */
	public String getRango() {
		return rango;
	}

	/**
	 * @param rango {@link String}
	 */
	public void setRango(final String rango) {
		this.rango = rango;
	}

	/**
	 * @return {@link String}
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento {@link String}
	 */
	public void setTipoDocumento(final String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return {@link String}
	 */
	public String getUo() {
		return uo;
	}

	/**
	 * @param uo {@link String}
	 */
	public void setUo(final String uo) {
		this.uo = uo;
	}

	/**
	 * @return {@link String}
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario {@link String}
	 */
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad {@link Long}
	 */
	public void setCantidad(final Long cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getIduo() {
		return iduo;
	}

	/**
	 * @param iduo {@link Long}
	 */
	public void setIduo(final Long iduo) {
		this.iduo = iduo;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getIdusuario() {
		return idusuario;
	}

	/**
	 * @param idusuario {@link Long}
	 */
	public void setIdusuario(Long idusuario) {
		this.idusuario = idusuario;
	}
	
}
