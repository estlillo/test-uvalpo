package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

import cl.exe.exedoc.mantenedores.dao.AdministradorAlertasBean;

@Entity
@Table(name = "FOLIOS_DOCUMENTO")
@SequenceGenerator(name = "SEQ_FOLIO", sequenceName = "SEQ_FOLIO", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "FolioDocumento.findById", query = "SELECT f FROM FolioDocumento f WHERE f.id = :id"),
		@NamedQuery(name = "FolioDocumento.findByAll", query = "SELECT f FROM FolioDocumento f"),
		@NamedQuery(name = "FolioDocumento.findByIdTipoDocumento", query = "SELECT f FROM FolioDocumento f WHERE f.tipoDocumento.id = :tipodocumento"),
		@NamedQuery(name = "FolioDocumento.findByFiltro", query = "SELECT f FROM FolioDocumento f WHERE f.fiscalia.id = :fiscalia and f.unidad.id = :unidad and f.tipoDocumento.id = :tipodocumento") })
public class FolioDocumento implements Serializable {

	private static final long serialVersionUID = -5535896380905609086L;

	private Long id;
	private Departamento fiscalia;
	private UnidadOrganizacional unidad;
	private TipoDocumento tipoDocumento;
	private Integer agnoActual;
	private Long numeroActual;
	private Boolean nivel;
	private Integer formato;
	private String unidadTmp;
	private Date fechaCreacion;
	
	private Long modo;

	public static final String ERROR_AL_OBTENER_FOLIOS = "Error al obtener folios: ";

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FOLIO")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nivel
	 */
	@Column(name = "NIVEL")
	public Boolean getNivel() {
		return nivel;
	}

	/**
	 * @param nivel
	 *            the nivel to set
	 */
	public void setNivel(Boolean nivel) {
		this.nivel = nivel;
	}

	@Column(name = "AGNO_ACTUAL")
	public Integer getAgnoActual() {
		return agnoActual;
	}

	public void setAgnoActual(Integer agnoActual) {
		this.agnoActual = agnoActual;
	}

	@Column(name = "NUMERO_ACTUAL")
	public Long getNumeroActual() {
		return numeroActual;
	}

	public void setNumeroActual(Long numeroActual) {
		this.numeroActual = numeroActual;
	}

	/**
	 * @return the formato
	 */
	@Column(name = "FORMATO")
	public Integer getFormato() {
		return formato;
	}

	/**
	 * @param formato
	 *            the formato to set
	 */
	public void setFormato(Integer formato) {
		this.formato = formato;
	}

	/**
	 * @param modo
	 *            the modo to set
	 */
	public void setModo(Long modo) {
		this.modo = modo;
	}

	/**
	 * @return the modo
	 */
	public Long getModo() {
		return modo;
	}

	/**
	 * @param tipoDocumento
	 *            the tipoDocumento to set
	 */
	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the tipoDocumento
	 */
	@ManyToOne
	@JoinColumn(name = "ID_TIPODOCUMENTO", nullable = false)
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param fiscalia
	 *            the fiscalia to set
	 */
	public void setFiscalia(Departamento fiscalia) {
		this.fiscalia = fiscalia;
	}

	/**
	 * @return the fiscalia
	 */
	@ManyToOne
	@JoinColumn(name = "ID_FISCALIA", nullable = true)
	public Departamento getFiscalia() {
		return fiscalia;
	}

	/**
	 * @param unidad
	 *            the unidad to set
	 */
	public void setUnidad(UnidadOrganizacional unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return the unidad
	 */
	@ManyToOne
	@JoinColumn(name = "ID_UNIDAD", nullable = true)
	public UnidadOrganizacional getUnidad() {
		return unidad;
	}

	@Transient
	public String getUnidadTmp() {

		if (this.getUnidad() == null) {
			return "Todos";
		} else {
			return this.getUnidad().getDescripcion();
		}
	}

	/**
	 * @return Fecha creacion
	 */
	@Column(name = "FECHA_CREACION")
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	

}
