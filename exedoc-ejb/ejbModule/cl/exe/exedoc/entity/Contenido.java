package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Contenido")
@DiscriminatorValue("1")
public class Contenido extends Parrafo implements Serializable {

	private static final long serialVersionUID = -4276391305145222153L;

}
