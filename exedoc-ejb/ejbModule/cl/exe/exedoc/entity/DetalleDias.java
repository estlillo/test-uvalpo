package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name= "DETALLE_DIAS")
@SequenceGenerator(name = "SEQ_DETALLE_DIAS", sequenceName="SEQ_DETALLE_DIAS", allocationSize = 1, initialValue = 1)
public class DetalleDias implements Serializable{

	private static final long serialVersionUID = 648141987806085135L;
	
	public static final Integer AM = 1;
	public static final Integer PM = 2;
	public static final Integer TODO = 3;

	private Long id;
	private DiaAdministrativo diaAdministrativo;
	private Date fechaDia;
	private Integer parteDia;
	private Integer parteDiaString;
	//private SolicitudDiaAdministrativo solicitudDiaAdministrativo;
	
	@Transient
	public static String getParteDia(Integer id) {
		switch (id) {
		case 1:
			return "AM";
		case 2:
			return "PM";
		case 3:
			return "TODO";
		}
		return "";
	}
	
	public void setParteDiaString(Integer parteDiaString) {
		this.parteDiaString = parteDiaString;
	}
	
	@Transient
	public String getParteDiaString() {
		switch (this.parteDia.intValue()) {
		case 1:
			return "AM";
		case 2:
			return "PM";
		case 3:
			return "TODO";
		}
		return "";
	}
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_DETALLE_DIAS")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_DIA_ADMINISTRATIVO", nullable = true)
	public DiaAdministrativo getDiaAdministrativo() {
		return diaAdministrativo;
	}
	
	public void setDiaAdministrativo(DiaAdministrativo diaAdministrativo) {
		this.diaAdministrativo = diaAdministrativo;
	}
	
	@Column(name = "FECHA_DIA")
	@Temporal(TemporalType.DATE)
	public Date getFechaDia() {
		return fechaDia;
	}
	
	public void setFechaDia(Date fechaDia) {
		this.fechaDia = fechaDia;
	}

	@Column(name = "PARTE_DIA")
	public Integer getParteDia() {
		return parteDia;
	}

	public void setParteDia(Integer parteDia) {
		this.parteDia = parteDia;
	}
	
//	@OneToOne
//	@JoinColumn(name = "ID_SOLICITUD_DIA_ADMINISTRATIVO")
//	public SolicitudDiaAdministrativo getSolicitudDiaAdministrativo() {
//		return solicitudDiaAdministrativo;
//	}
//
//	public void setSolicitudDiaAdministrativo(SolicitudDiaAdministrativo solicitudDiaAdministrativo) {
//		this.solicitudDiaAdministrativo = solicitudDiaAdministrativo;
//	}
	
	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [diaAdministrativo: ");
		representacion.append(this.diaAdministrativo.getId());
		representacion.append("], [fechaDia: ");
		representacion.append(this.fechaDia);
		representacion.append("], [parteDia: ");
		representacion.append(this.parteDia);
//		representacion.append("], [solicitudDiaAdministrativo: ");
//		representacion.append(this.solicitudDiaAdministrativo.getId());
		representacion.append("]");
		return representacion.toString();
	}
}