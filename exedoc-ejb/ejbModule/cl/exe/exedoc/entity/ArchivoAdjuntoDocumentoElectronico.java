package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.descriptores.ArchivoAdjunto;

@Entity
@Name("ArchivoAdjuntoDocumentoElectronico")
@DiscriminatorValue("10")
public class ArchivoAdjuntoDocumentoElectronico extends Archivo implements Serializable, ArchivoAdjunto {

	private static final long serialVersionUID = 6243965816781006899L;

	private String materia;
	private DocumentoElectronico documentoElectronico;
	private Persona adjuntadoPor;
	private Boolean xml = false;

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO_ELECTRONICO", nullable = true)
	public DocumentoElectronico getDocumentoElectronico() {
		return documentoElectronico;
	}

	public void setDocumentoElectronico(final DocumentoElectronico documentoElectronico) {
		this.documentoElectronico = documentoElectronico;
	}

	@Override
	@Column(name = "MATERIA")
	public String getMateria() {
		return materia;
	}

	@Override
	public void setMateria(final String materia) {
		this.materia = materia;
	}

	@Override
	@ManyToOne
	@JoinColumn(name = "ID_ADJUNTADO_POR", nullable = true)
	public Persona getAdjuntadoPor() {
		return adjuntadoPor;
	}

	@Override
	public void setAdjuntadoPor(final Persona adjuntadoPor) {
		this.adjuntadoPor = adjuntadoPor;
	}

	@Column(name = "XML")
	public Boolean getXml() {
		return xml;
	}

	public void setXml(final Boolean xml) {
		this.xml = xml;
	}

	@Transient
	public ArchivoAdjuntoDocumentoElectronico archivo() {
		final ArchivoAdjuntoDocumentoElectronico archivo = new ArchivoAdjuntoDocumentoElectronico();
		archivo.setId(super.getId());
		archivo.setCmsId(super.getCmsId());
		archivo.setFecha(super.getFecha());
		archivo.setNombreArchivo(super.getNombreArchivo());
		archivo.setContentType(super.getContentType());
		archivo.setArchivo(super.getArchivo());
		archivo.setIdNuevoArchivo(super.getIdNuevoArchivo());
		archivo.setMateria(this.materia);
		archivo.setDocumentoElectronico(this.documentoElectronico);
		archivo.setAdjuntadoPor(this.adjuntadoPor);
		return archivo;
	}

}
