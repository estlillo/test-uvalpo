package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity {@link TipoDocumento}.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "TIPOS_DOCUMENTOS")
@SequenceGenerator(name = "SEQ_TIPOS_DOCUMENTOS", sequenceName = "SEQ_TIPOS_DOCUMENTOS", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "tipoDocumentoActivo.yNoEsElectronico", query = "SELECT td FROM TipoDocumento "
				+ " td where td.visible = true and td.electronico = false order by td.descripcion"),
		@NamedQuery(name = "tipoDocumentoActivo.yEsElectronicoCreacion", query = "SELECT td FROM TipoDocumento "
				+ " td where td.visible = true and td.electronico = true and td.creacion = true order by td.descripcion"),
		@NamedQuery(name = "tipoDocumentoActivo.yEsElectronico", query = "SELECT td FROM TipoDocumento "
				+ " td where td.visible = true and td.electronico = true order by td.descripcion"),
		@NamedQuery(name = "tipoDocumentoActivo.tipoDocumento", query = "SELECT td FROM TipoDocumento "
				+ " td where td.visible = true order by td.descripcion") })
public class TipoDocumento implements Serializable {

	/**
	 * OFICIO ORDINARIO.
	 */
	public static final Integer OFICIO_ORDINARIO = 1;
	/**
	 * OFICIO CIRCULAR.
	 */
	public static final Integer OFICIO_CIRCULAR = 2;
	/**
	 * OFICIO RESERVADO.
	 */
	public static final Integer OFICIO_RESERVADO = 3;
	/**
	 * RESOLUCION.
	 */
	public static final Integer RESOLUCION = 6;
	/**
	 * MEMORANDUM.
	 */
	public static final Integer MEMORANDUM = 7;
	/**
	 * PROVIDENCIA.
	 */
	public static final Integer PROVIDENCIA = 8;
	/**
	 * DECRETO.
	 */
	public static final Integer DECRETO = 9;
	/**
	 * CARTA.
	 */
	public static final Integer CARTA = 10;
	/**
	 * FACTURA.
	 */
	public static final Integer FACTURA = 11;
	/**
	 * CONVENIO.
	 */
	public static final Integer CONVENIO = 22;
	/**
	 * CONTRATO.
	 */
	public static final Integer CONTRATO = 23;

	/**
	 * ANULACION_RESOLUCION.
	 */
	public static final Integer ANULACION_RESOLUCION = 98;
	
	/**
	 * SOLICITUD_FOLIO.
	 */
	public static final Integer SOLICITUD_FOLIO = 99;
	/**
	 * SIN_TIPO.
	 */
	public static final Integer SIN_TIPO = 50;

	private static final long serialVersionUID = -4397192804798784425L;

	private Integer id;
	private String descripcion;
	private Boolean electronico;
	private Boolean visible = true;
	private Boolean creacion;

	public static final Integer SOLICITUD_ADMINISTRATIVO = 15;
	public static final Integer SOLICITUD_VACACIONES = 16;
	// public static final Integer SOLICITUD_ADQUISICION = 17;
	// public static final Integer CONSULTA = 20;
	// public static final Integer RESPUESTA_CONSULTA = 21;
	// public static final Integer ACTA_ADJUDICACION = 24;

	public Boolean getCreacion() {
		return creacion;
	}

	public void setCreacion(Boolean creacion) {
		this.creacion = creacion;
	}

	/**
	 * Constructor.
	 */
	public TipoDocumento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Integer}
	 */
	public TipoDocumento(final Integer id) {
		this.id = id;
		switch (id) {
			case 1:
				descripcion = "Oficio Ordinario";
				electronico = true;
				visible = true;
				creacion = true;
				break;
			case 2:
				descripcion = "Oficio Circular";
				electronico = true;
				visible = true;
				creacion = true;
				break;
			case 3:
				descripcion = "Oficio Reservado";
				electronico = true;
				visible = true;
				creacion = true;
				break;
			case 6:
				descripcion = "Resolucion";
				electronico = true;
				visible = true;
				creacion = true;
				break;
			case 7:
				descripcion = "Memorandum";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 8:
				descripcion = "Providencia";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 9:
				descripcion = "Decreto";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 10:
				descripcion = "Carta";
				electronico = true;
				visible = true;
				creacion = true;
				break;
			case 11:
				descripcion = "Factura";
				electronico = true;
				visible = false;
				creacion = false;
				break;
			case 15:
				descripcion = "Solicitud Dia Administrativo";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 16:
				descripcion = "Solicitud Vacaciones";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 17:
				descripcion = "Solicitud Adquisicion";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 20:
				descripcion = "Consulta Ciudadana";
				electronico = true;
				visible = false;
				creacion = false;
				break;
			case 21:
				descripcion = "Respuesta Consulta Ciudadana";
				electronico = true;
				visible = false;
				creacion = false;
				break;
			case 22:
				descripcion = "Convenio";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 23:
				descripcion = "Contrato";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 24:
				descripcion = "Acta de Adjudicacion";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 98:
				descripcion = "Anulacion Resolucion";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 99:
				descripcion = "Solicitud Folio";
				electronico = true;
				visible = true;
				creacion = false;
				break;
			case 50:
				descripcion = "Anexo";
				electronico = false;
				visible = false;
				creacion = false;
				break;
			default:
				descripcion = "Anexo";
				electronico = false;
				visible = false;
				creacion = false;
				break;
		}
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPOS_DOCUMENTOS")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id {@link Integer}
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "ELECTRONICO")
	public Boolean getElectronico() {
		return electronico;
	}

	/**
	 * @param electronico {@link Boolean}
	 */
	public void setElectronico(final Boolean electronico) {
		this.electronico = electronico;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "VISIBLE")
	public Boolean getVisible() {
		return visible;
	}

	/**
	 * @param visible {@link Boolean}
	 */
	public void setVisible(final Boolean visible) {
		this.visible = visible;
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(id);
		representacion.append("], [descripcion: ");
		representacion.append(descripcion);
		representacion.append("]");
		return representacion.toString();
	}
}
