package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("DestinatarioDocumento")
@DiscriminatorValue("1")
public class DestinatarioDocumento extends ListaPersonasDocumento implements Serializable {

	private static final long serialVersionUID = -7893655303474920722L;

}
