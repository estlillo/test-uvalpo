package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Pablo
 * 
 */
@Entity
@Table(name = "REVISAR_DOCUMENTOS")
@SequenceGenerator(name = "SEQ_REVISAR_DOCUMENTOS", sequenceName = "SEQ_REVISAR_DOCUMENTOS", allocationSize = 1, initialValue = 1)
public class RevisarDocumentos implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1438831511994464966L;

	private Long id;
	private Documento documento;
	private Date fechaFirma;
	private Persona persona;
	private Boolean aprobado;
	private String comentario;

	/**
	 * Contructor.
	 */
	public RevisarDocumentos() {
		super();
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REVISAR_DOCUMENTOS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Documento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento {@link Documento}
	 */
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_FIRMA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaFirma() {
		return fechaFirma;
	}

	/**
	 * @param fechaFirma {@link Date}
	 */
	public void setFechaFirma(final Date fechaFirma) {
		this.fechaFirma = fechaFirma;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona {@link Persona}
	 */
	public void setPersona(final Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "APROBADO")
	public Boolean getAprobado() {
		return aprobado;
	}

	/**
	 * @param aprobado {@link Boolean}
	 */
	public void setAprobado(final Boolean aprobado) {
		this.aprobado = aprobado;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "COMENTARIO")
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario {@link String}
	 */
	public void setComentario(final String comentario) {
		this.comentario = comentario;
	}
	
}
