package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author pablo
 * 
 */
@Entity
@Table(name = "REVISAR_DOCUMENTOS_HIST")
@SequenceGenerator(name = "SEQ_REVISAR_DOCUMENTOS_HIST", sequenceName = "SEQ_REVISAR_DOCUMENTOS_HIST", allocationSize = 1, initialValue = 1)
@NamedQueries({ @NamedQuery(name = "RevisarDocumentosHistorial.findByDocumento", query = "SELECT r FROM RevisarDocumentosHistorial r WHERE r.documento.id = :idDoc") })
public class RevisarDocumentosHistorial implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1303523439492891832L;

	private Long id;
	private Documento documento;
	private Date fechaFirma;
	private Persona persona;
	private Boolean aprobado;
	private String comentario;

	/**
	 * Constructor.
	 */
	public RevisarDocumentosHistorial() {
		super();
	}

	public RevisarDocumentosHistorial(RevisarDocumentos revisar) {
		this.setAprobado(revisar.getAprobado());
		this.setComentario(revisar.getComentario());
		this.setDocumento(revisar.getDocumento());
		this.setFechaFirma(revisar.getFechaFirma());
		this.setPersona(revisar.getPersona());
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REVISAR_DOCUMENTOS_HIST")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Documento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento
	 *            {@link Documento}
	 */
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_FIRMA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaFirma() {
		return fechaFirma;
	}

	/**
	 * @param fechaFirma
	 *            {@link Date}
	 */
	public void setFechaFirma(final Date fechaFirma) {
		this.fechaFirma = fechaFirma;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona
	 *            {@link Persona}
	 */
	public void setPersona(final Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "APROBADO")
	public Boolean getAprobado() {
		return aprobado;
	}

	/**
	 * @param aprobado
	 *            {@link Boolean}
	 */
	public void setAprobado(final Boolean aprobado) {
		this.aprobado = aprobado;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "COMENTARIO")
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario
	 *            {@link String}
	 */
	public void setComentario(final String comentario) {
		this.comentario = comentario;
	}

}
