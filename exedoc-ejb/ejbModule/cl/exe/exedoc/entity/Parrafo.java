package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity Parrafo.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "PARRAFOS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ID_TIPO_PARRAFO", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "SEQ_PARRAFOS", sequenceName = "SEQ_PARRAFOS", allocationSize = 1, initialValue = 1)
public class Parrafo implements Serializable {

	private static final long serialVersionUID = -3252698839219408884L;

	private Long id;
	private Long numero;
	private String cuerpo;
	private DocumentoElectronico documento;

	/**
	 * Constructor.
	 */
	public Parrafo() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public Parrafo(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PARRAFOS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "NUMERO")
	public Long getNumero() {
		return numero;
	}

	/**
	 * @param numero {@link Long}
	 */
	public void setNumero(final Long numero) {
		this.numero = numero;
	}

	/**
	 * @return {@link String}
	 */
	@Lob
	@Column(name = "CUERPO")
	public String getCuerpo() {
		return cuerpo;
	}

	/**
	 * @param cuerpo {@link String}
	 */
	public void setCuerpo(final String cuerpo) {
		this.cuerpo = cuerpo;
	}

	/**
	 * @return {@link DocumentoElectronico}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO_ELECTRONICO", nullable = false)
	public DocumentoElectronico getDocumento() {
		return documento;
	}

	/**
	 * @param documento {@link DocumentoElectronico}
	 */
	public void setDocumento(final DocumentoElectronico documento) {
		this.documento = documento;
	}

}
