package cl.exe.exedoc.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.jboss.seam.annotations.Name;

@Entity
@Name("DiaAdministrativo")
@DiscriminatorValue("150")
@NamedQueries({
	@NamedQuery(name = "DiaAdministrativo.findById", query = "SELECT d FROM DiaAdministrativo d WHERE d.id = :id"), 
	@NamedQuery(name = "DiaAdministrativo.findByAll", query = "SELECT d FROM DiaAdministrativo d") 
})
public class DiaAdministrativo extends Vacaciones {

	private static final long serialVersionUID = 648141987806085135L;
	
	public static final String TIPO_DIA_ADMINISTRATIVO= "2";
	public static final String TIPO_DIA_FERIADO_LEGAL= "1";

	private Long diasPendientes;
	private Integer cantMediosDiasSolicitados;
	private Boolean diaAdministrativo;
	private Boolean feriadoLegal;
	private Boolean aceptadoDiaAdministrativo;
	private Boolean aceptadoDiaAdministrativoRRHH;
	private Boolean rechazadoDiaAdministrativo;
	private SolicitudDiaAdministrativo solicitudDiaAdministrativo;
	private List<DetalleDias> listDetalleDias;
	private Boolean creandoResolucion;

	public DiaAdministrativo() {
		
	}
	
	@Column(name = "CREANDO_RESOLUCION")
	public Boolean getCreandoResolucion() {
		return creandoResolucion;
	}

	public void setCreandoResolucion(Boolean creandoResolucion) {
		this.creandoResolucion = creandoResolucion;
	}

	@Column(name = "ACEPTADO_DIA_ADM")
	public Boolean getAceptadoDiaAdministrativo() {
		return aceptadoDiaAdministrativo;
	}

	public void setAceptadoDiaAdministrativo(Boolean aceptadoDiaAdministrativo) {
		this.aceptadoDiaAdministrativo = aceptadoDiaAdministrativo;
	}
	
	@Column(name = "ACEPTADO_DIA_ADM_RRHH")
	public Boolean getAceptadoDiaAdministrativoRRHH() {
		return aceptadoDiaAdministrativoRRHH;
	}

	public void setAceptadoDiaAdministrativoRRHH(Boolean aceptadoDiaAdministrativoRRHH) {
		this.aceptadoDiaAdministrativoRRHH = aceptadoDiaAdministrativoRRHH;
	}
	
	@Column(name = "RECHAZADO_DIA_ADM")
	public Boolean getRechazadoDiaAdministrativo() {
		return rechazadoDiaAdministrativo;
	}

	public void setRechazadoDiaAdministrativo(Boolean rechazadoDiaAdministrativo) {
		this.rechazadoDiaAdministrativo = rechazadoDiaAdministrativo;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "diaAdministrativoSolicitud", cascade = CascadeType.ALL)
	public SolicitudDiaAdministrativo getSolicitudDiaAdministrativo() {
		return solicitudDiaAdministrativo;
	}

	public void setSolicitudDiaAdministrativo(SolicitudDiaAdministrativo solicitudDiaAdministrativo) {
		this.solicitudDiaAdministrativo = solicitudDiaAdministrativo;
	}

	@Column(name = "DIA_ADM")
	public Boolean getDiaAdministrativo() {
		return diaAdministrativo;
	}

	public void setDiaAdministrativo(Boolean diaAdministrativo) {
		this.diaAdministrativo = diaAdministrativo;
	}

	@Column(name = "FERIADO_LEGAL")
	public Boolean getFeriadoLegal() {
		return feriadoLegal;
	}

	public void setFeriadoLegal(Boolean feriadoLegal) {
		this.feriadoLegal = feriadoLegal;
	}

	@Column(name = "DIAS_PENDIENTES")
	public Long getDiasPendientes() {
		return diasPendientes;
	}

	public void setDiasPendientes(Long diasPendientes) {
		this.diasPendientes = diasPendientes;
	}
	
	@Column(name = "CANT_MEDIOS_DIAS_SOLIC")
	public Integer getCantMediosDiasSolicitados() {
		return cantMediosDiasSolicitados;
	}

	public void setCantMediosDiasSolicitados(Integer cantMediosDiasSolicitados) {
		this.cantMediosDiasSolicitados = cantMediosDiasSolicitados;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "diaAdministrativo", cascade = CascadeType.ALL)
	public List<DetalleDias> getListDetalleDias() {
		return listDetalleDias;
	}

	public void setListDetalleDias(List<DetalleDias> listDetalleDias) {
		this.listDetalleDias = listDetalleDias;
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[diasPendientes: ");
		representacion.append(this.diasPendientes);
		representacion.append("], [cantMediosDiasSolicitados: ");
		representacion.append(this.cantMediosDiasSolicitados);
		representacion.append("], [diaAdministrativo: ");
		representacion.append(this.diaAdministrativo);
		representacion.append("], [feriadoLegal: ");
		representacion.append(this.feriadoLegal);
		representacion.append("], [aceptadoDiaAdministrativo: ");
		representacion.append(this.aceptadoDiaAdministrativo);
		representacion.append("], [rechazadoDiaAdministrativo: ");
		representacion.append(this.rechazadoDiaAdministrativo);
		representacion.append("], [solicitudDiaAdministrativo: ");
		representacion.append(this.solicitudDiaAdministrativo.getId());
		representacion.append("], [listDetalleDias: ");
		representacion.append(this.listDetalleDias);
		representacion.append("]");
		return representacion.toString();
	}
}