package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "FOLIOS_EN_FIRMA")
@SequenceGenerator(name = "SEQ_FOLIOS_EN_FIRMA", sequenceName = "SEQ_FOLIOS_EN_FIRMA", allocationSize = 1, initialValue = 1)
@NamedQueries( { @NamedQuery(name = "FolioEnFirma.findById", query = "SELECT fef FROM FolioEnFirma fef WHERE fef.id = :id"),
		@NamedQuery(name = "FolioEnFirma.findByAll", query = "SELECT fef FROM FolioEnFirma fef"),
		@NamedQuery(name = "FolioEnFirma.findByNumeroFolio", query = "SELECT fef FROM FolioEnFirma fef WHERE fef.numeroFolio = :numeroFolio"),
		@NamedQuery(name = "FolioEnFirma.findByIdDocumento", query = "SELECT fef FROM FolioEnFirma fef WHERE fef.idDocumento = :idDocumento") })
public class FolioEnFirma implements Serializable {

	private static final long serialVersionUID = -8395041794623641045L;

	private Long id;
	private String numeroFolio;
	private Date fechaReserva;
	private Boolean enUso;
	private Date fechaUso;
	private Long idDocumento;
	private TipoDocumento tipoDocumento;
	private Boolean tomaRazon;
	private Boolean exentoConRegistro;
	private String idDocumentoMI;
	private String agnoNumeroFolio;
	

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FOLIOS_EN_FIRMA")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NUMERO_FOLIO", nullable = false)
	public String getNumeroFolio() {
		return numeroFolio;
	}

	public void setNumeroFolio(String numeroFolio) {
		this.numeroFolio = numeroFolio;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_RESERVA")
	public Date getFechaReserva() {
		return fechaReserva;
	}

	public void setFechaReserva(Date fechaReserva) {
		this.fechaReserva = fechaReserva;
	}

	@Column(name = "EN_USO")
	public Boolean getEnUso() {
		return enUso;
	}

	public void setEnUso(Boolean enUso) {
		this.enUso = enUso;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_USO")
	public Date getFechaUso() {
		return fechaUso;
	}

	public void setFechaUso(Date fechaUso) {
		this.fechaUso = fechaUso;
	}

	@Column(name = "ID_DOCUMENTO", nullable = false)
	public Long getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_DOCUMENTO")
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Column(name = "TOMA_RAZON")
	public Boolean getTomaRazon() {
		return tomaRazon;
	}

	public void setTomaRazon(Boolean tomaRazon) {
		this.tomaRazon = tomaRazon;
	}
	
	@Column(name = "EXENTO_CON_REGISTRO")
	public Boolean getExentoConRegistro() {
		return exentoConRegistro;
	}

	public void setExentoConRegistro(Boolean exentoConRegistro) {
		this.exentoConRegistro = exentoConRegistro;
	}
	
	@Column(name = "ID_DOCUMENTO_MI")
	public String getIdDocumentoMI() {
		return idDocumentoMI;
	}

	public void setIdDocumentoMI(String idDocumentoMI) {
		this.idDocumentoMI = idDocumentoMI;
	}

	@Column(name = "AGNO_NUMERO_FOLIO")
	public String getAgnoNumeroFolio() {
		return agnoNumeroFolio;
	}

	public void setAgnoNumeroFolio(String agnoNumeroFolio) {
		this.agnoNumeroFolio = agnoNumeroFolio;
	}

}
