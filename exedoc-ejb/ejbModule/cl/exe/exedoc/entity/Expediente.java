package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Entity Expediente.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "EXPEDIENTES")
@SequenceGenerator(name = "SEQ_EXPEDIENTES", sequenceName = "SEQ_EXPEDIENTES", allocationSize = 1, initialValue = 1)
@NamedQueries({
	@NamedQuery(name = "Expediente.getVersionPersona", query = "SELECT count(e.id) + 1 FROM Expediente e WHERE " + "" +
			"((e.emisor.id= :idPersona AND e.fechaAcuseRecibo is not null) " + 
				"OR (e.destinatario.id = :idPersona AND e.fechaAcuseRecibo is null)) " + 
			"AND e.numeroExpediente = :numeroExpediente " + 
			"AND e.id != :idExpediente AND (e.version != null or e.version > 0)"),
	@NamedQuery(name = "Expediente.getVersionGrupo", query = "SELECT count(e.id) + 1 FROM Expediente e WHERE " + "" +
			"((e.grupoHistorico.id= :idGrupo AND e.fechaAcuseRecibo is not null) " + 
				"OR (e.grupo.id = :idGrupo AND e.fechaAcuseRecibo is null)) " + 
			"AND e.numeroExpediente = :numeroExpediente " + 
			"AND e.id != :idExpediente AND (e.version != null or e.version > 0)")
})
public class Expediente implements Serializable {

	private static final long serialVersionUID = -1163948317741481416L;

	private Long id;
	private String numeroExpediente;
	private Date fechaIngreso;
	private Date fechaDespacho;
	private Date fechaDespachoHistorico;
	private List<DocumentoExpediente> documentos;
	private Persona emisor;
	private Persona emisorHistorico;
	private Persona destinatario;
	//private Boolean destinatarioConRecepcion;
	private Persona destinatarioHistorico;
	private Boolean acuseRecibo;
	private Date fechaAcuseRecibo;
	private List<Observacion> observaciones;
	private Date archivado;
	private Date cancelado;
	private Expediente versionPadre;
	private List<Expediente> expedientesHijos;
	private Integer version;
	//private Documento documentoFlujoEstructurado;
	//private Boolean desierto;
	private Boolean copia;
	//private Boolean eliminado;
	private GrupoUsuario grupo;
	//private List<Instruccion> instrucciones;
	private Date reasignado;
	private Boolean reenvio;
	private Date archivadoHistorico;
	private GrupoUsuario grupoHistorico;

	/**
	 * Constructor.
	 */
	public Expediente() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public Expediente(final Long id) {
		super();
		this.id = id;
	}

//	/**
//	 * @return {@link Boolean}
//	 */
//	@Column(name = "ELIMINADO")
//	public Boolean getEliminado() {
//		return eliminado;
//	}
//
//	/**
//	 * @param eliminado {@link Boolean}
//	 */
//	public void setEliminado(final Boolean eliminado) {
//		this.eliminado = eliminado;
//	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EXPEDIENTES")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NUMERO_EXPEDIENTE")
	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	/**
	 * @param numeroExpediente {@link String}
	 */
	public void setNumeroExpediente(final String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_INGRESO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	/**
	 * @param fechaIngreso {@link Date}
	 */
	public void setFechaIngreso(final Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_DESPACHO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaDespacho() {
		return fechaDespacho;
	}

	/**
	 * @param fechaDespacho {@link Date}
	 */
	public void setFechaDespacho(final Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_DESPACHO_HISTORICO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaDespachoHistorico() {
		return fechaDespachoHistorico;
	}

	/**
	 * @param fechaDespachoHistorico {@link Date}
	 */
	public void setFechaDespachoHistorico(final Date fechaDespachoHistorico) {
		this.fechaDespachoHistorico = fechaDespachoHistorico;
	}

	/**
	 * @return {@link Expediente}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PADRE", nullable = true)
	public Expediente getVersionPadre() {
		return versionPadre;
	}

	/**
	 * @param versionPadre {@link Expediente}
	 */
	public void setVersionPadre(final Expediente versionPadre) {
		this.versionPadre = versionPadre;
	}

	/**
	 * @return {@link List} of {@link DocumentoExpediente}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "expediente", cascade = CascadeType.ALL)
	public List<DocumentoExpediente> getDocumentos() {
		return documentos;
	}

	/**
	 * @param documentos {@link List} of {@link DocumentoExpediente}
	 */
	public void setDocumentos(final List<DocumentoExpediente> documentos) {
		this.documentos = documentos;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DESTINATARIO", nullable = true)
	public Persona getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario {@link Persona}
	 */
	public void setDestinatario(final Persona destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_EMISOR", nullable = true)
	public Persona getEmisor() {
		return emisor;
	}

	/**
	 * @param emisor {@link Persona}
	 */
	public void setEmisor(final Persona emisor) {
		this.emisor = emisor;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public Boolean getAcuseRecibo() {
		acuseRecibo = false;
		if (fechaAcuseRecibo != null) {
			acuseRecibo = true;
		}
		return acuseRecibo;
	}

	/**
	 * @param acuseRecibo {@link Boolean}
	 */
//	public void setAcuseRecibo(final Boolean acuseRecibo) {
//		this.acuseRecibo = acuseRecibo;
//	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_ACUSE_RECIBO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaAcuseRecibo() {
		return fechaAcuseRecibo;
	}

	/**
	 * @param fechaAcuseRecibo {@link Date}
	 */
	public void setFechaAcuseRecibo(final Date fechaAcuseRecibo) {
		this.fechaAcuseRecibo = fechaAcuseRecibo;
	}

	/**
	 * @return {@link List} of {@link Observacion}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "expediente", cascade = CascadeType.ALL)
	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones {@link List} of {@link Observacion}
	 */
	public void setObservaciones(final List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "ARCHIVADO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getArchivado() {
		return archivado;
	}

	/**
	 * @param archivado {@link Date}
	 */
	public void setArchivado(final Date archivado) {
		this.archivado = archivado;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DESTINATARIO_HISTORICO", nullable = true)
	public Persona getDestinatarioHistorico() {
		return destinatarioHistorico;
	}

	/**
	 * @param destinatarioHistorico {@link Persona}
	 */
	public void setDestinatarioHistorico(final Persona destinatarioHistorico) {
		this.destinatarioHistorico = destinatarioHistorico;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_EMISOR_HISTORICO", nullable = true)
	public Persona getEmisorHistorico() {
		return emisorHistorico;
	}

	/**
	 * @param emisorHistorico {@link Persona}
	 */
	public void setEmisorHistorico(final Persona emisorHistorico) {
		this.emisorHistorico = emisorHistorico;
	}

	/**
	 * @return {@link List} of {@link Expediente}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "versionPadre")
	public List<Expediente> getExpedientesHijos() {
		return expedientesHijos;
	}

	/**
	 * @param expedientesHijos {@link List} of {@link Expediente}
	 */
	public void setExpedientesHijos(final List<Expediente> expedientesHijos) {
		this.expedientesHijos = expedientesHijos;
	}

//	/**
//	 * @return {@link Boolean}
//	 */
//	@Column(name = "DESTINATARIO_CON_RECEPCION")
//	public Boolean getDestinatarioConRecepcion() {
//		return destinatarioConRecepcion;
//	}
//
//	/**
//	 * @param destinatarioConRecepcion {@link Boolean}
//	 */
//	public void setDestinatarioConRecepcion(final Boolean destinatarioConRecepcion) {
//		this.destinatarioConRecepcion = destinatarioConRecepcion;
//	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "VERSION")
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version {@link Integer}
	 */
	public void setVersion(final Integer version) {
		this.version = version;
	}

//	/**
//	 * @return {@link Documento}
//	 */
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "ID_DOC_FLUJO_ESTRUCTRADO", nullable = true)
//	public Documento getDocumentoFlujoEstructurado() {
//		return documentoFlujoEstructurado;
//	}
//
//	/**
//	 * @param documentoFlujoEstructurado {@link Documento}
//	 */
//	public void setDocumentoFlujoEstructurado(final Documento documentoFlujoEstructurado) {
//		this.documentoFlujoEstructurado = documentoFlujoEstructurado;
//	}

//	/**
//	 * @return {@link Boolean}
//	 */
//	@Column(name = "DESIERTO")
//	public Boolean getDesierto() {
//		return desierto;
//	}
//
//	/**
//	 * @param desierto {@link Boolean}
//	 */
//	public void setDesierto(final Boolean desierto) {
//		this.desierto = desierto;
//	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "COPIA")
	public Boolean getCopia() {
		return copia;
	}

	/**
	 * @param copia {@link Boolean}
	 */
	public void setCopia(final Boolean copia) {
		this.copia = copia;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "CANCELADO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCancelado() {
		return cancelado;
	}

	/**
	 * @param cancelado {@link Date}
	 */
	public void setCancelado(final Date cancelado) {
		this.cancelado = cancelado;
	}

	@Override
	public boolean equals(final Object o) {
		Boolean estado = false;
		if (o != null) {
			if (o instanceof Expediente) {
				final Expediente e = (Expediente) o;
				if (this.numeroExpediente.equals(e.getNumeroExpediente())) {
					if (e.getFechaDespacho() == null || this.getFechaDespacho() == null) {
						estado = false;
					} else if (this.fechaDespacho.equals(e.getFechaDespacho())) {
						estado = true;
					}
				}
			}
		}
		return estado;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [numeroExpediente: ");
		representacion.append(this.numeroExpediente);
		representacion.append("], [fechaIngreso: ");
		representacion.append(this.fechaIngreso);
		representacion.append("], [fechaDespacho: ");
		representacion.append(this.fechaDespacho);
		representacion.append("], [fechaDespachoHistorico: ");
		representacion.append(this.fechaDespachoHistorico);
		representacion.append("], [versionPadre: ");
		representacion.append(this.versionPadre);
		// representacion.append("], [documentos: ");
		// representacion.append(this.documentos);
		representacion.append("], [emisor: ");
		representacion.append(this.emisor);
		representacion.append("], [emisorHistorico: ");
		representacion.append(this.emisorHistorico);
		representacion.append("], [destinatario: ");
		representacion.append(this.destinatario);
		representacion.append("], [destinatarioHistorico: ");
		representacion.append(this.destinatarioHistorico);
		representacion.append("], [acuseRecibo: ");
		representacion.append(this.acuseRecibo);
		representacion.append("], [fechaAcuseRecibo: ");
		representacion.append(this.fechaAcuseRecibo);
		/*
		 * representacion.append("], [observaciones: "); representacion.append(this.observaciones);
		 */
		representacion.append("], [archivado: ");
		representacion.append(this.archivado);
		representacion.append("]");
		return representacion.toString();
	}

	/**
	 * @param de {@link DocumentoExpediente}
	 */
	@Transient
	public void addDocumento(final DocumentoExpediente de) {
		if (this.documentos == null) {
			documentos = new ArrayList<DocumentoExpediente>();
		}
		documentos.add(de);
	}

	/**
	 * @param grupo the grupo to set
	 */
	public void setGrupo(GrupoUsuario grupo) {
		this.grupo = grupo;
	}

	/**
	 * @return the grupo
	 */
	@ManyToOne
	@JoinColumn(name = "ID_GRUPO", nullable = true)
	public GrupoUsuario getGrupo() {
		return grupo;
	}

//	@ManyToMany
//	@JoinTable(name = "INSTRUCCIONES_EXPEDIENTES", joinColumns = @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_INSTRUCCION", referencedColumnName = "ID"))
//	public List<Instruccion> getInstrucciones() {
//		return instrucciones;
//	}
//
//	public void setInstrucciones(List<Instruccion> instrucciones) {
//		this.instrucciones = instrucciones;
//	}

	@Column(name = "REASIGNADO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReasignado() {
		return reasignado;
	}

	public void setReasignado(Date reasignado) {
		this.reasignado = reasignado;
	}

	@Column(name = "REENVIO")
	public Boolean getReenvio() {
		return reenvio;
	}

	public void setReenvio(Boolean reenvio) {
		this.reenvio = reenvio;
	}

	@Column(name = "ARCHIVADO_HISTORICO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getArchivadoHistorico() {
		return archivadoHistorico;
	}

	public void setArchivadoHistorico(Date archivadoHistorico) {
		this.archivadoHistorico = archivadoHistorico;
	}

	@ManyToOne
	@JoinColumn(name = "ID_GRUPO_HISTORICO", nullable = true)
	public GrupoUsuario getGrupoHistorico() {
		return grupoHistorico;
	}

	public void setGrupoHistorico(GrupoUsuario grupoHistorico) {
		this.grupoHistorico = grupoHistorico;
	}

}
