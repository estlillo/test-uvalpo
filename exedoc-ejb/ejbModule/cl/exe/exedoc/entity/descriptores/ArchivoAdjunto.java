package cl.exe.exedoc.entity.descriptores;

import cl.exe.exedoc.entity.Persona;

/**
 * Indica que un Archivo es un documento adjunto.
 * 
 * @author Spola
 */
public interface ArchivoAdjunto {

	/**
	 * Retorna la materia asociada al Archivo.
	 * 
	 * @return materia
	 */
	String getMateria();

	/**
	 * Setea la materia asociada al Archivo.
	 * 
	 * @param materia Materia
	 */
	void setMateria(String materia);

	/**
	 * Retorna la persona que adjunta el archivo.
	 * 
	 * @return {@link Persona}
	 */
	Persona getAdjuntadoPor();

	/**
	 * Setea la persona que adjunta el archivo.
	 * 
	 * @param adjuntadoPor {@link Persona}
	 */
	void setAdjuntadoPor(Persona adjuntadoPor);
}
