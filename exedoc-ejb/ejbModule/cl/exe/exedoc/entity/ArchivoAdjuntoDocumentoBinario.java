package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

import cl.exe.exedoc.entity.descriptores.ArchivoAdjunto;

@Entity
@Name("ArchivoAdjuntoDocumentoBinario")
@DiscriminatorValue("12")
public class ArchivoAdjuntoDocumentoBinario extends Archivo implements Serializable, ArchivoAdjunto {

	private static final long serialVersionUID = 6975530518494191592L;

	private String materia;
	private DocumentoBinario documentoBinario;
	private Persona adjuntadoPor;
	private Boolean xml = false;

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO_BINARIO", nullable = true)
	public DocumentoBinario getDocumentoBinario() {
		return documentoBinario;
	}

	public void setDocumentoBinario(final DocumentoBinario documentoBinario) {
		this.documentoBinario = documentoBinario;
	}

	@Override
	@Column(name = "MATERIA")
	public String getMateria() {
		return materia;
	}

	@Override
	public void setMateria(final String materia) {
		this.materia = materia;
	}

	@Override
	@ManyToOne
	@JoinColumn(name = "ID_ADJUNTADO_POR", nullable = true)
	public Persona getAdjuntadoPor() {
		return adjuntadoPor;
	}

	@Override
	public void setAdjuntadoPor(final Persona adjuntadoPor) {
		this.adjuntadoPor = adjuntadoPor;
	}

	@Column(name = "XML")
	public Boolean getXml() {
		return xml;
	}

	public void setXml(final Boolean xml) {
		this.xml = xml;
	}

	@Override
	public boolean equals(final Object o) {
		if (o != null && o instanceof ArchivoAdjuntoDocumentoBinario) {
			return ((ArchivoAdjuntoDocumentoBinario) o).getNombreArchivo().equals(this.getNombreArchivo());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getNombreArchivo().hashCode();
	}

	@Transient
	public ArchivoAdjuntoDocumentoBinario archivo() {
		final ArchivoAdjuntoDocumentoBinario archivo = new ArchivoAdjuntoDocumentoBinario();
		archivo.setId(super.getId());
		archivo.setCmsId(super.getCmsId());
		archivo.setFecha(super.getFecha());
		archivo.setNombreArchivo(super.getNombreArchivo());
		archivo.setContentType(super.getContentType());
		archivo.setArchivo(super.getArchivo());
		archivo.setIdNuevoArchivo(super.getIdNuevoArchivo());
		archivo.setMateria(this.materia);
		archivo.setDocumentoBinario(this.documentoBinario);
		archivo.setAdjuntadoPor(this.adjuntadoPor);
		return archivo;
	}

}
