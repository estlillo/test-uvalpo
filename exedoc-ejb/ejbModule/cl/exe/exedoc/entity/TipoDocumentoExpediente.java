package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity Tipo Documento Expediente.
 * 
 * @author Ricardo
 * 
 */
@Entity
@Table(name = "TIPOS_DOCUMENTOS_EXPEDIENTES")
public class TipoDocumentoExpediente implements Serializable {

	/**
	 * ORIGINAL.
	 */
	public static final Integer ORIGINAL = 1;
	/**
	 * RESPUESTA.
	 */
	public static final Integer RESPUESTA = 2;
	public static final Integer RESPUESTA_SOLICITUD_DE_RESPUESTA = 3;
	public static final Integer ANEXO = 4;

	private static final long serialVersionUID = -5278329910932223809L;
	private Integer id;
	private String descripcion;

	/**
	 * Constructor.
	 */
	public TipoDocumentoExpediente() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Integer}
	 */
	public TipoDocumentoExpediente(final Integer id) {
		this.id = id;
		switch (id) {
		case 1:
			this.descripcion = "ORIGINAL";
			break;
		case 2:
			this.descripcion = "RESPUESTA";
			break;
		case 3:
			this.descripcion = "RESPUESTA_SOLICITUD_DE_RESPUESTA";
			break;
		case 4:
			this.descripcion = "ANEXO";
			break;
		default:
			break;
		}
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	/**
	 * @param id {@link Integer}
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null) {
			if (obj instanceof TipoDocumentoExpediente) {
				final TipoDocumentoExpediente e = (TipoDocumentoExpediente) obj;
				if (this.id.equals(e.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(id);
		representacion.append("], [descripcion: ");
		representacion.append(descripcion);
		representacion.append("]");
		return representacion.toString();
	}
}
