package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SOLICITUD_VACACIONES")
@SequenceGenerator(name = "SEQ_SOLICITUD_VACACIONES", sequenceName = "SEQ_SOLICITUD_VACACIONES", allocationSize = 1, initialValue = 1)
@NamedQueries({ 
	@NamedQuery(name = "SolicitudVacaciones.findById", query = "SELECT sv FROM SolicitudVacaciones sv WHERE sv.id = :id"), 
	@NamedQuery(name = "SolicitudVacaciones.findByAll", query = "SELECT sv FROM SolicitudVacaciones sv"),
	@NamedQuery(name = "SolicitudVacaciones.findActiveSolicitud", query = "SELECT sv FROM SolicitudVacaciones sv WHERE sv.solicitante = :solicitante AND sv.aceptado IS NULL AND sv.rechazado IS NULL"),
	@NamedQuery(name = "SolicitudVacaciones.findActiveSolicitudExcludeId", query = "SELECT sv FROM SolicitudVacaciones sv WHERE sv.solicitante = :solicitante AND NOT sv.id = :id AND sv.aceptado IS NULL AND sv.rechazado IS NULL")
})
public class SolicitudVacaciones implements Serializable {

	private static final long serialVersionUID = 4015633889158096458L;
	
	public final static String SIN_ESTADO = "Sin Estado";

	public final static String APROBADO = "Aprobado";
	public final static String RECHAZADO = "Rechazado";

	private Long id;
	private Persona solicitante;
	private Integer cantDiasSolicitados;
	private Boolean aceptado;
	private Boolean rechazado;
	private Vacaciones vacaciones;
	private List<DetalleDiasVacaciones> listDetalleDiasVacaciones;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITUD_VACACIONES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITANTE")
	public Persona getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Persona solicitante) {
		this.solicitante = solicitante;
	}

	@Column(name = "ACEPTADO")
	public Boolean getAceptado() {
		return aceptado;
	}

	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}
	
	@Column(name = "RECHAZADO")
	public Boolean getRechazado() {
		return rechazado;
	}

	public void setRechazado(Boolean rechazado) {
		this.rechazado = rechazado;
	}
	
	@Column(name = "CANT_DIAS_SOLICITADOS")
	public Integer getCantDiasSolicitados() {
		return cantDiasSolicitados;
	}

	public void setCantDiasSolicitados(Integer cantDiasSolicitados) {
		this.cantDiasSolicitados = cantDiasSolicitados;
	}
	
	@OneToOne
	@JoinColumn(name = "ID_VACACIONES")
	public Vacaciones getVacaciones() {
		return vacaciones;
	}
	
	public void setVacaciones(Vacaciones vacaciones) {
		this.vacaciones = vacaciones;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitudVacaciones", cascade = CascadeType.ALL)
	public List<DetalleDiasVacaciones> getListDetalleDiasVacaciones() {
		return listDetalleDiasVacaciones;
	}
	
	public void setListDetalleDiasVacaciones(List<DetalleDiasVacaciones> listDetalleDiasVacaciones) {
		this.listDetalleDiasVacaciones = listDetalleDiasVacaciones;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof SolicitudVacaciones) {
			SolicitudVacaciones e = (SolicitudVacaciones) obj;
			if (this.id.equals(e.getId())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [solicitante: ");
		representacion.append(this.solicitante.getId());
		representacion.append("], [cantDiasSolicitados: ");
		representacion.append(this.cantDiasSolicitados);
		representacion.append("], [aceptado: ");
		representacion.append(this.aceptado);
		representacion.append("], [vacaciones: ");
		representacion.append(this.vacaciones.getId());
		representacion.append("]");
		return representacion.toString();
	}
}