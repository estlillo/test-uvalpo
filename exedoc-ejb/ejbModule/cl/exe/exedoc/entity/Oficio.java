package cl.exe.exedoc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

@Entity
@Name("OFICIO")
@DiscriminatorValue("20")
public class Oficio extends DocumentoElectronico {

	private static final long serialVersionUID = -1539577906625109423L;

	@Transient
	public List<Parrafo> getAntecedentesDelOficio() {
		List<Parrafo> tmp = new ArrayList<Parrafo>();
		for (Parrafo p : super.parrafos) {
			if (p instanceof AntecedentesDelOficio) {
				tmp.add(p);
			}
		}
		return tmp;
	}

	public void setAntecedentesDelOficio(List<Parrafo> antecedentes) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(antecedentes);
	}

	@Transient
	public List<Parrafo> getContenido() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Contenido) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setContenido(List<Parrafo> contenido) {
		/*if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}*/
		super.parrafos = contenido;
	}

}
