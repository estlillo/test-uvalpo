package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPOS_RAZON")
public class TipoRazon implements Serializable {

	private static final long serialVersionUID = 8686337452213172937L;

	private Integer id;
	private String descripcion;
	public static final Integer EXENTO = 1;
	public static final Integer TOMA_RAZON = 2;
	public static final Integer EXENTO_CON_REGISTRO = 3;

	public TipoRazon() {
	}

	public TipoRazon(Integer id) {
		this.id = id;
		switch (id) {
			case 1:
				this.descripcion = "EXENTO";
				break;
			case 2:
				this.descripcion = "TOMA DE RAZON";
				break;
			case 3:
				this.descripcion = "EXENTO CON REGISTRO";
				break;
		}
	}

	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
