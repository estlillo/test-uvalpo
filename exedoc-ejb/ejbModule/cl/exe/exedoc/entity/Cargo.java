package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "CARGOS")
@SequenceGenerator(name = "SEQ_CARGOS", sequenceName = "SEQ_CARGOS", allocationSize = 1, initialValue = 1)
@NamedQueries({ @NamedQuery(name = "Cargo.findById", query = "SELECT c FROM Cargo c WHERE c.id = :id"),
		@NamedQuery(name = "Cargo.findByAll", query = "SELECT c FROM Cargo c") })
public class Cargo implements Serializable {

	private static final long serialVersionUID = 8680430771869057945L;

	private Long id;
	private String descripcion;
	private UnidadOrganizacional unidadOrganizacional;
	private Boolean cargoDefault;
	private List<Persona> personas;

	public Cargo() {
		super();
	}

	public Cargo(final Long id) {
		super();
		this.id = id;
	}

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CARGOS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "ID_UNIDAD_ORGANIZACIONAL")
	public UnidadOrganizacional getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(UnidadOrganizacional unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cargo")
	public List<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	@Column(name = "CARGO_DEFAULT")
	public Boolean getCargoDefault() {
		return cargoDefault;
	}

	public void setCargoDefault(Boolean cargoDefault) {
		this.cargoDefault = cargoDefault;
	}

	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}

}
