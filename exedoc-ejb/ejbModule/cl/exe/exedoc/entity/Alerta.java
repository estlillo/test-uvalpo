package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity Alertas.
 * 
 * @author Pablo
 */
@Entity
@Table(name = "ALERTAS")
@SequenceGenerator(name = "SEQ_ALERTAS", sequenceName = "SEQ_ALERTAS", allocationSize = 1, initialValue = 1)
@NamedQueries({
	@NamedQuery(name = "Alerta.selectAll", query = "SELECT a FROM Alerta a WHERE a.eliminado is null "),
	@NamedQuery(name = "Alerta.findById", query = "SELECT a FROM Alerta a WHERE a.id = :id"),
	@NamedQuery(name = "Alerta.findByName", query = "SELECT a FROM Alerta a WHERE a.nombre LIKE :nombre"),
	@NamedQuery(name = "Alerta.findByLowerName", query = "SELECT a FROM Alerta a WHERE lower(a.nombre) LIKE :nombre"),
	@NamedQuery(name = "Alerta.findByLowerNameExcludeId", query = "SELECT a FROM Alerta a WHERE lower(a.nombre) LIKE :nombre AND a.id != :id")
})
public class Alerta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1478794002828661277L;

	private Long id;
	private Long plazo;
	private String nombre;
	private Boolean eliminado;

	/**
	 * Constructor.
	 */
	public Alerta() {
		super();
	}

	/**
	 * Retorna ID de la alerta.
	 * 
	 * @return {@link Long}
	 */
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ALERTAS")
	public Long getId() {
		return id;
	}

	/**
	 * Inserta ID de la alerta.
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Retorna plazo de dias de la alerta.
	 * 
	 * @return {@link Long}
	 */
	@Column(name = "PLAZO")
	public Long getPlazo() {
		return plazo;
	}

	/**
	 * Inserta plazo de dias de la alerta.
	 * 
	 * @param plazo
	 *            {@link Long}
	 */
	public void setPlazo(final Long plazo) {
		this.plazo = plazo;
	}

	/**
	 * Retorna nombre de la alerta.
	 * 
	 * @return {@link String}
	 */
	@Column(name = "NOMBRE")
	public String getNombre() {
		return nombre;
	}

	/**
	 * Inserta nombre de la alerta.
	 * 
	 * @param nombre
	 *            {@link String}
	 */
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Retorna si esta eliminado.
	 * 
	 * @return {@link Boolean}
	 */
	@Column(name = "ELIMINADO")
	public Boolean getEliminado() {
		
	    				
			 return eliminado;
	}
	/**
	 * Inserta valor si esta eliminado.
	 * 
	 * @param eliminado
	 *            {@link Boolean}
	 */

	public void setEliminado(Boolean eliminado) {
		

			 this.eliminado = eliminado;
		
	}

}
