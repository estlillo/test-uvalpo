package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

/**
 * Entity Departamento.
 * 
 * @author Ricardo
 * 
 */
@Entity
@Table(name = "DEPARTAMENTOS")
@SequenceGenerator(name = "SEQ_DEPARTAMENTOS", sequenceName = "SEQ_DEPARTAMENTOS", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "Departamento.findById", query = "SELECT d FROM Departamento d WHERE d.id = :id"),
		@NamedQuery(name = "Departamento.findByAll", query = "SELECT d FROM Departamento d") })
public class Departamento implements Serializable {

	private static final long serialVersionUID = -921763514733309256L;

	private Long id;
	private String descripcion;
	private String ciudad;
	private Boolean conCargo;
	private Boolean virtual;
	private Division division;
	private List<UnidadOrganizacional> unidades;

	/**
	 * Constructor.
	 */
	public Departamento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public Departamento(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTAMENTOS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "CON_CARGO", nullable = false)
	public Boolean getConCargo() {
		return conCargo;
	}

	/**
	 * @param conCargo
	 *            {@link Boolean}
	 */
	public void setConCargo(final Boolean conCargo) {
		this.conCargo = conCargo;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "VIRTUAL", nullable = false)
	public Boolean getVirtual() {
		return virtual;
	}

	/**
	 * @param virtual
	 *            {@link Boolean}
	 */
	public void setVirtual(final Boolean virtual) {
		this.virtual = virtual;
	}

	/**
	 * @return {@link Division}
	 */
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "ID_DIVISION", nullable = false)
	public Division getDivision() {
		return division;
	}

	/**
	 * @param division
	 *            {@link Division}
	 */
	public void setDivision(final Division division) {
		this.division = division;
	}

	/**
	 * @return {@link UnidadOrganizacional}
	 */
	@OneToMany(mappedBy = "departamento", fetch = FetchType.LAZY, cascade = {
			CascadeType.MERGE, CascadeType.PERSIST })
	public List<UnidadOrganizacional> getUnidades() {
		return unidades;
	}

	/**
	 * @param unidades
	 *            {@link UnidadOrganizacional}
	 */
	public void setUnidades(final List<UnidadOrganizacional> unidades) {
		this.unidades = unidades;
	}

	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}
	
	/**
	 * @return {@link String}
	 */
	@Column(name = "CIUDAD", length = 100) 
	@Length(max = 100)
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	

}
