package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("DistribucionDocumento")
@DiscriminatorValue("2")
public class DistribucionDocumento extends ListaPersonasDocumento implements Serializable {

	private static final long serialVersionUID = -4398651000303938400L;

}
