package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "UNIDADES")
@SequenceGenerator(name = "SEQ_UNIDADES", sequenceName = "SEQ_UNIDADES", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "UnidadOrganizacional.findById", query = "SELECT u FROM UnidadOrganizacional u WHERE u.id = :id"),
		@NamedQuery(name = "UnidadOrganizacional.findByAll", query = "SELECT u FROM UnidadOrganizacional u"),
		@NamedQuery(name = "UnidadOrganizacional.findByDescripcion", query = "SELECT u FROM UnidadOrganizacional u WHERE UPPER(u.descripcion) LIKE :descripcionMayus") })
public class UnidadOrganizacional implements Serializable {

	private static final long serialVersionUID = 6609357323262654198L;

	private Long id;
	private String descripcion;
	private Boolean virtual;
	private List<Cargo> cargos;
	private Departamento departamento;
	
	public UnidadOrganizacional() {
		super();
	}

	public UnidadOrganizacional(final Long id) {
		super();
		this.id = id;
	}

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UNIDADES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "VIRTUAL", nullable = false)
	public Boolean getVirtual() {
		return virtual;
	}

	public void setVirtual(Boolean virtual) {
		this.virtual = virtual;
	}

	@OneToMany(mappedBy = "unidadOrganizacional", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	public List<Cargo> getCargos() {
		return cargos;
	}

	public void setCargos(List<Cargo> cargos) {
		this.cargos = cargos;
	}

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "ID_DEPARTAMENTO", nullable = false)
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}
}
