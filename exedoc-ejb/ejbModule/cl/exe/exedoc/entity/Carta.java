package cl.exe.exedoc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Carta")
@DiscriminatorValue("15")
public class Carta extends DocumentoElectronico {

	private static final long serialVersionUID = 6533958909004394130L;

	@Transient
	public List<Parrafo> getContenido() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Contenido) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setContenido(List<Parrafo> parrafos) {
		super.parrafos = parrafos;
	}

	public void addContenido(Contenido contenido) {
		if (super.getParrafos() == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		if (contenido != null) {
			parrafos.add(contenido);
		}
	}

}
