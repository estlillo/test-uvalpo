package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("ArchivoDocumentoBinario")
@DiscriminatorValue("1")
public class ArchivoDocumentoBinario extends Archivo implements Serializable {

	private static final long serialVersionUID = 8756753000608676165L;
	
	public ArchivoDocumentoBinario archivo() {
		ArchivoDocumentoBinario archivo = new ArchivoDocumentoBinario();
		archivo.setId(super.getId());
		archivo.setCmsId(super.getCmsId());
		archivo.setFecha(super.getFecha());
		archivo.setNombreArchivo(super.getNombreArchivo());
		archivo.setContentType(super.getContentType());
		archivo.setArchivo(super.getArchivo());
		archivo.setIdNuevoArchivo(super.getIdNuevoArchivo());
		return archivo;
	}
	
}
