package cl.exe.exedoc.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public abstract class Solicitud extends DocumentoElectronico{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5273305782492746720L;
	
	private Persona funcionario;
	
	@ManyToOne
	@JoinColumn(name = "ID_FUNCIONARIO_VACACIONES", nullable = true)
	public Persona getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Persona funcionario) {
		this.funcionario = funcionario;
	}
	
}
