package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "VISACIONES")
@SequenceGenerator(name = "SEQ_VISACIONES", sequenceName = "SEQ_VISACIONES", allocationSize = 1, initialValue = 1)
public class VisacionDocumento implements Serializable {

	private static final long serialVersionUID = -4825958682502519757L;

	private Long id;
	private Documento documento;
	private Persona persona;
	private Date fechaVisacion;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_VISACIONES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Column(name = "FECHA_VISACION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaVisacion() {
		return fechaVisacion;
	}

	public void setFechaVisacion(Date fechaVisacion) {
		this.fechaVisacion = fechaVisacion;
	}

}
