package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "PLANTILLAS")
@SequenceGenerator(name = "SEQ_PLANTILLAS", sequenceName = "SEQ_PLANTILLAS", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "Plantilla.findById", query = "SELECT p FROM Plantilla p WHERE p.id = :id"),
		@NamedQuery(name = "Plantilla.findByAll", query = "SELECT p FROM Plantilla p"),
		@NamedQuery(name = "Plantilla.findByTipoPlantilla", query = "SELECT p FROM Plantilla p WHERE p.tipoPlantilla.id = :tp")
})
public class Plantilla implements Serializable {

	private static final long serialVersionUID = -6170693312128440137L;

//	public static final Integer PLANTILLA_CONTRATACION = 1;
//	public static final Integer PLANTILLA_ADJUDICACION = 2;
//	public static final Integer PLANTILLA_DIAS_ADMINISTRATIVOS_S_REM = 3;
//	public static final Integer PLANTILLA_DIAS_ADMINISTRATIVOS_C_REM = 4;
//	public static final Integer PLANTILLA_FERIADO = 5;
//	public static final Integer PLANTILLA_CONCURSO = 6;

	private Integer id;
	private String nombre;
	private List<CamposPlantilla> camposPlantilla;
	private TipoPlantilla tipoPlantilla; 

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLANTILLAS")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NOMBRE", length = 100)
	@Length(max = 100)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plantilla", cascade = CascadeType.PERSIST)
	public List<CamposPlantilla> getCamposPlantilla() {
		return camposPlantilla;
	}

	public void setCamposPlantilla(List<CamposPlantilla> camposPlantilla) {
		this.camposPlantilla = camposPlantilla;
	}

	@OneToOne
	@JoinColumn(name = "ID_TIPO_PLANTILLA", nullable = true)
	public TipoPlantilla getTipoPlantilla() {
		return tipoPlantilla;
	}

	public void setTipoPlantilla(TipoPlantilla tipoPlantilla) {
		this.tipoPlantilla = tipoPlantilla;
	}
	
}
