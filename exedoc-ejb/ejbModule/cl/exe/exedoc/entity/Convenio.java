package cl.exe.exedoc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.validator.Length;
import org.jboss.seam.annotations.Name;

@Entity
@Name("CONVENIO")
@DiscriminatorValue("22")
public class Convenio extends DocumentoElectronico {

	private static final long serialVersionUID = 7160361496887585753L;

	private String titulo;
	private String prologo;
	private Persona responsable;
	//private ClasificacionTipoOtros clasificacionTipoOtros;
	//private ClasificacionSubtipoOtros clasificacionSubtipoOtros;

	@Column(name = "TITULO", length = 250)
	@Length(max = 250)
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Column(name = "PROLOGO", length = 2000)
	@Length(max = 2000)
	public String getPrologo() {
		return prologo;
	}

	public void setPrologo(String prologo) {
		this.prologo = prologo;
	}

	@ManyToOne
	@JoinColumn(name = "ID_RESPONSABLE")
	public Persona getResponsable() {
		return responsable;
	}

	public void setResponsable(Persona responsable) {
		this.responsable = responsable;
	}
	
	/*@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_TIPO_OTROS", nullable = true)
	public ClasificacionTipoOtros getClasificacionTipoOtros() {
		return clasificacionTipoOtros;
	}

	public void setClasificacionTipoOtros(ClasificacionTipoOtros clasificacionTipoOtros) {
		this.clasificacionTipoOtros = clasificacionTipoOtros;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_SUBTIPO_OTROS", nullable = true)
	public ClasificacionSubtipoOtros getClasificacionSubtipoOtros() {
		return clasificacionSubtipoOtros;
	}

	public void setClasificacionSubtipoOtros(ClasificacionSubtipoOtros clasificacionSubtipoOtros) {
		this.clasificacionSubtipoOtros = clasificacionSubtipoOtros;
	}*/

	@Transient
	public List<Parrafo> getConsiderandos() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Considerandos) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setConsiderandos(List<Parrafo> considerandos) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(considerandos);
	}

	@Transient
	public List<Parrafo> getTeniendoPresente() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof TeniendoPresente) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setTeniendoPresente(List<Parrafo> teniendoPresente) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(teniendoPresente);
	}

	@Transient
	public List<Parrafo> getConvienen() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Convienen) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setConvienen(List<Parrafo> convienen) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(convienen);
	}

}
