package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "TIPO_CONTENIDO")
@SequenceGenerator(name = "SEQ_TIPO_CONTENIDO", sequenceName = "SEQ_TIPO_CONTENIDO", allocationSize = 1, initialValue = 1)
@NamedQueries( { @NamedQuery(name = "TipoContenido.findById", query = "SELECT tc FROM TipoContenido tc WHERE tc.id = :id"),
		@NamedQuery(name = "TipoContenido.findByAll", query = "SELECT tc FROM TipoContenido tc") })
public class TipoContenido implements Serializable {

	private static final long serialVersionUID = -660237921716511585L;

	private Integer id;
	private String contenido;

	// resolucion
	public static final Integer TIPO = 1;
	public static final Integer MATERIA = 2;
	public static final Integer VISTOS = 3;
	public static final Integer CONSIDERANDO = 4;
	public static final Integer RESUELVO = 5;
	public static final Integer INDICACION = 6;
	
	// convenio
	public static final Integer TITULO = 7;
	public static final Integer PROLOGO = 8;
	public static final Integer TENIENDO_PRESENTE = 9;
	public static final Integer CONVIENEN = 10;
	
	// contrato
	public static final Integer PUNTOS = 11;

	public TipoContenido() {
	}

	public TipoContenido(Integer id) {
		this.id = id;
		switch (id) {
		case 1:
			this.contenido = "TIPO";
			break;
		case 2:
			this.contenido = "MATERIA";
			break;
		case 3:
			this.contenido = "VISTOS";
			break;
		case 4:
			this.contenido = "CONSIDERANDO";
			break;
		case 5:
			this.contenido = "RESUELVO";
			break;
		case 6:
			this.contenido = "INDICACION";
			break;
		case 7:
			this.contenido = "TITULO";
			break;
		case 8:
			this.contenido = "PROLOGO";
			break;
		case 9:
			this.contenido = "TENIENDO PRESENTE";
			break;
		case 10:
			this.contenido = "CONVIENEN";
			break;
		case 11:
			this.contenido = "PUNTOS";
			break;
		}
	}

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TIPO_CONTENIDO")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CONTENIDO", length = 30)
	@Length(max = 30)
	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

}
