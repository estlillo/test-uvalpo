package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Resuelvo")
@DiscriminatorValue("6")
public class Resuelvo extends Parrafo implements Serializable{

	private static final long serialVersionUID = -5953370698112933993L;

}
