package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

import cl.exe.exedoc.repositorio.ResolucionConverter;

/**
 * Entity Documento.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "DOCUMENTOS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ID_FORMATO_DOCUMENTO", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "SEQ_DOCUMENTOS", sequenceName = "SEQ_DOCUMENTOS", allocationSize = 1, initialValue = 1)
public class Documento implements Serializable {

	private static final long serialVersionUID = 8014464843879780413L;

	private Long id;
	private String numeroDocumento = "s/n";
	private Date fechaDocumentoOrigen;
	private String materia = "s/m";
	private FormatoDocumento formatoDocumento;
	private Boolean completado;
	private Date plazo;
	private Boolean reservado;
	private TipoDocumento tipoDocumento;
	private Date fechaCreacion;
	private EstadoDocumento estado;
	private List<VisacionDocumento> visaciones;
	private List<FirmaDocumento> firmas;
	private List<RevisarDocumentos> revisar;
	private List<Bitacora> bitacoras;
	private String emisor;
	private Long idEmisor;
	protected List<ListaPersonasDocumento> listaPersonas;
	private String antecedentes;
	private Persona autor;
	private String cmsId;
	private List<VisacionEstructuradaDocumento> visacionesEstructuradas;
	private List<FirmaEstructuradaDocumento> firmasEstructuradas;
	private List<RevisarEstructuradaDocumento> revisarEstructuradas;
	private Boolean desierto;
	private String codigoDescarga;
	//private Boolean respuestaSolicitud;
	// @Transient
	private Integer idNuevoDocumento;
	private TipoDocumentoExpediente tipoDocumentoExpediente;
	private Boolean enEdicion;
	private Boolean eliminable;
	private Boolean eliminado;
	private Long codigoBarra;
	
	private Alerta alerta;

	/**
	 * Constructor.
	 */
	public Documento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public Documento(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOCUMENTOS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NUMERO_DOCUMENTO")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento {@link String}
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return {@link String}
	 */
	@Transient
	public String getNumeroNumeroDocumento() {
		return (numeroDocumento.split("/"))[0];
	}

	/**
	 * @return {@link String}
	 */
	@Transient
	public String getAgnoNumeroDocumento() {
		return (numeroDocumento.split("/"))[1];
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_DOCUMENTO_ORIGEN")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaDocumentoOrigen() {
		return fechaDocumentoOrigen;
	}

	/**
	 * @param fechaDocumentoOrigen {@link Date}
	 */
	public void setFechaDocumentoOrigen(final Date fechaDocumentoOrigen) {
		this.fechaDocumentoOrigen = fechaDocumentoOrigen;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "MATERIA", length = 2000)
	@Length(max = 2000)
	public String getMateria() {
		return materia;
	}

	/**
	 * @param materia {@link String}
	 */
	public void setMateria(final String materia) {
		this.materia = materia;
	}

	/**
	 * @return {@link String}
	 */
	@Transient
	public String getShortMateria() {
		if (materia.length() > 100) {
			return materia.substring(0, 99).concat("...");
		} else {
			return materia;
		}
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "ANTECEDENTES", length = 2000)
	@Length(max = 2000)
	public String getAntecedentes() {
		return antecedentes;
	}
	
	@Transient
	public String getAntecedentesCorto() {
		if (antecedentes != null ) 
			return antecedentes.substring(0, this.antecedentes.length() >= 20 ? 20 : this.antecedentes.length()).concat(this.antecedentes.length() >= 20 ? "...." : "");
		return "";
	}

	/**
	 * @param antecedentes {@link String}
	 */
	public void setAntecedentes(final String antecedentes) {
		this.antecedentes = antecedentes;
	}

	/**
	 * @return {@link FormatoDocumento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_FORMATO", nullable = true)
	public FormatoDocumento getFormatoDocumento() {
		return formatoDocumento;
	}

	/**
	 * @param formatoDocumento {@link FormatoDocumento}
	 */
	public void setFormatoDocumento(final FormatoDocumento formatoDocumento) {
		this.formatoDocumento = formatoDocumento;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "PLAZO", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getPlazo() {
		return plazo;
	}

	/**
	 * @param plazo {@link Date}
	 */
	public void setPlazo(final Date plazo) {
		this.plazo = plazo;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "COMPLETADO")
	public Boolean getCompletado() {
		return completado;
	}

	/**
	 * @param completado {@link Boolean}
	 */
	public void setCompletado(final Boolean completado) {
		this.completado = completado;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "RESERVADO")
	public Boolean getReservado() {
		return reservado;
	}

	/**
	 * @param reservado {@link Boolean}
	 */
	public void setReservado(final Boolean reservado) {
		this.reservado = reservado;
	}

	/**
	 * @return {@link TipoDocumento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_TIPO_DOCUMENTO", nullable = true)
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento {@link TipoDocumento}
	 */
	public void setTipoDocumento(final TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_CREACION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion {@link Date}
	 */
	public void setFechaCreacion(final Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return {@link List} of {@link FirmaDocumento}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<FirmaDocumento> getFirmas() {
		return firmas;
	}

	/**
	 * @param firmas {@link List} of {@link FirmaDocumento}
	 */
	public void setFirmas(final List<FirmaDocumento> firmas) {
		if(this.firmas== null){
			this.firmas = new ArrayList<FirmaDocumento>();
		}
		if(firmas !=null){
					this.firmas = firmas;
		}
	}

	/**
	 * @return {@link List} of {@link VisacionDocumento}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<VisacionDocumento> getVisaciones() {
		return visaciones;
	}

	/**
	 * @param visaciones {@link List} of {@link VisacionDocumento}
	 */
	public void setVisaciones(final List<VisacionDocumento> visaciones) {
		this.visaciones = visaciones;
	}

	/**
	 * @return {@link RevisarDocumentos}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<RevisarDocumentos> getRevisar() {
		return revisar;
	}

	/**
	 * @param revisar {@link RevisarDocumentos}
	 */
	public void setRevisar(final List<RevisarDocumentos> revisar) {
		this.revisar = revisar;
	}

	/**
	 * @return {@link List} of {@link Bitacora}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<Bitacora> getBitacoras() {
		return bitacoras;
	}

	/**
	 * @param bitacoras {@link List} of {@link Bitacora}
	 */
	public void setBitacoras(final List<Bitacora> bitacoras) {
		this.bitacoras = bitacoras;
	}

	/**
	 * @param bitacora {@link Bitacora}
	 */
	@Transient
	public void addBitacora(final Bitacora bitacora) {
		if (bitacoras == null) {
			bitacoras = new ArrayList<Bitacora>();
		}
		bitacoras.add(bitacora);
	}

	/**
	 * @return {@link EstadoDocumento}
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ESTADO_DOCUMENTO")
	public EstadoDocumento getEstado() {
		return estado;
	}

	/**
	 * @param estado {@link EstadoDocumento}
	 */
	public void setEstado(final EstadoDocumento estado) {
		this.estado = estado;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "EMISOR")
	public String getEmisor() {
		return emisor;
	}
	
	@Transient
	public String getShortEmisor() {
		return this.emisor.substring(0, this.emisor.length() >= 30 ? 30 : this.emisor.length()).concat(this.emisor.length() >= 30 ? "...." : "");
	}

	/**
	 * @param emisor {@link String}
	 */
	public void setEmisor(final String emisor) {
		this.emisor = emisor;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID_EMISOR")
	public Long getIdEmisor() {
		return idEmisor;
	}

	/**
	 * @param idEmisor {@link Long}
	 */
	public void setIdEmisor(final Long idEmisor) {
		this.idEmisor = idEmisor;
	}

	/**
	 * @return {@link List} of {@link ListaPersonasDocumento}
	 */
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<ListaPersonasDocumento> getListaPersonas() {
		return listaPersonas;
	}

	/**
	 * @param listaPersonas {@link List} of {@link ListaPersonasDocumento}
	 */
	public void setListaPersonas(final List<ListaPersonasDocumento> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	/**
	 * @return {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public List<ListaPersonasDocumento> getDestinatarios() {
		if (this.getListaPersonas() != null) {
			final List<ListaPersonasDocumento> listDestinatarios = new ArrayList<ListaPersonasDocumento>();
			for (ListaPersonasDocumento lpd : this.getListaPersonas()) {
				if (lpd instanceof DestinatarioDocumento) {
					if(!this.esta(listDestinatarios, lpd.getDestinatario())){
						listDestinatarios.add(lpd);
					}
				}
			}
			return listDestinatarios;
		}
		return null;
	}

	public boolean esta(List<ListaPersonasDocumento> lista, String descripcion){
		boolean flag = false;
		
		for (ListaPersonasDocumento listaPersonasDocumento : lista) {
			if(listaPersonasDocumento.getDestinatario().equals(descripcion))flag = true;
		}
		return flag;
	}
	
	/**
	 * @return {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public List<ListaPersonasDocumento> getDistribucionDocumento() {
		if (this.getListaPersonas() != null) {
			final List<ListaPersonasDocumento> listDestinatarios = new ArrayList<ListaPersonasDocumento>();
			for (ListaPersonasDocumento lpd : this.getListaPersonas()) {
				if (lpd instanceof DistribucionDocumento) {
					if(!listDestinatarios.contains(lpd))listDestinatarios.add(lpd);
				}
			}
			return listDestinatarios;
		}
		return null;
	}
	
	/**
	 * Método agrega la lista de distribución sin validación, para utilizarse en la clase {@link ResolucionConverter}.
	 * 
	 * @return {@link List}<{@link ListaPersonasDocumento}>
	 */
	@Transient
	public List<ListaPersonasDocumento> getDistribucionDocumentoAll() {
		if (this.getListaPersonas() != null) {
			final List<ListaPersonasDocumento> listDestinatarios = new ArrayList<ListaPersonasDocumento>();
			for (ListaPersonasDocumento lpd : this.getListaPersonas()) {
				if (lpd instanceof DistribucionDocumento) {
					listDestinatarios.add(lpd);
				}
			}
			return listDestinatarios;
		}
		return null;
	}

	/**
	 * @param destinatarios {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public void addDestinatarios(final List<ListaPersonasDocumento> destinatarios) {
		if (this.listaPersonas == null) {
			this.listaPersonas = new ArrayList<ListaPersonasDocumento>();
		}
		this.listaPersonas.addAll(destinatarios);
	}

	/**
	 * @param destinatarios {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public void setDestinatarios(final List<ListaPersonasDocumento> destinatarios) {
		if (this.listaPersonas == null) {
			this.listaPersonas = new ArrayList<ListaPersonasDocumento>();
		}
		for (final Iterator<ListaPersonasDocumento> lista = listaPersonas.iterator(); lista.hasNext();) {
			if (lista.next() instanceof DestinatarioDocumento) {
				lista.remove();
			}
		}
		if (destinatarios != null) {
			//this.listaPersonas = new ArrayList<ListaPersonasDocumento>();
			this.listaPersonas.addAll(destinatarios);
		}
	}

	/**
	 * @param distribucion {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public void addDistribucion(final List<ListaPersonasDocumento> distribucion) {
		if (this.listaPersonas == null) {
			this.listaPersonas = new ArrayList<ListaPersonasDocumento>();
		}
		this.listaPersonas.addAll(distribucion);
	}

	/**
	 * @param distribucion {@link List} of {@link ListaPersonasDocumento}
	 */
	@Transient
	public void setDistribucion(final List<ListaPersonasDocumento> distribucion) {
		if (this.listaPersonas == null) {
			this.listaPersonas = new ArrayList<ListaPersonasDocumento>();
		}
		for (final Iterator<ListaPersonasDocumento> lista = listaPersonas.iterator(); lista.hasNext();) {
			if (lista.next() instanceof DistribucionDocumento) {
				lista.remove();
			}
		}
		if (distribucion != null) {
			this.listaPersonas.addAll(distribucion);
		}
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_AUTOR", nullable = true)
	public Persona getAutor() {
		return autor;
	}

	/**
	 * @param autor {@link Persona}
	 */
	public void setAutor(final Persona autor) {
		this.autor = autor;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "CMS_DOCID")
	public String getCmsId() {
		return cmsId;
	}

	/**
	 * @param cmsId {@link String}
	 */
	public void setCmsId(final String cmsId) {
		this.cmsId = cmsId;
	}

	/**
	 * @return {@link List} of {@link VisacionEstructuradaDocumento}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<VisacionEstructuradaDocumento> getVisacionesEstructuradas() {
		return visacionesEstructuradas;
	}

	/**
	 * @param visacionesEstructuradas {@link List} of {@link VisacionEstructuradaDocumento}
	 */
	public void setVisacionesEstructuradas(final List<VisacionEstructuradaDocumento> visacionesEstructuradas) {
		this.visacionesEstructuradas = visacionesEstructuradas;
	}

	/**
	 * @return {@link List} of {@link FirmaEstructuradaDocumento}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<FirmaEstructuradaDocumento> getFirmasEstructuradas() {
		return firmasEstructuradas;
	}

	/**
	 * @param firmasEstructuradas {@link List} of {@link FirmaEstructuradaDocumento}
	 */
	public void setFirmasEstructuradas(final List<FirmaEstructuradaDocumento> firmasEstructuradas) {
		this.firmasEstructuradas = firmasEstructuradas;
	}

	/**
	 * @return {@link RevisarEstructuradaDocumento}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<RevisarEstructuradaDocumento> getRevisarEstructuradas() {
		return revisarEstructuradas;
	}

	/**
	 * @param revisarEstructuradas {@link RevisarEstructuradaDocumento}
	 */
	public void setRevisarEstructuradas(final List<RevisarEstructuradaDocumento> revisarEstructuradas) {
		if(this.revisarEstructuradas== null)this.revisarEstructuradas = new ArrayList<RevisarEstructuradaDocumento>();
		if(revisarEstructuradas !=null){
			for (RevisarEstructuradaDocumento revisarEstructuradaDocumento : revisarEstructuradas) {
				boolean agregar = true;
				for(RevisarEstructuradaDocumento revisarEstructuradaDocumentoPersona :this.revisarEstructuradas){
					if(revisarEstructuradaDocumentoPersona.getPersona().equals(revisarEstructuradaDocumento.getPersona()))agregar = false;
				}
				if(agregar){
					this.revisarEstructuradas.add(revisarEstructuradaDocumento);
				}
			}
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null) {
			if (obj instanceof Documento) {
				final Documento doc = (Documento) obj;
				if (this.id != null && doc.getId() != null) {
					if (this.id.equals(doc.getId())) { return true; }
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [numeroDocumento: ");
		representacion.append(this.numeroDocumento);
		representacion.append("], [fechaDocumentoOrigen: ");
		representacion.append(this.fechaDocumentoOrigen);
		representacion.append("], [materia: ");
		representacion.append(this.materia);
		representacion.append("], [formatoDocumento: ");
		representacion.append(this.formatoDocumento);
		representacion.append("], [plazo: ");
		representacion.append(this.plazo);
		representacion.append("], [reservado: ");
		representacion.append(this.reservado);
		representacion.append("], [tipoDocumento: ");
		representacion.append(this.tipoDocumento);
		representacion.append("], [fechaCreacion: ");
		representacion.append(this.fechaCreacion);
		representacion.append("], [estado: ");
		representacion.append(this.estado);
		representacion.append("], [emisor: ");
		representacion.append(this.emisor);
		representacion.append("], [destinatarios: ");
		representacion.append(this.listaPersonas);
		representacion.append("], [antecedentes: ");
		representacion.append(this.antecedentes);
		return representacion.toString();
	}

	/**
	 * @return {@link Integer}
	 */
	@Transient
	public Integer getIdNuevoDocumento() {
		return idNuevoDocumento;
	}

	/**
	 * @param idNuevoDocumento Integer
	 */
	public void setIdNuevoDocumento(final Integer idNuevoDocumento) {
		this.idNuevoDocumento = idNuevoDocumento;
	}

	/**
	 * @return {@link TipoDocumentoExpediente}
	 */
	@Transient
	public TipoDocumentoExpediente getTipoDocumentoExpediente() {
		return tipoDocumentoExpediente;
	}

	/**
	 * @param tipoDocumentoExpediente {@link TipoDocumentoExpediente}
	 */
	public void setTipoDocumentoExpediente(final TipoDocumentoExpediente tipoDocumentoExpediente) {
		this.tipoDocumentoExpediente = tipoDocumentoExpediente;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public Boolean getEnEdicion() {
		return enEdicion;
	}

	/**
	 * @param enEdicion {@link Boolean}
	 */
	public void setEnEdicion(final Boolean enEdicion) {
		this.enEdicion = enEdicion;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public Boolean getEliminable() {
		return eliminable;
	}

	/**
	 * @param eliminable {@link Boolean}
	 */
	public void setEliminable(final Boolean eliminable) {
		this.eliminable = eliminable;
	}

	/**
	 * @return {@link Boolean}
	 */
//	@Column(name = "DESIERTO")
//	public Boolean getDesierto() {
//		return desierto;
//	}
//
//	/**
//	 * @param desierto {@link Boolean}
//	 */
//	public void setDesierto(final Boolean desierto) {
//		this.desierto = desierto;
//	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "ELIMINADO")
	public Boolean getEliminado() {
		return eliminado;
	}

	/**
	 * @param eliminado {@link Boolean}
	 */
	public void setEliminado(final Boolean eliminado) {
		this.eliminado = eliminado;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "CODIGO_DESCARGA", nullable = true, length = 10)
	@Length(max = 10)
	public String getCodigoDescarga() {
		return codigoDescarga;
	}

	/**
	 * @param codigoDescarga {@link String}
	 */
	public void setCodigoDescarga(final String codigoDescarga) {
		this.codigoDescarga = codigoDescarga;
	}

	/**
	 * @return {@link Alerta}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_ALERTA")
	public Alerta getAlerta() {
		return alerta;
	}

	/**
	 * @param alerta {@link Alerta}
	 */
	public void setAlerta(final Alerta alerta) {
		this.alerta = alerta;
	}
	
	/**
	 * @return {@link Long}
	 */
	@Column(name = "CODIGO_BARRA")
	public Long getCodigoBarra() {
		return codigoBarra;
	}

	/**
	 * @param codigoBarra {@link Long}
	 */
	public void setCodigoBarra(final Long codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	/**
	 * @return {@link DocumentoPapel}
	 */
	@Transient
	public DocumentoPapel getDocumentoPapel() {
		final DocumentoPapel dp = new DocumentoPapel();
		dp.setId(this.id);
		dp.setNumeroDocumento(this.numeroDocumento);
		dp.setFechaDocumentoOrigen(this.fechaDocumentoOrigen);
		dp.setMateria(this.materia);
		dp.setFormatoDocumento(this.formatoDocumento);
		dp.setPlazo(this.plazo);
		dp.setReservado(this.reservado);
		dp.setTipoDocumento(this.tipoDocumento);
		dp.setEstado(this.estado);
		dp.setVisaciones(this.visaciones);
		dp.setFirmas(this.firmas);
		dp.setEmisor(this.emisor);
		dp.setListaPersonas(this.listaPersonas);
		dp.setAntecedentes(this.antecedentes);
		dp.setAutor(this.autor);
		dp.setCmsId(this.cmsId);
		dp.setIdNuevoDocumento(this.idNuevoDocumento);
		dp.setTipoDocumentoExpediente(this.tipoDocumentoExpediente);
		dp.setEnEdicion(this.enEdicion);
		dp.setEliminable(this.eliminable);

		return dp;
	}

	/**
	 * @return {@link DocumentoBinario}
	 */
	@Transient
	public DocumentoBinario getDocumentoBinario() {
		final DocumentoBinario dp = new DocumentoBinario();
		dp.setId(this.id);
		dp.setNumeroDocumento(this.numeroDocumento);
		dp.setFechaDocumentoOrigen(this.fechaDocumentoOrigen);
		dp.setMateria(this.materia);
		dp.setFormatoDocumento(this.formatoDocumento);
		dp.setPlazo(this.plazo);
		dp.setReservado(this.reservado);
		dp.setTipoDocumento(this.tipoDocumento);
		dp.setEstado(this.estado);
		dp.setVisaciones(this.visaciones);
		dp.setFirmas(this.firmas);
		dp.setEmisor(this.emisor);
		dp.setListaPersonas(this.listaPersonas);
		dp.setAntecedentes(this.antecedentes);
		dp.setAutor(this.autor);
		dp.setCmsId(this.cmsId);
		dp.setIdNuevoDocumento(this.idNuevoDocumento);
		dp.setTipoDocumentoExpediente(this.tipoDocumentoExpediente);
		dp.setEnEdicion(this.enEdicion);
		dp.setEliminable(this.eliminable);

		return dp;
	}

}
