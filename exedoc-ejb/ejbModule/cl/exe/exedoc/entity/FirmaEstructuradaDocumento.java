package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FIRMAS_ESTRUCTURADAS")
@SequenceGenerator(name = "SEQ_FIRMAS_ESTRUCTURADAS", sequenceName = "SEQ_FIRMAS_ESTRUCTURADAS", allocationSize = 1, initialValue = 1)
public class FirmaEstructuradaDocumento implements Serializable, Comparable<FirmaEstructuradaDocumento> {

	private static final long serialVersionUID = 1762502624031851570L;

	private Long id;
	private Documento documento;
	private Persona persona;
	private Integer orden;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_FIRMAS_ESTRUCTURADAS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Column(name = "ORDEN")
	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof FirmaEstructuradaDocumento) { 
			return this.getId().equals(((FirmaEstructuradaDocumento) obj).getId()); 
		}
		return false;
	}

	public int compareTo(FirmaEstructuradaDocumento o) {
		return this.orden.compareTo(o.getOrden());
	}
}
