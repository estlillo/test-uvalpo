package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity Formato Documento.
 * @author Ricardo
 *
 */
@Entity
@Table(name = "FORMATOS_DOCUMENTOS")
public class FormatoDocumento implements Serializable {

	/**
	 * PAPEL.
	 */
	public static final Integer PAPEL = 1;
	/**
	 * DIGITAL.
	 */
	public static final Integer DIGITAL = 2;
	/**
	 * ELECTRONICO.
	 */
	public static final Integer ELECTRONICO = 3;

	private static final long serialVersionUID = -1037026810473317732L;
	private Integer id;
	private String descripcion;

	/**
	 * Constructor.
	 */
	public FormatoDocumento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Integer}
	 */
	public FormatoDocumento(final Integer id) {
		this.id = id;
		switch (id) {
			case 1:
				this.descripcion = "PAPEL";
				break;
			case 2:
				this.descripcion = "DIGITAL";
				break;
			case 3:
				this.descripcion = "ELECTRONICO";
				break;
			default:
				this.descripcion = "-";
		}
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	/**
	 * @param id {@link Integer}
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}
}
