package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Vistos")
@DiscriminatorValue("3")
public class Vistos extends Parrafo implements Serializable {

	private static final long serialVersionUID = 4741198899946624957L;

}
