package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;

@Entity
@Table(name = "CIUDADANOS")
@SequenceGenerator(name = "SEQ_CIUDADANOS", sequenceName = "SEQ_CIUDADANOS", allocationSize = 1, initialValue = 1)
public class Ciudadano implements Serializable {

	private static final long serialVersionUID = -3939844540252515534L;
	
	private Long id;
	private Long idUsuarioSgs;
	
	//Datos personales
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	
	//Datos opcionales
	private String run;
	private String email;
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CIUDADANOS")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "ID_USUARIO_SGS")
	public Long getIdUsuarioSgs() {
		return idUsuarioSgs;
	}
	
	public void setIdUsuarioSgs(Long idUsuarioSgs) {
		this.idUsuarioSgs = idUsuarioSgs;
	}
	
	@Column(name = "NOMBRES", length = 100)
	@Length(max = 100)
	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	@Column(name = "APELLIDO_PATERNO", length = 100)
	@Length(max = 100)
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	
	@Column(name = "APELLIDO_MATERNO", length = 100)
	@Length(max = 100)
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	@Column(name = "RUN", length = 15)
	@Length(max = 15)
	public String getRun() {
		return run;
	}
	
	public void setRun(String run) {
		this.run = run;
	}
	
	@Column(name = "EMAIL", length = 255)
	@Length(max = 255)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Transient
	public String getNombreCompleto(){
		return getNombres() + " " + getApellidoPaterno() + " " + getApellidoMaterno();
	}
}
