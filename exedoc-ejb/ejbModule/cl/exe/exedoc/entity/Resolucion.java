package cl.exe.exedoc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

@Entity
@Name("RESOLUCION")
@DiscriminatorValue("25")
public class Resolucion extends DocumentoElectronico {

	private static final long serialVersionUID = 9142368640255863058L;
	/*private Resolucion resolucionHija;
	private List<Resolucion> resolucionesPadres;*/
	private String indicaciones;
	// private List<ArchivoResolucion> archivosAdjuntos;
	private TipoRazon tipo;
	//private ClasificacionTipo clasificacionTipo;
	//private ClasificacionSubtipo clasificacionSubtipo;
//	private Boolean antigua = false;
	private String idDocumentoMI;
	private String agnoNumeroDocumento;
	
	//private List<Resolucion> resoluciones;

//	@Column(name = "ANTIGUA")
//	public Boolean getAntigua() {
//		return antigua;
//	}
//
//	public void setAntigua(Boolean antigua) {
//		this.antigua = antigua;
//	}

	@Column(name = "INDICACIONES")
	public String getIndicaciones() {
		return indicaciones;
	}

	public void setIndicaciones(String indicaciones) {
		this.indicaciones = indicaciones;
	}

	/*
	 * @OneToMany(fetch = FetchType.LAZY, mappedBy = "resolucion",
	 * cascade=CascadeType.ALL) public List<ArchivoResolucion>
	 * getArchivosAdjuntos() { return archivosAdjuntos; }
	 * 
	 * public void setArchivosAdjuntos(List<ArchivoResolucion> archivosAdjuntos)
	 * { this.archivosAdjuntos = archivosAdjuntos; }
	 */

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_RAZON", nullable = true)
	public TipoRazon getTipo() {
		return tipo;
	}

	public void setTipo(TipoRazon tipo) {
		this.tipo = tipo;
	}

	/*@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_TIPO", nullable = true)
	public ClasificacionTipo getClasificacionTipo() {
		return clasificacionTipo;
	}

	public void setClasificacionTipo(ClasificacionTipo clasificacionTipo) {
		this.clasificacionTipo = clasificacionTipo;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_SUBTIPO", nullable = true)
	public ClasificacionSubtipo getClasificacionSubtipo() {
		return clasificacionSubtipo;
	}

	public void setClasificacionSubtipo(ClasificacionSubtipo clasificacionSubtipo) {
		this.clasificacionSubtipo = clasificacionSubtipo;
	}*/

	@Column(name = "ID_DOCUMENTO_MI")
	public String getIdDocumentoMI() {
		return idDocumentoMI;
	}

	public void setIdDocumentoMI(String idDocumentoMI) {
		this.idDocumentoMI = idDocumentoMI;
	}

	@Column(name = "AGNO_NUMERO_DOCUMENTO")
	public String getAgnoNumeroDocumento() {
		return agnoNumeroDocumento;
	}

	public void setAgnoNumeroDocumento(String agnoNumeroDocumento) {
		this.agnoNumeroDocumento = agnoNumeroDocumento;
	}

	@Transient
	public List<Parrafo> getVistos() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Vistos) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setVistos(List<Parrafo> vistos) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(vistos);
	}

	@Transient
	public List<Parrafo> getConsiderandos() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Considerandos) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setConsiderandos(List<Parrafo> considerandos) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(considerandos);
	}

	@Transient
	public List<Parrafo> getTeniendoPresente() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof TeniendoPresente) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setTeniendoPresente(List<Parrafo> teniendoPresente) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(teniendoPresente);
	}

	@Transient
	public List<Parrafo> getResuelvo() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Resuelvo) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}
 
	public void setResuelvo(List<Parrafo> resuelvo) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(resuelvo);
	}
	
//	@ManyToMany
//	@JoinTable(name = "LISTA_RESOLUCIONES", joinColumns = @JoinColumn(name = "ID_PADRE", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_HIJA", referencedColumnName = "ID"))
//	public List<Resolucion> getResoluciones() {
//		return resoluciones;
//	}
//
//	public void setResoluciones(List<Resolucion> resoluciones) {
//		this.resoluciones = resoluciones;
//	}

	/*@ManyToOne
	@JoinColumn(name = "id_resolucion_hija", nullable = true)
	public Resolucion getResolucionHija() {
		return resolucionHija;
	}

	public void setResolucionHija(Resolucion resolucionHija) {
		this.resolucionHija = resolucionHija;
	}

	@OneToMany(mappedBy = "resolucionHija")
	public List<Resolucion> getResolucionesPadres() {
		return resolucionesPadres;
	}

	public void setResolucionesPadres(List<Resolucion> resolucionesPadres) {
		this.resolucionesPadres = resolucionesPadres;
	}*/

}
