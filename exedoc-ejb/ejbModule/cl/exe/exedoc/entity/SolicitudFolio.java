package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: SulicitudFolio.
 */
@Entity
@DiscriminatorValue("99")
public class SolicitudFolio extends DocumentoElectronico implements Serializable {

	private static final long serialVersionUID = -6602044707446769309L;

	private Date fechaSolicitud;
	private Persona solicitante;
	private Division division;
	private String gerente;
	private String temaResolucion;
	private String numeroAsignado;

	/**
	 * Constructor.
	 */
	public SolicitudFolio() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public SolicitudFolio(final Long id) {
		super(id);
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_SOLICITUD")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaSolicitud() {
		return this.fechaSolicitud;
	}

	/**
	 * @param fechaSolicitud {@link Date}
	 */
	public void setFechaSolicitud(final Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITANTE", nullable = true)
	public Persona getSolicitante() {
		return this.solicitante;
	}

	/**
	 * @param solicitante {@link Persona}
	 */
	public void setSolicitante(final Persona solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * @return {@link Division}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DIVISION", nullable = true)
	public Division getDivision() {
		return this.division;
	}

	/**
	 * @param division {@link Division}
	 */
	public void setDivision(final Division division) {
		this.division = division;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "GERENTE")
	public String getGerente() {
		return this.gerente;
	}

	/**
	 * @param gerente {@link String}
	 */
	public void setGerente(final String gerente) {
		this.gerente = gerente;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "TEMA_RESOLUCION")
	public String getTemaResolucion() {
		return this.temaResolucion;
	}

	/**
	 * @param temaResolucion {@link String}
	 */
	public void setTemaResolucion(final String temaResolucion) {
		this.temaResolucion = temaResolucion;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NUMERO_ASIGNADO")
	public String getNumeroAsignado() {
		return this.numeroAsignado;
	}

	/**
	 * @param numeroAsignado {@link String}
	 */
	public void setNumeroAsignado(final String numeroAsignado) {
		this.numeroAsignado = numeroAsignado;
	}

}
