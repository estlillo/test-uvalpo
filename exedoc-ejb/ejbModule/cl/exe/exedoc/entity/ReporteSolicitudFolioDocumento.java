package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "REPORTE_FOLIOS_DOCUMENTO")
@SequenceGenerator(name = "SEQ_REPORTE_FOLIO", sequenceName = "SEQ_REPORTE_FOLIO", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "ReporteSolicitudFolioDocumento.findById", query = "SELECT f FROM ReporteSolicitudFolioDocumento f WHERE f.id = :id"),
		@NamedQuery(name = "ReporteSolicitudFolioDocumento.findByAll", query = "SELECT f FROM ReporteSolicitudFolioDocumento f"),
		@NamedQuery(name = "ReporteSolicitudFolioDocumento.findByIdTipoDocumento", query = "SELECT f FROM ReporteSolicitudFolioDocumento f WHERE f.tipoDocumento = :tipodocumento"),
		@NamedQuery(name = "ReporteSolicitudFolioDocumento.findByFiltro", query = "SELECT f FROM ReporteSolicitudFolioDocumento f WHERE f.fiscalia = :fiscalia and f.unidad = :unidad and f.tipoDocumento.id = :id"),
		@NamedQuery(name = "ReporteSolicitudFolioDocumento.findByMateria", query = "SELECT f FROM ReporteSolicitudFolioDocumento f WHERE f.materia = :materia"), })
public class ReporteSolicitudFolioDocumento implements Serializable {

	private static final long serialVersionUID = -5535896380905609086L;

	private Long id;
	private Long fiscalia;
	private Long unidad;
	private Long tipoDocumento;
	private Integer agnoActual;
	private Long numeroActual;
	private String materia;
	private String Observacion;
	private Persona usuario;
	private Long nivel;
	private Integer formato;
	private Date fechaSolicitud;

	private Long modo;

	private TipoDocumento tipoDocumentoEntity;
	private Departamento departamento;
	private UnidadOrganizacional unidadOrganizacional;

	public static final String ERROR_AL_OBTENER_FOLIOS = "Error al obtener folios: ";

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REPORTE_FOLIO")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "TIPO_DOCUMENTO")
	public Long getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Column(name = "NUMERO_ACTUAL")
	public Long getNumeroActual() {
		return numeroActual;
	}

	public void setNumeroActual(Long numeroActual) {
		this.numeroActual = numeroActual;
	}
	
	
	

	@Column(name = "AGNO_ACTUAL")
	public Integer getAgnoActual() {
		return agnoActual;
	}

	public void setAgnoActual(Integer agnoActual) {
		this.agnoActual = agnoActual;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUARIO", nullable = false)
	public Persona getUsuario() {
		return usuario;
	}

	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the nivel
	 */
	@Column(name = "NIVEL")
	public Long getNivel() {
		return nivel;
	}

	/**
	 * @param nivel
	 *            the nivel to set
	 */
	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}

	/**
	 * @return the formato
	 */
	@Column(name = "FORMATO")
	public Integer getFormato() {
		return formato;
	}

	/**
	 * @param formato
	 *            the formato to set
	 */
	public void setFormato(Integer formato) {
		this.formato = formato;
	}

	/**
	 * @return the fiscalia
	 */
	@Column(name = "FISCALIA")
	public Long getFiscalia() {
		return fiscalia;
	}

	/**
	 * @return the Materia
	 */
	@Column(name = "MATERIA")
	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	/**
	 * @return the Observaciones
	 */
	@Column(name = "OBSERVACION")
	public String getObservacion() {
		return Observacion;
	}

	public void setObservacion(String observacion) {
		Observacion = observacion;
	}

	/**
	 * @param fiscalia
	 *            the fiscalia to set
	 */
	public void setFiscalia(Long fiscalia) {
		this.fiscalia = fiscalia;
	}

	/**
	 * @return the unidad
	 */
	@Column(name = "UNIDAD")
	public Long getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad
	 *            the unidad to set
	 */
	public void setUnidad(Long unidad) {
		this.unidad = unidad;
	}

	
	@Column(name = "FECHA_SOLICITUD")
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/**
	 * @param modo
	 *            the modo to set
	 */
	public void setModo(Long modo) {
		this.modo = modo;
	}

	/**
	 * @return the modo
	 */
	public Long getModo() {
		return modo;
	}

	@Transient
	public TipoDocumento getTipoDocumentoEntity() {
		return tipoDocumentoEntity;
	}

	public void setTipoDocumentoEntity(TipoDocumento tipoDocumentoEntity) {
		this.tipoDocumentoEntity = tipoDocumentoEntity;
	}

	@Transient
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Transient
	public UnidadOrganizacional getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(
			UnidadOrganizacional unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Transient
	public String getNombreUnidad() {
		if (this.getUnidadOrganizacional() == null) {
			return "Todos";
		} else {
			return this.getUnidadOrganizacional().getDescripcion();
		}
	}
}
