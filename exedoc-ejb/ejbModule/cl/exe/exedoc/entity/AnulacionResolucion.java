package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: AnulacionResolucion.
 * 
 * @author Ricardo Fuentes
 */
@Entity
@DiscriminatorValue("98")
public class AnulacionResolucion extends DocumentoElectronico implements Serializable {

	private static final long serialVersionUID = 2468746688332293874L;
	private Persona solicitante;
	private String temaResolucion;
	private String justificacion;
	private Date fechaAnulacion;
	private String jefe;
	private String cargo;
	private String respaldoAnulacion;
	private Date fechaRespaldo;
	private String numeroResolucion;
	private Date fechaResolucion;

	/**
	 * Constructor.
	 */
	public AnulacionResolucion() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public AnulacionResolucion(final Long id) {
		
		super(id);
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITANTE_ANULACION", nullable = true)
	public Persona getSolicitante() {
		return this.solicitante;
	}

	/**
	 * @param solicitante {@link Persona}
	 */
	public void setSolicitante(final Persona solicitante) {
		this.solicitante = solicitante;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "TEMA_RESOLUCION")
	public String getTemaResolucion() {
		return this.temaResolucion;
	}

	/**
	 * @param temaResolucion {@link String}
	 */
	public void setTemaResolucion(final String temaResolucion) {
		this.temaResolucion = temaResolucion;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "JUSTIFICACION")
	public String getJustificacion() {
		return this.justificacion;
	}

	/**
	 * @param justificacion {@link String}
	 */
	public void setJustificacion(final String justificacion) {
		this.justificacion = justificacion;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_ANULACION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaAnulacion() {
		return this.fechaAnulacion;
	}

	/**
	 * @param fechaAnulacion {@link Date}
	 */
	public void setFechaAnulacion(final Date fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "JEFE_SOLICITANTE")
	public String getJefe() {
		return this.jefe;
	}

	/**
	 * @param jefe {@link String}
	 */
	public void setJefe(final String jefe) {
		this.jefe = jefe;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "CARGO_JEFE_SOLICITANTE")
	public String getCargo() {
		return this.cargo;
	}

	/**
	 * @param cargo {@link String}
	 */
	public void setCargo(final String cargo) {
		this.cargo = cargo;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "RESPALDO_ANULACION")
	public String getRespaldoAnulacion() {
		return this.respaldoAnulacion;
	}

	/**
	 * @param respaldoAnulacion {@link String}
	 */
	
	public void setRespaldoAnulacion(final String respaldoAnulacion) {
		this.respaldoAnulacion = respaldoAnulacion;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_RESPALDO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaRespaldo() {
		return this.fechaRespaldo;
	}

	/**
	 * @param fechaRespaldo {@link Date}
	 */
	public void setFechaRespaldo(final Date fechaRespaldo) {
		this.fechaRespaldo = fechaRespaldo;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NUMERO_RESOLUCION_ANULADA")
	public String getNumeroResolucion() {
		return numeroResolucion;
	}

	/**
	 * @param numeroResolucion {@link String}
	 */
	public void setNumeroResolucion(final String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_RESOLUCION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaResolucion() {
		return fechaResolucion;
	}

	/**
	 * @param fechaResolucion {@link Date}
	 */
	public void setFechaResolucion(final Date fechaResolucion) {
		this.fechaResolucion = fechaResolucion;
	}

}
