package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SOLICITUD_DIA_ADM")
@SequenceGenerator(name = "SEQ_SOLICITUD_DIA_ADM", sequenceName = "SEQ_SOLICITUD_DIA_ADM", allocationSize = 1, initialValue = 1)
@NamedQueries({ 
	@NamedQuery(name = "SolicitudDiaAdministrativo.findById", query = "SELECT sd FROM SolicitudDiaAdministrativo sd WHERE sd.id = :id"), 
	@NamedQuery(name = "SolicitudDiaAdministrativo.findByAll", query = "SELECT sd FROM SolicitudDiaAdministrativo sd"),
	@NamedQuery(name = "SolicitudDiaAdministrativo.findActiveSolicitud", query = "SELECT sda FROM SolicitudDiaAdministrativo sda WHERE sda.solicitante = :solicitante AND sda.aceptado IS null AND sda.rechazado IS null"), 
	@NamedQuery(name = "SolicitudDiaAdministrativo.findActiveSolicitudExcludeId", query = "SELECT sda FROM SolicitudDiaAdministrativo sda WHERE sda.solicitante = :solicitante AND NOT sda.id = :id AND sda.aceptado IS null AND sda.rechazado IS null)")
})
public class SolicitudDiaAdministrativo implements Serializable {

	private static final long serialVersionUID = 4015633889158096458L;
	
	public final static String SIN_ESTADO = "Sin Estado";

	public final static String APROBADO = "Aprobado";
	public final static String RECHAZADO = "Rechazado";

	private Long id;
	private Persona solicitante;
	private Integer cantDiasAdmSolicitados;
	private Integer cantFeriadosLegalesSolicitados;
	private Integer cantMediosDiasAdmSolicitados;
	private Integer cantMediosFeriadosLegalesSolicitados;
	private Boolean aceptado;
	private Boolean rechazado;
	private Boolean diaAdministrativo;
	private Boolean feriadoLegal;
	private DiaAdministrativo diaAdministrativoSolicitud;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SOLICITUD_DIA_ADM")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITANTE")
	public Persona getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Persona solicitante) {
		this.solicitante = solicitante;
	}

	@Column(name = "ACEPTADO")
	public Boolean getAceptado() {
		return aceptado;
	}

	public void setAceptado(Boolean aceptado) {
		this.aceptado = aceptado;
	}

	@Column(name = "RECHAZADO")
	public Boolean getRechazado() {
		return rechazado;
	}

	public void setRechazado(Boolean rechazado) {
		this.rechazado = rechazado;
	}
	
	@Column(name = "CANT_DIAS_ADM_SOLIC")
	public Integer getCantDiasAdmSolicitados() {
		return cantDiasAdmSolicitados;
	}

	public void setCantDiasAdmSolicitados(Integer cantDiasAdmSolicitados) {
		this.cantDiasAdmSolicitados = cantDiasAdmSolicitados;
	}

	@Column(name = "CANT_FER_LEG_SOLIC")
	public Integer getCantFeriadosLegalesSolicitados() {
		return cantFeriadosLegalesSolicitados;
	}

	public void setCantFeriadosLegalesSolicitados(Integer cantFeriadosLegalesSolicitados) {
		this.cantFeriadosLegalesSolicitados = cantFeriadosLegalesSolicitados;
	}
	
	@Column(name = "CANT_MEDIOS_DIAS_ADM_SOLIC")
	public Integer getCantMediosDiasAdmSolicitados() {
		return cantMediosDiasAdmSolicitados;
	}

	public void setCantMediosDiasAdmSolicitados(Integer cantMediosDiasAdmSolicitados) {
		this.cantMediosDiasAdmSolicitados = cantMediosDiasAdmSolicitados;
	}

	@Column(name = "CANT_MEDIOS_FER_LEG_SOLIC")
	public Integer getCantMediosFeriadosLegalesSolicitados() {
		return cantMediosFeriadosLegalesSolicitados;
	}

	public void setCantMediosFeriadosLegalesSolicitados(Integer cantMediosFeriadosLegalesSolicitados) {
		this.cantMediosFeriadosLegalesSolicitados = cantMediosFeriadosLegalesSolicitados;
	}
	
	@Column(name = "DIA_ADMINISTRATIVO")
	public Boolean getDiaAdministrativo() {
		return diaAdministrativo;
	}

	public void setDiaAdministrativo(Boolean diaAdministrativo) {
		this.diaAdministrativo = diaAdministrativo;
	}

	@Column(name = "FERIADO_LEGAL")
	public Boolean getFeriadoLegal() {
		return feriadoLegal;
	}

	public void setFeriadoLegal(Boolean feriadoLegal) {
		this.feriadoLegal = feriadoLegal;
	}
	
	@OneToOne
	@JoinColumn(name = "DIA_ADMIN_SOLIC")
	public DiaAdministrativo getDiaAdministrativoSolicitud() {
		return diaAdministrativoSolicitud;
	}
	
	public void setDiaAdministrativoSolicitud(DiaAdministrativo diaAdministrativoSolicitud) {
		this.diaAdministrativoSolicitud = diaAdministrativoSolicitud;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof SolicitudDiaAdministrativo) {
			SolicitudDiaAdministrativo e = (SolicitudDiaAdministrativo) obj;
			if (this.id.equals(e.getId())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [solicitante: ");
		representacion.append(this.solicitante.getId());
		representacion.append("], [cantDiasAdmSolicitados: ");
		representacion.append(this.cantDiasAdmSolicitados);
		representacion.append("], [cantFeriadosLegalesSolicitados: ");
		representacion.append(this.cantFeriadosLegalesSolicitados);
		representacion.append("], [cantMediosDiasAdmSolicitados: ");
		representacion.append(this.cantMediosDiasAdmSolicitados);
		representacion.append("], [cantMediosFeriadosLegalesSolicitados: ");
		representacion.append(this.cantMediosFeriadosLegalesSolicitados);
		representacion.append("], [diaAdministrativo: ");
		representacion.append(this.diaAdministrativo);
		representacion.append("], [feriadoLegal: ");
		representacion.append(this.feriadoLegal);
		representacion.append("], [aceptado: ");
		representacion.append(this.aceptado);
		representacion.append("], [diaAdministrativoSolicitud: ");
		representacion.append(this.diaAdministrativoSolicitud.getId());
		representacion.append("]");
		return representacion.toString();
	}

}