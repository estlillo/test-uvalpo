package cl.exe.exedoc.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//@Entity
//@Table(name = "TIPOS_UO")
public class TipoUnidadOrganizacional { // implements Serializable {

	// private static final long serialVersionUID = -3875332071858614559L;

	private Integer id;

	private String descripcion;

	public static final Integer MANUAL = 1;
	public static final Integer ELECTRONICO = 2;

	public TipoUnidadOrganizacional() {
	}

	public TipoUnidadOrganizacional(Integer id) {
		this.id = id;
		switch (id) {
		case 1:
			this.descripcion = "MANUAL";
			break;
		case 2:
			this.descripcion = "ELECTRONICO";
			break;
		}
	}

//	@Column(name = "ID")
//	@Id
//	@GeneratedValue(/* generator="SEC_TIPOS_UNIDADES_ORGANIZACIONALES" */)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(id);
		representacion.append("], [descripcion: ");
		representacion.append(descripcion);
		representacion.append("]");
		return representacion.toString();
	}
}
