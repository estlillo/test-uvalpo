package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

/**
 * Entity Documento Binario.
 * 
 * @author Ricardo
 */
@Entity
@Name("DocumentoBinario")
@DiscriminatorValue("2")
public class DocumentoBinario extends Documento implements Serializable {

	private static final long serialVersionUID = 5642688702059711987L;

	private ArchivoDocumentoBinario archivo;
	//Transient
	private Integer idDocumentoReferencia;
	private String numDocumentoReferencia;
		
	private List<ArchivoAdjuntoDocumentoBinario> archivosAdjuntos = new ArrayList<ArchivoAdjuntoDocumentoBinario>();

	/**
	 * Constructor.
	 */
	public DocumentoBinario() {
		super();
	}

	/**
	 * @return {@link ArchivoDocumentoBinario}
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_ARCHIVO", nullable = true)
	public ArchivoDocumentoBinario getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo {@link ArchivoDocumentoBinario}
	 */
	public void setArchivo(final ArchivoDocumentoBinario archivo) {
		this.archivo = archivo;
	}

	/**
	 * @return {@link List} of {@link ArchivoAdjuntoDocumentoBinario}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documentoBinario", cascade = CascadeType.ALL)
	public List<ArchivoAdjuntoDocumentoBinario> getArchivosAdjuntos() {
		return archivosAdjuntos;
	}

	/**
	 * @param archivosAdjuntos {@link List} of {@link ArchivoAdjuntoDocumentoBinario}
	 */
	public void setArchivosAdjuntos(final List<ArchivoAdjuntoDocumentoBinario> archivosAdjuntos) {
		this.archivosAdjuntos = archivosAdjuntos;
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[[imagenDocumento: ");
		representacion.append("], [Documento: ");
		representacion.append(super.toString());
		representacion.append("]]");
		return representacion.toString();
	}

	
	@Transient 
	public Integer getIdDocumentoReferencia() { 
		return idDocumentoReferencia;
	}
	 
	public void setIdDocumentoReferencia(Integer idDocumentoReferencia) {
		this.idDocumentoReferencia = idDocumentoReferencia;
	}
	 
	@Transient 
	public String getNumDocumentoReferencia() {
		return numDocumentoReferencia;
	}
	  
	public void setNumDocumentoReferencia(String numDocumentoReferencia) {
		this.numDocumentoReferencia = numDocumentoReferencia;
	}
	 
}
