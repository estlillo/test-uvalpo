package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "OBSERVACIONES")
@SequenceGenerator(name = "SEQ_OBSERVACIONES", sequenceName = "SEQ_OBSERVACIONES", allocationSize = 1, initialValue = 1)
public class Observacion implements Serializable, Comparable<Observacion> {

	private static final long serialVersionUID = 4135375587109014546L;

	private Long id;
	private String observacion;
	private Expediente expediente;
	private Persona autor;
	private Date fecha;
	private Integer idNuevaObservacion;
	private ObservacionArchivo observacionArchivo;
	private TipoObservacion tipoObservacion;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_OBSERVACIONES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "OBSERVACION", length = 1000)
	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EXPEDIENTE", nullable = true)
	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getAutor() {
		return autor;
	}

	public void setAutor(Persona autor) {
		this.autor = autor;
	}

	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFecha() {
		return fecha;
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_OBSERVACION_ARCHIVO", nullable = true)
	public ObservacionArchivo getObservacionArchivo() {
		return observacionArchivo;
	}

	public void setObservacionArchivo(ObservacionArchivo observacionArchivo) {
		this.observacionArchivo = observacionArchivo;
	}
	
	@ManyToOne
	@JoinColumn(name = "id_tipo_observacion", nullable = true)
	public TipoObservacion getTipoObservacion() {
		return tipoObservacion;
	}

	public void setTipoObservacion(TipoObservacion tipoObservacion) {
		this.tipoObservacion = tipoObservacion;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public int hashCode() {
		return this.observacion.hashCode();
	}

	public int compareTo(Observacion arg0) {
		return this.fecha.compareTo(arg0.getFecha()) * -1;
	}

	@Transient
	public Integer getIdNuevaObservacion() {
		return idNuevaObservacion;
	}

	public void setIdNuevaObservacion(Integer idNuevaObservacion) {
		this.idNuevaObservacion = idNuevaObservacion;
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj != null) {
			if (obj instanceof Observacion) {
				final Observacion obs = (Observacion) obj;
				if ((this.autor != null && obs.getAutor() != null && this.autor.getId().equals(obs.getAutor().getId())) &&
						((this.tipoObservacion == null && obs.getTipoObservacion() == null) || 
								(this.tipoObservacion != null && obs.getTipoObservacion() != null && this.tipoObservacion.getId().equals(obs.getTipoObservacion().getId()))) &&
						((this.observacionArchivo == null && obs.getObservacionArchivo() == null) || 
								(this.observacionArchivo != null && obs.getObservacionArchivo() != null && this.observacionArchivo.getId().equals(obs.getObservacionArchivo().getId()))) &&
						this.fecha.equals(obs.getFecha()) &&
						this.observacion.equals(obs.getObservacion())) {
					return true;
				}
			}
		}
		return false;
	}
}
