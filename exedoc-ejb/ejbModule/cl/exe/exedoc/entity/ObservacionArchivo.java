package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.jboss.seam.annotations.Name;

@Entity
@Name("ObservacionArchivo")
@DiscriminatorValue("11")
@NamedQueries({
		@NamedQuery(name = "ObservacionArchivo.findById", query = "SELECT oa FROM ObservacionArchivo oa WHERE oa.id = :id"),
		@NamedQuery(name = "ObservacionArchivo.findByAll", query = "SELECT oa FROM ObservacionArchivo oa") })
public class ObservacionArchivo extends Archivo implements Serializable {

	private static final long serialVersionUID = 2818029820008529653L;

	private List<Observacion> observaciones;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "observacionArchivo", cascade = CascadeType.ALL) //
	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}

}
