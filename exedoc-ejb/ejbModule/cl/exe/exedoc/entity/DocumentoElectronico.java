package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.jboss.seam.annotations.Name;

/**
 * Entity {@link DocumentoElectronico}.
 * 
 * @author Ricardo
 */
@Entity
@Name("DocumentoElectronico")
@DiscriminatorValue("3")
public class DocumentoElectronico extends Documento implements Serializable {

	private static final long serialVersionUID = 3093403514989171003L;

	private String documentoXML;

	protected List<Parrafo> parrafos;

	private List<ArchivoAdjuntoDocumentoElectronico> archivosAdjuntos;

	private Date fechaDescargaContraloria;
	
	private String ciudad;

	/**
	 * Constructor.
	 */
	public DocumentoElectronico() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public DocumentoElectronico(final Long id) {
		super(id);
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DOCUMENTO_XML")
	@Lob()
	public String getDocumentoXML() {
		return documentoXML;
	}

	/**
	 * @param documentoXML {@link String}
	 */
	public void setDocumentoXML(final String documentoXML) {
		this.documentoXML = documentoXML;
	}

	/**
	 * @return {@link List} of {@link Parrafo}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documento", cascade = CascadeType.ALL)
	public List<Parrafo> getParrafos() {
		return parrafos;
	}

	/**
	 * @param parrafos {@link List} of {@link Parrafo}
	 */
	public void setParrafos(final List<Parrafo> parrafos) {
		this.parrafos = parrafos;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_DESCARGA_CONTRALORIA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaDescargaContraloria() {
		return fechaDescargaContraloria;
	}

	/**
	 * @param fechaDescargaContraloria {@link Date}
	 */
	public void setFechaDescargaContraloria(final Date fechaDescargaContraloria) {
		this.fechaDescargaContraloria = fechaDescargaContraloria;
	}

	/**
	 * @return {@link List} of {@link ArchivoAdjuntoDocumentoElectronico}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "documentoElectronico", cascade = CascadeType.ALL)
	public List<ArchivoAdjuntoDocumentoElectronico> getArchivosAdjuntos() {
		return archivosAdjuntos;
	}

	/**
	 * @param archivosAdjuntos {@link List} of {@link ArchivoAdjuntoDocumentoElectronico}
	 */
	public void setArchivosAdjuntos(final List<ArchivoAdjuntoDocumentoElectronico> archivosAdjuntos) {
		this.archivosAdjuntos = archivosAdjuntos;
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[[documentoXML: ");
		representacion.append(documentoXML);
		representacion.append("], [Documento: ");
		representacion.append(super.toString());
		representacion.append("]]");
		return representacion.toString();
	}
	
	@Column(name = "CIUDAD")
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

}
