package cl.exe.exedoc.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import org.hibernate.validator.Length;
import org.jboss.seam.annotations.Name;

@Entity
@Name("Vacaciones")
@DiscriminatorValue("160")
@NamedQueries( { @NamedQuery(name = "Vacaciones.findById", query = "SELECT v FROM Vacaciones v WHERE v.id = :id"), @NamedQuery(name = "Vacaciones.findByAll", query = "SELECT v FROM Vacaciones v") })
public class Vacaciones extends Solicitud {

	private static final long serialVersionUID = -7373767691106095898L;

	private Integer diasSolicitadosVacaciones;
	private Date fechaInicioVacaciones;
	private Date fechaTerminoVacaciones;
	private String motivoVacaciones;
	private SolicitudVacaciones solicitudVacaciones;
	private Boolean aceptadoVacaciones;
	private Boolean aceptadoVacacionesRRHH;
	private Boolean rechazadoVacaciones;
	private Boolean creandoResolucionVaca;

	@Column(name = "CREANDO_RESOLUCION_VACA")
	public Boolean getCreandoResolucionVaca() {
		return creandoResolucionVaca;
	}

	public void setCreandoResolucionVaca(Boolean creandoResolucionVaca) {
		this.creandoResolucionVaca = creandoResolucionVaca;
	}

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "vacaciones", cascade = CascadeType.ALL)
	public SolicitudVacaciones getSolicitudVacaciones() {
		return solicitudVacaciones;
	}

	public void setSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones) {
		this.solicitudVacaciones = solicitudVacaciones;
	}

	@Column(name = "DIAS_SOLICITADOS_VACACIONES")
	public Integer getDiasSolicitadosVacaciones() {
		return diasSolicitadosVacaciones;
	}

	public void setDiasSolicitadosVacaciones(Integer diasSolicitadosVacaciones) {
		this.diasSolicitadosVacaciones = diasSolicitadosVacaciones;
	}

	@Column(name = "FECHA_INICIO_VACACIONES")
	public Date getFechaInicioVacaciones() {
		return fechaInicioVacaciones;
	}

	public void setFechaInicioVacaciones(Date fechaInicioVacaciones) {
		this.fechaInicioVacaciones = fechaInicioVacaciones;
	}

	@Column(name = "FECHA_TERMINO_VACACIONES")
	public Date getFechaTerminoVacaciones() {
		return fechaTerminoVacaciones;
	}

	public void setFechaTerminoVacaciones(Date fechaTerminoVacaciones) {
		this.fechaTerminoVacaciones = fechaTerminoVacaciones;
	}

	@Column(name = "MOTIVO_VACACIONES", length = 1000)
	@Length(max=1000)
	public String getMotivoVacaciones() {
		return motivoVacaciones;
	}

	public void setMotivoVacaciones(String motivoVacaciones) {
		this.motivoVacaciones = motivoVacaciones;
	}
	
	@Column(name = "ACEPTADO_VACACIONES")
	public Boolean getAceptadoVacaciones() {
		return aceptadoVacaciones;
	}

	public void setAceptadoVacaciones(Boolean aceptadoVacaciones) {
		this.aceptadoVacaciones = aceptadoVacaciones;
	}
	
	@Column(name = "ACEPTADO_VACACIONES_RRHH")
	public Boolean getAceptadoVacacionesRRHH() {
		return aceptadoVacacionesRRHH;
	}

	public void setAceptadoVacacionesRRHH(Boolean aceptadoVacacionesRRHH) {
		this.aceptadoVacacionesRRHH = aceptadoVacacionesRRHH;
	}

	@Column(name = "RECHAZADO_VACACIONES")
	public Boolean getRechazadoVacaciones() {
		return rechazadoVacaciones;
	}

	public void setRechazadoVacaciones(Boolean rechazadoVacaciones) {
		this.rechazadoVacaciones = rechazadoVacaciones;
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[funcionario: ");
		representacion.append(this.getFuncionario().getIniciales());
		representacion.append("], [fechaInicioVacaciones: ");
		representacion.append(this.fechaInicioVacaciones);
		representacion.append("], [fechaTerminoVacaciones: ");
		representacion.append(this.fechaTerminoVacaciones);
		representacion.append("], [nDiasSolicitadosVacaciones: ");
		representacion.append(this.diasSolicitadosVacaciones);
		representacion.append("], [motivoVacaciones: ");
		representacion.append(this.motivoVacaciones);
		representacion.append("], [solicitudVacaciones: ");
		representacion.append(this.solicitudVacaciones);
		representacion.append("], [aceptadoVacaciones: ");
		representacion.append(this.aceptadoVacaciones);
		representacion.append("], [rechazadoVacaciones: ");
		representacion.append(this.rechazadoVacaciones);
		representacion.append("]");
		return representacion.toString();
	}
}