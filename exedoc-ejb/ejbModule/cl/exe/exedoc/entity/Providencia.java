package cl.exe.exedoc.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Providencia")
@DiscriminatorValue("10")
public class Providencia extends DocumentoElectronico {

	private static final long serialVersionUID = -6474685870301037974L;

	private List<AccionProvidencia> acciones;
	private String observaciones;

	@ManyToMany
	@JoinTable(name = "ACCIONES_PROVIDENCIAS", joinColumns = @JoinColumn(name = "PROVIDENCIA_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ACCIONES_ID", referencedColumnName = "ID"))
	public List<AccionProvidencia> getAcciones() {
		return acciones;
	}

	public void setAcciones(List<AccionProvidencia> acciones) {
		this.acciones = acciones;
	}

	@Column(name = "OBSERVACIONES")
	@Lob
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	@Transient
	@Override
	public String getMateria() {
		return observaciones;
	}

}
