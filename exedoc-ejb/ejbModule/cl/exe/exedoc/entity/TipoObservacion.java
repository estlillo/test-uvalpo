package cl.exe.exedoc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "tipo_observacion")
public class TipoObservacion implements java.io.Serializable {

	private static final long serialVersionUID = 2341677960025728595L;
	
	public static final Integer OBSERVACION_NORMAL = 1;
	public static final Integer OBSERVACION_SISTEMA = 2;
	public static final Integer OBSERVACION_DEVOLUCION = 3;
	public static final Integer OBSERVACION_RESOLUCION = 4;
	
	private Integer id;
	private String descripcion;

	public TipoObservacion() {
	}

	public TipoObservacion(Integer id) {
		this.id = id;
	}

	public TipoObservacion(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	@Id
	@Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "descripcion", length = 50)
	@Length(max = 50)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
