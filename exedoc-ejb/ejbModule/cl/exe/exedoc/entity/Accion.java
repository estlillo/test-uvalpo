/*
 * EXE Ingenieria y Software Ltda.
 */
package cl.exe.exedoc.entity;

/**
 * Enum que contendra las acciones que se podran ejecutar sobre los documentos.
 * 
 * @author Jose Nova
 * @version 1.0
 */
public enum Accion {

    /** Accion Firmar. */
    FIRMAR, 
    
    /** Accion Visar. */
    VISAR, 
    
    /** Accion Derivar. */
    DERIVAR,
    
    /** Accion Rechazar. */
    RECHAZAR,

    /** Accion Rechazar. */
    ACUSAR_RECIBO;

    
}
