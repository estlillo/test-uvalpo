package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "DEPENDENCIAS_EXTERNAS")
@SequenceGenerator(name = "SEQ_DEPENDENCIAS_EXTERNAS", sequenceName = "SEQ_DEPENDENCIAS_EXTERNAS", allocationSize = 1, initialValue = 1)
@NamedQueries( { 
	@NamedQuery(name = "DependenciaExterna.findById", query = "SELECT d FROM DependenciaExterna d WHERE d.id = :id"),
	@NamedQuery(name = "DependenciaExterna.findByDescripcion", query = "SELECT d FROM DependenciaExterna d WHERE upper(d.descripcion) = :descripcion"),
	@NamedQuery(name = "DependenciaExterna.findByDescripcionDistinctId", query = "SELECT d FROM DependenciaExterna d WHERE upper(d.descripcion) = :descripcion AND d.id <> :id"),
	@NamedQuery(name = "DependenciaExterna.findByAll", query = "SELECT d FROM DependenciaExterna d WHERE d.vigente = true "),
	@NamedQuery(name = "DependenciaExterna.findByAllInclusiveNotVisible", query = "SELECT d FROM DependenciaExterna d order by d.descripcion ")
})
public class DependenciaExterna implements Serializable {

	private static final long serialVersionUID = 1388094020230381712L;

	private Long id;
	private String descripcion;
	private List<PersonaExterna> personas;
	private Boolean vigente = true;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPENDENCIAS_EXTERNAS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "dependencia")
	public List<PersonaExterna> getPersonas() {
		return personas;
	}

	public void setPersonas(List<PersonaExterna> personas) {
		this.personas = personas;
	}
	
	@Column(name = "VIGENTE")
	public Boolean getVigente() {
		return vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}


	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}

}
