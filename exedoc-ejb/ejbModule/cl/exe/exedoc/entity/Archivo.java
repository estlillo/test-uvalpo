package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Entity Archivo.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "ARCHIVOS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ID_TIPO_ARCHIVO", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "SEQ_ARCHIVOS", sequenceName = "SEQ_ARCHIVOS", allocationSize = 1, initialValue = 1)
public class Archivo implements Serializable {

	private static final long serialVersionUID = -3858513098803981187L;
	private Long id;
	private String cmsId;
	private Date fecha;
	private String nombreArchivo;
	private String contentType;
	private byte[] archivo;
	private Integer idNuevoArchivo;

	/**
	 * Constructor.
	 */
	public Archivo() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public Archivo(final Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ARCHIVOS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "CMS_DOCID")
	public String getCmsId() {
		return cmsId;
	}

	/**
	 * @param cmsId {@link String}
	 */
	public void setCmsId(final String cmsId) {
		this.cmsId = cmsId;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha {@link Date}
	 */
	public void setFecha(final Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NOMBRE_ARCHIVO")
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo {@link String}
	 */
	public void setNombreArchivo(final String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "CONTENT_TYPE")
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType {@link String}
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return {@link Byte}
	 */
	@Transient
	public byte[] getArchivo() {
		return archivo;
	}

	/**
	 * @param archivo {@link Byte}
	 */
	public void setArchivo(final byte[] archivo) {
		this.archivo = archivo;
	}

	/**
	 * @return {@link Integer}
	 */
	@Transient
	public Integer getIdNuevoArchivo() {
		return idNuevoArchivo;
	}

	/**
	 * @param idNuevoArchivo {@link Integer}
	 */
	public void setIdNuevoArchivo(final Integer idNuevoArchivo) {
		this.idNuevoArchivo = idNuevoArchivo;
	}

}
