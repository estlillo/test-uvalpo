package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.jboss.seam.annotations.Name;

/**
 * @author Administrator
 */
@Entity
@Name("AccionProvidencia")
@Table(name = "ACCION_PROVIDENCIA")
public class AccionProvidencia implements Serializable {

	/**
	 * 
	 */
	public static final Integer TOMAR_CONOCIMIENTO = 1;
	/**
	 * 
	 */
	public static final Integer DAR_CUMPLIMIENTO = 2;
	/**
	 * 
	 */
	public static final Integer INFORMEME = 3;
	/**
	 * 
	 */
	public static final Integer ESTUDIAR_PROPONER = 4;
	/**
	 * 
	 */
	public static final Integer PROPONER_RESPUESTA = 5;
	/**
	 * 
	 */
	public static final Integer ELABORAR_PROPONER_RESOLUCION = 6;
	/**
	 * 
	 */
	public static final Integer ACUMULAR_ANTECEDENTES = 7;
	/**
	 * 
	 */
	public static final Integer RETENER_HASTA_NUEVA_ORDEN = 8;
	/**
	 * 
	 */
	public static final Integer CONFORME_PROPOSICION = 9;
	/**
	 * 
	 */
	public static final Integer INFORMAR_INTERESADO = 10;
	/**
	 * 
	 */
	public static final Integer DEVOLVER_ANTECEDENTES = 11;
	/**
	 * 
	 */
	public static final Integer CONVERSALO_CONMIGO = 12;
	/**
	 * 
	 */
	public static final Integer ACTIVAR_TRAMITE = 13;
	/**
	 * 
	 */
	public static final Integer ARCHIVAR = 14;
	/**
	 * 
	 */
	public static final Integer SU_OPINION = 15;
	/**
	 * 
	 */
	public static final Integer AMPLIAR_INFORME = 16;
	/**
	 * 
	 */
	public static final Integer SER_DE_SU_COMPETENCIA = 17;
	/**
	 * 
	 */
	public static final Integer ELABORAR_CONTRATO = 18;
	/**
	 * 
	 */
	public static final Integer INFORMAR_SUMARIO = 19;
	/**
	 * 
	 */
	public static final Integer CONTESTAR_RECURSO = 20;
	/**
	 * 
	 */
	public static final Integer RECOPILAR_ANTECEDENTES = 21;
	/**
	 * 
	 */
	public static final Integer NOTIFICAR = 22;
	/**
	 * 
	 */
	public static final Integer OTRO = 23;
	/**
	 * 
	 */
	public static final Integer ANTECEDENTES_PAGO = 24;

	private static final long serialVersionUID = 7008354871834794100L;

	private Integer id;
	private String descripcion;
	private List<Providencia> providencias;

	/**
	 * Constructor.
	 */
	public AccionProvidencia() {
	}

	/**
	 * @param id Integer
	 */
	public AccionProvidencia(final Integer id) {
		this.id = id;
		switch (id) {
			case 1:
				this.descripcion = "Tomar Conocimiento y Fines";
				break;
			case 2:
				this.descripcion = "Dar Cumplimiento";
				break;
			case 3:
				this.descripcion = "Informeme";
				break;
			case 4:
				this.descripcion = "Estudiar y Proponer";
				break;
			case 5:
				this.descripcion = "Proponer Documento de Respuesta";
				break;
			case 6:
				this.descripcion = "Elaborar y/o Proponer Resolucion";
				break;
			case 7:
				this.descripcion = "Acumular Antecedentes";
				break;
			case 8:
				this.descripcion = "Retener Hasta Nueva Orden";
				break;
			case 9:
				this.descripcion = "Conforme con Proposicion";
				break;
			case 10:
				this.descripcion = "Informar al Interesado";
				break;
			case 11:
				this.descripcion = "Devolver Antecedentes";
				break;
			case 12:
				this.descripcion = "Conversarlo Conmigo";
				break;
			case 13:
				this.descripcion = "Activar Tramite";
				break;
			case 14:
				this.descripcion = "Archivar";
				break;
			case 15:
				this.descripcion = "Su Opinion";
				break;
			case 16:
				this.descripcion = "Ampliar Informe";
				break;
			case 17:
				this.descripcion = "Ser de su Competencia";
				break;
			case 18:
				this.descripcion = "Elaborar Contrato";
				break;
			case 19:
				this.descripcion = "Informar Sumario";
				break;
			case 20:
				this.descripcion = "Contestar Recurso";
				break;
			case 21:
				this.descripcion = "Recopilar Antecedentes y/o Preparar Informe";
				break;
			case 22:
				this.descripcion = "Notificar";
				break;
			case 23:
				this.descripcion = "Otro";
				break;
			case 24:
				this.descripcion = "Adjuntar Antecedente para su Pago";
				break;
			default:
				break;
		}
	}

	/**
	 * @param id Integer
	 * @return String
	 */
	public static String getAccionProvidencia(final Integer id) {
		switch (id) {
			case 1:
				return "Tomar Conocimiento y Fines";
			case 2:
				return "Dar Cumplimiento";
			case 3:
				return "Informeme";
			case 4:
				return "Estudiar y Proponer";
			case 5:
				return "Proponer Documento de Respuesta";
			case 6:
				return "Elaborar y/o Proponer Resolucion";
			case 7:
				return "Acumular Antecedentes";
			case 8:
				return "Retener Hasta Nueva Orden";
			case 9:
				return "Conforme con Proposicion";
			case 10:
				return "Informar al Interesado";
			case 11:
				return "Devolver Antecedentes";
			case 12:
				return "Conversarlo Conmigo";
			case 13:
				return "Activar Tramite";
			case 14:
				return "Archivar";
			case 15:
				return "Su Opinion";
			case 16:
				return "Ampliar Informe";
			case 17:
				return "Ser de su Competencia";
			case 18:
				return "Elaborar Contrato";
			case 19:
				return "Informar Sumario";
			case 20:
				return "Contestar Recurso";
			case 21:
				return "Recopilar Antecedentes y/o Preparar Informe";
			case 22:
				return "Notificar";
			case 23:
				return "Otro";
			case 24:
				return "Adjuntar Antecedente para su Pago";
			default:
				break;
		}
		return "";
	}

	/**
	 * @return Integer
	 */
	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	/**
	 * @param id Integer
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return String
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion String
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return List<Providencia>
	 */
	@ManyToMany(mappedBy = "acciones")
	public List<Providencia> getProvidencias() {
		return providencias;
	}

	/**
	 * @param providencias List<Providencia>
	 */
	public void setProvidencias(final List<Providencia> providencias) {
		this.providencias = providencias;
	}

}
