package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "GRUPOUSUARIO")
@SequenceGenerator(name = "SEQ_GRUPOUSUARIO", sequenceName = "SEQ_GRUPOUSUARIO", allocationSize = 1, initialValue = 1)
@NamedQueries({ @NamedQuery(name = "GrupoUsuario.findById", query = "SELECT g FROM GrupoUsuario g WHERE g.id = :id"),
				@NamedQuery(name = "GrupoUsuario.findByAll", query = "SELECT g FROM GrupoUsuario g"),
				@NamedQuery(name = "GrupoUsuario.findByNombre", query = "SELECT g FROM GrupoUsuario g WHERE g.nombre = :nombre")})
public class GrupoUsuario implements Serializable {

	private static final long serialVersionUID = -921763514733308256L;

	private Long id;
	private String nombre;
	private List<Persona> usuarios;
	private transient boolean borrable;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GRUPOUSUARIO")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NOMBRE", nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@ManyToMany
	@JoinTable(name = "PERSONAS_GRUPO", joinColumns = @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID"))	
	public List<Persona> getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(List<Persona> usuarios) {
		this.usuarios = usuarios;
	}

	@Transient
	public boolean isBorrable() {
		return borrable;
	}

	public void setBorrable(final boolean borrable) {
		this.borrable = borrable;
	}

}
