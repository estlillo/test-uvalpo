package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "EXPEDIENTES_UNIDOS")
@SequenceGenerator(name = "SEQ_EXPEDIENTES_UNIDOS", sequenceName = "SEQ_EXPEDIENTES_UNIDOS", allocationSize = 1, initialValue = 1)
public class ExpedienteUnido implements Serializable {

	private static final long serialVersionUID = -7919867528959418919L;

	private Long id;
	private Expediente expediente;
	private Expediente expedientePadre;
	private Date fecha;


	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EXPEDIENTES_UNIDOS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EXPEDIENTE", nullable = false)
	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EXPEDIENTE_PADRE", nullable = false)
	public Expediente getExpedientePadre() {
		return expedientePadre;
	}

	public void setExpedientePadre(Expediente expedientePadre) {
		this.expedientePadre = expedientePadre;
	}

	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
}
