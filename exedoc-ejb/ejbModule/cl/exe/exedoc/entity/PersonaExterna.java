package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "PERSONAS_EXTERNAS")
@SequenceGenerator(name = "SEQ_PERSONAS_EXTERNAS", sequenceName = "SEQ_PERSONAS_EXTERNAS", allocationSize = 1, initialValue = 1)
@NamedQueries( {
		@NamedQuery(name = "PersonaExterna.findById", query = "SELECT p FROM PersonaExterna p WHERE p.id = :id"),
		@NamedQuery(name = "PersonaExterna.findByAll", query = "SELECT p FROM PersonaExterna p WHERE p.dependencia.vigente = true"),
		@NamedQuery(name = "PersonaExterna.findByIdDependencia", query = "SELECT p FROM PersonaExterna p WHERE p.dependencia.id = :id and p.vigente = true"),
		@NamedQuery(name = "PersonaExterna.findByIdDependenciaPersona", query = "SELECT p FROM PersonaExterna p WHERE p.dependencia.id = :id and rut is not null ORDER BY p.nombres"),
		@NamedQuery(name = "PersonaExterna.findByIdDependenciaNombre", query = "SELECT p FROM PersonaExterna p WHERE p.dependencia.id = :id and p.nombres= :nombres "),
		@NamedQuery(name = "PersonaExterna.findByRut", query = "SELECT count(p) FROM PersonaExterna p WHERE p.rut = :rut"),
		@NamedQuery(name = "PersonaExterna.findByRutAndId", query = "SELECT count(p) FROM PersonaExterna p WHERE p.rut = :rut and p.id != :id")})
public class PersonaExterna implements Serializable {

	private static final long serialVersionUID = 6222519189708178419L;

	private Long id;
	private DependenciaExterna dependencia;
	private String nombres;
	private String rut;
	private Persona persona;
	private String email;
	private Boolean vigente = true;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERSONAS_EXTERNAS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DEPENDENCIA", nullable = false)
	public DependenciaExterna getDependencia() {
		return dependencia;
	}

	public void setDependencia(DependenciaExterna dependencia) {
		this.dependencia = dependencia;
	}

	@Column(name = "NOMBRES", length = 250)
	@Length(max = 250)
	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	@Column(name = "RUT", length = 13)
	@Length(max = 13)
	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}
	
	@Column(name = "EMAIL", length = 50, nullable = true)
	@Length(max = 50)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@OneToOne
	@JoinColumn(name = "ID_PERSONA", nullable = true)
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	
	@Override
	public String toString() {
		return "[" + id.toString() + " " + nombres + "]";
	}

	@Column(name="VIGENTE")
	public Boolean getVigente() {
		return vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}

}
