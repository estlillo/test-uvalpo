package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HIST_ACT_TRANSPARENCIA")
@SequenceGenerator(name = "SEQ_HIST_ACT_TRANSPARENCIA", sequenceName = "SEQ_HIST_ACT_TRANSPARENCIA", allocationSize = 1, initialValue = 1)
public class HistorialActualizacionTransparencia implements Serializable {

	private static final long serialVersionUID = -1163948317741481416L;

	private Long id;
	private Long idUltimaSolicitudObtenida;
	private Date fechaActualizacion;
	private Persona solicitante;
	
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HIST_ACT_TRANSPARENCIA")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "ID_ULTIMA_SOLIC_OBTENIDA")
	public Long getIdUltimaSolicitudObtenida() {
		return idUltimaSolicitudObtenida;
	}

	public void setIdUltimaSolicitudObtenida(Long idUltimaSolicitudObtenida) {
		this.idUltimaSolicitudObtenida = idUltimaSolicitudObtenida;
	}

	@Column(name = "FECHA_ACTUALIZACION")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	@ManyToOne
	@JoinColumn(name = "ID_SOLICITANTE", nullable = true)
	public Persona getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Persona solicitante) {
		this.solicitante = solicitante;
	}

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [idUltimaSolicitudObtenida: ");
		representacion.append(this.idUltimaSolicitudObtenida);
		representacion.append("], [fechaActualizacion: ");
		representacion.append(this.fechaActualizacion);
		representacion.append("], [solicitante: ");
		representacion.append(this.solicitante);
		representacion.append("]");
		return representacion.toString();
	}
}