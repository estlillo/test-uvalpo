package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "DIVISIONES")
@SequenceGenerator(name = "SEQ_DIVISIONES", sequenceName = "SEQ_DIVISIONES", allocationSize = 1, initialValue = 1)
@NamedQueries({ 
	@NamedQuery(name = "Division.findById", query = "SELECT d FROM Division d WHERE d.id = :id"),
	@NamedQuery(name = "Division.findByAll", query = "SELECT d FROM Division d"),
	@NamedQuery(name = "Division.findByOrganizacion", query = "SELECT d FROM Division d WHERE d.organizacion.id = :idOrganizacion "),
	@NamedQuery(name = "Division.findByDescripcionOrganizacion", query = "SELECT d FROM Division d WHERE LOWER(d.descripcion) = LOWER(:descripcion) AND d.organizacion.id = :idOrganizacion "),
	@NamedQuery(name = "Division.findByDescripcionOrganizacionExcludeId", query = "SELECT d FROM Division d WHERE LOWER(d.descripcion) = LOWER(:descripcion) AND d.organizacion.id = :idOrganizacion AND d.id != :id ")
})
public class Division implements Serializable {

	private static final long serialVersionUID = -921763514733309256L;

	private Long id;
	private String descripcion;
	private Boolean conCargo;
	private Organizacion organizacion;
	private List<Departamento> departamentos;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DIVISIONES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "CON_CARGO", nullable = false)
	public Boolean getConCargo() {
		return conCargo;
	}

	public void setConCargo(Boolean conCargo) {
		this.conCargo = conCargo;
	}

	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "ID_ORGANIZACION", nullable = false)
	public Organizacion getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}

	@OneToMany(mappedBy = "division", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	public List<Departamento> getDepartamentos() {
		return departamentos;
	}

	public void setDepartamentos(List<Departamento> departamentos) {
		this.departamentos = departamentos;
	}

	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}
}
