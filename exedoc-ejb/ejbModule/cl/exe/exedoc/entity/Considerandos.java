package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Considerandos")
@DiscriminatorValue("4")
public class Considerandos extends Parrafo implements Serializable {

	private static final long serialVersionUID = -8451214815134813417L;

}
