package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity EstadoDocumento.
 * 
 * @author Ricardo
 * 
 */
@Entity
@Table(name = "ESTADOS_DOCUMENTOS")
public class EstadoDocumento implements Serializable {

	/**
	 * GUARDADO.
	 */
	public static final Integer GUARDADO = 1;
	/**
	 * VISADO.
	 */
	public static final Integer VISADO = 2;
	/**
	 * FIRMADO.
	 */
	public static final Integer FIRMADO = 3;
	/**
	 * DESPACHADO.
	 */
	public static final Integer DESPACHADO = 4;
	/**
	 * COMPLETADO.
	 */
	public static final Integer COMPLETADO = 5;
	/**
	 * CREADO.
	 */
	public static final Integer CREADO = 6;
	/**
	 * AUTORIZADO.
	 */
	public static final Integer AUTORIZADO = 7;
	/**
	 * RECHAZADO.
	 */
	public static final Integer RECHAZADO = 8;
	/**
	 * ENVIADO_CONTRALORIA.
	 */
	public static final Integer ENVIADO_CONTRALORIA = 9;
	/**
	 * APROBADO_CONTRALORIA.
	 */
	public static final Integer APROBADO_CONTRALORIA = 10;
	/**
	 * RECHAZADO_CONTRALORIA.
	 */
	public static final Integer RECHAZADO_CONTRALORIA = 11;
	/**
	 * BORRADOR_APROBADO.
	 */
	public static final Integer BORRADOR_APROBADO = 12;
	/**
	 * BORRADOR.
	 */
	public static final Integer BORRADOR = 13;
	
	/**
	 * BORRADOR_RECHAZADO.
	 */
	public static final Integer BORRADOR_RECHAZADO = 14;
	
	/**
	 * RECHAZADO.
	 */
	public static final Integer ANULADO = 15;
	

	private static final long serialVersionUID = 1232971242233844658L;
	private Integer id;
	private String descripcion;

	/**
	 * Constructor.
	 */
	public EstadoDocumento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            {@link Integer}
	 */
	public EstadoDocumento(final Integer id) {
		this.id = id;
		switch (id) {
		case 1:
			this.descripcion = "GUARDADO";
			break;
		case 2:
			this.descripcion = "VISADO";
			break;
		case 3:
			this.descripcion = "FIRMADO";
			break;
		case 4:
			this.descripcion = "DESPACHADO";
			break;
		case 5:
			this.descripcion = "PLAZO CERRADO";
			break;
		case 6:
			this.descripcion = "CREADO";
			break;
		case 7:
			this.descripcion = "AUTORIZADO";
			break;
		case 8:
			this.descripcion = "RECHAZADO";
			break;
		case 9:
			this.descripcion = "ENVIADO A CONTRALORIA";
			break;
		case 10:
			this.descripcion = "APROBADO EN CONTRALORIA";
			break;
		case 11:
			this.descripcion = "RECHAZADO EN CONTRALORIA";
			break;
		case 12:
			this.descripcion = "BORRADOR APROBADO";
			break;
		case 13:
			this.descripcion = "BORRADOR";
			break;
		case 14:
			this.descripcion = "BORRADOR_RECHAZADO";
			break;
		case 15:
			this.descripcion = "ANULADO";
			break;
		default:
			break;
		}
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            {@link Integer}
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

}
