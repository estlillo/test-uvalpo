package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPOS_FIRMA")
public class TipoFirma implements Serializable {

	private static final long serialVersionUID = 3288312465904417423L;

	private Integer id;
	private String descripcion;
	public static final Integer SIMPLE = 1;
	public static final Integer AVANZADA = 2;
	public static final Integer AMBAS = SIMPLE + AVANZADA;

	public TipoFirma() {
	}

	public TipoFirma(Integer id) {
		this.id = id;
		switch (id) {
		case 1:
			this.descripcion = "SIMPLE";
			break;
		case 2:
			this.descripcion = "AVANZADA";
			break;
		case 3:
			this.descripcion = "AMBAS";
			break;
		}
	}

	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
