package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("TeniendoPresente")
@DiscriminatorValue("5")
public class TeniendoPresente extends Parrafo implements Serializable {

	private static final long serialVersionUID = 469072375809794110L;

}
