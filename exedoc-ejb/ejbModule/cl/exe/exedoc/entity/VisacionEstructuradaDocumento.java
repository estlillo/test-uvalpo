package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "VISACIONES_ESTRUCTURADAS")
@SequenceGenerator(name = "SEQ_VISACIONES_ESTRUCTURADAS", sequenceName = "SEQ_VISACIONES_ESTRUCTURADAS", allocationSize = 1, initialValue = 1)
public class VisacionEstructuradaDocumento implements Serializable, Comparable<VisacionEstructuradaDocumento> {

	private static final long serialVersionUID = -4825958682502519757L;

	private Long id;
	private Documento documento;
	private Persona persona;
	private Integer orden;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_VISACIONES_ESTRUCTURADAS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Column(name = "ORDEN")
	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof VisacionEstructuradaDocumento) {
			return this.getId().equals(
					((VisacionEstructuradaDocumento) obj).getId());
		}
		return false;
	}

	public int compareTo(VisacionEstructuradaDocumento o) {
		return this.orden.compareTo(o.getOrden());
	}
}
