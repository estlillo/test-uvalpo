package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

/**
 * Entity Organizacion.
 * 
 * @author Ricardo Fuentes
 */
@Entity
@Table(name = "ORGANIZACIONES")
@SequenceGenerator(name = "SEQ_ORGANIZACIONES", sequenceName = "SEQ_ORGANIZACIONES", allocationSize = 1, initialValue = 1)
@NamedQueries({ @NamedQuery(name = "Organizacion.findById", query = "SELECT o FROM Organizacion o WHERE o.id = :id"),
		@NamedQuery(name = "Organizacion.findByAll", query = "SELECT o FROM Organizacion o") })
public class Organizacion implements Serializable {

	private static final long serialVersionUID = -921763514733309256L;

	private Long id;
	private String descripcion;
	private List<Division> divisiones;

	/**
	 * Constructor.
	 */
	public Organizacion() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param id {@link Long}
	 */
	public Organizacion(final Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ORGANIZACIONES")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	@Length(max = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "[" + id.toString() + " " + descripcion.toString() + "]";
	}

	/**
	 * @return {@link List} of {@link Division}
	 */
	@OneToMany(mappedBy = "organizacion", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	public List<Division> getDivisiones() {
		return divisiones;
	}

	/**
	 * @param divisiones {@link List} of {@link Division}
	 */
	public void setDivisiones(final List<Division> divisiones) {
		this.divisiones = divisiones;
	}
}
