package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "CAMPOS_PLANTILLA")
@SequenceGenerator(name = "SEQ_CAMPOS_PLANTILLA", sequenceName = "SEQ_CAMPOS_PLANTILLA", allocationSize = 1, initialValue = 1)
@NamedQueries( { @NamedQuery(name = "CamposPlantilla.findById", query = "SELECT cp FROM CamposPlantilla cp WHERE cp.id = :id"),
		@NamedQuery(name = "CamposPlantilla.findByAll", query = "SELECT cp FROM CamposPlantilla cp") })
public class CamposPlantilla implements Serializable {

	private static final long serialVersionUID = 1081815713042933062L;

	private Long id;
	private Plantilla plantilla;
	private TipoContenido tipoContenido;
	private String contenido;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CAMPOS_PLANTILLA")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PLANTILLA")
	public Plantilla getPlantilla() {
		return plantilla;
	}

	public void setPlantilla(Plantilla plantilla) {
		this.plantilla = plantilla;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_CONTENIDO", nullable = false)
	public TipoContenido getTipoContenido() {
		return tipoContenido;
	}

	public void setTipoContenido(TipoContenido tipoContenido) {
		this.tipoContenido = tipoContenido;
	}

	@Column(name = "CONTENIDO", length = 3000)
	@Length(max = 3000)
	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;

	}

}
