package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Pablo
 *
 */
@Entity
@Table(name = "REVISAR_ESTRUCTURADAS")
@SequenceGenerator(name = "SEQ_REVISION_ESTRUCTURADAS", sequenceName = "SEQ_REVISION_ESTRUCTURADAS", allocationSize = 1, initialValue = 1)
public class RevisarEstructuradaDocumento implements Serializable,
		Comparable<RevisarEstructuradaDocumento> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5571660666619381772L;
	
	private Long id;
	private Documento documento;
	private Persona persona;
	private Integer orden;

	/**
	 * Contructor.
	 */
	public RevisarEstructuradaDocumento() {
		super();
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REVISION_ESTRUCTURADAS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Documento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento {@link Documento}
	 */
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	/**
	 * @param persona {@link Persona}
	 */
	public void setPersona(final Persona persona) {
		this.persona = persona;
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ORDEN")
	public Integer getOrden() {
		return orden;
	}

	/**
	 * @param orden {@link Integer}
	 */
	public void setOrden(final Integer orden) {
		this.orden = orden;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof RevisarEstructuradaDocumento) {
			return this.getId().equals(
					((RevisarEstructuradaDocumento) obj).getId());
		}
		return false;
	}

	@Override
	public int compareTo(final RevisarEstructuradaDocumento o) {
		return this.orden.compareTo(o.getOrden());
	}
}
