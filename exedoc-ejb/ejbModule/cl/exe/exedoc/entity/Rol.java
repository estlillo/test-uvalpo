package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity Rol.
 * 
 * @author Ricardo Fuentes
 */
@Entity
@Table(name = "ROLES")
@SequenceGenerator(name = "SEQ_ROLES", sequenceName = "SEQ_ROLES", allocationSize = 1, initialValue = 1)
@NamedQueries({ @NamedQuery(name = "Rol.findById", query = "SELECT r FROM Rol r WHERE r.id = :id"),
		@NamedQuery(name = "Rol.findByAll", query = "SELECT r FROM Rol r") })
public class Rol implements Serializable {

	/**
	 * INGRESO_DOCUMENTOS value Rol(1, "INGRESO_DOCUMENTOS").
	 */
	public static final Rol INGRESO_DOCUMENTOS = new Rol(1, "INGRESO_DOCUMENTOS");

	/**
	 * BANDEJA_ENTRADA_DOCUMENTOS value Rol(2, "BANDEJA_ENTRADA_DOCUMENTOS").
	 */
	public static final Rol BANDEJA_ENTRADA_DOCUMENTOS = new Rol(2, "BANDEJA_ENTRADA_DOCUMENTOS");

	/**
	 * BANDEJA_SALIDA_DOCUMENTOS value Rol(3, "BANDEJA_SALIDA_DOCUMENTOS").
	 */
	public static final Rol BANDEJA_SALIDA_DOCUMENTOS = new Rol(3, "BANDEJA_SALIDA_DOCUMENTOS");

	/**
	 * FIRMA_DOCUMENTOS value Rol(4, "FIRMA_DOCUMENTOS").
	 */
	public static final Rol FIRMA_DOCUMENTOS = new Rol(4, "FIRMA_DOCUMENTOS");

	/**
	 * ADMINISTRADOR value Rol(5, "ADMINISTRADOR").
	 */
	public static final Rol ADMINISTRADOR = new Rol(5, "ADMINISTRADOR");

	/**
	 * INGRESAR_VISA_FIRMA value Rol(6, "INGRESAR_VISA_FIRMA").
	 */
	public static final Rol INGRESAR_VISA_FIRMA = new Rol(6, "INGRESAR_VISA_FIRMA");

	/**
	 * ROL_FOLIADOR value Rol(7, "ROL_FOLIADOR").
	 */
	public static final Rol ROL_FOLIADOR = new Rol(7, "ROL_FOLIADOR");

	/**
	 * Rol ROL_ANULADOR value Rol(8, "ROL_ANULADOR").
	 */
	public static final Rol ROL_ANULADOR = new Rol(8, "ROL_ANULADOR");
	
	public static final Rol JEFE_RRHH = new Rol(9, "JEFE_RRHH");
	public static final Rol JEFE = new Rol(15, "JEFE");
	public static final Rol FISCAL = new Rol (18, "FISCAL");
	public static final Rol ABASTECIMIENTO = new Rol(19, "ABASTECIMIENTO"); 
	/**
	 * MENU_PRINCIPAL value Rol(101, "MENU_PRINCIPAL").
	 */
	public static final Rol MENU_PRINCIPAL = new Rol(101, "MENU_PRINCIPAL");

	/**
	 * REPORTES value Rol(102, "reportes").
	 */
	public static final Rol REPORTES = new Rol(102, "REPORTES");

	
	/**
	 * Cierre Documento value Rol(103, "cierreDocumento").
	 */
	//NOT USED
	//public static final Rol CIERRE_DOCUMENTO = new Rol(103, "MENU_CIERRE_DOCUMENTOS");
	
	
	/**
	 * GESTION value Rol(104, "GESTION").
	 */
	public static final Rol GESTION = new Rol(104, "GESTION");
	
	
	/**
	 * SOLICITANTE_DE_RESPUESTA value Rol(105, "SOLICITANTE_DE_RESPUESTA").
	 */
	public static final Rol SOLICITANTE_DE_RESPUESTA = new Rol(105, "SOLICITANTE_DE_RESPUESTA");
	
	/**
	 * Visador value Rol(300, "VISADOR").
	 */
	public static final Rol VISADOR = new Rol(300, "VISADOR");
	
	/**
	 * Revisor value Rol(301, "REVISOR").
	 */
	public static final Rol REVISOR = new Rol(301, "REVISOR");
	
	/**
	 * REPORTE_REVISION value Rol(302, "REPORTE_REVISION").
	 */
	public static final Rol REPORTE_REVISION = new Rol(302, "REPORTE_REVISION");
	
	/**
	 * TRANSFORMA_DIGITAL_A_PAPEL value Rol(303, "TRANSFORMA_DIGITAL_A_PAPEL");
	 */
	public static final Rol TRANSFORMA_PAPEL_A_DIGITAL = new Rol(303, "TRANSFORMA_PAPEL_A_DIGITAL");
	
	private static final long serialVersionUID = 7096514713341079074L;

	private Integer id;
	private String descripcion;
	private List<Persona> personas;

	/**
	 * Constructor.
	 */
	public Rol() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Integer}
	 * @param descripcion {@link String}
	 */
	public Rol(final Integer id, final String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ROLES")
	public Integer getId() {
		return id;
	}

	/**
	 * @param id {@link Integer}
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion {@link String}
	 */
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return {@link List} of {@link Persona}
	 */
	@ManyToMany(mappedBy = "roles")
	public List<Persona> getPersonas() {
		return personas;
	}

	/**
	 * @param personas {@link List} of {@link Persona}
	 */
	public void setPersonas(final List<Persona> personas) {
		this.personas = personas;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof Rol) {
			final Rol r = (Rol) obj;
			return r.getId().equals(this.getId());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}

}
