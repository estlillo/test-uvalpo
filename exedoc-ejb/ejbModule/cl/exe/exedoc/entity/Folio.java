package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "FOLIOS")
@NamedQueries( { @NamedQuery(name = "Folio.findById", query = "SELECT f FROM Folio f WHERE f.id = :id"), @NamedQuery(name = "Folio.findByAll", query = "SELECT f FROM Folio f"),
		@NamedQuery(name = "Folio.findByIdTipoDocumento", query = "SELECT f FROM Folio f WHERE f.tipoDocumento.id = :id") })
public class Folio implements Serializable {

	private static final long serialVersionUID = -5535896380905609086L;

	private Long id;
	private Long numeroActual;
	private Integer agnoActual;
	private TipoDocumento tipoDocumento;

	@Column(name = "ID")
	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_DOCUMENTO", unique = true)
	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Column(name = "NUMERO_ACTUAL")
	public Long getNumeroActual() {
		return numeroActual;
	}

	public void setNumeroActual(Long numeroActual) {
		this.numeroActual = numeroActual;
	}

	@Column(name = "AGNO_ACTUAL")
	public Integer getAgnoActual() {
		return agnoActual;
	}

	public void setAgnoActual(Integer agnoActual) {
		this.agnoActual = agnoActual;
	}

}
