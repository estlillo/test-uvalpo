package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DOCUMENTOS_EXPEDIENTES")
@SequenceGenerator(name = "SEQ_DOCUMENTOS_EXPEDIENTES", sequenceName = "SEQ_DOCUMENTOS_EXPEDIENTES", allocationSize = 1, initialValue = 1)
public class DocumentoExpediente implements Serializable {

	private static final long serialVersionUID = 1512450432007626871L;

	private Long id;
	private Expediente expediente;
	private Documento documento;
	private TipoDocumentoExpediente tipoDocumentoExpediente;
	private Boolean enEdicion;

	 private Long idDocumentoReferencia;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DOCUMENTOS_EXPEDIENTES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EXPEDIENTE", nullable = false)
	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_DOCUMENTO_EXPEDIENTE", nullable = false)
	public TipoDocumentoExpediente getTipoDocumentoExpediente() {
		return tipoDocumentoExpediente;
	}

	public void setTipoDocumentoExpediente(TipoDocumentoExpediente tipoDocumentoExpediente) {
		this.tipoDocumentoExpediente = tipoDocumentoExpediente;
	}

	@Column(name = "EN_EDICION")
	public Boolean getEnEdicion() {
		return enEdicion;
	}

	public void setEnEdicion(Boolean enEdicion) {
		this.enEdicion = enEdicion;
	}

	
	  @Column(name = "ID_DOCUMENTO_REFERENCIA")
	  public Long getIdDocumentoReferencia() {
		  return idDocumentoReferencia;
	  }
	  
	  public void setIdDocumentoReferencia(Long idDocumentoReferencia) {
		  this.idDocumentoReferencia = idDocumentoReferencia;
	  }
	 

	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [documento: ");
		representacion.append(this.documento);
		representacion.append("], [tipoDocumentoExpediente: ");
		representacion.append(this.tipoDocumentoExpediente);
		representacion.append("], [enEdicion: ");
		representacion.append(this.enEdicion);
		representacion.append("]");
		return representacion.toString();
	}
}
