package cl.exe.exedoc.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Version;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "HISTORIAL_DESPACHO")
@SequenceGenerator(name = "SEQ_HISTORIAL_DESPACHO", sequenceName = "SEQ_HISTORIAL_DESPACHO", allocationSize = 1, initialValue = 1)
@NamedQueries({
				@NamedQuery(name = "HistorialDespacho.findByIdExp", query = "SELECT h FROM HistorialDespacho h WHERE h.expediente = :expediente")
				})
public class HistorialDespacho implements Serializable {
	
private static final long serialVersionUID = 1081815713042933062L; //cambiar

private Long id;
private Expediente expediente;
private Persona emisor;
private Persona destinatario;
private Date fechaDespacho;


@Id
@Column(name = "ID", nullable = false)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_HISTORIAL_DESPACHO")
@NotNull
public Long getId(){
	return id;
}

public void setId(Long id){
	this.id = id;
}



@OneToOne
@JoinColumn(name = "ID_EXPEDIENTE" , nullable = true)
public Expediente getExpediente() {
	return expediente;
}

public void setExpediente(Expediente expediente) {
	this.expediente = expediente;
}



@OneToOne
@JoinColumn(name = "ID_EMISOR" , nullable = true)
public Persona getEmisor(){
	return emisor;
}

public void setEmisor(Persona emisor){
	this.emisor = emisor;
}

@OneToOne
@JoinColumn(name = "ID_DESTINATARIO" , nullable = true)
public Persona getDestinatario(){
	return destinatario;
}

public void setDestinatario(Persona destinatario){
	this.destinatario = destinatario;
}

@Column (name = "FECHA_DESPACHO", nullable = false)
@Temporal (TemporalType.TIMESTAMP)
public Date getFechaDespacho(){
	return fechaDespacho;
}

public void setFechaDespacho(Date fechaDespacho){
	this.fechaDespacho = fechaDespacho;
}

}
//modificada el 27-02
