package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name= "DETALLE_DIAS_VACACIONES")
@SequenceGenerator(name = "SEQ_DETALLE_DIAS_VACACIONES", sequenceName="SEQ_DETALLE_DIAS_VACACIONES", allocationSize = 1, initialValue = 1)
public class DetalleDiasVacaciones implements Serializable{

	private static final long serialVersionUID = 648141987806085135L;
	
	private Long id;
	private Date fechaInicio;
	private Date fechaTermino;
	private SolicitudVacaciones solicitudVacaciones;
		
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_DETALLE_DIAS_VACACIONES")
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "FECHA_INICIO")
	@Temporal(TemporalType.DATE)
	public Date getFechaInicio() {
		return fechaInicio;
	}
	
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Column(name = "FECHA_TERMINO")
	@Temporal(TemporalType.DATE)
	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITUD_VACACIONES")
	public SolicitudVacaciones getSolicitudVacaciones() {
		return solicitudVacaciones;
	}

	public void setSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones) {
		this.solicitudVacaciones = solicitudVacaciones;
	}
	
	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [fechaInicio: ");
		representacion.append(this.fechaInicio);
		representacion.append("], [fechaTermino: ");
		representacion.append(this.fechaTermino);
		representacion.append("], [solicitudVacaciones: ");
		representacion.append(this.solicitudVacaciones.getId());
		representacion.append("]");
		return representacion.toString();
	}
}