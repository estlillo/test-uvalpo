package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FERIADOS")
@SequenceGenerator(name = "SEQ_FERIADOS", sequenceName = "SEQ_FERIADOS", allocationSize = 1, initialValue = 1)
@NamedQueries({
	@NamedQuery(name = "Feriados.findByAll", query = "SELECT f FROM Feriados f"), 
	@NamedQuery(name = "Feriados.esFeriado", 
		query = "SELECT f FROM Feriados f WHERE (f.dia = :dia AND f.mes = :mes AND f.agno = :agno) OR (f.dia = :dia AND f.mes = :mes AND (f.agno is null OR f.agno = 0))")
})
public class Feriados implements Serializable {

	private static final long serialVersionUID = 4678245050774586517L;

	private Long id;
	
	public Feriados() {
		super();
	}
	
	public Feriados(Long id) {
		this.id = id;
	}

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_FERIADOS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	/*private Date feriado;
	@Column(name = "FERIADO")
	@Temporal(TemporalType.DATE)
	public Date getFeriado() {
		return feriado;
	}

	public void setFeriado(Date feriado) {
		this.feriado = feriado;
	}*/
	
	private Integer dia;
	private Integer mes;
	private Integer agno;
	private String descripcion;
	
	@Column(name = "DIA")
	public Integer getDia() {
		return dia;
	}
	
	public void setDia(Integer dia) {
		this.dia = dia;
	}
	
	@Column(name = "MES")
	public Integer getMes() {
		return mes;
	}
	
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	
	@Column(name = "AGNO")
	public Integer getAgno() {
		return agno;
	}
	
	public void setAgno(Integer agno) {
		this.agno = agno;
	}

	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
}
