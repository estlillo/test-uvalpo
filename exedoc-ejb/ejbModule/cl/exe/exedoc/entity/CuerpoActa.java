package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("CuerpoActa")
@DiscriminatorValue("12")
public class CuerpoActa extends Parrafo implements Serializable {

	private static final long serialVersionUID = 4437647009373692896L;

}
