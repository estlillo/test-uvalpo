package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity Lista Personas Documentos.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "PERSONAS_DOCUMENTO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ID_TIPO_LISTA", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "SEQ_PERSONAS_DOCUMENTO", sequenceName = "SEQ_PERSONAS_DOCUMENTO", allocationSize = 1, initialValue = 1)
@NamedQueries(@NamedQuery(name = "ListaPersonasDocumento.DeleteAll", query = "delete from ListaPersonasDocumento l where l.documento.id = :idDocumento"))
public class ListaPersonasDocumento implements Serializable {

	private static final long serialVersionUID = -5684891732606098412L;

	private Long id;
	private String destinatario;
	private Persona destinatarioPersona;
	private Documento documento;

	/**
	 * Constructor.
	 */
	public ListaPersonasDocumento() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public ListaPersonasDocumento(final Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID_PERSONAS_DOCUMENTO")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERSONAS_DOCUMENTO")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "DESTINATARIO_EXTERNO")
	public String getDestinatario() {
		if(destinatario == null) return "";
		if (destinatario.trim().length() > 50) {
			return destinatario.substring(0, 50).concat("...");
		} else {
			return destinatario;
		}
		
	}

	/**
	 * @param destinatario {@link String}
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return {@link Documento}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @param documento {@link Documento}
	 */
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	/**
	 * @return {@link Persona}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_DESTINATARIO", nullable = true)
	public Persona getDestinatarioPersona() {
		return destinatarioPersona;
	}

	/**
	 * @param destinatarioPersona {@link Persona}
	 */
	public void setDestinatarioPersona(final Persona destinatarioPersona) {
		this.destinatarioPersona = destinatarioPersona;
	}

	
	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof ListaPersonasDocumento) {
			final ListaPersonasDocumento lpd = (ListaPersonasDocumento) obj;
			return lpd.getId().equals(this.id);
		}
		return false;
	}

	@Override
	public String toString() {
		return destinatario;
	}
}
