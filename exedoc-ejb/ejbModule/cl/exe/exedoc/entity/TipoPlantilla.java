package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPOS_PLANTILLAS")
public class TipoPlantilla implements Serializable {

	private static final long serialVersionUID = -1911979333240414967L;

	private Integer id;
	private String descripcion;
	
	public static final Integer RESOLUCION_DECRETO = 1;
	public static final Integer CONVENIO = 2;
	public static final Integer CONTRATO = 3;
	
	public TipoPlantilla() {
		
	}
	
	public TipoPlantilla(Integer id) {
		this.id = id;
		
		switch(id) {
		case 1:
			this.descripcion = "RESOLUCION/DECRETO";
			break;
		case 2:
			this.descripcion = "CONVENIO";
			break;
		case 3:
			this.descripcion = "CONTRATO";
			break;
		}
	}
	
	@Column(name = "ID")
	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "DESCRIPCION", length = 100)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
