package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

@Entity
@Name("Puntos")
@DiscriminatorValue("7")
public class Puntos extends Parrafo implements Serializable {

	private static final long serialVersionUID = 224573766633568388L;

}
