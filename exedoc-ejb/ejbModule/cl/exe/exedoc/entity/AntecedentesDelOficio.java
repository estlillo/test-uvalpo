package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

/**
 * Entity AntecedentesDelOficio.
 * 
 * @author Administrator
 *
 */
@Entity
@Name("AntecedentesDelOficio")
@DiscriminatorValue("2")
public class AntecedentesDelOficio extends Parrafo implements Serializable {

	private static final long serialVersionUID = -6002596498653284923L;

	/**
	 * Constructor.
	 */
	public AntecedentesDelOficio() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public AntecedentesDelOficio(final Long id) {
		super(id);
	}

}
