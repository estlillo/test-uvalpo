package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jboss.seam.annotations.Name;

/**
 * @author Ricardo Fuentes
 *
 */
@Entity
@Name("DocumentoPapel")
@DiscriminatorValue("1")
public class DocumentoPapel extends Documento implements Serializable {

	private static final long serialVersionUID = -4237808761549434714L;
	
	private Integer numeroHojas;
	
	private Integer numeroPaginas;

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "NUMERO_HOJAS")
	public Integer getNumeroHojas() {
		return numeroHojas;
	}

	/**
	 * @param numeroHojas {@link Integer}
	 */
	public void setNumeroHojas(final Integer numeroHojas) {
		this.numeroHojas = numeroHojas;
	}

	/**
	 * @return {@link Integer}
	 */
	@Column(name = "NUMERO_PAGINAS")
	public Integer getNumeroPaginas() {
		return numeroPaginas;
	}

	/**
	 * @param numeroPaginas {@link Integer}
	 */
	public void setNumeroPaginas(final Integer numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[[numeroHojas: ");
		representacion.append(this.numeroHojas);
		representacion.append("], [numeroPaginas: ");
		representacion.append(this.numeroPaginas);
		representacion.append("], [Documento: ");
		representacion.append(super.toString());
		representacion.append("]]");
		return representacion.toString();
	}
}
