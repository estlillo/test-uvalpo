package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.Length;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import cl.exe.exedoc.session.exception.RUTException;
import cl.exe.exedoc.util.RutUtil;

/**
 * Entity Persona.
 * 
 * @author Ricardo
 */
@Entity
@Table(name = "PERSONAS")
@SequenceGenerator(name = "SEQ_PERSONAS", sequenceName = "SEQ_PERSONAS", allocationSize = 1, initialValue = 1)
@NamedQueries({
		@NamedQuery(name = "Persona.findById", query = "SELECT p FROM Persona p WHERE p.id = :id"),
		@NamedQuery(name = "Persona.findByAll", query = "SELECT p FROM Persona p"),
		@NamedQuery(name = "Persona.findByIdCargo", query = "SELECT p FROM Persona p WHERE p.cargo.id = :idCargo"),
		@NamedQuery(name = "Persona.findImagenByRut", query = "SELECT p.imagenFirma FROM Persona p WHERE p.rut = :rut"),
		@NamedQuery(name = "Persona.findByRut", query = "SELECT count(p) FROM Persona p WHERE p.rut = :rut"),
		@NamedQuery(name = "Persona.findByRut_rut", query = "SELECT p.rut FROM Persona p WHERE p.rut = :rut"),
		@NamedQuery(name = "Persona.findByRutVigente", 
				query = "SELECT p FROM Persona p WHERE p.rut = :rut AND p.vigente = :vigente"),
		@NamedQuery(name = "Persona.findByUsuarioVigente", 
				query = "SELECT p FROM Persona p WHERE p.usuario = :usuario AND p.vigente = :vigente") 
		})
public class Persona implements Serializable {

	/**
	 * ID_USUARIO_CIUDADANO.
	 */
	public static final long ID_USUARIO_CIUDADANO = 1502901L;
	/**
	 * ID_USUARIO_TRANSPARENCIA.
	 */
	public static final long ID_USUARIO_TRANSPARENCIA = 1503254L;

	private static final long serialVersionUID = 4015633889158096458L;
	
	private static Log log = Logging.getLog(Persona.class);

	private Long id;
	private Long idPersona;
	private String rut;
	private Cargo cargo;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String email;
	private String usuario;
	private Boolean vigente = true;
	private Boolean distribucion = true;
	private Date fechaNoVigente;
	private Boolean personaDefault;
	private String iniciales;
	private byte[] keyStore;
	private String keyStorePassword;
	private String keyStoreAlias;
	private List<Rol> roles = new ArrayList<Rol>();
	private TipoFirma tipoFirma;
	private List<DestinatariosFrecuentes> destinatariosFrecuentes;
	private Boolean externo;
	private byte[] imagenFirma;
	private transient boolean destinatarioConRecepcion;
	private transient boolean destinatarioConCopia;
	private transient boolean borrable;
	private Persona aprobadorActosAdministrativos;
	private List<GrupoUsuario> grupos;

	/**
	 * Constructor.
	 */
	public Persona() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param id {@link Long}
	 */
	public Persona(final Long id) {
		super();
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PERSONAS")
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Long}
	 */
	@Column(name = "ID_PERSONA")
	public Long getIdPersona() {
		return idPersona;
	}

	/**
	 * @param idPersona {@link Long}
	 */
	public void setIdPersona(final Long idPersona) {
		this.idPersona = idPersona;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "RUT", length = 13, unique = true)
	@Length(max = 13)
	public String getRut() {
		return rut;
	}

	/**
	 * @param rut {@link String}
	 */
	public void setRut(final String rut) {
		this.rut = rut;
	}

	/**
	 * Metodo transient para obtener el rut del usuario formateado para la firma.
	 * 
	 * @return {@link String} el rut, formateado para firma.
	 */
	@Transient
	public String getRutFirma() {
		String rutFirma = this.rut;
		// Quitar primer 0 al RUT.
		rutFirma = rutFirma.substring(1);

		// Si el digito que viene es un 0, tambien debe quitarse.
		// Sino, se deja.
		if (rutFirma.charAt(0) == '0') {
			rutFirma = rutFirma.substring(1);
		}

		// Obtener digito verificador.
		final String verificador = rutFirma.substring(rutFirma.length() - 1);

		// Obtener todo lo de la izquierda del digito verificador.
		rutFirma = rutFirma.substring(0, rutFirma.length() - 1);

		// Anexar raya al final de parte del rut sin digito verificador.
		rutFirma = rutFirma.concat("-");

		// Anexar digito verificador.
		rutFirma = rutFirma.concat(verificador);

		return rutFirma;
	}

	/**
	 * @return {@link Cargo}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_CARGO", nullable = true)
	public Cargo getCargo() {
		return cargo;
	}

	/**
	 * @param cargo {@link Cargo}
	 */
	public void setCargo(final Cargo cargo) {
		this.cargo = cargo;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "NOMBRES", length = 100)
	@Length(max = 100)
	public String getNombres() {
		return nombres;
	}

	/**
	 * @param nombres {@link String}
	 */
	public void setNombres(final String nombres) {
		this.nombres = nombres;
		if (nombres != null) {
			this.nombres = nombres.toUpperCase();
		}
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "APELLIDO_PATERNO", length = 100)
	@Length(max = 100)
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	/**
	 * @param apellidoPaterno {@link String}
	 */
	public void setApellidoPaterno(final String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
		if (apellidoPaterno != null) {
			this.apellidoPaterno = apellidoPaterno.toUpperCase();
		}
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "APELLIDO_MATERNO", length = 100)
	@Length(max = 100)
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	/**
	 * @param apellidoMaterno {@link String}
	 */
	public void setApellidoMaterno(final String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
		if (apellidoMaterno != null) {
			this.apellidoMaterno = apellidoMaterno.toUpperCase();
		}
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "EMAIL", length = 50, nullable = true)
	@Length(max = 50)
	public String getEmail() {
		return email;
	}

	/**
	 * @param email {@link String}
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "USUARIO", unique = true)
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario {@link String}
	 */
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "VIGENTE")
	public Boolean getVigente() {
		return vigente;
	}

	/**
	 * @param vigente {@link Boolean}
	 */
	public void setVigente(final Boolean vigente) {
		this.vigente = vigente;
	}

	/**
	 * @return {@link Date}
	 */
	@Column(name = "FECHA_NO_VIGENTE")
	public Date getFechaNoVigente() {
		return fechaNoVigente;
	}

	/**
	 * @param fechaNoVigente {@link Date}
	 */
	public void setFechaNoVigente(final Date fechaNoVigente) {
		this.fechaNoVigente = fechaNoVigente;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "PERSONA_DEFAULT")
	public Boolean getPersonaDefault() {
		return personaDefault;
	}

	/**
	 * @param personaDefault {@link Boolean}
	 */
	public void setPersonaDefault(final Boolean personaDefault) {
		this.personaDefault = personaDefault;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "INICIALES")
	public String getIniciales() {
		return iniciales;
	}

	/**
	 * @param iniciales {@link String}
	 */
	public void setIniciales(final String iniciales) {
		this.iniciales = iniciales;
	}

	/**
	 * @return {@link Byte}
	 */
	@Column(name = "KEY_STORE")
	@Lob
	public byte[] getKeyStore() {
		return keyStore;
	}

	/**
	 * @param keyStore {@link Byte}
	 */
	public void setKeyStore(final byte[] keyStore) {
		this.keyStore = keyStore;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "KEY_STORE_PASSWORD")
	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	/**
	 * @param keyStorePassword {@link String}
	 */
	public void setKeyStorePassword(final String keyStorePassword) {
		this.keyStorePassword = keyStorePassword;
	}

	/**
	 * @return {@link String}
	 */
	@Column(name = "KEY_STORE_ALIAS")
	public String getKeyStoreAlias() {
		return keyStoreAlias;
	}

	/**
	 * @param keyStoreAlias {@link String}
	 */
	public void setKeyStoreAlias(final String keyStoreAlias) {
		this.keyStoreAlias = keyStoreAlias;
	}

	/**
	 * @return {@link List} of {@link Rol}
	 */
	@ManyToMany
	@JoinTable(name = "ROLES_PERSONAS", joinColumns = @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_ROL", referencedColumnName = "ID"))
	public List<Rol> getRoles() {
		return roles;
	}

	/**
	 * @param roles {@link List} of {@link Rol}
	 */
	public void setRoles(final List<Rol> roles) {
		this.roles = roles;
	}

	public void addRoles(Rol rol) {
		this.roles.add(rol);
	}

	/**
	 * @return {@link TipoFirma}
	 */
	@ManyToOne
	@JoinColumn(name = "ID_TIPO_FIRMA", nullable = true)
	public TipoFirma getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * @param tipoFirma {@link TipoFirma}
	 */
	public void setTipoFirma(final TipoFirma tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	/**
	 * @return {@link List} of {@link DestinatariosFrecuentes}
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "propietario")
	public List<DestinatariosFrecuentes> getDestinatariosFrecuentes() {
		return destinatariosFrecuentes;
	}

	/**
	 * @param destinatariosFrecuentes {@link List} of {@link DestinatariosFrecuentes}
	 */
	public void setDestinatariosFrecuentes(final List<DestinatariosFrecuentes> destinatariosFrecuentes) {
		this.destinatariosFrecuentes = destinatariosFrecuentes;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Column(name = "EXTERNO")
	public Boolean getExterno() {
		return externo;
	}

	/**
	 * @param externo {@link Boolean}
	 */
	public void setExterno(final Boolean externo) {
		this.externo = externo;
	}

	/**
	 * @return {@link Byte}
	 */
	@Column(name = "IMAGEN_FIRMA")
	@Lob
	public byte[] getImagenFirma() {
		return imagenFirma;
	}

	/**
	 * @param imagenFirma {@link Byte}
	 */
	public void setImagenFirma(final byte[] imagenFirma) {
		this.imagenFirma = imagenFirma;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof Persona) {
			final Persona e = (Persona) obj;
			if (this.id.equals(e.getId())) { return true; }
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder representacion = new StringBuilder();
		representacion.append("[id: ");
		representacion.append(this.id);
		representacion.append("], [rut: ");
		representacion.append(this.rut);
		representacion.append("], [cargo: ");
		representacion.append(this.cargo);
		representacion.append("], [nombres: ");
		representacion.append(this.nombres);
		representacion.append("], [apellidoPaterno: ");
		representacion.append(this.apellidoPaterno);
		representacion.append("], [apellidoMaterno: ");
		representacion.append(this.apellidoMaterno);
		representacion.append("], [email: ");
		representacion.append(this.email);
		representacion.append("], [usuario: ");
		representacion.append(this.usuario);
		representacion.append("], [vigente: ");
		representacion.append(this.vigente);
		representacion.append("], [fechaNoVigente: ");
		representacion.append(this.fechaNoVigente);
		representacion.append("], [personaDefault: ");
		representacion.append(this.personaDefault);
		representacion.append("], [iniciales: ");
		representacion.append(this.iniciales);
		representacion.append("]");
		return representacion.toString();
	}

	/**
	 * @return {@link String}
	 */
	@Transient
	public String getNombreApellido() {
		return ((this.nombres != null) ? this.nombres : "") + " "
				+ ((this.apellidoPaterno != null) ? this.apellidoPaterno : "");
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public boolean getDestinatarioConRecepcion() {
		return destinatarioConRecepcion;
	}

	/**
	 * @param destinatarioConRecepcion {@link Boolean}
	 */
	public void setDestinatarioConRecepcion(final boolean destinatarioConRecepcion) {
		this.destinatarioConRecepcion = destinatarioConRecepcion;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public boolean getDestinatarioConCopia() {
		return destinatarioConCopia;
	}

	/**
	 * @param destinatarioConCopia {@link Boolean}
	 */
	public void setDestinatarioConCopia(final boolean destinatarioConCopia) {
		this.destinatarioConCopia = destinatarioConCopia;
	}

	/**
	 * @return {@link Boolean}
	 */
	@Transient
	public boolean isBorrable() {
		return borrable;
	}

	/**
	 * @param borrable {@link Boolean}
	 */
	public void setBorrable(final boolean borrable) {
		this.borrable = borrable;
	}

	/**
	 * @return {@link String}
	 */
	@Transient
	public String getRutFormat() {

		try {
			final RutUtil ruT = new RutUtil(this.getRut());

			return ruT.getFormated();

		} catch (RUTException e) {
			log.info(e.getMessage());
		} catch (NullPointerException e) {
			log.info(e.getMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			log.info(e.getMessage());
		}
		return "";
	}
	
	@Column(name = "DISTRIBUCION")
	public Boolean getDistribucion() {
		return distribucion;
	}

	public void setDistribucion(Boolean distribucion) {
		this.distribucion = distribucion;
	}
	
	@ManyToOne
	@JoinColumn(name = "ID_PERSONA_APROBADOR", nullable = true)
	public Persona getAprobadorActosAdministrativos() {
		return aprobadorActosAdministrativos;
	}
	
	public void setAprobadorActosAdministrativos(
			Persona aprobadorActosAdministrativos) {
		this.aprobadorActosAdministrativos = aprobadorActosAdministrativos;
	}

	@ManyToMany
	@JoinTable(name = "PERSONAS_GRUPO", joinColumns = @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID"))
	public List<GrupoUsuario> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<GrupoUsuario> grupos) {
		this.grupos = grupos;
	}
}
