package cl.exe.exedoc.entity;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue("3")
public class InformacionFuncionario extends InformacionDocumento{
	
	private static final long serialVersionUID = 7243524812713762381L;

	private Persona funcionario;
	private Date desde;
	private Date hasta;
	private int tipoLicencia;
	private String nombreMedico;
	private String rutMedico;
	
	
	public Persona getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Persona funcionario) {
		this.funcionario = funcionario;
	}
	public Date getDesde() {
		return desde;
	}
	public void setDesde(Date desde) {
		this.desde = desde;
	}
	public Date getHasta() {
		return hasta;
	}
	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}
	public int getTipoLicencia() {
		return tipoLicencia;
	}
	public void setTipoLicencia(int tipoLicencia) {
		this.tipoLicencia = tipoLicencia;
	}
	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}
	public String getNombreMedico() {
		return nombreMedico;
	}
	public void setRutMedico(String rutMedico) {
		this.rutMedico = rutMedico;
	}
	public String getRutMedico() {
		return rutMedico;
	}

	
	
}
