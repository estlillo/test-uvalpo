package cl.exe.exedoc.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.validator.Length;
import org.jboss.seam.annotations.Name;

@Entity
@Name("CONTRATO")
@DiscriminatorValue("23")
public class Contrato extends DocumentoElectronico {

	private static final long serialVersionUID = -6682882479071486412L;

	private String titulo;
	private String prologo;
	//private ClasificacionTipoOtros clasificacionTipoOtros;
	//private ClasificacionSubtipoOtros clasificacionSubtipoOtros;
	
	@Column(name = "TITULO", length = 250)
	@Length(max = 250)
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	@Column(name = "PROLOGO", length = 2000)
	@Length(max = 2000)
	public String getPrologo() {
		return prologo;
	}
	
	public void setPrologo(String prologo) {
		this.prologo = prologo;
	}
	
	/*@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_TIPO_OTROS", nullable = true)
	public ClasificacionTipoOtros getClasificacionTipoOtros() {
		return clasificacionTipoOtros;
	}

	public void setClasificacionTipoOtros(ClasificacionTipoOtros clasificacionTipoOtros) {
		this.clasificacionTipoOtros = clasificacionTipoOtros;
	}

	@ManyToOne
	@JoinColumn(name = "ID_CLASIFICACION_SUBTIPO_OTROS", nullable = true)
	public ClasificacionSubtipoOtros getClasificacionSubtipoOtros() {
		return clasificacionSubtipoOtros;
	}

	public void setClasificacionSubtipoOtros(ClasificacionSubtipoOtros clasificacionSubtipoOtros) {
		this.clasificacionSubtipoOtros = clasificacionSubtipoOtros;
	}*/
	
	@Transient
	public List<Parrafo> getPuntos() {
		if (super.parrafos != null) {
			List<Parrafo> tmp = new ArrayList<Parrafo>();
			for (Parrafo p : super.parrafos) {
				if (p instanceof Puntos) {
					tmp.add(p);
				}
			}
			return tmp;
		}
		return null;
	}

	public void setPuntos(List<Parrafo> puntos) {
		if (super.parrafos == null) {
			super.parrafos = new ArrayList<Parrafo>();
		}
		super.parrafos.addAll(puntos);
	}
}
