package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "BITACORAS")
@SequenceGenerator(name = "SEQ_BITACORAS", sequenceName = "SEQ_BITACORAS", allocationSize = 1, initialValue = 1)
public class Bitacora implements Serializable {

	private static final long serialVersionUID = 7055651537466318555L;

	private Long id;
	private Documento documento;
	private Date fecha;
	private Persona persona;
	private EstadoDocumento estadoDocumento;

	public Bitacora() {
	}

	public Bitacora(int estadoDocumento, Documento documento, Persona persona) {
		this.documento = documento;
		this.fecha = new Date();
		this.persona = persona;
		this.estadoDocumento = new EstadoDocumento(estadoDocumento);
	}

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BITACORAS")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO")
	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PERSONA", nullable = false)
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@ManyToOne
	@JoinColumn(name = "ID_ESTADO_DOCUMENTO", nullable = true)
	public EstadoDocumento getEstadoDocumento() {
		return estadoDocumento;
	}

	public void setEstadoDocumento(EstadoDocumento estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}
}
