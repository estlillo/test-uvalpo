package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "PLANTILLA_RESPUESTA")
@SequenceGenerator(name = "SEQ_PLANTILLA_RESPUESTA", sequenceName = "SEQ_PLANTILLA_RESPUESTA", allocationSize = 1, initialValue = 1)
@NamedQueries( { @NamedQuery(name = "PlantillaRespuesta.findById", query = "SELECT pr FROM PlantillaRespuesta pr WHERE pr.id = :id"),
		@NamedQuery(name = "PlantillaRespuesta.findByAll", query = "SELECT pr FROM PlantillaRespuesta pr") })
public class PlantillaRespuesta implements Serializable {
	private static final long serialVersionUID = -3254446130644191660L;

	private Integer id;
	private String nombre;
	private String contenido;
	
	@Column(name = "ID") @Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLANTILLA_RESPUESTA")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "NOMBRE", length = 100)
	@Length(max = 100)
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(name = "CUERPO", length = 2000)
	@Length(max = 2000)
	public String getContenido() {
		return contenido;
	}
	
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
}
