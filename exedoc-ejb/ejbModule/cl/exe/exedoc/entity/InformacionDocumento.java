package cl.exe.exedoc.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INFO_DOCUMENTO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ID_TIPO_INFO", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "SEQ_INFO_DOCUMENTO", sequenceName = "SEQ_INFO_DOCUMENTO", allocationSize = 1, initialValue = 1)
public class InformacionDocumento implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6977135443641159466L;
	
	private Integer id;
	protected Documento documento;

	
	@ManyToOne
	@JoinColumn(name = "ID_DOCUMENTO", nullable = false)
	public Documento getDocumento() {
		return documento;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_INFO_DOCUMENTO")
	public Integer getId() {
		return id;
	}
	
	@PreRemove
	public void preRemove() {
		setDocumento(null);
	}
	
}
