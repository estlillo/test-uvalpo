package cl.exe.exedoc.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.validator.Length;

@Entity
@Table(name = "DESTINATARIOS_FRECUENTES")
@SequenceGenerator(name = "SEQ_DFRECUENTES", sequenceName = "SEQ_DFRECUENTES", allocationSize = 1, initialValue = 1)
@NamedQueries( { @NamedQuery(name = "DestinatariosFrecuentes.findById", query = "SELECT df FROM DestinatariosFrecuentes df WHERE df.id = :id"),
		@NamedQuery(name = "DestinatariosFrecuentes.findByAll", query = "SELECT df FROM DestinatariosFrecuentes df") })
public class DestinatariosFrecuentes implements Serializable {

	private static final long serialVersionUID = 6372297151434155389L;

	private Long id;
	private String nombreLista;
	private Persona propietario;
	private List<Persona> destinatariosFrecuentes;
	private Boolean esGrupo;

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DFRECUENTES")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NOMBRE_LISTA", length = 100)
	@Length(max = 100)
	public String getNombreLista() {
		return nombreLista;
	}

	public void setNombreLista(String nombreLista) {
		this.nombreLista = nombreLista;
	}

	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "ID_PROPIETARIO")
	public Persona getPropietario() {
		return propietario;
	}

	public void setPropietario(Persona propietario) {
		this.propietario = propietario;
	}

	@ManyToMany
	@JoinTable(name = "LISTAS_PERSONAS", joinColumns = @JoinColumn(name = "ID_LISTA", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID"))
	public List<Persona> getDestinatariosFrecuentes() {
		return destinatariosFrecuentes;
	}

	public void setDestinatariosFrecuentes(List<Persona> destinatariosFrecuentes) {
		this.destinatariosFrecuentes = destinatariosFrecuentes;
	}

	@Column(name = "ES_GRUPO")
	public Boolean getEsGrupo() {
		return esGrupo;
	}

	public void setEsGrupo(Boolean esGrupo) {
		this.esGrupo = esGrupo;
	}
}
