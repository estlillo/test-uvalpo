package cl.exe.exedoc.pojo.expediente;

import java.io.Serializable;
import java.util.Date;

public class DatosUsuarioBandejaSalida implements Serializable {
	
	private static final long serialVersionUID = 1276743875332524141L;
	
	private Long idExpediente;
	
	private Date fechaIngresoBandeja;
	
	private Date fechaDespacho;
	
	private String destinatarioUnidadOrganizacional;
	
	private String destinatarioCargo;
	
	private String destinatarioUsuario;
	
	private Date fechaAcuseRecibo;
	
	private Date fechaModificacionDocumento;
	
	private String estadoDocumentoModificado;
	
	private Date fechaArchivado;
	
	private String copia;
	
	private String nombreEmisor;
	
	private String destinatarioDepartamento;
	
	private Date fechaReasignado;
	
	public DatosUsuarioBandejaSalida() {
		
	}
	
	public String getNombreEmisor() {
		return nombreEmisor;
	}

	public void setNombreEmisor(String nombreEmisor) {
		this.nombreEmisor = nombreEmisor;
	}

	public Long getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(Long idExpediente) {
		this.idExpediente = idExpediente;
	}

	public Date getFechaIngresoBandeja() {
		return fechaIngresoBandeja;
	}

	public void setFechaIngresoBandeja(Date fechaIngresoBandeja) {
		this.fechaIngresoBandeja = fechaIngresoBandeja;
	}

	public Date getFechaDespacho() {
		return fechaDespacho;
	}

	public void setFechaDespacho(Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}

	public String getDestinatarioUnidadOrganizacional() {
		return destinatarioUnidadOrganizacional;
	}

	public void setDestinatarioUnidadOrganizacional(
			String destinatarioUnidadOrganizacional) {
		this.destinatarioUnidadOrganizacional = destinatarioUnidadOrganizacional;
	}

	public String getDestinatarioCargo() {
		return destinatarioCargo;
	}

	public void setDestinatarioCargo(String destinatarioCargo) {
		this.destinatarioCargo = destinatarioCargo;
	}

	public String getDestinatarioUsuario() {
		return destinatarioUsuario;
	}

	public void setDestinatarioUsuario(String destinatarioUsuario) {
		this.destinatarioUsuario = destinatarioUsuario;
	}

	public Date getFechaAcuseRecibo() {
		return fechaAcuseRecibo;
	}

	public void setFechaAcuseRecibo(Date fechaAcuseRecibo) {
		this.fechaAcuseRecibo = fechaAcuseRecibo;
	}

	public Date getFechaModificacionDocumento() {
		return fechaModificacionDocumento;
	}

	public void setFechaModificacionDocumento(Date fechaModificacionDocumento) {
		this.fechaModificacionDocumento = fechaModificacionDocumento;
	}

	public String getEstadoDocumentoModificado() {
		return estadoDocumentoModificado;
	}

	public void setEstadoDocumentoModificado(String estadoDocumentoModificado) {
		this.estadoDocumentoModificado = estadoDocumentoModificado;
	}
	
	public Date getFechaArchivado() {
		return fechaArchivado;
	}

	public void setFechaArchivado(Date fechaArchivado) {
		this.fechaArchivado = fechaArchivado;
	}
	
	public String getCopia() {
		return copia;
	}

	public void setCopia(String copia) {
		this.copia = copia;
	}
	
	@Override
	public String toString() {
		StringBuilder representacion = new StringBuilder();
		representacion.append("[fechaIngresoBandeja: ");
		representacion.append(this.fechaIngresoBandeja);
		representacion.append("], [fechaDespacho: ");
		representacion.append(this.fechaDespacho);
		representacion.append("], [destinatarioUnidadOrganizacional: ");
		representacion.append(this.destinatarioUnidadOrganizacional);
		representacion.append("], [destinatarioCargo: ");
		representacion.append(this.destinatarioCargo);
		representacion.append("], [destinatarioUsuario: ");
		representacion.append(this.destinatarioUsuario);
		representacion.append("], [fechaAcuseRecibo: ");
		representacion.append(this.fechaAcuseRecibo);		
		representacion.append("], [fechaModificacionDocumento: ");
		representacion.append(this.fechaModificacionDocumento);
		representacion.append("], [estadoDocumentoModificado: ");
		representacion.append(this.estadoDocumentoModificado);
		representacion.append("]");
		return representacion.toString();
	}

	public void setDestinatarioDepartamento(String destinatarioDepartamento) {
		this.destinatarioDepartamento = destinatarioDepartamento;
	}

	public String getDestinatarioDepartamento() {
		return destinatarioDepartamento;
	}

	public Date getFechaReasignado() {
		return fechaReasignado;
	}

	public void setFechaReasignado(Date fechaReasignado) {
		this.fechaReasignado = fechaReasignado;
	}



}
