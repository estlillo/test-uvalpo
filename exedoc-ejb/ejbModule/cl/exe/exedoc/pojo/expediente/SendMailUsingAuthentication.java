package cl.exe.exedoc.pojo.expediente;

/*
 * Some SMTP servers require a username and password authentication before you can use their Server for Sending mail.
 * This is most common with couple of ISP's who provide SMTP Address to Send Mail. This Program gives any example on how
 * to do SMTP Authentication (User and Password verification) This is a free source code and is provided as it is
 * without any warranties and it can be used in any your code for free. Author : Sudhir Ancha
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/*
 * To use this program, change values for the following three constants, SMTP_HOST_NAME -- Has your SMTP Host Name
 * SMTP_AUTH_USER -- Has your SMTP Authentication UserName SMTP_AUTH_PWD -- Has your SMTP Authentication Password Next
 * change values for fields emailMsgTxt -- Message Text for the Email emailSubjectTxt -- Subject for email
 * emailFromAddress -- Email Address whose name will appears as "from" address Next change value for "emailList". This
 * String array has List of all Email Addresses to Email Email needs to be sent to. Next to run the program, execute it
 * as follows, SendMailUsingAuthentication authProg = new SendMailUsingAuthentication();
 */

public class SendMailUsingAuthentication {

	private static String SMTP_MAIL_LIST_SEPARATOR = ",";
	private static String SMTP_HOST_NAME; // = "mail.subdere.gov.cl";
	private static String SMTP_AUTH_USER; // = "admexedoc";
	private static String SMTP_AUTH_PWD; // = "exedoc1437";
	private static String SMTP_CC;

	private static Boolean ACTIVE;

	private String emailMsgTxt;
	private String emailSubjectTxt;
	private String emailFromAddress;
	private byte[] file;
	private String filename;

	// Add List of Email address to who email needs to be sent to
	private String[] emailList;

	private static SendMailUsingAuthentication sendMail = null;

	private SendMailUsingAuthentication() {
		try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("mail.properties");
			Properties properties = new Properties();
			properties.load(inputStream);
			SMTP_HOST_NAME = properties.getProperty("SMTP_HOST_NAME");
			SMTP_AUTH_USER = properties.getProperty("SMTP_AUTH_USER");
			SMTP_AUTH_PWD = properties.getProperty("SMTP_AUTH_PWD");
			SMTP_CC = properties.getProperty("SMTP_CC");
			ACTIVE = properties.getProperty("ACTIVE").equals("y") ? true : false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static SendMailUsingAuthentication getSendMail() {
		if (sendMail == null) {
			sendMail = new SendMailUsingAuthentication();
		}
		limpiar();
		return sendMail;
	}

	public static void limpiar() {
		sendMail.emailMsgTxt = "";
		sendMail.emailSubjectTxt = "";
		sendMail.emailFromAddress = "";
		sendMail.file = null;
	}

	public void sendMail() throws Exception {
		if (ACTIVE) {
			// SendMailUsingAuthentication smtpMailSender = getSendMail();
			StringBuilder sb = new StringBuilder();
			for (String dest : getEmailList()) {
				sb.append(dest + " - ");
			}
			System.out.println("Sending mail to: " + sb.toString());
			// smtpMailSender.postMail(getEmailList(), getEmailSubjectTxt(),
			// getEmailMsgTxt(), getEmailFromAddress());
			postMail(getEmailList(), getEmailSubjectTxt(), getEmailMsgTxt(), getEmailFromAddress(), getFile(),
					getFilename());
			System.out.println("Sucessfully Sent mail to All Users");
			sendMail = null;
			emailMsgTxt = null;
			emailSubjectTxt = null;
			emailFromAddress = null;
			emailList = null;
			file = null;
			filename = null;
		} else {
			System.out.println("mail disabled");
		}
	}

	public void postMail(String recipients[], String subject, String message, String from, byte[] file, String filename)
			throws MessagingException {
		boolean debug = false;

		// Set the host smtp address
		Properties props = new Properties();
		try {

			props.load(Thread.currentThread().getContextClassLoader().getResource("mail.properties").openStream());

		} catch (IOException ioe) {

			props.put("mail.smtp.host", SMTP_HOST_NAME);
			props.put("mail.smtp.auth", "true");
		}

		Authenticator auth = new SMTPAuthenticator();
		Session session = Session.getDefaultInstance(props, auth);

		session.setDebug(debug);

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);

		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);

		String[] recipientsCC = new String[1];
		recipientsCC[0] = SMTP_CC;
		InternetAddress[] addressCC = new InternetAddress[recipientsCC.length];
		for (int i = 0; i < recipientsCC.length; i++) {
			addressCC[i] = new InternetAddress(recipientsCC[i]);
		}
		msg.setRecipients(Message.RecipientType.CC, addressCC);

		// Setting the Subject
		msg.setSubject(subject);

		// Setting the Date
		msg.setSentDate(new Date());

		// Message body
		MimeBodyPart body = new MimeBodyPart();
		body.setText(message);

		// Multipart
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(body);

		// Message attachment
		if (file != null && filename != null) {
			MimeBodyPart attach = new MimeBodyPart();
			DataSource fds = new ByteArrayDataSource(file, "application/pdf");
			attach.setDataHandler(new DataHandler(fds));
			attach.setFileName(filename);
			mp.addBodyPart(attach);
		}

		// msg.setContent(message, "text/plain");
		msg.setContent(mp);
		Transport.send(msg);
	}

	/**
	 * SimpleAuthenticator is used to do simple authentication when the SMTP server requires it.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {

		public PasswordAuthentication getPasswordAuthentication() {
			String username = SMTP_AUTH_USER;
			String password = SMTP_AUTH_PWD;
			return new PasswordAuthentication(username, password);
		}
	}

	public String getEmailMsgTxt() {
		return emailMsgTxt;
	}

	public void setEmailMsgTxt(String emailMsgTxt) {
		this.emailMsgTxt = emailMsgTxt;
	}

	public String getEmailSubjectTxt() {
		return emailSubjectTxt;
	}

	public void setEmailSubjectTxt(String emailSubjectTxt) {
		this.emailSubjectTxt = emailSubjectTxt;
	}

	public String getEmailFromAddress() {
		return emailFromAddress;
	}

	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}

	public String[] getEmailList() {
		return emailList;
	}

	public void setEmailList(String[] emailList) {
		this.emailList = emailList;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Boolean getACTIVE() {
		return ACTIVE;
	}

}
