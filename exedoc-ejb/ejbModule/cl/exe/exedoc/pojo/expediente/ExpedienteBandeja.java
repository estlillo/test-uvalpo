package cl.exe.exedoc.pojo.expediente;

import java.io.Serializable;
import java.util.Date;

import cl.exe.exedoc.entity.GrupoUsuario;

/**
 * Class utilizada para mostrar expediente en bandejas (entrada directo o copia, salida).
 * 
 * @author Ricardo Fuentes
 */
public class ExpedienteBandeja implements Serializable {

	private static final long serialVersionUID = 7277679852753922687L;
	private Long id;
	// private String numeroExpedienteOPartes;
	private Date fechaIngresoBandeja;
	private Date fechaAcuseRecibo;
	private Date fechaDespacho;
	private String numeroExpediente;
	private String nombreArchivo;
	private String tipoDocumento;
	private String materia;
	private String emisor;
	private String destinatario;
	private String formatoDocumento;
	private Date fechaDocumentoOrigen;
	private Date fechaIngreso;
	private Boolean recibido;
	private String numeroDocumento;
	private String rutProveedorFactura;
	private String numeroOrdenCompra;
	private String autor;
	private Boolean tieneCmsId;
	private Long idDocumento;
	private Date plazo;
	private Boolean completado;
	private Boolean copia;
	private Date fechaArchivado;
	private String resumenEjecutivo;
	private String motivoDevolucion;
	private String resumenEjecutivoRegistrar;
	private String motivoDevolucionRegistrar;
	private GrupoUsuario grupo;
	
	private String unidadEmisor;
	private String departamentoEmisor;
	private Date fechaReasignado;
	
	//= new GrupoUsuario();

	/**
	 * Constructor.
	 */
	public ExpedienteBandeja() {
		super();
	}

	/**
	 * @return {@link Long}
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id {@link Long}
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaIngresoBandeja() {
		return fechaIngresoBandeja;
	}

	/**
	 * @param fechaIngresoBandeja {@link Date}
	 */
	public void setFechaIngresoBandeja(final Date fechaIngresoBandeja) {
		this.fechaIngresoBandeja = fechaIngresoBandeja;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaAcuseRecibo() {
		return fechaAcuseRecibo;
	}

	/**
	 * @param fechaAcuseRecibo {@link Date}
	 */
	public void setFechaAcuseRecibo(final Date fechaAcuseRecibo) {
		this.fechaAcuseRecibo = fechaAcuseRecibo;
		this.recibido = false;
		if (fechaAcuseRecibo != null) {
			this.recibido = true;
		}
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaDespacho() {
		return fechaDespacho;
	}

	/**
	 * @param fechaDespacho {@link Date}
	 */
	public void setFechaDespacho(final Date fechaDespacho) {
		this.fechaDespacho = fechaDespacho;
	}

	/**
	 * @return {@link String}
	 */
	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	/**
	 * @param numeroExpediente {@link String}
	 */
	public void setNumeroExpediente(final String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	/**
	 * @return {@link String}
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento {@link String}
	 */
	public void setTipoDocumento(final String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return {@link String}
	 */
	public String getMateria() {
	//	return materia;
		if(materia == null) materia = "";
		if (materia.trim().length() > 100) {
			return materia.substring(0, 50).concat("...");
		} else {
			return materia;
		}
	}

	/**
	 * @param materia {@link String}
	 */
	public void setMateria(final String materia) {
		this.materia = materia;
	}

	/**
	 * @return {@link String}
	 */
	public String getEmisor() {
		return emisor;
	}

	/**
	 * @param emisor {@link String}
	 */
	public void setEmisor(final String emisor) {
		this.emisor = emisor;
	}

	/**
	 * @return {@link String}
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario {@link String}
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return {@link String}
	 */
	public String getFormatoDocumento() {
		return formatoDocumento;
	}

	/**
	 * @param formatoDocumento {@link String}
	 */
	public void setFormatoDocumento(final String formatoDocumento) {
		this.formatoDocumento = formatoDocumento;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaDocumentoOrigen() {
		return fechaDocumentoOrigen;
	}

	/**
	 * @param fechaDocumentoOrigen {@link Date}
	 */
	public void setFechaDocumentoOrigen(final Date fechaDocumentoOrigen) {
		this.fechaDocumentoOrigen = fechaDocumentoOrigen;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	/**
	 * @param fechaIngreso {@link Date}
	 */
	public void setFechaIngreso(final Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getRecibido() {
		return recibido;
	}

	/**
	 * @param recibido {@link Boolean}
	 */
	public void setRecibido(final Boolean recibido) {
		this.recibido = recibido;
	}

	/**
	 * @return {@link String}
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento {@link String}
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return {@link String}
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor {@link String}
	 */
	public void setAutor(final String autor) {
		this.autor = autor;
	}

	/*
	 * public String getNumeroExpedienteOPartes() { return numeroExpedienteOPartes; } public void
	 * setNumeroExpedienteOPartes(String numeroExpedienteOPartes) { this.numeroExpedienteOPartes =
	 * numeroExpedienteOPartes; }
	 */

	@Override
	public boolean equals(final Object o) {
		if (o == null) { return false; }
		if (o instanceof ExpedienteBandejaEntrada) {
			final ExpedienteBandejaEntrada e = (ExpedienteBandejaEntrada) o;
			if (numeroExpediente.equals(e.getNumeroExpediente())) { return true; }
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getTieneCmsId() {
		return tieneCmsId;
	}

	/**
	 * @param tieneCmsId {@link Boolean}
	 */
	public void setTieneCmsId(final Boolean tieneCmsId) {
		this.tieneCmsId = tieneCmsId;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @param idDocumento {@link Long}
	 */
	public void setIdDocumento(final Long idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getPlazo() {
		return plazo;
	}

	/**
	 * @param plazo {@link Date}
	 */
	public void setPlazo(final Date plazo) {
		this.plazo = plazo;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getCompletado() {
		return completado;
	}

	/**
	 * @param completado {@link Boolean}
	 */
	public void setCompletado(final Boolean completado) {
		this.completado = completado;
	}

	/**
	 * @return {@link String}
	 */
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	/**
	 * @param nombreArchivo {@link String}
	 */
	public void setNombreArchivo(final String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getCopia() {
		return copia;
	}

	/**
	 * @param copia {@link Boolean}
	 */
	public void setCopia(final Boolean copia) {
		this.copia = copia;
	}

	/**
	 * @return {@link String}
	 */
	public String getRutProveedorFactura() {
		return rutProveedorFactura;
	}

	/**
	 * @param rutProveedorFactura {@link String}
	 */
	public void setRutProveedorFactura(final String rutProveedorFactura) {
		this.rutProveedorFactura = rutProveedorFactura;
	}

	/**
	 * @return {@link String}
	 */
	public String getNumeroOrdenCompra() {
		return numeroOrdenCompra;
	}

	/**
	 * @param numeroOrdenCompra {@link String}
	 */
	public void setNumeroOrdenCompra(final String numeroOrdenCompra) {
		this.numeroOrdenCompra = numeroOrdenCompra;
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFechaArchivado() {
		return fechaArchivado;
	}

	/**
	 * @param fechaArchivado {@link Date}
	 */
	public void setFechaArchivado(final Date fechaArchivado) {
		this.fechaArchivado = fechaArchivado;
	}

	/**
	 * @return {@link String}
	 */
	public String getResumenEjecutivo() {
		return resumenEjecutivo;
	}

	/**
	 * @param resumenEjecutivo {@link String}
	 */
	public void setResumenEjecutivo(final String resumenEjecutivo) {
		this.resumenEjecutivo = resumenEjecutivo;
	}

	/**
	 * @return {@link String}
	 */
	public String getMotivoDevolucion() {
		return motivoDevolucion;
	}

	/**
	 * @param motivoDevolucion {@link String}
	 */
	public void setMotivoDevolucion(final String motivoDevolucion) {
		this.motivoDevolucion = motivoDevolucion;
	}

	/**
	 * @return {@link String}
	 */
	public String getResumenEjecutivoRegistrar() {
		return resumenEjecutivoRegistrar;
	}

	/**
	 * @param resumenEjecutivoRegistrar {@link String}
	 */
	public void setResumenEjecutivoRegistrar(final String resumenEjecutivoRegistrar) {
		this.resumenEjecutivoRegistrar = resumenEjecutivoRegistrar;
	}

	/**
	 * @return {@link String}
	 */
	public String getMotivoDevolucionRegistrar() {
		return motivoDevolucionRegistrar;
	}

	/**
	 * @param motivoDevolucionRegistrar {@link String}
	 */
	public void setMotivoDevolucionRegistrar(final String motivoDevolucionRegistrar) {
		this.motivoDevolucionRegistrar = motivoDevolucionRegistrar;
	}


	/**
	 * @return the grupoId
	 */
	public GrupoUsuario getGrupo() {
		return grupo;
	}

	/**
	 * @param grupoId the grupoId to set
	 */
	public void setGrupo(GrupoUsuario grupo) {
		this.grupo = grupo;
	}

	/**
	 * 
	 * @return
	 */
	public String getUnidadEmisor() {
		return unidadEmisor;
	}

	/**
	 * 
	 * @param unidadEmisor
	 */
	public void setUnidadEmisor(String unidadEmisor) {
		this.unidadEmisor = unidadEmisor;
	}

	/**
	 * Metodo accesador de la variable departamento emisor
	 * @return String departamentoEmisor
	 */
	public String getDepartamentoEmisor() {
		return departamentoEmisor;
	}

	/**
	 * Metodo mutador de la variable departamento emisor
	 * @param departamentoEmisor
	 */
	public void setDepartamentoEmisor(String departamentoEmisor) {
		this.departamentoEmisor = departamentoEmisor;
	}

	public Date getFechaReasignado() {
		return fechaReasignado;
	}

	public void setFechaReasignado(Date fechaReasignado) {
		this.fechaReasignado = fechaReasignado;
	}

	
}
