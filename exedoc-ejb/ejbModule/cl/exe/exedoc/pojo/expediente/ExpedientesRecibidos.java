package cl.exe.exedoc.pojo.expediente;

import java.util.ArrayList;
import java.util.List;

public class ExpedientesRecibidos {
	
	private List<Boolean> recibidos;
	private String numeroExpediente;
	
	public ExpedientesRecibidos(){
		recibidos = new ArrayList<Boolean>();
		numeroExpediente = "";
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public void setRecibido(Boolean recibido) {
		this.recibidos.add(recibido);
	}
	public List<Boolean> getRecibidos(){
		return this.recibidos;
	}
}
