package cl.exe.exedoc.pojo.expediente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;

/**
 * Clase utilizada para mostrar contenido de la bandeja de salida.
 * 
 * @author
 */
public class ExpedienteBandejaSalida extends ExpedienteBandeja implements Serializable {

	private static final long serialVersionUID = 1276743875332524141L;
	private Boolean archivado;
	private List<DatosUsuarioBandejaSalida> datosUsuariosAsociados;
	private Boolean despachado;
	private Persona destinatarioExpediente;
	private String destinatarioExpedienteString;
	private Boolean devolver;
	private List<Documento> documentosRelacionados;
	private Persona emisorExpediente;
	private Boolean expedienteNuevo;
	private Date fechaCreacionDocumento;
	private BigDecimal idUnidad;
	private Long promedioEncolado = 0L;
	private String rangoFechas;
	private String tiempoEncolado;
	private Long total = 0L;
	private Long totalUnidades = 0L;
	private String unidadOrg;
	private GrupoUsuario grupo = new GrupoUsuario();
	private Boolean recibido;
	/**
	 * Constructor.
	 */
	public ExpedienteBandejaSalida() {
		super();
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getArchivado() {
		return archivado;
	}

	/**
	 * @return {@link List} of {@link DatosUsuarioBandejaSalida}
	 */
	public List<DatosUsuarioBandejaSalida> getDatosUsuariosAsociados() {
		return datosUsuariosAsociados;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getDespachado() {
		return despachado;
	}

	/**
	 * @return {@link Persona}
	 */
	public Persona getDestinatarioExpediente() {
		return destinatarioExpediente;
	}

	/**
	 * @return {@link Boolean}
	 */
	public String getDestinatarioExpedienteString() {
		return destinatarioExpedienteString;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getDevolver() {
		return devolver;
	}

	/**
	 * @return {@link List} of {@link Documento}
	 */
	public List<Documento> getDocumentosRelacionados() {
		return documentosRelacionados;
	}

	/**
	 * @return {@link Persona}
	 */
	public Persona getEmisorExpediente() {
		return emisorExpediente;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Boolean getExpedienteNuevo() {
		return expedienteNuevo;
	}

	/**
	 * @return {@link Boolean}
	 */
	public Date getFechaCreacionDocumento() {
		return fechaCreacionDocumento;
	}

	/**
	 * @return {@link BigDecimal}
	 */
	public BigDecimal getIdUnidad() {
		return idUnidad;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getPromedioEncolado() {
		return promedioEncolado;
	}

	/**
	 * @return {@link String}
	 */
	public String getRangoFechas() {
		return rangoFechas;
	}

	/**
	 * @return {@link String}
	 */
	public String getTiempoEncolado() {
		return tiempoEncolado;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getTotal() {
		return total;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getTotalUnidades() {
		return totalUnidades;
	}

	/**
	 * @return {@link String}
	 */
	public String getUnidadOrg() {
		return unidadOrg;
	}

	/**
	 * @param archivado {@link Boolean}
	 */
	public void setArchivado(final Boolean archivado) {
		this.archivado = archivado;
	}

	/**
	 * @param datosUsuariosAsociados {@link List} of {@link DatosUsuarioBandejaSalida}
	 */
	public void setDatosUsuariosAsociados(final List<DatosUsuarioBandejaSalida> datosUsuariosAsociados) {
		this.datosUsuariosAsociados = datosUsuariosAsociados;
	}

	/**
	 * @param despachado {@link Boolean}
	 */
	public void setDespachado(final Boolean despachado) {
		this.despachado = despachado;
	}

	/**
	 * @param destintarioExpediente {@link Persona}
	 */
	public void setDestinatarioExpediente(final Persona destintarioExpediente) {
		this.destinatarioExpediente = destintarioExpediente;
	}

	/**
	 * @param destinatarioExpedienteString {@link Boolean}
	 */
	public void setDestinatarioExpedienteString(final String destinatarioExpedienteString) {
		this.destinatarioExpedienteString = destinatarioExpedienteString;
	}

	/**
	 * @param devolver {@link Boolean}
	 */
	public void setDevolver(final Boolean devolver) {
		this.devolver = devolver;
	}

	/**
	 * @param documentosRelacionados {@link List} of {@link Documento}
	 */
	public void setDocumentosRelacionados(final List<Documento> documentosRelacionados) {
		this.documentosRelacionados = documentosRelacionados;
	}

	/**
	 * @param emisorExpediente {@link Persona}
	 */
	public void setEmisorExpediente(final Persona emisorExpediente) {
		this.emisorExpediente = emisorExpediente;
	}

	/**
	 * @param expedienteNuevo {@link Boolean}
	 */
	public void setExpedienteNuevo(final Boolean expedienteNuevo) {
		this.expedienteNuevo = expedienteNuevo;
	}

	/**
	 * @param fechaCreacionDocumento {@link Boolean}
	 */
	public void setFechaCreacionDocumento(final Date fechaCreacionDocumento) {
		this.fechaCreacionDocumento = fechaCreacionDocumento;
	}

	/**
	 * @param idUnidad {@link BigDecimal}
	 */
	public void setIdUnidad(final BigDecimal idUnidad) {
		this.idUnidad = idUnidad;
	}

	/**
	 * @param promedioEncolado {@link Long}
	 */
	public void setPromedioEncolado(final Long promedioEncolado) {
		this.promedioEncolado = promedioEncolado;
	}

	/**
	 * @param rangoFechas {@link String}
	 */
	public void setRangoFechas(final String rangoFechas) {
		this.rangoFechas = rangoFechas;
	}

	/**
	 * @param tiempoEncolado String
	 */
	public void setTiempoEncolado(final String tiempoEncolado) {
		this.tiempoEncolado = tiempoEncolado;
	}

	/**
	 * @param total {@link Long}
	 */
	public void setTotal(final Long total) {
		this.total = total;
	}

	/**
	 * @param totalUnidades {@link Long}
	 */
	public void setTotalUnidades(final Long totalUnidades) {
		this.totalUnidades = totalUnidades;
	}

	/**
	 * @param unidadOrg {@link String}
	 */
	public void setUnidadOrg(final String unidadOrg) {
		this.unidadOrg = unidadOrg;
	}

	/**
	 * @return the grupoId
	 */
	public GrupoUsuario getGrupo() {
		return grupo;
	}

	/**
	 * @param grupoId the grupoId to set
	 */
	public void setGrupoId(GrupoUsuario grupo) {
		this.grupo = grupo;
	}

	public void setRecibido(Boolean recibido) {
		this.recibido = recibido;
	}

	public Boolean getRecibido() {
		return recibido;
	}
	
	

}
