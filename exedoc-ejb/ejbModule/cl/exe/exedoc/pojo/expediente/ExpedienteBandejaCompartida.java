package cl.exe.exedoc.pojo.expediente;

import java.io.Serializable;
import java.util.List;

import cl.exe.exedoc.entity.Expediente;

public class ExpedienteBandejaCompartida extends ExpedienteBandeja implements Serializable {

	private static final long serialVersionUID = -1335792627231029638L;

	private String remitente;
	
	private Integer version;

	private List<Expediente> expedientesAsociadosADocumento;
	
	private Boolean copia;
	
	private Boolean reservado;
	
	private String mensajeAlerta;
	
	private String nombreGrupo;
	
	private String nombreFiscalia;
			
	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public List<Expediente> getExpedientesAsociadosADocumento() {
		return expedientesAsociadosADocumento;
	}

	public void setExpedientesAsociadosADocumento(
			List<Expediente> expedientesAsociadosADocumento) {
		this.expedientesAsociadosADocumento = expedientesAsociadosADocumento;
	}

	public Boolean getCopia() {
		return copia;
	}

	public void setCopia(Boolean copia) {
		this.copia = copia;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getReservado() {
		return reservado;
	}

	public void setReservado(Boolean reservado) {
		this.reservado = reservado;
	}

	public String getMensajeAlerta() {
		return mensajeAlerta;
	}

	public void setMensajeAlerta(String mensajeAlerta) {
		this.mensajeAlerta = mensajeAlerta;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreFiscalia(String nombreFiscalia) {
		this.nombreFiscalia = nombreFiscalia;
	}

	public String getNombreFiscalia() {
		return nombreFiscalia;
	}
}
