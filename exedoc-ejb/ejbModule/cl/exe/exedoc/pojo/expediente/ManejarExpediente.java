package cl.exe.exedoc.pojo.expediente;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPathExpressionException;

import oracle.jdbc.OracleTypes;

import org.jboss.cache.CacheException;
import org.jboss.cache.aop.PojoCache;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.email.Email;
import cl.exe.email.message.Mail;
import cl.exe.email.utils.configuracion.AbstractConfiguracion;
import cl.exe.exedoc.dao.DaoFactory;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.repositorio.FoliadorLocal;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.BandejaCompartidaData;
import cl.exe.exedoc.session.BandejaEntradaData;
import cl.exe.exedoc.session.Customizacion;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.utils.Destinatario;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.exedoc.util.DestinatariosFirmaDocumentoMail;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.TextUtil;
import cl.exe.mail.Configuracion;

@Stateless
@Name("ManejarExpediente")
public class ManejarExpediente implements ManejarExpedienteInterface {

	private static final String DD_MM_YYYY_HH_MM = "dd/MM/yyyy HH:mm";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	private Persona usuario;

	@EJB
	private ManejarDocumentoInterface manejarDocumento;

	@EJB
	private FoliadorLocal foliador;
	
	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private AdministradorAlertas administradorAlertas;
	
	@EJB
	private Customizacion customizacion;
	
//	@EJB
//	private ManejarExpedienteInterface me;
	
//	@EJB
//	private AdministradorPersona pe;
	
	private static Object LOCK = new Object();

	/**
	 * Constructor.
	 */
	public ManejarExpediente() {
		super();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public void crearExpediente(final Expediente expediente) {
		if (expediente != null) {
			String numeroExpediente;
			synchronized (LOCK) {
				numeroExpediente = foliador.generarFolioExpediente();
			}
			expediente.setNumeroExpediente(numeroExpediente);
			em.persist(expediente);
		}
	}

	@Override
	public void modificarExpediente(final Expediente expediente) {
		if (expediente != null) {
			em.merge(expediente);
		}
	}

	@Override
	public void eliminarExpediente(final Long idExpediente) {
		final Expediente expediente = em.find(Expediente.class, idExpediente);
		if (expediente != null) {
			if ((expediente.getObservaciones() != null) && (expediente.getObservaciones().size() > 0)) {
		        for (Observacion o : expediente.getObservaciones()) {
		          o.setObservacionArchivo(null);
		        }
		      }
		      this.em.merge(expediente);
		      //this.em.flush();
		      this.em.remove(expediente);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminarExpedientes(final Expediente expediente, final boolean eliminarPadre) {
		final List<Expediente> expedientes = this.buscarExpedientesHijos(expediente);

		for (Expediente e : expedientes) {
			eliminarExpediente(e.getId());
		}
		if (eliminarPadre) {
			eliminarExpediente(expediente.getId());
		}
	}

	@Override
	public Expediente buscarExpediente(final Long idExpediente) {
		return em.find(Expediente.class, idExpediente);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Expediente> buscarExpediente(final String numeroExpediente) {
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT e FROM Expediente e ");
		jpQL.append("WHERE e.numeroExpediente = ? ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, numeroExpediente);
		return (List<Expediente>) query.getResultList();
	}

	/**
	 * Metodo que retorna una lista de expediente.
	 * 
	 * @param expedientePadre {@link Expediente}
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Expediente> buscarExpedientesHijos(final Expediente expedientePadre) {
		final StringBuilder jpQL = new StringBuilder(10);
		jpQL.append("SELECT e FROM Expediente e ");
		jpQL.append("WHERE e.versionPadre = ? ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, expedientePadre);
		return (List<Expediente>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
//	@Override
//	public List<Expediente> buscarExpediente(final Expediente expedientePadre, final boolean desierto) {
//		final StringBuilder jpQL = new StringBuilder();
//		jpQL.append("SELECT e FROM Expediente e ");
//		jpQL.append("WHERE e.versionPadre = ? ");
//		final Query query = em.createQuery(jpQL.toString());
//		query.setParameter(1, expedientePadre);
//		return (List<Expediente>) query.getResultList();
//	}
	@Override
	public boolean existeNumeroExpediente(final String numeroExpediente) {
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT e FROM Expediente e ");
		jpQL.append("WHERE e.numeroExpediente = ? ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, numeroExpediente);
		if (query.getResultList() != null) {
			if (query.getResultList().size() != 0) { return true; }
		}
		return false;
	}

	/*
	 * **************************************************************************
	 * **
	 */
	// Bandeja Entrada
	public List<ExpedienteBandejaEntrada> listarExpedienteBandejaEntradaUsuario(final Persona persona) {
		//List<ExpedienteBandejaEntrada> lista = buscarBandejaEntradaDesdeCache(persona);
		
		//if (lista == null) {
			
			/*
			final StringBuilder jpQL = new StringBuilder(
					"SELECT e.id, e.numeroExpediente, e.fechaIngreso, (case when e.acuseRecibois null then false else true end), e.version, e.copia, CONCAT(CONCAT(dest.nombres,' '), dest.apellidoPaterno), ");
			jpQL.append(" CONCAT(CONCAT(dest_hist.nombres,' '),dest_hist.apellidoPaterno) as nombreapellido ");
			jpQL.append(" FROM Expediente e left join e.destinatario dest  ");
			jpQL.append(" left join e.destinatarioHistorico dest_hist  WHERE ");
			jpQL.append(" e.archivado is null and ");
			jpQL.append(" ((e.destinatario.id = " + persona.getId()
					+ " and e.fechaDespacho is not null and e.fechaAcuseRecibo is null and eliminado <> true )");
			jpQL.append(" or (e.emisor.id = " + persona.getId()
					+ " and e.fechaDespacho is null and e.fechaAcuseRecibo is not null)) ");
			jpQL.append(" and e.copia = false");
			jpQL.append(" order by e.fechaIngreso");
			final Query query = em.createQuery(jpQL.toString());
			final List<Object[]> expedientes = query.getResultList();
			fin = System.currentTimeMillis();
			System.out.println("tiempo 2 "+(fin-ini));
			
			lista = this.armaBandejaEntrada(expedientes, false);*/
			
		List<ExpedienteBandejaEntrada> lista = this.armaBandejaEntradaProcedimiento(false);
			
			this.almacenarBandejaEntradaEnCache(persona, lista);
		//}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ExpedienteBandejaEntrada> listarExpedienteBandejaEntradaCopiaUsuario(final Persona persona) {
		List<ExpedienteBandejaEntrada> lista = this.buscarBandejaEntradaDesdeCache(persona);
		if (lista == null) {
			final StringBuilder jpQL = new StringBuilder();
			jpQL.append("SELECT e.id, e.numeroExpediente, e.fechaIngreso, (case when e.fechaAcuseRecibo is null then false else true end), ");
			jpQL.append(" e.version, e.copia, CONCAT(CONCAT(dest.nombres,' '), dest.apellidoPaterno), ");
			jpQL.append(" CONCAT(CONCAT(dest_hist.nombres,' '),dest_hist.apellidoPaterno) as nombre_apellido ");
			jpQL.append("FROM Expediente e ");
			jpQL.append("	left join e.destinatario dest ");
			jpQL.append("	left join e.destinatarioHistorico dest_hist ");
			jpQL.append("WHERE e.archivado is null ");
			jpQL.append("	and ((e.destinatario.id = :idPersona ");
			jpQL.append("	and e.fechaDespacho is not null ");
			jpQL.append("	and e.fechaAcuseRecibo is null) ");
			jpQL.append("	or (e.emisor.id = :idPersona2 ");
			jpQL.append("	and e.fechaDespacho is null ");
			jpQL.append("	and e.fechaAcuseRecibo is not null)) ");
			jpQL.append("	and e.copia = true and e.grupo.id is null ");
			jpQL.append("	and e.cancelado is null ");
			jpQL.append("   and e.reasignado is null ");
			jpQL.append("order by e.fechaIngreso ");
			final Query query = em.createQuery(jpQL.toString());
			query.setParameter("idPersona", persona.getId());
			query.setParameter("idPersona2", persona.getId());
			final List<Object[]> expedientes = query.getResultList();
			log.info("lista size {0}", expedientes.size());
			lista = this.armaBandejaEntrada(expedientes, false);
			this.almacenarBandejaEntradaEnCache(persona, lista);
		}
		return lista;
	}

	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaEntrada> armaBandejaEntrada(final List<Object[]> expedientes,
			final boolean soloVencidos) {
		final List<ExpedienteBandejaEntrada> ebeList = new ArrayList<ExpedienteBandejaEntrada>();

		final Query qremi1 = em
				.createQuery("select e.emisorHistorico.nombres || ' ' || e.emisorHistorico.apellidoPaterno from Expediente e where e.id = ?");
		final Query qremi2 = em
				.createQuery("select e.emisor.nombres || ' ' || e.emisor.apellidoPaterno from Expediente e where e.id = ?");
//		final Query qremi3 = em
//				.createQuery("select e.grupo.nombre from Expediente e where e.id = ?");

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo, doc.id ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		// final StringBuilder sqdoc = new
		// StringBuilder("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		// sqdoc.append("doc.reservado, doc.materia, doc.tipoDocumento.descripcion, doc.emisor, ");
		// sqdoc.append("doc.plazo, doc.formatoDocumento.descripcion, doc.tipoDocumento.id, doc.alerta.id ");
		// sqdoc.append("from Documento doc ");
		// sqdoc.append("where doc.id = ? ");

		final StringBuilder sqdoc = new StringBuilder();
		sqdoc.append("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		sqdoc.append("	doc.reservado, doc.materia, td.descripcion, doc.emisor, ");
		sqdoc.append("	doc.plazo, fd.descripcion, td.id, a.id ");
		sqdoc.append("from Documento doc ");
		sqdoc.append("	left join doc.tipoDocumento td ");
		sqdoc.append("	left join doc.formatoDocumento fd ");
		sqdoc.append("	left join doc.alerta a ");
		sqdoc.append("where doc.id = ? ");
		final Query qryDoc = em.createQuery(sqdoc.toString());

		final String sqdest = "select de.destinatario from ListaPersonasDocumento de where de.documento.id = ?";
		final Query qDest = em.createQuery(sqdest);

		for (Object[] e : expedientes) {
			//log.info("id Expediente {0}", (Long) e[0]);

			final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
			ebe.setId((Long) e[0]);
			ebe.setNumeroExpediente((String) e[1]);
			ebe.setFechaIngreso((Date) e[2]);
			final Boolean recibido = (Boolean) e[3];
			ebe.setRecibido(recibido);
			ebe.setVersion((Integer) e[4]);
			final Boolean copia = (Boolean) e[5];
			ebe.setCopia(copia);
			final String destinatarioExp = (String) e[6];
			final String destinatarioHistExp = (String) e[7];

			//log.info("id Expediente : 1", (Long) e[0]);

			/*
			 * if(archivado){ ebe.setFechaArchivado((Date) e[5]); }
			 */

			if (recibido) {
				qremi1.setParameter(1, ebe.getId());
				ebe.setRemitente((String) qremi1.getSingleResult());
			} else {
				qremi2.setParameter(1, ebe.getId());
				ebe.setRemitente((String) qremi2.getSingleResult());
			}

			qryDocs.setParameter(1, ebe.getId());
			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);
			//log.info("qryDocs.setParameter : {0}", (Long) e[0]);

			final List<Object[]> qdocs = qryDocs.getResultList();
			//log.info("qdocs {0}", qdocs.size());
			boolean respuesta = false;
			Long docId = null;
			Long docIdOriginal = null;
			boolean completado = true;
			final Date hoy = new Date();
			boolean encontrado = false;

			for (Object[] d : qdocs) {
				//log.info("segundo for {0}", (Integer) d[0]);

				final Integer tipoDocumento = (Integer) d[0];
				final Boolean c = (Boolean) d[2];
				final Date plazo = (Date) d[3];
				if (completado && c != null && !c && plazo != null) {
					if (plazo.before(hoy)) {
						completado = false;
					}
				}
				if (!encontrado) {
					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
						docId = (Long) d[1];
						respuesta = true;
						encontrado = true;
					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
						docIdOriginal = (Long) d[1];
						encontrado = true;
					}
				}
				//log.info("fin segundo for {0}", (Integer) d[0]);
			}

			if (!respuesta) {
				docId = docIdOriginal;
			}

			ebe.setCompletado(completado);

			qryDoc.setParameter(1, docId);
			try {
				final Object[] qdoc = (Object[]) qryDoc.getSingleResult();

				ebe.setNumeroDocumento((String) qdoc[0]);
				ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
				if (qdoc[2] != null) {
					ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
				}
				ebe.setTipoDocumento((String) qdoc[4]);
				ebe.setEmisor((String) qdoc[5]);
				ebe.setPlazo((Date) qdoc[6]);
				ebe.setFormatoDocumento((String) qdoc[7]);
				final Integer idTipoDoc = (Integer) qdoc[8];

				try {
					final Long idAlerta = (Long) qdoc[9];
					ebe.setMensajeAlerta(administradorAlertas.validarPlazo(idAlerta, ebe.getPlazo()));
				} catch (NullPointerException ex) {
					//log.error("Error: ", e);
				}

				final StringBuilder sb = new StringBuilder();

				if (
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO) ||
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_VACACIONES) ||
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADQUISICION)
				// ||
				idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
					if (destinatarioExp != null) {
						sb.append(destinatarioExp);
						sb.append(" ");
					} else {
						if (destinatarioHistExp != null) {
							sb.append(destinatarioHistExp);
							sb.append(" ");
						}
					}
				} else {
					qDest.setParameter(1, docId);
					final List<String> sDestinatarios = qDest.getResultList();
					for (String dd : sDestinatarios) {
						sb.append(dd);
						sb.append(" ");
					}
				}
				if (sb.length() != 0) {
					ebe.setDestinatario(sb.toString());
				}

				if (!soloVencidos) {
					ebeList.add(ebe);
				} else if (!completado) {
					ebeList.add(ebe);
				}

			} catch (NoResultException ex) {
				log.error("no encontro resultados...", ex);
			}
			//log.info("fin for Expediente {0}", (Long) e[0]);
		}

		return ebeList;
	}

	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaEntrada> armaBandejaEntradaMinisterio(final List<Object[]> expedientes,
			final boolean soloVencidos) {
		final List<ExpedienteBandejaEntrada> ebeList = new ArrayList<ExpedienteBandejaEntrada>();

		final Query qremi1 = em
				.createQuery("select e.emisorHistorico.nombres || ' ' || e.emisorHistorico.apellidoPaterno from Expediente e where e.id = ?");
		final Query qremi2 = em
				.createQuery("select e.emisor.nombres || ' ' || e.emisor.apellidoPaterno from Expediente e where e.id = ?");

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		final StringBuilder sqdoc = new StringBuilder("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		sqdoc.append("doc.reservado, doc.materia, doc.tipoDocumento.descripcion, doc.emisor, ");
		sqdoc.append("doc.plazo, doc.formatoDocumento.descripcion, doc.tipoDocumento.id ");
		sqdoc.append("from Documento doc ");
		sqdoc.append("where doc.id = ? ");
		final Query qryDoc = em.createQuery(sqdoc.toString());

		final String sqdest = "select de.destinatario from ListaPersonasDocumento de where de.documento.id = ?";
		final Query qDest = em.createQuery(sqdest);

		// String sqObsEjec=
		// "select o from Observacion o where o.expediente.id = ? and o.tipoObservacion.id= "
		// + TipoObservacion.OBSERVACION_EJECUTIVA + " ";
		// String sqObsEjec=
		// "select o from Observacion o where o.expediente.id = ? ";
		// Query qObsEjec = em.createQuery(sqObsEjec);

		// String sqObsDev=
		// "select o from Observacion o where o.expediente.id = ? and o.tipoObservacion.id= "
		// + TipoObservacion.OBSERVACION_DEVOLUCION + " ";
		// String sqObsDev=
		// "select o from Observacion o where o.expediente.id = ? ";
		// Query qObsDev = em.createQuery(sqObsDev);

		for (Object[] e : expedientes) {

			final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
			ebe.setId(((BigInteger) e[0]).longValue());
			ebe.setNumeroExpediente((String) e[1]);
			ebe.setFechaIngreso((Date) e[2]);
			final Boolean recibido = (Boolean) e[3];
			ebe.setRecibido(recibido);
			ebe.setVersion((Integer) e[4]);
			final Boolean copia = (Boolean) e[5];
			ebe.setCopia(copia);
			final String destinatarioExp = (String) e[6];
			final String destinatarioHistExp = (String) e[7];
			final String observacionEjec = (String) e[8];
			final String observacionDev = (String) e[9];

			/*
			 * if(archivado){ ebe.setFechaArchivado((Date) e[5]); }
			 */

			if (recibido) {
				qremi1.setParameter(1, ebe.getId());
				ebe.setRemitente((String) qremi1.getSingleResult());
			} else {
				qremi2.setParameter(1, ebe.getId());
				ebe.setRemitente((String) qremi2.getSingleResult());
			}

			qryDocs.setParameter(1, ebe.getId());
			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);

			final List<Object[]> qdocs = qryDocs.getResultList();

			boolean respuesta = false;
			Long docId = null;
			Long docIdOriginal = null;
			boolean completado = true;
			final Date hoy = new Date();
			boolean encontrado = false;

			for (Object[] d : qdocs) {

				final Integer tipoDocumento = (Integer) d[0];
				final Boolean c = (Boolean) d[2];
				final Date plazo = (Date) d[3];
				if (completado && c != null && !c && plazo != null) {
					if (plazo.before(hoy)) {
						completado = false;
					}
				}
				if (!encontrado) {
					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
						docId = (Long) d[1];
						respuesta = true;
						encontrado = true;
					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
						docIdOriginal = (Long) d[1];
						encontrado = true;
					}
				}
			}

			if (!respuesta) {
				docId = docIdOriginal;
			}

			ebe.setCompletado(completado);

			qryDoc.setParameter(1, docId);
			final Object[] qdoc = (Object[]) qryDoc.getSingleResult();

			ebe.setNumeroDocumento((String) qdoc[0]);
			ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
			if (qdoc[2] != null) {
				ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
			}
			ebe.setTipoDocumento((String) qdoc[4]);
			ebe.setEmisor((String) qdoc[5]);
			ebe.setPlazo((Date) qdoc[6]);
			ebe.setFormatoDocumento((String) qdoc[7]);
			final Integer idTipoDoc = (Integer) qdoc[8];

			final StringBuilder sb = new StringBuilder();

			if (
			// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO) ||
			// idTipoDoc.equals(TipoDocumento.SOLICITUD_VACACIONES) ||
			// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADQUISICION)
			// ||
			idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
				if (destinatarioExp != null) {
					sb.append(destinatarioExp);
					sb.append(" ");
				} else {
					if (destinatarioHistExp != null) {
						sb.append(destinatarioHistExp);
						sb.append(" ");
					}
				}
			} else {
				qDest.setParameter(1, docId);
				final List<String> sDestinatarios = qDest.getResultList();
				for (String dd : sDestinatarios) {
					sb.append(dd);
					sb.append(" ");
				}
			}
			if (sb.length() != 0) {
				ebe.setDestinatario(sb.toString());
			}

			// Observacion o = null;
			// qObsEjec.setParameter(1, docId);
			// List<Observacion> sObservaciones = qObsEjec.getResultList();
			// if(sObservaciones!= null && !sObservaciones.isEmpty()){
			// o= sObservaciones.get(0);
			// if(o!= null && o.getObservacion()!= null){
			// ebe.setResumenEjecutivo(o.getObservacion());
			// }
			// }
			ebe.setResumenEjecutivo(observacionEjec);
			ebe.setMotivoDevolucion(observacionDev);
			ebe.setResumenEjecutivoRegistrar(observacionEjec);
			ebe.setMotivoDevolucionRegistrar(observacionDev);

			// qObsDev.setParameter(1, docId);
			// sObservaciones = qObsDev.getResultList();
			// if(sObservaciones!= null && !sObservaciones.isEmpty()){
			// o= sObservaciones.get(0);
			// if(o!= null && o.getObservacion()!= null){
			// ebe.setMotivoDevolucion(o.getObservacion());
			// }
			// }

			ebe.setIdDocumento(docId);

			if (!soloVencidos) {
				ebeList.add(ebe);
			} else if (!completado) {
				ebeList.add(ebe);
			}

		}

		return ebeList;
	}

	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaEntrada> armaBandejaEntradaFirmaVDI(final List<Object[]> expedientes,
			final boolean soloVencidos, final Long idPersona) {
		final List<ExpedienteBandejaEntrada> ebeList = new ArrayList<ExpedienteBandejaEntrada>();

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		final StringBuilder sqdoc = new StringBuilder("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		sqdoc.append("doc.reservado, doc.materia, doc.tipoDocumento.descripcion, doc.emisor, ");
		sqdoc.append("doc.formatoDocumento.descripcion, doc.tipoDocumento.id, doc.id ");
		sqdoc.append("from Documento doc left outer join doc.visacionesEstructuradas ve ");
		sqdoc.append("left outer join doc.firmasEstructuradas fe ");
		sqdoc.append("where doc.id = ? ");
		sqdoc.append("and doc.tipoDocumento.id in (6,9,22,23)");
		sqdoc.append("and ve is null and fe is not null ");
		sqdoc.append("and fe.persona.id = " + idPersona);
		final Query qryDoc = em.createQuery(sqdoc.toString());

		final String sqdest = "select de.destinatario from ListaPersonasDocumento de where de.documento.id = ?";
		final Query qDest = em.createQuery(sqdest);

		for (Object[] e : expedientes) {

			final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
			ebe.setId(((BigInteger) e[0]).longValue());
			ebe.setNumeroExpediente((String) e[1]);
			ebe.setFechaIngreso((Date) e[2]);
			final Boolean recibido = (Boolean) e[3];
			ebe.setRecibido(recibido);
			ebe.setVersion((Integer) e[4]);
			final Boolean copia = (Boolean) e[5];
			ebe.setCopia(copia);
			final String destinatarioExp = (String) e[6];
			final String destinatarioHistExp = (String) e[7];

			/*
			 * if(archivado){ ebe.setFechaArchivado((Date) e[5]); }
			 */

			qryDocs.setParameter(1, ebe.getId());
			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);

			final List<Object[]> qdocs = qryDocs.getResultList();

			boolean respuesta = false;
			Long docId = null;
			Long docIdOriginal = null;
			boolean completado = true;
			final Date hoy = new Date();
			boolean encontrado = false;

			for (Object[] d : qdocs) {

				final Integer tipoDocumento = (Integer) d[0];
				final Boolean c = (Boolean) d[2];
				final Date plazo = (Date) d[3];
				if (completado && c != null && !c && plazo != null) {
					if (plazo.before(hoy)) {
						completado = false;
					}
				}
				if (!encontrado) {
					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
						docId = (Long) d[1];
						respuesta = true;
						encontrado = true;
					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
						docIdOriginal = (Long) d[1];
						encontrado = true;
					}
				}
			}

			if (!respuesta) {
				docId = docIdOriginal;
			}

			ebe.setCompletado(completado);

			qryDoc.setParameter(1, docId);

			try {
				final Object[] qdoc = (Object[]) qryDoc.getSingleResult();
				ebe.setNumeroDocumento((String) qdoc[0]);
				ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
				if (qdoc[2] != null) {
					ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
				}
				ebe.setTipoDocumento((String) qdoc[4]);
				ebe.setEmisor((String) qdoc[5]);
				ebe.setFormatoDocumento((String) qdoc[6]);
				final Integer idTipoDoc = (Integer) qdoc[7];
				ebe.setIdDocumento((Long) qdoc[8]);

				final StringBuilder sb = new StringBuilder();

				if (
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO) ||
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_VACACIONES) ||
				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADQUISICION)
				// ||
				idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
					if (destinatarioExp != null) {
						sb.append(destinatarioExp);
						sb.append(" ");
					} else {
						if (destinatarioHistExp != null) {
							sb.append(destinatarioHistExp);
							sb.append(" ");
						}
					}
				} else {
					qDest.setParameter(1, docId);
					final List<String> sDestinatarios = qDest.getResultList();
					for (String dd : sDestinatarios) {
						sb.append(dd);
						sb.append(" ");
					}
				}
				if (sb.length() != 0) {
					ebe.setDestinatario(sb.toString());
				}

				if (!soloVencidos) {
					ebeList.add(ebe);
				} else if (!completado) {
					ebeList.add(ebe);
				}
			} catch (NoResultException nre) {

			}
		}

		return ebeList;
	}

	private static final int CACHE_SIZE = 100;

	@In(required = false)
	@Out(required = false, scope = ScopeType.SESSION)
	private PojoCache pojoCache;

	@In(required = false)
	private BandejaEntradaData data;

	@In(required = false)
	private BandejaEntradaData dataCopia;

	@In(required = false)
	private BandejaCompartidaData dataCompartida;

	/**
	 * buscar Bandeja Entrada Desde Cache.
	 * 
	 * @param usuario {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaEntrada> buscarBandejaEntradaDesdeCache(final Persona usuario) {

		try {
			final String idUsuario = usuario.getId().toString();
			log.info("buscando bandeja entrada, id usuario = #0", idUsuario);
			final List<ExpedienteBandejaEntrada> list = (List<ExpedienteBandejaEntrada>) pojoCache.get(
					"bandejaEntrada", idUsuario);
			if (list != null) {
				log.info("bandeja de entrada recuperado desde cache, usuario #0", idUsuario);
			}
			return list;
		} catch (CacheException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException e) {
			return null;
		}

	}

	/**
	 * almacenar Bandeja Entrada En Cache.
	 * 
	 * @param usuario {@link Persona}.
	 * @param lista {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	private void almacenarBandejaEntradaEnCache(final Persona usuario, final List<ExpedienteBandejaEntrada> lista) {
		try {
			if (lista.size() > CACHE_SIZE) {
				final String idUsuario = usuario.getId().toString();
				log.info("bandeja de entrada almacenada en cache, usuario: #0", idUsuario);
				pojoCache.put("bandejaEntrada", idUsuario, lista);
			}
		} catch (CacheException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException ex) {
		}

	}

	@Override
	public void borrarExpedienteDeCache(final Expediente expediente) {
		if (data != null) {
			log.debug("borra expediente de data vista bandeja entrada, id = #0", expediente.getId());
			data.eliminarExpediente(expediente.getId());
		}
		if (dataCopia != null) {
			log.debug("borra expediente de data vista bandeja entrada copia, id = #0", expediente.getId());
			dataCopia.eliminarExpediente(expediente.getId());
		}
		if (dataCompartida != null) {
			log.debug("borra expediente de data vista bandeja compartida , id = #0", expediente.getId());
			dataCompartida.eliminarExpediente(expediente.getId());
		}
	}

	@Override
	public void archivarExpediente(final Expediente expediente) {
		Date ahora = new Date();
		expediente.setArchivado(ahora);
		// TODO might be useful
//		Observacion obs = new Observacion();
//		obs.setAutor(usuario);
//		obs.setFecha(ahora);
//		obs.setObservacion("Archivado por " + usuario.getNombreApellido());
//		obs.setExpediente(expediente);
//		em.merge(obs);
		this.modificarExpediente(expediente);

		this.borrarExpedienteDeCache(expediente);
	}

	@Override
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelarExpediente(final Expediente expediente) {
		if (expediente != null && expediente.getId() != null) {
			final List<Expediente> expedientes = this.buscarExpedientesHijos(expediente);
			if (expedientes != null) {
				for (Expediente ex : expedientes) {
					//ex.setCancelado(new Date());
					//this.modificarExpediente(ex);
					this.borrarExpedienteDeCache(ex);
				}
			}
			this.eliminarExpedientes(expediente, false);
			expediente.setCancelado(new Date());
			this.modificarExpediente(expediente);
		}
	}

	/*
	 * **************************************************************************
	 * **
	 */

	// Bandeja Entrada
	@SuppressWarnings("unchecked")
	@Override
	public List<ExpedienteBandejaEntrada> listarExpedienteRecibidosPorUsuario(final Persona persona) {

		// Expedientes re100 recibidos.
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT e FROM Expediente e ");
		jpQL.append("WHERE e.destinatario = ? ");
		jpQL.append("	and e.fechaDespacho is not null ");
		jpQL.append("	and e.fechaAcuseRecibo is null ");
		jpQL.append("	and e.archivado is null");

		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, persona);

		final List<Expediente> expedientes = query.getResultList();
		final List<ExpedienteBandejaEntrada> expedientesBandeja = this.armaEntrada(expedientes, false);

		// Expedientes con acuse de recibo, se cambia el emisor.
		final StringBuilder jpQL1 = new StringBuilder();
		jpQL1.append("SELECT e FROM Expediente e WHERE ");
		jpQL1.append(" e.emisor = ? ");
		jpQL1.append(" and e.fechaDespacho is null ");
		jpQL1.append(" and e.fechaAcuseRecibo is not null ");
		jpQL1.append(" and e.fechaAcuseRecibo is not null ");
		jpQL1.append(" and e.archivado is null");
		final Query query1 = em.createQuery(jpQL1.toString());
		query1.setParameter(1, persona);

		final List<Expediente> expedientes1 = query1.getResultList();
		expedientesBandeja.addAll(this.armaEntrada(expedientes1, true));

		return expedientesBandeja;
	}

	/**
	 * Metodo que arma la lista de expedientes para la bandeja de entrada.
	 * 
	 * @param expedientes {@link List} of {@link Expediente}
	 * @param recibido {@link Boolean}
	 * @return {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	private List<ExpedienteBandejaEntrada> armaEntrada(final List<Expediente> expedientes, final boolean recibido) {

		final List<ExpedienteBandejaEntrada> ebeList = new ArrayList<ExpedienteBandejaEntrada>();
		for (Expediente e : expedientes) {
			final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
			ebe.setId(e.getId());
			ebe.setRecibido(recibido);
			ebe.setNumeroExpediente(e.getNumeroExpediente());
			ebe.setFechaIngreso(e.getFechaIngreso());
			ebe.setCopia(e.getCopia());

			if (!recibido) {
				ebe.setRemitente(e.getEmisor().getNombres() + " " + e.getEmisor().getApellidoPaterno());
			} else {
				ebe.setRemitente(e.getEmisorHistorico().getNombres() + " "
						+ e.getEmisorHistorico().getApellidoPaterno());
			}
			final List<DocumentoExpediente> docs = e.getDocumentos();
			Collections.sort(docs, new Comparator<DocumentoExpediente>() {
				public int compare(final DocumentoExpediente o1, final DocumentoExpediente o2) {
					return o1.getDocumento().getFechaCreacion().compareTo(o2.getDocumento().getFechaCreacion());
				}
			});
			Documento doc = null;
			boolean respuesta = false;
			for (int i = docs.size() - 1; i > 0; i--) {
				if (docs.get(i).getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
					doc = docs.get(i).getDocumento();
					respuesta = true;
					break;
				}
			}
			if (!respuesta) {
				for (int i = 0; i < docs.size(); i++) {
					if (docs.get(i).getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
						doc = docs.get(i).getDocumento();
						break;
					}
				}
			}

			ebe.setNumeroDocumento(doc.getNumeroDocumento());
			ebe.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
			if (doc.getReservado() != null && doc.getReservado()) {
				ebe.setMateria("Materia Confidencial");
			} else {
				ebe.setMateria(doc.getMateria());
			}
			ebe.setTipoDocumento(doc.getTipoDocumento().getDescripcion());

			ebe.setEmisor(doc.getEmisor());

			final StringBuilder sb = new StringBuilder();
			for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
				sb.append(dd.getDestinatario() + " ");
			}
			ebe.setDestinatario(sb.toString());
			ebe.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			ebe.setPlazo(doc.getPlazo());

			boolean completado = true;
			for (DocumentoExpediente de : docs) {
				final Documento docTmp = de.getDocumento();
				if (docTmp.getCompletado() != null && !docTmp.getCompletado()) {
					if (docTmp.getPlazo() != null && docTmp.getPlazo().before(new Date())) {
						completado = false;
						break;
					}
				}
			}
			ebe.setCompletado(completado);
			ebe.setVersion(e.getVersion());
			ebeList.add(ebe);

		}
		return ebeList;
	}

	// Bandeja Salida
	@SuppressWarnings("unchecked")
	@Override
	public List<ExpedienteBandejaSalida> listarExpedientesNoDespachadosPorUsuario(final Persona persona) {
		final StringBuilder jpQL = new StringBuilder(10);
		jpQL.append("SELECT e ");
		jpQL.append("FROM Expediente e ");
		jpQL.append("WHERE e.emisor = ? ");
		jpQL.append("	and e.destinatario is null ");
		jpQL.append("	and e.cancelado is null ");
		jpQL.append("	and e.fechaAcuseRecibo is null ");
		jpQL.append("	and e.fechaDespacho is null ");
		jpQL.append("	and e.fechaDespachoHistorico is null ");
		jpQL.append("	and e.archivado is null ");
		jpQL.append("	and e.grupo.id is null");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, persona);

		return this.armaSalida(query.getResultList(), false, false);
	}

	/**
	 * Metodo que arma bandeja de salida.
	 * 
	 * @param expedientes {@link List} of {@link Expediente}
	 * @param recibido {@link Boolean}
	 * @param despachado {@link Boolean}
	 * @return {@link List} of {@link ExpedienteBandejaSalida}
	 */
	private List<ExpedienteBandejaSalida> armaSalidaGrupo(final List<Expediente> expedientes, final boolean recibido,
			final boolean despachado) {
		final List<ExpedienteBandejaSalida> ebsList = new ArrayList<ExpedienteBandejaSalida>();
		for (Expediente e : expedientes) {
			final ExpedienteBandejaSalida ebs = new ExpedienteBandejaSalida();
			ebs.setId(e.getId());
			boolean despacho = false;
			if(e.getFechaDespacho() != null) despacho = true;
			ebs.setDespachado(despacho);
			ebs.setNumeroExpediente(e.getNumeroExpediente());
			ebs.setFechaIngreso(e.getFechaIngreso());

			if (e.getVersionPadre() == null) {
				ebs.setExpedienteNuevo(true);
			} else {
				ebs.setExpedienteNuevo(false);
			}

			ebs.setDestinatarioExpediente(e.getDestinatario());

			final List<DocumentoExpediente> docs = e.getDocumentos();
			Documento doc = null;
			boolean respuesta = false;
			for (int i = docs.size() - 1; i > 0; i--) {
				if (docs.get(i).getTipoDocumentoExpediente()
						.equals(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA))) {
					doc = docs.get(i).getDocumento();
					respuesta = true;
					break;
				}
			}
			boolean hayDoc = true;
			if (!respuesta) {
				if (e.getDocumentos() != null && e.getDocumentos().size() != 0) {
					doc = e.getDocumentos().get(0).getDocumento();
				} else {
					doc = new Documento();
					hayDoc = false;
				}
			}
			if (hayDoc) {
				ebs.setNumeroDocumento(doc.getNumeroDocumento());
				ebs.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
				if (doc.getReservado()) {
					ebs.setMateria("Materia Confidencial");
				} else {
					ebs.setMateria(doc.getMateria());
				}
				ebs.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
				ebs.setEmisor(doc.getEmisor());

				final StringBuilder sb = new StringBuilder();

				if (doc.getTipoDocumento() != null && (
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
				// ||
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES)
				// ||
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADQUISICION)
				// ||
						doc.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION))) {
					if (e.getDestinatario() != null) {
						sb.append(e.getDestinatario().getNombreApellido());
						sb.append(" ");
					} else {
						if (e.getDestinatarioHistorico() != null) {
							sb.append(e.getDestinatarioHistorico().getNombreApellido());
							sb.append(" ");
						}
					}
				} else {
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
				}
				if (sb.length() != 0) {
					ebs.setDestinatario(sb.toString());
				} else {
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
					ebs.setDestinatario(sb.toString());
				}
				ebs.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			}
			ebs.setRecibido(recibido);

			ebs.setPlazo(doc.getPlazo());
			boolean completado = true;
			for (DocumentoExpediente de : docs) {
				final Documento docTmp = de.getDocumento();
				if (docTmp.getCompletado() != null && !docTmp.getCompletado()) {
					if (docTmp.getPlazo() != null && docTmp.getPlazo().before(new Date())) {
						completado = false;
						break;
					}
				}
			}
			ebs.setCompletado(completado);
			ebs.setGrupoId(e.getGrupo());
			ebsList.add(ebs);
		}
		return ebsList;
	}
	
	public boolean existeEspediente( List<ExpedienteBandejaSalida> listExpedientes, String numeroExpediente){
		boolean esta = false;
		
		for (ExpedienteBandejaSalida bandeja : listExpedientes) {
			if(bandeja.getNumeroExpediente().equals(numeroExpediente)) return true;
		}
		
		return esta;
	}
	
	private List<ExpedienteBandejaSalida> armaSalida(final List<Expediente> expedientes, final boolean recibido,
			final boolean despachado) {
		
		
		final List<ExpedienteBandejaSalida> ebsList = new ArrayList<ExpedienteBandejaSalida>();
		for (Expediente e : expedientes) {
			final ExpedienteBandejaSalida ebs = new ExpedienteBandejaSalida();
			if(!existeEspediente(ebsList ,e.getNumeroExpediente())){

			ebs.setId(e.getId());
			ebs.setDespachado(despachado);
			ebs.setNumeroExpediente(e.getNumeroExpediente());
			ebs.setFechaIngreso(e.getFechaIngreso());

			if (e.getVersionPadre() == null) {
				ebs.setExpedienteNuevo(true);
			} else {
				ebs.setExpedienteNuevo(false);
			}

			ebs.setDestinatarioExpediente(e.getDestinatario());

			final List<DocumentoExpediente> docs = e.getDocumentos();
			Documento doc = null;
			boolean respuesta = false;
			for (int i = docs.size() - 1; i > 0; i--) {
				if (docs.get(i).getTipoDocumentoExpediente()
						.equals(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA))) {
					doc = docs.get(i).getDocumento();
					respuesta = true;
					break;
				}
			}
			boolean hayDoc = true;
			if (!respuesta) {
				if (e.getDocumentos() != null && e.getDocumentos().size() != 0) {
					doc = e.getDocumentos().get(0).getDocumento();
				} else {
					doc = new Documento();
					hayDoc = false;
				}
			}
			if (hayDoc) {
				ebs.setNumeroDocumento(doc.getNumeroDocumento());
				ebs.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
				if (doc.getReservado()) {
					ebs.setMateria("Materia Confidencial");
				} else {
					ebs.setMateria(doc.getMateria());
				}
				ebs.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
				ebs.setEmisor(doc.getEmisor());

				final StringBuilder sb = new StringBuilder();

				if (doc.getTipoDocumento() != null && (
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
				// ||
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES)
				// ||
				// doc.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADQUISICION)
				// ||
						doc.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION))) {
					if (e.getDestinatario() != null) {
						sb.append(e.getDestinatario().getNombreApellido());
						sb.append(" ");
					} else {
						if (e.getDestinatarioHistorico() != null) {
							sb.append(e.getDestinatarioHistorico().getNombreApellido());
							sb.append(" ");
						}
					}
				} else {
						
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
				}
				if (sb.length() != 0) {
					ebs.setDestinatario(sb.toString());
				} else {
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
					ebs.setDestinatario(sb.toString());
				}
				ebs.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			}
			ebs.setRecibido(recibido);

			ebs.setPlazo(doc.getPlazo());
			boolean completado = true;
			for (DocumentoExpediente de : docs) {
				final Documento docTmp = de.getDocumento();
				if (docTmp.getCompletado() != null && !docTmp.getCompletado()) {
					if (docTmp.getPlazo() != null && docTmp.getPlazo().before(new Date())) {
						completado = false;
						break;
					}
				}
			}
			ebs.setCompletado(completado);
			ebs.setGrupoId(e.getGrupo());
			ebsList.add(ebs);
			}
		}
		return ebsList;
	}

	/**
	 * Metodo que lista expedientes que no han dado acuse de recibo.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private List<Expediente> expedientesDespachadosSinAcuseRecibo(final Persona persona) {
		final StringBuilder jpQL = new StringBuilder(10);
		jpQL.append("SELECT e ");
		jpQL.append("FROM Expediente e ");
		jpQL.append("WHERE e.emisor = ? ");
		jpQL.append("	and e.cancelado is null ");
		jpQL.append("	and e.destinatario is not null ");
		jpQL.append("	and e.fechaDespacho is not null ");
		jpQL.append("	and e.fechaAcuseRecibo is null ");
		jpQL.append("	and e.grupo is null");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, persona);

		final List<Expediente> expedientes = query.getResultList();
		final List<Expediente> expedientesDespachados = new ArrayList<Expediente>();
		for (Expediente e : expedientes) {
			if (!this.existeExpediente(expedientesDespachados, e.getVersionPadre())) {
				expedientesDespachados.add(e.getVersionPadre());
			}
		}
		return expedientesDespachados;
	}

	@Override
	public List<ExpedienteBandejaSalida> listarExpedientesDespachadosPorUsuario(final Persona persona) {

		final List<Expediente> expedientesDespachados = this.expedientesDespachadosSinAcuseRecibo(persona);
		
		final List<ExpedienteBandejaSalida> ebsList = this.armaSalida(expedientesDespachados, false, true);

		// expedientes que han dado acuse de recibo
		final List<Expediente> expedientesDespachadosRecibidos = this.expedientesDespachadosConAcuseRecibo(persona,
				expedientesDespachados);
		
		ebsList.addAll(this.armaSalida(expedientesDespachadosRecibidos, true, true));
		
		final List<ExpedienteBandejaSalida> finalList = new ArrayList<ExpedienteBandejaSalida>();
		
		for (ExpedienteBandejaSalida expediente : ebsList) {
			if(!existeEspediente(ebsList, expediente.getNumeroExpediente()))finalList.add(expediente);
		}
		
		return ebsList;
	}

	/**
	 * Metodo que lista expedientes que son enviado a grupos
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link Expediente}
	 */
	public List<ExpedienteBandejaSalida> expedientesDespachadosAGrupos(final Persona persona){
		
		long ini = System.currentTimeMillis();
		long fin; 
//		List<Expediente> expedientes = new ArrayList<Expediente>();
		List<Expediente> expedientesDespachados = new ArrayList<Expediente>();
		//listado desde el procedimiento almacenado
		 String mySQLGrupo = "{?=call EXE_DOC_BS.fn_bdja_slda_grupo(?)}";
	        ResultSet rsgrupo = null;
	        CallableStatement csgrupo = null;
	        Connection connectiongrupo = null;
	        
	        try {
	        	connectiongrupo = DaoFactory.getDaoFactoryConnection();
	            csgrupo = connectiongrupo.prepareCall(mySQLGrupo);

	            csgrupo.registerOutParameter(1, OracleTypes.CURSOR);
	            int usrID = persona.getId().intValue();
	            csgrupo.setInt(2, usrID);
	            csgrupo.executeQuery();
	            rsgrupo = (ResultSet) csgrupo.getObject(1);
	            fin = System.currentTimeMillis();
				
				connectiongrupo.close();
				 while(rsgrupo.next()){
					 long id = rsgrupo.getLong("ID");
					 Expediente e = this.buscarExpediente(id);
								if(!expedientesDespachados.contains(e.getVersionPadre()) && e.getVersionPadre().getArchivado() == null) expedientesDespachados.add(e.getVersionPadre());
				 }
	        }
	        catch(Exception ex){}
	         /*
		final StringBuilder jpQL = new StringBuilder(10);
		jpQL.append("SELECT e ");
		jpQL.append("FROM Expediente e ");
		jpQL.append("WHERE e.emisor = ? ");
		jpQL.append("	and e.cancelado is null ");
		jpQL.append("	and e.destinatario is null ");
		jpQL.append("	and e.grupo is not null ");
		
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, persona);
		expedientes = (List<Expediente>)query.getResultList();
		
		
		for (Expediente e : expedientes) {
			if (!this.existeExpediente(expedientesDespachados, e.getVersionPadre())) {
				if(!expedientesDespachados.contains(e.getVersionPadre()) && e.getVersionPadre().getArchivado() == null) expedientesDespachados.add(e.getVersionPadre());
			}
		}
		*/
	        fin = System.currentTimeMillis();
	        System.out.println("tiempo fin "+(fin-ini));
		return armaSalidaGrupo(expedientesDespachados, false, false);//this.armaSalida(expedientesDespachados, false, false);
	}
	
	/**
	 * Metodo que lista expedientes que han dado acuse de recibo.
	 * 
	 * @param persona {@link Persona}
	 * @param expedientesDespachados {@link List} of {@link Expediente}
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private List<Expediente> expedientesDespachadosConAcuseRecibo(final Persona persona,
			final List<Expediente> expedientesDespachados) {
		final Calendar liminf = new GregorianCalendar();
		FechaUtil.inicio(liminf);
		final Calendar limsup = new GregorianCalendar();
		FechaUtil.fin(limsup);

		// liminf.clear(Calendar.HOUR);
		// liminf.clear(Calendar.MILLISECOND);
		// liminf.set(Calendar.HOUR_OF_DAY, 0);
		// liminf.set(Calendar.MINUTE, 0);
		// liminf.set(Calendar.SECOND, 0);

		// limsup.clear(Calendar.HOUR);
		// limsup.clear(Calendar.MILLISECOND);
		// limsup.set(Calendar.HOUR_OF_DAY, 23);
		// limsup.set(Calendar.MINUTE, 59);
		// limsup.set(Calendar.SECOND, 59);

		final StringBuilder jpQL = new StringBuilder("SELECT e FROM Expediente e WHERE ");
		jpQL.append(" e.emisorHistorico = ? ");
		jpQL.append(" and e.cancelado is null ");
		jpQL.append(" and e.fechaAcuseRecibo between ? and ?");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, persona);
		query.setParameter(2, liminf.getTime());
		query.setParameter(3, limsup.getTime());
		final List<Expediente> expedientes = query.getResultList();
		final List<Expediente> expedientesDespachadosRecibidos = new ArrayList<Expediente>();
		for (Expediente e : expedientes) {
			if (!this.existeExpediente(expedientesDespachados, e.getVersionPadre())
					&& !expedientesDespachadosRecibidos.contains(e.getVersionPadre())) {
				expedientesDespachadosRecibidos.add(e.getVersionPadre());
			}
		}
		return expedientesDespachadosRecibidos;
	}

	/**
	 * Metodo que retorna true si el expediente se encuentra dentro de la lista.
	 * 
	 * @param expedientes {@link List} of {@link Expediente}
	 * @param expediente {@link Expediente}
	 * @return {@link Boolean}
	 */
	private boolean existeExpediente(final List<Expediente> expedientes, final Expediente expediente) {
		for (Expediente e : expedientes) {
			if (e.getNumeroExpediente().equals(expediente.getNumeroExpediente())) { return true; }
		}
		return false;
	}

	@Override
	public List<ExpedienteBandejaSalida> listarExpedientesArchivadosPorUsuario(final List<Object[]> expedientes) {
		final List<ExpedienteBandejaSalida> lista = this.armarReporteArchivados(expedientes, false, true);
		return lista;
	}

	/**
	 * Metodo que arma la lista para reportes archivados.
	 * 
	 * @param expedientes {@link List} of {@link Object}
	 * @param soloVencidos {@link Boolean}
	 * @param archivado {@link Boolean}
	 * @return {@link List} of {@link ExpedienteBandejaSalida}
	 */
	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaSalida> armarReporteArchivados(final List<Object[]> expedientes,
			final boolean soloVencidos, final boolean archivado) {
		final List<ExpedienteBandejaSalida> ebsList = new ArrayList<ExpedienteBandejaSalida>();

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		final StringBuilder sqdoc = new StringBuilder(
				"select doc.numeroDocumento, doc.fechaDocumentoOrigen, doc.fechaCreacion, ");
		sqdoc.append("doc.reservado, doc.materia, doc.tipoDocumento.descripcion, doc.emisor, ");
		sqdoc.append("doc.plazo, doc.formatoDocumento.descripcion, doc.tipoDocumento.id, doc.id ");
		sqdoc.append("from Documento doc ");
		sqdoc.append("where doc.id = ? ");
		final Query qryDoc = em.createQuery(sqdoc.toString());

		for (Object[] e : expedientes) {
			final ExpedienteBandejaSalida ebs = new ExpedienteBandejaSalida();
			ebs.setId(((Long) e[0]).longValue());
			ebs.setNumeroExpediente((String) e[1]);
			ebs.setFechaIngreso((Date) e[2]);
			final Boolean recibido = (Boolean) e[3];
			ebs.setRecibido(recibido);
			final Boolean copia = (Boolean) e[4];
			ebs.setCopia(copia);
			// String destinatarioExp = (String) e[5];
			// String destinatarioHistExp = (String) e[6];

			if (archivado) {
				ebs.setFechaArchivado((Date) e[7]);
			}

			final Long idDestinatario = (((Long) e[8]) != null) ? ((Long) e[8]).longValue() : null;
			final Long idDestinatarioHistorico = (((Long) e[9]) != null) ? ((Long) e[9]).longValue() : null;

			if (idDestinatario != null) {
				final Persona destinatario = em.find(Persona.class, idDestinatario);
				ebs.setDestinatarioExpediente(destinatario);
			} else if (idDestinatarioHistorico != null) {
				final Persona destinatarioHistorico = em.find(Persona.class, idDestinatarioHistorico);
				ebs.setDestinatarioExpediente(destinatarioHistorico);
			}

			ebs.setEmisor(this.consultarEmisor(ebs, recibido));

			qryDocs.setParameter(1, ebs.getId());
			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);

			final List<Object[]> qdocs = qryDocs.getResultList();

			boolean respuesta = false;
			Long docId = null;
			Long docIdOriginal = null;
			boolean completado = true;
			final Date hoy = new Date();
			boolean encontrado = false;

			for (Object[] d : qdocs) {

				final Integer tipoDocumento = (Integer) d[0];
				final Boolean comp = (Boolean) d[2];
				final Date plazo = (Date) d[3];
				if (completado && comp != null && !comp && plazo != null) {
					if (plazo.before(hoy)) {
						completado = false;
					}
				}
				if (!encontrado) {
					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
						docId = (Long) d[1];
						respuesta = true;
						encontrado = true;
					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
						docIdOriginal = (Long) d[1];
						encontrado = true;
					}
				}
			}

			if (!respuesta) {
				docId = docIdOriginal;
			}

			ebs.setCompletado(completado);

			qryDoc.setParameter(1, docId);
			final Object[] qdoc = (Object[]) qryDoc.getSingleResult();

			ebs.setNumeroDocumento((String) qdoc[0]);
			ebs.setFechaDocumentoOrigen((Date) qdoc[1]);
			ebs.setFechaCreacionDocumento((Date) qdoc[2]);
			if (qdoc[3] != null) {
				ebs.setMateria((Boolean) qdoc[3] ? "Materia Confidencial" : (String) qdoc[4]);
			}
			ebs.setTipoDocumento((String) qdoc[5]);
			ebs.setEmisor((String) qdoc[6]);
			ebs.setPlazo((Date) qdoc[7]);
			ebs.setFormatoDocumento((String) qdoc[8]);
			// Integer idTipoDoc = (Integer) qdoc[9];
			final Long idDoc = (((Long) qdoc[10]) != null) ? ((Long) qdoc[10]) : null;
			ebs.setIdDocumento(idDoc);

			final String destinatarios = this.buscarDestinatarios(docId);
			if (destinatarios.length() != 0) {
				ebs.setDestinatario(destinatarios);
			}

			if (!soloVencidos) {
				ebsList.add(ebs);
			} else if (!completado) {
				ebsList.add(ebs);
			}
		}

		return ebsList;
	}

	/**
	 * Metodo que entrega String con los destinatarios.
	 * 
	 * @param docId {@link Long}
	 * @return {@link String}
	 */
	@SuppressWarnings("unchecked")
	private String buscarDestinatarios(final Long docId) {
		final StringBuilder sb = new StringBuilder();

		final StringBuilder sql = new StringBuilder(10);
		sql.append("select de.destinatario ");
		sql.append("from ListaPersonasDocumento de ");
		sql.append("where de.documento.id = ? ");
		final Query qDest = em.createQuery(sql.toString());
		qDest.setParameter(1, docId);
		final List<String> sDestinatarios = qDest.getResultList();
		for (String dd : sDestinatarios) {
			sb.append(dd);
			sb.append(" ");
		}
		return sb.toString();
	}

	/**
	 * Metodo que busca el emisor de un expediente de la bandeja de salida.
	 * 
	 * @param ebs {@link ExpedienteBandejaEntrada}
	 * @param recibido {@link Boolean}
	 * @return {@link String}
	 */
	private String consultarEmisor(final ExpedienteBandejaSalida ebs, final Boolean recibido) {
		StringBuilder sql;
		if (recibido != null && recibido) {
			sql = new StringBuilder(5);
			sql.append("select e.emisorHistorico.nombres || ' ' || ");
			sql.append("	e.emisorHistorico.apellidoPaterno ");
			sql.append("from Expediente e ");
			sql.append("	where e.id = ? ");
			sql.append("	and e.cancelado is null ");
		} else {
			sql = new StringBuilder(5);
			sql.append("select e.emisor.nombres || ' ' || ");
			sql.append("	e.emisor.apellidoPaterno ");
			sql.append("from Expediente e ");
			sql.append("where e.id = ? ");
			sql.append("	and e.cancelado is null ");
		}
		final Query query = em.createQuery(sql.toString());
		query.setParameter(1, ebs.getId());
		return (String) query.getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void adjuntarDocumento(final Expediente expediente, final Documento documento,
			final TipoDocumentoExpediente tipoDocumentoExpediente, final boolean esPadre)
			throws DocumentNotUploadedException {
		if (esPadre) {
			manejarDocumento.crearDocumento(documento);
		}
		final DocumentoExpediente documentoExpediente = new DocumentoExpediente();
		documentoExpediente.setDocumento(documento);
		documentoExpediente.setExpediente(expediente);
		documentoExpediente.setTipoDocumentoExpediente(tipoDocumentoExpediente);
		documentoExpediente.setEnEdicion(true);
		em.persist(documentoExpediente);
		expediente.addDocumento(documentoExpediente);
	}

	// TODO revisar documentos anexos
	public void adjuntarDocumento(Expediente expediente, Documento documento, TipoDocumentoExpediente
			tipoDocumentoExpediente, boolean esPadre, Long idDocumentoReferencia) {
		if (esPadre) {
			try {
				manejarDocumento.crearDocumento(documento);
			} catch (DocumentNotUploadedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		DocumentoExpediente documentoExpediente = new
		DocumentoExpediente(); documentoExpediente.setDocumento(documento);
		documentoExpediente.setExpediente(expediente);
		documentoExpediente.setTipoDocumentoExpediente(tipoDocumentoExpediente); documentoExpediente.setEnEdicion(true);
		//documentoExpediente.setIdDocumentoReferencia(idDocumentoReferencia); em.persist(documentoExpediente);
		expediente.addDocumento(documentoExpediente);
	}
	 

	@Override
	public void agregarOriginalAExpediente(final Expediente expediente, final Documento documento, final boolean esPadre)
			throws DocumentNotUploadedException {
		final TipoDocumentoExpediente tipo = new TipoDocumentoExpediente(TipoDocumentoExpediente.ORIGINAL);
		this.adjuntarDocumento(expediente, documento, tipo, esPadre);
	}

	@Override
	public void agregarRespuestaAExpediente(final Expediente expediente, final Documento documento,
			final boolean esPadre) throws DocumentNotUploadedException {
		TipoDocumentoExpediente tipo = new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA);
		this.adjuntarDocumento(expediente, documento, tipo, esPadre);
	}
	
	@Override
	public void agregarRespuestaSolicitudAExpediente(final Expediente expediente, final Documento documento,
			final boolean esPadre) throws DocumentNotUploadedException {
		final TipoDocumentoExpediente tipo = new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA_SOLICITUD_DE_RESPUESTA);
		this.agregarDocumentoAExpediente(expediente, documento, esPadre,tipo);
	}
	private void agregarDocumentoAExpediente(final Expediente expediente, final Documento documento,
			final boolean esPadre, final TipoDocumentoExpediente tipo) throws DocumentNotUploadedException{
		this.adjuntarDocumento(expediente, documento, tipo, esPadre);
	}
	
	@Override
	public void agregarAnexoAExpediente(Expediente expediente, Documento documento, boolean esPadre) {
		TipoDocumentoExpediente tipo = new TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO);
		try {
			this.adjuntarDocumento(expediente, documento, tipo, esPadre);
		} catch (DocumentNotUploadedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void agregarAnexoAExpediente(Expediente expediente, Documento documento, boolean esPadre, Long
			idDocumentoReferencia) {
		TipoDocumentoExpediente tipo = new
		TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO);
		this.adjuntarDocumento(expediente, documento, tipo, esPadre, idDocumentoReferencia);
	}
	 
	@SuppressWarnings("unchecked")
	@Override
	public Long agregarDestinatarioAExpediente(final Expediente expediente, final Persona emisor,
			final Persona destinatario, final Date fechaIngreso) throws DocumentNotUploadedException {
		final Expediente copiaExpediente = this.crearCopiaExpediente(expediente, emisor, destinatario, fechaIngreso);
	
		this.guardarObservaciones(copiaExpediente, expediente);

		// copia de documentos
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT de ");
		jpQL.append("FROM DocumentoExpediente de ");
		jpQL.append("WHERE de.expediente = ? ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, expediente);
		final List<DocumentoExpediente> docs = (List<DocumentoExpediente>) query.getResultList();

		for (DocumentoExpediente de : docs) {
			if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
				this.agregarOriginalAExpediente(copiaExpediente, de.getDocumento(), false);
			} else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				this.agregarRespuestaAExpediente(copiaExpediente, de.getDocumento(), false);
			}
		}
		// fin copia documentos
		em.merge(copiaExpediente);

		return copiaExpediente.getId();
	}
	
	@Override
	public Long agregarDestinatarioAExpediente(Expediente expediente, Persona emisor, Destinatario destinatario,
			Date fechaIngreso) {
		Expediente copiaExpediente = new Expediente();
		copiaExpediente.setEmisor(emisor);
		copiaExpediente.setDestinatario(destinatario.getPersona());
		copiaExpediente.setVersionPadre(expediente);
		copiaExpediente.setFechaIngreso(fechaIngreso);
		copiaExpediente.setNumeroExpediente(expediente.getNumeroExpediente());

		copiaExpediente.setArchivado(null);

		copiaExpediente.setCopia(destinatario.isCopia());

		em.persist(copiaExpediente);

		this.guardarObservaciones(copiaExpediente, expediente);

		// copia de documentos

		List<DocumentoExpediente> docs = expediente.getDocumentos();

		/*
		 * 
		 * StringBuilder jpQL = new
		 * StringBuilder("SELECT de FROM DocumentoExpediente de WHERE ");
		 * jpQL.append(" de.expediente = ? "); Query query =
		 * em.createQuery(jpQL.toString()); query.setParameter(1, expediente);
		 * List<DocumentoExpediente> docs = (List<DocumentoExpediente>)
		 * query.getResultList();
		 */
		if (docs != null) {
			for (DocumentoExpediente de : docs) {

				Documento d = de.getDocumento();

				if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
					try {
						this.agregarOriginalAExpediente(copiaExpediente, d, false);
					} catch (DocumentNotUploadedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
					this.agregarAnexoAExpediente(copiaExpediente, d, false, de.getIdDocumentoReferencia());
				} else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
					try {
						this.agregarRespuestaAExpediente(copiaExpediente, d, false);
					} catch (DocumentNotUploadedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}// fin copia documentos
		}
		em.merge(copiaExpediente);
		em.flush();

		/*
		 * Boolean sw = destinatario.getExterno(); if(sw != null && sw) {
		 * Expediente espedienteWS = abrirListas(copiaExpediente);
		 * CallWebService.callDespachar(copiaExpediente); }
		 */

		return copiaExpediente.getId();
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private Expediente crearCopiaExpediente(final Expediente expediente,
			final Persona emisor, final Persona destinatario,
			final Date fechaIngreso) {
		final Expediente copiaExpediente = new Expediente();
		copiaExpediente.setEmisor(emisor);
		copiaExpediente.setDestinatario(destinatario);
		copiaExpediente.setVersionPadre(expediente);
		copiaExpediente.setFechaIngreso(fechaIngreso);
		copiaExpediente.setNumeroExpediente(expediente.getNumeroExpediente());
		
		copiaExpediente.setArchivado(null);

		copiaExpediente.setCopia(destinatario.getDestinatarioConCopia());

		em.persist(copiaExpediente);
		return copiaExpediente;
	}

	@SuppressWarnings("unchecked")
	@Override
	//@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long agregarDestinatarioAExpediente(final Expediente expediente, final Persona emisor,
			final GrupoUsuario destinatario, final Date fechaIngreso) throws DocumentNotUploadedException {
		final Expediente copiaExpediente = new Expediente();
		copiaExpediente.setEmisor(emisor);
		copiaExpediente.setDestinatario(null);
		copiaExpediente.setVersionPadre(expediente);
		copiaExpediente.setFechaIngreso(fechaIngreso);
		copiaExpediente.setNumeroExpediente(expediente.getNumeroExpediente());

		copiaExpediente.setArchivado(null);

		copiaExpediente.setCopia(null);

		copiaExpediente.setGrupo(destinatario);
		em.persist(copiaExpediente);

		this.guardarObservaciones(copiaExpediente, expediente);

		// TODO ISTAWA ESTA MAS MALA QUE LA COMIDA DEL PERRO!!!
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT de ");
		jpQL.append("FROM DocumentoExpediente de ");
		jpQL.append("WHERE de.expediente = ? ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, expediente);
		final List<DocumentoExpediente> docs = (List<DocumentoExpediente>) query.getResultList();

		for (DocumentoExpediente de : docs) {
			if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
				this.agregarOriginalAExpediente(copiaExpediente, de.getDocumento(), false);
			} else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				this.agregarRespuestaAExpediente(copiaExpediente, de.getDocumento(), false);
			}
		}
		// fin copia documentos
		em.merge(copiaExpediente);

		return copiaExpediente.getId();
	}
	/**
	 * Metodo que guarda las observaciones.
	 * 
	 * @param expediente {@link Expediente}
	 * @param expedienteOriginal {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private void guardarObservaciones(final Expediente expediente, final Expediente expedienteOriginal) {
//		final Query query = em.createQuery("select o from Observacion o where o.expediente = ?");
//		query.setParameter(1, expedienteOriginal);
//		final List<Observacion> observacionesExpediente = query.getResultList();
		final List<Observacion> observaciones = new ArrayList<Observacion>();
		if (expedienteOriginal.getObservaciones() != null) {
			for (Observacion obs : expedienteOriginal.getObservaciones()) {
				Observacion o = new Observacion();
			o.setObservacion(obs.getObservacion());
			if (!observaciones.contains(o)) {
				o.setExpediente(expediente);
				o.setFecha(obs.getFecha());
				o.setAutor(obs.getAutor());
				o.setObservacionArchivo(obs.getObservacionArchivo());
				o.setTipoObservacion(obs.getTipoObservacion());
					//em.persist(o);
					//em.flush();
				observaciones.add(o);
				}
			}
		}
		expediente.setObservaciones(observaciones);
		if(expediente.getObservaciones() == null) {
			expediente.setObservaciones(new ArrayList<Observacion>());
		}
		expediente.getObservaciones().addAll(observaciones);
		
		em.merge(expediente);
	}

	@Override
	public Integer actualizarVersion(final Expediente expediente) {
		if (expediente.getVersionPadre() == null) {
			return null;
		}
		Query query;
		if (expediente.getGrupo() == null) {
			query = em.createNamedQuery("Expediente.getVersionPersona");
			query.setParameter("idPersona", expediente.getDestinatario().getId());
			query.setParameter("numeroExpediente", expediente.getNumeroExpediente());
			query.setParameter("idExpediente", expediente.getId());
		} else {
			query = em.createNamedQuery("Expediente.getVersionGrupo");
			query.setParameter("idGrupo", expediente.getGrupo().getId());
			query.setParameter("numeroExpediente", expediente.getNumeroExpediente());
			query.setParameter("idExpediente", expediente.getId());
		}
		Integer contador = 1;
		try {
			contador = ((Long) query.getSingleResult()).intValue();
		} catch (NoResultException nre) {}

//		int contador = 1;
//		final Persona destinatario = expediente.getDestinatario();
//		if (expediente != null && destinatario != null) {
//			Expediente padreExpediente = expediente.getVersionPadre();
//			Long idPadre = (padreExpediente != null) ? padreExpediente.getId() : null;
//			while (idPadre != null) {
//				if (padreExpediente.getDestinatarioHistorico() != null) {
//					if (padreExpediente.getDestinatarioHistorico().getId().longValue() == destinatario.getId()
//							.longValue()) {
//						contador++;
//					}
//				}
//				if (padreExpediente.getVersionPadre() == null) {
//					if (padreExpediente.getEmisor() != null) {
//						if (padreExpediente.getEmisor().getId().longValue() == destinatario.getId().longValue()) {
//							contador++;
//						}
//					}
//				}
//				padreExpediente = padreExpediente.getVersionPadre();
//				idPadre = (padreExpediente != null) ? padreExpediente.getId() : null;
//			}
//		}
		return contador;
	}

	@Override
	public void despacharExpediente(final Expediente expediente) {
		Date fechaDespacho = new Date();
		if (!(expediente.getDestinatario() == null && expediente.getGrupo() == null)) {
			expediente.setVersion(this.actualizarVersion(expediente));
			expediente.setFechaIngreso(fechaDespacho);
		}
		expediente.setFechaDespacho(fechaDespacho);
		
		em.merge(expediente);
		em.flush();
		
		if (expediente.getDestinatario() != null) {
			enviarMailPorDestinatarios(expediente.getDestinatario(), 2, expediente);
		}
		if (expediente.getGrupo() != null) {
			enviarMailPorGrupo(expediente.getGrupo(), 2, expediente);
		}
		this.borrarExpedienteDeCache(expediente);
	}

	//@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void actualizarDocumentoExpediente(final Expediente expediente) {
		List<DocumentoExpediente> documentos = expediente.getDocumentos();
		
		try {
		if (documentos != null) {
			for (DocumentoExpediente de : documentos) {
				if (de.getEnEdicion()) {
					de.setEnEdicion(false);
					em.merge(de);
				}
			}
		}
		//expediente.setDocumentos(documentos);
		//em.merge(expediente);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	/**
//	 * Metodo encargado de mandar mail. al destinatario del expediente.
//	 * 
//	 * @param expediente {@link Expediente}
//	 */
//	public void enviarMail(final Expediente expediente) {
//		if (!customizacion.getEnviadorCorreo()) {
//			log.info("Enviador de correo desactivado...");
//			return;
//		}
//		if (expediente.getDestinatario() != null) {
//			final Email email = new Email();
//			final Mail mail = new Mail();
//			try {
//				mail.setForm(new InternetAddress(expediente.getDestinatario().getEmail()));
//				mail.setSubject("-EXEDOC-");
//				mail.setText("prueba de correo electronico");
//				email.enviarMail(mail);
//			} catch (AddressException e) {
//				log.error("Error al enviar por correo electronico.", e);
//			}
//		}
//
//	}

	@SuppressWarnings("unused")
	public byte[] getArchivo(DocumentoExpediente documento) {
		byte[] data = null;
		//repositorio.recuperarArchivo(documento.getArchivo().getCmsId());
		try {
			 data = repositorio.getFile(documento.getDocumento().getCmsId());
			 
			if (data != null) {
				final FacesContext facesContext = FacesContext
						.getCurrentInstance();
			} else {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ documento.getId() + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	@Override
	public void enviarMailPorGrupo(GrupoUsuario grupo, int plantilla, Expediente expediente) {
		//obligar a cargar
		grupo = em.find(GrupoUsuario.class, grupo.getId());
		if (grupo.getUsuarios() != null && grupo.getUsuarios().size() > 0) {
			for (Persona p : grupo.getUsuarios()) {
				enviarMailPorDestinatarios(p, plantilla, expediente);
			}
		}
	}
	/**
	 * SOS si faltan plantillas
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void enviarMailPorDestinatarios(final Persona destinatario, int plantilla, Expediente expediente) {
		if (!customizacion.getEnviadorCorreo()) {
			log.info("Enviador de correo desactivado...");
			return;
		}
		if (destinatario.getEmail() == null) {
			return;
		}
		final Email email = new Email();
		String mensaje = new String();
		String result = new String();
		Persona emisor = expediente.getEmisor();
		List<DocumentoExpediente> documentos = expediente.getDocumentos();
		// TODO COMPROBAR ORDEN DESCENDENTE
		Collections.sort(documentos, new Comparator<DocumentoExpediente>() {
			public int compare(final DocumentoExpediente o1, final DocumentoExpediente o2) {
				return o1.getDocumento().getFechaCreacion().compareTo(o2.getDocumento().getFechaCreacion());
			}
		});
		DocumentoExpediente documento = null;
		Date plazo = new Date();
		Cargo car = new Cargo();
		Division div = new Division();
		SimpleDateFormat format = new SimpleDateFormat();
		List<Observacion> obs = new ArrayList<Observacion>();
		String emisorNom = "";
		String observaciones = "";
		if(documentos != null && documentos.size() != 0) documento = documentos.get(0);//documento = documentos.get(documentos.size()-1);
		boolean tipoPlantilla = false;
		switch (plantilla) {
			case 1:
				// cargar plantilla firmado
				String pl = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_FIRMADO);
				InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(pl);
				try {
					mensaje = convertStreamToString(resourceAsStream);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				result = MessageFormat.format(mensaje, expediente.getId(), expediente.getNumeroExpediente(),
				destinatario.getNombres() + " " + destinatario.getApellidoPaterno(), new Date());
				break;

			case 2:
				// cargar plantilla despacho
				// TODO HAY QUE ARREGLAR ESTO ESTA MAS DESORDENADO QUE CUMPLEAÑOS DE MONO
				if(documento == null){
					String plDespacho = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_DESPACHO);
					InputStream resourceAsStreamDespacho = Thread.currentThread().getContextClassLoader()
												.getResourceAsStream(plDespacho);
					try {
						mensaje = convertStreamToString(resourceAsStreamDespacho);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				else{
					String plDespacho = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_DESPACHO_BINARIO);
					InputStream resourceAsStreamDespacho = Thread.currentThread().getContextClassLoader()
												.getResourceAsStream(plDespacho);
					try {
						mensaje = convertStreamToString(resourceAsStreamDespacho);
						tipoPlantilla = true;
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				plazo = new Date();
		
				//Persona emisor = expediente.getEmisor();
			
				emisorNom = emisor.getNombres() + " " + emisor.getApellidoPaterno() + " "
								+ emisor.getApellidoMaterno();
				car = emisor.getCargo();
			    div = emisor.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
				format = new SimpleDateFormat("dd/MM/yyyy");
				obs = expediente.getObservaciones();
				observaciones = "";
				if (obs != null && obs.size() > 0) {
					for (Iterator iterator = obs.iterator(); iterator.hasNext();) {
						Observacion observacion = (Observacion) iterator.next();
						observaciones = observaciones + "\n" + observacion.getObservacion();
					}

				}
				try {
					final DocumentoExpediente docExp = expediente.getDocumentos().get(0);
					final Documento doc = docExp.getDocumento();
					if (doc.getFechaDocumentoOrigen() != null) {
						plazo = doc.getFechaDocumentoOrigen();
					}
				} catch (Exception e1) {
					log.error("La fecha del documento ", e1);
				}
				if(tipoPlantilla){
					result = MessageFormat.format(mensaje, expediente.getNumeroExpediente(),
							(expediente.getGrupo() != null ? "Compartida" : (expediente.getCopia() ? "Copia" : "de Entrada")),
							format.format(new Date()), 
							documento.getDocumento().getNumeroDocumento(), 
							(documento.getDocumento().getMateria() != null ? documento.getDocumento().getMateria() : ""),
							documento.getDocumento().getTipoDocumento().getDescripcion(),
							documento.getDocumento().getEmisor(),
							(documento.getDocumento().getAntecedentes() != null ? documento.getDocumento().getAntecedentes() : ""));
							break;
				}
				else{
					result = MessageFormat.format(mensaje, expediente.getNumeroExpediente(), 
							(expediente.getGrupo() != null ? "Compartida" : (expediente.getCopia() ? "Copia" : "de Entrada")),
							format.format(new Date()));
							break;
				}
				
//			case 3:
//				//carga de plantillas para 
//				if(documento.getDocumento().getCmsId() == null){
//					String plDespacho = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_SOLICITUD_PAPEL);
//					InputStream resourceAsStreamDespacho = Thread.currentThread().getContextClassLoader().getResourceAsStream(plDespacho);
//					try {
//						mensaje = convertStreamToString(resourceAsStreamDespacho);
//					} catch (IOException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//				}
//				else{
//					String plDespacho = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_SOLICITUD_BINARIO);
//					InputStream resourceAsStreamDespacho = Thread.currentThread().getContextClassLoader()
//												.getResourceAsStream(plDespacho);
//					try {
//						mensaje = convertStreamToString(resourceAsStreamDespacho);
//						tipoPlantilla = true;
//					} catch (IOException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
//				}
//
//				//Persona emisor = expediente.getEmisor();
//			
//				emisorNom = emisor.getNombres() + " " + emisor.getApellidoPaterno() + " "
//								+ emisor.getApellidoMaterno();
//				car = emisor.getCargo();
//				div = emisor.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
//				format = new SimpleDateFormat("dd/MM/yyyy");
//				obs = expediente.getObservaciones();
//				observaciones = "";
//				if (obs != null && obs.size() > 0) {
//					for (Iterator iterator = obs.iterator(); iterator.hasNext();) {
//						Observacion observacion = (Observacion) iterator.next();
//						observaciones = observaciones + "\n" + observacion.getObservacion();
//					}
//
//				}
//				try {
//					final DocumentoExpediente docExp = expediente.getDocumentos().get(0);
//					final Documento doc = docExp.getDocumento();
//					if (doc.getFechaDocumentoOrigen() != null) {
//						plazo = doc.getFechaDocumentoOrigen();
//					}
//				} catch (Exception e1) {
//					log.error("La fecha del documento ", e1);
//				}
//				
//				String plazo1 = "El plazo para dar respuesta es el dia ";
//				
//				List<Expediente> listaHijos = obtenerExpedientesHijos(expediente);
//				
//				
//				String instrucciones = "";
//				Expediente exp = null;
//				if(listaHijos != null && listaHijos.size() > 0)exp = listaHijos.get(0);
//				
//				
//				
//				if(tipoPlantilla){
//					result = MessageFormat.format(mensaje, expediente.getNumeroExpediente(),instrucciones,"",plazo1 );// emisorNom, car.getDescripcion(), div.getDescripcion(),format.format(plazo), observaciones
//					break;
//				}
//				else{	
//					result = MessageFormat.format(mensaje, expediente.getNumeroExpediente(),instrucciones,"", plazo1);
//					break;
//				}
				
			case 4:
				// cargar plantilla firmado
				pl = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_NOTIFICACION);
				resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(pl);
				try {
					mensaje = convertStreamToString(resourceAsStream);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				result = MessageFormat.format(mensaje, expediente.getNumeroExpediente());
				break;
				
			default:
				System.out.println("ha ocurrido un error con la plantilla para el correo");
				break;
		}
		
		if (destinatario != null) {
			final String form = destinatario.getEmail();
			if (form != null) {
				final Mail mail = new Mail();
			
				try {
					final StringBuilder asunto = new StringBuilder();
					format = new SimpleDateFormat("yyyy");
					// asunto.append("Derivación del ");
					// asunto.append(destinatario.getCargo().getDescripcion());
					asunto.append("Derivacion ");
					if (emisor != null) {
						asunto.append("del " + TextUtil.eliminaTildes(emisor.getCargo().getDescripcion()));
					}
					//byte[] archivo = null;
					//if(documento.getDocumento().getCmsId() != null && documento.getDocumento().getCmsId().trim().length() > 0) archivo = getArchivo(documento);
					asunto.append(" de la "+emisor.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion());
					asunto.append(" a su Unidad o Division, N ingreso ");
					asunto.append(expediente.getNumeroExpediente());
//					asunto.append(" del ");
//					asunto.append(format.format(new Date()));
					mail.setForm(new InternetAddress(form));
					mail.setSubject(asunto.toString());
					mail.setText(result);
//					if (tipoPlantilla){
//						if( archivo != null){
//							final Query query = em.createQuery("select a from Archivo a where a.cmsId = :cmsId");
//							query.setParameter("cmsId",documento.getDocumento().getCmsId());
//							Archivo arch = null;
//							try{
//								arch = (Archivo) query.getSingleResult();
//							}
//							catch(Exception ex){}
//							mail.setData(archivo);
//							if(arch != null)mail.setNombreArchivo(arch.getNombreArchivo());
//							email.enviarMailArchivoAdjunto(mail);
//						}
//						email.enviarMail(mail);
//					}
//					else{
//						email.enviarMail(mail);
//					}
					email.enviarMail(mail);
					
				} catch (AddressException e) {
					log.error("Error al enviar por correo electronico.", e);
				}
			}
		}
	}

	@Override
	public void despacharExpedienteMinisterio(final Expediente expediente) {
		expediente.setVersion(this.actualizarVersion(expediente));
		expediente.setFechaDespacho(new Date());
		final List<DocumentoExpediente> documentos = expediente.getDocumentos();
		for (DocumentoExpediente docExp : documentos) {
			docExp.setEnEdicion(false);
			em.merge(docExp);
		}
		em.merge(expediente);
	}

	// @In(create = true)
	// private Renderer renderer;

	private static final String EMAIL_SENDER = "exedoc@exedoc.cl";

	@Out(required = false, scope = ScopeType.SESSION)
	private List<Expediente> expedientesMail;

	@Out(required = false, scope = ScopeType.SESSION)
	private DestinatariosFirmaDocumentoMail dataMailFirma;

	// @Out(required = false, scope = ScopeType.SESSION)
	// private List<Persona> destMailFirmas;

	 @Out(required = false, scope = ScopeType.SESSION)
	 private DatosSolicitud datosSolicitud;

//	 @Override
//	 public void despacharNotificacionEstadoSolDiaAdm(DatosSolicitud datosSolic) {
//		 try {
//			 SendMailUsingAuthentication sm =
//			 SendMailUsingAuthentication.getSendMail();
//			 if (sm.getACTIVE()) {
//				 if (datosSolicitud != null) {
//					 datosSolicitud = new DatosSolicitud();
//				 }
//		
//				 datosSolicitud = datosSolic;
//				 String body = "";
//				
//				 if (datosSolicitud != null) {
//					 body = armaBodyNotificacionSolicitudDias(datosSolicitud);
//				 }
//		
//				 log.info("Email sending init Estado Solicitud Día Administrativo: ");
//				
////				 sm.setEmailFromAddress(EMAIL_SENDER_SUBDERE);
//				 sm.setEmailMsgTxt(body);
//				 sm.setEmailSubjectTxt("Notificación Estado Solicitud Día Administrativo");
//				 sm.setEmailList(new String[] {
//						 datosSolicitud.getDestinatario().getEmail() });
//				 sm.setFile(datosSolic.getResolucionPDF());
//				 sm.setFilename("resolucion.pdf");
//				 sm.sendMail();
//				
//				 log.info("Email sending end Estado Solicitud Día Administrativo: ");
//				 sm = null;
//			 }
//		 } catch (Exception ex) {
//			 log.info("Email sending failed: " + ex.getMessage());
//		 }
//		 datosSolicitud = null;
//	 }

	// public void despacharNotificacionEstadoSolVaca(DatosSolicitud datosSolic)
	// {
	// try {
	// SendMailUsingAuthentication sm =
	// SendMailUsingAuthentication.getSendMail();
	// if (sm.getACTIVE()) {
	// if (datosSolicitud != null) {
	// datosSolicitud = new DatosSolicitud();
	// }
	//
	// datosSolicitud = datosSolic;
	// String body = "";
	//
	// if (datosSolicitud != null) {
	// body = armaBodyNotificacionSolicitudDiasVacaciones(datosSolicitud);
	// }
	//
	// log.info("Email sending init Estado Solicitud Vacaciones: ");
	//
	// sm.setEmailFromAddress(EMAIL_SENDER_SUBDERE);
	// sm.setEmailMsgTxt(body);
	// sm.setEmailSubjectTxt("Notificación Estado Solicitud Vacaciones");
	// sm.setEmailList(new String[] {
	// datosSolicitud.getDestinatario().getEmail() });
	// sm.setFile(datosSolic.getResolucionPDF());
	// sm.setFilename("resolucion.pdf");
	// sm.sendMail();
	//
	// log.info("Email sending end Estado Solicitud Vacaciones: ");
	// sm = null;
	// }
	// } catch (Exception ex) {
	// log.info("Email sending failed: " + ex.getMessage());
	// }
	//
	// datosSolicitud = null;
	// }

//	@Override
//	public void despacharNotificacionPorEmail(final List<Expediente> expedientes) {
//		try {
//			SendMailUsingAuthentication sm = SendMailUsingAuthentication.getSendMail();
//			if (sm.getACTIVE()) {
//				if (expedientesMail != null) {
//					expedientesMail.clear();
//				}
//
//				expedientesMail = expedientes;
//
//				for (Expediente expediente : expedientesMail) {
//					final String body = this.armaBodyNotificacionDespacho(expediente);
//
//					log.info("Email sending init Despacho Expediente: " + " " + expediente.getId() + " "
//							+ expediente.getNumeroExpediente());
//
//					sm.setEmailFromAddress(EMAIL_SENDER);
//					sm.setEmailMsgTxt(body);
//					sm.setEmailSubjectTxt("Notificacion Nuevo Expediente");
//					sm.setEmailList(new String[] { expediente.getDestinatario().getEmail() });
//					sm.sendMail();
//
//					log.info("Email sending end Despacho Expediente: ");
//				}
//				sm = null;
//			}
//		} catch (Exception ex) {
//			log.info("Error al enviar mail: " + ex.getMessage());
//			ex.printStackTrace();
//			for (Expediente expediente : expedientesMail) {
//				log.info("Fallo Email: " + " " + expediente.getId() + " " + expediente.getNumeroExpediente() + " "
//						+ expediente.getDestinatario().getEmail());
//			}
//
//			expedientesMail = null;
//		}
//	}

//	@Override
//	public void firmarNotificacionPorEmail(final DestinatariosFirmaDocumentoMail destMailFirma) {
//		try {
//			SendMailUsingAuthentication sm = SendMailUsingAuthentication.getSendMail();
//			if (sm.getACTIVE()) {
//				final Set<Persona> listP = destMailFirma.getDestinatarios();
//				this.dataMailFirma.setDocExp(destMailFirma.getDocExp());
//				this.dataMailFirma.setFirma(destMailFirma.getFirma());
//				for (Persona p : listP) {
//					if (p != null && p.getId() > 0) {
//						final String body = armaBodyNotificacionFirma(p);
//
//						log.info("Email sending init Notificacion Firma: ");
//
//						sm.setEmailFromAddress(EMAIL_SENDER);
//						sm.setEmailMsgTxt(body);
//						sm.setEmailSubjectTxt("Notificacion Firma Documento");
//						sm.setEmailList(new String[] { p.getEmail() });
//						sm.sendMail();
//
//						log.info("Email sending end Notificacion Firma: ");
//					}
//				}
//				sm = null;
//			}
//		} catch (Exception ex) {
//			log.info("Email sending failed: " + ex.getMessage());
//		}
//
//		dataMailFirma = null;
//	}

	/**
	 * Metodo que arma el body para enviar correo.
	 * 
	 * @param expediente {@link Expediente}
	 * @return {@link String}
	 */
	private String armaBodyNotificacionDespacho(final Expediente expediente) {
		final DateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY_HH_MM);
		String body = "Estimado(a) " + expediente.getDestinatario().getNombreApellido() + ",\nCon Fecha: "
				+ dateFormat.format(expediente.getFechaIngreso()) + "\nha recibido un nuevo expediente de: "
				+ expediente.getEmisor().getNombreApellido() + ", N° " + expediente.getNumeroExpediente() + "\n\n"
				+ "Documentos asociados:\n\n";

		String strDoc = "";
		for (DocumentoExpediente docExp : expediente.getDocumentos()) {
			strDoc = strDoc
					+ "Documento: "
					+ docExp.getDocumento().getTipoDocumento().getDescripcion()
					+ " "
					+ ((docExp.getDocumento().getNumeroDocumento() == null) ? "Sin numero" : docExp.getDocumento()
							.getNumeroDocumento())
					+ " "
					+ ((docExp.getDocumento().getEstado() == null) ? "Sin estado" : docExp.getDocumento().getEstado()
							.getDescripcion()) + " " + docExp.getDocumento().getAutor().getNombreApellido() + "\n";
		}
		body += strDoc;

		body += "\n\nEnviado por EXEDoc\n\n";

		return body;
	}

	private String armaBodyNotificacionFirma(final Persona destinatario) {
		final DateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY_HH_MM);

		String body = "Estimado(a)"
				+ destinatario.getNombreApellido()
				+ ",\nCon Fecha: "
				+ dateFormat.format(this.dataMailFirma.getFirma().getFechaFirma())
				+ "\nse ha firmado el documento: "
				+ dataMailFirma.getDocExp().getDocumento().getTipoDocumento().getDescripcion()
				+ ", N° "
				+ ((dataMailFirma.getDocExp().getDocumento().getNumeroDocumento() == null) ? "Sin numero"
						: dataMailFirma.getDocExp().getDocumento().getNumeroDocumento()) + ", N° Exp "
				+ dataMailFirma.getDocExp().getExpediente().getNumeroExpediente() + " firmado por: "
				+ dataMailFirma.getFirma().getPersona().getNombreApellido() + ".\n";
		
		body += "\n\nEnviado por EXEDoc\n\n";
		
		return body;
	}

	 private String armaBodyNotificacionSolicitudDias(DatosSolicitud datosSolic) {
		 DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		 DateFormat dateFormatParteDia = new SimpleDateFormat("dd/MM/yyyy");
		 String body = "Estimado(a)" + datosSolic.getDestinatario().getNombreApellido() + ",\nCon Fecha: " +
				 dateFormat.format(datosSolic.getFechaIngreso()) + "\n,se ha " + 
				 datosSolic.getEstadoSolicitud() + " la solicitud de " +
				 datosSolic.getTipoSolicitud() + "." +
				 "\nDicha solicitud se realizó por los siguientes días:\n\n";
		
		 for (DetalleDias dd : datosSolic.getListDetalleDias()) {
			 body += "Día Solicitado: " + dateFormatParteDia.format(dd.getFechaDia())
					 + ", parte del día: " + DetalleDias.getParteDia(dd.getParteDia()) +
					 ".\n";
		 }
		 body += "\n\nEnviado por EXEDoc\n\n";
		 return body;
	 }

	// private String armaBodyNotificacionSolicitudDiasVacaciones(DatosSolicitud
	// datosSolic) {
	// DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	// DateFormat dateFormatParteDia = new SimpleDateFormat("dd/MM/yyyy");
	// String body = "Estimado(a)" +
	// datosSolic.getDestinatario().getNombreApellido() + ",\nCon Fecha: " +
	// dateFormat.format(datosSolic.getFechaIngreso()) + "\n,se ha "
	// + datosSolic.getEstadoSolicitud() + " la solicitud de " +
	// datosSolic.getTipoSolicitud() + "\nque contempla desde el día " +
	// dateFormatParteDia.format(datosSolic.getFechaInicioDias())
	// + " hasta el día " +
	// dateFormatParteDia.format(datosSolic.getFechaTerminoDias()) + ".\n\n";
	//
	// body += "\n\nEnviado por SUBDERE exedoc\n\n" +
	// "Para ingresar al Sistema, siga este link: http://192.168.3.180:8080/exedoc";
	//
	// return body;
	// }

	@Override
	public DestinatariosFirmaDocumentoMail obtenerDatosNotificacion(final Documento documento,
			final String numeroExpediente) {
		final Documento docBuscado = documento;
		// manejarDocumento.buscarDocumento(idDocumento);
		final List<Expediente> listDocExp = this.obtenerExpedientes(numeroExpediente);
		// obtenerExpedientesAsocDocumento(docBuscado);

		final Set<Persona> destinatarios = this.obtenerDestinatariosNotificacionMail(listDocExp);

		final DocumentoExpediente docExp = new DocumentoExpediente();
		docExp.setDocumento(docBuscado);
		docExp.setExpediente(listDocExp.get(0));

		final DestinatariosFirmaDocumentoMail destMail = new DestinatariosFirmaDocumentoMail();
		destMail.setDestinatarios(destinatarios);
		destMail.setDocExp(docExp);
		destMail.setFirma(docBuscado.getFirmas().get(0));
		return destMail;
	}

	/**
	 * Metodo que retorna lista de destinatarios para notificacion por email.
	 * 
	 * @param listDocExp {@link List} of {@link Expediente}
	 * @return {@link Set} of {@link Persona}
	 */
	private Set<Persona> obtenerDestinatariosNotificacionMail(final List<Expediente> listDocExp) {
		final Set<Persona> destinatarios = new HashSet<Persona>();
		for (Expediente e : listDocExp) {
			destinatarios.add(e.getEmisor());
			destinatarios.add(e.getEmisorHistorico());
			destinatarios.add(e.getDestinatario());
			destinatarios.add(e.getDestinatarioHistorico());
		}
		return destinatarios;
	}

	/*
	 * @SuppressWarnings("unchecked") private List<Expediente> obtenerExpedientesAsocDocumento(Documento doc) {
	 * StringBuilder jpQL = new StringBuilder(
	 * "SELECT e FROM Expediente e where e.id in (SELECT de.expediente.id from DocumentoExpediente de where de.documento.id= "
	 * + doc.getId() + ")"); Query query = em.createQuery(jpQL.toString()); return (List<Expediente>)
	 * query.getResultList(); }
	 */

//	@Override
//	public void enviarMailUsuarioExtero(final String nombres, final Date fechaFirma, final String numeroDocumento,
//			final String codigoDescarga, final String email) {
//		final SendMailUsingAuthentication sm = SendMailUsingAuthentication.getSendMail();
//
//		final String body = this.armarBodyMailUsuarioExterno(nombres, fechaFirma, numeroDocumento, codigoDescarga);
//
//		sm.setEmailFromAddress(EMAIL_SENDER);
//		sm.setEmailMsgTxt(body);
//		sm.setEmailSubjectTxt("Envio Codigo Descarga");
//		sm.setEmailList(new String[] { email });
//		try {
//			sm.sendMail();
//		} catch (Exception e) {
//			log.error("Fallo en el envio del mail: " + e.getMessage());
//			e.printStackTrace();
//		}
//
//	}

	/**
	 * Metodo que arma body para email de usuario externo.
	 * 
	 * @param nombres {@link String}
	 * @param fechaFirma {@link Date}
	 * @param numDoc {@link String}
	 * @param codigoDescarga {@link String}
	 * @return {@link String}
	 */
	private String armarBodyMailUsuarioExterno(final String nombres, final Date fechaFirma, final String numDoc,
			final String codigoDescarga) {
		final DateFormat dateFormat = new SimpleDateFormat(DD_MM_YYYY_HH_MM);
		final StringBuilder body = new StringBuilder("Estimado(a) " + nombres + ", \n");
		body.append("con fecha " + dateFormat.format(fechaFirma) + " \n");
		body.append("se ha firmado el documento " + numDoc + ".\n\n");

		body.append("Se ha generado ademas el siguiente codigo de descarga para el documento: \n");
		body.append(codigoDescarga + "\n\n");

		body.append("Puede usar este codigo conjuntamente con el numero del documento \n");
		body.append("para descargar el documento a traves del sisema VDI, \n");
		body.append("http://localhost:8080/exedoc, opcion \"Descarga de Documentos\"");

		return body.toString();
	}

	/**
	 * Metodo que retorna Lista de expedientes, segun numero de expediente.
	 * 
	 * @param numeroExpediente {@link String}
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private List<Expediente> obtenerExpedientes(final String numeroExpediente) {
		final StringBuilder jpQL = new StringBuilder(10);
		jpQL.append("SELECT e FROM Expediente e where e.numeroExpediente = :numeroExpediente ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter("numeroExpediente", numeroExpediente);
		return (List<Expediente>) query.getResultList();
	}

	@Override
	public void acusarReciboExpediente(final Expediente expediente, final Persona persona) {
		acusarReciboExpediente(expediente, persona, false);
	}
	
	@Override
	public void acusarReciboExpediente(final Expediente expediente, final Persona persona, final boolean grupo) {
		expediente.setFechaAcuseRecibo(new Date());
		expediente.setFechaDespachoHistorico(expediente.getFechaDespacho());
		expediente.setEmisorHistorico(expediente.getEmisor());
		expediente.setEmisor(persona);
		if (grupo){
			expediente.setDestinatarioHistorico(persona);
			expediente.setGrupoHistorico(expediente.getGrupo());
			expediente.setCopia(false);
			expediente.setVersion(actualizarVersion(expediente));
		}else{
			expediente.setDestinatarioHistorico(expediente.getDestinatario());
		}
		expediente.setFechaDespacho(null);
		expediente.setDestinatario(null);
		expediente.setGrupo(null);
		em.merge(expediente);
	}

	/**
	 * @param expediente {@link Expediente}
	 * @param unidad {@link UnidadOrganizacional}
	 */
//	private void borrarExpedientesDestinatarioConRecepcion(final Expediente expediente,
//			final UnidadOrganizacional unidad) {
//		final Expediente expedientePadre = expediente.getVersionPadre();
//		final List<Expediente> expedientesHijos = expedientePadre.getExpedientesHijos();
//		for (Expediente expedienteHijo : expedientesHijos) {
//			if (!expedienteHijo.getId().equals(expediente.getId())) {
//				if (expedienteHijo.getDestinatario() != null
//						&& expedienteHijo.getDestinatario().getCargo().getUnidadOrganizacional().equals(unidad)) {
//					if (expedienteHijo.getDestinatarioConRecepcion()) {
//						em.remove(expedienteHijo);
//					}
//				}
//			}
//		}
//	}


	/**
	 * Metodo que des despacha expediente.
	 * 
	 * @param expediente {@link Expediente}
	 */
	public void desDespacharExpediente(final Expediente expediente) {
		expediente.setFechaDespacho(null);
		// TODO no puedo recuperar los documentos que estaban en Edicion.
		em.merge(expediente);
	}

	// public DatosSolicitud getDatosSolicitud() {
	// return datosSolicitud;
	// }
	//
	// public void setDatosSolicitud(DatosSolicitud datosSolicitud) {
	// this.datosSolicitud = datosSolicitud;
	// }

	// private Expediente abrirListas(Expediente expediente) {
	// for (DocumentoExpediente de : expediente.getDocumentos()) {
	// if (de.getDocumento() instanceof DocumentoElectronico) {
	// DocumentoElectronico documentoOriginal = (DocumentoElectronico)
	// de.getDocumento();
	// documentoOriginal.getParrafos().size();
	//
	// if (documentoOriginal instanceof Resolucion) {
	// ((Resolucion) documentoOriginal).getArchivosAdjuntos().size();
	// }
	// if (documentoOriginal instanceof Decreto) {
	// ((Decreto) documentoOriginal).getArchivosAdjuntos().size();
	// }
	// documentoOriginal.getDistribucionDocumento().size();
	// documentoOriginal.getVisaciones().size();
	// documentoOriginal.getFirmas().size();
	// documentoOriginal.getBitacoras().size();
	//
	// }
	// de.getDocumento().getFirmas().size();
	// de.getDocumento().getVisaciones().size();
	// de.getDocumento().getFirmasEstructuradas().size();
	// de.getDocumento().getVisacionesEstructuradas().size();
	// de.getDocumento().getBitacoras().size();
	// de.getDocumento().getDestinatarios().size();
	// de.getDocumento().getDistribucionDocumento().size();
	// de.getDocumento().getListaPersonas().size();
	// }
	// return expediente;
	// }
	/**
	 * Metodo que retorna Lista de expedientes, segun numero de expediente.
	 * 
	 * @param numeroExpediente {@link String}
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	public List<Expediente> obtenerExpedientesHijos(Expediente numeroExpediente) {
		if (numeroExpediente != null) {
			final StringBuilder jpQL = new StringBuilder();
			jpQL.append("SELECT e FROM Expediente e ");
			jpQL.append("WHERE e.versionPadre = ? ");
			jpQL.append(" and e.cancelado is null");
			final Query query = em.createQuery(jpQL.toString());
			query.setParameter(1, numeroExpediente);
			return (List<Expediente>) query.getResultList();
		} else {
			return new ArrayList<Expediente>();
		}
	}

	@Override
	public void eliminarDespacho(final Expediente expediente) {
		expediente.setVersion(this.actualizarVersion(expediente));
		expediente.setFechaDespacho(new Date());
		final List<DocumentoExpediente> documentos = expediente.getDocumentos();

		if (documentos != null) {
			for (DocumentoExpediente de : documentos) {
				if (de.getEnEdicion()) {
					de.setEnEdicion(false);
					em.merge(de);
				}
			}
		}
		em.merge(expediente);

		this.borrarExpedienteDeCache(expediente);
	}

	@SuppressWarnings("unchecked")
	public List<Expediente> obtienePrimerExpediene(final Expediente expediente) {
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT e FROM Expediente e ");
		jpQL.append("WHERE e.numeroExpediente = ? ");
		jpQL.append(" and e.cancelado is null ");
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, expediente.getNumeroExpediente());
		return (List<Expediente>) query.getResultList();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Persona> buscarDestinatarios(List<Expediente> expedientes) {
		// List<Expediente> expedientes = me.buscarExpediente(this.expediente,
		// false);
		List<Persona> listDestinatarios = new ArrayList();
		for (Expediente e : expedientes) {
			if (e.getDestinatario() != null) {
				listDestinatarios.add(e.getDestinatario());
			} else {
				listDestinatarios.add(e.getEmisor());
			}

		}
		return listDestinatarios;
	}

	public String convertStreamToString(InputStream is) throws IOException {
		/*
		 * 33.* To convert the InputStream to String we use the 34.* Reader.read(char[] buffer) method. We iterate until
		 * the 35.* Reader return -1 which means there's no more data to 36.* read. We use the StringWriter class to
		 * produce the string. 37.
		 */
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}

	}
	
	/**
	 * Metodo que retorna Bandeja Compartida.
	 * @author: SdelaF
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaCompartida}
	 */
	@SuppressWarnings("unchecked")
	public List<ExpedienteBandejaCompartida> listarExpedienteBandejaCompartidaUsuario(final Persona persona) {

		//SELECT e FROM Expediente e left join e.grupo g join g.usuarios u where u.id = 0
		
		
		List<ExpedienteBandejaCompartida> lista = this.buscarBandejaCompartidaDesdeCache(persona);
		if (lista == null) {
//			final StringBuilder jpQL = new StringBuilder(
//					"SELECT e ");
//			jpQL.append(" FROM Expediente e join e.grupo g joi ");
//			jpQL.append(" WHERE e.archivado is null and ");
//			jpQL.append(" ((e.fechaDespacho is not null and e.fechaAcuseRecibo is null and eliminado <> true )");
//			jpQL.append(" or (e.emisor.id = " + persona.getId() 
//					+ " and e.fechaDespacho is null and e.fechaAcuseRecibo is not null)) ");
//			jpQL.append(" and e.copia = false  ");
//			jpQL.append(" order by e.fechaIngreso");
			
			final StringBuilder jpQL = new StringBuilder(
					"SELECT e.id, e.numeroExpediente, e.fechaIngreso, (case when e.fechaAcuseRecibo is null then false else true end), e.version, e.copia, ");
			jpQL.append("g.nombre , '" + persona.getNombres() + "' as nombreapellido ");
			jpQL.append(" FROM Expediente e join e.grupo g join g.usuarios u where u.id =" + persona.getId() + " and e.cancelado is null ");

			final Query query = em.createQuery(jpQL.toString());
			final List<Object[]> expedientes = query.getResultList();
			lista = this.armaBandejaCompartida(expedientes, false);
			this.almacenarBandejaCompartidaEnCache(persona, lista);
		}
		return lista;
	}
	
	/**
	 * buscar Bandeja Compartida Desde Cache.
	 * 
	 * @param usuario {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaCompartida}
	 */
	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaCompartida> buscarBandejaCompartidaDesdeCache(final Persona usuario) {

		try {
			final String idUsuario = usuario.getId().toString();
			log.info("buscando bandeja compartida, id usuario = #0", idUsuario);
			final List<ExpedienteBandejaCompartida> list = (List<ExpedienteBandejaCompartida>) pojoCache.get(
					"bandejaCompartida", idUsuario);
			if (list != null) {
				log.info("bandeja compartida recuperada desde cache, usuario #0", idUsuario);
			}
			return list;
		} catch (CacheException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException e) {
			return null;
		}

	}
	
	/**
	 * almacenar Bandeja Compartida En Cache.
	 * 
	 * @param usuario {@link Persona}.
	 * @param lista {@link List} of {@link ExpedienteBandejaCompartida}
	 */
	private void almacenarBandejaCompartidaEnCache(final Persona usuario, 
								final List<ExpedienteBandejaCompartida> lista) {
		try {
			if (lista.size() > CACHE_SIZE) {
				final String idUsuario = usuario.getId().toString();
				log.info("bandeja compartida almacenada en cache, usuario: #0", idUsuario);
				pojoCache.put("bandejaCompartida", idUsuario, lista);
			}
		} catch (CacheException ex) {
			throw new RuntimeException(ex);
		} catch (NullPointerException ex) {
		}

	}

	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaCompartida> armaBandejaCompartida(final List<Object[]> expedientes,
			final boolean soloVencidos) {
		final List<ExpedienteBandejaCompartida> ebeList = new ArrayList<ExpedienteBandejaCompartida>();

		final Query qremi1 = em.createQuery("select e.emisorHistorico.nombres || ' ' || " +
								"e.emisorHistorico.apellidoPaterno, " +
								"e.emisorHistorico.cargo.unidadOrganizacional.departamento.descripcion " +
								"from Expediente e where e.id = ?");
		final Query qremi2 = em.createQuery("select e.emisor.nombres || ' ' || " +
								"e.emisor.apellidoPaterno, " +
								"e.emisor.cargo.unidadOrganizacional.departamento.descripcion " +
								"from Expediente e where e.id = ?");

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo, doc.id ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		final StringBuilder sqdoc = new StringBuilder();
		sqdoc.append("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		sqdoc.append("	doc.reservado, doc.materia, td.descripcion, doc.emisor, ");
		sqdoc.append("	doc.plazo, fd.descripcion, td.id, a.id ");
		sqdoc.append("from Documento doc ");
		sqdoc.append("	left join doc.tipoDocumento td ");
		sqdoc.append("	left join doc.formatoDocumento fd ");
		sqdoc.append("	left join doc.alerta a ");
		sqdoc.append("where doc.id = ? ");
		final Query qryDoc = em.createQuery(sqdoc.toString());

		final String sqdest = "select de.destinatario from ListaPersonasDocumento de where de.documento.id = ?";
		final Query qDest = em.createQuery(sqdest);

		for (Object[] e : expedientes) {
			log.info("id Expediente {0}", (Long) e[0]);

			final ExpedienteBandejaCompartida ebe = new ExpedienteBandejaCompartida();
			ebe.setId((Long) e[0]);
			ebe.setNumeroExpediente((String) e[1]);
			ebe.setFechaIngreso((Date) e[2]);
			final Boolean recibido = (Boolean) e[3];
			ebe.setRecibido(recibido);
			ebe.setVersion((Integer) e[4]);
			final Boolean copia = (Boolean) e[5];
			ebe.setCopia(copia);
			final String destinatarioExp = (String) e[6];
			ebe.setNombreGrupo(destinatarioExp);
			final String destinatarioHistExp = (String) e[7];

			log.info("id Expediente : 1", (Long) e[0]);

			List<Object[]> remitente = null;
			if (recibido) {
				qremi1.setParameter(1, ebe.getId());
				 remitente = qremi1.getResultList();
			} else {
				qremi2.setParameter(1, ebe.getId());
				remitente = qremi2.getResultList();
			}
			ebe.setRemitente((String) remitente.get(0)[0]);
			ebe.setNombreFiscalia((String) remitente.get(0)[1]);
			
			
			qryDocs.setParameter(1, ebe.getId());
			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);
			log.info("qryDocs.setParameter : {0}", (Long) e[0]);

			final List<Object[]> qdocs = qryDocs.getResultList();
			log.info("qdocs {0}", qdocs.size());
			boolean respuesta = false;
			Long docId = null;
			Long docIdOriginal = null;
			boolean completado = true;
			final Date hoy = new Date();
			boolean encontrado = false;

			for (Object[] d : qdocs) {
				log.info("segundo for {0}", (Integer) d[0]);

				final Integer tipoDocumento = (Integer) d[0];
				final Boolean c = (Boolean) d[2];
				final Date plazo = (Date) d[3];
				if (completado && c != null && !c && plazo != null) {
					if (plazo.before(hoy)) {
						completado = false;
					}
				}
				if (!encontrado) {
					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
						docId = (Long) d[1];
						respuesta = true;
						encontrado = true;
					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
						docIdOriginal = (Long) d[1];
						encontrado = true;
					}
				}
				log.info("fin segundo for {0}", (Integer) d[0]);
			}

			if (!respuesta) {
				docId = docIdOriginal;
			}

			ebe.setCompletado(completado);

			qryDoc.setParameter(1, docId);
			try {
				final Object[] qdoc = (Object[]) qryDoc.getSingleResult();

				ebe.setNumeroDocumento((String) qdoc[0]);
				ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
				if (qdoc[2] != null) {
					ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
				}
				ebe.setTipoDocumento((String) qdoc[4]);
				ebe.setEmisor((String) qdoc[5]);
				ebe.setPlazo((Date) qdoc[6]);
				ebe.setFormatoDocumento((String) qdoc[7]);
				final Integer idTipoDoc = (Integer) qdoc[8];

				try {
					final Long idAlerta = (Long) qdoc[9];
					ebe.setMensajeAlerta(administradorAlertas.validarPlazo(idAlerta, ebe.getPlazo()));
				} catch (NullPointerException ex) {
					log.error("Error: ", e);
				}

				final StringBuilder sb = new StringBuilder();

				if (idTipoDoc != null && idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
					if (destinatarioExp != null) {
						sb.append(destinatarioExp);
						sb.append(" ");
					} else {
						if (destinatarioHistExp != null) {
							sb.append(destinatarioHistExp);
							sb.append(" ");
						}
					}
				} else {
					qDest.setParameter(1, docId);
					final List<String> sDestinatarios = qDest.getResultList();
					for (String dd : sDestinatarios) {
						sb.append(dd);
						sb.append(" ");
					}
				}
				if (sb.length() != 0) {
					ebe.setDestinatario(sb.toString());
				}

				if (!soloVencidos) {
					ebeList.add(ebe);
				} else if (!completado) {
					ebeList.add(ebe);
				}

			} catch (NoResultException ex) {
				log.error("no encontro resultados...", ex);
			}
			log.info("fin for Expediente {0}", (Long) e[0]);
		}

		return ebeList;
	}
	
//	@Override
//	public void acusarReciboExpedienteCompartido(final Expediente expediente, final Persona persona) {
//		if (this.verificarAcuse(expediente)) {
//			this.acusarReciboExpediente(expediente, persona, true);
//		} else {
//			FacesMessages.instance().add("Expediente ya fue tomado.");
//		}
//	}
//
//	/**
//	 * Metodo que verifica si expediente grupal ya ha sido tomado.
//	 * @param expediente
//	 * @return
//	 */
//	private boolean verificarAcuse(Expediente expediente) {
//		final Query query = em.createQuery("select e.grupo.id from Expediente e where e.id = :id");
//		query.setParameter("id", expediente.getId());
//		final Long o = (Long) query.getSingleResult(); 
//		return o != null;
//	}
	public boolean estaEspediente(List<ExpedienteBandejaEntrada> listExpedientes, String numeroExpediente){
		boolean esta = false;
		
		for (ExpedienteBandejaEntrada bandeja : listExpedientes) {
			if(bandeja.getNumeroExpediente().equals(numeroExpediente)) return true;
		}
		
		return esta;
	}
	
	@SuppressWarnings("unchecked")
	private List<ExpedienteBandejaEntrada> armaBandejaEntradaProcedimiento(final boolean soloVencidos) {
		final List<ExpedienteBandejaEntrada> ebeList = new ArrayList<ExpedienteBandejaEntrada>();

		final Query qremi1 = em
				.createQuery("select e.emisorHistorico.nombres || ' ' || e.emisorHistorico.apellidoPaterno from Expediente e where e.id = ?");
		final Query qremi2 = em
				.createQuery("select e.emisor.nombres || ' ' || e.emisor.apellidoPaterno from Expediente e where e.id = ?");
//		final Query qremi3 = em
//				.createQuery("select e.grupo.nombre from Expediente e where e.id = ?");

		final StringBuilder sqexp = new StringBuilder(
				"select de.tipoDocumentoExpediente.id, doc.id, doc.completado, doc.plazo, doc.id ");
		sqexp.append("from Documento doc, DocumentoExpediente de ");
		sqexp.append("where  doc.id = de.documento.id and de.expediente.id = ? ");
		sqexp.append("and (de.tipoDocumentoExpediente.id = ? or de.tipoDocumentoExpediente.id = ?)  ");
		sqexp.append("order by doc.fechaCreacion desc");
		final Query qryDocs = em.createQuery(sqexp.toString());

		// final StringBuilder sqdoc = new
		// StringBuilder("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		// sqdoc.append("doc.reservado, doc.materia, doc.tipoDocumento.descripcion, doc.emisor, ");
		// sqdoc.append("doc.plazo, doc.formatoDocumento.descripcion, doc.tipoDocumento.id, doc.alerta.id ");
		// sqdoc.append("from Documento doc ");
		// sqdoc.append("where doc.id = ? ");

		final StringBuilder sqdoc = new StringBuilder();
		sqdoc.append("select doc.numeroDocumento, doc.fechaDocumentoOrigen, ");
		sqdoc.append("	doc.reservado, doc.materia, td.descripcion, doc.emisor, ");
		sqdoc.append("	doc.plazo, fd.descripcion, td.id, a.id ");
		sqdoc.append("from Documento doc ");
		sqdoc.append("	left join doc.tipoDocumento td ");
		sqdoc.append("	left join doc.formatoDocumento fd ");
		sqdoc.append("	left join doc.alerta a ");
		sqdoc.append("where doc.id = ? ");
		final Query qryDoc = em.createQuery(sqdoc.toString());

		final String sqdest = "select de.destinatario from ListaPersonasDocumento de where de.documento.id = ?";
		final Query qDest = em.createQuery(sqdest);

		//######################################
		
		
		 final StringBuilder sqlNatBE = new StringBuilder(
         		"SELECT e.id, e.numero_Expediente, e.fecha_Ingreso, e.fecha_acuse_Recibo, e.version, e.copia, ");
	 	sqlNatBE.append(" (p.nombres || ' ' || p.apellido_Paterno) as nombreapellido, (ph.nombres || ' ' || ph.apellido_Paterno) as nombreapellido_h ");
//		sqlNatBE.append(", i.dinstrucciones ");
	 	sqlNatBE.append("FROM expedientes e left join personas p on e.id_destinatario = p.id ");
	 	sqlNatBE.append("left join personas ph on e.id_destinatario_Historico = ph.id ");
//		sqlNatBE.append("left join (");
//		sqlNatBE.append("select id_expediente, wm_concat(id_instruccion) as dinstrucciones ");
//		sqlNatBE.append("from instrucciones_expedientes group by id_expediente) i on i.id_expediente = e.id ");
		sqlNatBE.append("WHERE e.archivado is null and (");
		sqlNatBE.append("(e.id_destinatario = " + usuario.getId() + " and e.fecha_Despacho is not null and e.fecha_acuse_Recibo is null )");
		sqlNatBE.append("or (e.id_emisor = " + usuario.getId() + " and e.fecha_Despacho is null and e.fecha_Acuse_Recibo is not null)) ");
		sqlNatBE.append(" and e.copia = '0' ");
		sqlNatBE.append(" and e.cancelado is null ");
		// new condition
		sqlNatBE.append("and e.reasignado is null ");
		sqlNatBE.append("order by e.fecha_Ingreso");
		final Query queryBE = em.createNativeQuery(sqlNatBE.toString());
		final List<Object[]> expedientesBE = queryBE.getResultList();
		
		for (Object[] e : expedientesBE) {
//			log.info("id Expediente {0}", e[0].toString());

//		List<Expediente> Temp = new ArrayList<Expediente>();
			//listado desde el procedimiento de entrada
//			 String mySQLCallentrada = "{?=call EXE_DOC_BE.fn_bdja_entda(?)}";
//	        ResultSet rS = null;
//	        CallableStatement cS = null;
//	        Connection connectionS = null;
//		        
//	        try {
//	            connectionS = DaoFactory.getDaoFactoryConnection();
//	            cS = connectionS.prepareCall(mySQLCallentrada);
//
//	            cS.registerOutParameter(1, OracleTypes.CURSOR);
//	            int usrID = usuario.getId().intValue();
//	            cS.setInt(2, usrID);
//	            cS.executeQuery(); 
//	            rS = (ResultSet) cS.getObject(1);
//	            
//	            while(rS.next()){
//	            	long id = rS.getLong(1);
	            	//System.out.println(id+" "+rS.getString("NUMERO_EXPEDIENTE")+" "+rS.getTimestamp("FECHA_INGRESO")+" "+rS.getBoolean("ACUSE_RECIBO")+" "+rS.getInt("VERSION")+" "+rS.getBoolean("COPIA")+" "+rS.getString("nombreapellido"));
	            	//Temp.add(me.buscarExpediente(id))
	            	//log.info("id Expediente {0}", (Long) e[0]);
			
			//if(!estaEspediente(ebeList, e[1].toString())) {
				
				final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
    			ebe.setId(Long.valueOf(e[0].toString()));
    			ebe.setNumeroExpediente(e[1].toString());
    			ebe.setFechaIngreso((Date) e[2]);
    			
    			Boolean recibido;
    			Boolean copia;
    			recibido = false;
    			if (e[3] != null) {
    				recibido = true;
    			}
//    			if (e[3] instanceof java.lang.Boolean)
//    				recibido = Boolean.valueOf(e[3].toString());
//				else recibido = Long.valueOf(e[3].toString()) == 1 ? true : false;
    			
//    			final Boolean recibido = Long.valueOf(e[3].toString()) == 1 ? true : false;
    			
    			ebe.setRecibido(recibido);
    			ebe.setVersion(Integer.valueOf(e[4].toString()));
//		    	ebe.setInstruccion(rS.getString("dinstrucciones"));
//    			final Boolean copia = Long.valueOf(e[5].toString()) == 1 ? true : false;
    			
    			if (e[5] instanceof java.lang.Boolean)
    				copia = Boolean.valueOf(e[5].toString());
				else copia = Long.valueOf(e[5].toString()) == 1 ? true : false;
    			
    			ebe.setCopia(copia);
    			final String destinatarioExp = e[6] != null ? e[6].toString() : "";
    			final String destinatarioHistExp = e[7] != null ? e[7].toString() : "";

    			try {
					if (recibido) {
						qremi1.setParameter(1, ebe.getId());
						ebe.setRemitente((String) qremi1.getSingleResult());
					} else {
						qremi2.setParameter(1, ebe.getId());
						ebe.setRemitente((String) qremi2.getSingleResult());
					}
				} catch (NoResultException e1) {
					System.out.println("Error al obtener Remitente. Error:" + e1.getMessage());
				}
    			
    			qryDocs.setParameter(1, ebe.getId());
    			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
    			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);
    			//log.info("qryDocs.setParameter : {0}", (Long) e[0]);

    			final List<Object[]> qdocs = qryDocs.getResultList();
    			//log.info("qdocs {0}", qdocs.size());
    			boolean respuesta = false;
    			Long docId = null;
    			Long docIdOriginal = null;
    			boolean completado = true;
    			final Date hoy = new Date();
    			boolean encontrado = false;

    			for (Object[] d : qdocs) {
    				//log.info("segundo for {0}", (Integer) d[0]);

    				final Integer tipoDocumento = (Integer) d[0];
    				final Boolean c = (Boolean) d[2];
    				final Date plazo = (Date) d[3];
    				if (completado && c != null && !c && plazo != null) {
    					if (plazo.before(hoy)) {
    						completado = false;
    					}
    				}
    				if (!encontrado) {
    					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
    						docId = (Long) d[1];
    						respuesta = true;
    						encontrado = true;
    					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
    						docIdOriginal = (Long) d[1];
    						encontrado = true;
    					}
    				}
    				//log.info("fin segundo for {0}", (Integer) d[0]);
    			}

    			if (!respuesta) {
    				docId = docIdOriginal;
    			}

    			ebe.setCompletado(completado);

    			qryDoc.setParameter(1, docId);
    			try {
    				final Object[] qdoc = (Object[]) qryDoc.getSingleResult();

    				ebe.setNumeroDocumento((String) qdoc[0]);
    				ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
    				if (qdoc[2] != null) {
    					ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
    				}
    				ebe.setTipoDocumento((String) qdoc[4]);
    				ebe.setEmisor((String) qdoc[5]);
    				ebe.setPlazo((Date) qdoc[6]);
    				ebe.setFormatoDocumento((String) qdoc[7]);
    				final Integer idTipoDoc = (Integer) qdoc[8];

    				try {
    					final Long idAlerta = (Long) qdoc[9];
		    					if(idAlerta != null && ebe.getPlazo() != null)
    					ebe.setMensajeAlerta(administradorAlertas.validarPlazo(idAlerta, ebe.getPlazo()));
		    					else
		    						ebe.setMensajeAlerta("");
    				} catch (NullPointerException ex) {
    					//log.error("Error: ", e);
    				}

    				final StringBuilder sb = new StringBuilder();

    				if (
    				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO) ||
    				// idTipoDoc.equals(TipoDocumento.SOLICITUD_VACACIONES) ||
    				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADQUISICION)
    				// ||
    				idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
    					if (destinatarioExp != null) {
    						sb.append(destinatarioExp);
    						sb.append(" ");
    					} else {
    						if (destinatarioHistExp != null) {
    							sb.append(destinatarioHistExp);
    							sb.append(" ");
    						}
    					}
    				} else {
    					qDest.setParameter(1, docId);
    					final List<String> sDestinatarios = qDest.getResultList();
    					for (String dd : sDestinatarios) {
    						sb.append(dd);
    						sb.append(" ");
    					}
    				}
    				if (sb.length() != 0) {
    					ebe.setDestinatario(sb.toString());
    				}

    				if (!soloVencidos) {
    					ebeList.add(ebe);
    					
    				} else if (!completado) {
    					ebeList.add(ebe);
    				}
    			

    			} catch (NoResultException ex) {
    				log.error("no encontro resultados...", ex);
    			}
    			//log.info("fin for Expediente {0}", (Long) e[0]);
        	//}
        }
		            
//		           connectionS.close();
//		           
//		        }catch(Exception ex){
//		        	try {
//						connectionS.close();
//					} catch (SQLException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//		        }
		        
			
			//######################################
		
		
//		for (Object[] e : expedientes) {
//			//log.info("id Expediente {0}", (Long) e[0]);
//
//			final ExpedienteBandejaEntrada ebe = new ExpedienteBandejaEntrada();
//			ebe.setId((Long) e[0]);
//			ebe.setNumeroExpediente((String) e[1]);
//			ebe.setFechaIngreso((Date) e[2]);
//			final Boolean recibido = (Boolean) e[3];
//			ebe.setRecibido(recibido);
//			ebe.setVersion((Integer) e[4]);
//			final Boolean copia = (Boolean) e[5];
//			ebe.setCopia(copia);
//			final String destinatarioExp = (String) e[6];
//			final String destinatarioHistExp = (String) e[7];
//
//			//log.info("id Expediente : 1", (Long) e[0]);
//
//			/*
//			 * if(archivado){ ebe.setFechaArchivado((Date) e[5]); }
//			 */
//
//			if (recibido) {
//				qremi1.setParameter(1, ebe.getId());
//				ebe.setRemitente((String) qremi1.getSingleResult());
//			} else {
//				qremi2.setParameter(1, ebe.getId());
//				ebe.setRemitente((String) qremi2.getSingleResult());
//			}
//
//			qryDocs.setParameter(1, ebe.getId());
//			qryDocs.setParameter(2, TipoDocumentoExpediente.RESPUESTA);
//			qryDocs.setParameter(3, TipoDocumentoExpediente.ORIGINAL);
//			//log.info("qryDocs.setParameter : {0}", (Long) e[0]);
//
//			final List<Object[]> qdocs = qryDocs.getResultList();
//			//log.info("qdocs {0}", qdocs.size());
//			boolean respuesta = false;
//			Long docId = null;
//			Long docIdOriginal = null;
//			boolean completado = true;
//			final Date hoy = new Date();
//			boolean encontrado = false;
//
//			for (Object[] d : qdocs) {
//				//log.info("segundo for {0}", (Integer) d[0]);
//
//				final Integer tipoDocumento = (Integer) d[0];
//				final Boolean c = (Boolean) d[2];
//				final Date plazo = (Date) d[3];
//				if (completado && c != null && !c && plazo != null) {
//					if (plazo.before(hoy)) {
//						completado = false;
//					}
//				}
//				if (!encontrado) {
//					if (tipoDocumento.equals(TipoDocumentoExpediente.RESPUESTA)) {
//						docId = (Long) d[1];
//						respuesta = true;
//						encontrado = true;
//					} else if (!respuesta && tipoDocumento.equals(TipoDocumentoExpediente.ORIGINAL)) {
//						docIdOriginal = (Long) d[1];
//						encontrado = true;
//					}
//				}
//				//log.info("fin segundo for {0}", (Integer) d[0]);
//			}
//
//			if (!respuesta) {
//				docId = docIdOriginal;
//			}
//
//			ebe.setCompletado(completado);
//
//			qryDoc.setParameter(1, docId);
//			try {
//				final Object[] qdoc = (Object[]) qryDoc.getSingleResult();
//
//				ebe.setNumeroDocumento((String) qdoc[0]);
//				ebe.setFechaDocumentoOrigen((Date) qdoc[1]);
//				if (qdoc[2] != null) {
//					ebe.setMateria((Boolean) qdoc[2] ? "Materia Confidencial" : (String) qdoc[3]);
//				}
//				ebe.setTipoDocumento((String) qdoc[4]);
//				ebe.setEmisor((String) qdoc[5]);
//				ebe.setPlazo((Date) qdoc[6]);
//				ebe.setFormatoDocumento((String) qdoc[7]);
//				final Integer idTipoDoc = (Integer) qdoc[8];
//
//				try {
//					final Long idAlerta = (Long) qdoc[9];
//					ebe.setMensajeAlerta(administradorAlertas.validarPlazo(idAlerta, ebe.getPlazo()));
//				} catch (NullPointerException ex) {
//					//log.error("Error: ", e);
//				}
//
//				final StringBuilder sb = new StringBuilder();
//
//				if (
//				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO) ||
//				// idTipoDoc.equals(TipoDocumento.SOLICITUD_VACACIONES) ||
//				// idTipoDoc.equals(TipoDocumento.SOLICITUD_ADQUISICION)
//				// ||
//				idTipoDoc.equals(TipoDocumento.RESOLUCION)) {
//					if (destinatarioExp != null) {
//						sb.append(destinatarioExp);
//						sb.append(" ");
//					} else {
//						if (destinatarioHistExp != null) {
//							sb.append(destinatarioHistExp);
//							sb.append(" ");
//						}
//					}
//				} else {
//					qDest.setParameter(1, docId);
//					final List<String> sDestinatarios = qDest.getResultList();
//					for (String dd : sDestinatarios) {
//						sb.append(dd);
//						sb.append(" ");
//					}
//				}
//				if (sb.length() != 0) {
//					ebe.setDestinatario(sb.toString());
//				}
//
//				if (!soloVencidos) {
//					ebeList.add(ebe);
//				} else if (!completado) {
//					ebeList.add(ebe);
//				}
//
//			} catch (NoResultException ex) {
//				log.error("no encontro resultados...", ex);
//			}
//			//log.info("fin for Expediente {0}", (Long) e[0]);
//		}

		return ebeList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Expediente buscarExpedientePadreOriginal(Expediente e){
		Query query = em.createQuery("select e from Expediente e where e.versionPadre is null and e.numeroExpediente like :num");
		query.setParameter("num", e.getNumeroExpediente());
		List<Expediente> expedientes = query.getResultList();
		if(expedientes.size()>0)
			return expedientes.get(0);
		return null;
	}
	
	/**
	 * Metodo que notifica que una resolución fue firmada
	 * 
	 */
	@Override
	public void enviarMailNotificacionFirma(Expediente expediente, DocumentoElectronico documento, List<Persona> destinatarios) {
		final Email email = new Email();
		String mensaje = new String();
		String result = new String();
		if (destinatarios.size() == 0) {
			return;
		}
		
		// cargar plantilla firmado
		String pl = Configuracion.getProperties().getProperty(AbstractConfiguracion.PLANTILLA_FIRMADO);
		InputStream resourceAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(pl);
		try {
			mensaje = convertStreamToString(resourceAsStream);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		result = MessageFormat.format(mensaje,(documento.getNumeroDocumento() != null ? documento.getNumeroDocumento() : "s/n"), expediente.getNumeroExpediente(),
				usuario.getNombreApellido(), df.format(new Date()));
		
		for (Persona destinatario : destinatarios) {
			final String form = destinatario.getEmail();
			
			if (form != null) {
				final Mail mail = new Mail();
			
				try {
					final StringBuilder asunto = new StringBuilder();
					final SimpleDateFormat format = new SimpleDateFormat("yyyy");
	
					asunto.append("Derivacion ");
					if (usuario.getNombreApellido() != null) {
						asunto.append("del " + TextUtil.eliminaTildes(usuario.getCargo().getDescripcion()));
					}
					
					asunto.append(" de la "+usuario.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion());
					asunto.append(" a su Unidad o Division, N ingreso ");
					asunto.append(expediente.getNumeroExpediente());
					asunto.append(" del ");
					asunto.append(format.format(new Date()));
					mail.setForm(new InternetAddress(form));
					mail.setSubject(asunto.toString());
					mail.setText(result);
				
					email.enviarMail(mail);
					
				} catch (AddressException e) {
					log.error("Error al enviar por correo electronico.", e);
				}
			}
		}
		FacesMessages.instance().add("La firma fue Notificada.");
	}
	
	
	public void transform(String inXML, OutputStream out)
			  throws TransformerConfigurationException,
			  TransformerException {
			  final String xsltSourceDecretoContraloria = "xslt/documentoElectronico.xsl";
			  TransformerFactory factory = TransformerFactory.newInstance();

			  
			 
			  FacesContext facesContext = FacesContext.getCurrentInstance();
			  ServletContext ctx = (ServletContext) facesContext.getExternalContext().getContext();
			  String templatePath = null;
			  templatePath = ctx.getRealPath(xsltSourceDecretoContraloria);
			  StreamSource xslStream = new StreamSource(new File(templatePath));
			  Transformer transformer = factory.newTransformer(xslStream);
			
			  StreamSource in = new StreamSource(inXML);
			  StreamResult result = new StreamResult(out);
			
			  transformer.transform(in,result);
			  System.out.println("The generated HTML file is:" + out.toString()+ " ooooo ");
			  }
			  
//	@Override
//	public void despacharNotificacionEstadoSolVaca(DatosSolicitud datosSolic) {
//		try {
//			SendMailUsingAuthentication sm = SendMailUsingAuthentication.getSendMail();
//
//			if (datosSolicitud != null) {
//				datosSolicitud = new DatosSolicitud();
//			}
//
//			datosSolicitud = datosSolic;
//			String body = "";
//
//			if (datosSolicitud != null) {
//				body = armaBodyNotificacionSolicitudDiasVacaciones(datosSolicitud);
//			}
//
//			log.info("Email sending init Estado Solicitud Vacaciones: ");
//
//			// sm.setEmailFromAddress(EMAIL_SENDER_SUBDERE);
//			sm.setEmailMsgTxt(body);
//			sm.setEmailSubjectTxt("Notificacion Estado Solicitud Vacaciones");
//			sm.setEmailList(new String[] { datosSolicitud.getDestinatario().getEmail() });
//			sm.setFile(datosSolic.getResolucionPDF());
//			sm.setFilename("resolucion.pdf");
//			sm.sendMail();
//
//			log.info("Email sending end Estado Solicitud Vacaciones: ");
//			sm = null;
//		} catch (Exception ex) {
//			log.info("Email sending failed: " + ex.getMessage());
//		}
//
//		datosSolicitud = null;
//	}
	
	private String armaBodyNotificacionSolicitudDiasVacaciones(DatosSolicitud datosSolic) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		DateFormat dateFormatParteDia = new SimpleDateFormat("dd/MM/yyyy");
		String body = "Estimado(a)" + datosSolic.getDestinatario().getNombreApellido() + ",\nCon Fecha: "
				+ dateFormat.format(datosSolic.getFechaIngreso()) + "\n,se ha " + datosSolic.getEstadoSolicitud()
				+ " la solicitud de " + datosSolic.getTipoSolicitud() + "\nque contempla desde el dia "
				+ dateFormatParteDia.format(datosSolic.getFechaInicioDias()) + " hasta el dia "
				+ dateFormatParteDia.format(datosSolic.getFechaTerminoDias()) + ".\n\n";
		return body;
	}
}
