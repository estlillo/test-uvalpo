package cl.exe.exedoc.pojo.expediente;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.utils.Destinatario;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.exedoc.util.DestinatariosFirmaDocumentoMail;

/**
 * Interface Manejador expediente Interface.
 * 
 * @author
 */
@Local
public interface ManejarExpedienteInterface {

	/**
	 * Metodo que permite guardar un nuevo expediente.
	 * 
	 * @param expediente {@link Expediente} con los datos que se persistiran
	 */
	void crearExpediente(final Expediente expediente);

	/**
	 * Metodo que permite modificar un expediente.
	 * 
	 * @param expediente {@link Expediente} datos del nuevo expediente, ademas debe contener el identificador del
	 *        expediente que se desea modificar
	 */
	void modificarExpediente(final Expediente expediente);

	/**
	 * Metodo que permite eliminar un expediente.
	 * 
	 * @param idExpediente {@link Long} identificador del expediente a eliminar
	 */
	void eliminarExpediente(final Long idExpediente);

	/**
	 * Metodo que elimina una lista de expedientes.
	 * 
	 * @param expediente {@link Expediente}
	 * @param eliminarPadre {@link Boolean}
	 */
	void eliminarExpedientes(final Expediente expediente, final boolean eliminarPadre);

	/**
	 * Metodo que permite buscar un expediente por el id.
	 * 
	 * @param idExpediente {@link Long} identificador del expediente que se desea buscar
	 * @return {@link Expediente} el expediente encontrado
	 */
	Expediente buscarExpediente(final Long idExpediente);

	/**
	 * Metodo valida la existencia del numero del expediente.
	 * 
	 * @param numeroExpediente {@link String}
	 * @return {@link Boolean}
	 */
	boolean existeNumeroExpediente(final String numeroExpediente);

	/**
	 * Metodo que permite adjuntar un documento, primero crea el documento y luego genera la relacion con el expediente.
	 * 
	 * @param expediente {@link Expediente} representa al expediente a quien se desea adjuntar el documento
	 * @param documento {@link Documento} representa el documento a adjuntar
	 * @param tipoDocumentoExpediente {@link TipoDocumentoExpediente} representa el tipo de documento (Antecedente,
	 *        Respuesta, etc)
	 * @param esPadre {@link Boolean}
	 * @throws DocumentNotUploadedException excepcion de documento no cargado
	 */
	void adjuntarDocumento(final Expediente expediente, final Documento documento,
			final TipoDocumentoExpediente tipoDocumentoExpediente, final boolean esPadre)
			throws DocumentNotUploadedException;

	// void agregarAnexoAExpediente(Expediente expediente, Documento documento,
	// boolean esPadre);
	// void agregarAnexoAExpediente(Expediente expediente, Documento documento,
	// boolean esPadre, Long
	// idDocumentoReferencia);

	/**
	 * Metodo que sirve para agregar respuestas a un expediente ya existente.
	 * 
	 * @param expediente {@link Expediente} al que se le agregara la respuesta
	 * @param documento {@link Documento} que representa la respuesta del documento
	 * @param esPadre {@link Boolean}
	 * @throws DocumentNotUploadedException excepcion de documento no cargado
	 */
	void agregarRespuestaAExpediente(final Expediente expediente, final Documento documento, final boolean esPadre)
			throws DocumentNotUploadedException;

	/**
	 * Metodo que sirve para agregar antecedentes a un expediente ya existente.
	 * 
	 * @param expediente {@link Expediente} al que se le agregara el antecedente
	 * @param documento {@link Documento} que representa el antecedente del documento
	 * @param esPadre {@link Boolean}
	 * @throws DocumentNotUploadedException excepcion de documento no cargado
	 */
	void agregarOriginalAExpediente(final Expediente expediente, final Documento documento, final boolean esPadre)
			throws DocumentNotUploadedException;

	/**
	 * Metodo que despacha expediente a ministario.
	 * 
	 * @param expediente {@link Expediente}
	 */
	void despacharExpedienteMinisterio(final Expediente expediente);

	/**
	 * Metodo que permite despachar un expediente.
	 * 
	 * @param expediente {@link Expediente} que sera despachado o enviado
	 */
	void despacharExpediente(final Expediente expediente);
	
	public void actualizarDocumentoExpediente(final Expediente expediente);

	/**
	 * Metodo que acusa recibo de un expediente.
	 * 
	 * @param expediente {@link Expediente}
	 * @param persona {@link Persona}
	 */
	void acusarReciboExpediente(final Expediente expediente, final Persona persona);

	/**
	 * Metodo que retorna la lista de expedientes recibidos para una determino usuario, bandeja entrada.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	List<ExpedienteBandejaEntrada> listarExpedienteRecibidosPorUsuario(final Persona persona);

	/**
	 * Metodo que lista expedientes no despachados por usuario.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link ExpedienteBandejaSalida}
	 */
	List<ExpedienteBandejaSalida> listarExpedientesNoDespachadosPorUsuario(final Persona persona);

	/**
	 * Metodo que agrega destinatario al expediente.
	 * 
	 * @param expediente {@link Expediente}
	 * @param emisor {@link Persona}
	 * @param destinatario {@link Persona}
	 * @param fechaIngreso {@link Date}
	 * @return {@link Long}
	 * @throws DocumentNotUploadedException excepcion de documento no cargado
	 */
	Long agregarDestinatarioAExpediente(final Expediente expediente, final Persona emisor, final Persona destinatario,
			final Date fechaIngreso) throws DocumentNotUploadedException;

	/**
	 * Metodo que busca expedientes, segun el numero de expediente.
	 * 
	 * @param numeroExpediente {@link String}
	 * @return {@link List} of {@link Expediente}
	 */
	List<Expediente> buscarExpediente(final String numeroExpediente);

	/**
	 * Metodo que lista expedientes despachados por usuario.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link ExpedienteBandejaSalida}
	 */
	List<ExpedienteBandejaSalida> listarExpedientesDespachadosPorUsuario(final Persona persona);

	/**
	 * Metodo que lista expedientes archivados por usuario.
	 * 
	 * @param expedientes {@link List} of {@link Object}
	 * @return {@link List} of {@link ExpedienteBandejaSalida}
	 */
	List<ExpedienteBandejaSalida> listarExpedientesArchivadosPorUsuario(final List<Object[]> expedientes);

	/**
	 * Metodo que despacha notificacion por Email.
	 * 
	 * @param expedientes {@link List} of {@link Expediente}
	 */
//	void despacharNotificacionPorEmail(final List<Expediente> expedientes);

	/**
	 * Metodo que firma notificacion por email.
	 * 
	 * @param destMailFirma {@link DestinatariosFirmaDocumentoMail}
	 */
//	void firmarNotificacionPorEmail(final DestinatariosFirmaDocumentoMail destMailFirma);

	/**
	 * Metodo que obtiene Destinatarios Firma Documento Mail.
	 * 
	 * @param documento {@link Documento}
	 * @param numeroExpediente {@link String}
	 * @return {@link DestinatariosFirmaDocumentoMail}
	 */
	DestinatariosFirmaDocumentoMail obtenerDatosNotificacion(final Documento documento, final String numeroExpediente);

	/**
	 * Metodo que actualiza version del expediente.
	 * 
	 * @param expediente {@link Expediente}
	 * @return {@link Integer}
	 */
	Integer actualizarVersion(final Expediente expediente);

	// void despacharNotificacionEstadoSolVaca(DatosSolicitud datosSolic);
	// void despacharNotificacionEstadoSolDiaAdm(DatosSolicitud datosSolic);

	/**
	 * Metodo que envia email a usuario externo.
	 * 
	 * @param nombres {@link String}
	 * @param fechaFirma {@link Date}
	 * @param numeroDocumento {@link String}
	 * @param codigoDescarga {@link String}
	 * @param email {@link String}
	 */
//	void enviarMailUsuarioExtero(final String nombres, final Date fechaFirma, final String numeroDocumento,
//			final String codigoDescarga, final String email);

	/**
	 * Metodo que lista Expedientes Bandeja Entrada segun Usuario.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	List<ExpedienteBandejaEntrada> listarExpedienteBandejaEntradaUsuario(final Persona persona);

	/**
	 * Metodo que lista Expediente Bandeja Entrada Copia segun Usuario.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaEntrada}
	 */
	List<ExpedienteBandejaEntrada> listarExpedienteBandejaEntradaCopiaUsuario(final Persona persona);

	/**
	 * Metodo que archiva expediente.
	 * 
	 * @param expediente {@link Expediente}
	 */
	void archivarExpediente(final Expediente expediente);

	/**
	 * Metodo que elimina expediente de la lista de la bandeja entrada, copia o directo.
	 * 
	 * @param expediente {@link Expediente}
	 */
	void borrarExpedienteDeCache(final Expediente expediente);

	/**
	 * Metodo que obtiene todos los expedientes hijos de un expediente padre directo.
	 * 
	 * @param numeroExpediente {@link Expediente}
	 */
	List<Expediente> obtenerExpedientesHijos(Expediente numeroExpediente);

	void eliminarDespacho(final Expediente expediente);

	List<Expediente> obtienePrimerExpediene(final Expediente expediente);

//	void enviarMail(final Expediente expediente);

	void enviarMailPorDestinatarios(final Persona destinatario, int plantilla, Expediente expediente);

	List<Persona> buscarDestinatarios(List<Expediente> expedientes);

	List<ExpedienteBandejaSalida> expedientesDespachadosAGrupos(final Persona persona);
	/**
	 * @param expediente {@link Expediente}
	 */
	void cancelarExpediente(Expediente expediente);
	
	/**
	 * Metodo que lista Expedientes Bandeja Compartida segun Usuario.
	 * 
	 * @param persona {@link Persona}
	 * @return {@link List} of {@link ExpedienteBandejaCompartida}
	 */
	List<ExpedienteBandejaCompartida> listarExpedienteBandejaCompartidaUsuario(final Persona persona);

	/**
	 * 
	 * @return {@link List} of {@link ExpedienteBandejaCompartida}
	 */
	Long agregarDestinatarioAExpediente(Expediente expediente, Persona emisor,
			GrupoUsuario destinatario, Date fechaIngreso)
			throws DocumentNotUploadedException;

	void acusarReciboExpediente(Expediente expediente, Persona persona,
			boolean grupo);

	Expediente buscarExpedientePadreOriginal(Expediente e);

	public void enviarMailNotificacionFirma(Expediente expediente, DocumentoElectronico documento, List<Persona> destinatarios);

	void agregarRespuestaSolicitudAExpediente(Expediente expediente,
			Documento documento, boolean esPadre)
			throws DocumentNotUploadedException;

//	void despacharNotificacionEstadoSolVaca(DatosSolicitud datosSolic);

	void agregarAnexoAExpediente(Expediente expediente, Documento documento,
			boolean esPadre);

	void agregarAnexoAExpediente(Expediente expediente, Documento documento,
			boolean esPadre, Long idDocumentoReferencia);

	Long agregarDestinatarioAExpediente(Expediente expediente, Persona emisor,
			Destinatario destinatario, Date fechaIngreso);

//	void despacharNotificacionEstadoSolDiaAdm(DatosSolicitud datosSolic);

	List<Expediente> buscarExpedientesHijos(Expediente expedientePadre);

	void enviarMailPorGrupo(GrupoUsuario grupo, int plantilla,
			Expediente expediente);
}
