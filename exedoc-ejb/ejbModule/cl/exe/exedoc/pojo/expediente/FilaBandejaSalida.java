package cl.exe.exedoc.pojo.expediente;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase utilizada para mostrar contenido de la bandeja de salida.
 * 
 * @author
 */
public class FilaBandejaSalida extends ExpedienteBandeja implements Serializable {

	private static final long serialVersionUID = 1276743875332524141L;
	
	Long id_expediente;
	Long idPadre;
	String destinatario;
	String numeroExpediente;
	Date fechaIngreso;
	String numero;
	String materia;
	String tipo;
	String Emisor;
	String DestinatarioDocumento;
	String formato;
	Boolean recibidoFila;
	boolean desierto;
	boolean anular;
	boolean eliminado;
	String nombreGrupo;
	Long idFinal;
	Boolean repetido;
	boolean despachado;

	//rs.getDate(7) tipo documento
	//rs.getDate(8) numero documento
	//rs.getDate(9) fecha documento origen
	//rs.getDate(10) materia
	//rs.getDate(11) emisor
	//rs.getDate(11) tipo papel digital electronico
	//rs.getDate(11) destinatario documento
	
	/**
	 * Constructor.
	 */
	public FilaBandejaSalida() {
		super();
	}

	public Long getId_expediente() {
		return id_expediente;
	}

	public void setId_expediente(long id_expediente) {
		this.setId(id_expediente);
		this.id_expediente = id_expediente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getNumeroExpediente() {
		return numeroExpediente;
	}

	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getMateria() {
//		return materia;
		if (materia == null) return "";
		if (materia.trim().length() > 100) {
			return materia.substring(0, 50).concat("...");
		} else {
			return materia;
		}
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEmisor() {
//		return Emisor;
		if(Emisor == null) return "";
		if (Emisor.trim().length() > 50) {
			return Emisor.substring(0, 50).concat("...");
		} else {
			return Emisor;
		}
	}

	public void setEmisor(String emisor) {
		Emisor = emisor;
	}


	public String getDestinatarioDocumento() {
		return DestinatarioDocumento;
	}

	public void setDestinatarioDocumento(String destinatarioDocumento) {
		DestinatarioDocumento = destinatarioDocumento;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public Boolean getRecibidoFila() {
		return recibidoFila;
	}

	public void setRecibidoFila(Boolean recibidoFila) {
		this.recibidoFila = recibidoFila;
	}

	public boolean isDesierto() {
		return desierto;
	}

	public void setDesierto(boolean desierto) {
		this.desierto = desierto;
	}

	public boolean isAnular() {
		return anular;
	}

	public void setAnular(boolean anular) {
		this.anular = anular;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public boolean getDespachado(){
		if(this.getFechaDespacho() == null) return false;
		if(this.getFechaDespacho() != null) return true;
		return false;
	}
	public void setDespachado(boolean despachado) {
		this.despachado = despachado;
	}

	public Long getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(long idPadre) {
		this.idPadre = idPadre;
	}

	public boolean getEliminado() {
		return eliminado;
	}

	public void setEliminado(boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Long getIdFinal() {
		if(this.destinatario.length() == 0){
			idFinal = this.getId();
		}
		else {
			idFinal = this.getIdPadre();
		}
		return idFinal;
	}

	public void setIdFinal(Long idFinal) {
		this.idFinal = idFinal;
	}


	public Boolean getRepetido() {
		return repetido;
	}


	public void setRepetido(Boolean repetido) {
		this.repetido = repetido;
	}
	
	
	
}
