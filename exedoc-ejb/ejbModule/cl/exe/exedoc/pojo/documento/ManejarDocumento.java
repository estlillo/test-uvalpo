package cl.exe.exedoc.pojo.documento;

import java.rmi.RemoteException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.model.SelectItem;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.LazyInitializationException;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.email.Email;
import cl.exe.email.message.Mail;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoBinario;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.RevisarDocumentos;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.repositorio.FoliadorLocal;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.Customizacion;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.exception.FoliadorException;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

@Stateless
@Name("ManejarDocumento")
public class ManejarDocumento implements ManejarDocumentoInterface {

	@PersistenceContext
	protected EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@Logger
	private Log logger;

	@DataModel
	private List<TipoDocumento> tipoDocumento;

	@Out(required = false)
	private List<SelectItem> tiposDocumentos;

	@EJB
	private FoliadorLocal foliador;

	private String tipoDocumentoFiltrado;
	
	@EJB
	private Customizacion customizacion;
	
	public static String ELECTRONICO = TipoDocumentosList.TXT_ELECTRONICO;

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public void crearDocumento(final Documento documento) throws DocumentNotUploadedException {
		
		if (documento != null) {
			if (documento.getId() != null) {
				documento.setEliminable(true);
				em.merge(documento);
			} else {
				em.persist(documento);
			}
			this.persistArchivo(documento);

			if (documento.getListaPersonas() != null) {
				for (ListaPersonasDocumento dd : documento.getListaPersonas()) {
					if (dd.getId() != null) {
						em.merge(dd);
					} else {
						em.persist(dd);
					}
				}
			}

			if (this.isFoliable(documento)) {
				this.foliarDocumento(documento);
				if (documento instanceof DocumentoElectronico) {
					this.persistXML(documento);
				}
			}
			
			documento.setCodigoBarra(documento.getId());
			em.merge(documento);
		}
	}

	public void actualizarDocumentoSimple(final Documento documento) {
		em.merge(documento);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarDocumento(final Documento documento) throws DocumentNotUploadedException {
		if (documento != null) {
			
			List<RevisarDocumentos> revisar = documento.getRevisar() ;
			if ((documento.getRevisarEstructuradas() == null || (documento.getRevisarEstructuradas() != null && documento.getRevisarEstructuradas().size() == 0)) && documento instanceof DocumentoElectronico  && revisar != null && revisar.size() > 0)  {
				documento.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR_APROBADO));
			}
			this.persistArchivo(documento);
//			List<FirmaEstructuradaDocumento> firmas = new ArrayList<FirmaEstructuradaDocumento>();
//			List<VisacionEstructuradaDocumento> visaciones = new ArrayList<VisacionEstructuradaDocumento>();
//			List<RevisarEstructuradaDocumento> revisiones = new ArrayList<RevisarEstructuradaDocumento>();
//			documento.setFirmasEstructuradas(firmas);
//			documento.setRevisarEstructuradas(revisiones);
//			documento.setVisacionesEstructuradas(visaciones);
			em.merge(documento);

			if ( documento instanceof DocumentoElectronico && this.isFoliable(documento)) {
				this.foliarDocumento(documento);
				if (documento instanceof DocumentoElectronico) {
					this.persistXML(documento);
				}
			}
		}
	}
	
	private int persistArchivo(final Documento documento) throws DocumentNotUploadedException {

		int res = 0;

		if (documento instanceof DocumentoBinario) {
			final ArchivoDocumentoBinario archivo = ((DocumentoBinario) documento).getArchivo();
			if (archivo != null && archivo.getCmsId() == null) {
				persistArchivo(archivo, documento);
			}
			final List<ArchivoAdjuntoDocumentoBinario> archivos = ((DocumentoBinario) documento).getArchivosAdjuntos();
			for (ArchivoAdjuntoDocumentoBinario a : archivos) {
				if (a.getCmsId() == null) {
					persistArchivo(a, documento, false);
				}
			}
		}
		// if (documento instanceof Adquisicion) {
		// ArchivoDocumentoSolicitudAdquisicion archivoAsistentes = ((Adquisicion) documento).getArchivoAsistentes();
		// if (archivoAsistentes != null && archivoAsistentes.getCmsId() == null) {
		// persistArchivo(archivoAsistentes, documento);
		// }
		// ArchivoDocumentoSolicitudAdquisicion archivoMuestra = ((Adquisicion) documento).getArchivoMuestra();
		// if (archivoMuestra != null && archivoMuestra.getCmsId() == null) {
		// persistArchivo(archivoMuestra, documento);
		// }
		// ArchivoDocumentoSolicitudAdquisicion archivoBases = ((Adquisicion) documento).getArchivoBases();
		// if (archivoBases != null && archivoBases.getCmsId() == null) {
		// persistArchivo(archivoBases, documento);
		// }
		// }
		if (documento instanceof DocumentoElectronico) {
			try {
				final List<ArchivoAdjuntoDocumentoElectronico> archivos = ((DocumentoElectronico) documento)
						.getArchivosAdjuntos();

				if (archivos != null) {
					for (ArchivoAdjuntoDocumentoElectronico archivo : archivos) {
						if (archivo.getCmsId() == null) {
							persistArchivo(archivo, documento);
						}
					}
				}
			} catch (DocumentNotUploadedException e) {
				logger.error("Error al guardar un adjunto, documento.id: " + documento.getId());
				res = -1;
			}
		}

		return res;
	}

	public void persistXMLFirma(final Documento documento) {
		repositorio.almacenarXmlFirmaXML((DocumentoElectronico) documento);
		em.merge(documento);
	}

	/**
	 * Metodo que persiste XML.
	 * 
	 * @param documento {@link Documento}
	 */
	private void persistXML(final Documento documento) {
		final String cmsId = repositorio.almacenarXmlFirmado((DocumentoElectronico) documento, true);
		documento.setCmsId(cmsId);
		em.merge(documento);
	}

	public void persistXML(final Documento documento, final byte[] archivo) throws RemoteException {
		final String cmsId = repositorio.almacenarXmlFirmado(archivo, (DocumentoElectronico) documento, true);
		documento.setCmsId(cmsId);
		em.merge(documento);
	}

	public void persistArchivo(final Archivo archivo, final Documento documento) throws DocumentNotUploadedException {
		persistArchivo(archivo, documento, true);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void persistArchivo(final Archivo archivo, final Documento documento, final boolean mergeDocumento)
			throws DocumentNotUploadedException {

		// if (archivo.getArchivo() != null) {
		if(archivo.getId()==null)
			em.persist(archivo);
		final String nombreArchivo = getNombreArchivo(archivo.getNombreArchivo(), archivo.getId().toString());
		String cmsId = null;
		logger.info("Inicio Guardar documento binario");
		long tiempoInicio = System.currentTimeMillis();
		cmsId = repositorio.almacenarArchivo(nombreArchivo, archivo, documento);
		long totalTiempo = System.currentTimeMillis() - tiempoInicio;		
		logger.info("Termino guardar documento binario");
		logger.info("El tiempo total de la ejecución de guardar un documento binario es de :" + totalTiempo + " miliseg");
		archivo.setCmsId(cmsId);
		em.persist(archivo);
		/*
		 * Parche agregado por E. Nicholls 13/04/2011 para resolver el problema que no chequea que Alfresco guardo el
		 * documento. Se reemplaza el codigo siguiente archivo.setCmsId(cmsId); em.merge(archivo); if (documento
		 * instanceof DocumentoBinario && mergeDocumento) { documento.setCmsId(cmsId); } por el de continuacion.
		 */

		if (cmsId == null) {
			// archivo.setArchivo(null);
			FacesMessages.instance().add("No se pudo almacenar documento en repositorio.");
			throw new DocumentNotUploadedException("No se pudo almacenar documento en repositorio.");
		} else {
			archivo.setCmsId(cmsId);
			em.merge(archivo);
			if (documento instanceof DocumentoBinario && mergeDocumento) {
				documento.setCmsId(cmsId);
			}
		}

		/*
		 * Fin parche Lo de abajo estaba comentado.
		 */

		// em.merge(documento);
		/*
		 * } else { throw new DocumentNotUploadedException(); }
		 */
	}

	/**
	 * Metodo que obtiene el nombre del documento del alfreco.
	 * 
	 * @param nombreArchivo {@link String}
	 * @param id {@link String}
	 * @return {@link String}
	 */
	private String getNombreArchivo(final String nombreArchivo, final String id) {
		String extension = "";
		final StringTokenizer st = new StringTokenizer(nombreArchivo, ".");
		while (st.hasMoreElements()) {
			extension = st.nextToken();
		}
		final String nombre = id + "." + extension.toLowerCase();
		return nombre;
	}

	public void cambiaOriginalBinario(final DocumentoPapel documentoPapel, final DocumentoBinario documentoBinario) {
		Query query = em.createQuery("UPDATE Documento d SET d.formatoDocumento.id = 2 WHERE d.id= ?");
		query.setParameter(1, documentoPapel.getId());
		query.executeUpdate();

		final ArchivoDocumentoBinario archivo = documentoBinario.getArchivo();
		if (archivo.getArchivo() != null) {
			em.persist(archivo);
			em.flush();
			final String nombreArchivo = getNombreArchivo(archivo.getNombreArchivo(), archivo.getId().toString());
			final String cmsId = repositorio.almacenarArchivo(nombreArchivo, archivo.getArchivo(),
					archivo.getContentType());
			archivo.setCmsId(cmsId);
			em.merge(archivo);
			documentoBinario.setCmsId(cmsId);
			query = em.createQuery("UPDATE Documento SET cmsId = ?, archivo = ?, formatoDocumento = ? WHERE id = ?");
			query.setParameter(1, cmsId);
			query.setParameter(2, archivo);
			query.setParameter(3, new FormatoDocumento(FormatoDocumento.DIGITAL));
			query.setParameter(4, documentoPapel.getId());
			query.executeUpdate();
			
			query = em.createNativeQuery("update documentos set id_formato_documento = ? where id = ? ");
			query.setParameter(1, FormatoDocumento.DIGITAL.intValue());
			query.setParameter(2, documentoPapel.getId());
			query.executeUpdate();
			
		}

	}

	/**
	 * Metodo que indica si un documento es foliable.
	 * 
	 * @param documento {@link Documento}
	 * @return {@link Boolean}
	 */
	private boolean isFoliable(final Documento documento) {
		try {
			final boolean foliable = documento.getFirmas() != null && documento.getFirmas().size() != 0
					&& documento.getNumeroDocumento() == null
					&& (documento.getFirmasEstructuradas() == null || documento.getFirmasEstructuradas().size() == 0);
			return foliable;
		} catch (LazyInitializationException e) {
			return false;
		}
	}

	/**
	 * Metodo que folia el documento.
	 * 
	 * @param documento {@link Documento}
	 */
	private void foliarDocumento(final Documento documento) {
		try {
			final String numeroDocumento = foliador.generarFolio(documento.getTipoDocumento(), null);
			documento.setNumeroDocumento(numeroDocumento);
		} catch (FoliadorException e) {
			e.printStackTrace();
		}
		em.merge(documento);
	}

	/**
	 * Método que permite eliminar un documento
	 * 
	 * @param idDocumento identificador del documento que se eliminará
	 */
	public void eliminarDocumento(final Long idDocumento) {
		Documento documento = em.find(Documento.class, idDocumento);
		if (documento != null) {
			final Query query = em.createQuery("DELETE FROM DocumentoExpediente de WHERE de.documento = ?");
			query.setParameter(1, documento).executeUpdate();
			em.remove(documento);
			em.flush();
			documento = em.find(Documento.class, idDocumento);
		}
	}

	/**
	 * Método que permite eliminar un documento de forma virtual.
	 * 
	 * @param idDocumento identificador del documento que se eliminará
	 */
	public void eliminarDocumento(final Long idDocumento, final boolean permanente) {
		if (permanente) {
			eliminarDocumento(idDocumento);
		} else {
			final Documento documento = em.find(Documento.class, idDocumento);
			documento.setEliminado(true);
			em.merge(documento);
		}
	}

	/**
	 * Método que permite buscar un documento por su id
	 * 
	 * @param idDocumento identificador del documento que se desea buscar
	 * @return documento encontrado
	 */
	public Documento buscarDocumento(final Long idDocumento) {
		return em.find(Documento.class, idDocumento);
	}

	@SuppressWarnings("unchecked")
	public List<Documento> buscarDocumentoAntecedentes(final Long idDocumento) {
		final StringBuilder sbQuery = new StringBuilder(
				"Select  DISTINCT(d) From DocumentoExpediente de, Documento d Where ");
		sbQuery.append(" de.documento = d ");
		sbQuery.append(" and de.idDocumentoReferencia = ? ");
		final Query query = em.createQuery(sbQuery.toString());
		query.setParameter(1, idDocumento);
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	public boolean existeDocumento(final String numeroDocumento, final Integer idTipoDocumento) {
		final Query query = em
				.createQuery("Select d From Documento d Where d.numeroDocumento = ? and d.tipoDocumento.id = ?");
		query.setParameter(1, numeroDocumento);
		query.setParameter(2, idTipoDocumento);
		final List<Object> docs = query.getResultList();
		if (docs != null && docs.size() != 0) { return true; }
		return false;
	}

	/**
	 * Método que permite buscar un listado de documentos
	 * 
	 * @param documento filtros bases para la búsqueda
	 * @param fechaDesde comienzo del rango de fechas por el cual se buscaran los documentos
	 * @param fechaHasta fin del rango de fechas por el cual se buscaran los documentos
	 * @return una lista de documentos encontrados
	 */
	@SuppressWarnings("unchecked")
	public List<Documento> listarDocumentos(final Documento documento, final Date fechaDesde, final Date fechaHasta) {
		List<Documento> lista = null;
		final String jpQL = "SELECT d FROM Documento d ";
		String where = "";
		if (documento != null) {
			if (documento.getId() != null && documento.getId() > 0) {
				where += " d.id = ? ";
			}
			if (documento.getNumeroDocumento() != null && documento.getNumeroDocumento().length() > 0) {
				where += (where.length() > 0 ? "and" : "") + " d.numeroDocumento = ? ";
			}
			if (fechaDesde != null && fechaHasta != null) {
				where += (where.length() > 0 ? "and" : "") + " d.fechaDocumentoOrigen between ? and ? ";
			} else if (fechaDesde != null && fechaHasta == null) {
				where += (where.length() > 0 ? "and" : "") + " d.fechaDocumentoOrigen >= ? ";
			} else if (fechaDesde == null && fechaHasta != null) {
				where += (where.length() > 0 ? "and" : "") + " d.fechaDocumentoOrigen <= ? ";
			} else if (documento.getFechaDocumentoOrigen() != null) {
				where += (where.length() > 0 ? "and" : "") + " d.fechaDocumentoOrigen = ? ";
			}
			// Agregar mas filtros si se necesitan
		}
		final Query query = em.createQuery(jpQL);
		if (documento != null) {
			int index = 1;
			if (documento.getId() != null && documento.getId() > 0) {
				query.setParameter(index, documento.getId());
				index++;
			}
			if (documento.getNumeroDocumento() != null && documento.getNumeroDocumento().length() > 0) {
				query.setParameter(index, documento.getNumeroDocumento());
				index++;
			}
			if (fechaDesde != null && fechaHasta != null) {
				query.setParameter(index, fechaDesde);
				index++;
				query.setParameter(index, fechaHasta);
				index++;
			} else if (fechaDesde != null && fechaHasta == null) {
				query.setParameter(index, fechaDesde);
				index++;
			} else if (fechaDesde == null && fechaHasta != null) {
				query.setParameter(index, fechaHasta);
				index++;
			} else if (documento.getFechaDocumentoOrigen() != null) {
				query.setParameter(index, documento.getFechaDocumentoOrigen());
				index++;
			}
		}
		lista = query.getResultList();
		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerTiposDeDocumentos() {
		logger.debug("[obtenerTiposDeDocumentos] Iniciando método.");
		tipoDocumento = em.createQuery("select td from TipoDocumento td where visible = true order by descripcion asc ").getResultList();
		tiposDocumentos = new ArrayList<SelectItem>();
		List<SelectItem> tiposDocumentosElectronicos = new ArrayList<SelectItem>();
		tiposDocumentos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		for (TipoDocumento td : tipoDocumento) {
			if (td.getElectronico()) {
				tiposDocumentosElectronicos.add(new SelectItem(td.getId(), td.getDescripcion() + " (" + ELECTRONICO + ")"));
			} else {
				tiposDocumentos.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		if (tiposDocumentosElectronicos.size() > 0) {
			//tiposDocumentos.add(new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR_ELECTRONICO));
			for (SelectItem s : tiposDocumentosElectronicos) {
				tiposDocumentos.add(s);
			}
		}
		logger.debug("[obtenerTiposDeDocumentos] Finalizando método.");
		return tiposDocumentos;
	}

	public List<SelectItem> getTiposDocumentos() {
		return tiposDocumentos;
	}

	public void setTiposDocumentos(final List<SelectItem> tiposDocumentos) {
		this.tiposDocumentos = tiposDocumentos;
	}

	public List<TipoDocumento> getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(final List<TipoDocumento> tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTipoDocumentoFiltrado() {
		return tipoDocumentoFiltrado;
	}

	public void setTipoDocumentoFiltrado(final String tipoDocumentoFiltrado) {
		this.tipoDocumentoFiltrado = tipoDocumentoFiltrado;
	}
	
	/**
	 *  {@link ListaPersonasDocumento}
	 */
	@Override
	public void removeDestinatarios(Long idDocumento) {
		Query query = em.createQuery("delete from ListaPersonasDocumento l where l.documento.id = :idDocumento");
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
	}
	@Override
	public void limpiarFlujoDocumento(Long idDocumento) {
		//Quitamos visaciones
		Query query = em.createQuery("delete from VisacionDocumento l where l.documento.id = :idDocumento");
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		//Quitamos Firmas
		query = em.createQuery("delete from FirmaDocumento l where l.documento.id = :idDocumento");
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		//Quitamos revisiones
		query = em.createQuery("delete from RevisarDocumentos l where l.documento.id = :idDocumento");
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		
		
	}

	@Override
	public void limpiarRevisionesEstructuradasBorradas(Long idDocumento, List<Long> idsValidos) {
		Query query = em.createQuery("delete from RevisarEstructuradaDocumento l where not (l.id in (:idsValidos)) and l.documento.id = :idDocumento");
		query.setParameter("idsValidos",idsValidos);
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		
	}

	@Override
	public void limpiarVisacionesEstructuradasBorradas(Long idDocumento, List<Long> idsValidos) {
		Query query = em.createQuery("delete from VisacionEstructuradaDocumento l where not (l.id in (:idsValidos)) and l.documento.id = :idDocumento");
		query.setParameter("idsValidos",idsValidos);
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		
	}

	@Override
	public void limpiarFirmasEstructuradasBorradas(Long idDocumento, List<Long> idsValidos) {
		
		Query query = em.createQuery("delete from FirmaEstructuradaDocumento l where not (l.id in (:idsValidos)) and l.documento.id = :idDocumento");
		query.setParameter("idsValidos",idsValidos);
		query.setParameter("idDocumento",idDocumento);
		query.executeUpdate();
		
	}
	@Override
	public void notificarRechazoResolucion(Persona destinatarioEmail, Resolucion res, Observacion obs){
		if (destinatarioEmail.getEmail() == null) {
			logger.info("Resolucion Rechazada: Usuario sin email... " + destinatarioEmail.getNombreApellido());
			return;
		}
		if (customizacion.getEnviadorCorreo()) {
			final Email email = new Email();
			Mail mail = null;
			String msj = "";
			try {
				mail = new Mail();
				mail.setForm(new InternetAddress(destinatarioEmail.getEmail()));
				mail.setSubject("Resolucion Rechazada");
				mail.setText(generarCuerpoMailRechazoResolucion(destinatarioEmail,res,obs));
				//logger.info("Enviando Mail Visador a : "+destinatarioEmail.getNombreApellido()+"\n"+mail.getText());
				email.enviarMail(mail);
			} catch (AddressException e1) {
				msj = MessageFormat.format(
						"Ocurrio un error con el siguiente mail: {0} ", destinatarioEmail.getEmail());
				logger.error(msj, e1);
			}
		} else {
			logger.info("Resolucion Rechazada: Enviador de correo desactivado... " + destinatarioEmail.getEmail());
		}
	}
	private String generarCuerpoMailRechazoResolucion(Persona destinatarioEmail, Resolucion res, Observacion obs) {

		final StringBuilder cuerpo = new StringBuilder();

		cuerpo.append("Estimado (a): ").append(
				destinatarioEmail.getNombreApellido());
		cuerpo.append("\nLe informamos que "+obs.getAutor().getNombreApellido());
		cuerpo.append(" ha rechazado la Resolucion N°"+res.getNumeroDocumento());
		cuerpo.append(" con la siguiente observaciones: \n\t" +obs.getObservacion());
		cuerpo.append("\n\nEnviado por EXEDoc\n\n");
		return cuerpo.toString();
	}

	@Override
	public void removeRevisarEstructurada(Long revisarId) {
		RevisarEstructuradaDocumento revisor = em.find(RevisarEstructuradaDocumento.class, revisarId);
		em.remove(revisor);
	}
	@Override
	public void removeVisacionEstructurada(VisacionEstructuradaDocumento visador) {
		em.remove(visador);
	}
	@Override
	public void removeFirmaEstructurada(FirmaEstructuradaDocumento firmante) {
		em.remove(firmante);
	}
	
	@Override
	public boolean isEliminable(Documento doc, Expediente expediente, Persona usuario) {
		boolean retorno = false;
		doc = em.find(Documento.class, doc.getId());
		if (expediente.getVersionPadre() != null) {
			if (doc.getAutor().getId().equals(usuario.getId())) {
				if (expediente.getFechaDespacho() == null) {
					if (doc.getVisaciones() == null || (doc.getVisaciones() != null && doc.getVisaciones().size() == 0)) {
						if (doc.getFirmas() == null || (doc.getFirmas() != null && doc.getFirmas().size() == 0)) {
							StringBuilder hsql = new StringBuilder();
							hsql.append("SELECT count(de) FROM DocumentoExpediente de ");
							hsql.append("WHERE de.documento.id = :idDoc ");
							hsql.append("AND de.expediente.id = :idExp ");
							Long count = ((Long)em.createQuery(hsql.toString()).setParameter("idDoc", doc.getId()).setParameter("idExp", expediente.getVersionPadre().getId()).getSingleResult()).longValue();
							if (count.equals(0L)) {
								retorno = true;
							}
						}
					}
				}
			}
		}
		return retorno;
	}
}
