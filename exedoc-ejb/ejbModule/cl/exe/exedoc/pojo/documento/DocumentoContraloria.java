package cl.exe.exedoc.pojo.documento;

import java.util.Date;

public class DocumentoContraloria implements Comparable<DocumentoContraloria> {
	private Long id;
	private String numeroDocumento;
	private Date fechaDocumentoOrigen;
	private String materia;
	private String descripcionTipoDocumento;
	private Date fechaDescargaContraloria;

	/**
	 * Constructor.
	 */
	public DocumentoContraloria() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(final String materia) {
		this.materia = materia;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getDescripcionTipoDocumento() {
		return descripcionTipoDocumento;
	}

	public void setDescripcionTipoDocumento(final String descripcionTipoDocumento) {
		this.descripcionTipoDocumento = descripcionTipoDocumento;
	}

	public Date getFechaDocumentoOrigen() {
		return fechaDocumentoOrigen;
	}

	public void setFechaDocumentoOrigen(final Date fechaDocumentoOrigen) {
		this.fechaDocumentoOrigen = fechaDocumentoOrigen;
	}

	public Date getFechaDescargaContraloria() {
		return fechaDescargaContraloria;
	}

	public void setFechaDescargaContraloria(final Date fechaDescargaContraloria) {
		this.fechaDescargaContraloria = fechaDescargaContraloria;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof DocumentoContraloria) { 
			return this.getId().equals(((DocumentoContraloria) obj).getId()); 
		}
		return false;
	}

	public int hashCode() {
		return this.getId().hashCode();
	}

	public int compareTo(final DocumentoContraloria o) {
		return this.getId().compareTo(o.getId());
	}

}
