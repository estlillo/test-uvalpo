package cl.exe.exedoc.pojo.documento;

public class DocumentoDescarga {
	private Long id;
	private String materia;
	private byte[] documento;
	private String nombreArchivo;
	private String cmsId;

	/**
	 * Constructor.
	 */
	public DocumentoDescarga() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(final String materia) {
		this.materia = materia;
	}

	public byte[] getDocumento() {
		return documento;
	}

	public void setDocumento(final byte[] documento) {
		this.documento = documento;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(final String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getCmsId() {
		return cmsId;
	}

	public void setCmsId(final String cmsId) {
		this.cmsId = cmsId;
	}

}
