package cl.exe.exedoc.pojo.documento;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;

@Local
public interface ManejarDocumentoInterface {

	void crearDocumento(Documento documento) throws DocumentNotUploadedException;

	void actualizarDocumento(Documento documento) throws DocumentNotUploadedException;

	void eliminarDocumento(Long idDocumento);

	void eliminarDocumento(Long idDocumento, boolean permanente);

	Documento buscarDocumento(Long idDocumento);

	boolean existeDocumento(String numeroDocumento, Integer idTipoDocumento);

	List<Documento> listarDocumentos(Documento documento, Date fechaDesde, Date fechaHasta);

	List<SelectItem> obtenerTiposDeDocumentos();

	List<SelectItem> getTiposDocumentos();

	void setTiposDocumentos(List<SelectItem> tiposDocumentos);

	List<TipoDocumento> getTipoDocumento();

	void setTipoDocumento(List<TipoDocumento> tipoDocumento);

	String getTipoDocumentoFiltrado();

	void setTipoDocumentoFiltrado(String tipoDocumentoFiltrado);

	void cambiaOriginalBinario(DocumentoPapel documentoPapel, DocumentoBinario documentoBinario);

	List<Documento> buscarDocumentoAntecedentes(Long idDocumento);

	void persistXML(Documento documento, byte[] archivo) throws Exception;

	void persistXMLFirma(Documento documento);

	void persistArchivo(Archivo archivo, Documento documento) throws DocumentNotUploadedException;

	void actualizarDocumentoSimple(Documento documento);

	void removeDestinatarios(Long idDocumento);

	void limpiarFlujoDocumento(Long idDocumento);

	void limpiarRevisionesEstructuradasBorradas(Long idDocumento, List<Long> idsValidos);
	void limpiarVisacionesEstructuradasBorradas(Long idDocumento, List<Long> idsValidos);
	void limpiarFirmasEstructuradasBorradas(Long idDocumento, List<Long> idsValidos);

	void notificarRechazoResolucion(Persona destinatarioEmail, Resolucion res,	Observacion obs);
	void removeVisacionEstructurada(VisacionEstructuradaDocumento visador);
	void removeFirmaEstructurada(FirmaEstructuradaDocumento firmante);
	void removeRevisarEstructurada(Long revisarId);

	boolean isEliminable(Documento doc, Expediente expediente, Persona usuario);
}
