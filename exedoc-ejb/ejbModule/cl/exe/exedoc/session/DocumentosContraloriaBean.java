package cl.exe.exedoc.session;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.pojo.documento.DocumentoContraloria;
import cl.exe.exedoc.pojo.documento.DocumentoDescarga;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.util.DocUtils;

@Name("documentosContraloria")
@Stateful
public class DocumentosContraloriaBean implements DocumentosContraloria {

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private RepositorioLocal repositorio;

	private String ulrDocumentoContraloria = "documentosContraloria";
	private List<DocumentoContraloria> documentosAContraloria;

	private List<DocumentoDescarga> documentosDescarga;
	private Documento documentoDescarga;

	@Begin(join = true)
	public String begin() {
		log.info("Comenzando Documentos a Contraloría");
		obtieneDocumentosAContraloria();
		documentosDescarga = new ArrayList<DocumentoDescarga>();
		documentoDescarga = null;
		return ulrDocumentoContraloria;
	}

	@SuppressWarnings("unchecked")
	private void obtieneDocumentosAContraloria() {
		documentosAContraloria = new ArrayList<DocumentoContraloria>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append("DISTINCT d.id, d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, td.descripcion, d.fechaDescargaContraloria ");
		sb.append("FROM ");
		sb.append("Documento d left outer join d.firmaEstructurada  fe , ");
		sb.append("TipoRazon tr, ");
		sb.append("Firma f, ");
		sb.append("TipoDocumento td, ");
		sb.append("FormatoDocumento fd ");
		sb.append("WHERE ");
		sb.append("tr.id in (" + TipoRazon.TOMA_RAZON + "," + TipoRazon.EXENTO_CON_REGISTRO + ") ");
		sb.append("and d.tipoRazon.id = tr.id ");
		sb.append("and d.tipoDocumento.id in (" + TipoDocumento.RESOLUCION + "," + TipoDocumento.DECRETO + ") ");
		sb.append("and f.documento.id = d.id ");
		sb.append("and d.tipoDocumento.id = td.id ");
		sb.append("and d.formato.id = fd.id ");
		sb.append("and fd.id in (" + FormatoDocumento.ELECTRONICO + ")");

		log.info("Query obtieneDocumentosAContraloria: " + sb.toString());
		Query query = entityManager.createQuery(sb.toString());
//		Query query = entityManager.createNativeQuery(sb.toString());
		List<Object[]> documentosQuery = query.getResultList();
		for (Object[] o : documentosQuery) {
			DocumentoContraloria dc = new DocumentoContraloria();
			dc.setId(((BigInteger) o[0]).longValue());
			dc.setNumeroDocumento((String) o[1]);
			dc.setFechaDocumentoOrigen((Date) o[2]);
			dc.setMateria((String) o[3]);
			dc.setDescripcionTipoDocumento((String) o[4]);
			dc.setFechaDescargaContraloria((Date) o[5]);
			documentosAContraloria.add(dc);
		}
	}

	public void descargar(DocumentoContraloria documento) {
		documentosDescarga.clear();
		TipoDocumento tdResolucion = new TipoDocumento(TipoDocumento.RESOLUCION);
		TipoDocumento tdDecreto = new TipoDocumento(TipoDocumento.DECRETO);

		Documento doc = null;
		if (documento.getDescripcionTipoDocumento().equals(tdResolucion.getDescripcion())) {
			doc = entityManager.find(Resolucion.class, documento.getId());
		}
		if (documento.getDescripcionTipoDocumento().equals(tdDecreto.getDescripcion())) {
			doc = entityManager.find(Decreto.class, documento.getId());
		}

		documentoDescarga = doc;
		documentoDescarga.getBitacoras().size();

		DocumentoDescarga dd = new DocumentoDescarga();
		dd.setId(doc.getId());
		dd.setMateria(doc.getMateria());
		dd.setNombreArchivo(documento.getDescripcionTipoDocumento() + "_" + documento.getNumeroDocumento() + ".XML");
		dd.setCmsId(doc.getCmsId());
		documentosDescarga.add(dd);

		List<ArchivoAdjuntoDocumentoElectronico> archivos = ((DocumentoElectronico) doc).getArchivosAdjuntos();
		for (ArchivoAdjuntoDocumentoElectronico a : archivos) {
			dd = new DocumentoDescarga();
			dd.setId(a.getId());
			dd.setMateria(a.getMateria());
			dd.setNombreArchivo(a.getNombreArchivo());
			dd.setCmsId(a.getCmsId());
			documentosDescarga.add(dd);
		}
	}

	public void descargarArchivo(DocumentoDescarga archivo) {
		this.despliegaArchivo(archivo);
	}

	private void despliegaArchivo(DocumentoDescarga archivo) {
		/*byte[] data = buscarArchivoRepositorio(archivo.getCmsId());
		if (data != null) {
			descargaAttach(data, archivo.getNombreArchivo());
		} else {
			FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + archivo.getId() + ")");
		}*/
	    try {
            if (!DocUtils.getFile(archivo.getId(), entityManager)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + archivo.getId() + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private byte[] buscarArchivoRepositorio(String cmsId) {
		return repositorio.recuperarArchivo(cmsId);
	}

	private void descargaAttach(byte[] data, String nombre) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.setHeader("Content-disposition", "attachment; filename=\"" + nombre + "\"");
			response.getOutputStream().write(data);
			response.getOutputStream().flush();
			facesContext.responseComplete();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void descargarZip() {
		byte[] comprimido = this.comprimirZip();
		this.descargaAttach(comprimido, "archivos.zip");
		this.registroDescarga();
	}

	private void registroDescarga() {
		Date fechaDescarga = new Date();
		if (documentoDescarga instanceof DocumentoElectronico) {
			((DocumentoElectronico) documentoDescarga).setFechaDescargaContraloria(fechaDescarga);
			documentoDescarga.getBitacoras().add(new Bitacora(EstadoDocumento.ENVIADO_CONTRALORIA, this.documentoDescarga, this.usuario));
			entityManager.merge(documentoDescarga);
			for (DocumentoContraloria dc : documentosAContraloria) {
				if (dc.getId().equals(documentoDescarga.getId())) {
					dc.setFechaDescargaContraloria(fechaDescarga);
				}
			}
		}
	}

	private byte[] comprimirZip() {
		log.info("Comprimiendo");
		int BUFFER_SIZE = 512;
		try {
			BufferedInputStream origin = null;
			ByteArrayOutputStream dest = new ByteArrayOutputStream();
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));

			byte[] data = new byte[BUFFER_SIZE];

			for (DocumentoDescarga dd : documentosDescarga) {
				log.info("Comprimiendo archivo:" + dd.getNombreArchivo());
				//byte[] file = buscarArchivoRepositorio(dd.getCmsId());
				byte[] file = repositorio.getFile(dd.getCmsId());
				ByteArrayInputStream bi = new ByteArrayInputStream(file);

				origin = new BufferedInputStream(bi, BUFFER_SIZE);
				ZipEntry entry = new ZipEntry(dd.getNombreArchivo());
				out.putNextEntry(entry);

				int count;
				while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
					out.write(data, 0, count);
				}
				origin.close();
			}
			out.close();
			return dest.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Factory(value = "documentosAContraloria", scope = ScopeType.PAGE)
	public List<DocumentoContraloria> getDocumentosAContraloria() {
		return documentosAContraloria;
	}

	public String end() {

		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public List<DocumentoDescarga> getDocumentosDescarga() {
		return documentosDescarga;
	}

}
