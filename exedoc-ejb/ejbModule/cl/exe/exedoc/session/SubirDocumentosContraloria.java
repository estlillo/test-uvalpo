package cl.exe.exedoc.session;

import javax.ejb.Local;

import org.richfaces.event.UploadEvent;

@Local
public interface SubirDocumentosContraloria {
	public String begin();

	public String ingresarDocumento();

	public void uploadListener(UploadEvent event) throws Exception;

	public String end();

	public Integer getTipoRespuestaContraloria();

	public void setTipoRespuestaContraloria(Integer tipoRespuestaContraloria);

	public void destroy();

}
