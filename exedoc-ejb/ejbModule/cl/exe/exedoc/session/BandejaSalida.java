package cl.exe.exedoc.session;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.pojo.expediente.FilaBandejaSalida;

@Local
public interface BandejaSalida {

	String begin();

	String end();

	void destroy();

	Map<Long, Boolean> getSelectedDespachados();

	void setSelectedDespachados(Map<Long, Boolean> selectedDespachados);

	Map<Long, Boolean> getSelectedDesiertos();

	void setSelectedDesiertos(Map<Long, Boolean> selectedDesiertos);

	List<ExpedienteBandejaSalida> getListExpedientes();

	void setListExpedientes(List<ExpedienteBandejaSalida> listExpedientes);

	void despacharExpedientes();

	void setListaDesertar(Long id);

	void buscarExpedientes();

	String obtenerEstilo(String tipoDoc);

	Boolean getDesdeDespacho();

	void setDesdeDespacho(Boolean desdeDespacho);

	String anularDespachoExpedientes();

	Map<Long, Boolean> getSelectedAnular();

	void setSelectedAnular(Map<Long, Boolean> selectedAnular);
	public List<FilaBandejaSalida> getListaBandeja();

	public void setListaBandeja(List<FilaBandejaSalida> listaBandeja);
	
	boolean isValidarComplete();

	public void setValidarComplete(final boolean validarComplete);

	boolean getEstadoDesertar();

	void setEstadoDesertar(boolean estadoDesertar);

	String obtenerEstiloRepetido(FilaBandejaSalida fila);
	
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
}
