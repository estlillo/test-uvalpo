package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearOficioOrdinario {

	void destroy();

	String end();

	String volver();

	void visualizar();

	void guardar();

	void firmar();

	void visar();

	String agregarDocumento();

	void guardarYCrearExpediente();

	Oficio getOficioOrdinario();

	String getParrafo();

	void setParrafo(String parrafo);

	List<SelectPersonas> getDistribucionDocumento();

	List<SelectPersonas> getDestinatariosDocumento();

	Boolean getFirmado();

	Boolean getVisado();

	List<VisacionDocumento> getVisaciones();

	String completar();

	Boolean getCompletado();

	boolean isEditable();

	boolean isEditableSinVisa();

	boolean isDespachable();

	boolean isRenderedBotonVisar();

	boolean isRenderedBotonFirmar();

	boolean isRenderedBotonDespachar();

	boolean isRenderedBotonGuardar();

	boolean isRenderedBotonGuardarCrearExpediente();

	boolean isRenderedBotonVisualizar();

	boolean isRenderedBotonVolver();

	boolean isRenderedBotonFirmarAvanzado();

	String buscaDocumento();

	String getUrlFirma();

	boolean validarFirmaAvanzada();

	String guardaParaFirma();

	Boolean getValidarPanelApplet();

	void setValidarPanelApplet(Boolean validarPanelApplet);

	void validar();

	void resetValidar();

	/* **************************************************************************
	 * Archivos Anexos - Antecedentes************************************************************************
	 */
	void limpiarAnexos();

	int getMaxFilesQuantity();

	void listener(UploadEvent event) throws Exception;

	void agregarAntecedenteArchivo();

	void verArchivo(Long idArchivo);

	// void eliminarAnexoDocumento(Integer numDoc);
	String getMateriaArchivo();

	void setMateriaArchivo(String materiaArchivo);

	// List<Documento> getListDocumentosAnexo();

	/* **************************************************************************
	 * DESPACHAR EXPEDIENTES************************************************************************
	 */
//	String despacharExpediente();

	List<Documento> getListDocumentosAnexo();

	List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	String getSessionId();

	String buscaDocumento1();

	String buscaDocumento2();
	
	void eliminarArchivoAntecedente(ArchivoAdjuntoDocumentoElectronico archivo);

	boolean isValidarAntecedente();

	void removeArchivoAnexo();

	boolean isDocumentoValido();

	void previsualizar();

}
