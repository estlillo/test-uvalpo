package cl.exe.exedoc.session;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import javax.persistence.Query;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.In;
import org.jboss.seam.ScopeType;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import com.arjuna.ats.arjuna.tools.report;
import com.sun.tools.ws.wsdl.framework.Entity;

import cl.exe.exedoc.entity.FolioDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.ReporteSolicitudFolioDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.mantenedores.AdministradorDepartamentos;
import cl.exe.exedoc.mantenedores.AdministradorTiposDocumentos;
import cl.exe.exedoc.mantenedores.AdministradorUnidades;
import cl.exe.exedoc.mantenedores.dao.AdministradorReporteFolio;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;
import cl.exe.firma.xml.Documento;

@Name("solicitudFolioDocumento")
@Stateful
@Scope(ScopeType.CONVERSATION)
public class SolicitudFolioDocumentoBean implements SolicitudFolioDocumento, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -316049077961024818L;

	private static final int LARGO_MAXIMO = 255;

	@Logger
	private Log log;

	private FolioDocumento folioDocumento;

	private final String SOLICITUD_FOLIO_DOCUMENTO = "solicitudFolioDocumento";
	private final String SELECCIONE = "<<Seleccionar>>";
	private final String NO_PUEDE_OBTENER = "No se pudo generar folio. Verifique que el folio esté creado.";

	@PersistenceContext
	private EntityManager em;

	@EJB
	JerarquiasLocal jerarquia;

	@EJB
	MantenedorFolios mantenedor;

	@EJB
	AdministradorReporteFolio adminFolio;

	private List<SelectItem> tipoDocumentos;
	private List<SelectItem> fiscalias;
	private List<SelectItem> unidades;

	private Long tipoDocumento = JerarquiasLocal.INICIO;
	private Long idFiscalia = JerarquiasLocal.INICIO;
	private Long idUnidad = JerarquiasLocal.INICIO;
	private String Materia;
	private String AgnoActual;
	private String valor;
	private String observacion;
	// private boolean check;

	@In(required = true)
	private Persona usuario;

	private Boolean habilitaUnidad;

	@Begin(join = true)
	@Override
	public String begin() {

		log.info("beginning conversation");
		this.folioDocumento = new FolioDocumento();
        this.setTipoDocumentos(TipoDocumentosList.getInstanceTipoDocumentoList(em)
        						.getTiposDocumentoListVisible(SELECCIONE));

		this.setFiscalias(jerarquia.getFiscalias());
        this.setFiscalia(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());

        this.setUnidades(jerarquia.getUnidadesOrganzacionales(SELECCIONE, this.usuario.getCargo()
        							.getUnidadOrganizacional().getDepartamento().getId()));
        this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional().getId());
		this.setTipoDocumento(Long.valueOf(-1));
		final Calendar toDay = Calendar.getInstance();
		this.setAgnoActual(String.valueOf(toDay.get(Calendar.YEAR)));
		this.setValor("0");

		this.setObservacion("");
		this.setMateria("");
		folioDocumento.setNivel(false);

		return SOLICITUD_FOLIO_DOCUMENTO;
	}

	// add additional action methods that participate in this conversation

	@End
	public String end() {
		// implement your end conversation business logic
		log.info("ending conversation");
		return "home";
	}

	@Remove
    public void destroy() { }


	@Override
	public List<SelectItem> getTipoDocumentos() {
		return tipoDocumentos;
	}

	@Override
	public void setTipoDocumentos(List<SelectItem> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	@Override
	public Long getTipoDocumento() {
		return tipoDocumento;
	}

	@Override
	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Long getFiscalia() {
		return idFiscalia;
	}

	@Override
	public void setFiscalia(Long idFiscalia) {
		this.idFiscalia = idFiscalia;
	}

	@Override
	public Long getUnidad() {
		return idUnidad;
	}

	@Override
	public void setUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	@Override
	public String getMateria() {
		return Materia;
	}

	@Override
	public void setMateria(String Materia) {
		this.Materia = Materia;
	}

	@Override
	public List<SelectItem> getFiscalias() {
		return fiscalias;
	}

	@Override
	public void setFiscalias(List<SelectItem> fiscalias) {
		this.fiscalias = fiscalias;
	}

	@Override
	public List<SelectItem> getUnidades() {
		return unidades;
	}

	@Override
	public void setUnidades(List<SelectItem> unidades) {
		this.unidades = unidades;
	}

	@Override
	public String generar() {
		log.info("generar busqueda: " + getFiscalia() + " " + getMateria() + " " + getUnidad());
		return null;
	}

	@Override
	public String getAgnoActual() {
		return AgnoActual;
	}

	@Override
	public void setAgnoActual(String agno) {
		AgnoActual = agno;
	}

	@Override
	public void obtenerFolio() {
		Boolean estado = Boolean.TRUE;
		this.Materia = this.Materia.trim();
		if (this.Materia.length() == 0) {
			FacesMessages.instance().add("Debe ingresar Materia");
			estado = Boolean.FALSE;
		}
		int largoMateria = this.Materia.trim().length();
		if (this.Materia != null && !this.Materia.trim().isEmpty() && largoMateria >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres, en el campo \"Materia\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}

		this.observacion = this.observacion.trim();
		if (this.observacion.length() == 0) {
			FacesMessages.instance().add("Debe ingresar Observación");
			estado = Boolean.FALSE;
		}
		int largoObs = this.observacion.trim().length();
		if (this.observacion != null && !this.observacion.trim().isEmpty() && largoObs >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres, en el campo \"Observación\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}

		if (this.idUnidad == null || this.idUnidad < 0) {
			if (!folioDocumento.getNivel()) {
				FacesMessages.instance().add("Debe seleccionar una unidad.");
				estado = Boolean.FALSE;
			}
		}

		if (this.tipoDocumento == null || this.tipoDocumento < 1) {
			FacesMessages.instance().add("Debe seleccionar un tipo de documento.");
			estado = Boolean.FALSE;
		}
		//int asd=(int)this.tipoDocumento.intValue();
		//td.setId(asd);

		if (estado) {
			final StringBuilder sql = new StringBuilder(
					"SELECT f FROM FolioDocumento f WHERE f.fiscalia.id = :fiscalia and f.tipoDocumento.id = :tipodocumento ");
			if (this.getUnidad() != null && this.getUnidad() > 0) {
				sql.append(" and f.unidad.id = :unidad ");
			} else {
				sql.append(" and f.unidad.id is null ");
			}
			// System.out.println(sql.toString());
			final Query query = em.createQuery(sql.toString());
			// final Query query =
			// em.createNamedQuery("FolioDocumento.findByFiltro");

			query.setParameter("fiscalia", this.getFiscalia());
			if (this.getUnidad() != null && this.getUnidad() > 0) {
				query.setParameter("unidad", this.getUnidad());
			}
			query.setParameter("tipodocumento", this.getTipoDocumento().intValue());
			this.folioDocumento = new FolioDocumento();
			try {

				folioDocumento = (FolioDocumento) query.getSingleResult();
				//dto.setId(this.getFiscalia());
				//uo.setId(this.getUnidad());
				//folioDocumento = (FolioDocumento) query.getSingleResult();
				folioDocumento.setNumeroActual(folioDocumento.getNumeroActual() + 1);
				this.setValor(String.valueOf(folioDocumento.getNumeroActual()));
				mantenedor.actualizar(folioDocumento);
				if (folioDocumento.getNivel()) {
					this.setUnidad(null);
				}
				ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento = new ReporteSolicitudFolioDocumento();
				reporteSolicitudFolioDocumento.setFiscalia(this.getFiscalia());
				reporteSolicitudFolioDocumento.setUnidad(this.getUnidad());
				reporteSolicitudFolioDocumento.setTipoDocumento(this.getTipoDocumento());
				reporteSolicitudFolioDocumento.setMateria(this.getMateria());
				reporteSolicitudFolioDocumento.setAgnoActual(Integer.parseInt(this.getAgnoActual()));
				reporteSolicitudFolioDocumento.setNumeroActual(Long.valueOf(this.getValor()));
				reporteSolicitudFolioDocumento.setObservacion(this.getObservacion());
				reporteSolicitudFolioDocumento.setUsuario(this.usuario);
				reporteSolicitudFolioDocumento.setFechaSolicitud(new Date());
				if (adminFolio.crear(reporteSolicitudFolioDocumento)) {
					final StringBuilder msg = new StringBuilder("El N° de folio para "	+ folioDocumento.getFiscalia().getDescripcion());
					if (folioDocumento.getUnidad() != null && folioDocumento.getUnidad().getId() > 0 ) {
						msg.append(", "	+ folioDocumento.getUnidad().getDescripcion());
					}
					msg.append(", "	+ folioDocumento.getTipoDocumento().getDescripcion());
					msg.append(" es: ");
					msg.append(folioDocumento.getNumeroActual() + "/");
					msg.append(folioDocumento.getAgnoActual());
					FacesMessages.instance().add(msg.toString());
					this.limpiarFolio();
				} else {
					FacesMessages.instance().add(NO_PUEDE_OBTENER);
					this.limpiarFolio();
				}
			} catch (PersistenceException e) {
				// ## mensaje para decir que no se puede obtener el folio
				FacesMessages.instance().add(NO_PUEDE_OBTENER);
				e.printStackTrace();
				this.limpiarFolio();
			}
		}
	}

	@Override
	public String getValor() {
		return valor;
	}

	@Override
	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String increment() {
		return null;
	}

	@Override
	public String getObservacion() {
		return observacion;
	}

	@Override
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Override
	public void nivelFiscalia() {
		if (folioDocumento.getNivel()) {
			this.idUnidad = jerarquia.INICIO;
			this.setHabilitaUnidad(true);
		} else {
			this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional().getId());
			this.setHabilitaUnidad(false);
		}
	}

	@Override
	public void setHabilitaUnidad(Boolean habilitaUnidad) {
		this.habilitaUnidad = habilitaUnidad;
	}

	@Override
	public Boolean getHabilitaUnidad() {
		return habilitaUnidad;
	}

	@Override
	public void setFolioDocumento(FolioDocumento folioDocumento) {
		this.folioDocumento = folioDocumento;
	}

	@Override
	public FolioDocumento getFolioDocumento() {
		return folioDocumento;
	}

	@Override
	public void limpiarFolio() {

		final Calendar toDay = Calendar.getInstance();
		this.setAgnoActual(String.valueOf(toDay.get(Calendar.YEAR)));
		this.setValor("0");
		this.setObservacion("");
		this.setMateria("");
		folioDocumento.setNivel(false);
		this.setHabilitaUnidad(false);
        this.setFiscalia(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional().getId());
		this.setTipoDocumento(Long.valueOf(-1));

		//
		// this.setHabilitaUnidad(false);
		// this.folioDocumento = new FolioDocumento();
		//
		// this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional()
		// .getDepartamento().getId());
		//
		// this.setTipoDocumento(jerarquias.INICIO.intValue());
		//
		// folioDocumento.setNivel(false);
		// final Calendar toDay = Calendar.getInstance();
		// this.valorAgno = Integer.toString(toDay.get(Calendar.YEAR));
		// this.valorInicial = "1";
		// this.editable = false;
	}
}
