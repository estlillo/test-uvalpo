package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearOficioReservado {

	void destroy();

	String end();

	void guardar();

	void visualizar();

	void firmar();

	void visar();

	String agregarDocumento();

	void guardarYCrearExpediente();

	Oficio getOficioReservado();

	String getParrafo();

	void setParrafo(String parrafo);

	String getUrlFirma();

	String buscaDocumento();

	List<SelectPersonas> getDistribucionDocumento();

	List<SelectPersonas> getDestinatariosDocumento();

	Boolean getFirmado();

	Boolean getVisado();

	List<VisacionDocumento> getVisaciones();

	String completar();

	Boolean getCompletado();

	boolean isEditable();

	boolean isRenderedBotonVolver();

	boolean isEditableSinVisa();

	boolean isDespachable();

	boolean isRenderedBotonFirmar();

	boolean isRenderedBotonFirmarAvanzado();

	boolean isRenderedBotonDespachar();

	boolean isRenderedBotonGuardar();

	boolean isRenderedBotonGuardarCrearExpediente();

	boolean isRenderedBotonVisar();

	boolean isRenderedBotonVisualizar();

	Boolean getValidarPanelApplet();

	void setValidarPanelApplet(Boolean validarPanelApplet);

	String guardaParaFirma();

	boolean verificaFirmaAvanzada();

	void validar();

	void resetValidar();

	boolean validarFirmaAvanzada();

	/* **************************************************************************
	 * Archivos Anexos - Antecedentes************************************************************************
	 */
	void limpiarAnexos();

	int getMaxFilesQuantity();

	void listener(UploadEvent event) throws Exception;

	void agregarAntecedenteArchivo();

	void verArchivo(Long idArchivo);

	// void eliminarAnexoDocumento(Integer numDoc);
	String getMateriaArchivo();

	void setMateriaArchivo(String materiaArchivo);

	// List<Documento> getListDocumentosAnexo();

	/* **************************************************************************
	 * DESPACHAR EXPEDIENTES************************************************************************
	 */
//	String despacharExpediente();

	List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	String getSessionId();

	String buscaDocumento1();

	String buscaDocumento2();
	
	void eliminarArchivoAntecedente(ArchivoAdjuntoDocumentoElectronico archivo);

	boolean isDocumentoValido();

	boolean isValidarAntecedente();

	void removeArchivoAnexo();

	void previsualizar();

}
