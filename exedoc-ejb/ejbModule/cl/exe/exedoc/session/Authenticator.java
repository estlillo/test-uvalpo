package cl.exe.exedoc.session;

import java.util.List;

import javax.persistence.EntityManager;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;

/**
 * @author Administrator
 */
@Name("authenticator")
public class Authenticator {

	@Out(required = false, scope = ScopeType.SESSION)
	private Persona usuario;

	@In
	private Credentials credentials;

	@In
	private Identity identity;

	@In
	private EntityManager entityManager;

	/**
	 * Constructor.
	 */
	public Authenticator() {
		super();
	}

	/**
	 * @return {@link boolean}
	 */
	@SuppressWarnings("rawtypes")
	public boolean authenticate() {
		final String username = credentials.getUsername().trim();
		System.out.println("Username: " + username);
		final List objetoUsuario = entityManager.createNamedQuery("Persona.findByUsuarioVigente")
				.setParameter("usuario", username).setParameter("vigente", Boolean.TRUE).getResultList();
		if (objetoUsuario != null && objetoUsuario.size() == 1) {
			usuario = (Persona) objetoUsuario.get(0);
			this.setRoles(usuario);
			return true;
		}
		this.identity.logout();
		FacesMessages.instance().add(
				"Existe un problema con su Usuario, si el problema persiste, comuniquese con el administrador");
		return false;
	}

	/**
	 * @param persona {@link Persona}
	 */
	private void setRoles(final Persona persona) {
		for (Rol rol : persona.getRoles()) {
			identity.addRole(rol.getDescripcion());
		}
		if (persona.getGrupos() != null && persona.getGrupos().size() > 0) {
			identity.addRole("GRUPO");
		}
		//identity.addRole(Rol.MENU_PRINCIPAL.getDescripcion());
		//identity.addRole(Rol.REPORTES.getDescripcion());
	}

	/**
	 * @return {@link String}
	 */
	public String getURLexedoc() {
		final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/exedoc";
		return urlDocumento;
	}
//TODO Implementar
	public boolean authenticateLDAP(String username, String password) {
		// TODO Auto-generated method stub
		return false;
	}

}
