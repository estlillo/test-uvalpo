package cl.exe.exedoc.session;

import java.util.Date;

import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.HistorialDespacho;
import cl.exe.exedoc.entity.Persona;


public interface AdministrarHistorialDespacho {
	
	
	public boolean guardarHistorial(HistorialDespacho guardar);

}
