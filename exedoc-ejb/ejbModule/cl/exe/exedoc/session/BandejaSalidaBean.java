package cl.exe.exedoc.session;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.HistorialDespacho;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ExpedientesRecibidos;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.pojo.expediente.FilaBandejaSalida;

@Stateful
@Scope(ScopeType.SESSION)
@Name("bandejaSalida")
public class BandejaSalidaBean implements BandejaSalida {

	private static final String GUION = " - ";
	private static final String ID = "id";
	private static final String REPETIDO = "repetidos";

	@PersistenceContext
	EntityManager em;
	
	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private AdministrarHistorialDespacho adminDpacho; //24-02
	
	private Map<Long, Boolean> selectedDespachados = new HashMap<Long, Boolean>();
	private Map<Long, Boolean> selectedDesiertos = new HashMap<Long, Boolean>();
	private Map<Long, Boolean> selectedAnular = new HashMap<Long, Boolean>();

	private List<Long> idExpedientesDesertar = new ArrayList<Long>();

	private List<ExpedienteBandejaSalida> listExpedientes;
	private List<FilaBandejaSalida> listaBandeja;
	private HashMap<String, ExpedientesRecibidos> recibidos;
	private List<GrupoUsuario> grupoUsuarios = new ArrayList<GrupoUsuario>();
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho = false;

	private boolean validarComplete;
	private boolean estadoDesertar;
	
	@Begin(join = true)
	public String begin() {
		desdeDespacho = false;
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja de Salida", "inicio", new Date());
		log.info("beginning conversation");
		listExpedientes = new ArrayList<ExpedienteBandejaSalida>();
		listaBandeja = new ArrayList<FilaBandejaSalida>();
		recibidos = new HashMap<String, ExpedientesRecibidos>();
		this.buscarExpedientes();
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja de Salida", "fin", new Date());
		return "bandejaSalida";
	}

	
	@SuppressWarnings("deprecation")
	@Override
	public void despacharExpedientes() {
		final Set<Long> idExpedientes = selectedDespachados.keySet();
		boolean noEnviados = false;
		List<Long> expPadres = new ArrayList<Long>();

		int count = 0;
		int sinDest=0;
		int conDest=0;
		for (Long id : idExpedientes) {
			if ((Boolean) selectedDespachados.get(id)) {
				log.info("Despachando IdExpediente: " + id);
				Expediente expediente = me.buscarExpediente(id);
				if (expediente.getVersionPadre() != null) {
					if (!expPadres.contains(expediente.getVersionPadre().getId())) {
						expPadres.add(expediente.getVersionPadre().getId());
					}
				} else {
					sinDest++;
				}
				count++;
			}
		}
		for (Long id : expPadres) {
			Expediente expediente = em.find(Expediente.class, id);
			final List<Expediente> expedientes = me.buscarExpedientesHijos(expediente);
			if (expedientes != null && expedientes.size() != 0) {
				me.despacharExpediente(expediente);
				for (Expediente e : expedientes) {
					me.despacharExpediente(e);
					HistorialDespacho guardar = new HistorialDespacho();
					guardar.setExpediente(e);
					guardar.setEmisor(e.getEmisor());
					guardar.setDestinatario(e.getDestinatario());
					guardar.setFechaDespacho(e.getFechaDespacho());
					em.persist(guardar);
				}
				desdeDespacho = true;	
				conDest ++;
				//FacesMessages.instance().add(new FacesMessage("Expediente(s) Despachado(s)."));
				
			} else {
				sinDest++;;
			}
		}
		
		if (count == 0) {
			FacesMessages.instance().add(Severity.WARN, "Debe seleccionar al menos un expediente.");
			return;
		}

		if (noEnviados) {
			
			sinDest++;		
		//FacesMessages.instance().add(Severity.WARN,"No fue posible enviar algunos expedientes, pues no tienen destinatarios.");
		}
		if(sinDest==0 && conDest!=0){
			FacesMessages.instance().add(Severity.WARN, "Expediente(s) Despachado(s)."); 
		} else if(sinDest!=0 && conDest!=0){
			FacesMessages.instance().add(Severity.WARN, "Algunos expedientes no fueron despachados porque no tenian destinatarios"); 
		}else if(sinDest!=0 && conDest==0){
			FacesMessages.instance().add(Severity.WARN, "Los expedientes no fueron despachados porque no tenian destinatarios"); 
		}
		buscarExpedientes();
	}

	public void setListaDesertar(Long id) { 
		if (selectedDesiertos.containsKey(id)) {
			idExpedientesDesertar.remove(id);
			selectedDesiertos.remove(id);
		} else {
			idExpedientesDesertar.add(id);
			selectedDesiertos.put(id, true);
		}
	}

	private void limpiarBandejaSalida() {
		Collection<ExpedienteBandejaSalida> listTemp = new ArrayList<ExpedienteBandejaSalida>();
		for (ExpedienteBandejaSalida expedienteBandeja : listExpedientes) {
			if (!idExpedientesDesertar.contains(expedienteBandeja.getId())) {
				listTemp.add(expedienteBandeja);
			}
		}
		listExpedientes.clear();
		listExpedientes.addAll(listTemp);
	}

	private Set<Persona> buscarDestinatarios(long idExpediente) {

		Set<Persona> listDestinatarios = new HashSet<Persona>();
		grupoUsuarios = new ArrayList<GrupoUsuario>();
		Expediente expediente = null;

		expediente = me.buscarExpediente(idExpediente);

		if (expediente != null) {
			List<Expediente> expedientes = me.buscarExpedientesHijos(expediente);
			for (Expediente e : expedientes) {
				try {
					if (e.getDestinatario() != null){
						e.getDestinatario().setDestinatarioConCopia(e.getCopia());
						listDestinatarios.add(e.getDestinatario());
					} else {
						if(e.getDestinatarioHistorico() != null){
							e.getDestinatarioHistorico().setDestinatarioConCopia(e.getCopia());
							listDestinatarios.add(e.getDestinatarioHistorico());
						}
						else{
							if(e.getGrupo() != null){
								
								if(!grupoUsuarios.contains(e.getGrupo())){
									
									grupoUsuarios.add(e.getGrupo());
								}
							}
						}
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}

		return listDestinatarios;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedientes() {
		selectedDespachados.clear();
		selectedDesiertos.clear();
		idExpedientesDesertar.clear();
		selectedAnular.clear();
		
		 listaBandeja = new ArrayList<FilaBandejaSalida>();
		
		if(listExpedientes == null){
			listExpedientes = new ArrayList<ExpedienteBandejaSalida>();
		}
		else{
			listExpedientes.clear();
		}
		//listExpedientes.addAll(me.listarExpedientesDespachadosPorUsuario(usuario));
//		long ini = System.currentTimeMillis();
//		long fin; 
		
		
		/*Se habilita sql nativo para empaquetamiento*/
		final StringBuilder sqlNatBS = new StringBuilder();
//		sqlNatBS.append("SELECT DISTINCT b.id, b.fecha_despacho_historico, b.NUMERO_expediente, b.FECHA_INGRESO, b.id_destinatario_historico, ");
//		sqlNatBS.append("b.acuse_recibo, tipo.DESCRIPCION tipoDocumento, docu.NUMERO_DOCUMENTO, docu.FECHA_DOCUMENTO_ORIGEN, docu.MATERIA, ");
//		sqlNatBS.append("docu.EMISOR, tp.DESCRIPCION formato, '' destDoc, docu.ID, docu.PLAZO, docu.COMPLETADO, 8 Tipo, ");
//		sqlNatBS.append("gr.NOMBRE, p.NOMBRES || ' ' || p.apellido_paterno, ca.descripcion cargo, un.descripcion unidad, b.id_padre ");
//		sqlNatBS.append("FROM expedientes  b ");
//		sqlNatBS.append("left join grupousuario gr on (gr.id = b.id_grupo) ");
//		sqlNatBS.append("left join personas p on (b.id_destinatario_historico is not null and p.id = b.id_destinatario_historico) ");
//		sqlNatBS.append("left join cargos ca on (ca.id = p.id_cargo) ");
//		sqlNatBS.append("left join unidades un on (un.id=ca.id_unidad_organizacional) ");
//		sqlNatBS.append("left join documentos_expedientes doc on(doc.id_expediente = b.id and doc.id_tipo_documento_expediente = 1) ");
//		sqlNatBS.append("left join documentos docu on(docu.id = doc.id_documento) ");
//		//sqlNatBS.append("left join personas_documento pd on (pd.id_documento = docu.id and pd.id_tipo_lista = 1) ");
//		sqlNatBS.append("left join formatos_documentos tp on (tp.id = docu.id_formato) ");
//		sqlNatBS.append("left join tipos_documentos tipo on (tipo.id = docu.id_tipo_documento) ");
//		sqlNatBS.append("WHERE id_emisor_historico = " + usuario.getId() + " and b.fecha_acuse_recibo is not null ");
//		sqlNatBS.append("and CAST(fecha_acuse_recibo AS DATE) BETWEEN (CAST(CURRENT_TIMESTAMP AS DATE) -1) and CAST(CURRENT_TIMESTAMP AS DATE) ");
		
//		sqlNatBS.append("UNION ");
//		sqlNatBS.append("SELECT DISTINCT b.id, b.fecha_despacho_historico, b.NUMERO_expediente, b.FECHA_INGRESO, b.id_destinatario_historico, b.acuse_recibo, ");
//		sqlNatBS.append("tipo.DESCRIPCION tipoDocumento, docu.NUMERO_DOCUMENTO, docu.FECHA_DOCUMENTO_ORIGEN, docu.MATERIA, docu.EMISOR, tp.DESCRIPCION formato, ");
//		sqlNatBS.append("'' destDoc, docu.ID, docu.PLAZO, docu.COMPLETADO, 4 Tipo, gr.NOMBRE, p.NOMBRES || ' ' || p.apellido_paterno, ca.descripcion cargo, un.descripcion unidad, ");
//		sqlNatBS.append("b.id_padre ");
//		sqlNatBS.append("FROM expedientes  b ");
//		sqlNatBS.append("left join grupousuario gr on (gr.id = b.id_grupo) ");
//		sqlNatBS.append("left join personas p on (b.id_destinatario_historico is not null and p.id = b.id_destinatario_historico) ");
//		sqlNatBS.append("left join cargos ca on (ca.id = p.id_cargo) ");
//		sqlNatBS.append("left join unidades un on (un.id=ca.id_unidad_organizacional) ");
//		sqlNatBS.append("left join documentos_expedientes doc on(doc.id_expediente = b.id and doc.id_tipo_documento_expediente = 1) ");
//		sqlNatBS.append("left join documentos docu on(docu.id = doc.id_documento) ");
//		//sqlNatBS.append("left join personas_documento pd on (pd.id_documento = docu.id and pd.id_tipo_lista = 1) ");
//		sqlNatBS.append("left join formatos_documentos tp on (tp.id = docu.id_formato_documento) ");
//		sqlNatBS.append("left join tipos_documentos tipo on (tipo.id = docu.id_tipo_documento) ");
//		sqlNatBS.append("WHERE b.id in ( ");
//		sqlNatBS.append("	SELECT e.id ");
//		sqlNatBS.append("	FROM Expedientes e "); 
//		sqlNatBS.append("	WHERE e.id_emisor_historico = " + usuario.getId() + " and e.fecha_acuse_recibo is not null and e.cancelado is null ");
//		sqlNatBS.append("	and CAST(fecha_acuse_recibo AS DATE) BETWEEN (CAST(CURRENT_TIMESTAMP AS DATE) -1) and CAST(CURRENT_TIMESTAMP AS DATE) ) ");
		
//		sqlNatBS.append("UNION ");
		
		//-- despachado con acuse de recibo
		sqlNatBS.append("SELECT DISTINCT b.id, b.fecha_despacho_historico fecha_despacho, b.NUMERO_expediente, b.FECHA_INGRESO, b.id_destinatario, b.fecha_acuse_recibo, d.DESCRIPCION tipoDocumento, ");
		sqlNatBS.append("c.NUMERO_DOCUMENTO, c.FECHA_DOCUMENTO_ORIGEN, c.MATERIA, c.EMISOR, e.DESCRIPCION formato, b.copia, c.ID, ");
		sqlNatBS.append("c.PLAZO, c.COMPLETADO, 8 Tipo, gr.NOMBRE, p.NOMBRES || ' ' || p.apellido_paterno, ca.descripcion cargo, un.descripcion unidad, b.id_padre ");
		sqlNatBS.append("FROM expedientes b ");
		sqlNatBS.append("left join documentos_expedientes a on a.id_expediente = b.id ");
		sqlNatBS.append("left join documentos c on a.id_documento = c.id ");
		sqlNatBS.append("left join grupousuario gr on (gr.id = b.id_grupo) ");
		sqlNatBS.append("left join personas p on (p.id = b.id_destinatario_historico) ");
		sqlNatBS.append("left join cargos ca on (ca.id = p.id_cargo) ");
		sqlNatBS.append("left join unidades un on (un.id=ca.id_unidad_organizacional) ");
		//sqlNatBS.append("left join personas_documento pd on (pd.id_documento = c.id and pd.id_tipo_lista = 1) ");
		sqlNatBS.append("left join tipos_documentos d on d.id = c.id_tipo_documento ");
		sqlNatBS.append("left join formatos_documentos e on e.id = c.id_formato ");
		sqlNatBS.append("WHERE ( a.id_expediente, c.fecha_creacion ) in ( ");
		sqlNatBS.append("		select w.iddocexp,  max(w.docfc) ");
		sqlNatBS.append("		from ( select  docexp.id_expediente iddocexp, doc.fecha_creacion docfc ");
		sqlNatBS.append("				from Documentos_expedientes docexp, expedientes  exp, documentos doc ");
		sqlNatBS.append("				where docexp.id_expediente  = exp.id and docexp.id_documento  = doc.id and ");
		sqlNatBS.append("				( (exp.id_emisor_historico = " + usuario.getId() + " ");
		sqlNatBS.append("					and exp.reasignado is null and exp.cancelado is null ");
		sqlNatBS.append("					and exp.fecha_acuse_recibo between CURRENT_DATE and (CURRENT_DATE +1)) ");
		sqlNatBS.append("				) )w group by w.iddocexp ) ");
		
		sqlNatBS.append("UNION ");
		
		// -- despachado sin acuse de recibo
		sqlNatBS.append("SELECT DISTINCT b.id, b.fecha_despacho, b.NUMERO_expediente, b.FECHA_INGRESO, b.id_destinatario, ");
		sqlNatBS.append("b.fecha_acuse_recibo, d.DESCRIPCION tipoDocumento, c.NUMERO_DOCUMENTO, c.FECHA_DOCUMENTO_ORIGEN, c.MATERIA, ");
		sqlNatBS.append("c.EMISOR, e.DESCRIPCION formato, b.copia, c.ID, c.PLAZO, c.COMPLETADO, 2 Tipo, gr.NOMBRE, ");
		sqlNatBS.append("p.NOMBRES || ' ' || p.apellido_paterno, ca.descripcion cargo, un.descripcion unidad, b.id_padre ");
		sqlNatBS.append("FROM expedientes b ");
		sqlNatBS.append("left join documentos_expedientes a on a.id_expediente = b.id ");
		sqlNatBS.append("left join grupousuario gr on (gr.id = b.id_grupo) ");
		sqlNatBS.append("left join personas p on (p.id = b.id_destinatario) ");
		sqlNatBS.append("left join cargos ca on (ca.id = p.id_cargo) ");
		sqlNatBS.append("left join unidades un on (un.id=ca.id_unidad_organizacional) ");
		sqlNatBS.append("left join documentos c on a.id_documento = c.id ");
		//sqlNatBS.append("left join personas_documento pd on (pd.id_documento = c.id and pd.id_tipo_lista = 1) ");
		sqlNatBS.append("left join tipos_documentos d on d.id = c.id_tipo_documento ");
		sqlNatBS.append("left join formatos_documentos e on e.id = c.id_formato ");
		sqlNatBS.append("WHERE ( a.id_expediente, c.fecha_creacion ) in ");
		sqlNatBS.append("	( select w.iddocexp,  max(w.docfc) ");
		sqlNatBS.append("		from ");
		sqlNatBS.append("		( select  docexp.id_expediente iddocexp, doc.fecha_creacion docfc ");
		sqlNatBS.append("			from Documentos_expedientes docexp, expedientes  exp, documentos doc ");
		sqlNatBS.append("			where docexp.id_expediente = exp.id and docexp.id_documento = doc.id ");
		sqlNatBS.append("			and ( (exp.id_emisor = " + usuario.getId() + " and  ( exp.id_destinatario is not  null or exp.id_grupo is not null) ");
		sqlNatBS.append("			and exp.reasignado is null ");
		sqlNatBS.append("			and exp.cancelado is null and exp.fecha_despacho is  not null  and  exp.fecha_acuse_recibo is null) ");
		sqlNatBS.append("	) )w group by w.iddocexp ) ");
		
		sqlNatBS.append("UNION ");
		
		sqlNatBS.append("SELECT DISTINCT b.id, b.fecha_despacho, b.NUMERO_expediente, b.FECHA_INGRESO, b.id_destinatario, b.fecha_acuse_recibo, ");
		sqlNatBS.append("d.DESCRIPCION tipoDocumento, c.NUMERO_DOCUMENTO, c.FECHA_DOCUMENTO_ORIGEN, c.MATERIA, c.EMISOR, e.DESCRIPCION formato, ");
		sqlNatBS.append("b.copia, c.ID, c.PLAZO, c.COMPLETADO, 1 Tipo, gr.NOMBRE, p.NOMBRES || ' ' || p.apellido_paterno, ");
		sqlNatBS.append("ca.descripcion cargo, un.descripcion unidad, b.id_padre ");
		sqlNatBS.append("FROM expedientes b ");
		sqlNatBS.append("left join documentos_expedientes a on a.id_expediente = b.id ");
		sqlNatBS.append("left join grupousuario gr on (gr.id = b.id_grupo) ");
		sqlNatBS.append("left join personas p on (p.id = b.id_destinatario) ");
		sqlNatBS.append("left join cargos ca on (ca.id = p.id_cargo) ");
		sqlNatBS.append("left join unidades un on (un.id=ca.id_unidad_organizacional) ");
		sqlNatBS.append("left join documentos c on a.id_documento = c.id ");
		//sqlNatBS.append("left join personas_documento pd on (pd.id_documento = c.id and pd.id_tipo_lista = 1) ");
		sqlNatBS.append("left join tipos_documentos d on d.id = c.id_tipo_documento ");
		sqlNatBS.append("left join formatos_documentos e on e.id = c.id_formato ");
		sqlNatBS.append("WHERE ( a.id_expediente, c.fecha_creacion ) in ");
		sqlNatBS.append("	( select w.iddocexp,  max(w.docfc) ");
		sqlNatBS.append("		from ( select docexp.id_expediente iddocexp, doc.fecha_creacion docfc ");
		sqlNatBS.append("				from Documentos_expedientes docexp, documentos doc, expedientes exp ");
		sqlNatBS.append("				left join expedientes padre on (padre.id = exp.id_padre) ");
		sqlNatBS.append("				left join expedientes hijos on (hijos.id_padre = exp.id) ");
		sqlNatBS.append("				where docexp.id_expediente = exp.id and docexp.id_documento = doc.id ");
		sqlNatBS.append("				    and exp.id_emisor = " + usuario.getId() + " and exp.fecha_acuse_recibo is null ");
		sqlNatBS.append("					and exp.fecha_despacho is null and exp.cancelado is null ");
		//sqlNatBS.append("					and exp.id_grupo is null and padre.id_padre is null ");
		sqlNatBS.append("                   and ((exp.id_padre is null and exp.archivado is null) or (exp.id_padre is not null and padre.archivado is null))  ");
		sqlNatBS.append("					and (((exp.id_padre is null and hijos.id is null) and not(exp.id_padre is not null and padre.id_padre is null)) ");
		sqlNatBS.append("						or (not(exp.id_padre is null and hijos.id is null) and (exp.id_padre is not null and padre.id_padre is null))) ");
		sqlNatBS.append("					and exp.fecha_despacho_historico is null ");
		
		sqlNatBS.append("	)w group by w.iddocexp )");
		
//		sqlBS.append("where e.emisor.id = :usuario and e.fechaAcuseRecibo is null ");
//		sqlBS.append("and e.fechaDespacho is null and e.cancelado is null ");
//		sqlBS.append("and (((e.versionPadre is null and hijos is null) and not(e.versionPadre is not null and padre.versionPadre is null)) ");
//		sqlBS.append("   or (not(e.versionPadre is null and hijos is null) and (e.versionPadre is not null and padre.versionPadre is null))) ");
//		sqlBS.append("and e.fechaDespachoHistorico is null and e.archivado is null ");
		
		//System.out.println(sqlNatBS.toString());
		final Query queryBS = em.createNativeQuery(sqlNatBS.toString());
		final List<Object[]> expedientesBS = queryBS.getResultList();
		
		armarBandejaFilasBandeja(expedientesBS);
		
		//listado desde el procedimiento almacenado
//		String bandejaSalida = "{?=call BANDEJA_SALIDA.fn_bdja_slda(?)}";
//        ResultSet rs = null;
//        CallableStatement cs = null;
//        CallableStatement csg = null;
//        CallableStatement BS = null;
//        Connection connection = null;
//        
//        try {
//            connection = DaoFactory.getDaoFactoryConnection();
//
//            BS = connection.prepareCall(bandejaSalida);
//            
//            
//            //Para la bandeja de salida PROCEDIMIENTO
//            BS.registerOutParameter(1,OracleTypes.CURSOR);
//            int usr = usuario.getId().intValue();
//            BS.setInt(2, usr);
//            BS.executeQuery();
//            rs = (ResultSet) BS.getObject(1);
//            fin = System.currentTimeMillis();
//			//System.out.println("tiempo consulta procedimiento "+(fin-ini));
//			ini = System.currentTimeMillis();
//            
//            armarBandejaFilasBandeja(rs);
//		
//			fin = System.currentTimeMillis();
//			//System.out.println("tiempo armado "+(fin-ini));
//            connection.close();
//        	}
//         	catch (Exception e) {
//         			e.printStackTrace();
//         }
          
	//#########################################
//        ini = System.currentTimeMillis();
			

		
//		this.cargarDestinatariosExpedientes(listExpedientes);
		
//		fin = System.currentTimeMillis();
	//	System.out.println("tiempo 2: "+(fin-ini));
//		ArrayList<ExpedienteBandejaSalida> listExpedientesAux = new ArrayList<ExpedienteBandejaSalida>();
//		for (int i = 0; i < listExpedientes.size(); i++) {
//			ExpedienteBandejaSalida array_element = listExpedientes.get(i);
//			if (this.buscarEstadoAnularDespacho(array_element)) {
//				// no se puede eliminar por que ya se acuso recibo
//				array_element.setDevolver(true);
//			} else {
//				array_element.setDevolver(false);
//			}
//			listExpedientesAux.add(array_element);
//		}
//		listExpedientes.clear();
//		listExpedientes.addAll(listExpedientesAux);
//		Collections.sort(listExpedientes, new Comparator<ExpedienteBandejaSalida>() {
//			public int compare(ExpedienteBandejaSalida o1, ExpedienteBandejaSalida o2) {
//				return o1.getFechaIngreso().compareTo(o2.getFechaIngreso());
//			}
//		});
		for (FilaBandejaSalida f : listaBandeja) {
			f.setAnular(puedeAnular(f.getNumeroExpediente(),f.getId_expediente(),f.getIdPadre(),f.getRecibidoFila()));
			f.setRepetido(estaNumeroExpediente(f.getNumeroExpediente(), f.getId_expediente()));
		}
		Collections.sort(listaBandeja, new Comparator<FilaBandejaSalida>() {
			public int compare(FilaBandejaSalida o1, FilaBandejaSalida o2) {
				return o1.getFechaIngreso().compareTo(o2.getFechaIngreso());
			}
		});
		desdeDespacho = Boolean.FALSE;
	}

	public void armarBandeja(ResultSet rs)
	{
		try {
			
		while(rs.next()){
        	
        	long id = rs.getLong(1);
        	Expediente e = me.buscarExpediente(id);
        	final ExpedienteBandejaSalida ebsg = new ExpedienteBandejaSalida();
        	
        	if(!estaEspediente(e.getNumeroExpediente())){
        		ebsg.setId(e.getId());
			if(e.getFechaDespacho() != null ){
				ebsg.setDespachado(true);
			}
			else {
				ebsg.setDespachado(false);
			}
			ebsg.setNumeroExpediente(e.getNumeroExpediente());
			ebsg.setFechaIngreso(e.getFechaIngreso());
			if (e.getVersionPadre() == null) {
				ebsg.setExpedienteNuevo(true);
			} else {
				ebsg.setExpedienteNuevo(false);
			}

			ebsg.setDestinatarioExpediente(e.getDestinatario()); //23-02-2012
			

			final List<DocumentoExpediente> docs = e.getDocumentos();
			Documento doc = null;
			boolean respuesta = false;
			for (int i = docs.size() - 1; i > 0; i--) {
				if (docs.get(i).getTipoDocumentoExpediente()
						.equals(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA))) {
					doc = docs.get(i).getDocumento();
					respuesta = true;
					break;
				}
			}
			boolean hayDoc = true;
			if (!respuesta) {
				if (e.getDocumentos() != null && e.getDocumentos().size() != 0) {
					doc = e.getDocumentos().get(0).getDocumento();
				} else {
					doc = new Documento();
					hayDoc = false;
				}
			}
			if (hayDoc) {
				ebsg.setNumeroDocumento(doc.getNumeroDocumento());
				ebsg.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
				if (doc.getReservado()) {
					ebsg.setMateria("Materia Confidencial");
				} else {
					ebsg.setMateria(doc.getMateria());
				}
				ebsg.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
				ebsg.setEmisor(doc.getEmisor());

				final StringBuilder sb = new StringBuilder();

				if (doc.getTipoDocumento() != null && (
						doc.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION))) {
					if (e.getDestinatario() != null) {
						sb.append(e.getDestinatario().getNombreApellido());
						sb.append(" ");
					} else {
						if (e.getDestinatarioHistorico() != null) {
							sb.append(e.getDestinatarioHistorico().getNombreApellido());
							sb.append(" ");
						}
					}
				} else {
						
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
				}
				if (sb.length() != 0) {
					ebsg.setDestinatario(sb.toString());
					
				} else {
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
					ebsg.setDestinatario(sb.toString());
					//ebsg.setDestinatarioExpedienteString(sb.toString()); //23-02-2012
				}
				ebsg.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			}
			ebsg.setRecibido(e.getAcuseRecibo());

			ebsg.setPlazo(doc.getPlazo());
			boolean completado = true;
			for (DocumentoExpediente de : docs) {
				final Documento docTmp = de.getDocumento();
				if (docTmp.getCompletado() != null && !docTmp.getCompletado()) {
					if (docTmp.getPlazo() != null && docTmp.getPlazo().before(new Date())) {
						completado = false;
						break;
					}
				}
			}
			
			ebsg.setCompletado(completado);
			ebsg.setGrupoId(e.getGrupo());
			ebsg.getDestinatarioExpedienteString(); //--
			
			//24-02 - Muestra el nombre del destinatario de un expediente (grupo)
			if(e.getGrupo() != null)  
			{
				ebsg.setDestinatarioExpedienteString(e.getGrupo().getNombre());
			}
			
			//verifica que el expediente no se repita en la bandeja de salida
			if(!estaEspediente(ebsg.getNumeroExpediente()))
			{
				listExpedientes.add(ebsg);
			}
			
           }
        } //FIN DEL CICLO WHILE
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public boolean recibido(String numero){
		
		if(recibidos.containsKey(numero)){
			return !recibidos.get(numero).getRecibidos().contains(false);
		}
		return false;
	}
	
//	public void armarBandejaFilasBandeja(ResultSet rs){
//			try {
//				while(rs.next()){
//					
//					FilaBandejaSalida fila = new FilaBandejaSalida();
//					
//					long idDestinatario = rs.getLong(5);
//					int acuseRecibo = rs.getInt(6);
//				
//					ExpedientesRecibidos exR = new ExpedientesRecibidos();
////					if(recibidos.containsKey(rs.getString(3)))exR = recibidos.get(rs.getString(3));
////					exR.setNumeroExpediente(rs.getString(3));
////					if(acuseRecibo == 1){
////						exR.setRecibido(true);
////						recibidos.put(rs.getString(3), exR);
////					}
////					if(acuseRecibo == 0){
////						exR.setRecibido(false);
////						recibidos.put(rs.getString(3), exR);
//					//rs.getInt("TIPO")
//					
////					}
//					if(rs.getInt("TIPO")==8 || rs.getInt("TIPO")==4){
//						acuseRecibo=1;
//					}
//					
//					if(rs.getBoolean("ELIMINADO"))fila.setEliminado(rs.getBoolean("ELIMINADO"));
//					else fila.setEliminado(false);
//					fila.setId_expediente(rs.getLong(1));
//					fila.setFechaDespacho(rs.getDate(2));
//					fila.setNumeroExpediente(rs.getString(3));
//					fila.setFechaIngreso(rs.getDate(4));
//					fila.setTipo(rs.getString("DESCRIPCION"));
//					fila.setFormato(rs.getString(12));
//					fila.setNombreGrupo(rs.getString("NOMBRE"));
//					fila.setNumeroDocumento(rs.getString("NUMERO_DOCUMENTO"));
//					fila.setFechaDocumentoOrigen(rs.getDate("FECHA_DOCUMENTO_ORIGEN"));
//					fila.setMateria( rs.getString("MATERIA"));
//					fila.setEmisor(rs.getString("EMISOR"));
//					fila.setIdPadre(rs.getLong("ID_PADRE"));
//					
//					fila.setRecibido(acuseRecibo);
//					String destinatario = "";
//					if(rs.getString(19) != null) destinatario = rs.getString(19);
//					if(rs.getString(19) != null) destinatario =destinatario + GUION;
//					if(rs.getString(19) != null && rs.getString(20) != null) destinatario = destinatario + rs.getString(20);
//					if(rs.getString(20) != null && rs.getString(20) != null) destinatario = destinatario + GUION;
//					if(rs.getString(19) != null && rs.getString(20) != null && rs.getString(21) != null) destinatario =destinatario+ rs.getString(21);
//					fila.setDestinatario((fila.getNombreGrupo() != null && fila.getNombreGrupo().length() > 0)? fila.getNombreGrupo()+GUION+destinatario:destinatario);
//					fila.setDestinatarioDocumento(rs.getString("DESTINATARIO_EXTERNO"));
//					fila.setDesierto(rs.getBoolean("DESIERTO"));
//					if(!estaNumeroEspediente(fila.getNumeroExpediente(), fila.getDestinatario().length()))
//					listaBandeja.add(fila);
//					
//					
//					
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//	}
	
	public void armarBandejaFilasBandeja(List<Object[]> expBS){
		
		for (Object[] e : expBS) {
			try {
				FilaBandejaSalida fila = new FilaBandejaSalida();

				Boolean acuseRecibo = false;// = e[5] != null ? Integer.valueOf(e[5].toString()) : 0;
				if (e[5] != null) {
					acuseRecibo = true;
				}
//				if (e[5] != null) {
//					if (e[5] instanceof java.lang.Boolean)
//						acuseRecibo = Boolean.valueOf(e[5].toString());
//					else acuseRecibo = Long.valueOf(e[5].toString()) == 1 ? true : false;
//				}
//				else acuseRecibo = false;
				
//				if (Integer.valueOf(e[16].toString()) == 8
//						|| Integer.valueOf(e[16].toString()) == 4) {
//					acuseRecibo = true;
//				}

				

				fila.setId_expediente(Long.valueOf(e[0].toString()));
				fila.setFechaDespacho((Date) e[1]);
				fila.setNumeroExpediente(e[2].toString());
				fila.setFechaIngreso((Date) e[3]);
				fila.setTipo(e[6].toString());
				fila.setFormato(e[11] != null ? e[11].toString() : "");
				fila.setNombreGrupo(e[17] != null ? e[17].toString() : "");
				fila.setNumeroDocumento(e[7].toString());
				fila.setFechaDocumentoOrigen((Date) e[8]);
				fila.setMateria(e[9].toString());
				fila.setEmisor(e[10].toString());
				if (e[21] != null)
					fila.setIdPadre(Long.valueOf(e[21].toString()));
				else fila.setIdPadre(new Long(0));
				
				
//				fila.setRecibido(acuseRecibo);
				fila.setRecibidoFila(acuseRecibo);
				if (acuseRecibo) {
					fila.setAnular(false);
				}
				
				String destinatario = "";
				if (e[18] != null)
					destinatario = e[18].toString();
				if (e[19] != null)
					destinatario += GUION + e[19].toString();
				if (e[20] != null)
					destinatario += GUION + e[20].toString();
				fila.setDestinatario((fila.getNombreGrupo() != null && fila
						.getNombreGrupo().length() > 0) ? (fila.getNombreGrupo() + " (GRUPO)") : destinatario);
				
				fila.setCopia(e[12] != null ? (Boolean)e[12] : false);
			
//				//TODO debe ser puesto en el mismo nivel de la validacion del poder anular
//				fila.setRepetido(estaNumeroExpediente(fila.getNumeroExpediente(), fila.getDestinatario().length()));
				
//				if (!estaNumeroEspediente(fila.getNumeroExpediente(), fila
//						.getDestinatario().length()))
					listaBandeja.add(fila);
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void armarBandejaPROCEDIMIENTO(ResultSet rs)
	{
		try {
			
		while(rs.next()){
        	
        	long id = rs.getLong(1);
        	Date fechaDespacho = rs.getDate(2);
        	String numeroExpediente = rs.getString(3);
        	Date fechaIngreso = rs.getDate(4);
        	long idPadre = rs.getLong(5);
//        	long idDestinatario = rs.getLong(6);
        	int acuseRecibo = rs.getInt(7);
//        	long idGrupo = rs.getInt(8);
        	
        	
        	
        	Expediente e = me.buscarExpediente(id);
        	final ExpedienteBandejaSalida ebsg = new ExpedienteBandejaSalida();
        	
        	if(!estaEspediente(numeroExpediente)){
        		ebsg.setId(id);
			if(fechaDespacho != null ){
				ebsg.setDespachado(true);
			}
			else {
				ebsg.setDespachado(false);
			}
			ebsg.setNumeroExpediente(numeroExpediente);
			ebsg.setFechaIngreso(fechaIngreso);
			if (idPadre == 0L ) {
				ebsg.setExpedienteNuevo(true);
			} else {
				ebsg.setExpedienteNuevo(false);
			}
			ebsg.setDestinatarioExpediente(e.getDestinatario()); //23-02-2012
			
			
			
			final List<DocumentoExpediente> docs = e.getDocumentos();
			Documento doc = null;
			boolean respuesta = false;
			for (int i = docs.size() - 1; i > 0; i--) {
				if (docs.get(i).getTipoDocumentoExpediente()
						.equals(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA))) {
					doc = docs.get(i).getDocumento();
					respuesta = true;
					break;
				}
			}
			boolean hayDoc = true;
			if (!respuesta) {
				if (e.getDocumentos() != null && e.getDocumentos().size() != 0) {
					doc = e.getDocumentos().get(0).getDocumento();
				} else {
					doc = new Documento();
					hayDoc = false;
				}
			}
			if (hayDoc) {
				ebsg.setNumeroDocumento(doc.getNumeroDocumento());
				ebsg.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
				if (doc.getReservado()) {
					ebsg.setMateria("Materia Confidencial");
				} else {
					ebsg.setMateria(doc.getMateria());
				}
				ebsg.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
				ebsg.setEmisor(doc.getEmisor());

				final StringBuilder sb = new StringBuilder();

				if (doc.getTipoDocumento() != null && (
						doc.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION))) {
					if (e.getDestinatario() != null) {
						sb.append(e.getDestinatario().getNombreApellido());
						sb.append(" ");
					} else {
						if (e.getDestinatarioHistorico() != null) {
							sb.append(e.getDestinatarioHistorico().getNombreApellido());
							sb.append(" ");
						}
					}
				} else {
						
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
				}
				if (sb.length() != 0) {
					ebsg.setDestinatario(sb.toString());
					
				} else {
					for (ListaPersonasDocumento dd : doc.getDestinatarios()) {
						if (dd instanceof DestinatarioDocumento) {
							sb.append(dd.getDestinatario() + " ");
						}
					}
					ebsg.setDestinatario(sb.toString());
					//ebsg.setDestinatarioExpedienteString(sb.toString()); //23-02-2012
				}
				ebsg.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			}
			ebsg.setRecibido(acuseRecibo == 0 ? false :true );

			ebsg.setPlazo(doc.getPlazo());
			boolean completado = true;
			for (DocumentoExpediente de : docs) {
				final Documento docTmp = de.getDocumento();
				if (docTmp.getCompletado() != null && !docTmp.getCompletado()) {
					if (docTmp.getPlazo() != null && docTmp.getPlazo().before(new Date())) {
						completado = false;
						break;
					}
				}
			}
			
			ebsg.setCompletado(completado);
			ebsg.setGrupoId(e.getGrupo());
			
			
			//24-02 - Muestra el nombre del destinatario de un expediente (grupo)
			if(e.getGrupo() != null)  
			{
				ebsg.setDestinatarioExpedienteString(e.getGrupo().getNombre());
			}
			
			//verifica que el expediente no se repita en la bandeja de salida
			if(!estaEspediente(ebsg.getNumeroExpediente()))
			{
				listExpedientes.add(ebsg);
			}
			
           }
        } //FIN DEL CICLO WHILE
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	public boolean estaEspediente(String numeroExpediente){
		boolean esta = false;
		
		for (ExpedienteBandejaSalida bandeja : listExpedientes) {
			if(bandeja.getNumeroExpediente().equals(numeroExpediente)) return true;
		}
		
		return esta;
	}
	// TODO IMPROVE!!!
	public boolean estaNumeroExpediente(String numeroExpediente, Long idExpediente){
		boolean esta = false;
		for (FilaBandejaSalida bandeja : listaBandeja) {
			if(bandeja.getNumeroExpediente().equals(numeroExpediente)){
				if(!idExpediente.equals(bandeja.getId_expediente())){
					esta = true;
					break;
				}
			}
		}
		return esta;
	}
	
	private boolean puedeAnular(String numeroExpediente, long idExpediente, long idExpedientePadre, boolean acuseRecibo){
		boolean estado = true;
		if (acuseRecibo) {
			return false;
		}
		for (FilaBandejaSalida bandeja : listaBandeja) {
			if(bandeja.getNumeroExpediente().equals(numeroExpediente) && bandeja.getIdPadre().equals(idExpedientePadre)){
				if(!bandeja.getId_expediente().equals(idExpediente)){
					if (bandeja.getRecibidoFila()) {
						estado = false;
						break;
					}
				}
			}
		}
		return estado;
	}
	
	public boolean buscarEstadoAnularDespacho(ExpedienteBandejaSalida expedientes) {
		boolean flag = false;
		expedientes.setRecibido(false);
		Expediente e = me.buscarExpediente(new Long(expedientes.getId()));

		if (!e.getAcuseRecibo()) {
			List<Expediente> desExpedientes = me.obtenerExpedientesHijos(e);
			if (desExpedientes.size() > 0) {
				for (int i = 0; i < desExpedientes.size(); i++) {
					Expediente array_element = desExpedientes.get(i);
					if (array_element.getAcuseRecibo()) {
						expedientes.setRecibido(true);
					}
				}
			}
		} else {
			flag = true;
		}
		return flag;
	}
	
//	public boolean buscarEstadoAnularDespachoFlag(Long id) {
//		boolean flag = false;
//		
//		Expediente e = me.buscarExpediente(id);
//
//		if (!e.getAcuseRecibo()) {
//			List<Expediente> desExpedientes = me.obtenerExpedientesHijos(e);
//			if (desExpedientes.size() > 0) {
//				for (int i = 0; i < desExpedientes.size(); i++) {
//					Expediente array_element = desExpedientes.get(i);
//					if (array_element.getEliminado() == null) {
//						array_element.setEliminado(false);
//					}
//					if (array_element.getEliminado()) {
//						flag = true;
//					}
//				}
//			}
//		} else {
//			flag = true;
//		}
//		return flag;
//	}

	private void cargarDestinatariosExpedientes(List<ExpedienteBandejaSalida> listExpedientes2) {

		Set<Persona> desExpedientes = null;
		Iterator<Persona> iter = null;
		Persona persona = null;
		GrupoUsuario grupoUsuario = null;
		
		
		Iterator<GrupoUsuario> iteratorGrupo = null;
		for (ExpedienteBandejaSalida e : listExpedientes2) {
			
			desExpedientes = new HashSet<Persona>();
			desExpedientes = this.buscarDestinatarios(e.getId());
			
			
			iteratorGrupo = this.getGrupoUsuarios().iterator();
			if (desExpedientes != null) {

				iter = desExpedientes.iterator();
				
				while (iter.hasNext()) {

					if (e.getDestinatarioExpedienteString() == null || e.getDestinatarioExpedienteString().isEmpty()) {
						persona = iter.next();
						
						if (persona.getCargo() != null && persona.getNombreApellido() != null && persona.getCargo().getDescripcion() != null
								&& persona.getCargo().getUnidadOrganizacional().getDescripcion() != null) {
							e.setDestinatarioExpedienteString(persona.getNombreApellido() + GUION
									+ persona.getCargo().getDescripcion() + GUION
									+ persona.getCargo().getUnidadOrganizacional().getDescripcion());
						}						
					} else {
						persona = iter.next();
						if (persona.getCargo() != null && persona.getNombreApellido() != null && persona.getCargo().getDescripcion() != null
								&& persona.getCargo().getUnidadOrganizacional().getDescripcion() != null) {
							try {
									if (iter.hasNext()) {
										persona = iter.next();
										if(persona.getCargo() != null )
										e.setDestinatarioExpedienteString(e.getDestinatarioExpedienteString() + " <br/> "
												+ persona.getNombreApellido() + GUION + persona.getCargo().getDescripcion()
												+ GUION + persona.getCargo().getUnidadOrganizacional().getDescripcion());
									}
								}
							 catch (NoSuchElementException ex) {
								log.error("Error: No hay más destinatario", ex);
							}
						}
					}
				}	
				while(iteratorGrupo.hasNext()){
					grupoUsuario = iteratorGrupo.next();
					String destinatarioString = grupoUsuario.getNombre() ;
					if(e.getDestinatarioExpedienteString() != null)destinatarioString = destinatarioString + GUION + e.getDestinatarioExpedienteString();
					e.setDestinatarioExpedienteString(destinatarioString);
				}
				
			}
		}
	}

	@End
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Map<Long, Boolean> getSelectedDespachados() {
		return selectedDespachados;
	}

	public void setSelectedDespachados(Map<Long, Boolean> selectedDespachados) {
		this.selectedDespachados = selectedDespachados;
	}

	public List<ExpedienteBandejaSalida> getListExpedientes() {
		if (listExpedientes == null) {
			listExpedientes = new ArrayList<ExpedienteBandejaSalida>();
		}
		if (listExpedientes.isEmpty()) {
			this.buscarExpedientes();
		}
		return listExpedientes;
	}

	public void setListExpedientes(List<ExpedienteBandejaSalida> listExpedientes) {
		this.listExpedientes = listExpedientes;
	}

	public Map<Long, Boolean> getSelectedDesiertos() {
		return selectedDesiertos;
	}

	public void setSelectedDesiertos(Map<Long, Boolean> selectedDesiertos) {
		this.selectedDesiertos = selectedDesiertos;
	}

	public String obtenerEstilo(String tipoDoc) {
		String estilo = "";

		if (tipoDoc.equals("Consulta Ciudadana") || tipoDoc.equals("Consulta")) {
			estilo = "consulta";
		}

		if (tipoDoc.equals("Respuesta Consulta Ciudadana") || tipoDoc.equals("Respuesta Consulta")) {
			estilo = "respuesta";
		}

		return estilo;
	}

	@Override
	public Boolean getDesdeDespacho() {
		if (desdeDespacho == null) {
			desdeDespacho = Boolean.FALSE;
		}
		return desdeDespacho;
	}

	@Override
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}

	@SuppressWarnings("deprecation")
	public String anularDespachoExpedientes() {
		boolean flagAnulado = false;
		String msgAnulado = new String();
		boolean flagNoAnulado=false;
		String msgNoAnulado = new String();
		boolean flagSinDespacho = false;
		String msgSinDespacho = new String();
		boolean flagSinSeleccion = true;
		List<Long> expedientesPadres = new ArrayList<Long>();
			Set<Long> idExpedientes = selectedAnular.keySet();
			for (Long id : idExpedientes) {
				if (((Boolean) selectedAnular.get(id))) {
					flagSinSeleccion = false;
					Expediente e = me.buscarExpediente(id);
					if (e.getVersionPadre() != null) {
						if (!expedientesPadres.contains(e.getVersionPadre().getId())) {
							expedientesPadres.add(e.getVersionPadre().getId());
						}
					}
					else {
						if (!flagSinDespacho) {
							msgSinDespacho += e.getNumeroExpediente();
						} else {
							if (!msgSinDespacho.matches(Pattern.quote(e.getNumeroExpediente()))) {
								msgSinDespacho += " - " + e.getNumeroExpediente();
							}
						}
						flagSinDespacho = true;
					}
				}
			}
			for (Long id : expedientesPadres) {
				// e.setEliminado(true);
				// me.modificarExpediente(e);
				// se le avisa a los hijos que son eliminados , y que deben desaparecer de la bandeja de entrada
				Expediente e = me.buscarExpediente(id);
				List<Expediente> desExpedientes = me.obtenerExpedientesHijos(e);
				if(desExpedientes.size() == 0) {
					if (!flagSinDespacho) {
						msgSinDespacho += e.getNumeroExpediente();
					} else {
						if (!msgSinDespacho.matches(Pattern.quote(e.getNumeroExpediente()))) {
							msgSinDespacho += " - " + e.getNumeroExpediente();
						}
					}
					flagSinDespacho = true;
				} else {
					boolean sePuede = true;
					for (int i=0; i < desExpedientes.size() && sePuede; i++) {
						Expediente expediente = desExpedientes.get(i);
						if(expediente.getFechaAcuseRecibo() != null) {
							sePuede = false;
							if (!flagNoAnulado) {
								msgNoAnulado += e.getNumeroExpediente();
							} else {
								if (!msgNoAnulado.matches(Pattern.quote(e.getNumeroExpediente()))) {
									msgNoAnulado += " - " + e.getNumeroExpediente();
								}
							}
							flagNoAnulado= true;
						} else {
							if (expediente.getReasignado() != null) {
								sePuede = false;
								if (!flagNoAnulado) {
									msgNoAnulado += e.getNumeroExpediente();
								} else {
									if (!msgNoAnulado.matches(Pattern.quote(e.getNumeroExpediente()))) {
										msgNoAnulado += " - " + e.getNumeroExpediente();
									}
								}
								flagNoAnulado= true;
							} else {
								if (expediente.getFechaDespacho() == null) {
									sePuede = false;
									if (!flagSinDespacho) {
										msgSinDespacho += e.getNumeroExpediente();
									} else {
										if (!msgSinDespacho.matches(Pattern.quote(e.getNumeroExpediente()))) {
											msgSinDespacho += " - " + e.getNumeroExpediente();
										}
									}
									flagSinDespacho = true;
								}
							}
						}
					}
					
					if(sePuede){
						e.setFechaDespacho(null);
						em.merge(e);
						Iterator<Expediente> ie = desExpedientes.iterator();
						while (ie.hasNext()) {
							Expediente array_element = ie.next();
							eliminarHistorialDespacho(array_element);
							me.eliminarExpediente(array_element.getId());
							//array_element.setCancelado(ahora);
							if (!flagAnulado) {
								msgAnulado = e.getNumeroExpediente();
							} else {
								if (!msgAnulado.matches(Pattern.quote(e.getNumeroExpediente()))) {
									msgAnulado += " - " + e.getNumeroExpediente();
								}
							}
							flagAnulado = true;
						}
					}
				}
			}
			
			if (flagAnulado) {
				FacesMessages.instance().add("Han sido anulados los siguientes expedientes : " + msgAnulado);
				this.validarComplete = Boolean.TRUE;
			}
			if (flagSinDespacho) {
				FacesMessages.instance().add("Expedientes no anulados porque no han sido despachados : " + msgSinDespacho);
			}
			if (flagNoAnulado) {
				FacesMessages.instance().add("Expedientes que ya no pueden ser anulados : " + msgNoAnulado);
				this.validarComplete = Boolean.TRUE;
			}
			if (flagSinSeleccion) {
				FacesMessages.instance().add("Debe seleccionar un Expediente");
			}
			desdeDespacho = true;
			return "bandejaSalida";
	}

	public Map<Long, Boolean> getSelectedAnular() {
		return selectedAnular;
	}

	public void setSelectedAnular(Map<Long, Boolean> selectedAnular) {
		this.selectedAnular = selectedAnular;
	}

	/**
	 * @return the grupoUsuarios
	 */
	public List<GrupoUsuario> getGrupoUsuarios() {
		return grupoUsuarios;
	}

	/**
	 * @param grupoUsuarios the grupoUsuarios to set
	 */
	public void setGrupoUsuarios(List<GrupoUsuario> grupoUsuarios) {
		this.grupoUsuarios = grupoUsuarios;
	}

	public List<FilaBandejaSalida> getListaBandeja() {
		return listaBandeja;
	}

	public void setListaBandeja(List<FilaBandejaSalida> listaBandeja) {
		this.listaBandeja = listaBandeja;
	}
	
	/**
	 * @return {@link Boolean}
	 */
	public boolean isValidarComplete() {
		return validarComplete;
	}


	/**
	 * @param validarComplete {@link Boolean}
	 */
	public void setValidarComplete(final boolean validarComplete) {
		this.validarComplete = validarComplete;
	}
	
	public boolean getEstadoDesertar() {
		return estadoDesertar;
	}


	public void setEstadoDesertar(final boolean estadoDesertar) {
		this.estadoDesertar = estadoDesertar;
	}
	
	@Override
	public String obtenerEstiloRepetido(FilaBandejaSalida fila){
		
		if(fila.getRepetido())return REPETIDO;
		return ""; 
	}
	
	private void eliminarHistorialDespacho(Expediente e) {
		Query query = em.createNamedQuery("HistorialDespacho.findByIdExp");
		query.setParameter("expediente", e);
		List<HistorialDespacho> lista = (List<HistorialDespacho>)query.getResultList();
		if (lista.size() > 0) {
			Iterator<HistorialDespacho> ihd = lista.iterator();
			while (ihd.hasNext()) {
				HistorialDespacho hd = ihd.next();
				em.remove(hd);
			}
		}
	}
	
//	private void ejbPassive() {
//		// TODO Auto-generated method stub
//
//	}

}
