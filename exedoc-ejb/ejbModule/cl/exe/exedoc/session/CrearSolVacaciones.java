package cl.exe.exedoc.session;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Vacaciones;

@Local
public interface CrearSolVacaciones {

    public String end();
    public void destroy();
    public String aprobarSolVacaciones();
    public String rechazarSolVacaciones();
    public Vacaciones getVacaciones();
    
    public void setVacaciones(Vacaciones vacaciones);
    
    public boolean isEditable();
    public boolean isRenderedBotonGuardar();
    public void guardarYCrearExpediente();
    public boolean isRenderedBotonGuardarCrearExpediente();

    public String agregarDocumento();
    
    public Persona getFuncionario();
    public void setFuncionario(Persona funcionario);
    public String getGrado();
    public void setGrado(String grado);

    public Double getDiasVacacionesPendientes();
    public void setDiasVacacionesPendientes(Double diasVacacionesPendientes);
    public String getCalidadJuridica();
    public void setCalidadJuridica(String calidadJuridica);
    public boolean isDiasOk();
    public void setDiasOk(boolean diasOk);
    public Persona getSolicitante();
    public void setSolicitante(Persona solicitante);
    public boolean isRenderedBotonAceptarRechazar();    
    public String getTipoDia();
    public void setTipoDia(String tipoDia);
    public String getDatosObtenidos();
    public void setDatosObtenidos(String datosObtenidos);
    public boolean isRenderedMensajeRechazado();
    public DetalleDiasVacaciones getDetalleDiasVacaciones();
    public void setDetalleDiasVacaciones(DetalleDiasVacaciones detalleDiasVacaciones);
	Boolean getVisado();
	Boolean getFirmado();
	void obtenerDatosExternos();
	String guardarDocumento();

}