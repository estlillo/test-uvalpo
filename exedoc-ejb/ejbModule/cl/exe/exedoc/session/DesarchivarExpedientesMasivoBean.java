package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name(DesarchivarExpedientesMasivoBean.NAME)
@Scope(ScopeType.SESSION)
public class DesarchivarExpedientesMasivoBean implements DesarchivarExpedientesMasivo {

	/**
	 * NAME de la clase.
	 */
	public static final String NAME = "desarchivarExpedientesMasivo";

	private static final String BUSQUEDA_EXPEDIENTES_ARCHIVADOS = "busquedaExpedientesArchivados";
	private static final String EXPEDIENTES_ARCHIVADOS = "desarchivarExpedientes";
	private String tipoDocumentoBusqueda;
	private String numeroExpedienteBusqueda;
	private String numeroDocumentoBusqueda;
	private String emisorDocumentoBusqueda;
	private Date fechaArchivadoInicioBusqueda;
	private Date fechaArchivadoTerminoBusqueda;
	private String materiaDocumentoBusqueda;
	private Long organismoBusqueda;

	private Long organizacion;
	private Long idDivision;
	private Long idDepto;
	private Long idUnidad;
	private Long idCargo;
	private Long idPersona;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDeptos;
	private List<SelectItem> listUnidades;
	private List<SelectItem> listCargos;
	private List<SelectItem> listPersonas;

	private String clasificacionTipoBusqueda;
	private String clasificacionSubtipoBusqueda;
	private String clasificacionMateriaBusqueda;
	private List<SelectItem> listClasificacionTipo = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionSubtipo = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionMateria = new ArrayList<SelectItem>();
	private int flagMostrarClasificaciones;
	// private static String _TIPO_DOCUMENTO_RESOLUCION = "6";

	private List<ExpedienteBandejaSalida> expedientesArchivados;
	private Set<Persona> listDestinatarios;
	private Map<Long, Boolean> selectedDesarchivados;

	private List<Long> idExpedientes;
	private int copiaDirectoFlag;
	private boolean administrador = false;

	private static final Locale LOCALE = new Locale("es", "CL");
	private static final String TIMEZONE = "America/Cayenne";

	@Logger
	private Log log;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private ManejarExpedienteInterface me;

	@PersistenceContext
	protected EntityManager em;

	@In(required = true)
	private Persona usuario;

	@Out(required = true)
	private String homeCrear;

	@Factory(value = "expedientesArchivados", scope = ScopeType.SESSION)
	@Override
	public String begin(final boolean admin, final boolean owner) {
		this.administrador = admin;
		log.info("Iniciando modulo...");

		tipoDocumentoBusqueda = "";
		numeroExpedienteBusqueda = "";
		numeroDocumentoBusqueda = "";
		emisorDocumentoBusqueda = "";
		fechaArchivadoInicioBusqueda = null;
		fechaArchivadoTerminoBusqueda = null;
		materiaDocumentoBusqueda = "";

		this.buscarPersonas();

		expedientesArchivados = new ArrayList<ExpedienteBandejaSalida>();
		listDestinatarios = new HashSet<Persona>();
		selectedDesarchivados = new HashMap<Long, Boolean>();

		idExpedientes = new ArrayList<Long>();

		this.cargarListas();
		
		if (this.administrador) {
			if (owner) {
				idPersona = usuario.getId();
				homeCrear = EXPEDIENTES_ARCHIVADOS;
				return EXPEDIENTES_ARCHIVADOS;
			}
			homeCrear = NAME;
			return NAME;
		} else {
			idPersona = usuario.getId();
		}
		homeCrear = BUSQUEDA_EXPEDIENTES_ARCHIVADOS;
		return BUSQUEDA_EXPEDIENTES_ARCHIVADOS;
	}

	/**
	 * Metodo que carga las listas default.
	 */
	private void cargarListas() {
		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
		organismoBusqueda =  usuario.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getOrganizacion().getId();
//		this.buscarDivisiones();
//		idDivision = (Long) listDivisiones.get(1).getValue();
//		this.buscarDeptos();
//		listDeptos = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setIdDepto(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidades();
	}

	private List<SelectItem> buscarOrganizaciones() {
		limpiarPersonaBusqueda();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarPersonaBusqueda();
		organizacion = idOrganizacion;
		listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void limpiarPersonaBusqueda() {
		this.organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;
		idDepto = JerarquiasLocal.INICIO;
		idUnidad = JerarquiasLocal.INICIO;
		idCargo = JerarquiasLocal.INICIO;
		idPersona = administrador ? JerarquiasLocal.INICIO : usuario.getId();

		listDivisiones = new ArrayList<SelectItem>();
		listDeptos = new ArrayList<SelectItem>();
		listUnidades = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		listPersonas = new ArrayList<SelectItem>();

		listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDeptos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarExpedienteDelUsuario() {
		log.info("******************************************************************");
		log.info("Nombre usuario: {0}, apellidos: {1} {2} ", usuario.getNombres(), usuario.getApellidoPaterno(),
				usuario.getApellidoMaterno());
		log.info("*****************************************************************");
		this.buscarExpedientes(Boolean.FALSE);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedientes(final Boolean isAdmin) {
		log.info("[buscarExpedientes] Iniciando busqueda...");

		final long init = System.currentTimeMillis();
		expedientesArchivados.clear();
 
		final Boolean estado = this.validarCampos();

		if (estado) {

			final StringBuilder consultaSQL = this.armarSQLExpedientesArchivados(isAdmin);
			log.info("[buscarExpedientes] Consulta SQL a ejecutar: " + consultaSQL);

			final Query qry = em.createQuery(consultaSQL.toString());
//			if(fechaArchivadoInicioBusqueda != null &&  fechaArchivadoInicioBusqueda != null ){
				if (fechaArchivadoInicioBusqueda != null && fechaArchivadoInicioBusqueda != null) {
					final Date fechaTerminoConHora = FechaUtil.fin(fechaArchivadoTerminoBusqueda);
					qry.setParameter("fechaArchivadoInicioBusqueda", fechaArchivadoInicioBusqueda);
					qry.setParameter("fechaArchivadoTerminoBusqueda", fechaTerminoConHora);
				}
	
				final List<Object[]> objectArray = qry.getResultList();
				log.info("Total de registros obtenidos desde la BD: {0}", objectArray.size());
				log.info("Tiempo de ejecucion de la consulta: {0} ms.", System.currentTimeMillis() - init);
	
				//expedientesArchivados = me.listarExpedientesArchivadosPorUsuario(objectArray);
				listarExpedientesArchivados(objectArray);
	
				log.info("[buscarExpedientes] Total de registros a desplegar: {0}", expedientesArchivados.size());
				log.info("[buscarExpedientes] Tiempo total de ejecucion: {0}", System.currentTimeMillis() - init);
//			}
//			else{
//				FacesMessages.instance().add("Debe ingresar el rango de fechas");
//			}
			this.cargarListas();
		}
	}

	/**
	 * Valida campos del formulario.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarCampos() {
		Boolean estado = Boolean.TRUE;
		if (numeroExpedienteBusqueda.isEmpty() && numeroDocumentoBusqueda.isEmpty() && idPersona.equals(-1)) {
			if (fechaArchivadoInicioBusqueda == null && fechaArchivadoTerminoBusqueda == null) {
				FacesMessages.instance().add("Se debe incluir mínimo el rango de fechas");
				estado = Boolean.FALSE;
			} else if (fechaArchivadoInicioBusqueda != null && fechaArchivadoTerminoBusqueda == null) {
				FacesMessages.instance().add("Debe incluir la fecha de término");
				estado = Boolean.FALSE;
			} else if (fechaArchivadoInicioBusqueda == null && fechaArchivadoTerminoBusqueda != null) {
				FacesMessages.instance().add("Debe incluir la fecha de inicio");
				estado = Boolean.FALSE;
			}
		}
		return estado;
	}

	/**
	 * Metodo que crea {@link StringBuilder} con Query.
	 * 
	 * @param isAdmin {@link Boolean}
	 * @return {@link StringBuilder}
	 */
	private StringBuilder armarSQLExpedientesArchivados(final Boolean isAdmin) {
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("select e.id, e.numeroExpediente, e.archivado, ");
		jpQL.append("dest_hist.nombres as dest_hist_nombres, dest_hist.apellidoPaterno as dest_hist_apellido, ");
		jpQL.append("c.descripcion as cargo, u.descripcion as unidad, ");
		jpQL.append("d.id as id_doc, d.numeroDocumento, d.fechaDocumentoOrigen, d.fechaCreacion, ");
		jpQL.append("d.materia, td.descripcion as doc_tipo, fd.descripcion as doc_formato ");
		jpQL.append("from Expediente e ");
		jpQL.append("inner join e.documentos de ");
		jpQL.append("inner join de.documento d ");
		jpQL.append("left join d.tipoDocumento td ");
		jpQL.append("left join d.formatoDocumento fd ");
		jpQL.append("left join e.destinatario dest ");
		jpQL.append("inner join e.destinatarioHistorico dest_hist ");
		jpQL.append("inner join dest_hist.cargo c ");
		jpQL.append("inner join c.unidadOrganizacional u ");
		jpQL.append("inner join u.departamento dep ");
		jpQL.append("inner join dep.division div ");
		jpQL.append("inner join div.organizacion o ");
		jpQL.append("where e.cancelado is null and e.archivado is not null ");
		//jpQL.append("and dest_hist.cargo.id = c.id and c.unidadOrganizacional.id = u.id ");
		//jpQL.append("and u.departamento.id = dep.id and dep.division.id = div.id ");

		if (tipoDocumentoBusqueda != null && !"-1".equals(tipoDocumentoBusqueda)) {
			jpQL.append("and d.tipoDocumento.id = " + tipoDocumentoBusqueda + " ");
		}

		if (numeroExpedienteBusqueda != null && !numeroExpedienteBusqueda.trim().isEmpty()) {
			jpQL.append("and upper(e.numeroExpediente) like upper('" + numeroExpedienteBusqueda.trim() + "%') ");
		}

		if (!numeroDocumentoBusqueda.isEmpty()) {
			jpQL.append("and upper(d.numeroDocumento) like upper('%");
			jpQL.append(numeroDocumentoBusqueda);
			jpQL.append("%') ");
		}

		if (!"".equals(emisorDocumentoBusqueda)) {
			jpQL.append("and upper(d.emisor) like upper('%");
			jpQL.append(emisorDocumentoBusqueda);
			jpQL.append("%') ");
		}

		if (fechaArchivadoInicioBusqueda != null && fechaArchivadoTerminoBusqueda != null) {
			jpQL.append(" and e.archivado between :fechaArchivadoInicioBusqueda ");
			jpQL.append(" and :fechaArchivadoTerminoBusqueda ");
		}

		if (materiaDocumentoBusqueda.length() > 0) {
			jpQL.append("and upper(d.materia) like upper('%");
			jpQL.append(materiaDocumentoBusqueda);
			jpQL.append("%') ");
		}

		if (isAdmin) {
			jpQL.append(" and e.versionPadre is not null "); //no desarchivar lo que nunca ha sido tramitado
			if (idPersona != null && idPersona != -1) {
				jpQL.append(" and dest_hist.id = " + idPersona);
			} else if (idCargo != null && idCargo != -1) {
				jpQL.append(" and c.id= " + idCargo);
			} else if (idUnidad != null && idUnidad != -1) {
				jpQL.append(" and u.id= " + idUnidad);
			} else if (idDepto != null && idDepto != -1) {
				jpQL.append(" and dep.id= " + idDepto);
			} else if (idDivision != null && idDivision != -1) {
				jpQL.append(" and div.id = " + idDivision);
			} else if (organizacion != null && organizacion != -1) {
				jpQL.append(" and o.id = " + organizacion);
			}
		} else {
			jpQL.append(" and dest_hist.id = " + usuario.getId() + " ");
		}

		jpQL.append(" and (d.eliminado is null or d.eliminado = false) ");
		jpQL.append(" and d.fechaCreacion = (");
		jpQL.append("   select max(d2.fechaCreacion) "); 
		jpQL.append("   from Expediente e2 ");
		jpQL.append("   inner join e2.documentos ed2 ");
		jpQL.append("   inner join ed2.documento d2 ");
		jpQL.append("   where e2.id = e.id ");
		jpQL.append("   and (d2.eliminado is null or d2.eliminado = false) )");
		
		jpQL.append(" order by e.archivado asc ");

		return jpQL;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdExpediente(final String numeroExpediente) {
		StringBuffer hql;
		Long idExpediente = null;
		hql = new StringBuffer("SELECT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = '" + numeroExpediente + "' ");
		hql.append("order by e.id desc limit 1");
		final Query query = em.createQuery(hql.toString());
		final List<Long> ids = query.getResultList();
		if (ids.size() >= 1) {
			idExpediente = ids.get(0);
		}
		return idExpediente;
	}

	public void setExpedientesADesarchivar(final Long idExpediente) {
		if (selectedDesarchivados.containsKey(idExpediente)) {
			idExpedientes.remove(idExpediente);
			selectedDesarchivados.remove(idExpediente);
		} else {
			idExpedientes.add(idExpediente);
			selectedDesarchivados.put(idExpediente, true);
		}
	}

	@SuppressWarnings("unchecked")
	public void buscarDeptos() {
		final List<Division> divisiones = em.createNamedQuery("Division.findById").setParameter("id", idDivision)
				.getResultList();
		boolean conCargo = false;

		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDivision(idDivision);
				buscarCargos();
				buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDeptos = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidades.clear();
		listUnidades = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidades() {
		final List<Departamento> deptos = em.createNamedQuery("Departamento.findById").setParameter("id", idDepto)
				.getResultList();
		boolean conCargo = false;

		if (deptos != null && deptos.size() == 1) {
			final Departamento depto = deptos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDepartamento(idDivision, idDepto);
				buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidades = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, idDepto);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, idUnidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, idCargo);
		if (listPersonas.size() == 1) {
			idPersona = (Long) listPersonas.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void agregarPersona() {
		if (!idCargo.equals(JerarquiasLocal.INICIO)) {
			if (idPersona.equals(JerarquiasLocal.TODOS)) {
				listDestinatarios.addAll(em.createNamedQuery("Persona.findById").setParameter("id", idCargo)
						.getResultList());
			} else if (idPersona.equals(JerarquiasLocal.CUALQUIERA)) {
				final List<Persona> personasTmp = em.createQuery("select p from Persona p where p.cargo.id = ?")
						.setParameter(1, idCargo).getResultList();
				for (Persona p : personasTmp) {
					p.setDestinatarioConRecepcion(true);
					listDestinatarios.add(p);
				}
			} else {
				if(!idPersona.equals(JerarquiasLocal.INICIO)){
					final Persona p = (Persona) em.createQuery("select p from Persona p where p.id = :param")
							.setParameter("param", idPersona).getSingleResult();
					if (copiaDirectoFlag == 0) {
						p.setDestinatarioConCopia(false);
					} else {
						p.setDestinatarioConCopia(true);
					}
					listDestinatarios.add(p);
				}else {
					FacesMessages.instance().add("Debe seleccionar una persona al asignar expedientes");
					return;
				}
			}
		}
		this.cargarListas();	
	}

	public void eliminarPersona(final Persona persona) {
		listDestinatarios.remove(persona);
	}
	
	@Override
	public String desarchivar() {
			for (Long idExpediente : idExpedientes) {
				final Expediente expediente = me.buscarExpediente(idExpediente);
				
				// borrar destinatarios previos
				// innecesario
//				List<Expediente> expedientes = me.buscarExpediente(expediente, false);
//				if (expedientes != null) {
//					Iterator<Expediente> itExpedientes = expedientes.iterator();
//					while (itExpedientes.hasNext()) {
//						Expediente i = (Expediente)itExpedientes.next();
//						me.eliminarExpediente(i.getId());
//					}
//				}
			    
				// Se agrega una observacion al expediente
				final Observacion obs = new Observacion();
				obs.setAutor(usuario);
				obs.setExpediente(expediente);
				obs.setFecha(new Date());
				obs.setObservacion("Desarchivado por " + usuario.getNombreApellido());
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_SISTEMA));
				// expediente.getObservaciones().add(obs);
				em.merge(obs);

				// Desarchivar el expediente
				expediente.setArchivadoHistorico(expediente.getArchivado());
				expediente.setArchivado(null);
				em.merge(expediente);
				log.info("Expediente [" + expediente.getId() + ", " + expediente.getNumeroExpediente()
						+ "] desarchivado");
				FacesMessages.instance().add("Expediente(s) desarchivado(s)");
			}

			if (idExpedientes.size() < 1) {
				FacesMessages.instance().add("Debe seleccionar Expediente(s)");
			}

			return end();
	}

	public String desarchivarYAsignar() {
		if (listDestinatarios.size() > 0) {
			final Date fechaIngreso = new Date();
			// Se itera primeramente por cada expediente
			for (Long idExpediente : idExpedientes) {
				final Expediente expediente = me.buscarExpediente(idExpediente);
				
				// borrar destinatarios previos
				List<Expediente> expedientes = me.buscarExpedientesHijos(expediente);
				if (expedientes != null) {
					Iterator<Expediente> itExpedientes = expedientes.iterator();
					while (itExpedientes.hasNext()) {
						Expediente i = (Expediente)itExpedientes.next();
						me.eliminarExpediente(i.getId());
					}
				}

				// Se agrega una observacion al expediente
				final Observacion obs = new Observacion();
				obs.setAutor(usuario);
				obs.setExpediente(expediente);
				obs.setFecha(fechaIngreso);
				obs.setObservacion("Desarchivado y asignado por " + usuario.getNombreApellido());
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_SISTEMA));
				em.merge(obs);

				// Desarchivar el expediente
				expediente.setArchivadoHistorico(expediente.getArchivado());
				expediente.setArchivado(null);
				expediente.setReasignado(fechaIngreso);
				em.merge(expediente);
				log.info("Expediente [" + expediente.getId() + ", " + expediente.getNumeroExpediente()
						+ "] desarchivado");
				for (Persona destinatario : listDestinatarios) {
					try {
						final Long idNuevoExp = me.agregarDestinatarioAExpediente(expediente, usuario, destinatario,
								fechaIngreso);
						final Expediente nuevoExp = me.buscarExpediente(idNuevoExp);
						log.info("Asignando expediente [" + nuevoExp.getId() + ", " + expediente.getNumeroExpediente()
								+ "] a persona con id " + destinatario.getId());
						me.despacharExpediente(nuevoExp);
					} catch (DocumentNotUploadedException e) {
						e.printStackTrace();
					}
				}
			}
			FacesMessages.instance().add("Expediente(s) desarchivado(s) y asignado(s)");

			if (idExpedientes.size() < 1) {
				FacesMessages.instance().add("Debe seleccionar Expediente(s)");
				return "";
			}
		} else {
			FacesMessages.instance().add("Debe seleccionar destinatarios antes de desarchivar/asignar el expediente");
			return "";
		}
		return end();
	}

	public boolean renderedVerExpediente(final Long idDocumento) {
		boolean valido = true;
		final Documento doc = em.find(Documento.class, idDocumento);

		if (doc instanceof Oficio && doc.getReservado()) {
			for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
				if (!destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
					valido = false;
				} else {
					valido = true;
					break;
				}
			}
		}

		return valido;
	}

	public String end() {
		expedientesArchivados = null;
		selectedDesarchivados = null;
		listDestinatarios = null;

		log.info("Finalizando modulo...");
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {

	}

	public String getTipoDocumentoBusqueda() {
		return tipoDocumentoBusqueda;
	}

	public void setTipoDocumentoBusqueda(final String tipoDocumentoBusqueda) {
		this.tipoDocumentoBusqueda = tipoDocumentoBusqueda;
	}

	public String getNumeroExpedienteBusqueda() {
		return numeroExpedienteBusqueda;
	}

	public void setNumeroExpedienteBusqueda(final String numeroExpedienteBusqueda) {
		this.numeroExpedienteBusqueda = numeroExpedienteBusqueda.trim();
	}

	public String getNumeroDocumentoBusqueda() {
		return numeroDocumentoBusqueda;
	}

	public void setNumeroDocumentoBusqueda(final String numeroDocumentoBusqueda) {
		this.numeroDocumentoBusqueda = numeroDocumentoBusqueda.trim();
	}

	public String getEmisorDocumentoBusqueda() {
		return emisorDocumentoBusqueda;
	}

	public void setEmisorDocumentoBusqueda(final String emisorDocumentoBusqueda) {
		this.emisorDocumentoBusqueda = emisorDocumentoBusqueda.trim();
	}

	public Date getFechaArchivadoInicioBusqueda() {
		return fechaArchivadoInicioBusqueda;
	}

	public void setFechaArchivadoInicioBusqueda(final Date fechaArchivadoInicioBusqueda) {
		this.fechaArchivadoInicioBusqueda = fechaArchivadoInicioBusqueda;
	}

	public Date getFechaArchivadoTerminoBusqueda() {
		return fechaArchivadoTerminoBusqueda;
	}

	public void setFechaArchivadoTerminoBusqueda(final Date fechaArchivadoTerminoBusqueda) {
		this.fechaArchivadoTerminoBusqueda = fechaArchivadoTerminoBusqueda;
	}

	public String getMateriaDocumentoBusqueda() {
		return materiaDocumentoBusqueda;
	}

	public void setMateriaDocumentoBusqueda(final String materiaDocumentoBusqueda) {
		this.materiaDocumentoBusqueda = materiaDocumentoBusqueda.trim();
	}

	public Long getIdDivision() {
		return idDivision;
	}

	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	public Long getIdDepto() {
		return idDepto;
	}

	public void setIdDepto(final Long idDepto) {
		this.idDepto = idDepto;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(final Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(final Long idCargo) {
		this.idCargo = idCargo;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(final Long idPersona) {
		this.idPersona = idPersona;
	}

	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	public void setListDivisiones(final List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	public List<SelectItem> getListDeptos() {
		return listDeptos;
	}

	public void setListDeptos(final List<SelectItem> listDeptos) {
		this.listDeptos = listDeptos;
	}

	public List<SelectItem> getListUnidades() {
		return listUnidades;
	}

	public void setListUnidades(final List<SelectItem> listUnidades) {
		this.listUnidades = listUnidades;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public void setListCargos(final List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	public List<ExpedienteBandejaSalida> getExpedientesArchivados() {
		return expedientesArchivados;
	}

	public void setExpedientesArchivados(final List<ExpedienteBandejaSalida> expedientesArchivados) {
		this.expedientesArchivados = expedientesArchivados;
	}

	public Object[] getListDestinatarios() {
		return listDestinatarios.toArray();
	}

	public void setListDestinatarios(final Set<Persona> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}

	public Map<Long, Boolean> getSelectedDesarchivados() {
		return selectedDesarchivados;
	}

	public void setSelectedDesarchivados(final Map<Long, Boolean> selectedDesarchivados) {
		this.selectedDesarchivados = selectedDesarchivados;
	}

	public List<Long> getIdExpedientes() {
		return idExpedientes;
	}

	public void setIdExpedientes(final List<Long> idExpedientes) {
		this.idExpedientes = idExpedientes;
	}

	public int getCopiaDirectoFlag() {
		return copiaDirectoFlag;
	}

	public void setCopiaDirectoFlag(final int copiaDirectoFlag) {
		this.copiaDirectoFlag = copiaDirectoFlag;
	}

	public Locale getLOCALE() {
		return LOCALE;
	}

	public String getTIMEZONE() {
		return TIMEZONE;
	}

	public String getClasificacionTipoBusqueda() {
		return clasificacionTipoBusqueda;
	}

	public void setClasificacionTipoBusqueda(final String clasificacionTipoBusqueda) {
		this.clasificacionTipoBusqueda = clasificacionTipoBusqueda;
	}

	public String getClasificacionSubtipoBusqueda() {
		return clasificacionSubtipoBusqueda;
	}

	public void setClasificacionSubtipoBusqueda(final String clasificacionSubtipoBusqueda) {
		this.clasificacionSubtipoBusqueda = clasificacionSubtipoBusqueda;
	}

	public String getClasificacionMateriaBusqueda() {
		return clasificacionMateriaBusqueda;
	}

	public void setClasificacionMateriaBusqueda(final String clasificacionMateriaBusqueda) {
		this.clasificacionMateriaBusqueda = clasificacionMateriaBusqueda;
	}

	public List<SelectItem> getListClasificacionTipo() {
		return listClasificacionTipo;
	}

	public void setListClasificacionTipo(final List<SelectItem> listClasificacionTipo) {
		this.listClasificacionTipo = listClasificacionTipo;
	}

	public List<SelectItem> getListClasificacionSubtipo() {
		return listClasificacionSubtipo;
	}

	public void setListClasificacionSubtipo(final List<SelectItem> listClasificacionSubtipo) {
		this.listClasificacionSubtipo = listClasificacionSubtipo;
	}

	public List<SelectItem> getListClasificacionMateria() {
		return listClasificacionMateria;
	}

	public void setListClasificacionMateria(final List<SelectItem> listClasificacionMateria) {
		this.listClasificacionMateria = listClasificacionMateria;
	}

	public int getFlagMostrarClasificaciones() {
		return flagMostrarClasificaciones;
	}

	public void setFlagMostrarClasificaciones(final int flagMostrarClasificaciones) {
		this.flagMostrarClasificaciones = flagMostrarClasificaciones;
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public void setOrganismoBusqueda(final Long organismoBusqueda) {
		this.organismoBusqueda = organismoBusqueda;
	}

	@Override
	public Long getOrganismoBusqueda() {
		return organismoBusqueda;
	}
	
	private void listarExpedientesArchivados(List<Object[]> resultado) {
		expedientesArchivados = new ArrayList<ExpedienteBandejaSalida>();
		//Expediente exp = null;
		for (Object[] o : resultado) {
			ExpedienteBandejaSalida row = new ExpedienteBandejaSalida();
			//e.id
			row.setId((Long)o[0]);
			//e.numeroExpediente
			row.setNumeroExpediente((String)o[1]);
			//e.archivado
			row.setFechaArchivado((Date)o[2]);
			
			Persona persona = new Persona();
			Cargo cargo = new Cargo();
			UnidadOrganizacional unidad = new UnidadOrganizacional();
			//dest_hist.nombres as dest_hist_nombre
			persona.setNombres(o[3] != null ? (String)o[3] : "");
			//dest_hist.apellidoPaterno as dest_hist_apellido
			persona.setApellidoPaterno(o[4] != null ? (String)o[4] : "");
			//c.descripcion as cargo
			cargo.setDescripcion(o[5] != null ? (String)o[5] : "");
			//u.descripcion as unidad
			unidad.setDescripcion(o[6] != null ? (String)o[6] : "");
			cargo.setUnidadOrganizacional(unidad);
			persona.setCargo(cargo);
			row.setDestinatarioExpediente(persona);
			//d.id as id_doc
			row.setIdDocumento((Long)o[7]);
			//d.numeroDocumento
			row.setNumeroDocumento(o[8] != null ? (String)o[8] : "");
			//d.fechaDocumentoOrigen
			row.setFechaDocumentoOrigen(o[9] != null ? (Date)o[9] : null);
			//d.fechaCreacion
			row.setFechaCreacionDocumento((Date)o[10]);
			//d.materia
			row.setMateria(o[11] != null ? (String)o[11] : "");
			//td.descripcion as doc_tipo
			row.setTipoDocumento(o[12] != null ? (String)o[12] : "");
			//fd.descripcion as doc_formato
			row.setFormatoDocumento(o[13] != null ? (String)o[13]: "");
			
			expedientesArchivados.add(row);
		}
	}
}
