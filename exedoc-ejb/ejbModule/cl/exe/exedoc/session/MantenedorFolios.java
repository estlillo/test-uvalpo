package cl.exe.exedoc.session;

import java.util.List;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.FolioDocumento;



/**
 * @author Sergio
 * 
 */
public interface MantenedorFolios
{
	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return {@link String}
	 */
	String begin();

	FolioDocumento getFolioDocumento();

	void setFolioDocumento(FolioDocumento folioDocumento);

	List<FolioDocumento> getListFolios();

	void setListFolios(List<FolioDocumento> listFolios);

	Long getFiscalia();

	void setFiscalia(Long fiscalia);

	Long getUnidad();

	void setUnidad(Long unidad);

	List<SelectItem> getFiscalias();

	void setFiscalias(List<SelectItem> fiscalias);

	List<SelectItem> getUnidades();

	void setUnidades(List<SelectItem> unidades);

	void setListTipoDocumentos(List<SelectItem> listTipoDocumentos);

	List<SelectItem> getListTipoDocumentos();

	void setTipoDocumento(Integer tipoDocumento);

	Integer getTipoDocumento();

	void guardarFolio();

	boolean crear(FolioDocumento folioDocumento);

	boolean actualizar(FolioDocumento folioDocumento);

	void editarFolio(Long id);

	Boolean validarFolio();

	void nivelFiscalia();

	void setHabilitaUnidad(boolean habilitaUnidad);

	boolean getHabilitaUnidad();

	void limpiarFolio();

	void setEditable(Boolean editable);

	Boolean getEditable();

	void setValorInicial(String valorInicial);

	String getValorInicial();

	void setValorAgno(String valorAgno);

	String getValorAgno();
	
}
