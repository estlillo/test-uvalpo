package cl.exe.exedoc.session;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;

public class BandejaEntradaData {

	public BandejaEntradaData() {
		System.out.println("bandeja de entrada data instanciado");
	}

	public BandejaEntradaData(List<ExpedienteBandejaEntrada> lista) {
		System.out.println("bandeja de entrada data instanciado con lista");
		listExpedientes = lista;
		for (ExpedienteBandejaEntrada e : listExpedientes)
			map.put(e.getId(), e);
	}

	private HashMap<Long, ExpedienteBandejaEntrada> map = new HashMap<Long, ExpedienteBandejaEntrada>();

	private List<ExpedienteBandejaEntrada> listExpedientes;

	public List<ExpedienteBandejaEntrada> getListExpedientes() {
		return listExpedientes;
	}

	public void acusarRecibo(Long id) {
		ExpedienteBandejaEntrada e = map.get(id);
		if (e != null)
			e.setRecibido(true);
			e.setFechaAcuseRecibo(new Date());
	}

	/**
	 * Metodo que elimina expediente de la lista listExpedientes para la bandeja
	 * entrada.
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public void eliminarExpediente(final Long id) {
		final ExpedienteBandejaEntrada e = map.get(id);
		if (e != null) {
			map.remove(id);
			listExpedientes.remove(e);
		}
	}

	public boolean expRepetidos(ExpedienteBandejaEntrada exp) {
		if (listExpedientes != null && !listExpedientes.isEmpty()) {
			for (ExpedienteBandejaEntrada e : listExpedientes) {
				if (!e.getId().equals(exp.getId())
						&& e.getNumeroExpediente().equals(
								exp.getNumeroExpediente())) {
					return true;
				}
			}
			return false;
		}
		return false;
	}

	/*
	 * public void cambiaFolioOPartesExpediente(Long id, String
	 * numeroExpedienteOPartes) { ExpedienteBandejaEntrada e = map.get(id); if
	 * (e != null) { e.setNumeroExpedienteOPartes(numeroExpedienteOPartes); } }
	 */
}
