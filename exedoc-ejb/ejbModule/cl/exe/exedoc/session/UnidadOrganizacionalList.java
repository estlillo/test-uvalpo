package cl.exe.exedoc.session;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import cl.exe.exedoc.entity.UnidadOrganizacional;

@Name("unidadOrganizacionalList")
public class UnidadOrganizacionalList extends EntityQuery<UnidadOrganizacional> {

	private static final long serialVersionUID = 5184922799576210689L;

	@Override
	public String getEjbql() {
		return "select unidadOrganizacional from UnidadOrganizacional unidadOrganizacional";
	}
}
