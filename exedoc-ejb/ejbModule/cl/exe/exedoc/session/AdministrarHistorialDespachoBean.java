package cl.exe.exedoc.session;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.HistorialDespacho;
import cl.exe.exedoc.entity.Persona;
import javax.ejb.Remove;
import org.jboss.seam.annotations.Destroy;

@Stateful
@Name("administrarHistorialDespacho")
@Scope(ScopeType.CONVERSATION)
public class AdministrarHistorialDespachoBean implements AdministrarHistorialDespacho{
	
	@PersistenceContext
	EntityManager em;
	
	public AdministrarHistorialDespachoBean(){
		
	}
	
	public boolean guardarHistorial(HistorialDespacho guardar){
		
		try
		{
			em.persist(guardar);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}

	@Remove
	public void remove() {
	}

	@Destroy
	public void destroy() {
	}	
}
