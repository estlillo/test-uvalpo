package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoBinario;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearDocumentoBinario {

	// public String begin();
	public String end();

	public void destroy();

	// public void ini();

	public Expediente getExpediente();

	public void setExpediente(Expediente expediente);

	public List<Documento> getListDocumentosRespuesta();

	public void setListDocumentosRespuesta(List<Documento> listDocumentosRespuesta);

//	public String getMateria();
//
//	public void setMateria(String materia);

	public List<SelectItem> getListTipoDocumentos();

	public Integer getTipoDocumentoID();

	public void setTipoDocumentoID(Integer tipoDocumentoID);

	public void agregarDocumento();

	public List<SelectPersonas> getDestinatariosDocumento();

//	public List<SelectPersonas> getDistribucionDocumento();

	public DocumentoBinario getDocumento();

	public void setDocumento(DocumentoBinario documento);

	public void listener(UploadEvent event) throws Exception;

	public ArchivoDocumentoBinario getArchivo();

	public void verArchivo();

	/* **************************************************************************
	 * Archivos Anexos - Antecedentes************************************************************************
	 */

//	public void listenerAntecedentes(UploadEvent event) throws Exception;

//	public void agregarAntecedentes();

//	public void verArchivoAntecedente(Long idArchivo);

//	public void eliminarArchivoAntecedente(Integer idNuevoArchivo);

//	public List<ArchivoAdjuntoDocumentoBinario> getListaArchivosAntecedente();

//	public String getMateriaArchivo();

//	public void setMateriaArchivo(String materiaArchivo);

	List<SelectItem> getNivelesUrgencia();

	void setNivelesUrgencia(List<SelectItem> nivelesUrgencia);

	Long getIdAlerta();

	void setIdAlerta(Long idAlerta);

	void cargarNivelesUrgencia();

	/**
	 * @param docOrg {@link Documento}
	 */
	void setDocumentoOriginal(Documento docOrg);

	/**
	 * @return {@link Documento}
	 */
	Documento getDocumentoOriginal();

	List<Persona> autocomplete(Object suggest);

	void seleccionarDest(Persona p);

	void setDestinatario(String destinatario);

	String getDestinatario();

	void removeDestinatarios(Long id);

	void listenerQuitar();

	String validaCampos();

	boolean getDeshabilitarUpload();
	
//	public List<org.richfaces.model.UploadItem> getFileUploadCurrent();

//	public void setFileUploadCurrent(List<org.richfaces.model.UploadItem> fileUploadCurrent);

//	ArchivoAdjuntoDocumentoBinario getArchivoAdjuntoTemporal();

//	void setArchivoAdjuntoTemporal(ArchivoAdjuntoDocumentoBinario doc);

//	void clearUploadAntecedente();

	void clearUploadData();
	
//	void verArchivoAntecedentes(Long id);
	
	boolean isRenderedBotonGuardar();

//	List<ArchivoAdjuntoDocumentoBinario> getArchivosAntecedentes();

	void begin();
}
