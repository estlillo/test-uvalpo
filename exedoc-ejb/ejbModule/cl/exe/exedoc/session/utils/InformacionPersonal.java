package cl.exe.exedoc.session.utils;

import java.io.Serializable;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.SolicitudDiaAdministrativo;
import cl.exe.exedoc.entity.SolicitudVacaciones;

public class InformacionPersonal implements Serializable{

	private static final long serialVersionUID = -2427219053661922382L;

	private boolean solicitud;
	private Persona solicitante;
	private SolicitudDiaAdministrativo solicitudDiaAdministrativo;
	private SolicitudVacaciones solicitudVacaciones;
	//private List<DetalleDias> diasSolicitados;
	private String grado;
	private Double diasVacacionesPendientes;
	private Double diasAdministrativosPendientes;
	private String calidadJuridica;
	private String datosObtenidos;
	
	public InformacionPersonal(){
		
	}
	
	public void setSolicitud(boolean solicitud) {
		this.solicitud = solicitud;
	}
	public boolean isSolicitud() {
		return solicitud;
	}
	public void setSolicitante(Persona solicitante) {
		this.solicitante = solicitante;
	}
	public Persona getSolicitante() {
		return solicitante;
	}
	
	public void init(){
		setSolicitante(null);
		setCalidadJuridica("");
		setGrado("");
		setDiasAdministrativosPendientes(null);
		setDiasVacacionesPendientes(null);
		setDatosObtenidos(null);
	}
	public void setDatosObtenidos(String datosObtenidos) {
		this.datosObtenidos = datosObtenidos;
	}
	public String getDatosObtenidos() {
		return datosObtenidos;
	}
	public void setDiasVacacionesPendientes(Double diasVacacionesPendientes) {
		this.diasVacacionesPendientes = diasVacacionesPendientes;
	}
	public Double getDiasVacacionesPendientes() {
		return diasVacacionesPendientes;
	}
	public void setGrado(String grado) {
		this.grado = grado;
	}
	public String getGrado() {
		return grado;
	}
	public void setCalidadJuridica(String calidadJuridica) {
		this.calidadJuridica = calidadJuridica;
	}
	public String getCalidadJuridica() {
		return calidadJuridica;
	}
	public void setDiasAdministrativosPendientes(
			Double diasAdministrativosPendientes) {
		this.diasAdministrativosPendientes = diasAdministrativosPendientes;
	}
	public Double getDiasAdministrativosPendientes() {
		return diasAdministrativosPendientes;
	}
	public void setSolicitudVacaciones(SolicitudVacaciones solicitudVacaciones) {
		this.solicitudVacaciones = solicitudVacaciones;
	}
	public SolicitudVacaciones getSolicitudVacaciones() {
		return solicitudVacaciones;
	}
	public void setSolicitudDiaAdministrativo(SolicitudDiaAdministrativo solicitudDiaAdministrativo) {
		this.solicitudDiaAdministrativo = solicitudDiaAdministrativo;
	}
	public SolicitudDiaAdministrativo getSolicitudDiaAdministrativo() {
		return solicitudDiaAdministrativo;
	}
	
}
