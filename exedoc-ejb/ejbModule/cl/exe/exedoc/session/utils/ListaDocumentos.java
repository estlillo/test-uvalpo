package cl.exe.exedoc.session.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;

@Name("listaDocumentos")
@Scope(ScopeType.CONVERSATION)
public class ListaDocumentos implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2674441459877033460L;
	private List<Documento> listDocumentosRespuesta=new ArrayList<Documento>(){
		@Override
		public boolean add(Documento e) {
			return super.add(e);
		}
	};
	private List<Documento> listDocumentosAnexo=new ArrayList<Documento>();
	private List<Documento> listDocumentosEliminados=new ArrayList<Documento>();

	public ListaDocumentos(){
		System.out.println("Creando Lista Documentos");
	}
	
	
	public void init(){
		listDocumentosRespuesta=new ArrayList<Documento>();
		listDocumentosAnexo=new ArrayList<Documento>();
		listDocumentosEliminados=new ArrayList<Documento>();
	}
	
	public void addAnexo(Documento anexo){
		listDocumentosAnexo.add(anexo);
	}
	
	public void addRespuesta(Documento respuesta){
		listDocumentosRespuesta.add(respuesta);
	}
	
	public void removeRespuesta(Documento respuesta){
		listDocumentosRespuesta.remove(respuesta);
	}

	public void removeRespuesta(int idx){
		listDocumentosRespuesta.remove(idx);
	}

	
	
	public void limpiarAnexos(){
		listDocumentosAnexo.clear();
	}
	
	public boolean existeNombreArchivo(String nombreArchivo) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getArchivo() != null && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	
	public void relacionarAnexoConDocumento(Documento documentoOriginal) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				
				
				DocumentoBinario docBin = (DocumentoBinario) doc;
				
				
				if (docBin.getIdDocumentoReferencia() != null) {
					for (Documento docResp : listDocumentosRespuesta) {
						
						if (docResp.getId().equals(new Long(docBin.getIdDocumentoReferencia()))) {
							docBin.setIdDocumentoReferencia(docResp.getIdNuevoDocumento());
							break;
						}
					}
					if (documentoOriginal.getId().equals(new Long(docBin.getIdDocumentoReferencia()))) {
						docBin.setIdDocumentoReferencia(documentoOriginal.getIdNuevoDocumento());
					}
				}
			}
		}
	}
	
	public Long buscaDocumentoReferencia(Integer idDocumento,Documento documentoOriginal) {
		Long id = null;
		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getIdNuevoDocumento().equals(idDocumento)) {
				id = doc.getId();
				break;
			}
		}
		if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
			id = documentoOriginal.getId();
		}
		return id;
	}
	
	public List<Documento> getAnexos(){
		return listDocumentosAnexo;
	}
	
	
	public List<Documento> getRespuestas(){
		return listDocumentosRespuesta;
	}
	
	public Documento getRespuesta(int idx){
		return listDocumentosRespuesta.get(idx);
	}

	public void setAnexos(List<Documento> anexos){
		this.listDocumentosAnexo=anexos;
	}
	
	public void setRespuestas(List<Documento> respuestas){
		this.listDocumentosRespuesta=respuestas;
	}

	
	
	public List<Documento> getEliminados(){
		return listDocumentosEliminados;
	}
	
	public Documento getDocumentoDeListaRespuestas( Integer numDoc) {
		return getDocumentoDeLista(listDocumentosRespuesta,numDoc);
	}
	
	public Documento getDocumentoDeListaAnexos( Integer numDoc) {
		return getDocumentoDeLista(listDocumentosAnexo,numDoc);
	}

	public void eliminarDocumento(Integer numDoc) {
		int contador = 0;
		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getIdNuevoDocumento() != null && doc.getIdNuevoDocumento().equals(numDoc)) {
				if (doc.getId() != null) {
					listDocumentosEliminados.add(doc);
				}
				listDocumentosRespuesta.remove(contador);
				break;
			}
			contador++;
		}
	}

	public void eliminarAnexoDocumento(Integer numDoc) {
		int contador = 0;
		for (Documento doc : listDocumentosAnexo) {
			if (doc.getIdNuevoDocumento() != null && doc.getIdNuevoDocumento().equals(numDoc)) {
				if (doc.getId() != null) {
					listDocumentosEliminados.add(doc);
				}
				listDocumentosAnexo.remove(contador);
				break;
			}
			contador++;
		}
	}
	
	
	private Documento getDocumentoDeLista(List<Documento> lista, Integer numDoc) {
		Documento docTmp = null;
		for (Iterator<Documento> doc = lista.iterator(); doc.hasNext();) {
			docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (id.equals(numDoc)) {
				break;
			}
		}
		return docTmp;
	}
	
	
	public void reemplazaDocumentoRespuesta(Documento documento){
		reemplazaDocumento(listDocumentosRespuesta,documento);
	}
	
	public void reemplazaDocumentoAnexo(Documento documento){
		reemplazaDocumento(listDocumentosAnexo,documento);
	}
	
	
	public final Documento actualizaListasDocumentos(Documento documento) {
		return actualizaListasDocumentos(documento, true);
	}
	
	
	public final Documento actualizaListasDocumentos(Documento documento,boolean eliminable) {
	
		Documento documentoOriginal=null;
		
		if (documento.getTipoDocumentoExpediente() != null) {
			if (documento.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				if (!existeDocumento(listDocumentosRespuesta,documento)) {
					if(eliminable){
						documento.setEliminable(true);
					}
					listDocumentosRespuesta.add(documento);
				} else {
					if (documento.getId() != null) {
						listDocumentosRespuesta.remove(documento);
						listDocumentosRespuesta.add(documento);
					} else {
						this.reemplazaDocumento(listDocumentosRespuesta,documento);
					}
				}
			} else if (documento.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
				if (!existeDocumento(listDocumentosAnexo,documento)) {
					listDocumentosAnexo.add(documento);
				} else {
					if (documento.getId() != null) {
						listDocumentosAnexo.remove(documento);
						listDocumentosAnexo.add(documento);
					} else {
						this.reemplazaDocumento(listDocumentosAnexo,documento);
					}
				}
			}
		}
		return documentoOriginal;
	}
	
	public final Documento actualizaListasDocumentos2(Documento documento){
		return actualizaListasDocumentos(documento, false);
	}
	
	public boolean existeDocumentoAnexo(Documento doc){
		return existeDocumento(listDocumentosAnexo,doc);
	}
	
	public boolean existeDocumentoRespuesta(Documento doc){
		return existeDocumento(listDocumentosRespuesta,doc);
	}
	
	private boolean existeDocumento(List<Documento> listaDocumentos, Documento documento) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(documento.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos, Documento documento) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(documento.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(documento);
			}
		}
		listaDocumentos = lista;// TODO FIXME QUE SE QUIERE HACER ACA!!!
	}

	public Documento distribuirDocumentos() {
		if(listDocumentosRespuesta.isEmpty()){
			System.out.println("distribuirDocumentos:empty");
			return null;
		}
		Documento documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
		return documentoOriginal;
	}
	
	public final void noDistribuirDocumentos(Documento documentoOriginal){
		List<Documento> list = new ArrayList<Documento>();
		list.add(documentoOriginal);
		list.addAll(listDocumentosRespuesta);
		listDocumentosRespuesta.clear();
		listDocumentosRespuesta.addAll(list);
	}


	public List<Documento> getAnexos(Integer idNuevoDocumento) {
		List<Documento> documentos = new ArrayList<Documento>();
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (idNuevoDocumento.equals(docBin.getIdDocumentoReferencia())) {
					documentos.add(docBin);
				}
			}
		}
		return documentos;

	}
	
	
}
