package cl.exe.exedoc.session.utils;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * Interface.
 * 
 * @author Ricardo
 */
@Local
public interface SeleccionarPersonas {

	/**
	 * Metodo que busca {@link Persona} segun el usuario y su vigencia.
	 */
	void buscarUsuario();

	/**
	 * Metodo que busca segun los parametros de usuario, nombres y apellido paterno.
	 */
	void buscar();
	
	/**
	 * Metodo que agrega una persona a la lista de destinatarios de un expediente.
	 * 
	 * @param persona {@link Persona}
	 * @param copia {@link Boolean}
	 */
	void agregar(final Persona persona, final Boolean copia);
	
	/**
	 * Limpiar formulario.
	 */
	void limpiarFormulario(final Boolean limpiarLista);

	/**
	 * @param resultadoBusqueda {@link List} of {@link Persona}, lista de personas.
	 */
	void setResultadoBusqueda(final List<Persona> resultadoBusqueda);

	/**
	 * @return resultadoBusqueda {@link List} of {@link Persona}, lista de personas.
	 */
	List<Persona> getResultadoBusqueda();

	/**
	 * @param usuario {@link String}
	 */
	void setUsuario(final String usuario);

	/**
	 * @return {@link String} usuario.
	 */
	String getUsuario();

	/**
	 * @param nombres {@link String}
	 */
	void setNombres(final String nombres);

	/**
	 * @return {@link String} Nombre.
	 */
	String getNombres();

	/**
	 * @param apellido {@link String}
	 */
	void setApellido(final String apellido);

	/**
	 * @return {@link String} Apellido.
	 */
	String getApellido();

	/**
	 * @return {@link List} of {@link SelectPersonas}
	 */
	List<SelectPersonas> getDestinatariosDocumento();

	/**
	 * @param destinatariosDocumento {@link List} of {@link SelectPersonas}
	 */
	void setDestinatariosDocumento(List<SelectPersonas> destinatariosDocumento);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getEstado();

	Documento getDocumento();

	void setDocumento(Documento documento);

	void agregarDestinatarioExpediente(Persona persona, Boolean copia);
}
