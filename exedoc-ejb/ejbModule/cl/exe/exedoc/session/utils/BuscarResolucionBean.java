package cl.exe.exedoc.session.utils;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Resolucion;

@Name("buscarResolucion")
@Stateless
public class BuscarResolucionBean implements BuscarResolucion {
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	@PersistenceContext
	private EntityManager entityManager;

	private Date fechaInicio;
	private Date fechaFinal;
	private String numeroResolucion;
	private List<Resolucion> resolucionesEncontradas;

	@Logger
	private Log log;

	public BuscarResolucionBean() {
		super();
	}

	@Override
	public String getNumeroResolucion() {
		return this.numeroResolucion;
	}

	@Override
	public void setNumeroResolucion(String numeroResolucion) {
		this.numeroResolucion = numeroResolucion;
	}

	@Override
	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	@Override
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Override
	public Date getFechaFinal() {
		return this.fechaFinal;
	}

	@Override
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarResoluciones() {
		log.info("Buscando resoluciones...");

		final StringBuilder sql = new StringBuilder(
				"SELECT p FROM Resolucion p WHERE ");
		List<String> filtros = new LinkedList<String>();
		if (numeroResolucion != null && numeroResolucion.length() != 0)
			filtros.add(MessageFormat.format(
					"upper(p.numeroDocumento) like upper(''%{0}%'')",
					numeroResolucion));
		if (fechaInicio != null)
			filtros.add("p.fechaDocumentoOrigen >= TO_DATE('"+simpleDateFormat.format(fechaInicio)+"','dd/mm/yyyy')");
		if (fechaFinal != null)
			filtros.add("p.fechaDocumentoOrigen <= TO_DATE('"+simpleDateFormat.format(fechaFinal)+"','dd/mm/yyyy')");
		if (filtros.size() == 0) {
			log.info("Intengo de busqueda con filtros vacios!");
			FacesMessages.instance().add("Debe filtar por algun campo.");
			return;
		}
		for (int i = 0; i < filtros.size() - 1; i++)
			sql.append(filtros.get(i) + " and ");
		sql.append(filtros.get(filtros.size() - 1));
		final Query q = entityManager.createQuery(sql.toString());
		resolucionesEncontradas = q.getResultList();
		if (resolucionesEncontradas.size() == 0)
			FacesMessages.instance().add("No se encontraron resultados.");
	}

	@Override
	public List<Resolucion> getResolucionesEncontradas() {
		return resolucionesEncontradas;
	}

	@Override
	public void setResolucionesEncontradas(
			List<Resolucion> resolucionesEncontradas) {
		this.resolucionesEncontradas = resolucionesEncontradas;
	}
	
	public String limpiarFormulario() {
		this.resolucionesEncontradas = new ArrayList<Resolucion>();
		this.numeroResolucion = null;
		this.fechaInicio = null;
		this.fechaFinal = null;
		return "Richfaces.showModalPanel('panelBuscarResolucion');";
	}

}
