package cl.exe.exedoc.session.utils;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Resolucion;

@Local
public interface BuscarResolucion {
	public String getNumeroResolucion();
	public void setNumeroResolucion(String numeroResolucion);
	public Date getFechaInicio();
	public void setFechaInicio(Date fechaInicio);
	public Date getFechaFinal();
	public void setFechaFinal(Date fechaFinal);
	public void buscarResoluciones();
	void setResolucionesEncontradas(List<Resolucion> resolucionesEncontradas);
	List<Resolucion> getResolucionesEncontradas();
	String limpiarFormulario();
}
