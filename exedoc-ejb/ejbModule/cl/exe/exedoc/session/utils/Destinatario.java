package cl.exe.exedoc.session.utils;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import cl.exe.exedoc.entity.Persona;

public class Destinatario implements Serializable {

	private static final long serialVersionUID = 6093129296945630107L;

	private Persona persona;
	private boolean copia;
	private boolean destinatarioConRecepcion;
	private boolean borrable;

	public Destinatario(Persona persona, boolean copia) {
		this.persona = persona;
		this.copia = copia;
	}

	public Persona getPersona() {
		return persona;
	}

	public boolean isCopia() {
		return copia;
	}

	public static Set<Destinatario> transform(Collection<Persona> map) {
		Set<Destinatario> dest = new HashSet<Destinatario>();
		for (Persona p : map) {
			dest.add(new Destinatario(p, false));
		}
		return dest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((persona == null) ? 0 : persona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Destinatario other = (Destinatario) obj;
		if (persona == null) {
			if (other.persona != null)
				return false;
		} else if (!persona.equals(other.persona))
			return false;
		return true;
	}

	public boolean getDestinatarioConRecepcion() {
		return destinatarioConRecepcion;
	}

	public void setDestinatarioConRecepcion(boolean destinatarioConRecepcion) {
		this.destinatarioConRecepcion = destinatarioConRecepcion;
	}

	public void setBorrable(boolean borrable) {
		this.borrable = borrable;
	}

	public boolean getBorrable() {
		return borrable;
	}

}
