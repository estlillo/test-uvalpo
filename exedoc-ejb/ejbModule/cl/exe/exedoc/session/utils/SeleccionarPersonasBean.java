package cl.exe.exedoc.session.utils;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author
 */
@Name("seleccionarPersonasBean")
@Stateless
public class SeleccionarPersonasBean implements SeleccionarPersonas {

	/**
	 * entityManager.
	 */
	@PersistenceContext
	private EntityManager entityManager;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Persona> resultadoBusqueda;

	private Boolean estado;

	@In(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	@Out(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	private Documento documento;

	private String usuario;
	private String nombres;
	private String apellido;

	/**
	 * Constructor.
	 */
	public SeleccionarPersonasBean() {
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUsuario() {

		if (usuario != null) {
			final Query q = entityManager.createQuery("from Persona where usuario like :usuario and vigente=true");
			q.setParameter("usuario", MessageFormat.format("%{0}%", usuario));
			this.setResultadoBusqueda(q.getResultList());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscar() {
		final StringBuilder sql = new StringBuilder("SELECT p FROM Persona p WHERE ");

		if (usuario != null && usuario.length() != 0) {
			sql.append(MessageFormat.format("upper(p.usuario) like upper(''%{0}%'') and ", usuario));
		}
		if (nombres != null && nombres.length() != 0) {
			sql.append(MessageFormat.format("upper(p.nombres) like upper(''%{0}%'') and ", nombres));
		}
		if (apellido != null && apellido.length() != 0) {
			sql.append(MessageFormat.format("upper(p.apellidoPaterno) like upper(''%{0}%'') and ", apellido));
		}
		sql.append(" vigente=true ");

		final Query q = entityManager.createQuery(sql.toString());
		this.setResultadoBusqueda(q.getResultList());
		if (documento != null && documento.getTipoDocumentoExpediente() != null) {
			if (documento.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				estado = Boolean.TRUE;
			} else {
				estado = Boolean.FALSE;
			}
		} else {
			estado = Boolean.FALSE;
		}
		this.limpiarFormulario(false);
	}

	@Override
	public void agregar(final Persona persona, final Boolean copia) {
		persona.setDestinatarioConCopia(copia);
		persona.setBorrable(true);
		if (documento != null && documento.getTipoDocumentoExpediente() != null) {
			if (documento.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				final String descripcion = MessageFormat.format("{0} {1} {2} - {3}", persona.getNombreApellido(),
						persona.getCargo().getDescripcion(), persona.getCargo().getUnidadOrganizacional()
								.getDescripcion(), copia ? "COPIA" : "DIRECTO");
				final SelectPersonas selectPersona = new SelectPersonas(descripcion, persona);
				final int index = Collections.binarySearch(destinatariosDocumento, selectPersona,
						ComparatorUtils.compararListaSelectPersonas);
				if (index >= 0) {
					FacesMessages.instance().add("El usuario ya esta agregado");
				} else {
					destinatariosDocumento.add(selectPersona);
				}
			} else {
				listDestinatarios.add(persona);
			}
		} else {
			listDestinatarios.add(persona);
		}

		// this.limpiarFormulario(true);
	}

	@Override
	public void limpiarFormulario(final Boolean limpiarLista) {

		if (resultadoBusqueda != null && limpiarLista) {
			this.limpiarLista();
		}
		this.usuario = "";
		this.nombres = "";
		this.apellido = "";
	}

	/**
	 * Limpiar lista resultadoBusqueda.
	 */
	private void limpiarLista() {
		this.resultadoBusqueda.clear();
	}

	@Override
	public void setResultadoBusqueda(final List<Persona> resultadoBusqueda) {
		this.resultadoBusqueda = resultadoBusqueda;
	}

	@Override
	public List<Persona> getResultadoBusqueda() {
		return resultadoBusqueda;
	}

	@Override
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	@Override
	public String getUsuario() {
		return usuario;
	}

	@Override
	public void setNombres(final String nombres) {
		this.nombres = nombres;
	}

	@Override
	public String getNombres() {
		return nombres;
	}

	@Override
	public void setApellido(final String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String getApellido() {
		return apellido;
	}

	@Override
	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}

	@Override
	public void setDestinatariosDocumento(final List<SelectPersonas> destinatariosDocumento) {
		this.destinatariosDocumento = destinatariosDocumento;
	}

	@Override
	public Boolean getEstado() {
		return estado;
	}

	@Override
	public Documento getDocumento() {
		return documento;
	}

	@Override
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	
	@Override
	public void agregarDestinatarioExpediente(final Persona persona, final Boolean copia) {
		//persona.setDestinatarioConCopia(copia);
		//persona.setBorrable(true);
		this.compruebaExpedientes(persona);
		persona.setDestinatarioConCopia(copia);
		persona.setBorrable(true);
		this.listDestinatarios.add(persona);
	}
	
	@SuppressWarnings("unchecked")
	private void compruebaExpedientes(final Persona p) {
		final StringBuilder existeEnBandejaQL = new StringBuilder(
				"SELECT e.copia FROM Expediente e WHERE ");
		existeEnBandejaQL.append("e.numeroExpediente = '"
				+ this.expediente.getNumeroExpediente() + "' AND ");
		existeEnBandejaQL
				.append("((e.destinatario.id = "
						+ p.getId()
						+ " AND e.fechaDespacho IS NOT NULL AND e.fechaAcuseRecibo IS NULL ) ");
		existeEnBandejaQL
				.append("OR (e.emisor.id = "
						+ p.getId()
						+ " AND e.fechaDespacho IS NULL AND e.fechaAcuseRecibo IS NOT NULL AND e.fechaAcuseRecibo IS NOT NULL )) ");
		existeEnBandejaQL
				.append("AND e.archivado IS NULL ");
		Boolean copia = null;
		try {
			final List<Boolean> copias = entityManager.createQuery(
					existeEnBandejaQL.toString()).getResultList();
			if (copias != null && !copias.isEmpty()) {
				copia = copias.get(0);
			}
			if (copia != null) {
				if (!copia) {
					FacesMessages.instance().add(
							"El expediente ya se encuentra en la Bandeja de entrada Directo de: "
									+ p.getNombreApellido());
				} else {
					FacesMessages.instance().add(
							"El expediente ya se encuentra en la Bandeja de entrada Copia de: "
									+ p.getNombreApellido());
				}
			}
		} catch (NoResultException nre) {
		}
	}
}
