package cl.exe.exedoc.session.utils;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.util.SelectPersonas;

public class ExpedienteDocumento implements Serializable{

	private static final long serialVersionUID = 9013468099703942804L;
	private Expediente expediente;
	private Documento documento;
	private Documento documentoOriginal;
	private List<SelectPersonas> emisorDocumento;
	private Set<Destinatario> listDestinatarios;
	private List<SelectPersonas> destinatariosDocumento;
	private List<SelectPersonas> distribucionDocumento;
	private List<Observacion> observaciones;
	private Boolean vistoDesdeReporte;
	private Boolean desdeDespacho;
	
	public ExpedienteDocumento(){
		System.out.println("Crear Expediente Documento");
	}
	
	public Set<Destinatario> getListDestinatarios() {
		return listDestinatarios;
	}
	public void setListDestinatarios(Set<Destinatario> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}
	public Expediente getExpediente() {
		return expediente;
	}
	public Documento getDocumento() {
		return documento;
	}
	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}
	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}
	public void setDocumentoOriginal(Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}
	public void setVistoDesdeReporte(Boolean vistoDesdeReporte) {
		this.vistoDesdeReporte = vistoDesdeReporte;
	}
	public Boolean getVistoDesdeReporte() {
		return vistoDesdeReporte;
	}
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}
	public Boolean getDesdeDespacho() {
		return desdeDespacho;
	}
	public void setDestinatariosDocumento(List<SelectPersonas> destinatariosDocumento) {
		this.destinatariosDocumento = destinatariosDocumento;
	}
	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}
	public void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento) {
		this.distribucionDocumento = distribucionDocumento;
	}
	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}
	public void setObservaciones(List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}
	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	public void setEmisorDocumento(List<SelectPersonas> emisorDocumento) {
		this.emisorDocumento = emisorDocumento;
	}

	public List<SelectPersonas> getEmisorDocumento() {
		return emisorDocumento;
	}
	
}
