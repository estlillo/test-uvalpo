package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("seleccionarVisaciones")
public class SeleccionarVisacionesBean implements SeleccionarVisaciones {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Documento documento;
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<VisacionEstructuradaDocumento> listVisacionEstructuradaEliminada;
	@In(required = true)
	private Persona usuario;
	
	private static final String SELECCIONAR_INICIO = "<<Seleccionar>>";
	
	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	private List<SelectItem> buscarOrganizaciones() {
		limpiar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@SuppressWarnings("unchecked")
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiar();
		organizacion = idOrganizacion;
		
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			Departamento departamento = departamentos.get(0);
			if (departamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	@Override
	public void agregarDestinatarioById(Long id) {
		try {// por la actualizacion de ajax

			Persona p = (Persona) em.createNamedQuery("Persona.findById")
					.setParameter("id", id).getSingleResult();
			agregar(p);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("cargando datos");
		}
	}
	private void agregar(Persona p){
		if (documento.getVisacionesEstructuradas() == null) {
			documento.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
		}
		if (esFirmante(p)) {
			FacesMessages.instance().add("No puede visar si es firmante");
			return;
		}
		if (!contiene(p)) {
			VisacionEstructuradaDocumento ved = new VisacionEstructuradaDocumento();
			ved.setOrden(documento.getVisacionesEstructuradas().size() + 1);
			ved.setPersona(p);
			ved.setDocumento(documento);
			documento.getVisacionesEstructuradas().add(ved);
			this.restablecerOrden(ved.getOrden());
			limpiar();
//			listOrganizacion.clear();
//			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) this.listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			this.buscarUnidadesOrganizacionales();
		} else {
			FacesMessages.instance().add("No se puede visar más de una vez");
			return;
		}
	}
	public void agregar() {
		try {// por la actualizacion de ajax
			if (!persona.equals(JerarquiasLocal.INICIO)) {
				Persona p = (Persona) em.createNamedQuery("Persona.findById").setParameter("id", this.persona).getSingleResult();
				agregar(p);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("cargando datos");
		}
	}

	private boolean contiene(Persona p) {
		List<VisacionEstructuradaDocumento> listVED = documento.getVisacionesEstructuradas();
		for (VisacionEstructuradaDocumento ved : listVED) {
			if (ved.getPersona().getId().equals(p.getId())) {
				return true;
			}
		}
		return false;
	}

	private boolean esFirmante(Persona p) {
		List<FirmaEstructuradaDocumento> listFED = documento.getFirmasEstructuradas();
		for (FirmaEstructuradaDocumento fed : listFED) {
			if (fed.getPersona().getId().equals(p.getId())) {
				return true;
			}
		}
		return false;
	}

	private void limpiar() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void eliminar(Integer orden) {
		int contador = 0;
		for (VisacionEstructuradaDocumento ved : documento.getVisacionesEstructuradas()) {
			if (ved.getOrden().equals(orden)) {
				VisacionEstructuradaDocumento v = new VisacionEstructuradaDocumento();
				v = ved;
				if (listVisacionEstructuradaEliminada == null)
					listVisacionEstructuradaEliminada = new ArrayList<VisacionEstructuradaDocumento>();
				listVisacionEstructuradaEliminada.add(v);
				documento.getVisacionesEstructuradas().remove(contador);
				restablecerOrden(orden);
				break;
			}
			contador++;
		}
	}

	private void restablecerOrden(Integer orden) {
		int contador = 1;
		for (VisacionEstructuradaDocumento ved : documento.getVisacionesEstructuradas()) {
			if (!ved.getOrden().equals(contador)) {
				ved.setOrden(contador);
			}
			contador++;
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(Long persona) {
		this.persona = persona;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		//this.getListOrganizacion();
		//this.getListDivision();
		return listDepartamento;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public List<VisacionEstructuradaDocumento> getVisaciones() {
		return documento.getVisacionesEstructuradas();
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			this.buscarUnidadesOrganizacionales();
		}
		return listOrganizacion;
	}

	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
}
