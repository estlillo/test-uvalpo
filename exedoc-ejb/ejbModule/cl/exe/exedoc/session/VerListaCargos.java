package cl.exe.exedoc.session;

import java.util.ArrayList;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Cargo;

@Local
public interface VerListaCargos {  
	
	//seam-gen methods
	public String begin();
	public String increment();
	public String end();
	public int getValue();
	public void destroy();
	
   //add additional interface methods here	
	public ArrayList<Cargo> getCrgList();
}
