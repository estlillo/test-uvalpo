package cl.exe.exedoc.session;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;

@Stateful
@Scope(ScopeType.SESSION)
@Name("bandejaEntradaCopia")
public class BandejaEntradaCopiaBean implements BandejaEntradaCopia {

	private Map<Long, Boolean> selectedRecibosCopia;

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	@EJB
	private ManejarExpedienteInterface me;

	@In(required = false, scope = ScopeType.SESSION)
	@Out(scope = ScopeType.SESSION)
	private BandejaEntradaData dataCopia;

	public BandejaEntradaData getDataCopia() {
		return dataCopia;
	}

	@Begin(join = true)
	public String begin() {
		log.info("beginning conversation: bandejaEntradaCopia");
		selectedRecibosCopia = new HashMap<Long, Boolean>();
		buscarExpedientes();
		homeCrear = "bandejaEntradaCopia";
		return "bandejaEntradaCopia";
	}

	public void buscarExpedientes() {
		log.info("Buscando expedientes de: " + usuario.getId());
		List<ExpedienteBandejaEntrada> lista;
		lista = me.listarExpedienteBandejaEntradaCopiaUsuario(usuario);
		dataCopia = new BandejaEntradaData(lista);
		desdeDespacho = Boolean.FALSE;
	}

	public String acusarRecibo() {
		log.info("acusar recibo ..."+selectedRecibosCopia);
		long ini = System.currentTimeMillis();
		Iterator<Long> iter = selectedRecibosCopia.keySet().iterator();
		boolean accionAcusaRecibo = false;
		
		//acuso de recibo ok
		String msjAcuseRecibo = "Los siguientes Expedientes se acuso recibo exitosamente : ";
		boolean flagAcuse = false;
		// eliminados o reasignados
		String msjEliminado = "Existen Expedientes retirados de su Bandeja";
		boolean flagEliminado = false;

		while (iter.hasNext()) {
			Long id = iter.next();
			if (((Boolean) selectedRecibosCopia.get(id))) {
				accionAcusaRecibo = true;
				Expediente expediente = me.buscarExpediente(id);
				if (expediente == null || 
						(expediente != null && expediente.getCancelado() != null) || 
						(expediente != null && expediente.getReasignado() != null)) {
					if(!flagEliminado) {
						if (expediente != null) {
							if (!msjEliminado.substring(msjEliminado.length()-1).equals(" ")) {
								msjEliminado += " : " + expediente.getNumeroExpediente();
							} else {
								if (!msjEliminado.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
									msjEliminado += " - " + expediente.getNumeroExpediente();
								}
							}
						}
					}
					flagEliminado = true;
				} else {
					if(!flagAcuse){
						msjAcuseRecibo += expediente.getNumeroExpediente();
					} else {
						if (!msjAcuseRecibo.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
							msjAcuseRecibo = msjAcuseRecibo + " - " + expediente.getNumeroExpediente();
						}
					}
					flagAcuse = true;
					me.acusarReciboExpediente(expediente, usuario);
					dataCopia.acusarRecibo(expediente.getId());
				}
			}
		}
		
		if(flagAcuse){
			FacesMessages.instance().add(msjAcuseRecibo);
		}
		if(flagEliminado){
			FacesMessages.instance().add(msjEliminado);
		}
		if (!accionAcusaRecibo) {
			FacesMessages.instance().add("Debe seleccionar al menos un Expediente.");
		} else {
			selectedRecibosCopia.clear();
			buscarExpedientes();
		}
		long fin = System.currentTimeMillis();
		log.info("acuso recibo en #0 milisegundos", (fin - ini));
		selectedRecibosCopia.clear();
		buscarExpedientes();
		return "bandejaEntradaCopia";
	}

	public String obtenerEstilo(ExpedienteBandejaEntrada exp) {
		String estilo = "";

		if (exp.getTipoDocumento().equals("Consulta Ciudadana") || exp.getTipoDocumento().equals("Consulta")) {
			estilo = "consulta";
		}

		if (exp.getTipoDocumento().equals("Respuesta Consulta Ciudadana") || exp.getTipoDocumento().equals("Respuesta Consulta")) {
			estilo = "respuesta";
		}

		if (dataCopia.expRepetidos(exp)) {
			estilo = "repetidos";
		}

		return estilo;
	}

	@End
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Map<Long, Boolean> getSelectedRecibosCopia() {
		return selectedRecibosCopia;
	}

	public void setSelectedRecibosCopia(Map<Long, Boolean> selectedRecibosCopia) {
		this.selectedRecibosCopia = selectedRecibosCopia;
	}

	@Override
	public Boolean getDesdeDespacho() {
		if (desdeDespacho == null) {
			desdeDespacho = Boolean.FALSE;
		}
		return desdeDespacho;
	}

	@Override
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}
}
