package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.pojo.documento.DocumentoContraloria;
import cl.exe.exedoc.pojo.documento.DocumentoDescarga;

@Local
public interface DocumentosContraloria {

	public String begin();

	public String end();

	public void destroy();

	public List<DocumentoContraloria> getDocumentosAContraloria();

	public List<DocumentoDescarga> getDocumentosDescarga();

	public void descargar(DocumentoContraloria documento);

	public void descargarArchivo(DocumentoDescarga archivo);

	public void descargarZip();

}
