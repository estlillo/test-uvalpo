package cl.exe.exedoc.session;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Contenido;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DestinatariosFirmaDocumentoMail;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("crearMemorandum")
public class CrearMemorandumBean implements CrearMemorandum {
	
	private static final String BLANK = " ";
	private static final String STRING = "/";
	private Long rand = 0L;
	
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	
	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosAnexo;*/

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Memorandum memorandum;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private String parrafo;

	private Boolean firmado;
	private Boolean visado;
	private Boolean completado;

	public void visualizar() {
		if (!firmado  && !visado) {
			armaDocumento(false, false);
		}
	}

	public void guardar() {
		armaDocumento(false, false);
		FacesMessages.instance().add("Documento Guardado");
	}

	public void firmar() {
		if (verifica()) {
			if (usuario.getId().equals(memorandum.getIdEmisor())) {
				memorandum.setFechaDocumentoOrigen(new Date());
				this.armaDocumento(false, true);
				//agregarListaDestinatariosExpediente();
				List<Persona> destinatariosEmail = new ArrayList<Persona>();
				for (SelectPersonas sp : distribucionDocumento) {
					destinatariosEmail.add(sp.getPersona());
				}
				me.enviarMailNotificacionFirma(expediente, memorandum, destinatariosEmail);
				FacesMessages.instance().add("Documento Firmado");
//				if (expediente != null && expediente.getId() != null) {
//					DestinatariosFirmaDocumentoMail destMail = me.obtenerDatosNotificacion(memorandum, expediente.getNumeroExpediente());
//					me.firmarNotificacionPorEmail(destMail);
//				}
			} else {
				FacesMessages.instance().add("Ud. No puede Firmar este Documento, sólo lo puede hacer el Emisor (DE)");
			}
		}
	}

	public void agregarListaDestinatariosExpediente() {
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		for (SelectPersonas sp : destinatariosDocumento) {
			sp.getPersona().setDestinatarioConCopia(false);
			listDestinatarios.add(sp.getPersona());
		}
		for (SelectPersonas sp : distribucionDocumento) {
			sp.getPersona().setDestinatarioConCopia(true);
			listDestinatarios.add(sp.getPersona());
		}
	}
	
	
	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos() {
		if (this.memorandum.getArchivosAdjuntos() != null) {
			int cont = 0;

			for (ArchivoAdjuntoDocumentoElectronico a : this.memorandum.getArchivosAdjuntos()) {
				a.setIdNuevoArchivo(cont++);
			}
			return this.memorandum.getArchivosAdjuntos();
		}
		return new ArrayList<ArchivoAdjuntoDocumentoElectronico>();

	}


	public void visar() {
		if (verifica()) {
			if (!yaVisado()) {
				if (autorNoVisa()) {
					this.armaDocumento(true, false);
					FacesMessages.instance().add("Documento Visado");
				} else {
					FacesMessages.instance().add("El autor no puede visar el documento");
				}
			} else {
				FacesMessages.instance().add("Usted ya viso el Documento");
			}
		}
	}

	/**
	 * Permite validar que el Usuario que intenta visar el Documento, no sea el
	 * Autor. Retorna true en caso de ser inválido y false en caso de ser válido
	 * el usuario.
	 * 
	 * @return
	 */
	public boolean autorNoVisa() {
		if (!memorandum.getAutor().getId().equals(usuario.getId())) {
			return true;
		}
		return false;
	}

	public boolean yaVisado() {
		if (memorandum != null && memorandum.getVisaciones() != null) {
			for (VisacionDocumento visa : memorandum.getVisaciones()) {
				if (visa.getPersona().getId().equals(usuario.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public String end() {
		log.info("ending conversation");
//		if (firmado && this.expediente == null) {
//			guardarYCrearExpediente();
//			return "";
//		}
		parrafo = "";
		destinatariosDocumento = null;
		distribucionDocumento = null;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}
		if (memorandum.getId() != null && expediente != null && expediente.getId() != null) {
			memorandum.setEliminable(md.isEliminable(memorandum, expediente, usuario));
		}
		return homeCrear;
	}

	public String completar() {
		this.memorandum.setCompletado(true);
		this.memorandum.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.memorandum, this.usuario));
		return end();
	}

	private boolean documentoValido;
	
	@Override
	public boolean isDocumentoValido() {
		return documentoValido;
	}
	
	public String agregarDocumento() {
		documentoValido = verifica();
		if (documentoValido) {
			armaDocumento(false, false);
			return end();
		}
		return "";
	}

	public void guardarYCrearExpediente() {
		documentoValido = verifica();
		if (documentoValido) {
			armaDocumento(false, false);
			documentoOriginal = listDocumentosRespuesta.get(0);
			listDocumentosRespuesta.remove(0);
		}
	}

	private boolean verifica() {
		boolean guardar = true;
		if (memorandum.getMateria() == null || (memorandum.getMateria() != null && memorandum.getMateria().trim().isEmpty())) {
			FacesMessages.instance().add("La Materia es obligatoria");
			guardar = false;
		}
		if (memorandum.getEmisor() == null) {
			FacesMessages.instance().add("Debe Seleccionar un Emisor (DE) al Documento");
			guardar = false;
		}
		if (destinatariosDocumento == null || destinatariosDocumento.size() == 0) {
			FacesMessages.instance().add("Debe Seleccionar por lo menos un Destinatario (A) al Documento");
			guardar = false;
		}
		return guardar;
	}

	private void persistirDocumento() {
	    try {
    		if (this.memorandum.getId() != null) {
    			md.actualizarDocumento(this.memorandum);
    
    		} else if (this.expediente != null && this.expediente.getId() != null) {
    			me.agregarRespuestaAExpediente(expediente, this.memorandum, true);
    		}
	    } catch (DocumentNotUploadedException e) {
	        e.printStackTrace();
	    }
		/*if (this.expediente != null) {
			for (Documento doc : listDocumentosAnexo) {
				if (doc.getId() == null) {
					if (doc instanceof DocumentoBinario) {
						DocumentoBinario docBin = (DocumentoBinario) doc;
						if (docBin.getIdDocumentoReferencia().equals(this.memorandum.getIdNuevoDocumento())) {
							me.agregarAnexoAExpediente(expediente, doc, true, this.memorandum.getId());
						}
						doc.setEnEdicion(true);
					}
				}
			}
		}*/
	}

	private void armaDocumento(boolean visar, boolean firmar) {
		this.memorandum.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		// TipoDocumento: 7,'Memorandum'
		this.memorandum.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.MEMORANDUM));

		if (!this.getFirmado()) {
			if (!visar && !firmar) {
				if (this.memorandum.getId() != null) {
					this.memorandum.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
					this.memorandum.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.memorandum, this.usuario));
				} else {
					this.memorandum.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
					this.memorandum.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.memorandum, this.usuario));
				}
			}
			if (firmar) {
				this.repositorio.firmar(this.memorandum, usuario);
				this.memorandum.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.memorandum, this.usuario));
				//Pruebas mias
				List<Expediente> padre = me.obtienePrimerExpediene(expediente);
				List<Persona> dest = me.buscarDestinatarios(padre);
				for (Persona expediente : dest) {
					me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
				}
			}
			if (visar) {
				this.repositorio.visar(this.memorandum, usuario);
				this.memorandum.addBitacora(new Bitacora(EstadoDocumento.VISADO, this.memorandum, this.usuario));
			}
		}

		if (this.memorandum.getCompletado() == null) {
			this.memorandum.setCompletado(false);
		}

		this.limpiarDestinatarios(this.memorandum.getId());
		this.setDistribucionDocumento(this.memorandum);
		this.setDestinatarioDocumento(this.memorandum);

		this.memorandum.setReservado(false);

		this.setParrafo();

		if (this.memorandum.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.memorandum);
			} else {
				if (this.memorandum.getId() != null) {
					listDocumentosRespuesta.remove(this.memorandum);
					listDocumentosRespuesta.add(this.memorandum);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} /*else if (this.memorandum.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
			if (!existeDocumento(listDocumentosAnexo)) {
				listDocumentosAnexo.add(this.memorandum);
			} else {
				if (this.memorandum.getId() != null) {
					listDocumentosAnexo.remove(this.memorandum);
					listDocumentosAnexo.add(this.memorandum);
				} else {
					this.reemplazaDocumento(listDocumentosAnexo);
				}
			}
		}*/ else if (this.memorandum.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = memorandum;
		}
		
		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		
		if (this.listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico doc : this.listaArchivosEliminados) {
				if (doc.getId() != null)
					this.eliminaAntecedente(doc.getId());
			}
		}
		this.listaArchivosEliminados = null;
		
		persistirDocumento();
	}

	private void setDestinatarioDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDestinatarios(ddList);
	}

	private void setDistribucionDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);

		}
		dc.setDistribucion(ddList);
	}

	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.memorandum.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.memorandum.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.memorandum);
			}
		}
		listaDocumentos = lista;
	}

	private void setParrafo() {
		if (this.memorandum.getContenido() == null) {
			List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
			Contenido parrafo = new Contenido();
			parrafo.setNumero(1L);
			parrafo.setDocumento(this.memorandum);
			listaParrafos.add(parrafo);
			this.memorandum.setContenido(listaParrafos);
		}
		this.memorandum.getContenido().get(0).setCuerpo(this.parrafo);
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String getParrafo() {
		parrafo = "";
		if (this.memorandum != null && this.memorandum.getContenido() != null && this.memorandum.getContenido().size() != 0) {
			parrafo = this.memorandum.getContenido().get(0).getCuerpo();
		}
		return parrafo;
	}

	public void setParrafo(String parrafo) {
		if (this.memorandum.getContenido() == null) {
			List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
			Contenido cont = new Contenido();
			cont.setNumero(1L);
			cont.setDocumento(this.memorandum);
			listaParrafos.add(cont);
			this.memorandum.setContenido(listaParrafos);
		}
		this.memorandum.getContenido().get(0).setCuerpo(parrafo);
		this.parrafo = parrafo;
	}

	public Memorandum getMemorandum() {
		return memorandum;
	}

	public Boolean getFirmado() {
		if (this.memorandum != null && this.memorandum.getFirmas() != null && this.memorandum.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public Boolean getVisado() {
		if (this.memorandum != null && this.memorandum.getVisaciones() != null && this.memorandum.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public List<VisacionDocumento> getVisaciones() {
		List<VisacionDocumento> visaciones = null;
		if (this.memorandum != null && this.memorandum.getVisaciones() != null) {
			visaciones = memorandum.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}

	public Boolean getCompletado() {
		if (this.memorandum != null && this.memorandum.getCompletado() != null && !this.memorandum.getCompletado()) {
			completado = true;
		} else {
			completado = false;
		}
		return completado;
	}

	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getVisado()) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isEditableSinVisa() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isDespachable() {
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		return true;
	}

	public boolean isRenderedBotonVisar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.memorandum.getId() == null) {
			return false;
		}
		if (!autorNoVisa()) {
			return false;
		}
		if (!usuario.getRoles().contains(Rol.VISADOR)) {
			return false;
		}
		if (yaVisado()) {
			return false;
		}
		return !isRenderedBotonFirmar();
	}

	public boolean isRenderedBotonFirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (!isEditableSinVisa()) {
			return false;
		}
		if (usuario.getId().equals(memorandum.getIdEmisor()) && this.memorandum.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (getFirmado()) {
			if (listDestinatarios != null && listDestinatarios.size() != 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	/***************************************************************************
	 * Archivos Anexos - Antecedentes
	 **************************************************************************/
	
	private ArchivoAdjuntoDocumentoElectronico archivoAnexo;
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;
	private String materiaArchivo;

//	private List<ArchivoDocumentoBinario> archivosAnexos;
//	private String materiaArchivo;
//	private int contadorDocumentos;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	public void limpiarAnexos() {
		archivoAnexo = null;
		materiaArchivo = null;
	}

	public int getMaxFilesQuantity() {
		int valor = 20;
		/*
		 * if (archivosAnexos == null || archivosAnexos.isEmpty()) { valor = 1;
		 * } else { valor = archivosAnexos.size(); }
		 */
		return valor;
	}

	public void listener(UploadEvent event) throws Exception {
		archivoAnexo = new ArchivoAdjuntoDocumentoElectronico();
		UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		String contentType = item.getContentType();
		archivoAnexo.setNombreArchivo(fileName);
		archivoAnexo.setContentType(contentType);
		byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivoAnexo.setArchivo(data);
	}
	
	boolean validarAntecedente;
	
	@Override
	public boolean isValidarAntecedente() {
		return validarAntecedente;
	}
	
	@Override
	public void removeArchivoAnexo() {
		archivoAnexo = null;
	}
	
	@Override
	public void agregarAntecedenteArchivo() {
		validarAntecedente = true;
		if (materiaArchivo == null || (materiaArchivo != null && materiaArchivo.trim().isEmpty())) {
			FacesMessages.instance().add("Debe ingresar Materia");
			validarAntecedente = false;
		}
		if (archivoAnexo == null || (archivoAnexo != null && archivoAnexo.getArchivo() == null)) {
			FacesMessages.instance().add("Debe ingresar Archivo");
			validarAntecedente = false;
		}
		if (validarAntecedente) {
			if (this.memorandum.getArchivosAdjuntos() == null) {
				this.memorandum.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
			}
			archivoAnexo.setDocumentoElectronico(this.memorandum);
			archivoAnexo.setFecha(new Date());
			archivoAnexo.setAdjuntadoPor(usuario);
			archivoAnexo.setMateria(materiaArchivo);
			this.memorandum.getArchivosAdjuntos().add(archivoAnexo);
		}
	}

	/*private boolean existeNombreArchivo(String nombreArchivo) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getArchivo() != null && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) {
					return true;
				}
			}
		}
		return false;
	}*/

	public void verArchivo(Long idArchivo) {
		System.out.println("idArchivo: " + idArchivo);
		//ArchivoDocumentoBinario a = em.find(ArchivoDocumentoBinario.class, idArchivo);
		//buscarArchivoRepositorio(a);
		try {
            if (!DocUtils.getFile(idArchivo, em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + idArchivo + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private void buscarArchivoRepositorio(Archivo a) {
		/*byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		if (data != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			try {
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
				response.setContentType(a.getContentType());
				response.setHeader("Content-disposition", "attachment; filename=\"" + a.getNombreArchivo() + "\"");
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
				facesContext.responseComplete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + a.getId() + ")");
		}*/
	}
	
	@Override
	public void eliminarArchivoAntecedente(ArchivoAdjuntoDocumentoElectronico archivo) {
		if (this.listaArchivosEliminados == null) {
			this.listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		}
		if (archivo.getId() != null) {
			this.listaArchivosEliminados.add(archivo);
		}
		this.memorandum.getArchivosAdjuntos().remove(archivo);
	}

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public void setMateriaArchivo(String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	/*public List<Documento> getListDocumentosAnexo() {
		List<Documento> documentos = new ArrayList<Documento>();
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (memorandum.getIdNuevoDocumento().equals(docBin.getIdDocumentoReferencia())) {
					documentos.add(docBin);
				}
			}
		}
		return documentos;
	}*/

	/***************************************************************************
	 * DESPACHAR EXPEDIENTES
	 **************************************************************************/
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	private List<Long> idExpedientes;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

//	public String despacharExpediente() {
//		if (listDestinatarios.size() != 0) {
//			armaDocumento(false, false);
//			if (expediente == null || expediente.getId() == null) {
//				distribuirDocumentos();
//			}
//			this.guardarExpediente();
//			desdeDespacho = true;
//			return "bandejaSalida";
//		} else {
//			FacesMessages.instance().add("Debe Seleccionar un Destinatario del Expediente");
//			return "";
//		}
//	}
//
//	@End
//	private void guardarExpediente() {
//		if (expediente == null) {
//			expediente = new Expediente();
//			expediente.setFechaIngreso(new Date());
//			expediente.setEmisor(usuario);
//			expediente.setObservaciones(null);
//			me.crearExpediente(expediente);
//			try {
//                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
//            } catch (DocumentNotUploadedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//		} else {
//			me.eliminarExpedientes(expediente, false);
//			expediente.setObservaciones(null);
//			me.modificarExpediente(expediente);
//
//			// Cerrar plazo al documento original.
//			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado() && !this.documentoOriginal.getId().equals(this.memorandum.getId())) {
//				this.documentoOriginal.setCompletado(true);
//				this.documentoOriginal.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
//				try {
//                    md.actualizarDocumento(this.documentoOriginal);
//                } catch (DocumentNotUploadedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//			}
//
//		}
//
//		if (listDocumentosEliminados != null) {
//			for (Documento doc : listDocumentosEliminados) {
//				md.eliminarDocumento(doc.getId());
//			}
//		}
//
//		for (Documento doc : listDocumentosRespuesta) {
//		    try {
//    			if (doc.getId() == null) {
//    				me.agregarRespuestaAExpediente(expediente, doc, true);
//    			} else {
//    				// Cerrar plazo a los documentos respuesta.
//    				if (doc.getCompletado() != null && !doc.getCompletado() && !doc.getId().equals(this.memorandum.getId())) {
//    					doc.setCompletado(true);
//    					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
//    					md.actualizarDocumento(doc);
//    				}
//    			}
//		    } catch (DocumentNotUploadedException e) {
//		        e.printStackTrace();
//		    }
//		}
//
//		/*for (Documento doc : listDocumentosAnexo) {
//			if (doc.getId() == null) {
//				if (doc instanceof DocumentoBinario) {
//					DocumentoBinario docBin = (DocumentoBinario) doc;
//					Long idDocumentoReferencia = buscaDocumentoReferencia(docBin.getIdDocumentoReferencia());
//					if (idDocumentoReferencia != null) {
//						me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia);
//					} else {
//						me.agregarAnexoAExpediente(expediente, doc, true);
//					}
//				} else {
//					me.agregarAnexoAExpediente(expediente, doc, true);
//				}
//			} else {
//				md.actualizarDocumento(doc);
//			}
//		}*/
//
//		this.guardarObservaciones();
//		idExpedientes = new ArrayList<Long>();
//		Date fechaIngreso = new Date();
//
//		for (Persona de : listDestinatarios) {
//			try {
//                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
//            } catch (DocumentNotUploadedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//		}
//
//		List<Expediente> expedientes = new ArrayList<Expediente>();
//
//		for (Long id : idExpedientes) {
//			Expediente e = me.buscarExpediente(id);
//			me.despacharExpediente(e);
//			expedientes.add(e);
//		}
//		me.despacharExpediente(expediente);
//		//me.despacharNotificacionPorEmail(expedientes);
//
//		FacesMessages.instance().add("Expediente Despachado");
//	}

//	private Long buscaDocumentoReferencia(Integer idDocumento) {
//		Long id = null;
//		for (Documento doc : listDocumentosRespuesta) {
//			if (doc.getIdNuevoDocumento().equals(idDocumento)) {
//				id = doc.getId();
//				break;
//			}
//		}
//		if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
//			id = documentoOriginal.getId();
//		}
//		return id;
//	}

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	public void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}
	
//	@Override
//	public String getVisaciones() {
//		if (this.memorandum.getVisacionesEstructuradas() == null) {
//			this.memorandum.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
//		}
//		final List<VisacionEstructuradaDocumento> visaciones = this.memorandum.getVisacionesEstructuradas();
//
//		final StringBuilder sb = new StringBuilder();
//		for (int i = visaciones.size() - 1; i >= 0; i--) {
//			sb.append(visaciones.get(i).getPersona().getNombreApellido() + STRING + BLANK);
//		}
//		// sb.append(resolucion.getAutor().getIniciales().toLowerCase());
//
//		return sb.toString();
//	}
	
	@Override
	public List<FirmaEstructuradaDocumento> getFirmas() {
		if (this.memorandum.getFirmasEstructuradas() == null) {
			this.memorandum.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		return this.memorandum.getFirmasEstructuradas();
	}
	
	private String cacheRandom(){
		
		String idCache = "0";
		try{
			idCache = String.valueOf(Math.floor((Math.random()*1000))); 
		  }catch(Exception ex){
		    ex.printStackTrace();
		  }
		return(idCache.replace(".", ""));
	}
	
	@Override
	public Long getRand() {
		try{
			rand = Long.valueOf(cacheRandom()); 
		  }catch(Exception ex){
		    ex.printStackTrace();
		  }
		
		return rand;
	}

	@Override
	public void setRand(Long rand) {
		this.rand = rand;
	}
	
//	@Override
//	public String getUrlFirma() {
//		  Properties defaultProps = new Properties();
//	        try {
//	            defaultProps.load(getArchivoStream("customizacion.properties"));
//	        }
//	        catch (Exception e) {
//	            log.error("No se pudo cargar archivo de configuracion customizacion.properties");
//	        }
//
//			final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
//			return urlDocumento;
//	}
	
	@Override
	public String getUrlFirma() {
		  Properties defaultProps = new Properties();
	      try {
	         // defaultProps.load(getArchivoStream("customizacion.properties"));
	          final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("exedoc/customizacion.properties");
	          defaultProps.load(resourceAsStream);

	      }
	      catch (Exception e) {
	          log.error("No se pudo cargar archivo de configuracion customizacion.properties");
	      }
	      String default_port = "8080";
	      String port = defaultProps.getProperty("default_port", default_port.toString());
	      String IP = defaultProps.getProperty("IP", System.getProperty("jboss.bind.address"));
	      
			//final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
	      final String urlDocumento = "http://" + IP  + ":" + port + STRING;
			return urlDocumento;
	}
	
	private InputStream getArchivoStream(String nombre) {
      ClassLoader loader = Thread.currentThread().getContextClassLoader();
      return loader.getResourceAsStream(nombre);
	}

	@Override
	public final String getSessionId() {
		final FacesContext fCtx = FacesContext.getCurrentInstance();
		final HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		final String sessionId = session.getId();
		return sessionId;
	}
	
	@Override
	public String buscaDocumento() {   
		log.info("buscando doc..." + memorandum.getId());

		memorandum = em.find(Memorandum.class, memorandum.getId());
		if (memorandum.getFirmas() == null || memorandum.getFirmas().isEmpty()) {
			this.armaDocumento(false, false);
		}

		//firmado = true;
		memorandum.getParrafos().size();
		memorandum.getDistribucionDocumento().size();
		memorandum.getVisaciones().size();
		memorandum.getFirmas().size();
		memorandum.getBitacoras().size();
		memorandum.getRevisarEstructuradas().size();
		memorandum.getVisacionesEstructuradas().size();
		memorandum.getFirmasEstructuradas().size();
		memorandum.getArchivosAdjuntos().size();

		if (memorandum.getCmsId() != null) {
			if (memorandum.getDistribucionDocumento() != null && 
					memorandum.getDistribucionDocumento().size() > 0) {
				List<Persona> lista = new ArrayList<Persona>();
				for (ListaPersonasDocumento p : memorandum.getDistribucionDocumento()) {
					if (p.getDestinatarioPersona() != null) {
						lista.add(p.getDestinatarioPersona());
					}
				}
				if (lista.size() > 0) {
					me.enviarMailNotificacionFirma(expediente, memorandum, lista);
				}
			}
		}

		if (documentoOriginal.getId().equals(memorandum.getId())) {
			documentoOriginal.setNumeroDocumento(memorandum.getNumeroDocumento());
			documentoOriginal.setFechaDocumentoOrigen(memorandum.getFechaDocumentoOrigen());
			documentoOriginal.setEstado(memorandum.getEstado());
			documentoOriginal.setFirmas(memorandum.getFirmas());
		} else {
			for (Documento d : listDocumentosRespuesta) {
				if (d.getId().equals(memorandum.getId())) {
					d.setNumeroDocumento(memorandum.getNumeroDocumento());
					d.setFechaDocumentoOrigen(memorandum.getFechaDocumentoOrigen());
					d.setEstado(memorandum.getEstado());
					d.setFirmas(memorandum.getFirmas());
					d.setEliminable(false);
					break;
				}
			}
		}

		return "";
	}
	
	@Override
	public String buscaDocumento2() {
		this.buscaDocumento();
		return "crearMemorandum";
	}
	
	@Transactional
	private void limpiarDestinatarios(final Long id) {
		try {
			Query query = em.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}
	
	@Transactional
	private void eliminaAntecedente(Long id){
		ArchivoAdjuntoDocumentoElectronico archivo = em.find(ArchivoAdjuntoDocumentoElectronico.class, id);
		em.remove(archivo);
	}
	
	@Override
	public void previsualizar() {
		memorandum.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.MEMORANDUM));
		memorandum.setListaPersonas(new ArrayList<ListaPersonasDocumento>());
		setDistribucionDocumento(memorandum);
		setDestinatarioDocumento(memorandum);
		List<ArchivoAdjuntoDocumentoElectronico> listaArchivos = getListaArchivos();
		memorandum.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		for (ArchivoAdjuntoDocumentoElectronico a : listaArchivos) {
			memorandum.getArchivosAdjuntos().add(a);
		}
	}
}
