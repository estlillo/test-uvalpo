package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;

@Stateful
@Name("verListaCargos")
public class VerListaCargosBean implements VerListaCargos {

	@Logger
	private Log log;

	private int value;

	@Begin
	public String begin() {
		// implement your begin conversation business logic
		log.info("beginning conversation");
		crgList = (ArrayList<Cargo>) buscarCrg();
		return "verListaCargos";
	}

	public String increment() {
		log.info("incrementing");
		value++;
		return "verListaCargos";
	}

	public List<Cargo> buscarCrg() {
		List<Cargo> listCargo = new ArrayList<Cargo>();
		try {
			CargoList a = new CargoList();
			listCargo = (List<Cargo>) a.getResultList();
			return listCargo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listCargo;
	}

	// add additional action methods that participate in this conversation

	private ArrayList<Cargo> crgList = new ArrayList<Cargo>();

	public ArrayList<Cargo> getCrgList() {
		return crgList;
	}

	public void setCrgList(ArrayList<Cargo> crgList) {
		this.crgList = crgList;
	}

	@End
	public String end() {
		// implement your end conversation business logic
		log.info("ending conversation");
		return "home";
	}

	public int getValue() {
		return value;
	}

	@Destroy
	@Remove
	public void destroy() {
	}
}
