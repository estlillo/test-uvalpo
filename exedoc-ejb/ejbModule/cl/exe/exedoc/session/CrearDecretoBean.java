package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.xml.xpath.XPathExpressionException;

import org.hibernate.HibernateException;
import org.hibernate.validator.Range;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.core.Conversation;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoPlantilla;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("crearDecreto")
public class CrearDecretoBean implements CrearDecreto {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	//TODO revisar documentos anexos
	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosAnexo;*/

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Decreto decreto;

	@In(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;

	private Integer idPlantilla;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	private static final long ID_CARGO_FISCAL = 71;

	private Boolean firmado;
	public boolean toAbastecimiento;
	private Boolean visado;
	private boolean guardado;

	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	private Integer idClasificacionSubTipoSelec;

	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	@Range(min = 2, max = 999999999, message = "Debe ingresar una clasificacion")
	private Integer idClasificacionTipoSelec;

	List<SelectItem> tipos1 = null;
	List<SelectItem> tipos2 = null;

	private Integer tipoDecreto = 1;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	private List<ArchivoAdjuntoDocumentoElectronico> archivosAnexos;
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;
	private String materiaArchivo;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	private List<Long> idExpedientes;

	private String tipoArchivo;

	public void agregarAntecedente() {
		if (this.decreto.getArchivosAdjuntos() == null) {
			this.decreto.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		}
		if (!archivosAnexos.isEmpty()) {
			for (ArchivoAdjuntoDocumentoElectronico archivo : archivosAnexos) {
				if (true/*!existeNombreArchivo(archivo.getNombreArchivo())*/) {
					archivo.setDocumentoElectronico(this.decreto);
					archivo.setFecha(new Date());
					archivo.setIdNuevoArchivo(this.decreto.getArchivosAdjuntos().size());
					archivo.setAdjuntadoPor(usuario);
					archivo.setMateria(materiaArchivo);
					this.decreto.getArchivosAdjuntos().add(archivo.archivo());
				}
			}
		}
		limpiarAnexos();
	}

	/*private boolean existeNombreArchivo(String nombreArchivo) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getArchivo() != null && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) {
					return true;
				}
			}
		}
		return false;
	}*/

	public void agregarDocumento() {
		armaDocumento(false, false);
	}

	private void armaDocumento(boolean visar, boolean firmar) {
		/*
		 * El Flujo propuesto por UChile, permite una vez firmado el documento, continuar visando o firmando, 
		 * aunque no se permite la modificacion del expediente.
		 */
		if (this.getFirmado() && !visar && !firmar) {
			return;
		}
		this.decreto.setPlazo(null);
		this.configurarArchivosAdjuntos();

		this.decreto.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		this.decreto.setTipoDocumento(new TipoDocumento(TipoDocumento.DECRETO));

		//TODO MODELO ROLES UCHILE
		/*final boolean modificarVisaFirmas = this.modificarVisaFirmas();
		if (modificarVisaFirmas) {
            if (this.decreto.getVisacionesEstructuradas() != null
                    && !this.decreto.getVisacionesEstructuradas().isEmpty()) {
				this.eliminarVisasEstructuradas();
			}
			if (this.decreto.getFirmasEstructuradas() != null && !this.decreto.getFirmasEstructuradas().isEmpty()) {
			    this.eliminarFirmasEstructuradas();
			}
		}*/

		if (!visar && !firmar) {
			if (this.decreto.getId() != null) {
				this.decreto.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
				this.decreto.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.decreto, this.usuario));
			} else {
				this.decreto.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
				this.decreto.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.decreto, this.usuario));
			}
		}
		if (firmar) {
			this.repositorio.firmar(this.decreto, usuario);
		    // Actualizar estado en servicios externos.
		    System.out.println("Actualiza Estado");
		    this.decreto.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.decreto, this.usuario));
			//Pruebas mias
			List<Expediente> padre = me.obtienePrimerExpediene(expediente);
			List<Persona> dest = me.buscarDestinatarios(padre);
			for (Persona expediente : dest) {
				me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
			}
		}
		if (visar) {
			this.repositorio.visar(this.decreto, usuario);
		    // Actualizar estado en servicios externos.
		    System.out.println("Actualiza Estado");
		    this.decreto.addBitacora(new Bitacora(EstadoDocumento.VISADO, this.decreto, this.usuario));
		}

		this.decreto.setReservado(false);
		this.decreto.setTipo(new TipoRazon(tipoDecreto));

		this.setDistribucionDocumento();

		//TODO MODELO ROLES UCHILE
        /*if (idClasificacionTipoSelec != null && idClasificacionTipoSelec != -1) {
            this.decreto.setClasificacionTipo(new ClasificacionTipo(idClasificacionTipoSelec,
                    getDescripcionTipo(idClasificacionTipoSelec)));
        }
        if (idClasificacionSubTipoSelec != null && idClasificacionSubTipoSelec != -1) {
            this.decreto.setClasificacionSubtipo(new ClasificacionSubtipo(idClasificacionSubTipoSelec,
                    getDescripcionSubtipo(idClasificacionSubTipoSelec)));
        }*/

		this.actualizaListasDocumentos();

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		this.persistirDocumento();

		//TODO MODELO ROLES UCHILE
		/*if (modificarVisaFirmas) {
			actualizaVisaFirma();
		}*/
	}

	private void configurarArchivosAdjuntos() {
		if (listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico a : listaArchivosEliminados) {
				String sqlBorra = "DELETE FROM Archivo a WHERE a.id = " + a.getId();
				em.createQuery(sqlBorra).executeUpdate();
//				em.createNativeQuery(sqlBorra).executeUpdate();
			}
		}
	}

	private void eliminarVisasEstructuradas() {
		if (this.decreto.getId() != null) {
//			String sqlBorra = "DELETE FROM VISACIONES_ESTRUCTURADAS WHERE id_documento = " + decreto.getId();
//			em.createNativeQuery(sqlBorra).executeUpdate();
			String sqlBorra = "DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = " + decreto.getId();
			em.createQuery(sqlBorra).executeUpdate();
		}
	}

	private void eliminarFirmasEstructuradas() {
		if (this.decreto.getId() != null) {
//			String sqlBorra = "DELETE FROM FIRMAS_ESTRUCTURADAS WHERE id_documento = " + decreto.getId();
//			em.createNativeQuery(sqlBorra).executeUpdate();
			String sqlBorra = "DELETE FROM FirmaEstructuradaDocumento fe  WHERE fe.documento.id = " + decreto.getId();
			em.createQuery(sqlBorra).executeUpdate();
		}
	}

	private void setDistribucionDocumento() {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDocumento(this.decreto);
			dd.setDestinatarioPersona(item.getPersona());
			ddList.add(dd);
		}
		this.decreto.setDistribucion(ddList);
	}

	private String getDescripcionTipo(Integer idTipo) {
//		if (idTipo != null && idTipo != -1) {
//			return (String) em.createNativeQuery("select descripcion from clasificacion_tipo where id = :id").setParameter("id", idTipo).getSingleResult();
//		}
		if (idTipo != null && idTipo != -1) {
			return (String) em.createQuery("select ct.descripcion from ClasificacionTipo ct where ct.id = :id").setParameter("id", idTipo).getSingleResult();
		}
		return "";
	}

	private String getDescripcionSubtipo(Integer idSubtipo) {
//		if (idSubtipo != null && idSubtipo != -1) {
//			return (String) em.createNativeQuery("select descripcion from clasificacion_subtipo where id = :id").setParameter("id", idSubtipo).getSingleResult();
//		}
		if (idSubtipo != null && idSubtipo != -1) {
			return (String) em.createQuery("select cs.descripcion from ClasificacionSubtipo cs where cs.id = :id").setParameter("id", idSubtipo).getSingleResult();
		}
		return "";
	}

	private void actualizaListasDocumentos() {
		if (this.decreto.getTipoDocumentoExpediente() != null) {
			if (this.decreto.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				if (!existeDocumento(listDocumentosRespuesta)) {
					this.decreto.setEliminable(true);
					listDocumentosRespuesta.add(this.decreto);
				} else {
					if (this.decreto.getId() != null) {
						listDocumentosRespuesta.remove(this.decreto);
						listDocumentosRespuesta.add(this.decreto);
					} else {
						this.reemplazaDocumento(listDocumentosRespuesta);
					}
				}
			} /*else if (this.decreto.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
				if (!existeDocumento(listDocumentosAnexo)) {
					listDocumentosAnexo.add(this.decreto);
				} else {
					if (this.decreto.getId() != null) {
						listDocumentosAnexo.remove(this.decreto);
						listDocumentosAnexo.add(this.decreto);
					} else {
						this.reemplazaDocumento(listDocumentosAnexo);
					}
				}
			}*/
		}
	}

	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.decreto.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.decreto.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.decreto);
			}
		}
		listaDocumentos = lista;
	}

	private void persistirDocumento() {
	    try {
		if (this.decreto.getId() != null) {
			md.actualizarDocumento(this.decreto);

		} else if (this.expediente != null && this.expediente.getId() != null) {
			me.agregarRespuestaAExpediente(expediente, this.decreto, true);
		}
	    }catch (DocumentNotUploadedException e) {
            e.printStackTrace();
        }
	}

	@SuppressWarnings("unchecked")
	private void actualizaVisaFirma() {
		if (this.decreto.getId() != null) {
//			String sqlVisasActuales = "SELECT * FROM VISACIONES_ESTRUCTURADAS WHERE id_documento = " + decreto.getId() + " order by orden asc";
//			List<VisacionEstructuradaDocumento> visas = em.createNativeQuery(sqlVisasActuales, VisacionEstructuradaDocumento.class).getResultList();
			String sqlVisasActuales = "SELECT ve FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = " + decreto.getId() + " order by ve.orden asc";
			List<VisacionEstructuradaDocumento> visas = em.createQuery(sqlVisasActuales).getResultList();
			this.decreto.getVisacionesEstructuradas().clear();
			this.decreto.getVisacionesEstructuradas().addAll(visas);

//			String sqlFirmasActuales = "SELECT * FROM FIRMAS_ESTRUCTURADAS WHERE id_documento = " + decreto.getId() + " order by orden asc";
//			List<FirmaEstructuradaDocumento> firmas = em.createNativeQuery(sqlFirmasActuales, FirmaEstructuradaDocumento.class).getResultList();
			String sqlFirmasActuales = "SELECT fe FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = " + decreto.getId() + " order by fe.orden asc";
			List<FirmaEstructuradaDocumento> firmas = em.createQuery(sqlFirmasActuales).getResultList();
			this.decreto.getFirmasEstructuradas().clear();
			this.decreto.getFirmasEstructuradas().addAll(firmas);
		}
	}

	/**
	 * Metodo para obtener un documento una vez firmado.
	 * 
	 * @return la vista de redireccion (al despachar expediente).
	 */
	public String buscaDocumento() {
		log.info("buscando doc..." + decreto.getId());
		decreto = em.find(Decreto.class, decreto.getId());
		if (decreto.getFirmas() == null || decreto.getFirmas().isEmpty()) {
			this.armaDocumento(false, false);
		}

		firmado = true;
		decreto.getParrafos().size();
		decreto.getDistribucionDocumento().size();
		decreto.getVisaciones().size();
		decreto.getFirmas().size();
		decreto.getBitacoras().size();
		decreto.getVisacionesEstructuradas().size();
		decreto.getFirmasEstructuradas().size();
		decreto.getArchivosAdjuntos().size();

		for (Documento d : listDocumentosRespuesta) {
			if (d.getId().equals(decreto.getId())) {
				d.setNumeroDocumento(decreto.getNumeroDocumento());
				d.setEstado(decreto.getEstado());
				d.setFirmas(decreto.getFirmas());
				break;
			}
		}
		
		this.decreto.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.decreto, this.usuario));

		return this.despacharExpediente();
	}

	public String cambiar() {
		return "crearDecreto";
	}

	/**
	 * Metodo para despachar un expediente.
	 * 
	 * @return la vista a la que redirigira el despacho.
	 */
	public String despacharExpediente() {

		if (!guardado) {
			this.armaDocumento(false, false);
		}
		if (expediente == null || expediente.getId() == null) {
			this.distribuirDocumentos();
		}
		if (this.guardarExpediente()) {
			idPlantillaSelec = null;
			this.end();
			Conversation.instance().end();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			if (expediente == null || expediente.getId() == null) {
			    this.noDistribuirDocumentos();
			}
			return "";
		}
	}

	private void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

	/**
	 * Metodo para guardar y despachar un expediente.
	 * 
	 * @return true si el expediente fue despachado correctamente. false en caso contrario.
	 */
	@Transactional
	private boolean guardarExpediente() {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo al documento anterior
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado()
					&& !this.documentoOriginal.getId().equals(this.decreto.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(
						new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
                    md.actualizarDocumento(this.documentoOriginal);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			}
		}

		/*for (Documento doc : listDocumentosAnexo) {
			if (doc.getId() == null) {
				if (doc instanceof DocumentoBinario) {
					DocumentoBinario docBin = (DocumentoBinario) doc;
					Long idDocumentoReferencia = buscaDocumentoReferencia(docBin.getIdDocumentoReferencia());
					if (idDocumentoReferencia != null) {
						me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia);
					} else {
						me.agregarAnexoAExpediente(expediente, doc, true);
					}
				} else {
					me.agregarAnexoAExpediente(expediente, doc, true);
				}
			} else {
				md.actualizarDocumento(doc);
			}
		}*/

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
			try {
    		    if (doc.getId() == null) {
    				me.agregarRespuestaAExpediente(expediente, doc, true);
    			} else {
    				// Cerrar plazo al documento anterior
    				if (doc.getCompletado() != null && !doc.getCompletado() && !doc.getId().equals(this.decreto.getId())) {
    					doc.setCompletado(true);
    					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
    					md.actualizarDocumento(doc);
    				}
    			}
			} catch (DocumentNotUploadedException e) {
			    e.printStackTrace();
			}
		}

		this.guardarObservaciones();
		idExpedientes = new ArrayList<Long>();
		final Date fechaIngreso = new Date();

		Persona de = null;
		
		//TODO MODELO ROLES UCHILE
		/*if (this.decreto.getVisacionesEstructuradas() != null 
				&& this.decreto.getVisacionesEstructuradas().size() != 0) {
			Collections.sort(this.decreto.getVisacionesEstructuradas(),
					new Comparator<VisacionEstructuradaDocumento>() {
						public int compare(VisacionEstructuradaDocumento o1, VisacionEstructuradaDocumento o2) {
							return o1.getOrden().compareTo(o2.getOrden());
						}
					});
			de = this.decreto.getVisacionesEstructuradas().get(0).getPersona();
			if (de.equals(usuario)) {
				FacesMessages.instance().add("Debe visar el documento antes de Despacharlo");
				return false;
			}
		} else if (this.decreto.getFirmasEstructuradas() != null && this.decreto.getFirmasEstructuradas().size() != 0) {
			Collections.sort(this.decreto.getFirmasEstructuradas(), new Comparator<FirmaEstructuradaDocumento>() {
				public int compare(FirmaEstructuradaDocumento o1, FirmaEstructuradaDocumento o2) {
					return o1.getOrden().compareTo(o2.getOrden());
				}
			});
			de = this.decreto.getFirmasEstructuradas().get(0).getPersona();
			if (de.equals(usuario)) {
				FacesMessages.instance().add("Debe firmar el documento antes de Despacharlo");
				return false;
			}
		}*/
		
		if (de != null) {
			log.info("Despachando a : #0", de.getUsuario());
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			// TODO: No borrar. Puede ser usado para posible personalizacion.
			/*if (this.decreto.getFirmas() != null && this.decreto.getFirmas().size() != 0) {
				this.agregarDistribucionParaDespacho(fechaIngreso);*/
				/*if (this.documentoOriginal instanceof Adquisicion) {
					Query query = em.createNamedQuery("FlujoEstructurado.findByIdFlujo");
					query.setParameter("idFlujo", FlujoEstructurado.DESPACHO_RESOLUCION);
					List<FlujoEstructurado> flujos = query.getResultList();
					if (flujos != null) {
						for (FlujoEstructurado flujo : flujos) {
							de = flujo.getResponsable();
							if (de.equals(usuario)) {
								FacesMessages.instance().add(
										"Debe seleccionar un destinatario al expediente antes de Despacharlo");
								return false;
							}
							log.info("Despachando a : #0", de.getUsuario());
							idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
						}
					}
				}*/
			//}
			// Si no hay mas acciones en el flujo, se envia a la distribucion.
			if (!this.agregarDistribucionParaDespacho(fechaIngreso)) {
				// Si no hay distribucion, despachar a Oficina de Partes.
				log.info("A Oficina de Partes.");
				try {
					this.agregarOfPartesParaDespacho(fechaIngreso);
				} catch (HibernateException e) {
					log.error("Error al intentar despachar a Oficina de Partes: #0", e.getMessage());
					e.printStackTrace();
					// No debe despachar.
					idExpedientes.clear();
				}
			}
		}

		final List<Expediente> expedientes = new ArrayList<Expediente>();
		if (!idExpedientes.isEmpty()) {
			for (Long id : idExpedientes) {
				final Expediente e = me.buscarExpediente(id);
				me.despacharExpediente(e);
				expedientes.add(e);
			}
			me.despacharExpediente(expediente);
			//me.despacharNotificacionPorEmail(expedientes);

			FacesMessages.instance().add("Expediente Despachado");
			return true;
		} else {
			FacesMessages.instance().add("No hay destinatarios a quien despachar");
			return false;
		}
	}

//	private Long buscaDocumentoReferencia(Integer idDocumento) {
//		Long id = null;
//		for (Documento doc : listDocumentosRespuesta) {
//			if (doc.getIdNuevoDocumento().equals(idDocumento)) {
//				id = doc.getId();
//				break;
//			}
//		}
//		if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
//			id = documentoOriginal.getId();
//		}
//		return id;
//	}

	private void noDistribuirDocumentos() {
		List<Documento> list = new ArrayList<Documento>();
		list.add(documentoOriginal);
		list.addAll(listDocumentosRespuesta);
		listDocumentosRespuesta.clear();
		listDocumentosRespuesta.addAll(list);
	}

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	/**
	 * Metodo que permite agregar a las personas presentes en la distribucion del documento como destinatarios del
	 * expediente. Esto sera en caso que no existan mas acciones que hacer para este documento dentro del flujo
	 * estructurado.
	 * 
	 * @param fechaIngreso la fecha de ingreso que tendra el expediente a despachar. Necesaria para crear el expediente
	 *        a despachar.
	 * @return true si el agregado de la distribucion al expediente fue satisfactoria. false en caso contrario (si el
	 *         documento no presentaba distribucion).
	 */
	private boolean agregarDistribucionParaDespacho(final Date fechaIngreso) {
		if (distribucionDocumento == null || distribucionDocumento.isEmpty()) {
			return false;
		}
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		for (SelectPersonas sp : distribucionDocumento) {
			final Persona p = em.find(Persona.class, sp.getPersona().getId());
			// TODO MODELO ROLES UCHILE
			/*
			if (p.getRoles().contains(new Rol(Rol.JEFE)) && p.getRoles().contains(new Rol(Rol.ABASTECIMIENTO))) {
				p.setDestinatarioConCopia(false);
			} else {
				p.setDestinatarioConCopia(true);
			}*/
			listDestinatarios.add(p);
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, p, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}
		log.info("A distribucion.");
		return true;
	}
	
	/**
	 * Metodo que setea al Jefe de la Oficina de Partes como destinatario del expediente. Esto ocurre en caso que no
	 * existan mas acciones en el flujo estructurado y el documento no tenga distribucion.
	 * 
	 * @param fechaIngreso fecha necesaria para el despacho del expediente.
	 */
	private void agregarOfPartesParaDespacho(final Date fechaIngreso) {
		// Obtener Jefe de Oficina de Partes.
		final Persona jefeOP = this.getJefeOfPartes();
		
		// agregarlo como destinatario de expediente.
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		listDestinatarios.add(jefeOP);
		try {
            idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, jefeOP, fechaIngreso));
        } catch (DocumentNotUploadedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	/**
	 * Metodo para obtener al jefe de la Oficina de Partes.
	 * 
	 * @return el jefe de la Oficina de Partes.
	 */
	private Persona getJefeOfPartes() {
		// TODO: Debe haber una mejor forma de hacer esto.
		// Obtener unidad Oficina de Partes.
		final Query ofPartesQuery = em.createNamedQuery("UnidadOrganizacional.findByDescripcion");
		ofPartesQuery.setParameter("descripcionMayus", "%AREA OFICINA DE PARTES%");
		final UnidadOrganizacional ofPartes = (UnidadOrganizacional) ofPartesQuery.getSingleResult();
		
		// Obtener cargo Jefe de Oficina de Partes.
		final StringBuilder cargoJefeOPHQL = new StringBuilder("select c from Cargo c ");
		cargoJefeOPHQL.append("where c.unidadOrganizacional.id = ").append(ofPartes.getId());
		cargoJefeOPHQL.append(" and upper(c.descripcion) like '%JEFE%'");
		final Query cargoJefeOPQuery = em.createQuery(cargoJefeOPHQL.toString());
		final Cargo cargoJefeOP = (Cargo) cargoJefeOPQuery.getSingleResult();
		
		// Obtener persona Jefe de Oficina de Partes.
		final Query jefeOPQuery = em.createNamedQuery("Persona.findByIdCargo");
		jefeOPQuery.setParameter("idCargo", cargoJefeOP.getId());
		jefeOPQuery.setMaxResults(1);
		final Persona jefeOP = (Persona) jefeOPQuery.getSingleResult();
		
		return jefeOP;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String desvisarYDesfirmar(Decreto dec) {
		if (dec.getFirmas() == null || dec.getFirmas().isEmpty()) {
			if (dec.getVisaciones() != null && !dec.getVisaciones().isEmpty()) {
				for (VisacionDocumento visa : dec.getVisaciones()) {
					VisacionDocumento visaElim = em.find(VisacionDocumento.class, visa.getId());
					em.remove(visaElim);
				}
				dec.getVisaciones().clear();
				dec.setVisaciones(null);
			}

			if (dec.getVisacionesEstructuradas() != null && !dec.getVisacionesEstructuradas().isEmpty()) {
				for (VisacionEstructuradaDocumento visaEstructurada : dec.getVisacionesEstructuradas()) {
					VisacionEstructuradaDocumento visaEstructElim = em.find(VisacionEstructuradaDocumento.class, visaEstructurada.getId());
					em.remove(visaEstructElim);
				}
				dec.getVisacionesEstructuradas().clear();
				dec.setVisacionesEstructuradas(null);
			}

			if (dec.getFirmasEstructuradas() != null && !dec.getFirmasEstructuradas().isEmpty()) {
				for (FirmaEstructuradaDocumento fed : dec.getFirmasEstructuradas()) {
					FirmaEstructuradaDocumento fedElim = em.find(FirmaEstructuradaDocumento.class, fed.getId());
					em.remove(fedElim);
				}
				dec.getFirmasEstructuradas().clear();
				dec.setFirmasEstructuradas(null);
			}

			dec.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
			dec.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, dec, usuario));
			dec.setCmsId(null);
			expediente.setFechaDespacho(null);
			persistirDocumento();

			FacesMessages.instance().add("Decreto desvisado y desfirmado");
		} else {
			FacesMessages.instance().add("El decreto no se puede desvisar, pues ya esta firmado");
		}
		return "crearDecreto";
	}

	public void eliminarArchivoAntecedente(Integer idNuevoArchivo) {
		if (this.listaArchivosEliminados == null) {
			this.listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		}

		for (ArchivoAdjuntoDocumentoElectronico a : this.decreto.getArchivosAdjuntos()) {
			if (a.getIdNuevoArchivo().equals(idNuevoArchivo)) {
				if (a.getId() != null) {
					this.listaArchivosEliminados.add(a);
				}
				this.decreto.getArchivosAdjuntos().remove(a);
				break;
			}
		}

		if (decreto != null) {
			if (decreto.getId() != null) {
				em.merge(decreto);
				em.flush();
			}
		}
	}

	public String end() {
		distribucionDocumento = new ArrayList<SelectPersonas>();
		listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		this.limpiarAnexos();
		guardado = false;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}
		vistoDesdeReporte = null;
		//flujo = null;
		return homeCrear;
	}

	public void firmar() {
		for (FirmaEstructuradaDocumento fed : decreto.getFirmasEstructuradas()) {
			if (fed.getPersona().equals(usuario)) {
				this.armaDocumento(false, true);
				Query query = em.createQuery("DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.id = ?");
				query.setParameter(1, fed.getId());
				decreto.getFirmasEstructuradas().remove(0);
				query.executeUpdate();
				decreto.getFirmasEstructuradas().remove(fed);

				salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
					if (de.getDocumento().getId().equals(decreto.getId())) {
						for (FirmaEstructuradaDocumento fe : de.getDocumento().getFirmasEstructuradas()) {
							if (fe.getPersona().equals(usuario)) {
								de.getDocumento().getFirmasEstructuradas().remove(fe);
								break salida;
							}
						}
					}
				}
				FacesMessages.instance().add("Resolución firmada.");
				break;
			}
		}
		guardado = true;
	}

	public String getConsiderando() {
		String considerando = "";
		if (decreto.getConsiderandos() == null || decreto.getConsiderandos().isEmpty()) {
			Considerandos parrafo = new Considerandos();
			parrafo.setCuerpo(considerando);
			parrafo.setDocumento(this.decreto);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.decreto.setConsiderandos(parrafos);
		}
		List<Parrafo> cons = this.decreto.getConsiderandos();
		Parrafo parr = cons.get(0);
		considerando = parr.getCuerpo();
		return considerando;
	}

	public Decreto getDecreto() {
		if (decreto == null) {
			decreto = new Decreto();
			Calendar ahora = new GregorianCalendar();
			decreto.setFechaCreacion(ahora.getTime());
			ahora.add(Calendar.DATE, 5);
			decreto.setPlazo(ahora.getTime());

			decreto.setAutor(usuario);
			// tipo temporal, Siempre se toma como original el primero.
			decreto.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
			// id temporal para borrar un documento
			decreto.setIdNuevoDocumento(Integer.MAX_VALUE);
			decreto.setReservado(false);
			decreto.setEnEdicion(true);
		}
		return decreto;
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}

	public Boolean getFirmado() {
		if (this.decreto != null && this.decreto.getFirmas() != null && this.decreto.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public List<FirmaEstructuradaDocumento> getFirmas() {
		if (decreto.getFirmasEstructuradas() == null) {
			decreto.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		return decreto.getFirmasEstructuradas();
	}

	/*public Integer getIdClasificacionSubTipoSelec() {
		if (decreto != null && decreto.getClasificacionSubtipo() != null) {
			idClasificacionSubTipoSelec = decreto.getClasificacionSubtipo().getId();
		}
		return idClasificacionSubTipoSelec;
	}*/

	/*public Integer getIdClasificacionTipoSelec() {
		if (decreto != null && decreto.getClasificacionTipo() != null) {
			idClasificacionTipoSelec = decreto.getClasificacionTipo().getId();
		}
		return idClasificacionTipoSelec;
	}*/

	public Integer getIdPlantilla() {
		return idPlantilla;
	}

	public Integer getIdPlantillaSelec() {
		return idPlantillaSelec;
	}

	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos() {
		if (this.decreto.getArchivosAdjuntos() != null) {
			int cont = 0;

			for (ArchivoAdjuntoDocumentoElectronico a : this.decreto.getArchivosAdjuntos()) {
				a.setIdNuevoArchivo(cont++);
			}
			return this.decreto.getArchivosAdjuntos();
		}
		return new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
	}

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public int getMaxFilesQuantity() {
		int valor = 20;
		/*
		 * if (archivosAnexos == null || archivosAnexos.isEmpty()) { valor = 1;
		 * } else { valor = archivosAnexos.size(); }
		 */
		return valor;
	}

	public String getResuelvo() {
		String resuelvo = "";
		if (decreto.getResuelvo() == null || decreto.getResuelvo().isEmpty()) {
			Resuelvo parrafo = new Resuelvo();
			parrafo.setCuerpo(resuelvo);
			parrafo.setDocumento(this.decreto);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.decreto.setResuelvo(parrafos);
		}
		List<Parrafo> res = this.decreto.getResuelvo();
		Parrafo parr = res.get(0);
		resuelvo = parr.getCuerpo();
		return resuelvo;
	}

	public Integer getTipoDecreto() {
		if (decreto != null && decreto.getTipo() != null) {
			tipoDecreto = decreto.getTipo().getId();
		}
		return tipoDecreto;
	}

	/*
	public List<SelectItem> getTipos1() {
		if (tipos1 == null) {
			tipos1 = new ArrayList<SelectItem>();
		}
		if (tipos1.isEmpty()) {
			tipos1.addAll(obtenerClasificacionTipo());
		}
		return tipos1;
	}
	public List<SelectItem> getTipos2() {
		if (tipos2 == null) {
			tipos2 = new ArrayList<SelectItem>();
		}
		if (tipos2.isEmpty()) {
			tipos2.addAll(obtenerClasificacionSubTipo());
		}
		return tipos2;
	}
	*/

	public String getUrlFirma() {
		String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}

	public String getVisaciones() {
		if (this.decreto.getVisacionesEstructuradas() == null) {
			this.decreto.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
		}
		List<VisacionEstructuradaDocumento> visaciones = decreto.getVisacionesEstructuradas();

		StringBuilder sb = new StringBuilder();
		for (int i = visaciones.size() - 1; i >= 0; i--) {
			sb.append(visaciones.get(i).getPersona().getIniciales() + "/");
		}
//		sb.append(decreto.getAutor().getIniciales().toLowerCase());

		return sb.toString();
	}

	public List<VisacionDocumento> getVisacionesV() {
		List<VisacionDocumento> visaciones = null;
		if (this.decreto != null && this.decreto.getVisaciones() != null) {
			visaciones = decreto.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public Boolean getVisado() {
		if (this.decreto != null && this.decreto.getVisaciones() != null && this.decreto.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public String getVistos() {
		String vistos = "";
		if (decreto.getVistos() == null || decreto.getVistos().isEmpty()) {
			Vistos parrafo = new Vistos();
			parrafo.setCuerpo(vistos);
			parrafo.setDocumento(this.decreto);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.decreto.setVistos(parrafos);
		}
		vistos = this.decreto.getVistos().get(0).getCuerpo();
		return vistos;
	}

	public void guardarYCrearExpediente() {
		armaDocumento(false, false);
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);

		List<VisacionEstructuradaDocumento> visaciones = decreto.getVisacionesEstructuradas();
		List<FirmaEstructuradaDocumento> firmas = decreto.getFirmasEstructuradas();

		if (visaciones != null && visaciones.size() > 0) {
			listDestinatarios.add(visaciones.get(0).getPersona());
		} else if (firmas != null && firmas.size() > 0) {
			listDestinatarios.add(firmas.get(0).getPersona());
		}

		idClasificacionTipoSelec = null;
		idClasificacionSubTipoSelec = null;
	}

	public boolean isDespachable() {
		if (modificarVisaFirmas() || usuarioTieneVisas() || usuarioTieneFirmas()) {
			return true;
		}
		return false;
	}

	private boolean usuarioTieneVisas() {
		if (decreto.getVisaciones() != null && decreto.getVisaciones().size() > 0) {
			List<VisacionDocumento> listVisaciones = decreto.getVisaciones();
			for (VisacionDocumento v : listVisaciones) {
				if (v.getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean usuarioTieneFirmas() {
		if (decreto.getFirmas() != null && decreto.getFirmas().size() > 0) {
			List<FirmaDocumento> listFirmas = decreto.getFirmas();
			for (FirmaDocumento f : listFirmas) {
				if (f.getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isDisabledGuardar() {
		return guardado;
	}

	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getVisado()) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isEditableSinVisa() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
//		if (this.getFirmado()) {
//			return false;
//		}
		return true;
	}

	public boolean isRenderedBotonDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (decreto != null && decreto.getId() != null) {
			if ((decreto.getFirmasEstructuradas() != null && decreto.getFirmasEstructuradas().size() > 0)
					|| (decreto.getVisacionesEstructuradas() != null && decreto.getVisacionesEstructuradas().size() > 0) || toAbastecimiento) {
				for (VisacionEstructuradaDocumento ve : decreto.getVisacionesEstructuradas()) {
					if (ve.getPersona().equals(usuario)) {
						return false;
					}
				}
				for (FirmaEstructuradaDocumento fe : decreto.getFirmasEstructuradas()) {
					if (fe.getPersona().equals(usuario)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisar() {
		if (this.decreto.getId() == null) {
			return false;
		}
		return !isRenderedBotonFirmar();
	}

	private boolean isRenderedBotonFirmar() {
		if (!isEditableSinVisa()) {
			return false;
		}
		if (usuario.getId().equals(decreto.getIdEmisor())) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

    /**
     * Metodo para saber si se debe mostrar el boton de firmar (accion asignada de firma y documento no visto desde
     * reporte).
     * 
     * @return true si se puede mostrar el boton de firmar. Si no se puede, false.
     */
	public boolean isRenderedFirmar() {
		if (vistoDesdeReporte != null) {
			if (!vistoDesdeReporte) {
				return true;
			}
		}
		
		// TODO: No borrar esto, pues se puede usar para una posterior personalizacion.
        /*if (decreto != null && decreto.getFirmasEstructuradas() != null 
                && decreto.getFirmasEstructuradas().size() != 0) {
            List<FirmaEstructuradaDocumento> firmas = decreto.getFirmasEstructuradas();
            Collections.sort(firmas);
            if (decreto.getVisacionesEstructuradas() != null && decreto.getVisacionesEstructuradas().size() == 0) {
                if (firmas.get(0).getPersona().equals(usuario)) { return true; }
            }
        }*/
		return false;
	}

	public boolean isRenderedFirmarExento() {
		return isRenderedFirmar() && this.tipoDecreto.equals(TipoRazon.EXENTO);
	}

	public boolean isRenderedFirmarContraloria() {
		return isRenderedFirmar() && !this.tipoDecreto.equals(TipoRazon.EXENTO);
	}

	public boolean isRenderedPlantillaList() {
		if (this.decreto != null) {
			if (this.decreto.getId() != null) {
				return true;
			}
		}
		return false;
	}

    /**
     * Metodo para saber si se debe mostrar el boton de visar (accion asignada de visa y documento no visto desde
     * reporte).
     * 
     * @return true si se puede mostrar el boton de visar. Si no se puede, false.
     */
	public boolean isRenderedVisar() {
		if (vistoDesdeReporte != null) {
			if (!vistoDesdeReporte) {
				return true;
			}
		}
		
		// TODO: No borrar esto, pues se puede usar para una posterior personalizacion.
        /*if (this.decreto != null && this.decreto.getId() != null) {
            if (decreto != null && decreto.getVisacionesEstructuradas() != null
                    && decreto.getVisacionesEstructuradas().size() != 0) {
                List<VisacionEstructuradaDocumento> visas = decreto.getVisacionesEstructuradas();
                Collections.sort(visas);
                if (visas.get(0).getPersona().equals(usuario)) { return true; }
            }
        }*/
		return false;
	}
	
	/* (non-Javadoc)
	 * @see cl.exe.exedoc.session.CrearDecreto#isRenderedRechazar()
	 */
	public boolean isRenderedRechazar() {
		if (vistoDesdeReporte != null) {
			if (!vistoDesdeReporte) {
				return true;
			}
		}
		
		return false;
	}

	public void limpiarAnexos() {
		if (archivosAnexos == null) {
			archivosAnexos = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		} else {
			archivosAnexos.clear();
		}
		materiaArchivo = null;
	}

	public void listener(UploadEvent event) throws Exception {
		ArchivoAdjuntoDocumentoElectronico archivo = new ArchivoAdjuntoDocumentoElectronico();
		UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
		archivosAnexos.add(archivo);
	}

	public boolean modificarVisaFirmas() {

		for (Rol r : usuario.getRoles()) {
			if (r.equals(Rol.INGRESAR_VISA_FIRMA)) {
				return true;
			}
		}
		return false;
	}

	/*@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerClasificacionSubTipo() {
		List<ClasificacionSubtipo> list;
		List<SelectItem> item = new ArrayList<SelectItem>();
		if (idClasificacionSubTipoSelec == null || idClasificacionSubTipoSelec == 0) {
			list = em.createNamedQuery("ClasificacionSubtipo.findByAll").getResultList();
			/* item.add(new SelectItem(-1, "<<Seleccionar SubTipo>>")); */
			/*for (ClasificacionSubtipo pr : list) {
				item.add(new SelectItem(pr.getId(), pr.getDescripcion()));
			}
		} else {
			Query query = em.createNamedQuery("ClasificacionSubtipo.findById");
			query.setParameter("id", idClasificacionSubTipoSelec);
			list = query.getResultList();
			if (list != null && list.size() > 0) {
				item.add(new SelectItem(list.get(0).getId(), list.get(0).getDescripcion()));
			}
		}
		return item;
	}*/

//	/*
//	 @SuppressWarnings("unchecked")
//	public List<SelectItem> obtenerClasificacionTipo() {
//		List<ClasificacionTipo> list;
//		List<SelectItem> item = new ArrayList<SelectItem>();
//		if (idClasificacionTipoSelec == null || idClasificacionTipoSelec == 0) {
//			list = em.createNamedQuery("ClasificacionTipo.findByAll").getResultList();
//			/* item.add(new SelectItem(-1, "<<Seleccionar Tipo>>")); */
//			for (ClasificacionTipo pr : list) {
//				item.add(new SelectItem(pr.getId(), pr.getDescripcion()));
//			}
//		} else {
//			Query query = em.createNamedQuery("ClasificacionTipo.findById");
//			query.setParameter("id", idClasificacionTipoSelec);
//			list = query.getResultList();
//			if (list != null && list.size() > 0) {
//				item.add(new SelectItem(list.get(0).getId(), list.get(0).getDescripcion()));
//			}
//		}
//		return item;
//	}
//	 */
	

	@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerPlantillas() {
		List<Plantilla> plantillas;
		List<SelectItem> plantillasItem = new ArrayList<SelectItem>();
		if (idPlantillaSelec == null || idPlantillaSelec == 0) {
			plantillas = em.createNamedQuery("Plantilla.findByTipoPlantilla").setParameter("tp", TipoPlantilla.RESOLUCION_DECRETO).getResultList();
			plantillasItem.add(new SelectItem(-1, "<<Seleccionar Plantilla>>"));
			for (Plantilla pr : plantillas) {
				plantillasItem.add(new SelectItem(pr.getId(), pr.getNombre()));
			}
		} else {
			Query query = em.createNamedQuery("Plantilla.findById");
			query.setParameter("id", idPlantillaSelec);
			plantillas = query.getResultList();
			if (plantillas != null && plantillas.size() > 0) {
				plantillasItem.add(new SelectItem(plantillas.get(0).getId(), plantillas.get(0).getNombre()));
			}
			setearPlantilla(idPlantillaSelec);
		}
		if (this.decreto.getId() == null && idPlantillaSelec != null) {
			agregarDocumento();
		}
		return plantillasItem;
	}

	private void setearPlantilla(Integer idPlantilla) {
		if (!idPlantilla.equals(-1)) {
			aplicaPlantilla(idPlantilla);
		}
	}

	private void aplicaPlantilla(Integer idPlantilla) {
		Plantilla plantilla;
		try {
			plantilla = (Plantilla) em.createNamedQuery("Plantilla.findById").setParameter("id", idPlantilla).getSingleResult();
		} catch (NoResultException nre) {
			plantilla = null;
		}
		if (this.decreto != null && this.decreto.getParrafos() != null) {
			this.decreto.getParrafos().clear();
		}
		if (plantilla != null) {
			for (CamposPlantilla cpr : plantilla.getCamposPlantilla()) {
				if (cpr.getTipoContenido().getId().equals(TipoContenido.TIPO)) {
					if (cpr.getContenido().equals("EXENTO")) {
						tipoDecreto = 1;
					} else if (cpr.getContenido().equals("TOMA DE RAZON")) {
						tipoDecreto = 2;
					} else {
						tipoDecreto = 3;
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					String materia = cpr.getContenido();
					this.decreto.setMateria(materia);
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.VISTOS)) {
					Vistos parrafo = new Vistos();
					parrafo.setCuerpo(cpr.getContenido());
					parrafo.setDocumento(this.decreto);
					parrafo.setNumero(1L);
					List<Parrafo> parrafos = new ArrayList<Parrafo>();
					parrafos.add(parrafo);
					this.decreto.setVistos(parrafos);
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					Considerandos parrafo = new Considerandos();
					parrafo.setCuerpo(cpr.getContenido());
					parrafo.setDocumento(this.decreto);
					parrafo.setNumero(1L);
					List<Parrafo> parrafos = new ArrayList<Parrafo>();
					parrafos.add(parrafo);
					this.decreto.setConsiderandos(parrafos);
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.RESUELVO)) {
					String resuelvo = "";
					resuelvo = cpr.getContenido();
					Resuelvo parrafo = new Resuelvo();
					parrafo.setCuerpo(resuelvo);
					parrafo.setDocumento(this.decreto);
					parrafo.setNumero(1L);
					List<Parrafo> parrafos = new ArrayList<Parrafo>();
					parrafos.add(parrafo);
					this.decreto.setResuelvo(parrafos);
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.INDICACION)) {
					this.decreto.setIndicaciones(cpr.getContenido());
				}
			}
		}
	}

	public void plantillaListener(ValueChangeEvent event) {
		Integer id = (Integer) event.getNewValue();
		if (!id.equals(-1)) {
			aplicaPlantilla(id);
		}
	}

	public boolean renderedBotonDesvisarDesfirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		return usuario.getId().equals(ID_CARGO_FISCAL) && (decreto.getFirmas() == null || decreto.getFirmas().isEmpty()) && (decreto.getVisaciones() != null && !decreto.getVisaciones().isEmpty());
	}

	public void tipoRazonListener(ValueChangeEvent event) {
		PhaseId phaseId = event.getPhaseId();
		tipoDecreto = (Integer) event.getNewValue();
		if (phaseId.equals(PhaseId.ANY_PHASE)) {
			event.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
			event.queue();
		} else if (phaseId.equals(PhaseId.UPDATE_MODEL_VALUES)) {
			if (tipoDecreto != null) {
				if (tipoDecreto.equals(TipoRazon.EXENTO)) {
					tipoArchivo = "*";
				} else {
					List<ArchivoAdjuntoDocumentoElectronico> adjuntos = this.decreto.getArchivosAdjuntos();
					if (adjuntos != null) {
						for (ArchivoAdjuntoDocumentoElectronico ar : adjuntos) {
							if (ar != null) {
								if (!ar.getContentType().equals("application/pdf")) {
									FacesMessages.instance().add("No se puede cambiar el tipo de resolucion, pues ya existe un archivo que no es PDF");
									this.setTipoDecreto(TipoRazon.EXENTO);
									return;
								}
							}
						}
					}
					tipoArchivo = "pdf";
				}
				if (decreto != null) {
					if (decreto.getId() != null) {
						decreto.setTipo(new TipoRazon(tipoDecreto));
						em.merge(decreto);
					}
				}
			}
		}
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public void salirPanel() {
		log.info("SALIR PANEL");
		limpiarAnexos();
	}

	public void setConsiderando(String considerando) {
		List<Parrafo> cons = this.decreto.getConsiderandos();
		cons.get(0).setCuerpo(considerando);
	}

	public void setDecreto(Decreto decreto) {
		this.decreto = decreto;
	}

	public void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento) {
		this.distribucionDocumento = distribucionDocumento;
	}

	public void setIdClasificacionSubTipoSelec(Integer idClasificacionSubTipoSelec) {
		this.idClasificacionSubTipoSelec = idClasificacionSubTipoSelec;
	}

	public void setIdClasificacionTipoSelec(Integer idClasificacionTipoSelec) {
		this.idClasificacionTipoSelec = idClasificacionTipoSelec;
	}

	public void setIdPlantilla(Integer idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public void setIdPlantillaSelec(Integer idPlantillaSelec) {
		this.idPlantillaSelec = idPlantillaSelec;
	}

	public void setMateriaArchivo(String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	public void setResuelvo(String resuelvo) {
		List<Parrafo> res = this.decreto.getResuelvo();
		res.get(0).setCuerpo(resuelvo);
	}

	public void setTipoDecreto(Integer tipoDecreto) {
		this.tipoDecreto = tipoDecreto;
	}

	public void setTipos1(List<SelectItem> tipos1) {
		this.tipos1 = tipos1;
	}

	public void setTipos2(List<SelectItem> tipos2) {
		this.tipos2 = tipos2;
	}

	public void setVistos(String vistos) {
		List<Parrafo> vis = this.decreto.getVistos();
		vis.get(0).setCuerpo(vistos);
	}

	public void verArchivo(Long id) {
		/*Archivo archivo = em.find(Archivo.class, id);
		if (archivo != null && archivo.getCmsId() != null) {
			byte[] data = repositorio.recuperarArchivo(archivo.getCmsId());
			if (data != null) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				try {
					HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
					response.setContentType(archivo.getContentType());
					response.setHeader("Content-disposition", "attachment; filename=\"" + archivo.getNombreArchivo() + "\"");
					response.getOutputStream().write(data);
					response.getOutputStream().flush();
					facesContext.responseComplete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + archivo.getId() + ")");
			}
		}*/
	    try {
            if (!DocUtils.getFile(id, em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + id + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    public String visar() {
	 // TODO: No borrar lo comentado, pues se puede usar para una posterior personalizacion.
		/*boolean permisoVisa = false;
		for (VisacionEstructuradaDocumento ved : decreto.getVisacionesEstructuradas()) {
			if (ved.getPersona().equals(usuario)) {
				permisoVisa = true;
			}
		}
		if (permisoVisa) {*/
			this.armaDocumento(true, false);
			/*VisacionEstructuradaDocumento ved = null;
			for (VisacionEstructuradaDocumento ve : decreto.getVisacionesEstructuradas()) {
				if (ve.getPersona().equals(usuario)) {
					ved = ve;
					break;
				}
			}

			Query query = em.createQuery("DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.id = ?");
			query.setParameter(1, ved.getId());
			query.executeUpdate();
			decreto.getVisacionesEstructuradas().remove(ved);

			salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
				if (de.getDocumento().getId().equals(decreto.getId())) {
					for (VisacionEstructuradaDocumento ve : de.getDocumento().getVisacionesEstructuradas()) {
						if (ve.getPersona().equals(usuario)) {
							de.getDocumento().getVisacionesEstructuradas().remove(ve);
							break salida;
						}
					}
				}
			}
			FacesMessages.instance().add("Decreto visado.");
		} else {
			FacesMessages.instance().add("Ud. No puede Visar el Decreto");
		}*/

		guardado = true;
		
		return this.despacharExpediente();
	}

	public void visualizar() {
		if (!firmado && !visado) {
			armaDocumento(false, false);
		}
	}

	/* (non-Javadoc)
	 * @see cl.exe.exedoc.session.CrearDecreto#rechazar()
	 */
	public String rechazar() {
		// Redirigir.
		return "";
	}

	public final String getSessionId() {
		final FacesContext fCtx = FacesContext.getCurrentInstance();
		final HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		final String sessionId = session.getId();
		return sessionId;
	}
}
