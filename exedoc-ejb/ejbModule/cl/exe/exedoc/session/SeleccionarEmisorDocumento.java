package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface SeleccionarEmisorDocumento {  
	
	public void destroy();
	
	public Long getDivision();
	public void setDivision(Long division);
	public Long getDepartamento();
	public void setDepartamento(Long departamento);
	public Long getUnidadOrganizacional();
	public void setUnidadOrganizacional(Long unidadOrganizacional);
	public Long getCargo();
	public void setCargo(Long cargo);
	
	public List<SelectItem> getListDivision();
	public List<SelectItem> getListDepartamento();
	public List<SelectItem> getListUnidadesOrganizacionales();
	public List<SelectItem> getListCargos();
	public List<SelectItem> getListPersonas();
	
	public void buscarDepartamentos();
	public void buscarUnidadesOrganizacionales();
	public void buscarCargos();
	public void buscarPersonas();
	
	public void agregarDestinatario();		

	public List<SelectPersonas> getDestinatariosDocumento();
	
	public Long getPersona();
	public void setPersona(Long persona);
	
	public void eliminarDestinatarioDocumento(String destinatario);
	
	public void buscarDivisiones();
	public Long getOrganizacion();
	public void setOrganizacion(Long organizacion);
	public List<SelectItem> getListOrganizacion();
	public void setListOrganizacion(List<SelectItem> listOrganizacion);
	
	public List<SelectItem> getListaJefes();
	public void agregarDestinatarioProvidencia();

	void limpiarDestinatario();
}
