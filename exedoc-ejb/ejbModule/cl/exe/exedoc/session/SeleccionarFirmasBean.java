package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DependenciaExterna;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.PersonaExterna;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("seleccionarFirmas")
public class SeleccionarFirmasBean implements SeleccionarFirmas {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Documento documento;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<FirmaEstructuradaDocumento> listFirmaEstructuradaEliminadas;
	@In(required = false, scope = ScopeType.CONVERSATION)
	private Long idDepartamento;
	
	@In(required = true)
	private Persona usuario;
	
	private static final String SELECCIONAR_INICIO = "<<Seleccionar>>";

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Externos
	private Long dependencia = JerarquiasLocal.INICIO;
	private Long personaExterna = JerarquiasLocal.INICIO;
	private List<SelectItem> listDependencia = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonasExternas = new ArrayList<SelectItem>();

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	private List<SelectItem> buscarOrganizaciones() {
		limpiar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	// @SuppressWarnings("unchecked")
	public void buscarDivisiones() {
		long idOrganizacion = organizacion;
		limpiar();
		organizacion = idOrganizacion;
		// Query query = em.createNamedQuery("Organizacion.findById");
		// query.setParameter("id", organizacion);
		// List<Organizacion> organizaciones = query.getResultList();
		/*
		 * if (organizaciones != null && organizaciones.size() == 1) {
		 * Organizacion organizacion = organizaciones.get(0); }
		 */
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			Departamento departamento = departamentos.get(0);
			if (departamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}
	@Override
	public void agregarPorId(Long id){
		if (documento.getFirmasEstructuradas() == null) {
			documento.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		Persona p = (Persona)em.createNamedQuery("Persona.findById")
				.setParameter("id", id).getSingleResult();
		agregaPersona(p);
	}
	public void agregar() {
		if (documento.getFirmasEstructuradas() == null) {
			documento.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		try {// por la actualizacion de ajax
			/*
			 * if (documento.getFirmasEstructuradas().size() >= 1) {
			 * FacesMessages
			 * .instance().add("Sólo se puede agregar a un firmante"); return;
			 * }
			 */
			if (!persona.equals(JerarquiasLocal.INICIO)) {
				Persona p = (Persona) em.createNamedQuery("Persona.findById")
						.setParameter("id", this.persona).getSingleResult();
				agregaPersona(p);
			} else if (!personaExterna.equals(JerarquiasLocal.INICIO)) {
				PersonaExterna pe = (PersonaExterna) em
						.createNamedQuery("PersonaExterna.findById")
						.setParameter("id", personaExterna).getSingleResult();
				Persona p = (Persona) pe.getPersona();
				agregaPersona(p);
				listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
				this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
				this.buscarUnidadesOrganizacionales();
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("cargando datos");
		}
	}

	private void agregaPersona(Persona p) {
		if (esVisador(p)) {
			FacesMessages.instance().add("No puede firmar si es visador");
			return;
		}
		if (!contiene(p)) {
			FirmaEstructuradaDocumento fed = new FirmaEstructuradaDocumento();
			fed.setOrden(documento.getFirmasEstructuradas().size() + 1);
			fed.setPersona(p);
			fed.setDocumento(documento);
			documento.getFirmasEstructuradas().add(fed);
			this.restablecerOrden(fed.getOrden());
			limpiar();
//			listOrganizacion.clear();
//			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			this.buscarUnidadesOrganizacionales();
			// if (documento instanceof ActaAdjudicacion) {
			// this.setComisionActaAdjudicacion(p);
			// }
		} else {
			FacesMessages.instance().add("No se puede firmar más de una vez");
			return;
		}
	}

	// private void setComisionActaAdjudicacion(Persona p) {
	// ActaAdjudicacion acta = (ActaAdjudicacion) documento;
	// if (acta.getComisionActaAdjudicacion() == null){
	// acta.setComisionActaAdjudicacion(new
	// ArrayList<ComisionActaAdjudicacion>());
	// }
	// ComisionActaAdjudicacion comision = new ComisionActaAdjudicacion();
	// comision.setNombre(p.getNombreApellido());
	// comision.setActaAdjudicacion(acta);
	// acta.getComisionActaAdjudicacion().add(comision);
	// }

	private boolean contiene(Persona p) {
		List<FirmaEstructuradaDocumento> listFED = documento
				.getFirmasEstructuradas();
		for (FirmaEstructuradaDocumento fed : listFED) {
			if (fed.getPersona().getId().equals(p.getId())) {
				return true;
			}
		}
		return false;
	}

	private boolean esVisador(Persona p) {
		List<VisacionEstructuradaDocumento> listVED = documento
				.getVisacionesEstructuradas();
		for (VisacionEstructuradaDocumento ved : listVED) {
			if (ved.getPersona().getId().equals(p.getId())) {
				return true;
			}
		}
		return false;
	}

	private void limpiar() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void eliminar(Integer orden) {
		int contador = 0;
		for (FirmaEstructuradaDocumento fed : documento
				.getFirmasEstructuradas()) {
			if (fed.getOrden().equals(orden)) {
				FirmaEstructuradaDocumento f = new FirmaEstructuradaDocumento();
				f = fed;
				if (listFirmaEstructuradaEliminadas == null)
					listFirmaEstructuradaEliminadas = new ArrayList<FirmaEstructuradaDocumento>();
				listFirmaEstructuradaEliminadas.add(f);
				documento.getFirmasEstructuradas().remove(contador);
				restablecerOrden(orden);
				break;
			}
			contador++;
		}
	}

	private void restablecerOrden(Integer orden) {
		int contador = 1;
		for (FirmaEstructuradaDocumento ved : documento
				.getFirmasEstructuradas()) {
			if (!ved.getOrden().equals(contador)) {
				ved.setOrden(contador);
			}
			contador++;
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(Long persona) {
		this.persona = persona;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		this.getListOrganizacion();
		this.getListDivision();
		return listDepartamento;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public List<FirmaEstructuradaDocumento> getFirmas() {
		return documento.getFirmasEstructuradas();
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			
			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			this.buscarUnidadesOrganizacionales();
		}
		return listOrganizacion;
	}

	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public Long getDependencia() {
		return dependencia;
	}

	public void setDependencia(Long dependencia) {
		this.dependencia = dependencia;
	}

	public Long getPersonaExterna() {
		return personaExterna;
	}

	public void setPersonaExterna(Long personaExterna) {
		this.personaExterna = personaExterna;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getListDependencia() {
		if (listDependencia.size() == 0) {
			listPersonasExternas.clear();
			listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
			listDependencia.clear();
			listDependencia.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));

			Query query = em
					.createNamedQuery("DependenciaExterna.findByAll");

			final List<DependenciaExterna> dependencias = query.getResultList();

			if (dependencias != null) {
				for (DependenciaExterna de : dependencias) {
					listDependencia.add(new SelectItem(de.getId(), de
							.getDescripcion()));
				}
			}
		}
		return listDependencia;
	}

	public List<SelectItem> getListPersonasExternas() {
		return listPersonasExternas;
	}

	@SuppressWarnings("unchecked")
	public void buscarPersonasExternas() {
		if (!dependencia.equals(JerarquiasLocal.INICIO)) {
			listPersonasExternas.clear();
			listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
			Query query = em
					.createNamedQuery("PersonaExterna.findByIdDependenciaPersona");
			query.setParameter("id", dependencia);
			List<PersonaExterna> personasExternas = query.getResultList();
			if (personasExternas != null) {
				for (PersonaExterna pe : personasExternas) {
					listPersonasExternas.add(new SelectItem(pe.getId(), pe
							.getNombres()));
				}
			}
		}
	}

	public boolean getRenderedExternos() {
		// if (documento instanceof ActaAdjudicacion) {
		// return false;
		// }
		//return true;
		return false;
	}
	
	@Override
	public Long getIdDepartamento() {
		return idDepartamento;
	}

	@Override
	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

}
