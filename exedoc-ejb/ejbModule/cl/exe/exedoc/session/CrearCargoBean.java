package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.validator.Length;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.UnidadOrganizacional;

@Stateful
@Name("crearCargo")
public class CrearCargoBean implements CrearCargo {

	public CrearCargoBean() {
		super();

		unidOrgList.addAll((ArrayList<SelectItem>) buscarUnidades());
		cargoList.addAll((ArrayList<SelectItem>) buscarCargos());
		cargoDefault = false;
	}

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	EntityManager em;

	public List<SelectItem> buscarUnidades() {
		List<SelectItem> unidades = new ArrayList<SelectItem>();
		List<UnidadOrganizacional> listUni = new ArrayList<UnidadOrganizacional>();
		try {
			UnidadOrganizacionalList a = new UnidadOrganizacionalList();
			listUni = a.getResultList();

			for (UnidadOrganizacional u : listUni) {
				unidades.add(new SelectItem(u.getId(), u.getDescripcion()));
			}
			return unidades;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return unidades;
	}

	public List<SelectItem> buscarCargos() {
		List<SelectItem> cargos = new ArrayList<SelectItem>();
		List<Cargo> listCargo = new ArrayList<Cargo>();
		cargos.add(new SelectItem("0", ""));
		try {
			CargoList a = new CargoList();
			listCargo = a.getResultList();
			for (Cargo c : listCargo) {
				cargos.add(new SelectItem(c.getId(), c.getDescripcion() + " - "
						+ c.getUnidadOrganizacional().getDescripcion()));
			}
			return cargos;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cargos;
	}

	public Cargo buscarCargoId(String nameCargo) {
		Cargo cargo = new Cargo();
		try {
			cargo = (Cargo) em.createQuery(
					"select c from Cargo c where c.descripcion = :param")
					.setParameter("param", nameCargo).getSingleResult();
			return cargo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cargo;
	}

	public List<Cargo> buscarCargo() {
		List<Cargo> listCargo = new ArrayList<Cargo>();
		try {
			CargoList a = new CargoList();
			listCargo = a.getResultList();
			return listCargo;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return listCargo;
	}

	@Logger
	private Log log;

	@Begin(join = true)
	public String begin() {
		return "crearCargo";
	}

	@In
	private FacesMessages facesMessages;

	private Cargo cargoIngresado;

	public String registrarCargo() {
		try {
			cargoIngresado = new Cargo();
			cargoIngresado.setDescripcion(descripcion);
			cargoIngresado.setCargoDefault(cargoDefault);
			Cargo cargoPad;
			if (cargoPadre != null && cargoPadre != 0) {
				cargoPad = new Cargo();
				cargoPad.setId(cargoPadre.longValue());
			}
			UnidadOrganizacional unidad = new UnidadOrganizacional();
			if (unidadOrg != null && unidadOrg != 0) {
				unidad = new UnidadOrganizacional();
				unidad.setId(unidadOrg.longValue());
				cargoIngresado.setUnidadOrganizacional(unidad);
			}
			log.info("cargo [#1] ingresado #0", cargoIngresado.getId(),
					cargoIngresado.getDescripcion());
			em.persist(cargoIngresado);
			facesMessages.add("cargo [#0: #1] ingresado", cargoIngresado
					.getId(), cargoIngresado.getDescripcion());
		} catch (Exception ex) {
			facesMessages.add("error al ingresar cargo: #0", ex.getMessage());
		}
		return "cargoList";
	}

	private String descripcion;
	private Integer cargoId;
	private Integer unidadOrg;
	private Integer cargoPadre;
	private Boolean cargoDefault;

	private ArrayList<SelectItem> unidOrgList = new ArrayList<SelectItem>();
	private ArrayList<SelectItem> cargoList = new ArrayList<SelectItem>();

	// add additional action methods that participate in this conversation

	@End
	public String end() {
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public ArrayList<SelectItem> getUnidadesOrganizacionalesList() {
		return null;
	}

	public void setUnidadesOrganizacionalesList(
			ArrayList<SelectItem> unidadesOrganizacionalesList) {
	}

	public Object[] getUnidadOrganizacional() {
		return null;
	}

	public void setUnidadOrganizacional(Object[] unidadOrganizacional) {

	}

	public ArrayList<SelectItem> getUnidOrgList() {
		return unidOrgList;
	}

	public void setUnidOrgList(ArrayList<SelectItem> unidOrgList) {
		this.unidOrgList = unidOrgList;
	}

	@Length(max = 40)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ArrayList<SelectItem> getCargoList() {
		return cargoList;
	}

	public void setCargoList(ArrayList<SelectItem> cargoList) {
		this.cargoList = cargoList;
	}

	public Integer getCargoPadre() {
		return cargoPadre;
	}

	public void setCargoPadre(Integer cargoPadre) {
		this.cargoPadre = cargoPadre;
	}

	public Boolean getCargoDefault() {
		return cargoDefault;
	}

	public void setCargoDefault(Boolean cargoDefault) {
		this.cargoDefault = cargoDefault;
	}

	public Integer getUnidadOrg() {
		return unidadOrg;
	}

	public void setUnidadOrg(Integer unidadOrg) {
		this.unidadOrg = unidadOrg;
	}

	public Integer getCargoId() {
		return cargoId;
	}

	public void setCargoId(Integer cargoId) {
		this.cargoId = cargoId;
	}
}
