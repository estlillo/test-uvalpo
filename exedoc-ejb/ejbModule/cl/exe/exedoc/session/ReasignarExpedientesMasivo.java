package cl.exe.exedoc.session;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

@Local
public interface ReasignarExpedientesMasivo {

	/**
	 * Metodo que inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	String end();

	void destroy();

	void buscarExpedientes();

	/**
	 * Agregar persona para la reasignacion del expediente seleccionado.
	 */
	void agregarPersona();

	String reasignar();

	/**
	 * Metodo que busca los departamentos del sistema segun la division.
	 */
	void buscarDeptos();

	void buscarUnidades();

	void buscarCargos();

	void buscarPersonas();

	void buscarDeptosReasignar();

	void buscarUnidadesReasignar();

	void buscarCargosReasignar();

	void buscarPersonasReasignar();

	void eliminarPersona(Persona persona);

	Long obtenerIdExpediente(String numeroExpediente);

	void setExpedientesAReasignar(Long idExpediente);

	// void esFactura();

	// Getters & Setters
	String getTipoDocumentoBusqueda();

	void setTipoDocumentoBusqueda(String tipoDocumentoBusqueda);

	String getNumeroExpedienteBusqueda();

	void setNumeroExpedienteBusqueda(String numeroExpedienteBusqueda);

	String getNumeroDocumentoBusqueda();

	void setNumeroDocumentoBusqueda(String numeroDocumentoBusqueda);

	String getEmisorDocumentoBusqueda();

	void setEmisorDocumentoBusqueda(String emisorDocumentoBusqueda);

	Date getFechaInicioDocumentoBusqueda();

	void setFechaInicioDocumentoBusqueda(Date fechaInicioDocumentoBusqueda);

	Date getFechaTerminoDocumentoBusqueda();

	void setFechaTerminoDocumentoBusqueda(Date fechaTerminoDocumentoBusqueda);

	String getMateriaDocumentoBusqueda();

	void setMateriaDocumentoBusqueda(String materiaDocumentoBusqueda);

	String getRutProveedorBusqueda();

	void setRutProveedorBusqueda(String rutProveedorBusqueda);

	Integer getNumeroOrdenCompraBusqueda();

	void setNumeroOrdenCompraBusqueda(Integer numeroOrdenCompraBusqueda);

	Long getIdDivision();

	void setIdDivision(Long idDivision);

	Long getIdDepto();

	void setIdDepto(Long idDepto);

	Long getIdUnidad();

	void setIdUnidad(Long idUnidad);

	Long getIdCargo();

	void setIdCargo(Long idCargo);

	Long getIdPersona();

	void setIdPersona(Long idPersona);

	Long getIdDivisionReasignar();

	void setIdDivisionReasignar(Long idDivisionReasignar);

	Long getIdDeptoReasignar();

	void setIdDeptoReasignar(Long idDeptoReasignar);

	Long getIdUnidadReasignar();

	void setIdUnidadReasignar(Long idUnidadReasignar);

	Long getIdCargoReasignar();

	void setIdCargoReasignar(Long idCargoReasignar);

	Long getIdPersonaReasignar();

	void setIdPersonaReasignar(Long idPersonaReasignar);

	List<SelectItem> getListDivisiones();

	void setListDivisiones(List<SelectItem> listDivisiones);

	List<SelectItem> getListDeptos();

	void setListDeptos(List<SelectItem> listDeptos);

	List<SelectItem> getListUnidades();

	void setListUnidades(List<SelectItem> listUnidades);

	List<SelectItem> getListCargos();

	void setListCargos(List<SelectItem> listCargos);

	List<SelectItem> getListPersonas();

	void setListPersonas(List<SelectItem> listPersonas);

	List<SelectItem> getListDivisionesReasignar();

	void setListDivisionesReasignar(List<SelectItem> listDivisionesReasignar);

	List<SelectItem> getListDeptosReasignar();

	void setListDeptosReasignar(List<SelectItem> listDeptosReasignar);

	List<SelectItem> getListUnidadesReasignar();

	void setListUnidadesReasignar(List<SelectItem> listUnidadesReasignar);

	List<SelectItem> getListCargosReasignar();

	void setListCargosReasignar(List<SelectItem> listCargosReasignar);

	List<SelectItem> getListPersonasReasignar();

	void setListPersonasReasignar(List<SelectItem> listPersonasReasignar);

	void setListDestinatarios(Set<Persona> listDestinatarios);

	Object[] getListDestinatarios();

	List<ExpedienteBandejaSalida> getResultadosBusqueda();

	void setResultadosBusqueda(List<ExpedienteBandejaSalida> resultadosBusqueda);

	Map<Long, Boolean> getSelectedReasignados();

	void setSelectedReasignados(Map<Long, Boolean> selectedReasignados);

	Locale getLocale();

	String getTIMEZONE();

	boolean isFactura();

	void setFactura(boolean factura);

	int getCopiaDirectoFlag();

	void setCopiaDirectoFlag(int copiaDirectoFlag);

	/**
	 * Metodo que busca las divisiones.
	 */
	void buscarDivisiones();

	/**
	 * Metodo que busca las divisiones para reasignar.
	 */
	void buscarDivisionesReasignar();

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	Long getOrganizacionReasignar();

	void setOrganizacionReasignar(Long organizacionReasignar);

	List<SelectItem> getListOrganizacion();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	List<SelectItem> getListOrganizacionReasignar();

	void setListOrganizacionReasignar(List<SelectItem> listOrganizacionReasignar);

}
