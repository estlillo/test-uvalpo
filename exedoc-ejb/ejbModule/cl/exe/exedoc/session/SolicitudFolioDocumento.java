package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;
import cl.exe.exedoc.entity.FolioDocumento;

@Local
public interface SolicitudFolioDocumento
{
    public String begin();
    public String increment();
    public String end();
    public void destroy();
 
    

    // add additional interface methods here
	void setTipoDocumentos(List<SelectItem> tipoDocumentos);
	List<SelectItem> getTipoDocumentos();
	Long getTipoDocumento();
	void setTipoDocumento(Long tipoDocumento);
	Long getFiscalia();
	void setFiscalia(Long idFiscalia);
	Long getUnidad();
	void setUnidad(Long idUnidad);
	String getMateria();
	void setMateria(String idMateria);
	List<SelectItem> getFiscalias();
	void setFiscalias(List<SelectItem> fiscalias);
	void setUnidades(List<SelectItem> unidades);
	String generar();
	List<SelectItem> getUnidades();
	String getAgnoActual();
	void setAgnoActual(String agno);
	void obtenerFolio();
	
	String getValor();
	void setValor(String valor);
	String getObservacion();
	void setObservacion(String observacion);
	
	void nivelFiscalia();
	void setFolioDocumento(FolioDocumento folioDocumento);
	FolioDocumento getFolioDocumento();
	void setHabilitaUnidad(Boolean habilitaUnidad);
	Boolean getHabilitaUnidad();
	void limpiarFolio();

}
