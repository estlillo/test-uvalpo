package cl.exe.exedoc.session;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.email.Email;
import cl.exe.email.message.Mail;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;

/**
 * Implementacion de {@link ProcesoAutomatico}.
 * 
 */
@Stateless
@Name(ProcesoAutomaticoBean.NAME)
public class ProcesoAutomaticoBean implements ProcesoAutomatico {

	/**
	 * NAME = "procesoAutomatico".
	 */
	public static final String NAME = "procesoAutomatico";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private AdministradorAlertas administradorAlertas;
	
	@EJB
	private Customizacion customizacion;

	private boolean enviarCorreo;
	private String notificacionCorreo;
	
	/**
	 * Constructor.
	 */
	public ProcesoAutomaticoBean() {
		super();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void iniciarProceso(final Date fecha) {
		enviarCorreo = customizacion.getEnviadorCorreo();
		notificacionCorreo = customizacion.getNotificacionEnviadorCorreo();
		if (!enviarCorreo) {
			log.info("Enviador de correo desactivado");
		} else {
			this.notificar(Boolean.FALSE);
	
			try {
				administradorAlertas.obtenerDocumetosYEnviarAlerta(fecha);
				// TODO ESTO ES DE MINPUB
				//administradorAlertas.generarAlertasSolicitudesDeRespuesta(fecha);
			} catch (PersistenceException e) {
				log.error(
						"PersistenceException: Ocurrio un error al obtener y enviar las alertas. ",
						e);
			} catch (NullPointerException e) {
				log.error(
						"NullPointerException: Ocurrio un error al obtener y enviar las alertas. ",
						e);
			}

//		TODO ESTO ES DE MINPUB
//		final String q = "BEGIN CARGAR_EXPEDIENTES_OFFLINE(); END;";
//		try {
//			em.createNativeQuery(q).executeUpdate();
//		} catch (PersistenceException e) {
//			log.error(
//					"PersistenceException: Ocurrio un error al ejecutar el procedimiento de carga. ",
//					e);
//		}
		
			this.notificar(Boolean.TRUE);
		}
	}

	private void notificar(final Boolean notificaFin) {
		if (notificacionCorreo != null &&
				notificacionCorreo.trim().length() > 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	
			Email email = new Email();
	
			String cuerpo = notificaFin ? "Finalizo el proceso de Notificacion (" + sdf.format(new Date()) + ")"
					: "Se inicio el proceso de Notificacion (" + sdf.format(new Date()) + ")";
			String asunto = notificaFin ? "Finalizo Proceso Notificacion"
					: "Inicio Proceso Notificacion";
			
			try {
				Mail mail = new Mail();
				mail.setSubject(asunto);
				mail.setText(cuerpo);
				mail.setForm(new InternetAddress(notificacionCorreo));
				email.enviarMail(mail);
			} catch (AddressException e) {
				log.error("AddressException", e);
			}
		} else {
			log.info("No existe correo de destino para notificaciones del enviador de correo");
		}
	}
	
	//TODO aceptar
	public void getProperties() {
	  Properties defaultProps = new Properties();
	  enviarCorreo = false;
	  notificacionCorreo = null;
      try {
          final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("exedoc/customizacion.properties");
          defaultProps.load(resourceAsStream);
          if (defaultProps.getProperty("email_sender_active") != null) {
        	  enviarCorreo = defaultProps.getProperty("email_sender_ective").equalsIgnoreCase("y") ? true : false;
          }
          if (defaultProps.getProperty("email_sender_notification") != null) {
        	  notificacionCorreo = defaultProps.getProperty("email_sender_notification");
        	  if (notificacionCorreo.trim().length() == 0) {
        		  notificacionCorreo = null;
        	  }
          }
      }
      catch (Exception e) {
          
      }
	}

}
