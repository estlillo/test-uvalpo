package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("seleccionarEmisorDocumento")
public class SeleccionarEmisorDocumentoBean implements SeleccionarEmisorDocumento {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = true)
	private Persona usuario;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Documento documento;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	private List<SelectItem> buscarOrganizaciones() {
		limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@SuppressWarnings("unchecked")
	public void buscarDivisiones() {
		long idOrganizacion = organizacion;
		limpiarDestinatario();
		organizacion = idOrganizacion;
		Query query = em.createNamedQuery("Organizacion.findById");
		query.setParameter("id", organizacion);
		List<Organizacion> organizaciones = query.getResultList();
//		if (organizaciones != null && organizaciones.size() == 1) {
//			Organizacion organizacion = organizaciones.get(0);
//		}
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			Departamento departamento = departamentos.get(0);
			if (departamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	public void agregarDestinatario() {
		try {
			if (!this.persona.equals(JerarquiasLocal.INICIO)) {
				Persona persona = em.find(Persona.class, this.persona);
				String value = persona.getNombres() + " " + persona.getApellidoPaterno();

				Cargo cargo = em.find(Cargo.class, this.cargo);
				value += " - " + cargo.getDescripcion();
				if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					UnidadOrganizacional uo = em.find(UnidadOrganizacional.class, this.unidadOrganizacional);
					value += " - " + uo.getDescripcion();
				} else if (!persona.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					Departamento depto = em.find(Departamento.class, this.departamento);
					value += " - " + depto.getDescripcion();
				} else {
					Division d = em.find(Division.class, this.division);
					value += " - " + d.getDescripcion();
				}
				if (destinatariosDocumento != null && !destinatariosDocumento.isEmpty()) {
					SelectPersonas s = new SelectPersonas(value);
					for (SelectPersonas sp : destinatariosDocumento) {
						if (sp.getDescripcion().equals(s.getDescripcion())) {
							FacesMessages.instance().add("El emisor no puede ser parte de los destinatarios del documento");
							return;
						}
					}
				}
				documento.setEmisor(value);
				documento.setIdEmisor(persona.getId());
				this.limpiarDestinatario();
			}
		} catch (Exception e) {
			log.info("Cargando Campos");
		}
	}

	public void agregarDestinatarioProvidencia() {
		try {
			if (!this.persona.equals(JerarquiasLocal.INICIO)) {
				Persona persona = em.find(Persona.class, this.persona);
				String value = persona.getNombres() + " " + persona.getApellidoPaterno();

				Cargo cargo = persona.getCargo();
				if (cargo.getDescripcion().equals("Jefe Division") || cargo.getDescripcion().equals("Jefe Departamento") || cargo.getDescripcion().equals("Jefe Unidad")) {
					value = cargo.getDescripcion();
				} else {
					value += " - " + cargo.getDescripcion();
				}
				UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
				Departamento depto = uo.getDepartamento();
				if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					value += " - " + uo.getDescripcion();
				} else if (!persona.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					value += " - " + depto.getDescripcion();
				} else {
					Division d = depto.getDivision();
					value += " - " + d.getDescripcion();
				}
				documento.setEmisor(value);
				documento.setIdEmisor(persona.getId());
				//listOrganizacion.addAll(this.buscarOrganizaciones());
				//organizacion = (Long) listOrganizacion.get(1).getValue();
				//buscarDivisiones();
				//this.limpiarDestinatario();
			}
		} catch (Exception e) {
			log.info("Cargando Campos");
		}
	}

	@Override
	public void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		/*if (listOrganizacion.size() == 2) {
			organizacion = (Long) listOrganizacion.get(1).getValue();
			buscarDivisiones();
		}*/
	}

	public void eliminarDestinatarioDocumento(String destinatario) {
		documento.setEmisor(null);
		documento.setIdEmisor(null);
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(Long persona) {
		this.persona = persona;
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		List<SelectPersonas> destinatariosDocumento = new ArrayList<SelectPersonas>();
		if (documento.getEmisor() != null) {
			destinatariosDocumento.add(new SelectPersonas(documento.getEmisor()));
		}
		return destinatariosDocumento;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			listOrganizacion.addAll(this.buscarOrganizaciones());
			//organizacion = (Long) listOrganizacion.get(1).getValue();
			//buscarDivisiones();
		}
		return listOrganizacion;
	}

	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public List<SelectItem> getListaJefes() {
		List<SelectItem> listaJefes = jerarquias.getJefe(this.usuario, JerarquiasLocal.TEXTO_INICIAL);
		if (jerarquias.esJefe(this.usuario)) {
			if (!estaJefe(listaJefes, this.usuario.getId())) {
				listaJefes.add(new SelectItem(this.usuario.getId(), this.usuario.getNombreApellido()));
			}
		}

		return listaJefes;
	}

	private boolean estaJefe(List<SelectItem> listaJefes, Long id) {
		for (SelectItem si : listaJefes) {
			if (si.getValue().equals(id)) {
				return true;
			}
		}
		return false;
	}

}
