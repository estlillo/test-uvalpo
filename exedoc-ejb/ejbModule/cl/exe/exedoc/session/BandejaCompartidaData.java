package cl.exe.exedoc.session;

import java.util.HashMap;
import java.util.List;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaCompartida;



public class BandejaCompartidaData {

	public BandejaCompartidaData() {
		System.out.println("bandeja compartida data instanciado");
	}

	public BandejaCompartidaData(List<ExpedienteBandejaCompartida> lista) {
		System.out.println("bandeja compartida data instanciado con lista");
		listExpedientes = lista;
		for (ExpedienteBandejaCompartida e : listExpedientes)
			map.put(e.getId(), e);
	}

	private HashMap<Long, ExpedienteBandejaCompartida> map = new HashMap<Long, ExpedienteBandejaCompartida>();

	private List<ExpedienteBandejaCompartida> listExpedientes;

	public List<ExpedienteBandejaCompartida> getListExpedientes() {
		return listExpedientes;
	}

	public void acusarRecibo(Long id) {
		ExpedienteBandejaCompartida e = map.get(id);
		if (e != null)
			e.setRecibido(true);
	}

	/**
	 * Metodo que elimina expediente de la lista listExpedientes para la bandeja
	 * entrada.
	 * 
	 * @param id
	 *            {@link Long}
	 */
	public void eliminarExpediente(final Long id) {
		final ExpedienteBandejaCompartida e = map.get(id);
		if (e != null) {
			map.remove(id);
			listExpedientes.remove(e);
		}
	}

	public boolean expRepetidos(ExpedienteBandejaCompartida exp) {
		if (listExpedientes != null && !listExpedientes.isEmpty()) {
			for (ExpedienteBandejaCompartida e : listExpedientes) {
				if (!e.getId().equals(exp.getId())
						&& e.getNumeroExpediente().equals(
								exp.getNumeroExpediente())) {
					return true;
				}
			}
			return false;
		}
		return false;
	}

	/*
	 * public void cambiaFolioOPartesExpediente(Long id, String
	 * numeroExpedienteOPartes) { ExpedienteBandejaEntrada e = map.get(id); if
	 * (e != null) { e.setNumeroExpedienteOPartes(numeroExpedienteOPartes); } }
	 */
}
