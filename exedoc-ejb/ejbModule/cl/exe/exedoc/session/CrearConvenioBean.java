package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.core.Conversation;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Convienen;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.TeniendoPresente;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoPlantilla;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.GeneradorResolucionRespuestaInterface;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("crearConvenio")
public class CrearConvenioBean implements CrearConvenio {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private GeneradorResolucionRespuestaInterface gen;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	//TODO Revisar Documentos Anexos
	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosAnexo;*/

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	// @Out(required = false, value = "documento", scope =
	// ScopeType.CONVERSATION)
	private Convenio convenio;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@In(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;

	private Integer idPlantilla;
	private Integer idPlantillaResolucion;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private Boolean firmado;
	private Boolean visado;
	private boolean guardado;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	private List<ArchivoAdjuntoDocumentoElectronico> archivosAnexos;
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;
	private String materiaArchivo;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	private List<Long> idExpedientes;

	private String tipoArchivo;

	private static final long ID_CARGO_FISCAL = 71;

//	@In(required = false, scope = ScopeType.PAGE)
//	@Out(required = false, scope = ScopeType.PAGE)
//	private Integer idClasificacionSubTipoOtrosSelec;
//
//	@In(required = false, scope = ScopeType.PAGE)
//	@Out(required = false, scope = ScopeType.PAGE)
//	@Range(min = 2, max = 999999999, message = "Debe ingresar una clasificacion")
//	private Integer idClasificacionTipoOtrosSelec;
//
//	List<SelectItem> tipos1Otros = null;
//	List<SelectItem> tipos2Otros = null;

	public void agregarAntecedente() {
		if (this.convenio.getArchivosAdjuntos() == null) {
			this.convenio.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		}
		if (!archivosAnexos.isEmpty()) {
			for (ArchivoAdjuntoDocumentoElectronico archivo : archivosAnexos) {
				if (true/*!existeNombreArchivo(archivo.getNombreArchivo())*/) {
					archivo.setDocumentoElectronico(this.convenio);
					archivo.setFecha(new Date());
					archivo.setIdNuevoArchivo(this.convenio.getArchivosAdjuntos().size());
					archivo.setAdjuntadoPor(usuario);
					archivo.setMateria(materiaArchivo);
					this.convenio.getArchivosAdjuntos().add(archivo.archivo());
				}
			}
		}
		limpiarAnexos();
	}

	/*private boolean existeNombreArchivo(String nombreArchivo) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getArchivo() != null && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) {
					return true;
				}
			}
		}
		return false;
	}*/

	public void agregarDocumento() {
		armaDocumento(false, false);
	}

	private void armaDocumento(boolean visar, boolean firmar) {
		if (this.getFirmado()) {
			return;
		}
		this.convenio.setPlazo(null);
		this.configurarArchivosAdjuntos();

		this.convenio.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		this.convenio.setTipoDocumento(new TipoDocumento(TipoDocumento.CONVENIO));

		boolean modificarVisaFirmas = modificarVisaFirmas();
		if (modificarVisaFirmas) {
			if (this.convenio.getVisacionesEstructuradas() != null && !this.convenio.getVisacionesEstructuradas().isEmpty()) {
				eliminarVisasEstructuradas();
			}
			if (this.convenio.getFirmasEstructuradas() != null && !this.convenio.getFirmasEstructuradas().isEmpty()) {
				eliminarFirmasEstructuradas();
			}
		}

		if (!visar && !firmar) {
			if (this.convenio.getId() != null) {
				this.convenio.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
				this.convenio.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.convenio, this.usuario));
			} else {
				this.convenio.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
				this.convenio.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.convenio, this.usuario));
			}
		}
		if (firmar) {
			this.repositorio.firmar(this.convenio, usuario);
			this.convenio.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.convenio, this.usuario));
			//Pruebas mias
			List<Expediente> padre = me.obtienePrimerExpediene(expediente);
			List<Persona> dest = me.buscarDestinatarios(padre);
			for (Persona expediente : dest) {
				me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
			}
		}
		if (visar) {
			this.repositorio.visar(this.convenio, usuario);
			this.convenio.addBitacora(new Bitacora(EstadoDocumento.VISADO, this.convenio, this.usuario));
		}

		this.convenio.setReservado(false);

		this.setDistribucionDocumento();

//		if (idClasificacionTipoOtrosSelec != null && idClasificacionTipoOtrosSelec != -1) {
//			this.convenio.setClasificacionTipoOtros(new ClasificacionTipoOtros(idClasificacionTipoOtrosSelec, getDescripcionTipoOtros(idClasificacionTipoOtrosSelec)));
//		}
//
//		if (idClasificacionSubTipoOtrosSelec != null && idClasificacionSubTipoOtrosSelec != -1) {
//			this.convenio.setClasificacionSubtipoOtros(new ClasificacionSubtipoOtros(idClasificacionSubTipoOtrosSelec, getDescripcionSubtipoOtros(idClasificacionSubTipoOtrosSelec)));
//		}

		actualizaListasDocumentos();

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		persistirDocumento();

		if (modificarVisaFirmas) {
			actualizaVisaFirma();
		}
	}
//
//	private String getDescripcionTipoOtros(Integer idTipoOtros) {
//		if (idTipoOtros != null && idTipoOtros != -1) {
//			return (String) em.createNativeQuery("select descripcion from clasificacion_tipo_otros where id = :id").setParameter("id", idTipoOtros).getSingleResult();
//		}
//		return "";
//	}
//
//	private String getDescripcionSubtipoOtros(Integer idSubtipoOtros) {
//		if (idSubtipoOtros != null && idSubtipoOtros != -1) {
//			return (String) em.createNativeQuery("select descripcion from clasificacion_subtipo_otros where id = :id").setParameter("id", idSubtipoOtros).getSingleResult();
//		}
//		return "";
//	}

	private void configurarArchivosAdjuntos() {
		if (listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico a : listaArchivosEliminados) {
				String sqlBorra = "DELETE FROM ARCHIVOS WHERE id = " + a.getId();
				em.createNativeQuery(sqlBorra).executeUpdate();
			}
		}
	}

	private void eliminarVisasEstructuradas() {
		if (this.convenio.getId() != null) {
			String sqlBorra = "DELETE FROM VISACIONES_ESTRUCTURADAS WHERE id_documento = " + convenio.getId();
			em.createNativeQuery(sqlBorra).executeUpdate();
		}
	}

	private void eliminarFirmasEstructuradas() {
		if (this.convenio.getId() != null) {
			String sqlBorra = "DELETE FROM FIRMAS_ESTRUCTURADAS WHERE id_documento = " + convenio.getId();
			em.createNativeQuery(sqlBorra).executeUpdate();
		}
	}

	private void setDistribucionDocumento() {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDocumento(this.convenio);
			dd.setDestinatarioPersona(item.getPersona());
			ddList.add(dd);
		}
		this.convenio.setDistribucion(ddList);
	}

	private void actualizaListasDocumentos() {
		if (this.convenio.getTipoDocumentoExpediente() != null) {
			if (this.convenio.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				if (!existeDocumento(listDocumentosRespuesta)) {
					this.convenio.setEliminable(true);
					listDocumentosRespuesta.add(this.convenio);
				} else {
					if (this.convenio.getId() != null) {
						listDocumentosRespuesta.remove(this.convenio);
						listDocumentosRespuesta.add(this.convenio);
					} else {
						this.reemplazaDocumento(listDocumentosRespuesta);
					}
				}
			} /*else if (this.convenio.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
				if (!existeDocumento(listDocumentosAnexo)) {
					listDocumentosAnexo.add(this.convenio);
				} else {
					if (this.convenio.getId() != null) {
						listDocumentosAnexo.remove(this.convenio);
						listDocumentosAnexo.add(this.convenio);
					} else {
						this.reemplazaDocumento(listDocumentosAnexo);
					}
				}
			}*/
		}
	}

	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.convenio.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.convenio.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.convenio);
			}
		}
		listaDocumentos = lista;
	}

	private void persistirDocumento() {
	    try {
		if (this.convenio.getId() != null) {
			md.actualizarDocumento(this.convenio);

		} else if (this.expediente != null && this.expediente.getId() != null) {
			me.agregarRespuestaAExpediente(expediente, this.convenio, true);
		}
	    } catch (DocumentNotUploadedException e) {
	        e.printStackTrace();
	    }
	}

	@SuppressWarnings("unchecked")
	private void actualizaVisaFirma() {
		if (this.convenio.getId() != null) {
//			String sqlVisasActuales = "SELECT * FROM VISACIONES_ESTRUCTURADAS WHERE id_documento = " + convenio.getId() + " order by orden asc";
//			List<VisacionEstructuradaDocumento> visas = em.createNativeQuery(sqlVisasActuales, VisacionEstructuradaDocumento.class).getResultList();
			String sqlVisasActuales = "SELECT * FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = " + convenio.getId() + " order by ve.orden asc";
			List<VisacionEstructuradaDocumento> visas = em.createQuery(sqlVisasActuales).getResultList();
			this.convenio.getVisacionesEstructuradas().clear();
			this.convenio.getVisacionesEstructuradas().addAll(visas);

			String sqlFirmasActuales = "SELECT * FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = " + convenio.getId() + " order by fe.orden asc";
			List<FirmaEstructuradaDocumento> firmas = em.createQuery(sqlFirmasActuales).getResultList();
			this.convenio.getFirmasEstructuradas().clear();
			this.convenio.getFirmasEstructuradas().addAll(firmas);
		}
	}

	public String buscaDocumento() {
		log.info("buscando doc..." + convenio.getId());
		convenio = em.find(Convenio.class, convenio.getId());
		if (convenio.getFirmas() == null || convenio.getFirmas().isEmpty()) {
			armaDocumento(false, false);
		}

		firmado = true;
		convenio.getParrafos().size();
		convenio.getDistribucionDocumento().size();
		convenio.getVisaciones().size();
		convenio.getFirmas().size();
		convenio.getBitacoras().size();
		convenio.getVisacionesEstructuradas().size();
		convenio.getFirmasEstructuradas().size();
		convenio.getArchivosAdjuntos().size();

		for (Documento d : listDocumentosRespuesta) {
			if (d.getId().equals(convenio.getId())) {
				d.setNumeroDocumento(convenio.getNumeroDocumento());
				d.setEstado(convenio.getEstado());
				d.setFirmas(convenio.getFirmas());
				break;
			}
		}

		return "";
	}

	/*
	 * public String cambiar() { return "crearConvenio"; }
	 */

	public String despacharExpediente() {
		if (!guardado) {
			armaDocumento(false, false);
		}
		if (expediente == null || expediente.getId() == null) {
			distribuirDocumentos();
		}
		if (this.guardarExpediente()) {
			idPlantillaSelec = null;
			end();
			Conversation.instance().end();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			if (expediente == null || expediente.getId() == null) {
				noDistribuirDocumentos();
			}
			return "";
		}
	}

	private void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

	@SuppressWarnings("unchecked")
	private boolean guardarExpediente() {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo al documento anterior
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado() && !this.documentoOriginal.getId().equals(this.convenio.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
                    md.actualizarDocumento(this.documentoOriginal);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			}
		}

		/*for (Documento doc : listDocumentosAnexo) {
			if (doc.getId() == null) {
				if (doc instanceof DocumentoBinario) {
					DocumentoBinario docBin = (DocumentoBinario) doc;
					Long idDocumentoReferencia = buscaDocumentoReferencia(docBin.getIdDocumentoReferencia());
					if (idDocumentoReferencia != null) {
						me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia);
					} else {
						me.agregarAnexoAExpediente(expediente, doc, true);
					}
				} else {
					me.agregarAnexoAExpediente(expediente, doc, true);
				}
			} else {
				md.actualizarDocumento(doc);
			}
		}*/

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getId() == null) {
				try {
                    me.agregarRespuestaAExpediente(expediente, doc, true);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			} else {
				// Cerrar plazo al documento anterior
				if (doc.getCompletado() != null && !doc.getCompletado() && !doc.getId().equals(this.convenio.getId())) {
					doc.setCompletado(true);
					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
					try {
                        md.actualizarDocumento(doc);
                    } catch (DocumentNotUploadedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
				}
			}
		}

		this.guardarObservaciones();
		idExpedientes = new ArrayList<Long>();
		Date fechaIngreso = new Date();

		Persona de = null;
		if (this.convenio.getVisacionesEstructuradas() != null && this.convenio.getVisacionesEstructuradas().size() != 0) {
			Collections.sort(this.convenio.getVisacionesEstructuradas(), new Comparator<VisacionEstructuradaDocumento>() {
				public int compare(VisacionEstructuradaDocumento o1, VisacionEstructuradaDocumento o2) {
					return o1.getOrden().compareTo(o2.getOrden());
				}
			});
			de = this.convenio.getVisacionesEstructuradas().get(0).getPersona();
			if (de.equals(usuario)) {
				FacesMessages.instance().add("Debe visar el documento antes de Despacharlo");
				return false;
			}
		} else if (this.convenio.getFirmasEstructuradas() != null && this.convenio.getFirmasEstructuradas().size() != 0) {
			Collections.sort(this.convenio.getFirmasEstructuradas(), new Comparator<FirmaEstructuradaDocumento>() {
				public int compare(FirmaEstructuradaDocumento o1, FirmaEstructuradaDocumento o2) {
					return o1.getOrden().compareTo(o2.getOrden());
				}
			});
			de = this.convenio.getFirmasEstructuradas().get(0).getPersona();
			if (de.equals(usuario)) {
				FacesMessages.instance().add("Debe firmar el documento antes de Despacharlo");
				return false;
			}
		}

		if (de != null) {
			log.info("Despachando a : #0", de.getUsuario());
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			if (this.convenio.getFirmas() != null && this.convenio.getFirmas().size() != 0) {
				agregarListaDestinatariosExpediente(fechaIngreso);
//				if (this.documentoOriginal instanceof Adquisicion) {
//					Query query = em.createNamedQuery("FlujoEstructurado.findByIdFlujo");
//					query.setParameter("idFlujo", FlujoEstructurado.DESPACHO_RESOLUCION);
//					List<FlujoEstructurado> flujos = query.getResultList();
//					if (flujos != null) {
//						for (FlujoEstructurado flujo : flujos) {
//							de = flujo.getResponsable();
//							if (de.equals(usuario)) {
//								FacesMessages.instance().add("Debe seleccionar un destinatario al expediente antes de Despacharlo");
//								return false;
//							}
//							log.info("Despachando a : #0", de.getUsuario());
//							idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
//						}
//					}
//				}
			}
		}

		List<Expediente> expedientes = new ArrayList<Expediente>();
		if (!idExpedientes.isEmpty()) {
			for (Long id : idExpedientes) {
				Expediente e = me.buscarExpediente(id);
				me.despacharExpediente(e);
				expedientes.add(e);
			}
			me.despacharExpediente(expediente);
			//me.despacharNotificacionPorEmail(expedientes);

			FacesMessages.instance().add("Expediente Despachado");
			return true;
		} else {
			FacesMessages.instance().add("No hay destinatarios a quien despachar");
			return false;
		}
	}

//	private Long buscaDocumentoReferencia(Integer idDocumento) {
//		Long id = null;
//		for (Documento doc : listDocumentosRespuesta) {
//			if (doc.getIdNuevoDocumento().equals(idDocumento)) {
//				id = doc.getId();
//				break;
//			}
//		}
//		if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
//			id = documentoOriginal.getId();
//		}
//		return id;
//	}

	private void noDistribuirDocumentos() {
		List<Documento> list = new ArrayList<Documento>();
		list.add(documentoOriginal);
		list.addAll(listDocumentosRespuesta);
		listDocumentosRespuesta.clear();
		listDocumentosRespuesta.addAll(list);
	}

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	private void agregarListaDestinatariosExpediente(Date fechaIngreso) {
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		for (SelectPersonas sp : distribucionDocumento) {
			Persona p = em.find(Persona.class, sp.getPersona().getId());
			/*
			 * if (p.getRoles().contains(new Rol(Rol.JEFE)) &&
			 * p.getRoles().contains(new Rol(Rol.ABASTECIMIENTO))) {
			 * p.setDestinatarioConCopia(false); } else {
			 */
			p.setDestinatarioConCopia(true);
			// }
			listDestinatarios.add(p);
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, p, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String desvisarYDesfirmar(Convenio dec) {
		if (dec.getFirmas() == null || dec.getFirmas().isEmpty()) {
			if (dec.getVisaciones() != null && !dec.getVisaciones().isEmpty()) {
				for (VisacionDocumento visa : dec.getVisaciones()) {
					VisacionDocumento visaElim = em.find(VisacionDocumento.class, visa.getId());
					em.remove(visaElim);
				}
				dec.getVisaciones().clear();
				dec.setVisaciones(null);
			}

			if (dec.getVisacionesEstructuradas() != null && !dec.getVisacionesEstructuradas().isEmpty()) {
				for (VisacionEstructuradaDocumento visaEstructurada : dec.getVisacionesEstructuradas()) {
					VisacionEstructuradaDocumento visaEstructElim = em.find(VisacionEstructuradaDocumento.class, visaEstructurada.getId());
					em.remove(visaEstructElim);
				}
				dec.getVisacionesEstructuradas().clear();
				dec.setVisacionesEstructuradas(null);
			}

			if (dec.getFirmasEstructuradas() != null && !dec.getFirmasEstructuradas().isEmpty()) {
				for (FirmaEstructuradaDocumento fed : dec.getFirmasEstructuradas()) {
					FirmaEstructuradaDocumento fedElim = em.find(FirmaEstructuradaDocumento.class, fed.getId());
					em.remove(fedElim);
				}
				dec.getFirmasEstructuradas().clear();
				dec.setFirmasEstructuradas(null);
			}

			dec.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
			dec.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, dec, usuario));
			dec.setCmsId(null);
			expediente.setFechaDespacho(null);
			persistirDocumento();

			FacesMessages.instance().add("Decreto desvisado y desfirmado");
		} else {
			FacesMessages.instance().add("El decreto no se puede desvisar, pues ya esta firmado");
		}
		return "crearDecreto";
	}

	public void eliminarArchivoAntecedente(Integer idNuevoArchivo) {
		if (this.listaArchivosEliminados == null) {
			this.listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		}

		for (ArchivoAdjuntoDocumentoElectronico a : this.convenio.getArchivosAdjuntos()) {
			if (a.getIdNuevoArchivo().equals(idNuevoArchivo)) {
				if (a.getId() != null) {
					this.listaArchivosEliminados.add(a);
				}
				this.convenio.getArchivosAdjuntos().remove(a);
				break;
			}
		}

		if (convenio != null) {
			if (convenio.getId() != null) {
				em.merge(convenio);
				em.flush();
			}
		}
	}

	public String end() {
		distribucionDocumento = new ArrayList<SelectPersonas>();
		listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		limpiarAnexos();
		guardado = false;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}
		vistoDesdeReporte = null;
		return homeCrear;
	}

	public void firmar() {
		for (FirmaEstructuradaDocumento fed : convenio.getFirmasEstructuradas()) {
			if (fed.getPersona().equals(usuario)) {
				this.armaDocumento(false, true);
				Query query = em.createQuery("DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.id = ?");
				query.setParameter(1, fed.getId());
				convenio.getFirmasEstructuradas().remove(0);
				query.executeUpdate();
				convenio.getFirmasEstructuradas().remove(fed);

				salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
					if (de.getDocumento().getId().equals(convenio.getId())) {
						for (FirmaEstructuradaDocumento fe : de.getDocumento().getFirmasEstructuradas()) {
							if (fe.getPersona().equals(usuario)) {
								de.getDocumento().getFirmasEstructuradas().remove(fe);
								break salida;
							}
						}
					}
				}
				FacesMessages.instance().add("Resolución firmada.");
				break;
			}
		}
		guardado = true;
	}

	public String getConsiderando() {
		String considerando = "";
		if (convenio.getConsiderandos() == null || convenio.getConsiderandos().isEmpty()) {
			Considerandos parrafo = new Considerandos();
			parrafo.setCuerpo(considerando);
			parrafo.setDocumento(this.convenio);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.convenio.setConsiderandos(parrafos);
		}
		List<Parrafo> cons = this.convenio.getConsiderandos();
		Parrafo parr = cons.get(0);
		considerando = parr.getCuerpo();
		return considerando;
	}

	public Convenio getConvenio() {
		if (convenio == null) {
			convenio = new Convenio();
			Calendar ahora = new GregorianCalendar();
			convenio.setFechaCreacion(ahora.getTime());
			ahora.add(Calendar.DATE, 5);
			convenio.setPlazo(ahora.getTime());

			convenio.setAutor(usuario);
			// tipo temporal, Siempre se toma como original el primero.
			convenio.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
			// id temporal para borrar un documento
			convenio.setIdNuevoDocumento(Integer.MAX_VALUE);
			convenio.setReservado(false);
			convenio.setEnEdicion(true);
		}
		return convenio;
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}

	public Boolean getFirmado() {
		if (this.convenio != null && this.convenio.getFirmas() != null && this.convenio.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public List<FirmaEstructuradaDocumento> getFirmas() {
		if (convenio.getFirmasEstructuradas() == null) {
			convenio.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		return convenio.getFirmasEstructuradas();
	}

	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos() {
		if (this.convenio.getArchivosAdjuntos() != null) {
			int cont = 0;

			for (ArchivoAdjuntoDocumentoElectronico a : this.convenio.getArchivosAdjuntos()) {
				a.setIdNuevoArchivo(cont++);
			}
			return this.convenio.getArchivosAdjuntos();
		}
		return new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
	}

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public int getMaxFilesQuantity() {
		int valor = 20;
		/*
		 * if (archivosAnexos == null || archivosAnexos.isEmpty()) { valor = 1;
		 * } else { valor = archivosAnexos.size(); }
		 */
		return valor;
	}

	public String getConvienen() {
		String convienen = "";
		if (convenio.getConvienen() == null || convenio.getConvienen().isEmpty()) {
			Convienen parrafo = new Convienen();
			parrafo.setCuerpo(convienen);
			parrafo.setDocumento(this.convenio);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.convenio.setConvienen(parrafos);
		}
		List<Parrafo> res = this.convenio.getConvienen();
		Parrafo parr = res.get(0);
		convienen = parr.getCuerpo();
		return convienen;
	}

	public String getUrlFirma() {
		String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}

	public String getVisaciones() {
		if (this.convenio.getVisacionesEstructuradas() == null) {
			this.convenio.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
		}
		List<VisacionEstructuradaDocumento> visaciones = convenio.getVisacionesEstructuradas();

		StringBuilder sb = new StringBuilder();
		for (int i = visaciones.size() - 1; i >= 0; i--) {
			sb.append(visaciones.get(i).getPersona().getIniciales() + "/");
		}
		sb.append(convenio.getAutor().getIniciales().toLowerCase());

		return sb.toString();
	}

	public List<VisacionDocumento> getVisacionesV() {
		List<VisacionDocumento> visaciones = null;
		if (this.convenio != null && this.convenio.getVisaciones() != null) {
			visaciones = convenio.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public Boolean getVisado() {
		if (this.convenio != null && this.convenio.getVisaciones() != null && this.convenio.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public String getTeniendoPresente() {
		String teniendoPresente = "";
		if (convenio.getTeniendoPresente() == null || convenio.getTeniendoPresente().isEmpty()) {
			TeniendoPresente parrafo = new TeniendoPresente();
			parrafo.setCuerpo(teniendoPresente);
			parrafo.setDocumento(this.convenio);
			parrafo.setNumero(1L);
			List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.convenio.setTeniendoPresente(parrafos);
		}
		teniendoPresente = this.convenio.getTeniendoPresente().get(0).getCuerpo();
		return teniendoPresente;
	}

	public void guardarYCrearExpediente() {
		armaDocumento(false, false);
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);

		List<VisacionEstructuradaDocumento> visaciones = convenio.getVisacionesEstructuradas();
		List<FirmaEstructuradaDocumento> firmas = convenio.getFirmasEstructuradas();

		if (visaciones != null && visaciones.size() > 0) {
			listDestinatarios.add(visaciones.get(0).getPersona());
		} else if (firmas != null && firmas.size() > 0) {
			listDestinatarios.add(firmas.get(0).getPersona());
		}

//		idClasificacionTipoOtrosSelec = null;
//		idClasificacionSubTipoOtrosSelec = null;
	}

//	@SuppressWarnings("unchecked")
//	public List<SelectItem> obtenerClasificacionSubTipoOtros() {
//		List<ClasificacionSubtipoOtros> list;
//		List<SelectItem> item = new ArrayList<SelectItem>();
//		if (idClasificacionSubTipoOtrosSelec == null || idClasificacionSubTipoOtrosSelec == 0) {
//			list = em.createNamedQuery("ClasificacionSubtipoOtros.findByAll").getResultList();
//			/* item.add(new SelectItem(-1, "<<Seleccionar SubTipo>>")); */
//			for (ClasificacionSubtipoOtros pr : list) {
//				item.add(new SelectItem(pr.getId(), pr.getDescripcion()));
//			}
//		} else {
//			Query query = em.createNamedQuery("ClasificacionSubtipoOtros.findById");
//			query.setParameter("id", idClasificacionSubTipoOtrosSelec);
//			list = query.getResultList();
//			if (list != null && list.size() > 0) {
//				item.add(new SelectItem(list.get(0).getId(), list.get(0).getDescripcion()));
//			}
//		}
//		return item;
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<SelectItem> obtenerClasificacionTipoOtros() {
//		List<ClasificacionTipoOtros> list;
//		List<SelectItem> item = new ArrayList<SelectItem>();
//		if (idClasificacionTipoOtrosSelec == null || idClasificacionTipoOtrosSelec == 0) {
//			list = em.createNamedQuery("ClasificacionTipoOtros.findByAll").getResultList();
//			/* item.add(new SelectItem(-1, "<<Seleccionar Tipo>>")); */
//			for (ClasificacionTipoOtros pr : list) {
//				item.add(new SelectItem(pr.getId(), pr.getDescripcion()));
//			}
//		} else {
//			Query query = em.createNamedQuery("ClasificacionTipoOtros.findById");
//			query.setParameter("id", idClasificacionTipoOtrosSelec);
//			list = query.getResultList();
//			if (list != null && list.size() > 0) {
//				item.add(new SelectItem(list.get(0).getId(), list.get(0).getDescripcion()));
//			}
//		}
//		return item;
//	}

	public boolean isDespachable() {
		if (modificarVisaFirmas() || usuarioTieneVisas() || usuarioTieneFirmas()) {
			return true;
		}
		return false;
	}

	private boolean usuarioTieneVisas() {
		if (convenio.getVisaciones() != null && convenio.getVisaciones().size() > 0) {
			List<VisacionDocumento> listVisaciones = convenio.getVisaciones();
			for (VisacionDocumento v : listVisaciones) {
				if (v.getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean usuarioTieneFirmas() {
		if (convenio.getFirmas() != null && convenio.getFirmas().size() > 0) {
			List<FirmaDocumento> listFirmas = convenio.getFirmas();
			for (FirmaDocumento f : listFirmas) {
				if (f.getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isDisabledGuardar() {
		return guardado;
	}

	public boolean getEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getVisado()) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getVisado()) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isEditableSinVisa() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isRenderedBotonDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (convenio != null && convenio.getId() != null) {
			if ((convenio.getFirmasEstructuradas() != null && convenio.getFirmasEstructuradas().size() > 0)
					|| (convenio.getVisacionesEstructuradas() != null && convenio.getVisacionesEstructuradas().size() > 0)) {
				for (VisacionEstructuradaDocumento ve : convenio.getVisacionesEstructuradas()) {
					if (ve.getPersona().equals(usuario)) {
						return false;
					}
				}
				for (FirmaEstructuradaDocumento fe : convenio.getFirmasEstructuradas()) {
					if (fe.getPersona().equals(usuario)) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisar() {
		if (this.convenio.getId() == null) {
			return false;
		}
		return !isRenderedBotonFirmar();
	}

	private boolean isRenderedBotonFirmar() {
		if (!isEditableSinVisa()) {
			return false;
		}
		if (usuario.getId().equals(convenio.getIdEmisor())) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedFirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (convenio != null && convenio.getFirmasEstructuradas() != null && convenio.getFirmasEstructuradas().size() != 0) {
			List<FirmaEstructuradaDocumento> firmas = convenio.getFirmasEstructuradas();
			Collections.sort(firmas);
			if (convenio.getVisacionesEstructuradas() != null && convenio.getVisacionesEstructuradas().size() == 0) {
				if (firmas.get(0).getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isRenderedPlantillaList() {
		if (this.convenio != null) {
			if (this.convenio.getId() != null) {
				return true;
			}
		}
		return false;
	}

	public boolean isRenderedVisar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.convenio != null && this.convenio.getId() != null) {
			if (convenio != null && convenio.getVisacionesEstructuradas() != null && convenio.getVisacionesEstructuradas().size() != 0) {
				List<VisacionEstructuradaDocumento> visas = convenio.getVisacionesEstructuradas();
				Collections.sort(visas);
				if (visas.get(0).getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	public void limpiarAnexos() {
		if (archivosAnexos == null) {
			archivosAnexos = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		} else {
			archivosAnexos.clear();
		}
		materiaArchivo = null;
	}

	public void listener(UploadEvent event) throws Exception {
		ArchivoAdjuntoDocumentoElectronico archivo = new ArchivoAdjuntoDocumentoElectronico();
		UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
		archivosAnexos.add(archivo);
	}

	public boolean modificarVisaFirmas() {
		//TODO MODELO ROLES UCHILE
		/*
		for (Rol r : usuario.getRoles()) {
			if (r.getId().equals(Rol.INGRESAR_VISA_FIRMA_CONVENIO)) {
				return true;
			}
		}*/
		return false;
	}

	public boolean renderedBotonDesvisarDesfirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		return usuario.getId().equals(ID_CARGO_FISCAL) && (convenio.getFirmas() == null || convenio.getFirmas().isEmpty()) && (convenio.getVisaciones() != null && !convenio.getVisaciones().isEmpty());
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public void salirPanel() {
		log.info("SALIR PANEL");
		limpiarAnexos();
	}

	public void setConsiderando(String considerando) {
		List<Parrafo> cons = this.convenio.getConsiderandos();
		cons.get(0).setCuerpo(considerando);
	}

	public void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento) {
		this.distribucionDocumento = distribucionDocumento;
	}

	public void setMateriaArchivo(String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	public void setTeniendoPresente(String teniendoPresente) {
		List<Parrafo> res = this.convenio.getTeniendoPresente();
		res.get(0).setCuerpo(teniendoPresente);
	}

	public void setConvienen(String convienen) {
		List<Parrafo> vis = this.convenio.getConvienen();
		vis.get(0).setCuerpo(convienen);
	}

//	public Integer getIdClasificacionSubTipoOtrosSelec() {
//		if (convenio != null && convenio.getClasificacionSubtipoOtros() != null) {
//			idClasificacionSubTipoOtrosSelec = convenio.getClasificacionSubtipoOtros().getId();
//		}
//		return idClasificacionSubTipoOtrosSelec;
//	}
//
//	public void setIdClasificacionSubTipoOtrosSelec(Integer idClasificacionSubTipoOtrosSelec) {
//		this.idClasificacionSubTipoOtrosSelec = idClasificacionSubTipoOtrosSelec;
//	}
//
//	public Integer getIdClasificacionTipoOtrosSelec() {
//		if (convenio != null && convenio.getClasificacionTipoOtros() != null) {
//			idClasificacionTipoOtrosSelec = convenio.getClasificacionTipoOtros().getId();
//		}
//		return idClasificacionTipoOtrosSelec;
//	}
//
//	public void setIdClasificacionTipoOtrosSelec(Integer idClasificacionTipoOtrosSelec) {
//		this.idClasificacionTipoOtrosSelec = idClasificacionTipoOtrosSelec;
//	}
//
//	public List<SelectItem> getTipos1Otros() {
//		if (tipos1Otros == null) {
//			tipos1Otros = new ArrayList<SelectItem>();
//		}
//		if (tipos1Otros.isEmpty()) {
//			tipos1Otros.addAll(obtenerClasificacionTipoOtros());
//		}
//		return tipos1Otros;
//	}
//
//	public void setTipos1Otros(List<SelectItem> tipos1Otros) {
//		this.tipos1Otros = tipos1Otros;
//	}
//
//	public List<SelectItem> getTipos2Otros() {
//		if (tipos2Otros == null) {
//			tipos2Otros = new ArrayList<SelectItem>();
//		}
//		if (tipos2Otros.isEmpty()) {
//			tipos2Otros.addAll(obtenerClasificacionSubTipoOtros());
//		}
//		return tipos2Otros;
//	}
//
//	public void setTipos2Otros(List<SelectItem> tipos2Otros) {
//		this.tipos2Otros = tipos2Otros;
//	}

	public void verArchivo(Long id) {
		/*Archivo archivo = em.find(Archivo.class, id);
		if (archivo != null && archivo.getCmsId() != null) {
			byte[] data = repositorio.recuperarArchivo(archivo.getCmsId());
			if (data != null) {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				try {
					HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
					response.setContentType(archivo.getContentType());
					response.setHeader("Content-disposition", "attachment; filename=\"" + archivo.getNombreArchivo() + "\"");
					response.getOutputStream().write(data);
					response.getOutputStream().flush();
					facesContext.responseComplete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + archivo.getId() + ")");
			}
		}*/
	    try {
            if (!DocUtils.getFile(id, em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + id + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void visar() {
		boolean permisoVisa = false;
		for (VisacionEstructuradaDocumento ved : convenio.getVisacionesEstructuradas()) {
			if (ved.getPersona().equals(usuario)) {
				permisoVisa = true;
			}
		}
		if (permisoVisa) {
			this.armaDocumento(true, false);
			VisacionEstructuradaDocumento ved = null;
			for (VisacionEstructuradaDocumento ve : convenio.getVisacionesEstructuradas()) {
				if (ve.getPersona().equals(usuario)) {
					ved = ve;
					break;
				}
			}

			Query query = em.createQuery("DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.id = ?");
			query.setParameter(1, ved.getId());
			query.executeUpdate();
			convenio.getVisacionesEstructuradas().remove(ved);

			salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
				if (de.getDocumento().getId().equals(convenio.getId())) {
					for (VisacionEstructuradaDocumento ve : de.getDocumento().getVisacionesEstructuradas()) {
						if (ve.getPersona().equals(usuario)) {
							de.getDocumento().getVisacionesEstructuradas().remove(ve);
							break salida;
						}
					}
				}
			}
			FacesMessages.instance().add("Decreto visado.");
		} else {
			FacesMessages.instance().add("Ud. No puede Visar el Decreto");
		}

		guardado = true;
	}

	public void visualizar() {
		if (!firmado && !visado) {
			armaDocumento(false, false);
		}
	}

	public void plantillaListener(ValueChangeEvent event) {
		Integer id = (Integer) event.getNewValue();
		if (!id.equals(-1)) {
			aplicaPlantilla(id);
		}
	}

	public void plantillaResolucionListener(ValueChangeEvent event) {
		Integer id = (Integer) event.getNewValue();
		if (!id.equals(-1)) {
			this.idPlantillaResolucion = id;
		}
	}

	private void aplicaPlantilla(Integer idPlantilla) {
		Plantilla plantilla;
		try {
			plantilla = (Plantilla) em.createNamedQuery("Plantilla.findById").setParameter("id", idPlantilla).getSingleResult();
		} catch (NoResultException nre) {
			plantilla = null;
		}
		if (plantilla != null) {
			for (CamposPlantilla cpc : plantilla.getCamposPlantilla()) {
				if (cpc.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					String materia = cpc.getContenido();
					this.convenio.setMateria(materia);
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TITULO)) {
					String titulo = cpc.getContenido();
					this.convenio.setTitulo(titulo);
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PROLOGO)) {
					String prologo = cpc.getContenido();
					this.convenio.setPrologo(prologo);
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					if (this.convenio.getConsiderandos() != null && !this.convenio.getConsiderandos().isEmpty()) {
						this.convenio.getConsiderandos().get(0).setCuerpo(cpc.getContenido());
					} else {
						Considerandos parrafo = new Considerandos();
						parrafo.setCuerpo(cpc.getContenido());
						parrafo.setDocumento(this.convenio);
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.convenio.setConsiderandos(parrafos);
					}
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TENIENDO_PRESENTE)) {
					String teniendoPresente = "";
					teniendoPresente = cpc.getContenido();
					if (this.convenio.getTeniendoPresente() != null && !this.convenio.getTeniendoPresente().isEmpty()) {
						this.convenio.getTeniendoPresente().get(0).setCuerpo(teniendoPresente);
					} else {
						TeniendoPresente parrafo = new TeniendoPresente();
						parrafo.setCuerpo(teniendoPresente);
						parrafo.setDocumento(this.convenio);
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.convenio.setTeniendoPresente(parrafos);
					}
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONVIENEN)) {
					String convienen = "";
					convienen = cpc.getContenido();
					if (this.convenio.getConvienen() != null && !this.convenio.getConvienen().isEmpty()) {
						this.convenio.getConvienen().get(0).setCuerpo(convienen);
					} else {
						Convienen parrafo = new Convienen();
						parrafo.setCuerpo(convienen);
						parrafo.setDocumento(this.convenio);
						parrafo.setNumero(1L);
						List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.convenio.setConvienen(parrafos);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerPlantillas() {
		List<Plantilla> plantillas;
		List<SelectItem> plantillasItem = new ArrayList<SelectItem>();
		if (idPlantillaSelec == null || idPlantillaSelec == 0) {
			plantillas = em.createNamedQuery("Plantilla.findByTipoPlantilla").setParameter("tp", TipoPlantilla.CONVENIO).getResultList();
			plantillasItem.add(new SelectItem(-1, "<<Seleccionar Plantilla>>"));
			for (Plantilla pc : plantillas) {
				plantillasItem.add(new SelectItem(pc.getId(), pc.getNombre()));
			}
		} else {
			Query query = em.createNamedQuery("Plantilla.findById");
			query.setParameter("id", idPlantillaSelec);
			plantillas = query.getResultList();
			if (plantillas != null && plantillas.size() > 0) {
				plantillasItem.add(new SelectItem(plantillas.get(0).getId(), plantillas.get(0).getNombre()));
			}
			setearPlantilla(idPlantillaSelec);
		}
		if (this.convenio.getId() == null && idPlantillaSelec != null) {
			agregarDocumento();
		}
		return plantillasItem;
	}

	private void setearPlantilla(Integer idPlantilla) {
		if (!idPlantilla.equals(-1)) {
			aplicaPlantilla(idPlantilla);
		}
	}

	public String cambiar() {
		return "crearConvenio";
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerPlantillasResolucion() {
		List<Plantilla> plantillas;
		List<SelectItem> plantillasItem = new ArrayList<SelectItem>();
		if (idPlantillaSelec == null || idPlantillaSelec == 0) {
			plantillas = em.createNamedQuery("Plantilla.findByTipoPlantilla").setParameter("tp", TipoPlantilla.RESOLUCION_DECRETO).getResultList();
			plantillasItem.add(new SelectItem(-1, "<<Seleccionar Plantilla>>"));
			for (Plantilla pc : plantillas) {
				plantillasItem.add(new SelectItem(pc.getId(), pc.getNombre()));
			}
		} else {
			Query query = em.createNamedQuery("Plantilla.findById");
			query.setParameter("id", idPlantillaSelec);
			plantillas = query.getResultList();
			if (plantillas != null && plantillas.size() > 0) {
				plantillasItem.add(new SelectItem(plantillas.get(0).getId(), plantillas.get(0).getNombre()));
			}
		}
		return plantillasItem;
	}

	public String generarResolucion() {
		return gen.generarResolucion(convenio, idPlantillaResolucion, this.usuario);
	}

	public Integer getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(Integer idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public Integer getIdPlantillaResolucion() {
		return idPlantillaResolucion;
	}

	public void setIdPlantillaResolucion(Integer idPlantillaResolucion) {
		this.idPlantillaResolucion = idPlantillaResolucion;
	}

}
