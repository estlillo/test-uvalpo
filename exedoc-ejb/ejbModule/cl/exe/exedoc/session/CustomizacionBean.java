package cl.exe.exedoc.session;

import javax.ejb.Stateless;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.util.singleton.CustomizacionSingleton;

/**
 * @author Administrator
 *
 */
@Stateless
@Scope(ScopeType.APPLICATION)
@Name(CustomizacionBean.NAME)
public class CustomizacionBean implements Customizacion {
	
	/**
	 * 
	 */
	public static final String NAME = "customizacion";
	
	@Logger
	private Log log;
	
	/**
	 * Constructor.
	 */
	public CustomizacionBean() {
		super();
	}

	@Override
	public String getPropiedad(final String key) {
		String val = null;
		try {
			val = CustomizacionSingleton.getInstance().getPropiedad(key);
		} catch (NullPointerException e) {
			log.info("Archivo no encontrado");
			//val = "America";
		}
		return val;
	}

	@Override
	public Long getDefaultTimeout() {
		return CustomizacionSingleton.getInstance().getDefaultTimeout();
	}
	
	@Override
	public boolean getEnviadorCorreo() {
		return CustomizacionSingleton.getInstance().getEnviadorCorreo();
	}
	
	@Override
	public String getNotificacionEnviadorCorreo() {
		return CustomizacionSingleton.getInstance().getNotificacionEnviadorCorreo();
	}
}
