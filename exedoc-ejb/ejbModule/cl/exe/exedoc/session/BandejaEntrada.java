package cl.exe.exedoc.session;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;

@Local
public interface BandejaEntrada {

	public String begin();

	public String end();

	public void destroy();

	/*
	 * public List<ExpedienteBandejaEntrada> getListExpedientes(); public void
	 * setListExpedientes(List<ExpedienteBandejaEntrada> listExpedientes);
	 */
	public Map<Long, Boolean> getSelectedRecibos();

	public void setSelectedRecibos(Map<Long, Boolean> selectedRecibos);

	public Map<Long, Boolean> getSelectedUnir();

	public void setSelectedUnir(Map<Long, Boolean> selectedUnir);

	public List<ExpedienteBandejaEntrada> getExpedientesUnir();

	public void setExpedientesUnir(List<ExpedienteBandejaEntrada> expedientesUnir);

	public String acusarRecibo();

	public void buscarExpedientes();

	public void setListaUnir(Long id);

	public boolean isRenderedBotonUnir();

	public String unirExpedientes() throws DocumentNotUploadedException;

	public Long getIdExpedientePadre();

	public void setIdExpedientePadre(Long idExpedientePadre);

	public void seleccionarExpedientePadre(Long idExpedientePadre);

	public Map<Long, Boolean> getSelectedPadre();

	public void setSelectedPadre(Map<Long, Boolean> selectedPadre);

	public String obtenerEstilo(ExpedienteBandejaEntrada exp);

	Boolean getDesdeDespacho();

	void setDesdeDespacho(Boolean desdeDespacho);

	void limpiarUnir();

}
