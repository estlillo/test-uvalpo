package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link SeleccionarRevisarBorrador}.
 */
@Stateful
@Name("seleccionarRevisarBorrador")
public class SeleccionarRevisarBorradorBean implements SeleccionarRevisarBorrador {

	private static final String ERROR = "Error: ";
	
	private static final String SELECCIONAR_INICIO = "<<Seleccionar>>";

	private static final String ID = "id";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Documento documento;
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<RevisarEstructuradaDocumento> listRevisarEstructuradaEliminadas;

	@In(required = true)
	private Persona usuario;
	
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	/**
	 * Contructor.
	 */
	public SeleccionarRevisarBorradorBean() {
		super();
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@Override
	@SuppressWarnings({ "unchecked", "unused" })
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiar();
		organizacion = idOrganizacion;
		final Query query = em.createNamedQuery("Organizacion.findById");
		query.setParameter(ID, organizacion);
		final List<Organizacion> organizaciones = query.getResultList();
		if (organizaciones != null && organizaciones.size() == 1) {
			final Organizacion org = organizaciones.get(0);
		}
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento depto = departamentos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}
	private void agregar(Persona p){
		if (documento.getVisacionesEstructuradas() == null) {
			documento.setRevisarEstructuradas(new ArrayList<RevisarEstructuradaDocumento>());
		}
		if (this.esFirmante(p)) {
			FacesMessages.instance().add("No puede visar si es firmante");
			return;
		}
		if (!this.contiene(p)) {
			final RevisarEstructuradaDocumento red = new RevisarEstructuradaDocumento();
			red.setOrden(documento.getRevisarEstructuradas().size() + 1);
			red.setPersona(p);
			red.setDocumento(documento);
			documento.getRevisarEstructuradas().add(red);
            this.restablecerOrden(red.getOrden());
            limpiar();
//			listOrganizacion.clear();
//			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//            this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//            this.buscarUnidadesOrganizacionales();
		} else {
			FacesMessages.instance().add("No se puede revisar más de una vez");
			return;
		}
	}
	@Override
	public void agregarPorId(Long id) {
		try {
			final Persona p = (Persona) em.createNamedQuery("Persona.findById").setParameter(ID, id)
					.getSingleResult();
			agregar(p);
		} catch (PersistenceException e) {
			log.info(ERROR, e);
		} catch (NullPointerException e) {
			log.info(ERROR, e);
		}
	}
	@Override
	public void agregar() {
		try {
			if (!persona.equals(JerarquiasLocal.INICIO)) {
				final Persona p = (Persona) em.createNamedQuery("Persona.findById").setParameter(ID, this.persona)
						.getSingleResult();
				agregar(p);
			}
		} catch (PersistenceException e) {
			log.info(ERROR, e);
		} catch (NullPointerException e) {
			log.info(ERROR, e);
		}
	}

	@Override
	public void eliminar(final Integer orden) {
		int contador = 0;
		for (RevisarEstructuradaDocumento red : documento.getRevisarEstructuradas()) {
			try {
				if (red.getOrden().equals(orden)) {
					RevisarEstructuradaDocumento r = new RevisarEstructuradaDocumento();
					r = red;
					if (listRevisarEstructuradaEliminadas == null)
						listRevisarEstructuradaEliminadas = new ArrayList<RevisarEstructuradaDocumento>();
					listRevisarEstructuradaEliminadas.add(r);
					documento.getRevisarEstructuradas().remove(contador);
					this.restablecerOrden(orden);
					break;
				}
			} catch (IndexOutOfBoundsException e) {
				log.error("Error: ", e);
			} catch (NullPointerException e) {
				log.error("Error: ", e);
			}
			contador++;
		}
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @param p {@link Persona}
	 * @return {@link Boolean}
	 */
	private boolean contiene(final Persona p) {
		final List<RevisarEstructuradaDocumento> listRED = documento.getRevisarEstructuradas();
		for (RevisarEstructuradaDocumento red : listRED) {
			if (red.getPersona().getId().equals(p.getId())) { return true; }
		}
		return false;
	}

	/**
	 * @param p {@link Persona}
	 * @return {@link Boolean}
	 */
	private boolean esFirmante(final Persona p) {
		final List<FirmaEstructuradaDocumento> listFED = documento.getFirmasEstructuradas();
		for (FirmaEstructuradaDocumento fed : listFED) {
			if (fed.getPersona().getId().equals(p.getId())) { return true; }
		}
		return false;
	}

	/**
	 * 
	 */
	private void limpiar() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @param orden {@link Integer}
	 */
	private void restablecerOrden(final Integer orden) {
		int contador = 1;
		for (RevisarEstructuradaDocumento red : documento.getRevisarEstructuradas()) {
			if (!red.getOrden().equals(contador)) {
				red.setOrden(contador);
			}
			contador++;
		}
	}

	@Override
	public JerarquiasLocal getJerarquias() {
		return jerarquias;
	}

	@Override
	public void setJerarquias(final JerarquiasLocal jerarquias) {
		this.jerarquias = jerarquias;
	}

	@Override
	public Documento getDocumento() {
		return documento;
	}

	@Override
	public void setDocumento(final Documento documento) {
		this.documento = documento;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		//this.getListOrganizacion();
		//this.getListDivision();
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public void setListUnidadesOrganizacionales(final List<SelectItem> listUnidadesOrganizacionales) {
		this.listUnidadesOrganizacionales = listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public void setListCargos(final List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<RevisarEstructuradaDocumento> getRevisar() {
		return documento.getRevisarEstructuradas();
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			listOrganizacion.addAll(this.buscarOrganizaciones());
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//            this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//            this.buscarUnidadesOrganizacionales();
		}
		return listOrganizacion;
	}
}
