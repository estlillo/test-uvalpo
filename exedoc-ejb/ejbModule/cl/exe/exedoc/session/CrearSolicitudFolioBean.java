package cl.exe.exedoc.session;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.config.Roles;
import cl.exe.exedoc.config.Roles.PropiedadesRoles;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.SolicitudFolio;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.util.FechaUtil;

/**
 * class {@link CrearSolicitudFolioBean}, utilizada para crear documento Solicitid de folio para una Resolucion.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(CrearSolicitudFolioBean.NAME)
@Scope(ScopeType.CONVERSATION)
public class CrearSolicitudFolioBean implements CrearSolicitudFolio, Serializable {

	/**
	 * NAME nombre de la clase.
	 */
	public static final String NAME = "crearSolicitudFolio";

	private static final Locale LOCATE = FechaUtil.LOCALE;

	private static final long serialVersionUID = -5274960467790194897L;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	@Out(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	private SolicitudFolio solicitudFolio;

	private Boolean flagFoliador = false;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	@In(required = false)
	private String homeCrear;

	private Boolean loaded = Boolean.TRUE;

	@Logger
	private Log log;

	private Boolean visado;

	private Boolean firmado;

	/**
	 * Constructor.
	 */
	public CrearSolicitudFolioBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("Iniciando converzacion : {0}", CrearSolicitudFolioBean.NAME);
		this.loaded = Boolean.FALSE;
		this.limpiar();

		List<Rol> roles = this.usuario.getRoles();
		PropiedadesRoles r = Roles.PropiedadesRoles.ROL_FOLIADOR;
		String rol = r.name();
		for (int i = 0; i < roles.size(); i++) {
			Rol array_element = roles.get(i);
			if (rol.equalsIgnoreCase(array_element.getDescripcion())) {
				flagFoliador = true;
			}
		}

		return CrearSolicitudFolioBean.NAME;
	}

	@Override
	public String end() {
		this.limpiar();
		this.loaded = Boolean.TRUE;
		log.info("Finalizando converzacion : {0}", CrearSolicitudFolioBean.NAME);
		return homeCrear;
	}

	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public String limpiarFormulario() {
		log.info("Limpiar formulario solicitud folio");
		this.limpiar();
		return CrearSolicitudFolioBean.NAME;
	}

	/**
	 * Limpia e inicializa datos para el formulario.
	 */
	private void limpiar() {
		// solicitudFolio = new SolicitudFolio();
		// em.refresh(usuario);
		log.info("usuario : ", usuario.toString());
		final Persona persona = em.find(Persona.class, usuario.getId());
		log.info("persona : ", persona.toString());
		solicitudFolio.setFechaSolicitud(new Date());
		solicitudFolio.setSolicitante(persona);
		solicitudFolio.setDivision(persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision());
	}

	@Override
	public Boolean isEditable() {
		Boolean isEditable = Boolean.TRUE;

		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return false; }
		if (this.getVisado()) {
			isEditable = Boolean.FALSE;
		}
		if (this.getFirmado()) {
			isEditable = Boolean.FALSE;
		}

		return isEditable;
	}

	@Override
	public Boolean getVisado() {
		if (this.solicitudFolio != null && this.solicitudFolio.getVisaciones() != null
				&& this.solicitudFolio.getVisaciones().size() != 0) {
			visado = Boolean.TRUE;
		} else {
			visado = Boolean.FALSE;
		}
		return visado;
	}

	@Override
	public Boolean getFirmado() {
		if (this.solicitudFolio != null && this.solicitudFolio.getFirmas() != null
				&& this.solicitudFolio.getFirmas().size() != 0) {
			firmado = Boolean.TRUE;
		} else {
			firmado = Boolean.FALSE;
		}
		return firmado;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String guardarSolicitud() {
		final int cont = listDocumentosRespuesta.size() + 1;
		this.solicitudFolio.setNumeroDocumento(this.documentoOriginal.getNumeroNumeroDocumento());
		this.solicitudFolio.setAutor(usuario);
		this.solicitudFolio.setFechaCreacion(new Date());
		this.solicitudFolio.setEnEdicion(false);
		this.solicitudFolio.setReservado(false);
		this.solicitudFolio.setEliminable(false);
		this.solicitudFolio.setEmisor(usuario.getNombreApellido());
		this.solicitudFolio.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		this.solicitudFolio.setTipoDocumento(new TipoDocumento(TipoDocumento.SOLICITUD_FOLIO));
		// consulta por el folio
		final String q = "SELECT d FROM Documento d WHERE d.numeroAsignado = :id";

		final Query query = em.createQuery(q).setParameter("id", this.solicitudFolio.getNumeroAsignado());
		final List<Documento> docs = (List<Documento>) query.getResultList();
		if (!docs.isEmpty()) {
			FacesMessages.instance().add(
					"El codigo del folio debe ser Unico , el que usted ingreso ya existe en la base de datos");
			return "";
		}

		this.solicitudFolio.setIdNuevoDocumento(cont);
		if (this.solicitudFolio.getId() != null) {
			this.solicitudFolio.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
			em.merge(this.solicitudFolio);
		} else {
			this.solicitudFolio.setEstado(new EstadoDocumento(EstadoDocumento.CREADO));

			em.persist(this.solicitudFolio);
			final Long codigo = solicitudFolio.getId();
			solicitudFolio.setCodigoBarra(codigo);
			this.actualizarDocumentoExpediente();
		}

		return this.end();
	}

	/**
	 * Actualizar documento de respuesta del expediente actual, permite agregar mas documentos de respuestas el
	 * expediente.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void actualizarDocumentoExpediente() {

		final DocumentoExpediente de = new DocumentoExpediente();

		de.setDocumento(this.solicitudFolio);
		de.setExpediente(this.expediente);
		de.setEnEdicion(false);
		de.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));

		em.persist(de);

		de.getDocumento().setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
		if (de.getDocumento().getEliminado() == null || !de.getDocumento().getEliminado()) {
			this.listDocumentosRespuesta.add(de.getDocumento());
		}
		de.getDocumento().setEliminable(true);
	}

	@Override
	public SolicitudFolio getSolicitudFolio() {
		return solicitudFolio;
	}

	@Override
	public void setSolicitudFolio(final SolicitudFolio solicitudFolio) {
		this.solicitudFolio = solicitudFolio;
	}

	@Override
	public Boolean getLoaded() {
		return loaded;
	}

	@Override
	public void setLoaded(final Boolean loaded) {
		this.loaded = loaded;
	}

	@Override
	public Locale getLocate() {
		return LOCATE;
	}

	public Boolean getFlagFoliador() {
		return flagFoliador;
	}

	public void setFlagFoliador(Boolean flagFoliador) {
		this.flagFoliador = flagFoliador;
	}

}
