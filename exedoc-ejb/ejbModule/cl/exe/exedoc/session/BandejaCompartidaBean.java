package cl.exe.exedoc.session;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaCompartida;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;

@Stateful
@Scope(ScopeType.SESSION)
@Name("bandejaCompartida")
public class BandejaCompartidaBean implements BandejaCompartida {

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManejarExpedienteInterface me;

	private Map<Long, Boolean> selectedRecibos;

	//private Map<Long, Boolean> selectedUnir;
	//private List<ExpedienteBandejaCompartida> expedientesUnir;
	//private Map<Long, Boolean> selectedPadre;

	@In(required = false, scope = ScopeType.SESSION)
	@Out(scope = ScopeType.SESSION)
	private BandejaCompartidaData dataCompartida;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private Long idExpedientePadre;
	
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;

	private String BANDEJA_COMPARTIDA = "bandejaCompartida";
	public BandejaCompartidaData getDataCompartida() {
		return dataCompartida;
	}

	public void buscarExpedientes() {
		log.info("Buscando expedientes de: " + usuario.getId());
		List<ExpedienteBandejaCompartida> lista;
		lista = me.listarExpedienteBandejaCompartidaUsuario(usuario);
		dataCompartida = new BandejaCompartidaData(lista);
		desdeDespacho = Boolean.FALSE;
	}

	@Begin(join = true)
	public String begin() {
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja Compartida", "inicio", new Date());
		homeCrear = "bandejaCompartida";
		log.info("beginning conversation: bandejaCompartida");
		selectedRecibos = new HashMap<Long, Boolean>();
		//selectedUnir = new HashMap<Long, Boolean>();
		//expedientesUnir = new ArrayList<ExpedienteBandejaCompartida>();
		//selectedUnir.clear();
		this.buscarExpedientes();
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja Compartida", "fin", new Date());
		return BANDEJA_COMPARTIDA;
	}

	public String acusarRecibo() {
		log.info("acusar recibo ...");
		long ini = System.currentTimeMillis();
		Iterator<Long> iter = selectedRecibos.keySet().iterator();
		boolean accionAcusaRecibo = false;
		
		//acuso de recibo ok
		String msjAcuseRecibo = "Los siguientes Expedientes se movieron exitosamente a su Bandeja de Entrada : ";
		boolean flagAcuse = false;
		//eliminados o reasignados
		String msjEliminado = "Existen Expedientes retirados de su Bandeja";
		boolean flagEliminado = false;
		//acuso de recibo previo
		String msjAcusePrevio = "Los siguientes Expedientes ya fueron acusados de recibo : ";
		boolean flagAcusePrevio = false;
		
		while (iter.hasNext()) {
			Long id = iter.next();
			if ((Boolean) selectedRecibos.get(id)) {
				accionAcusaRecibo = true;
				Expediente expediente = me.buscarExpediente(id);
				if (expediente == null || 
						(expediente != null && expediente.getCancelado() != null) || 
						(expediente != null && expediente.getReasignado() != null)) {
					if(!flagEliminado) {
						if (expediente != null) {
							if (!msjEliminado.substring(msjEliminado.length()-1).equals(" ")) {
								msjEliminado += " : " + expediente.getNumeroExpediente();
							} else {
								if (!msjEliminado.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
									msjEliminado += " - " + expediente.getNumeroExpediente();
								}
							}
						}
					}
					flagEliminado = true;
				} else {
					if (!this.verificarAcuse(expediente)) {
						if(!flagAcusePrevio) {
							msjAcusePrevio += expediente.getNumeroExpediente();
						} else {
							if (!msjAcusePrevio.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
								msjAcusePrevio = msjAcusePrevio + " - " + expediente.getNumeroExpediente();
							}
						}
						flagAcusePrevio = true;
					} else {
						if(!flagAcuse){
							msjAcuseRecibo += expediente.getNumeroExpediente();
						} else {
							if (!msjAcuseRecibo.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
								msjAcuseRecibo = msjAcuseRecibo + " - " + expediente.getNumeroExpediente();
							}
						}
						flagAcuse = true;
						me.acusarReciboExpediente(expediente, usuario, true);
						dataCompartida.acusarRecibo(expediente.getId());
					}
				}
			}
		}
		
		if(flagAcuse){
			FacesMessages.instance().add(msjAcuseRecibo);
		}
		if(flagAcusePrevio){
			FacesMessages.instance().add(msjAcusePrevio);
		}
		if(flagEliminado){
			FacesMessages.instance().add(msjEliminado);
		}
		if (!accionAcusaRecibo) {
			FacesMessages.instance().add("Debe seleccionar al menos un Expediente.");
		} else {
			selectedRecibos.clear();
			//selectedUnir.clear();				
			this.buscarExpedientes();
		}
		long fin = System.currentTimeMillis();
		log.info("acuso recibo en #0 milisegundos", (fin - ini));
		
		return BANDEJA_COMPARTIDA;
	}

//	public void setListaUnir(Long id) {
//		limpiarListaUnir(selectedUnir);
//		if (selectedUnir.containsKey(id)) {
//			expedientesUnir.remove(getExpedienteDesdeLista(id));
//			selectedUnir.remove(id);
//		} else {
//			expedientesUnir.add(getExpedienteDesdeLista(id));
//			selectedUnir.put(id, true);
//		}
//	}

	/*
	 * Permite limpiar la lista, dado que la utilidad de ordenamiento de columnas de RichFaces y el Actualizar agrega
	 * elementos a dicha lista con valor false, lo que provocaba inconsistencia. Input: Map<Long, Boolean>
	 */
	private void limpiarListaUnir(Map<Long, Boolean> mapLong) {
		Set<Long> b = new HashSet<Long>();
		for (Long id : mapLong.keySet()) {
			if (mapLong.get(id) == false) {
				b.add(id);
			}
		}
		for (Long id : b) {
			mapLong.remove(id);
		}
	}

//	public boolean isRenderedBotonUnir() {
//		return expedientesUnir.size() >= 2;
//	}

	private ExpedienteBandejaCompartida getExpedienteDesdeLista(Long idExpediente) {
		for (ExpedienteBandejaCompartida exp : dataCompartida.getListExpedientes()) {
			if (exp.getId().equals(idExpediente)) { return exp; }
		}
		return null;
	}

//	public String unirExpedientes() {
//		/*
//		 * Se limpia lista ya que trae elementos "false" que corresponden a basura.
//		 */
//		limpiarListaUnir(selectedUnir);
//		// obtener expediente padre
//		Expediente expPadre = me.buscarExpediente(idExpedientePadre);
//		if (expPadre.getAcuseRecibo() == null || expPadre.getAcuseRecibo() == false) {
//			me.acusarReciboExpediente(expPadre, usuario);
//			dataCompartida.acusarRecibo(expPadre.getId());
//		}
//		selectedUnir.remove(idExpedientePadre);
//
//		// copiar documentos de los otros expedientes a expediente padre
//		for (Long idExp : selectedUnir.keySet()) {
//			if (!idExp.equals(idExpedientePadre)) {
//				Expediente exp = me.buscarExpediente(idExp);
//				if (exp.getAcuseRecibo() == null || exp.getAcuseRecibo() == false) {
//					me.acusarReciboExpediente(exp, usuario);
//					dataCompartida.acusarRecibo(exp.getId());
//				}
//				for (DocumentoExpediente docExp : exp.getDocumentos()) {
//					if (docExp.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)
//							|| docExp.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
//						docExp.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
//						try {
//							me.agregarRespuestaAExpediente(expPadre, docExp.getDocumento(), false);
//						} catch (DocumentNotUploadedException e) {
//							e.printStackTrace();
//						}
//					} /*
//					 * else if (docExp.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
//					 * me.agregarAnexoAExpediente(expPadre, docExp.getDocumento(), false,
//					 * docExp.getIdDocumentoReferencia()); }
//					 */
//				}
//				// agregar observaciones de los otros expedientes a expediente
//				// padre
//				guardarObservaciones(expPadre, exp);
//
//				// archivar los otros expedientes
//				me.archivarExpediente(exp);
//			}
//		}
//		em.merge(expPadre);
//
//		selectedUnir.clear();
//		selectedPadre.clear();
//		expedientesUnir.clear();
//
//		FacesMessages.instance().add("Expedientes unidos exitosamente");
//
//		return end();
//	}
//
//	private void guardarObservaciones(Expediente expediente, Expediente expedienteOriginal) {
//		List<Observacion> observaciones = new ArrayList<Observacion>();
//		for (Observacion obs : expedienteOriginal.getObservaciones()) {
//			Observacion o = new Observacion();
//			o.setObservacion(obs.getObservacion());
//			if (!observaciones.contains(o)) {
//				o.setExpediente(expediente);
//				o.setFecha(obs.getFecha());
//				o.setAutor(obs.getAutor());
//				em.persist(o);
//				observaciones.add(o);
//			}
//		}
//		Observacion obsUnion = new Observacion();
//		obsUnion.setObservacion("EXPEDIENTE FUSIONADO CON " + expedienteOriginal.getNumeroExpediente());
//		obsUnion.setExpediente(expediente);
//		obsUnion.setFecha(new Date());
//		obsUnion.setAutor(usuario);
//		em.persist(obsUnion);
//		observaciones.add(obsUnion);
//
//		if (expediente.getObservaciones() == null) {
//			expediente.setObservaciones(observaciones);
//		} else {
//			expediente.getObservaciones().addAll(observaciones);
//		}
//		em.merge(expediente);
//	}
//
//	public void seleccionarExpedientePadre(Long idExpedientePadre) {
//		if (selectedPadre.get(idExpedientePadre).equals(false)) {
//			this.idExpedientePadre = null;
//		} else {
//			this.idExpedientePadre = idExpedientePadre;
//			for (Long idExpedienteUnir : selectedPadre.keySet()) {
//				if (!idExpedienteUnir.equals(idExpedientePadre)) {
//					selectedPadre.put(idExpedienteUnir, false);
//				}
//			}
//		}
//	}

	@End
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Map<Long, Boolean> getSelectedRecibos() {
		return selectedRecibos;
	}

	public void setSelectedRecibos(Map<Long, Boolean> selectedRecibos) {
		this.selectedRecibos = selectedRecibos;
	}

//	public Map<Long, Boolean> getSelectedUnir() {
//		return selectedUnir;
//	}
//
//	@Override
//	public void setSelectedUnir(Map<Long, Boolean> selectedUnir) {
//		this.selectedUnir = selectedUnir;
//	}
//
//	public List<ExpedienteBandejaCompartida> getExpedientesUnir() {
//		return expedientesUnir;
//	}
//
//	public void setExpedientesUnir(List<ExpedienteBandejaCompartida> expedientesUnir) {
//		this.expedientesUnir = expedientesUnir;
//	}

	public Long getIdExpedientePadre() {
		return idExpedientePadre;
	}

	public void setIdExpedientePadre(Long idExpedientePadre) {
		this.idExpedientePadre = idExpedientePadre;
	}

//	public Map<Long, Boolean> getSelectedPadre() {
//		if (selectedPadre == null) {
//			selectedPadre = inicializarSelectedPadre();
//		}
//		return selectedPadre;
//	}
//
//	public void setSelectedPadre(Map<Long, Boolean> selectedPadre) {
//		this.selectedPadre = selectedPadre;
//	}

	public String obtenerEstilo(ExpedienteBandejaCompartida exp) {
		String estilo = "";

		if (exp.getTipoDocumento() != null && (exp.getTipoDocumento().equals("Consulta Ciudadana") || exp.getTipoDocumento().equals("Consulta"))) {
			estilo = "consulta";
		}

		if (exp.getTipoDocumento() != null && (exp.getTipoDocumento().equals("Respuesta Consulta Ciudadana")
				|| exp.getTipoDocumento().equals("Respuesta Consulta"))) {
			estilo = "respuesta";
		}

		if (dataCompartida.expRepetidos(exp)) {
			estilo = "repetidos";
		}

		return estilo;
	}

//	private Map<Long, Boolean> inicializarSelectedPadre() {
//		Map<Long, Boolean> map = new HashMap<Long, Boolean>();
//
//		for (ExpedienteBandejaCompartida ebe : expedientesUnir) {
//			map.put(ebe.getId(), false);
//		}
//
//		return map;
//	}
	
	/**
	 * Metodo que verifica si expediente grupal ya ha sido tomado.
	 * @param expediente
	 * @return
	 */
	private boolean verificarAcuse(Expediente expediente) {
		final Query query = em.createQuery("select e.grupo.id from Expediente e where e.id = :id");
		query.setParameter("id", expediente.getId());
		final Long o = (Long) query.getSingleResult(); 
		return o != null;
	}

	@Override
	public Boolean getDesdeDespacho() {
		if (desdeDespacho == null) {
			desdeDespacho = Boolean.FALSE;
		}
		return desdeDespacho;
	}

	@Override
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}
}
