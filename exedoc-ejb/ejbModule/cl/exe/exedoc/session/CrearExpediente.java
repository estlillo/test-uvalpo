package cl.exe.exedoc.session;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoBinario;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Administrator
 *
 */
@Local
public interface CrearExpediente {

	/**
	 * 
	 */
	void distribuirDocumentos();

	/**
	 * @return {@link String}
	 */
	String begin();

	/**
	 * @return {@link String}
	 */
	String end();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return {@link boolean}
	 */
	boolean getGuardarExpediente();

	/**
	 * @return {@link String}
	 */
	String getFormato();

	/**
	 * @param formato {@link String}
	 */
	void setFormato(String formato);

	/**
	 * @return {@link List}<{@link SelectPersonas}>
	 */
	List<SelectPersonas> getDestinatariosDocumento();

	/**
	 * @return {@link List}<{@link SelectPersonas}>
	 */
	List<SelectPersonas> getDistribucionDocumento();

	/**
	 * @return {@link Date}
	 */
	Date getFechaIngreso();

	/**
	 * @param fechaIngreso {@link Date}
	 */
	void setFechaIngreso(Date fechaIngreso);

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	List<SelectItem> getListTipoDocumentos();

	/**
	 * @return {@link Integer}
	 */
	Integer getTipoDocumentoID();

	/**
	 * @param tipoDocumentoID {@link Integer}
	 */
	void setTipoDocumentoID(Integer tipoDocumentoID);

	/**
	 * @return {@link Integer}
	 */
	Integer getTipoDocumentoAdjuntar();

	/**
	 * @param tipoDocumentoAdjuntar {@link Integer}
	 */
	void setTipoDocumentoAdjuntar(Integer tipoDocumentoAdjuntar);

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	List<SelectItem> getListaDocumentosAdjuntar();

	/**
	 * @return {@link String}
	 */ 
	String adjuntarDocumentoElectronico();

	/**
	 * @param numDoc {@link Integer}
	 */
	void eliminarDocumento(Integer numDoc);

	/**
	 * @return {@link ArchivoDocumentoBinario}
	 */
	ArchivoDocumentoBinario getArchivo();

	/**
	 * @param archivo {@link ArchivoDocumentoBinario}
	 */
	void setArchivo(ArchivoDocumentoBinario archivo);

	/**
	 * @param event {@link UploadEvent}
	 * @throws Exception 
	 */
	void listener(UploadEvent event) throws Exception;

	/**
	 * @param event {@link ValueChangeEvent}
	 */
	void valueChanged(ValueChangeEvent event);

	/**
	 * @return {@link DocumentoPapel}
	 */
	DocumentoPapel getDocumentoPapel();

	/**
	 * @param documentoPapel {@link DocumentoPapel}
	 */
	void setDocumentoPapel(DocumentoPapel documentoPapel);

	/**
	 * @return {@link DocumentoBinario}
	 */
	DocumentoBinario getDocumentoBinario();

	/**
	 * @param documentoBinario {@link DocumentoBinario}
	 */
	void setDocumentoBinario(DocumentoBinario documentoBinario);

	/**
	 * 
	 */
	void agregarDocumentoPapel();

	/**
	 * Agregar documento binario al expediente.
	 */
	void agregarDocumentoBinario();

	/**
	 * @return {@link List}<{@link Documento}> 
	 */
	List<Documento> getListDocumentosRespuesta();

	/**
	 * @return {@link boolean}
	 */
	boolean getEsFactura();

	/**
	 * @return {@link Long}
	 */
	Long getMontoFactura();

	/**
	 * @param montoFactura {@link Long}
	 */
	void setMontoFactura(Long montoFactura);

	/**
	 * @param event {@link ValueChangeEvent}
	 */
	void verificaTipoDocumento(ValueChangeEvent event);

	/**
	 * @return {@link String}
	 */
	String despacharExpediente();

	/**
	 * 
	 */
	void guardarYCrearExpediente();

	/**
	 * @param docBin {@link DocumentoBinario}
	 */
	void validarContraFechaDoc(DocumentoBinario docBin);

	/**
	 * @param event {@link UploadEvent}
	 * @throws Exception 
	 */
	void listenerAntecedentes(UploadEvent event) throws Exception;

	/**
	 * 
	 */
	void agregarAntecedentes();

	/**
	 * @param idArchivo {@link Long}
	 */
	void verArchivoAntecedente(Long idArchivo);

	/**
	 * @param idNuevoArchivo {@link Integer}
	 */
	void eliminarArchivoAntecedente(Integer idNuevoArchivo);

	/**
	 * @return {@link String}
	 */
	List<ArchivoAdjuntoDocumentoBinario> getListaArchivosAntecedente();

	/**
	 * @return {@link List}<{@link ArchivoAdjuntoDocumentoBinario}>
	 */
	String getMateriaArchivo();

	/**
	 * @param materiaArchivo {@link String}
	 */
	void setMateriaArchivo(String materiaArchivo);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getAgregarDestinatarioDocumento();

	/**
	 * @param agregarDestinatarioDocumento {@link Boolean}
	 */
	void setAgregarDestinatarioDocumento(final Boolean agregarDestinatarioDocumento);

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	List<SelectItem> getListDocumentosElectronicos();

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	List<SelectItem> getListDocumentosNoElectronicos();

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	List<SelectItem> getNivelesUrgencia();

	/**
	 * @param nivelesUrgencia {@link List}<{@link SelectItem}>
	 */
	void setNivelesUrgencia(List<SelectItem> nivelesUrgencia);

	/**
	 * @return {@link Long}
	 */
	Long getIdAlerta();

	/**
	 * @param idAlerta {@link Long}
	 */
	void setIdAlerta(Long idAlerta);

	/**
	 * @param validarComplete {@link Boolean}
	 */
	void setValidarComplete(boolean validarComplete);

	/**
	 * @return {@link Boolean}
	 */
	boolean isValidarComplete();

	/**
	 * @param List<String>
	 */
	List<Persona> autocomplete(Object suggest);
	
	/**
	 * @param persona
	 */
	void seleccionarDest(Persona p);
	/**
	 * @param event
	 */
	void validarTexto(ValueChangeEvent event);
	/**
	 * @return destinatario
	 */
	String getDestinatario();
	/**
	 * @param String
	 */
	void setDestinatario(String destinatario);

	void listenerQuitar();

	void clearUploadData();

	void clearUploadAntecedente();

	ArchivoAdjuntoDocumentoBinario getArchivoAdjuntoTemporal();

	void setArchivoAdjuntoTemporal(ArchivoAdjuntoDocumentoBinario doc);

	
}
