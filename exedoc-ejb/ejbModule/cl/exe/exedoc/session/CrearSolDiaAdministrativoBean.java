package cl.exe.exedoc.session;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DiaAdministrativo;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.SolicitudDiaAdministrativo;
import cl.exe.exedoc.entity.SolicitudVacaciones;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.Vacaciones;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.utils.InformacionPersonal;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.externos.DatosDiasAdministrativosManager;

@Stateful
@Name("crearSolDiaAdministrativo")
public class CrearSolDiaAdministrativoBean implements CrearSolDiaAdministrativo {

	@EJB
	private DatosDiasAdministrativosManager datosDiasAdministrativos; //dadmDAO;
	
	@In(required = false, value = "esSolicitud", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "esSolicitud", scope = ScopeType.CONVERSATION)
	private Boolean esSolicitud;
	
	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	private InformacionPersonal informacionPersonal;

	@In(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;

	private Persona jefatura;

	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	private List<DetalleDias> diasSolicitados;
	
	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	private List<DetalleDias> diasSolicitadosEliminados;
	
	private InformacionPersonalSrv informacionPersonalSrv;

	private DetalleDias detalleDias;

	private String periodoDia = "";

	private Date fechaDia = null;

	private Persona funcionario;
	private String nombreFuncionario;
	private Double feriadosLegalesPendientes;
	private boolean diasOk;
	private String tipoDia;
	
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private JerarquiasLocal jerarquias;
	
//	@EJB
//	private Authenticator auth;

	@In(required = false)
	private Customizacion customizacion;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private DiaAdministrativo diaAdministrativo;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private String parrafo;

	private Boolean firmado;
	private Boolean visado;
	private Boolean completado;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	private List<Long> idExpedientes;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;
	
	private boolean flagInformacion = false;

	@SuppressWarnings("unchecked")
	private boolean diaYaPedidoVacaciones(Date dia) {
		if (usuario.getId() != null) {
			Query query = em.createNamedQuery("SolicitudVacaciones.findActiveSolicitud");
			query.setParameter("solicitante", usuario);
			List<SolicitudVacaciones> solicitudes = query.getResultList();
			for (SolicitudVacaciones solVac : solicitudes) {
				//Vacaciones vac = (Vacaciones) em.createQuery("SELECT v FROM Vacaciones v WHERE v.idSolicitudVacaciones = :idSolVac").setParameter("idSolVac", solVac.getId()).getSingleResult();
				Vacaciones vac = solVac.getVacaciones();
				if (vac != null && 
						vac.getFechaInicioVacaciones() != null && 
						vac.getFechaTerminoVacaciones() != null) {
					Date desde = vac.getFechaInicioVacaciones();
					Date hasta = vac.getFechaTerminoVacaciones();
					if ((dia.equals(desde) || dia.after(desde)) && (dia.equals(hasta) || dia.before(hasta))) {
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	public void agregarDia() {
		this.detalleDias = new DetalleDias();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date hoy = null;
		try {
			hoy = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (periodoDia != null && this.fechaDia != null) {
			if (periodoDia.equals("1")) {
				detalleDias.setParteDia(1);
			} else if (periodoDia.equals("2")) {
				detalleDias.setParteDia(2);
			} else if (periodoDia.equals("3")) {
				detalleDias.setParteDia(3);
			}
			detalleDias.setFechaDia(this.fechaDia);
			detalleDias.setDiaAdministrativo(diaAdministrativo);
			if (diasSolicitados == null) {
				diasSolicitados = new ArrayList<DetalleDias>();
			}

			boolean insertado = false;
			boolean noCalifica = false;
			//List<DetalleDias> listAux = new ArrayList<DetalleDias>();
			//listAux.addAll(diasSolicitados);
			int cont = 0;
			if (!diaYaPedidoVacaciones(fechaDia)) {
				if (datosDiasAdministrativos.esDiaHabil(this.fechaDia)) {
					if (diasSolicitados.size() < 12) {
						if (diasSolicitados.size() > 0) {
							for (DetalleDias dd : diasSolicitados) {
								int index = ComparatorUtils.detalleDiasComparator.compare(dd, detalleDias);
								if (index == 1) {
									diasSolicitados.add(detalleDias);
									insertado = true;
									break;
								} else {
									if (index == -1) {
										noCalifica = true;
										break;
									}
								}
								cont++;
							}
							if (noCalifica) {
								FacesMessages.instance().add("Ya existe un elemento para ese día.");
								return;
							}
						} else {
							diasSolicitados.add(detalleDias);
							insertado = true;
						}
						if (!insertado && !noCalifica) {
							diasSolicitados.add(detalleDias);
						}
					} else {
						FacesMessages.instance().add("Ya ha completado el máximo de elementos a solicitar.");
						return;
					}
				} else {
					FacesMessages.instance().add("Este no es un día hábil.");
					return;
				}
			} else {
				FacesMessages.instance().add("Ya se han pedido vacaciones que contienen este día.");
				return;
			}
			limpiarDiasSolicitados();
			Collections.sort(this.diasSolicitados, ComparatorUtils.detalleDiasComparator);
			//diasSolicitados = listAux;
		} else {
			FacesMessages.instance().add("Debe ingresar Fecha y período válidos");
			// La fecha no puede ser mas antigua que hoy
			return;
		}
	}

	public void eliminarDia(Date fecha, Integer parteDia) {
		if (diasSolicitadosEliminados == null)
			diasSolicitadosEliminados = new ArrayList<DetalleDias>();
		for (DetalleDias dd : diasSolicitados) {
			if (dd.getFechaDia().equals(fecha) && dd.getParteDia().equals(parteDia)) {
				diasSolicitados.remove(dd);
				if (dd.getId() != null)
					diasSolicitadosEliminados.add(dd);
				break;
			}
		}
	}



	public String agregarDocumento() {
		boolean estabaRechazado = isRenderedMensajeRechazado();
		if (validarSolicitud(diaAdministrativo.getSolicitudDiaAdministrativo())) {
			armaDocumento(false, false);
			diasOk = true;
			if (estabaRechazado || expediente == null || expediente.getId() == null) {
				return despacharExpediente();
			}
			return end();
		}
		return "";
	}

	private SolicitudDiaAdministrativo crearSolicitudDiaAdministrativo() {
		SolicitudDiaAdministrativo solicitud = new SolicitudDiaAdministrativo();
		solicitud.setCantMediosDiasAdmSolicitados(diaAdministrativo.getCantMediosDiasSolicitados());
		solicitud.setSolicitante(diaAdministrativo.getFuncionario());
		solicitud.setDiaAdministrativo(diaAdministrativo.getDiaAdministrativo());
		solicitud.setFeriadoLegal(diaAdministrativo.getFeriadoLegal());
		solicitud.setDiaAdministrativoSolicitud(diaAdministrativo);
		return solicitud;
	}

	private void modificarSolicitudDiaAdministrativo(SolicitudDiaAdministrativo solicitud) {
		solicitud.setCantMediosDiasAdmSolicitados(diaAdministrativo.getCantMediosDiasSolicitados());
		solicitud.setSolicitante(diaAdministrativo.getFuncionario());
		solicitud.setDiaAdministrativo(diaAdministrativo.getDiaAdministrativo());
		solicitud.setFeriadoLegal(diaAdministrativo.getFeriadoLegal());
		solicitud.setRechazado(null);
		solicitud.setAceptado(null);
	}
	
	public Integer getTipoDocumento(){
		return TipoDocumento.SOLICITUD_ADMINISTRATIVO;
	}
	
	public void poblarDocumento(){
		
	}

	public void postProceso(){
		
	}
	
	@SuppressWarnings("unchecked")
	protected void armaDocumento(boolean visar, boolean firmar) {
		//obtenerJefatura(null);
		obtenerJefatura(usuario);
		diaAdministrativo.setMateria("Solicitud de Dias Administrativos - " + informacionPersonal.getSolicitante().getNombreApellido());
		diaAdministrativo.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		diaAdministrativo.setTipoDocumento(em.find(TipoDocumento.class, getTipoDocumento()));

		bitacora(visar,firmar);

		if (diaAdministrativo.getCompletado() == null) {
			diaAdministrativo.setCompletado(false);
		}

		diaAdministrativo.setReservado(false);

		this.limpiarDestinatarios(this.diaAdministrativo.getId());
		setDestinatarioDocumento(diaAdministrativo);

		if (this.tipoDia == null || this.tipoDia.equals("")) {
			diaAdministrativo.setDiaAdministrativo(false);
			diaAdministrativo.setFeriadoLegal(false);
		} else {
			if (tipoDia.equals(DiaAdministrativo.TIPO_DIA_FERIADO_LEGAL)) {
				diaAdministrativo.setDiaAdministrativo(false);
				diaAdministrativo.setFeriadoLegal(true);
			}
			if (tipoDia.equals(DiaAdministrativo.TIPO_DIA_ADMINISTRATIVO)) {
				diaAdministrativo.setDiaAdministrativo(true);
				diaAdministrativo.setFeriadoLegal(false);
			}
		}
		diaAdministrativo.setFuncionario(usuario);
		diaAdministrativo.setRechazadoDiaAdministrativo(null);
		diaAdministrativo.setAceptadoDiaAdministrativo(null);
		diaAdministrativo.setAceptadoDiaAdministrativoRRHH(null);

//		Documento tmp=listaDocumentos.actualizaListasDocumentos2(getDocumento());
//		if(tmp!=null) {
//			setDocumentoOriginal(tmp);
//		}
		if (diaAdministrativo.getId() == null) {
			SolicitudDiaAdministrativo solDiaAdm = crearSolicitudDiaAdministrativo();
			//em.persist(solDiaAdm);
			diaAdministrativo.setSolicitudDiaAdministrativo(solDiaAdm);
			//em.persist(diaAdministrativo);
		} else {
			//String hql = "select sda from SolicitudDiaAdministrativo sda where sda.id= ?";
			//Query query = em.createQuery(hql);
			//query.setParameter(1, ((DiaAdministrativo)this.getDocumento()).getIdSolicitudDiaAdm());
			//SolicitudDiaAdministrativo solDiaAdm = (SolicitudDiaAdministrativo) query.getSingleResult();
			SolicitudDiaAdministrativo solDiaAdm = diaAdministrativo.getSolicitudDiaAdministrativo();
			//modificarSolicitudDiaAdministrativo(solDiaAdm);
			
			solDiaAdm.setSolicitante(diaAdministrativo.getFuncionario());
			solDiaAdm.setDiaAdministrativo(diaAdministrativo.getDiaAdministrativo());
			solDiaAdm.setFeriadoLegal(diaAdministrativo.getFeriadoLegal());
			solDiaAdm.setRechazado(null);
			solDiaAdm.setAceptado(null);
			//em.merge(solDiaAdm);

			//String hql2 = "select dd from DetalleDias dd where dd.idSolicitudDiaAdministrativo= ?";
			//Query query2 = em.createQuery(hql2);
			//query2.setParameter(1, solDiaAdm.getId());
			//List<DetalleDias> listDetalleDias = query2.getResultList();
			//for (DetalleDias dd : listDetalleDias) {
			//	em.remove(dd);
			//}
			
		}
		
		if (diasSolicitadosEliminados != null && 
				diasSolicitadosEliminados.size() > 0) {
			Iterator<DetalleDias> idd = diasSolicitadosEliminados.iterator();
			while (idd.hasNext()) {
				eliminaDetalleDias(idd.next().getId());
			}
		}
		diasSolicitadosEliminados = null;

		for (DetalleDias dd : this.diasSolicitados) {
			dd.setDiaAdministrativo(diaAdministrativo);
			//dd.setSolicitudDiaAdministrativo(diaAdministrativo.getSolicitudDiaAdministrativo());
		}
		diaAdministrativo.setListDetalleDias(this.diasSolicitados);
		informacionPersonal.setSolicitud(true);
		
		if (this.diaAdministrativo.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.diaAdministrativo);
			} else {
				if (this.diaAdministrativo.getId() != null) {
					listDocumentosRespuesta.remove(this.diaAdministrativo);
					listDocumentosRespuesta.add(this.diaAdministrativo);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} else if (this.diaAdministrativo.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = diaAdministrativo;
		}
		
		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		persistirDocumento();
	}

	private void obtenerJefatura(Persona solicitante) {
		Persona persona = null;
		if (solicitante != null) {
			persona = solicitante;
		}
		else {
			if (informacionPersonal.getSolicitante() != null) {
				persona = informacionPersonal.getSolicitante();
			}
			else{
				persona = this.usuario;
			}
		}
		//Persona aprobadorActosAdministrativos = persona.getAprobadorActosAdministrativos();
		Persona aprobadorActosAdministrativos = null;
		List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
		if (listJefesRRHH != null) {
			for (Persona p : listJefesRRHH) {
				if (!this.usuario.getId().equals(p.getId())) {
					aprobadorActosAdministrativos = p;
					break;
				}
			}
			if (aprobadorActosAdministrativos != null) {
				listDestinatarios = new HashSet<Persona>();
				listDestinatarios.add(aprobadorActosAdministrativos);
				destinatariosDocumento = new LinkedList<SelectPersonas>();
				agregarDestinatarioDocumento(aprobadorActosAdministrativos);
			}
		}
	}

	


	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (informacionPersonal.getDiasAdministrativosPendientes() != null) {
			if (informacionPersonal.getDiasAdministrativosPendientes().doubleValue() < 0) {
				return false;
			}
		}
		if (this.expediente != null) {
			if (this.diaAdministrativo != null && this.diaAdministrativo.getId() != null) {
				if (diaAdministrativo.getRechazadoDiaAdministrativo() == null
						|| (diaAdministrativo.getRechazadoDiaAdministrativo() != null && diaAdministrativo.getRechazadoDiaAdministrativo())) {
					if (!this.diaAdministrativo.getAutor().getId().equals(usuario.getId())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public boolean validarSolicitud(SolicitudDiaAdministrativo solicitudDiaAdministrativo) {
		boolean tieneDiasSuficientes = false;

		if (!tieneSolicitudEnCurso(solicitudDiaAdministrativo)) {
			if (this.tipoDia != null && (this.diasSolicitados != null && this.diasSolicitados.size() > 0)) {

				float cantDiasSol = 0;
				if (this.diasSolicitados != null && this.diasSolicitados.size() <= 12) {
					for (DetalleDias dd : this.diasSolicitados) {
						if ((dd.getParteDia().equals(DetalleDias.AM) || dd.getParteDia().equals(DetalleDias.PM))) {
							cantDiasSol += 0.5;
						} else if (dd.getParteDia().equals(DetalleDias.TODO)) {
							cantDiasSol += 1.0;
						}
					}
				}
				if (cantDiasSol <= 6) {
					if ((informacionPersonal.getDiasAdministrativosPendientes() - cantDiasSol) >= 0) {
						tieneDiasSuficientes = true;
					}
				}
				if (tieneDiasSuficientes) {
					diasOk = true;
					return true;
				} else {
					FacesMessages.instance().add("La cantidad de días solicitados supera el máximo o los disponibles");
					diasOk = false;
					return false;
				}
			} else {
				FacesMessages.instance().add("Debe seleccionar el tipo y debe ingresar los días a solicitar");
				return false;
			}
		} else {
			FacesMessages.instance().add("Usted no puede generar una solicitud, mientras mantenga otra en curso");
			diasOk = false;
			return false;
		}
	}


	public void anularVariables(){
		diasSolicitados = null;
		fechaDia = null;
		periodoDia = "";
		diasOk = true;
	}
	
	public boolean validacion(){
		return validarSolicitud(null);
	}
	
	
	@SuppressWarnings("unchecked")
	private boolean tieneSolicitudEnCurso(SolicitudDiaAdministrativo solicitudDiaAdministrativo) {
		if (usuario.getId() != null) {
			Query query;
			if (solicitudDiaAdministrativo == null || solicitudDiaAdministrativo.getId() == null) {
				query = em.createNamedQuery("SolicitudDiaAdministrativo.findActiveSolicitud");
				query.setParameter("solicitante", usuario);
			} else {
				query = em.createNamedQuery("SolicitudDiaAdministrativo.findActiveSolicitudExcludeId");
				query.setParameter("solicitante", usuario);
				query.setParameter("id", solicitudDiaAdministrativo.getId());
			}
			List<SolicitudDiaAdministrativo> solicitudes = query.getResultList();
			return (solicitudes.size() > 0) ? true : false;
		}
		return false;
	}

	public boolean isRenderedBotonAceptarRechazar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getId() != null && this.diaAdministrativo != null) {
			DiaAdministrativo diaAdministrativoDoc = diaAdministrativo;
			//System.out.println("Aceptado:"+diaAdministrativoDoc.getAceptadoDiaAdministrativo());
			//System.out.println("Rechazado:"+diaAdministrativoDoc.getRechazadoDiaAdministrativo());
			
			boolean esJefeRRHH = false;
			List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
			for (Persona p : listJefesRRHH) {
				if (this.usuario.getId().equals(p.getId())) {
					esJefeRRHH = true;
					break;
				}
			}
			
			/*Si: No es jefe de RRHH y no ha sido ni aceptado ni rechazado por el(ambos null)
			 *    o si es jefe de RRHH y no ha sido ni aceptado ni rechazado por el(ambos null)*/
			if ( (!esJefeRRHH && diaAdministrativoDoc.getRechazadoDiaAdministrativo() == null && diaAdministrativoDoc.getAceptadoDiaAdministrativo() == null)
					|| (esJefeRRHH && diaAdministrativoDoc.getRechazadoDiaAdministrativo() == null && diaAdministrativoDoc.getAceptadoDiaAdministrativoRRHH() == null)) {
				for (Documento doc : listDocumentosRespuesta) {
					if (doc instanceof Resolucion) {
						//System.out.println("Ya existe resolucion");
						return false;
					}
				}
				/**
				 * Valida que el usuario sea el jefe del solicitante.
				 */
				obtenerJefatura(diaAdministrativoDoc.getFuncionario());
				if (listDestinatarios != null) {
					for (Persona p : listDestinatarios) {
						if (p.getId().equals(usuario.getId())) {
							//System.out.println("Es jefe directo");
							return true;
						}
					}
					//listDestinatarios.clear();
				}
				List<Persona> listJefeRRHH = jerarquias.buscarJefeRRHH();
				if (listJefeRRHH != null) {
					for (Persona p : listJefeRRHH) {
						if (p.getId().equals(usuario.getId())) {
							//System.out.println("Es jefe de RRHH");
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean isRenderedMensajeRechazado() {
		if (diaAdministrativo != null && 
				diaAdministrativo.getRechazadoDiaAdministrativo() != null && 
				diaAdministrativo.getRechazadoDiaAdministrativo()) {
			if (diaAdministrativo.getAutor().getId().equals(usuario.getId())) {
				return true;
			}
		}
		return false;
	}

	public boolean existePersona() {
		return diaAdministrativo.getFuncionario() != null;
	}

	//@End
	public String end() {
		log.info("ending conversation");
		//this.funcionario = null;
		//diaAdministrativo = null;
		//informacionPersonal.init();
		return homeCrear;
	}

	public String endAndSendToRRHH() {
		log.info("ending conversation");
//		this.funcionario = null;
//		diaAdministrativo = null;
//		String conservaCalidad=informacionPersonal.getCalidadJuridica();
//		informacionPersonal.init();
//		informacionPersonal.setCalidadJuridica(conservaCalidad);
//		editor=new Editor();
//		setDocumento(new Resolucion());
//		editor.setDocumento(getDocumento());
//		
//		Calendar ahora = new GregorianCalendar();
//		// resolucion.setFechaDocumentoOrigen(ahora.getTime());
//		getDocumento().setFechaCreacion(ahora.getTime());
//		ahora.add(Calendar.DATE, 5);
//		getDocumento().setPlazo(ahora.getTime());
//
//		getDocumento().setAutor(usuario);
//		// tipo temporal, Siempre se toma como original el primero.
//		getDocumento().setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
//		// id temporal para borrar un documento
//		getDocumento().setIdNuevoDocumento(Integer.MAX_VALUE);
//		getDocumento().setReservado(false);
//		getDocumento().setEnEdicion(true);
		diaAdministrativo = null;
		homeCrear = "crearSolDiaAdministrativo";
		//homeCrear = homeCrear;
		return "crearResolucion";
	}

	@SuppressWarnings("unchecked")
	public String aprobarSolDiaAdministrativo() {
//		String hql = "select sd from SolicitudDiaAdministrativo sd where sd.id= ? ";
//		Query query = em.createQuery(hql);
		
		DiaAdministrativo diaAdministrativoDoc = diaAdministrativo;
//		query.setParameter(1, diaAdministrativoDoc.getIdSolicitudDiaAdm());
//		SolicitudDiaAdministrativo solic = (SolicitudDiaAdministrativo) query.getSingleResult();
		
		SolicitudDiaAdministrativo solic = diaAdministrativo.getSolicitudDiaAdministrativo();
		solic.setAceptado(Boolean.TRUE);
		solic.setRechazado(Boolean.FALSE);
		em.merge(solic);
		
		boolean esJefeRRHH = false;
		listDestinatarios = new HashSet<Persona>();
		List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
		if (listJefesRRHH != null) {
			for (Persona p : listJefesRRHH) {
				if (this.usuario.getId().equals(p.getId())) {
					esJefeRRHH = true;
					break;
				}
			}
			listDestinatarios.addAll(listJefesRRHH);
		}
		if (esJefeRRHH) {
			diaAdministrativoDoc.setAceptadoDiaAdministrativoRRHH(true);
			List<DetalleDias> listDetalleDias = diaAdministrativoDoc.getListDetalleDias();

			DatosSolicitud datosSol = new DatosSolicitud();
			datosSol.setDestinatario(diaAdministrativo.getFuncionario());
			datosSol.setFechaIngreso(new Date());
			datosSol.setEstadoSolicitud(SolicitudDiaAdministrativo.APROBADO + ", por RRHH ");
			datosSol.setTipoSolicitud("Día Administrativo. Debe esperar la generación y aprobación de la Resolución");
			datosSol.setListDetalleDias(listDetalleDias);

			//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
			if (datosSol.getDestinatario().getEmail() != null) {
				// TODO SEND MAIL
				//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
			}
			
			//idPlantillaSelec = (diaAdministrativoDoc.getFeriadoLegal()) ? Plantilla.PLANTILLA_DIAS_ADMINISTRATIVOS_C_REM : Plantilla.PLANTILLA_DIAS_ADMINISTRATIVOS_S_REM;
			//informacionPersonal.setSolicitudDiaAdministrativo(solic);
			return endAndSendToRRHH();
		}
		
		if (!esJefeRRHH) {
			diaAdministrativoDoc.setAceptadoDiaAdministrativo(true);
//			String sqlDetalleDias = null;
//			sqlDetalleDias = "select dd from DetalleDias dd where dd.idSolicitudDiaAdministrativo= ?";
//			Query queryDetalleDias = em.createQuery(sqlDetalleDias);
//			queryDetalleDias.setParameter(1, solic.getId());
//			List<DetalleDias> listDetalleDias = queryDetalleDias.getResultList();
			
			List<DetalleDias> listDetalleDias = diaAdministrativoDoc.getListDetalleDias();

			DatosSolicitud datosSol = new DatosSolicitud();
			datosSol.setDestinatario(diaAdministrativo.getFuncionario());
			datosSol.setFechaIngreso(new Date());
			datosSol.setEstadoSolicitud(SolicitudDiaAdministrativo.APROBADO + ", por RRHH ");
			datosSol.setTipoSolicitud("Día Administrativo. Debe esperar la generación y aprobación de la Resolución");
			datosSol.setListDetalleDias(listDetalleDias);

			//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
			if (datosSol.getDestinatario().getEmail() != null) {
				// TODO SEND MAIL
				//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
			}
		}
		return despacharExpediente();
	}



	public String rechazarSolDiaAdministrativo() {
//		String hql = "select sd from SolicitudDiaAdministrativo sd where sd.id= ?";
//		Query query = em.createQuery(hql);
//		query.setParameter(1, ((DiaAdministrativo)this.getDocumento()).getIdSolicitudDiaAdm());
//		SolicitudDiaAdministrativo solic = (SolicitudDiaAdministrativo) query.getSingleResult();
		SolicitudDiaAdministrativo solic = diaAdministrativo.getSolicitudDiaAdministrativo();
		solic.setAceptado(false);
		solic.setRechazado(true);
		em.merge(solic);

		diaAdministrativo.setRechazadoDiaAdministrativo(true);

		diaAdministrativo.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.RECHAZADO));
		diaAdministrativo.addBitacora(new Bitacora(EstadoDocumento.RECHAZADO, diaAdministrativo, this.usuario));

		persistirDocumento();

		DatosSolicitud datosSol = new DatosSolicitud();
		datosSol.setDestinatario(diaAdministrativo.getFuncionario());
		datosSol.setFechaIngreso(new Date());
		datosSol.setEstadoSolicitud(SolicitudDiaAdministrativo.RECHAZADO);
		datosSol.setTipoSolicitud("Día Administrativo");
		datosSol.setListDetalleDias(diaAdministrativo.getListDetalleDias());

		//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
		if (datosSol.getDestinatario().getEmail() != null) {
			// TODO SEND MAIL
			//me.despacharNotificacionEstadoSolDiaAdm(datosSol);
		}

		listDestinatarios =  new HashSet<Persona>();
		listDestinatarios.add(diaAdministrativo.getFuncionario());
		return despacharExpediente();
	}

	public String despacharExpediente() {
		if (listDestinatarios.size() != 0) {
			if (expediente == null || expediente.getId() == null) {
				distribuirDocumentos();
			}
			this.guardarExpediente();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			FacesMessages.instance().add("Debe Existir un Destinatario de RRHH");
			return "";
		}
	}

	public Persona getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Persona funcionario) {
		this.funcionario = funcionario;
	}

	public DiaAdministrativo getDiaAdministrativo() {
		return diaAdministrativo;
	}

	public void setDiaAdministrativo(DiaAdministrativo diaAdministrativo) {
		this.diaAdministrativo = diaAdministrativo;
	}

	public String getNombreFuncionario() {
		return nombreFuncionario;
	}

	public void setNombreFuncionario(String nombreFuncionario) {
		this.nombreFuncionario = nombreFuncionario;
	}

	public Persona getUsuario() {
		return usuario;
	}

	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}

	public String getCalidadJuridica() {
		return informacionPersonal.getCalidadJuridica();
	}

	public void setCalidadJuridica(String calidadJuridica) {
		informacionPersonal.setCalidadJuridica(calidadJuridica);
	}

	public boolean isDiasOk() {
		return diasOk;
	}

	public void setDiasOk(boolean diasOk) {
		this.diasOk = diasOk;
	}

	public Double getFeriadosLegalesPendientes() {
		return feriadosLegalesPendientes;
	}

	public void setFeriadosLegalesPendientes(Double feriadosLegalesPendientes) {
		this.feriadosLegalesPendientes = feriadosLegalesPendientes;
	}

	public Double getDiasAdministrativosPendientes() {
		return informacionPersonal.getDiasAdministrativosPendientes();
	}

	public void setDiasAdministrativosPendientes(Double diasAdministrativosPendientes) {
		informacionPersonal.setDiasAdministrativosPendientes(diasAdministrativosPendientes);
	}

	public String getGrado() {
		return informacionPersonal.getGrado();
	}

	public void setGrado(String grado) {
		informacionPersonal.setGrado(grado);
	}

	public String getTipoDia() {
		if (diaAdministrativo != null && 
				diaAdministrativo.getDiaAdministrativo() != null && 
				diaAdministrativo.getDiaAdministrativo()) {
			return DiaAdministrativo.TIPO_DIA_ADMINISTRATIVO;
		}
		if (diaAdministrativo != null && 
				diaAdministrativo.getFeriadoLegal() != null && 
				diaAdministrativo.getFeriadoLegal()) {
			return DiaAdministrativo.TIPO_DIA_FERIADO_LEGAL;
		}
		return DiaAdministrativo.TIPO_DIA_FERIADO_LEGAL;
	}

	public void setTipoDia(String tipoDia) {
		this.tipoDia = tipoDia;
	}

	public Persona getSolicitante() {
		if (informacionPersonal == null) {
			informacionPersonalSrv = new InformacionPersonalSrv();
			informacionPersonalSrv.setDocumento(diaAdministrativo);
			informacionPersonalSrv.setUsuario(usuario);
			informacionPersonalSrv.init();
			informacionPersonal = informacionPersonalSrv.getInformacionPersonal();
		}
		return informacionPersonal.getSolicitante();
	}

	public void setSolicitante(Persona solicitante) {
		informacionPersonal.setSolicitante(solicitante);
	}

	public List<DetalleDias> getDiasSolicitados() {
		if(this.diasSolicitados == null) {
			this.diasSolicitados = new ArrayList<DetalleDias>();
			if (this.diasSolicitados.size() == 0 && diaAdministrativo.getListDetalleDias() != null && diaAdministrativo.getListDetalleDias().size() > 0) {
				this.diasSolicitados.addAll(diaAdministrativo.getListDetalleDias());
			}
		}
		if (this.diasSolicitados.size() > 0)
			Collections.sort(this.diasSolicitados, ComparatorUtils.detalleDiasComparator);
		return this.diasSolicitados;
	}

	public void cargarListaSolicitados() {
		this.diasSolicitados = diaAdministrativo.getListDetalleDias();
	}

	public Persona getJefatura() {
		return jefatura;
	}

	public void setJefatura(Persona jefatura) {
		this.jefatura = jefatura;
	}

	public String getDatosObtenidos() {
		if(informacionPersonal!=null)
			return informacionPersonal.getDatosObtenidos();
		return "";
	}

	public void setDatosObtenidos(String datosObtenidos) {
		informacionPersonal.setDatosObtenidos(datosObtenidos);
	}

	public DetalleDias getDetalleDias() {
		return detalleDias;
	}

	public void setDetalleDias(DetalleDias detalleDias) {
		this.detalleDias = detalleDias;
	}

	public String getPeriodoDia() {
		return periodoDia;
	}

	public void setPeriodoDia(String periodoDia) {
		this.periodoDia = periodoDia;
	}

	public Date getFechaDia() {
		return fechaDia;
	}

	public void setFechaDia(Date fechaDia) {
		this.fechaDia = fechaDia;
	}

	public Integer getIdPlantillaSelec() {
		return idPlantillaSelec;
	}

	public void setIdPlantillaSelec(Integer idPlantillaSelec) {
		this.idPlantillaSelec = idPlantillaSelec;
	}
	
	@Destroy
	@Remove
	@Override
	public final void destroy() {
	}

	@Override
	public final Boolean getVisado() {
		//boolean visado;
		if (diaAdministrativo != null && diaAdministrativo.getVisaciones() != null && diaAdministrativo.getVisaciones().size() > 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	@Override
	public final Boolean getFirmado() {
		//boolean firmado;
		if (diaAdministrativo != null && diaAdministrativo.getFirmas() != null && !diaAdministrativo.getFirmas().isEmpty()) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}
	
	private void bitacora(boolean visar, boolean firmar) {
		if (!this.getFirmado()) {
			if (!visar && !firmar) {
				if (diaAdministrativo.getId() != null) {
					diaAdministrativo.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
					diaAdministrativo.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, diaAdministrativo, this.usuario));
				} else {
					diaAdministrativo.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
					diaAdministrativo.addBitacora(new Bitacora(EstadoDocumento.CREADO, diaAdministrativo, this.usuario));
				}
			}
			if (firmar) {
				this.repositorio.firmar(diaAdministrativo, usuario);
				// Estado del documento debe setearse despues de firmarlo.
				diaAdministrativo.setEstado(new EstadoDocumento(EstadoDocumento.FIRMADO));
				diaAdministrativo.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, diaAdministrativo, this.usuario));
			}
			if (visar) {
				this.repositorio.visar(diaAdministrativo, usuario);
				diaAdministrativo.addBitacora(new Bitacora(EstadoDocumento.VISADO, diaAdministrativo, this.usuario));
			}
		}
	}
	
	@Transactional
	private void limpiarDestinatarios(final Long id) {
		try {
			Query query = em.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}
	
	private void setDestinatarioDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDestinatarios(ddList);
	}
	
	@Transactional
	private void eliminaDetalleDias(Long id) {
		DetalleDias dd = em.find(DetalleDias.class, id);
		em.remove(dd);
	}
	
	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.diaAdministrativo.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}
	
	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.diaAdministrativo.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.diaAdministrativo);
			}
		}
		listaDocumentos = lista;
	}
	
	private void agregarDestinatarioDocumento(Persona destinatario) {
		if (destinatariosDocumento == null) {
			destinatariosDocumento = new LinkedList<SelectPersonas>();
		}
		String value = destinatario.getNombres() + " "
				+ destinatario.getApellidoPaterno();
		final Cargo car = destinatario.getCargo();
		final UnidadOrganizacional uo = car.getUnidadOrganizacional();
		final Departamento depto = uo.getDepartamento();
		final Division d = depto.getDivision();
		if ("Jefe Division".equals(car.getDescripcion())
				|| "Jefe Departamento".equals(car.getDescripcion())
				|| "Jefe Unidad".equals(car.getDescripcion())) {
			value = car.getDescripcion();
		} else {
			value += " - " + car.getDescripcion();
		}
		if (!destinatario.getCargo().getUnidadOrganizacional().getVirtual()) {
			value += " - " + uo.getDescripcion();
		} else if (!destinatario.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
			value += " - " + depto.getDescripcion();
		} else {
			value += " - " + d.getDescripcion();
		}
		//if (this.destinatariosDocumento.contains(new SelectPersonas(value, destinatario)))
			this.destinatariosDocumento.add(new SelectPersonas(value, destinatario));
	}
	
	private void persistirDocumento() {
	    try {
    		if (this.diaAdministrativo.getId() != null) {
    			md.actualizarDocumento(this.diaAdministrativo);
    
    		} else if (this.expediente != null && this.expediente.getId() != null) {
    			me.agregarRespuestaAExpediente(expediente, this.diaAdministrativo, true);
    		}
	    } catch (DocumentNotUploadedException e) {
	        e.printStackTrace();
	    }
	}
	
	private void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}
	
	@End
	private void guardarExpediente() {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo al documento original.
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado() && !this.documentoOriginal.getId().equals(this.diaAdministrativo.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
                    md.actualizarDocumento(this.documentoOriginal);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			}

		}

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
		    try {
    			if (doc.getId() == null) {
    				me.agregarRespuestaAExpediente(expediente, doc, true);
    			} else {
    				// Cerrar plazo a los documentos respuesta.
    				if (doc.getCompletado() != null && !doc.getCompletado() && !doc.getId().equals(this.diaAdministrativo.getId())) {
    					doc.setCompletado(true);
    					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
    					md.actualizarDocumento(doc);
    				}
    			}
		    } catch (DocumentNotUploadedException e) {
		        e.printStackTrace();
		    }
		}

		idExpedientes = new ArrayList<Long>();
		Date fechaIngreso = new Date();

		for (Persona de : listDestinatarios) {
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}

		List<Expediente> expedientes = new ArrayList<Expediente>();

		for (Long id : idExpedientes) {
			Expediente e = me.buscarExpediente(id);
			me.despacharExpediente(e);
			expedientes.add(e);
		}
		me.despacharExpediente(expediente);
		//me.despacharNotificacionPorEmail(expedientes);

		FacesMessages.instance().add("Expediente Despachado");
	}
	
	@Override
	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null 
				&& this.expediente.getId() != null
				&& this.expediente.getFechaDespacho() == null 
				&& this.diaAdministrativo != null 
				&& this.diaAdministrativo.getAutor().equals(usuario)) {
			return true;
		}
		return false;
	}

	@Override
	public void guardarYCrearExpediente() {
		esSolicitud = Boolean.TRUE;
		if (validarSolicitud(diaAdministrativo.getSolicitudDiaAdministrativo())) {
			armaDocumento(false, false);
			documentoOriginal = listDocumentosRespuesta.get(0);
			listDocumentosRespuesta.remove(0);
			if (listDestinatarios == null || listDestinatarios.size() == 0)
				esSolicitud = Boolean.FALSE;
		}
	}

	@Override
	public String guardarDocumento() {
		if (validarSolicitud(diaAdministrativo.getSolicitudDiaAdministrativo())) {
			armaDocumento(false, false);
			diasOk = true;
			return end();
		}
		return "";
	}
	
	@Override
	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}
	
	@Override
	public void obtenerDatosExternos() {
		if (informacionPersonal == null || 
				(informacionPersonal != null && informacionPersonal.getDatosObtenidos() == null)) {
			informacionPersonal = new InformacionPersonal();
			informacionPersonalSrv = new InformacionPersonalSrv();
			informacionPersonalSrv.setDocumento(diaAdministrativo);
			informacionPersonalSrv.setUsuario(usuario);
			informacionPersonalSrv.setInformacionPersonal(informacionPersonal);
			informacionPersonalSrv.init();
			informacionPersonal = informacionPersonalSrv.getInformacionPersonal();
		}
	}
	
	@Override
	public void limpiarDiasSolicitados() {
		fechaDia = null;
		periodoDia = null;
	}

}