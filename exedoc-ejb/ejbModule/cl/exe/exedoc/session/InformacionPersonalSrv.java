package cl.exe.exedoc.session;

import javax.ejb.Stateless;

import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Solicitud;
import cl.exe.exedoc.session.utils.InformacionPersonal;

//@Name("informacionPersonalSrv")
//@Scope(ScopeType.SESSION)
//@AutoCreate
//@Stateless
public class InformacionPersonalSrv {
	
	//@In 
	private Persona usuario;
	
    //@In(required = false, scope = ScopeType.CONVERSATION)
   //@Out(required = false, scope = ScopeType.CONVERSATION)
	private InformacionPersonal informacionPersonal;
    
    //@In(required = false, scope = ScopeType.CONVERSATION)
	private Documento documento;

    public InformacionPersonalSrv() {}
    
    public Persona getUsuario() {
    	return this.usuario;
    }
    
    public void setUsuario(Persona usuario) {
    	this.usuario = usuario;
    }
    
    public InformacionPersonal getInformacionPersonal() {
    	return this.informacionPersonal;
    }
    
    public void setInformacionPersonal(InformacionPersonal informacionPersonal) {
    	this.informacionPersonal = informacionPersonal;
    }
    
    public Documento getDocumento() {
    	return this.documento;
    }
    
    public void setDocumento(Documento documento) {
    	this.documento = documento;
    }
	private void cargarInformacionPersonal(){
		//if (informacionPersonal == null) return;
		try {
			//TODO Comentado hasta que se implemente solucion RRHH
//			DatosSolicitud datosSolicitud = null;
//			datosSolicitud = DatosInterfacesExternas.getDiasAdministrativosManager(informacionPersonal.getSolicitante().getIdPersona());
//			if (datosSolicitud == null) return;
//			informacionPersonal.setDiasAdministrativosPendientes(datosSolicitud.getDiasPendientes());
//			informacionPersonal.setGrado(datosSolicitud.getGrado());
//			informacionPersonal.setCalidadJuridica(datosSolicitud.getCalidadJuridica());
//			
//			datosSolicitud = DatosInterfacesExternas.getDatosVacacionesManager(informacionPersonal.getSolicitante().getIdPersona());
//			informacionPersonal.setDiasVacacionesPendientes(datosSolicitud.getDiasPendientes());
//			informacionPersonal.setDatosObtenidos("OK");
			
			informacionPersonal.setDiasAdministrativosPendientes(10D);
			informacionPersonal.setGrado("1");
			informacionPersonal.setCalidadJuridica("PLANTA");
			informacionPersonal.setDiasVacacionesPendientes(10D);
			informacionPersonal.setDatosObtenidos("OK");

		} catch (Exception e) {
			e.printStackTrace();
			FacesMessages.instance().add("No se pudo conectar a Sistema de Personal.");
		}
	}
	
	//@Create
	public void init(){
		System.out.println("Inicializando carga de datos");
//		if(informacionPersonal==null) return;
		informacionPersonal = new InformacionPersonal();
		informacionPersonal.setSolicitante(usuario);
		if (documento != null 
				&& documento instanceof Solicitud 
				&& ((Solicitud)documento).getFuncionario() != null 
				&& ((Solicitud)documento).getFuncionario().getId() > 0) {
			informacionPersonal.setSolicitante(((Solicitud)documento).getFuncionario());
		} else if (documento != null && !(documento instanceof Solicitud)) {
			Persona autor = documento.getAutor();
			informacionPersonal.setSolicitante(autor);
		} else {
//			if (usuario != null && usuario.getId() != 0) {
//				informacionPersonal.setSolicitante(usuario);
//			}
		}
		cargarInformacionPersonal();
	}

}
