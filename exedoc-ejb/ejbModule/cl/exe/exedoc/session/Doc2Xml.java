package cl.exe.exedoc.session;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DocumentoElectronico;

@Local
public interface Doc2Xml {

	public DocumentoElectronico getDocumento(Long id);
	public byte[] getXmlData(DocumentoElectronico doc);
}
