package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.AccionProvidencia;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DestinatariosFirmaDocumentoMail;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("crearProvidencia")
public class CrearProvidenciaBean implements CrearProvidencia {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	//TODO revisar documentos anexos
	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosAnexo;*/

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Providencia providencia;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private Boolean completado;
	private Boolean firmado;

	@DataModel
	private Map<Long, Boolean> accionesProvidencia;

	public void visualizar() {
		if (!firmado) {
			armaDocumento(false, false);
		}
	}

	public void guardar() {
		armaDocumento(false, false);
		FacesMessages.instance().add("Documento Guardado");
	}

	public void firmar() {
		if (verifica()) {
			if (usuario.getId().equals(providencia.getIdEmisor())) {
				providencia.setFechaDocumentoOrigen(new Date());
				this.armaDocumento(false, true);
				//agregarListaDestinatariosExpediente();
				FacesMessages.instance().add("Documento Firmado");
//				if (expediente != null && expediente.getId() != null) {
//					DestinatariosFirmaDocumentoMail destMail = me.obtenerDatosNotificacion(providencia, expediente.getNumeroExpediente());
//					me.firmarNotificacionPorEmail(destMail);
//				}
			} else {
				FacesMessages.instance().add("Ud. No puede Firmar este Documento, solo lo puede hacer el Emisor (DE)");
			}
		}
	}

	public void agregarListaDestinatariosExpediente() {
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		for (SelectPersonas sp : destinatariosDocumento) {
			sp.getPersona().setDestinatarioConCopia(false);
			sp.getPersona().setBorrable(false);
			listDestinatarios.add(sp.getPersona());
		}
	}

	public String end() {
		log.info("ending conversation");
		if (firmado && this.expediente == null) {
			guardarYCrearExpediente();
			return "";
		}
		this.accionesProvidencia = null;
		destinatariosDocumento = null;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}
		return homeCrear;
	}

	public String completar() {
		this.providencia.setCompletado(true);
		this.providencia.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.providencia, this.usuario));
		return end();
	}

	public String agregarDocumento() {
		if (verifica()) {
			armaDocumento(false, false);
			FacesMessages.instance().add("Documento Guardado");
			return end();
		} else {
			return "";
		}
	}

	public void guardarYCrearExpediente() {
		// if (verifica()) {
		armaDocumento(false, false);
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
		// }
	}

	private boolean verifica() {
		boolean guardar = true;
		if (providencia.getEmisor() == null) {
			FacesMessages.instance().add(new FacesMessage("Debe Seleccionar un Emisor (DE) al Documento"));
			guardar = false;
		}
		if (destinatariosDocumento == null || destinatariosDocumento.size() == 0) {
			FacesMessages.instance().add(new FacesMessage("Debe Seleccionar por lo menos un Destinatario (A) al Documento"));
			guardar = false;
		}
		return guardar;
	}

	private void persistirDocumento() {
	    try {
    		if (this.providencia.getId() != null) {
    			md.actualizarDocumento(this.providencia);
    		} else if (this.expediente != null && this.expediente.getId() != null) {
    			me.agregarRespuestaAExpediente(expediente, this.providencia, true);
    		}
	    } catch (DocumentNotUploadedException e) {
	        e.printStackTrace();
	    }
		/*if (this.expediente != null) {
			for (Documento doc : listDocumentosAnexo) {
				if (doc.getId() == null) {
					if (doc instanceof DocumentoBinario) {
						DocumentoBinario docBin = (DocumentoBinario) doc;
						if (docBin.getIdDocumentoReferencia().equals(this.providencia.getIdNuevoDocumento())) {
							me.agregarAnexoAExpediente(expediente, doc, true, this.providencia.getId());
						}
						doc.setEnEdicion(true);
					}
				}
			}
		}*/
	}

	private void armaDocumento(boolean visar, boolean firmar) {
		this.providencia.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		// TipoDocumento: 8 - Providencia
		this.providencia.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.PROVIDENCIA));
		if (!this.getFirmado()) {
			if (!visar && !firmar) {
				if (this.providencia.getId() != null) {
					this.providencia.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
					this.providencia.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.providencia, this.usuario));
				} else {
					this.providencia.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
					this.providencia.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.providencia, this.usuario));
				}
			}
			if (firmar) {
				this.repositorio.firmar(this.providencia, usuario);
				this.providencia.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.providencia, this.usuario));
				//Pruebas mias
				List<Expediente> padre = me.obtienePrimerExpediene(expediente);
				List<Persona> dest = me.buscarDestinatarios(padre);
				for (Persona expediente : dest) {
					me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
				}
			}
		}

		if (this.providencia.getCompletado() == null) {
			this.providencia.setCompletado(false);
		}

		setDestinatarioDocumento(providencia);

		this.providencia.setReservado(false);

		this.setAccionesProvidencia();

		if (this.providencia.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.providencia);
			} else {
				if (this.providencia.getId() != null) {
					listDocumentosRespuesta.remove(this.providencia);
					listDocumentosRespuesta.add(this.providencia);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} /*else if (this.providencia.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
			if (!existeDocumento(listDocumentosAnexo)) {
				listDocumentosAnexo.add(this.providencia);
			} else {
				if (this.providencia.getId() != null) {
					listDocumentosAnexo.remove(this.providencia);
					listDocumentosAnexo.add(this.providencia);
				} else {
					this.reemplazaDocumento(listDocumentosAnexo);
				}
			}
		}*/ else if (this.providencia.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = providencia;
		}
		
		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		
		persistirDocumento();
	}

	private void setDestinatarioDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDestinatarios(ddList);
	}

	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.providencia.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.providencia.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.providencia);
			}
		}
		listaDocumentos = lista;
	}

	private void setAccionesProvidencia() {
		Set<Long> idAcciones = this.accionesProvidencia.keySet();
		List<AccionProvidencia> acciones = new ArrayList<AccionProvidencia>();
		for (Long idAccion : idAcciones) {
			if (((Boolean) this.accionesProvidencia.get(idAccion))) {
				acciones.add(em.find(AccionProvidencia.class, new Integer(idAccion.intValue())));
			}
		}
		this.providencia.setAcciones(acciones);
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Providencia getProvidencia() {
		return providencia;
	}

	public Map<Long, Boolean> getAccionesProvidencia() {
		if (this.providencia != null && this.providencia.getAcciones() != null && this.providencia.getAcciones().size() == 1) {
			for (AccionProvidencia ap : this.providencia.getAcciones()) {
				if (ap.getId().equals(-1)) {
					accionesProvidencia = null;
					this.providencia.getAcciones().remove(ap);
					break;
				}
			}
		}

		if (accionesProvidencia == null || accionesProvidencia.size() == 0) {
			if (this.providencia != null && this.providencia.getAcciones() != null && this.providencia.getAcciones().size() != 0) {
				this.accionesProvidencia = new HashMap<Long, Boolean>();
				for (AccionProvidencia ap : this.providencia.getAcciones()) {
					this.accionesProvidencia.put(new Long(ap.getId()), true);
				}
			}
			if (accionesProvidencia == null) {
				accionesProvidencia = new HashMap<Long, Boolean>();
			}
		}
		return accionesProvidencia;
	}

	public Boolean getFirmado() {
		if (this.providencia != null && this.providencia.getFirmas() != null && this.providencia.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}

	public Boolean getCompletado() {
		if (this.providencia != null && this.providencia.getCompletado() != null && !this.providencia.getCompletado()) {
			completado = true;
		} else {
			completado = false;
		}
		return completado;
	}

	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public boolean isDespachable() {
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		return true;
	}

	public boolean isRenderedBotonFirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (!isEditable()) {
			return false;
		}
		if (usuario.getId().equals(providencia.getIdEmisor())) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (getFirmado()) {
			if (listDestinatarios.size() != 0) {
				return true;
			}
		}
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) {
			return true;
		}
		return false;
	}

	/***************************************************************************
	 * Archivos Anexos - Antecedentes
	 **************************************************************************/

	private ArchivoDocumentoBinario archivo;
	private String materiaArchivo;
	private int contadorDocumentos;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	public void listener(UploadEvent event) throws Exception {
		archivo = new ArchivoDocumentoBinario();
		UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
	}

	public void agregarAntecedenteArchivo() {
		if (archivo != null) {
			if (true/*!existeNombreArchivo(archivo.getNombreArchivo())*/) {
				Date hoy = new Date();
				DocumentoBinario doc = new DocumentoBinario();
				//doc.setIdDocumentoReferencia(this.providencia.getIdNuevoDocumento());
				doc.setMateria(this.materiaArchivo);
				doc.setAutor(usuario);
				doc.setFechaCreacion(hoy);
				doc.setIdNuevoDocumento(contadorDocumentos++);
				doc.setEnEdicion(true);
				doc.setReservado(false);
				archivo.setFecha(hoy);
				archivo.setIdNuevoArchivo(contadorDocumentos++);
				doc.setArchivo(archivo);
				//doc.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO));
				doc.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.DIGITAL));
				doc.setTipoDocumento(new TipoDocumento(TipoDocumento.SIN_TIPO));
				//listDocumentosAnexo.add(doc);
			} else {
				FacesMessages.instance().add("Ya existe un archivo con ese nombre");
			}
			archivo = null;
			materiaArchivo = null;
		}
	}

	/*private boolean existeNombreArchivo(String nombreArchivo) {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getArchivo() != null && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) {
					return true;
				}
			}
		}
		return false;
	}*/

	public void verArchivo(Long idArchivo) {
		/*ArchivoDocumentoBinario a = em.find(ArchivoDocumentoBinario.class, idArchivo);
		buscarArchivoRepositorio(a);*/
	    try {
            if (!DocUtils.getFile(idArchivo, em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + idArchivo + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private void buscarArchivoRepositorio(Archivo a) {
		byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		if (data != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			try {
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
				response.setContentType(a.getContentType());
				response.setHeader("Content-disposition", "attachment; filename=\"" + a.getNombreArchivo() + "\"");
				// .replace('
				// ',
				// '_'));
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
				facesContext.responseComplete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + a.getId() + ")");
		}
	}

	/*public void eliminarAnexoDocumento(Integer numDoc) {
		int contador = 0;
		for (Documento doc : listDocumentosAnexo) {
			if (doc.getIdNuevoDocumento() != null && doc.getIdNuevoDocumento().equals(numDoc)) {
				if (doc.getId() != null) {
					listDocumentosEliminados.add(doc);
				}
				listDocumentosAnexo.remove(contador);
				break;
			}
			contador++;
		}
	}*/

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public void setMateriaArchivo(String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	/*public List<Documento> getListDocumentosAnexo() {
		List<Documento> documentos = new ArrayList<Documento>();
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (providencia.getIdNuevoDocumento().equals(docBin.getIdDocumentoReferencia())) {
					documentos.add(docBin);
				}
			}
		}
		return documentos;
	}*/

	/***************************************************************************
	 * DESPACHAR EXPEDIENTES
	 **************************************************************************/
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	private List<Long> idExpedientes;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	public String despacharExpediente() {
		if (listDestinatarios.size() != 0) {
			// armaDocumento(false, false);
			if (expediente == null || expediente.getId() == null) {
				distribuirDocumentos();
			}
			this.guardarExpediente();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			FacesMessages.instance().add("Debe Seleccionar un Destinatario del Expediente");
			return "";
		}
	}

	@End
	private void guardarExpediente() {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado() && !this.documentoOriginal.getId().equals(this.providencia.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
                    md.actualizarDocumento(this.documentoOriginal);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			}
		}

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
		    try {
			if (doc.getId() == null) {
				me.agregarRespuestaAExpediente(expediente, doc, true);
			} else {
				// Cerrar plazo
                    if (doc.getCompletado() != null && !doc.getCompletado()
                            && !doc.getId().equals(this.providencia.getId())) {
					doc.setCompletado(true);
					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
					md.actualizarDocumento(doc);
				}
			}
		    } catch (DocumentNotUploadedException e) {
		        e.printStackTrace();
		    }
		}

		/*for (Documento doc : listDocumentosAnexo) {
			if (doc.getId() == null) {
				if (doc instanceof DocumentoBinario) {
					DocumentoBinario docBin = (DocumentoBinario) doc;
					Long idDocumentoReferencia = buscaDocumentoReferencia(docBin.getIdDocumentoReferencia());
					if (idDocumentoReferencia != null) {
						me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia);
					} else {
						me.agregarAnexoAExpediente(expediente, doc, true);
					}
				} else {
					me.agregarAnexoAExpediente(expediente, doc, true);
				}
			} else {
				md.actualizarDocumento(doc);
			}
		}*/

		this.guardarObservaciones();
		idExpedientes = new ArrayList<Long>();
		Date fechaIngreso = new Date();

		for (Persona de : listDestinatarios) {
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}

		List<Expediente> expedientes = new ArrayList<Expediente>();

		for (Long id : idExpedientes) {
			Expediente e = me.buscarExpediente(id);
			me.despacharExpediente(e);
			expedientes.add(e);
		}
		me.despacharExpediente(expediente);
		//me.despacharNotificacionPorEmail(expedientes);

		FacesMessages.instance().add("Expediente Despachado");
	}

//	private Long buscaDocumentoReferencia(Integer idDocumento) {
//		Long id = null;
//		for (Documento doc : listDocumentosRespuesta) {
//			if (doc.getIdNuevoDocumento().equals(idDocumento)) {
//				id = doc.getId();
//				break;
//			}
//		}
//		if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
//			id = documentoOriginal.getId();
//		}
//		return id;
//	}

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	public void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

}
