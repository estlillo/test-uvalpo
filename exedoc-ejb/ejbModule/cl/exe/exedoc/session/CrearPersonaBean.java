package cl.exe.exedoc.session;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Persona;

@Stateful
@Name("crearPersona")
public class CrearPersonaBean implements CrearPersona {

	@Logger
	private Log log;

	@PersistenceContext
	EntityManager em;

	// datos del form
	private String rut;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String email;
	private String usuario;
	private Long idCargo;
	private Boolean personaDefault;

	@Begin
	public String begin() {
		// implement your begin conversation business logic
		log.info("beginning conversation");
		return "crearPersona";
	}

	@End
	public String guardarPersona() {
		Persona persona = new Persona();
		persona.setRut(this.rut);
		persona.setNombres(this.nombres);
		persona.setApellidoPaterno(this.apellidoPaterno);
		persona.setApellidoMaterno(this.apellidoMaterno);
		persona.setEmail(this.email);
		persona.setUsuario(this.usuario);
		persona.setPersonaDefault(this.personaDefault);
		Cargo cargo = (Cargo) em.createQuery("select c from Cargo c where c.id = :param")
				.setParameter("param", idCargo).getSingleResult();
		persona.setCargo(cargo);
		em.persist(persona);
		return "crearPersona";
	}

	@End
	public String end() {
		// implement your end conversation business logic
		log.info("ending conversation");
		return "crearPersona";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Boolean getPersonaDefault() {
		return personaDefault;
	}

	public void setPersonaDefault(Boolean personaDefault) {
		this.personaDefault = personaDefault;
	}
}
