package cl.exe.exedoc.session;

import static cl.exe.exedoc.util.JerarquiasLocal.TEXTO_INICIAL;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.validator.Length;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.mantenedores.AdministradorPersona;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * @author Ricardo Fuentes
 */
@Stateful
@Name("crearDocumentoPapel")
public class CrearDocumentoPapelBean implements CrearDocumentoPapel {
	private static final String SELECCIONAR = JerarquiasLocal.TEXTO_INICIAL;

	@Logger
	private Log log;

	private static final String NO_PUEDE_AGREGAR = "No se puede agregar a esta persona como destinatario, ";

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	private String homeCrear;
	
	@In(required = false, scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrearDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	// @In(required = false, scope = ScopeType.CONVERSATION)
	// @Out(required = false, scope = ScopeType.CONVERSATION)
	// private List<Documento> listDocumentosAnexo;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, value = "documento")
	private DocumentoPapel documento;

	@In(required = true)
	private Persona usuario;

	// Datos Documento
//	@Length(max = 2000)
//	private String materia;
	private List<SelectItem> listTipoDocumentos;
	private Long tipoDocumentoID;

	// DestinatarioDocumento
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private AdministradorAlertas administradorAlertas;

	@EJB
	private AdministradorPersona adminPersona;

	private Long codigoBarra;

	private String destinatario = "";


	private List<SelectItem> nivelesUrgencia;
	private Long idAlerta;
	/**
	 * Constructor.
	 */
	public CrearDocumentoPapelBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation");
		listTipoDocumentos = this.buscarTipoDocumentosPapel();
		this.setDestinatario("");
		return "adjuntarRespuestaExpediente";
	}

	@Create
	@Override
	public void ini() {
		listTipoDocumentos = this.buscarTipoDocumentosPapel();
		this.getDestinatariosDocumento();
		
		this.tipoDocumentoID = JerarquiasLocal.INICIO;
	}

	@Override
	public String end() {
		//this.materia = "";
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		this.tipoDocumentoID = JerarquiasLocal.INICIO;
		if (this.vistoDesdeReporte != null && this.vistoDesdeReporte) {
			if (this.homeCrearDesdeReporte != null ) {
				return this.homeCrearDesdeReporte;
			}
		}
		if (documento.getId() != null) {
			documento.setEliminable(md.isEliminable(documento, expediente, usuario));
		}
		return homeCrear;
	}

	@Override
	public String agregarDocumento() {
		boolean ok = true;
		if (tipoDocumentoID.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Debe seleccionar el Tipo de Documento");
			ok = false;
		}
		if (documento.getNumeroDocumento() == null || 
				(documento.getNumeroDocumento() != null && documento.getNumeroDocumento().trim().length() == 0)) {
			FacesMessages.instance().add("El Número de Documento es obligatorio");
			ok = false;
		}
		if (documento.getAntecedentes() != null) {
			documento.setAntecedentes(documento.getAntecedentes().trim());
			if (documento.getAntecedentes().length() > 2000) {
				FacesMessages.instance().add("El Antecedente debe ser menor a 2000 caracteres");
				ok = false;
			}
		}
		if (documento.getMateria() == null || 
				(documento.getMateria() != null && documento.getMateria().trim().length() == 0)) {
			FacesMessages.instance().add("La Materia es obligatoria");
			ok = false;
		} else {
			documento.setMateria(documento.getMateria().trim());
			if (documento.getMateria().length() > 2000) {
				FacesMessages.instance().add("La Materia debe ser menor a 2000 caracteres");
				ok = false;
			}
		}
		if (documento.getFechaDocumentoOrigen() == null) {
			FacesMessages.instance().add("La Fecha de Origen es obligatoria");
			ok = false;
		}
		if(documento.getEmisor() == null || (documento.getEmisor() != null && documento.getEmisor().trim().length() == 0)) {
			FacesMessages.instance().add("El campo \"De\" es obligatorio");
			ok = false;
		} else {
			documento.setEmisor(documento.getEmisor().trim());
			if (documento.getEmisor() != null && documento.getEmisor().length() > 255){
				FacesMessages.instance().add("El campo \"De\" debe ser menor a 255 caracteres.");
				ok = false;
			}
		}
		if(this.destinatario != null && this.destinatario.trim().length() > 255){
			FacesMessages.instance().add("El campo \"A\" debe ser menor a 255 caracteres.");
			ok = false;
		}
		if ((destinatariosDocumento == null || destinatariosDocumento.size() == 0)) {
			if (this.getDestinatario() == null
					|| this.getDestinatario().trim().equals("")) {
				FacesMessages.instance().add(
						"Debe ingresar un Destinatario al Documento");
				ok = false;
			}
		}
		if (documento.getPlazo() != null) {
			if (documento.getPlazo().compareTo(documento.getFechaDocumentoOrigen()) < 0) {
				FacesMessages.instance().add("El Plazo no puede ser anterior a la Fecha Origen del Documento");
				ok = false;
			}
		}
		
		if (ok) {
			this.armaDocumento();
			return this.end();
		} else {
			return "";
		}
	}

	/**
	 * Metodo que arma un documento.
	 */
	private void armaDocumento() {
//		if (!this.documento.getReservado()) {
//			this.documento.setMateria(this.materia);
//		}
		if (this.getIdAlerta() != null && !this.getIdAlerta().equals(JerarquiasLocal.INICIO)) {
			this.documento.setAlerta(
					administradorAlertas.buscarAlerta(this.getIdAlerta()));
		}
		documento.setFormatoDocumento(new FormatoDocumento(
				FormatoDocumento.PAPEL));
		this.removeDestinatarios(documento.getId());
		this.setDestinatarioDocumento(documento);
		final TipoDocumento tipo = em.find(TipoDocumento.class,
				this.tipoDocumentoID.intValue());
		documento.setTipoDocumento(tipo);

		if (documento.getEstado() == null) {
			documento.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
			//documento.setEliminable(true);
		}

		if (!this.documento.getTipoDocumentoExpediente().getId()
				.equals(TipoDocumentoExpediente.ORIGINAL)) {
			if (!this.existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.documento);
			} else {
				if (this.documento.getId() != null) {
					listDocumentosRespuesta.remove(this.documento);
					listDocumentosRespuesta.add(this.documento);
				} else {
					listDocumentosRespuesta = this
							.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} else if (this.documento.getTipoDocumentoExpediente().getId()
				.equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = documento;
		}

		this.persistirDocumento();
	}


	/**
	 * Metodo encargado de persistir documento.
	 */
	private void persistirDocumento() {
		try {
			if (this.documento.getId() != null) {
				md.actualizarDocumento(this.documento);

			} else if (this.expediente != null
					&& this.expediente.getId() != null) {
				me.agregarRespuestaAExpediente(expediente, this.documento, true);
			}
		} catch (DocumentNotUploadedException e) {
			log.error("error al cargar documentos. ", e);
		}

		// if (this.expediente != null) {
		// for (Documento doc : listDocumentosAnexo) {
		// if (doc.getId() == null) {
		// if (doc instanceof DocumentoBinario) {
		// DocumentoBinario docBin = (DocumentoBinario) doc;
		// if
		// (docBin.getIdDocumentoReferencia().equals(this.documento.getIdNuevoDocumento()))
		// {
		// me.agregarAnexoAExpediente(expediente, doc, true,
		// this.documento.getId());
		// }
		// doc.setEnEdicion(true);
		// } } } }

	}

	/**
	 * Metodo valida que el numero del dicumento este en la lista.
	 * 
	 * @param listaDocumentos
	 *            {@link List} of {@link Documento}
	 * @return {@link Boolean}
	 */
	private boolean existeDocumento(final List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(
					this.documento.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que reemplaza documento en la lista.
	 * 
	 * @param listaDocumentos
	 *            {@link List} of {@link Documento}
	 * @return {@link List} of {@link Documento}
	 */
	private List<Documento> reemplazaDocumento(
			final List<Documento> listaDocumentos) {
		final List<Documento> lista = new ArrayList<Documento>();
		for (final Iterator<Documento> doc = listaDocumentos.iterator(); doc
				.hasNext();) {
			final Documento docTmp = doc.next();
			final Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.documento.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.documento);
			}
		}
		return lista;
	}

	/**
	 * set del destinatario de documentos.
	 * 
	 * @param doc
	 *            {@link Documento}
	 */
	private void setDestinatarioDocumento(final Documento doc) {
		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		if (destinatariosDocumento.size() == 0) {
			// List<ListaPersonasDocumento> list = new
			// ArrayList<ListaPersonasDocumento>();
			DestinatarioDocumento personadocumento = new DestinatarioDocumento();
			personadocumento.setDestinatario(this.getDestinatario());
			personadocumento.setDocumento(doc);
			ddList.add(personadocumento);
			// documento.setDestinatarios(ddList);
		} else {
				
			for (SelectPersonas item : this.destinatariosDocumento) {
				final DestinatarioDocumento dd = new DestinatarioDocumento();
				dd.setDestinatario(item.getDescripcion());
				dd.setDocumento(doc);
				if (!this.esta(ddList,item.getDescripcion())) {
					ddList.add(dd);
				}
			}
		}
		List<ListaPersonasDocumento> l = doc.getDestinatarios();
		doc.setDestinatarios(ddList);
	}

	public boolean esta(List<ListaPersonasDocumento> lista, String descripcion){
		boolean flag = false;
		
		for (ListaPersonasDocumento listaPersonasDocumento : lista) {
			if(listaPersonasDocumento.getDestinatario().equals(descripcion))flag = true;
		}
		return flag;
	}
	
	
	/**
	 * Metodo que busca los tipos de documentos.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarTipoDocumentos() {
		if (usuario.getCargo().getDescripcion().toUpperCase()
				.startsWith("JEFE")
		// || usuario.getRoles().contains(new Rol(Rol.SUBROGANTE))
		) {
			return TipoDocumentosList.getInstanceTipoDocumentoList(em)
					.getTiposDocumentoList(TEXTO_INICIAL);
		} else {
			final List<SelectItem> listaTiposDocumentos = TipoDocumentosList
					.getInstanceTipoDocumentoList(em)
					.getTiposDocumentoElectronicoList(TEXTO_INICIAL);
			SelectItem tipo = null;
			for (SelectItem tipoDoc : listaTiposDocumentos) {
				if (tipoDoc.getValue().equals(TipoDocumento.PROVIDENCIA)) {
					tipo = tipoDoc;
				}
			}
			listaTiposDocumentos.remove(tipo);
			return listaTiposDocumentos;
		}
	}

	/**
	 * Metodo que busca todos los tipos de documento papel.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarTipoDocumentosPapel() {
		return TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoNoElectronicoVisible(TEXTO_INICIAL);
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public Expediente getExpediente() {
		return expediente;
	}

	@Override
	public void setExpediente(final Expediente expediente) {
		this.expediente = expediente;
	}

	@Override
	public List<Documento> getListDocumentosRespuesta() {
		return listDocumentosRespuesta;
	}

	@Override
	public void setListDocumentosRespuesta(
			final List<Documento> listDocumentosRespuesta) {
		this.listDocumentosRespuesta = listDocumentosRespuesta;
	}

//	@Override
//	public String getMateria() {
//		if (this.documento != null) {
//			if (this.documento.getReservado()) {
//				materia = "Materia Reservada";
//			} else {
//				materia = this.documento.getMateria();
//			}
//		}
//		return materia;
//	}
//
//	@Override
//	public void setMateria(final String materia) {
//		this.materia = materia;
//	}

	@Override
	public List<SelectItem> getListTipoDocumentos() {
		if (listTipoDocumentos == null) {
			listTipoDocumentos = this.buscarTipoDocumentos();
		}
		return listTipoDocumentos;
	}

	@Override
	public List<SelectItem> getListTipoDocumentosPapel() {
		if (listTipoDocumentos == null) {
			listTipoDocumentos = this.buscarTipoDocumentosPapel();
		}
		return listTipoDocumentos;
	}

	@Override
	public Long getTipoDocumentoID() {
		if (this.documento != null && this.documento.getTipoDocumento() != null) {
			this.tipoDocumentoID = this.documento.getTipoDocumento().getId()
					.longValue();
		}
		return tipoDocumentoID;
	}

	@Override
	public void setTipoDocumentoID(final Long tipoDocumentoID) {
		this.tipoDocumentoID = tipoDocumentoID;
	}

	@Override
	public List<SelectPersonas> getDestinatariosDocumento() {
		
		if (destinatariosDocumento == null && this.documento != null
				&& this.documento.getDestinatarios() != null
				&& this.documento.getDestinatarios().size() != 0) {
			
			for (ListaPersonasDocumento lpd : this.documento.getDestinatarios()) {
				destinatariosDocumento = new ArrayList<SelectPersonas>();
				SelectPersonas persona = new SelectPersonas(lpd.getDestinatario(), lpd.getDestinatarioPersona());
				if(!destinatariosDocumento.contains(persona))destinatariosDocumento.add(persona);
			}
		}
		
		return destinatariosDocumento;
	}

	@Override
	public DocumentoPapel getDocumento() {
		return documento;
	}

	@Override
	public void setDocumento(final DocumentoPapel documento) {
		this.documento = documento;
	}

	@Override
	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}

	@Override
	public void setDocumentoOriginal(final Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}

	@Override
	public Long getCodigoBarra() {
		log.info("Codigo de barra ", documentoOriginal.getCodigoBarra());
		return documentoOriginal.getCodigoBarra();
	}

	@Override
	public void setCodigoBarra(final Long codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	@Override
	public List<Persona> autocomplete(Object suggest) {

		final String pref = (String) suggest;

		List<Persona> personas = adminPersona.getPersonas(pref,
				destinatariosDocumento);

		return personas;
	}

	public void seleccionarDest(Persona p) {

		SelectPersonas select = new SelectPersonas(p.getNombreApellido()
				+ " - " + p.getCargo().getDescripcion() + " - "
				+ p.getCargo().getUnidadOrganizacional().getDescripcion(), p);
		if (!destinatariosDocumento.contains(select)) {
			destinatariosDocumento.add(select);
		} else {
			FacesMessages.instance().add(NO_PUEDE_AGREGAR);
		}
	}

	@Override
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	@Override
	public String getDestinatario() {
		return this.destinatario;
	}
	@Override
	public void removeDestinatarios(Long id) {
		md.removeDestinatarios(id);
	}
	private void cargarNivelesUrgencia() {

		if (this.getDocumento() != null
				&& this.getDocumento().getAlerta() != null
				&& this.getDocumento().getId() != null) {
			this.setIdAlerta(this.getDocumento().getAlerta().getId());
		} else {
			this.setIdAlerta(JerarquiasLocal.INICIO);
		}

		final List<Alerta> alertas = administradorAlertas.buscarAlerta();
		this.setNivelesUrgencia(new ArrayList<SelectItem>());

		this.getNivelesUrgencia().add(
				new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR, SELECCIONAR, false, true));

		if (alertas != null) {
			for (Alerta a : alertas) {
				this.getNivelesUrgencia().add(
						new SelectItem(a.getId(), a.getNombre()));
			}
		}
	}

	@Override
	public final List<SelectItem> getNivelesUrgencia() {
		if(nivelesUrgencia == null)
			this.cargarNivelesUrgencia();
		return nivelesUrgencia;
	}

	@Override
	public final void setNivelesUrgencia(final List<SelectItem> nivelesUrgencia) {
		this.nivelesUrgencia = nivelesUrgencia;
	}
	

	@Override
	public final Long getIdAlerta() {
		return idAlerta;
	}

	@Override
	public final void setIdAlerta(final Long idAlerta) {
		this.idAlerta = idAlerta;
	}

	@Override
	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
//		se comenta esta condicion porque bloquea situaciones que no debe
//		if(this.expediente.getDestinatarioHistorico() != null){
//			if(this.expediente.getDestinatarioHistorico().equals(usuario)) {return false;}
//		}
		if(this.expediente != null && this.expediente.getId() != null) {return true;}
		return false;
	
	}


}
