package cl.exe.exedoc.session;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.xml.xpath.XPathExpressionException;

import org.hibernate.LazyInitializationException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.core.Conversation;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaDocumento;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.RevisarDocumentos;
import cl.exe.exedoc.entity.RevisarDocumentosHistorial;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.TipoPlantilla;
import cl.exe.exedoc.entity.TipoRazon;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.mantenedores.AdministradorPersonaDocumento;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Ricardo Fuentes
 */
@Stateful
@Name("crearResolucion")
public class CrearResolucionBean implements CrearResolucion {
	private static final String SELECCIONAR = "<<Seleccionar>>";

	private static final String DELETE_FROM_REVISAR_ESTRUCTURADA_WHERE_ID = "DELETE FROM RevisarEstructuradaDocumento ve WHERE ve.id = ?";
	private static final String BLANK = " ";
	private static final String CREAR_RESOLUCION = "crearResolucion";
	private static final String STRING = "/";
	private static final long ID_CARGO_JEFATURA_DIVISION_ADM_FINANZAS = 3;
	private static final long ID_CARGO_JEFATURA_RR_HH = 4;
	private static final long ID_CARGO_JEFE_UNIDAD_OF_PARTES = 16;
	private static final long ID_CARGO_JEFE_UNIDAD_ABASTECIMIENTO = 21;
	private static final long ID_CARGO_FISCAL = 71;
	private Long rand = 0L;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private AdministradorAlertas administradorAlertas;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private AdministradorPersonaDocumento admPersonaDocumento;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	@Out(required = false)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Resolucion resolucion;

	@In(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;
	
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long idDepartamento;

	private List<Long> idExpedientes;

	private Boolean firmado;
	private Boolean visado;
	private boolean guardado;
	private Boolean anulado;

	private Integer tipoResolucion = 1;

	private String tipoArchivo;

//	private Plantilla plantilla;

	private String comentarios;
	private String observacionRechazo;
	private boolean aprobado;
	private Long idAlerta;
	private Resolucion resAuxiliar;

	private List<ArchivoAdjuntoDocumentoElectronico> archivosAnexos;
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;
	private String materiaArchivo;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	public List<Resolucion> hijas = new ArrayList<Resolucion>();

	private List<SelectItem> nivelesUrgencia;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<RevisarEstructuradaDocumento> listRevisarEstructuradaEliminadas;
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<VisacionEstructuradaDocumento> listVisacionEstructuradaEliminada;
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<FirmaEstructuradaDocumento> listFirmaEstructuradaEliminadas;
	/**
	 * Constructor.
	 */
	public CrearResolucionBean() {
		super();
	}

	@Override
	public String buscaDocumento() {   
		log.info("buscando doc..." + resolucion.getId());

		resolucion = em.find(Resolucion.class, resolucion.getId());
		if (resolucion.getFirmas() == null || resolucion.getFirmas().isEmpty()) {
			this.armaDocumento(false, false, false, false);
		}

		//firmado = true;
		resolucion.getParrafos().size();
		resolucion.getDistribucionDocumento().size();
		resolucion.getVisaciones().size();
		resolucion.getFirmas().size();
		resolucion.getBitacoras().size();
		resolucion.getRevisarEstructuradas().size();
		resolucion.getVisacionesEstructuradas().size();
		resolucion.getFirmasEstructuradas().size();
		resolucion.getArchivosAdjuntos().size();
		resolucion.getRevisar().size();
//		resolucion.getResoluciones().size();
		

		if (documentoOriginal.getId().equals(resolucion.getId())) {
			documentoOriginal.setNumeroDocumento(resolucion.getNumeroDocumento());
			documentoOriginal.setFechaDocumentoOrigen(resolucion.getFechaDocumentoOrigen());
			documentoOriginal.setEstado(resolucion.getEstado());
			documentoOriginal.setFirmas(resolucion.getFirmas());
		} else {
			for (Documento d : listDocumentosRespuesta) {
				if (d.getId().equals(resolucion.getId())) {
					d.setNumeroDocumento(resolucion.getNumeroDocumento());
					d.setFechaDocumentoOrigen(resolucion.getFechaDocumentoOrigen());
					d.setEstado(resolucion.getEstado());
					d.setFirmas(resolucion.getFirmas());
					d.setEliminable(false);
					break;
				}
			}
		}

		return "";
	}
	
	@Override
	public String buscaDocumento2() {
//		agregarResolucionHijaAExpediente();
		List<Persona> destinatarios = new ArrayList<Persona>();
		List<ListaPersonasDocumento> dis = resolucion.getDistribucionDocumento();
		for (ListaPersonasDocumento listaPersonasDocumento : dis) {
			if (listaPersonasDocumento.getDestinatarioPersona() != null) {
				destinatarios.add(listaPersonasDocumento.getDestinatarioPersona());
			}
		}
		// THIS SHOULD BE EMPTY
//		List<VisacionEstructuradaDocumento> visadores = resolucion.getVisacionesEstructuradas();
//		for (VisacionEstructuradaDocumento visacionEstructuradaDocumento : visadores) {
//			destinatarios.add(visacionEstructuradaDocumento.getPersona());
//		}
		
		this.buscaDocumento();
		if (resolucion.getCmsId() != null)
			me.enviarMailNotificacionFirma(this.expediente, resolucion, destinatarios);
		return CREAR_RESOLUCION;
	}

	/**
	 * @return {@link Boolean}
	 */
	private Boolean documentoFirmado() {
		if (resolucion != null && resolucion.getId() != null && resolucion.getNumeroDocumento() != null) { return true; }
		return false;
	}

	/**
	 * @return {@link Long}
	 */
	@SuppressWarnings("unchecked")
	private Long existeSolDiaAdm() {
		final StringBuilder hql = new StringBuilder("select sda.id as id ");
		hql.append("from solicitud_dia_administrativo sda ");
		hql.append("where sda.id in ( ");
		hql.append("select d1.id_solicitud_adm ");
		hql.append("from documentos d1 ");
		hql.append("where d1.id in ( ");
		hql.append("select de1.id_documento ");
		hql.append("from documentos_expedientes de1 ");
		hql.append("where de1.id_expediente in ( ");
		hql.append("select e.id ");
		hql.append("from expedientes e ");
		hql.append("where e.id in ( ");
		hql.append("select de.id_expediente ");
		hql.append("from documentos_expedientes de ");
		hql.append("where de.id_documento= ");
		hql.append(resolucion.getId());
		hql.append(") ");
		hql.append(") ");
		hql.append(") ");
		hql.append(") ");
		hql.append("and (sda.aceptado= '0' or sda.aceptado is null) ");
		hql.append("and (sda.rechazado= '0' or sda.rechazado is null) ");

		final Query query = em.createNativeQuery(hql.toString());
		final List<BigInteger> arrayObject = query.getResultList();
		Long idSolDiaAdm = null;
		for (BigInteger o : arrayObject) {
			idSolDiaAdm = (((BigInteger) o) != null) ? ((BigInteger) o).longValue() : null;
		}
		return idSolDiaAdm;
	}

	@SuppressWarnings("unchecked")
	private Long existeSolVacaciones() {
		final StringBuilder hql = new StringBuilder("select sv.id as id ");
		hql.append("from solicitud_vacaciones sv ");
		hql.append("where sv.id in ( ");
		hql.append("select d1.id_solicitud_vacaciones ");
		hql.append("from documentos d1 ");
		hql.append("where d1.id in ( ");
		hql.append("select de1.id_documento ");
		hql.append("from documentos_expedientes de1 ");
		hql.append("where de1.id_expediente in ( ");
		hql.append("select e.id ");
		hql.append("from expedientes e ");
		hql.append("where e.id in ( ");
		hql.append("select de.id_expediente ");
		hql.append("from documentos_expedientes de ");
		hql.append("where de.id_documento= ");
		hql.append(resolucion.getId());
		hql.append(") ");
		hql.append(") ");
		hql.append(") ");
		hql.append(") ");
		hql.append("and (sv.aceptado= '0' or sv.aceptado is null) ");
		hql.append("and (sv.rechazado= '0' or sv.rechazado is null)");

		final Query query = em.createNativeQuery(hql.toString());
		log.info("[filtrarDocumentosEnBandejaEntrada] SQL a ejecutar: " + hql.toString());
		final List<BigInteger> arrayObject = query.getResultList();
		Long idSolVaca = null;
		for (BigInteger o : arrayObject) {
			idSolVaca = (((BigInteger) o) != null) ? ((BigInteger) o).longValue() : null;
		}
		return idSolVaca;
	}

	@Override
	public boolean isEditable() {
		Boolean estado = true;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				estado = Boolean.FALSE;
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			estado = Boolean.FALSE;
		}
		if (resolucion.getVisaciones() != null && resolucion.getVisaciones().size() > 0) {
			return Boolean.FALSE;
		}
		if (resolucion.getFirmas() != null && resolucion.getFirmas().size() > 0) {
			return Boolean.FALSE;
		}
		
		List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		if(visaciones != null){
			for (VisacionEstructuradaDocumento visacionEstructuradaDocumento : visaciones) {
				if(visacionEstructuradaDocumento.getPersona().equals(usuario)){
					return Boolean.FALSE;
				}
			}
		}
		
		List<FirmaEstructuradaDocumento> firmantes = resolucion.getFirmasEstructuradas();
		if(firmantes != null){
			for (FirmaEstructuradaDocumento firmaEstructuradaDocumento : firmantes) {
				if(firmaEstructuradaDocumento.getPersona().equals(usuario)){
					return Boolean.FALSE;
				}
			}
		}
		
		if (resolucion.getEstado() != null) {
			if (resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)) {
				estado = Boolean.FALSE;
			}
		}
		if (this.getVisado()) {
			estado = Boolean.FALSE;
		}
		if (this.getFirmado()) {
			estado = Boolean.FALSE;
		}
		if (this.getAnulado()) {
			estado = Boolean.FALSE;
		}
		return estado;
	}

	@Override
	public void limpiarAnexos() {
		if (archivosAnexos == null) {
			archivosAnexos = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		} else {
			archivosAnexos.clear();
		}
		materiaArchivo = null;
	}

	@Override
	public int getMaxFilesQuantity() {
		final int valor = 20;
		/*
		 * if (archivosAnexos == null || archivosAnexos.isEmpty()) { valor = 1; } else { valor = archivosAnexos.size();
		 * }
		 */
		return valor;
	}

	@Override
	public void agregarAntecedente() {
		if (this.resolucion.getArchivosAdjuntos() == null) {
			this.resolucion.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		}
		if (!archivosAnexos.isEmpty()) {
			for (ArchivoAdjuntoDocumentoElectronico archivo : archivosAnexos) {
				if (true/* !existeNombreArchivo(archivo.getNombreArchivo()) */) {
					archivo.setDocumentoElectronico(this.resolucion);
					archivo.setFecha(new Date());
					archivo.setIdNuevoArchivo(this.resolucion.getArchivosAdjuntos().size());
					archivo.setAdjuntadoPor(usuario);
					archivo.setMateria(materiaArchivo);
					this.resolucion.getArchivosAdjuntos().add(archivo.archivo());
				}
			}
		}
		this.limpiarAnexos();
	}

	@Override
	public void eliminarArchivoAntecedente(final Integer idNuevoArchivo) {
		if (this.listaArchivosEliminados == null) {
			this.listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		}

		for (ArchivoAdjuntoDocumentoElectronico a : this.resolucion.getArchivosAdjuntos()) {
			if (a.getIdNuevoArchivo().equals(idNuevoArchivo)) {
				if (a.getId() != null) {
					this.listaArchivosEliminados.add(a);
				}
				this.resolucion.getArchivosAdjuntos().remove(a);
				break;
			}
		}
	}

	@Override
	public boolean isRechazado() {
		Boolean estado = Boolean.FALSE;
		if (resolucion.getEstado() != null && resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR)) {
			estado = Boolean.TRUE;
		}

		return estado;
	}

	@Override
	public boolean isAprobado() {
		Boolean estado = Boolean.FALSE;
		if (resolucion.getEstado() != null && resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR)) {
			estado = Boolean.TRUE;
		}

		return estado;
	}

	@Override
	public boolean isCompletadoBorrador() {
		List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		if(visaciones != null){
			for (VisacionEstructuradaDocumento visacionEstructuradaDocumento : visaciones) {
				if(visacionEstructuradaDocumento.getPersona().equals(usuario)) return Boolean.FALSE;
			}
		}
		Boolean completado = (this.isRechazado() || this.isAprobado()) && resolucion.getAutor().equals(this.usuario);
		return completado;
	}

	@Override
	public boolean isBorrador() {

		if (resolucion.getEstado() != null && resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR)
				&& !resolucion.getAutor().equals(this.usuario)) { return true; }

		return false;
	}
	@Override
	public boolean isBorradorRevisadoYSoyRevisor() {

		if (resolucion.getEstado() != null)  /*resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)
													||
											*/
			if( resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_RECHAZADO))
				for(RevisarDocumentos rev: resolucion.getRevisar())
					if(rev.getPersona().equals(this.usuario)) {
						this.comentarios = rev.getComentario();
						return true; 
					}

		return false;
	}
	

	@Override
	public void listener(final UploadEvent event) throws Exception {
		final ArchivoAdjuntoDocumentoElectronico archivo = new ArchivoAdjuntoDocumentoElectronico();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
		archivosAnexos.add(archivo);
	}

	@Override
	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	@Override
	public void setMateriaArchivo(final String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	@Override
	public void verArchivo(final Long id) {
		try {
			if (!DocUtils.getFile(id, em)) {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: " + id + ")");
			}
		} catch (XPathExpressionException e) {
			log.error("No existe el archivo, consulte con el administrador", e);
		} catch (IOException e) {
			log.error("No existe el archivo, ", e);
		}
	}

	@Override
	public void salirPanel() {
		log.info("SALIR PANEL");
		this.limpiarAnexos();
	}

	@Override
	public void visualizar() {
		if (!firmado && !visado) {
			this.armaDocumento(false, false, false, false);
		}
	}

	@Override
	public void firmar() {
		for (FirmaEstructuradaDocumento fed : resolucion.getFirmasEstructuradas()) {
			if (fed.getPersona().equals(usuario)) {
				this.armaDocumento(false, true, false, false);
				final Query query = em.createQuery("DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.id = ?");
				query.setParameter(1, fed.getId());
				resolucion.getFirmasEstructuradas().remove(0);
				query.executeUpdate();
				resolucion.getFirmasEstructuradas().remove(fed);

				salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
					if (de.getDocumento().getId().equals(resolucion.getId())) {
						for (FirmaEstructuradaDocumento fe : de.getDocumento().getFirmasEstructuradas()) {
							if (fe.getPersona().equals(usuario)) {
								de.getDocumento().getFirmasEstructuradas().remove(fe);
								break salida;
							}
						}
					}
				}
				FacesMessages.instance().add("Resolución firmada.");
				break;
			}
		}
		guardado = true;
	}

	@Override
	public String visar() {
		boolean permisoVisa = false;
			VisacionEstructuradaDocumento ved = null;
			for (VisacionEstructuradaDocumento ve : resolucion.getVisacionesEstructuradas()) {
				if (ve.getPersona().equals(usuario)) {
				permisoVisa = true;
					ved = ve;
					break;
				}
			}
		if (permisoVisa) {
			this.armaDocumento(true, false, false, false);

			if (ved != null && ved.getId() != null) {
				final Query query = em.createQuery("DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.id = ?");
				query.setParameter(1, ved.getId());
				query.executeUpdate();
				resolucion = em.find(Resolucion.class , resolucion.getId());
				resolucion.getVisacionesEstructuradas().remove(ved);
				// modificación para que vcuando se vise quede el siguiente como destinatario
				if(resolucion.getVisacionesEstructuradas().size() > 0){
					this.listDestinatarios.add(resolucion.getVisacionesEstructuradas().get(0).getPersona());
				}
				else{
					if(resolucion.getFirmasEstructuradas() != null && resolucion.getFirmasEstructuradas().size() > 0){
						this.listDestinatarios.add(resolucion.getFirmasEstructuradas().get(0).getPersona());
					}
				}
				// fin validación
			}

			salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
				if (de.getDocumento().getId().equals(resolucion.getId())) {
					for (VisacionEstructuradaDocumento ve : de.getDocumento().getVisacionesEstructuradas()) {
						if (ve.getPersona().equals(usuario)) {
							de.getDocumento().getVisacionesEstructuradas().remove(ve);
							break salida;
						}
					}
				}
			}
			FacesMessages.instance().add("Resolución visada");
			guardado = true;
			resolucion.setEliminable(false);
			return homeCrear;
		} else {
			FacesMessages.instance().add("Ud. No puede Visar la Resolución");
		}

		guardado = true;
		return "";
	}

	@Override
	
	public String end() {
		distribucionDocumento = new ArrayList<SelectPersonas>();
		listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		this.limpiarAnexos();
		guardado = false;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}

		/*final List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		final List<VisacionDocumento> visacionesHechas = resolucion.getVisaciones();
		final List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
		final List<RevisarEstructuradaDocumento> revisar = resolucion.getRevisarEstructuradas();
		if (revisar == null || (visacionesHechas != null && visacionesHechas.size() == 0) && 
				this.resolucion.getAutor().equals(usuario) && 
				((visaciones != null && visaciones.size() > 0) || (firmas != null && firmas.size() > 0))) {
			listDestinatarios.clear();
			Collections.sort(visaciones, ComparatorUtils.visasComparator);
			if(visaciones.size()!=0 && visaciones !=null){
			listDestinatarios.add(visaciones.get(0).getPersona());
			}
		}*/
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente", "inicio", new Date());
		
//		if (revisar != null && revisar.size() > 0) {
//			if (documentoOriginal != null && documentoOriginal.getAutor() != null && documentoOriginal.getAutor().getId().equals(usuario.getId())) {
//				listDestinatarios.add(revisar.get(0).getPersona());
//			}
//			resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));
//		} else if (visaciones != null && visaciones.size() > 0 ) {
//			listDestinatarios.add(visaciones.get(0).getPersona());
//		}
		
		listRevisarEstructuradaEliminadas = null;
		listVisacionEstructuradaEliminada = null;
		listFirmaEstructuradaEliminadas = null;
		vistoDesdeReporte = null;
		//this.resolucion=null;
		hijas=new ArrayList<Resolucion>();
		log.info(homeCrear);
		if (resolucion.getId() != null && expediente != null && expediente.getId() != null) {
			resolucion.setEliminable(md.isEliminable(resolucion, expediente, usuario));
		}
		return homeCrear;
	}

	@Override
	public String agregarDocumento() {
		// TODO faltan condiciones
		documentoValido = true;
		if (resolucion.getMateria() == null || (resolucion.getMateria() != null && resolucion.getMateria().trim().isEmpty())) {
			FacesMessages.instance().add("La Materia es obligatoria");
			documentoValido = false;
			return "";
		}
		long start = System.currentTimeMillis();
		final List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		final List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
		final List<RevisarEstructuradaDocumento> revisar = resolucion.getRevisarEstructuradas();
		if (this.getIdAlerta() != null && this.getIdAlerta().longValue() != 0) {
			this.getResolucion().setAlerta(
					administradorAlertas.buscarAlerta(this.getIdAlerta()));
		}
		this.armaDocumento(false, false, false, false);
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(),
				"Crear Resolucion", System.currentTimeMillis() - start);

		
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente", "inicio", new Date());

//		if (revisar != null && revisar.size() > 0) {
//			listDestinatarios.add(revisar.get(0).getPersona());
//			resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));
//		} else if (visaciones != null && visaciones.size() > 0) {
//			listDestinatarios.add(visaciones.get(0).getPersona());
//		} else if (firmas != null && firmas.size() > 0) {
//			listDestinatarios.add(firmas.get(0).getPersona());
//		}
		
		return this.end();
	}

	/**
	 * Metodo para limpiar los archivos adjuntados
	 * 
	 */
	@Override
	public void clearUploadData() {
		log.info("[Crear Resolucion] Limpiando archivo");
		archivosAnexos.remove(0);
	}
	
	@Override
	public void anularResolucion() {
		armaDocumento(false, false, false, true);
	}
	
	private void guardarHistorialRevisor(RevisarDocumentos revisar) {
		
		RevisarDocumentosHistorial revisarHistorial = new RevisarDocumentosHistorial(revisar);
		try {
			em.persist(revisarHistorial);
		} catch (Exception e) {
			log.error("Error al persistir el historial de revision.", e);
		}
	}

	private void armaDocumentoRechazo(){
		this.configurarArchivosAdjuntos();
		this.imagenEstadoResolucion();
		this.resolucion.setEnEdicion(true);//vuelve a estar abilitado para editarse
		this.resolucion.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		// TipoDocumento: 6,'Resolucion'
		this.resolucion.setTipoDocumento(new TipoDocumento(TipoDocumento.RESOLUCION));
		final boolean modificarVisaFirmas = this.modificarVisaFirmas();
		this.resolucion.addBitacora(new Bitacora(EstadoDocumento.BORRADOR_RECHAZADO, this.resolucion, this.usuario));
		this.resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));

		Observacion obsRechazo = new Observacion();
		obsRechazo.setObservacion(observacionRechazo);
		obsRechazo.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_RESOLUCION));
		obsRechazo.setAutor(this.usuario);
		obsRechazo.setExpediente(this.expediente);
		obsRechazo.setFecha(new Date());
		observaciones.add(obsRechazo);
		
		//NUEVO FLUJO
		List<RevisarEstructuradaDocumento> nuevoRevisar = new LinkedList<RevisarEstructuradaDocumento>();
		List<RevisarEstructuradaDocumento> revisar = new LinkedList<RevisarEstructuradaDocumento>();
		List<VisacionEstructuradaDocumento> nuevoVisar = new LinkedList<VisacionEstructuradaDocumento>();
		List<FirmaEstructuradaDocumento> nuevoFirmar = new LinkedList<FirmaEstructuradaDocumento>();
		//MANDAR MAILS Y RECONSTRUIR FLUJO
		if(this.resolucion.getRevisar()!=null)
			Collections.sort(this.resolucion.getRevisar(), ComparatorUtils.revisarDocumentosComparatorByDate);
			for(RevisarDocumentos rev: this.resolucion.getRevisar()){
				boolean revi = false;
				//log.info("REV MAIL A: ("+rev.getPersona().getUsuario()+") "+rev.getPersona().getNombreApellido());
				RevisarEstructuradaDocumento ved = new RevisarEstructuradaDocumento();
				ved.setOrden(nuevoRevisar.size() + 1);
				ved.setPersona(rev.getPersona());
				ved.setDocumento(this.resolucion);
				if(nuevoRevisar.size()==0){
				nuevoRevisar.add(ved);
				}else{
				for(RevisarEstructuradaDocumento pe:nuevoRevisar){					
					if(pe.getPersona().getId().equals(ved.getPersona().getId())){
						revi=true;
						}					
				}
					if(!revi)
						nuevoRevisar.add(ved);	
				}
				this.guardarHistorialRevisor(rev);
			}
		if(this.resolucion.getVisaciones()!=null)
			Collections.sort(this.resolucion.getVisaciones(), ComparatorUtils.visasDocumentosComparatorByDate);
			for(VisacionDocumento rev: this.resolucion.getVisaciones()){
				log.info("VIS MAIL A: ("+rev.getPersona().getUsuario()+") "+rev.getPersona().getNombreApellido());
				md.notificarRechazoResolucion(rev.getPersona(), this.resolucion, obsRechazo);
				VisacionEstructuradaDocumento ved = new VisacionEstructuradaDocumento();
				ved.setOrden(nuevoVisar.size() + 1);
				ved.setPersona(rev.getPersona());
				ved.setDocumento(this.resolucion);
				nuevoVisar.add(ved);
			}
		//Sacar visaciones pendientes
		if(this.resolucion.getVisacionesEstructuradas()!=null)
			Collections.sort(this.resolucion.getVisacionesEstructuradas(), ComparatorUtils.visasComparator);
			for(VisacionEstructuradaDocumento rev: this.resolucion.getVisacionesEstructuradas()){
				log.info("VIS Pendiente MAIL A: ("+rev.getPersona().getUsuario()+") "+rev.getPersona().getNombreApellido());
				rev.setOrden(nuevoVisar.size() + 1);
				nuevoVisar.add(rev);
			}
		md.limpiarFlujoDocumento(this.resolucion.getId());
		this.resolucion.setVisacionesEstructuradas(nuevoVisar);
		this.resolucion.setRevisarEstructuradas(nuevoRevisar);
		//LIMPIAR VISAS Y FIRMAS
		this.resolucion.setVisaciones(new LinkedList<VisacionDocumento>());
		this.resolucion.setFirmas(new LinkedList<FirmaDocumento>());

		this.resolucion.setReservado(false);
		this.resolucion.setTipo(new TipoRazon(tipoResolucion));
		this.limpiarDestinatarios(this.resolucion.getId());
		this.setDistribucionDocumento(this.resolucion);
		this.actualizaListasDocumentos();
		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		if (modificarVisaFirmas) {
			this.actualizaVisaFirma();
		}
		if (this.listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico doc : this.listaArchivosEliminados) {
				if (doc.getId() != null)
					this.eliminaAntecedente(doc.getId());
			}
		}
		this.listaArchivosEliminados = null;
		this.persistirDocumento();
		
	}
	/**
	 * @param visar {@link boolean}
	 * @param firmar {@link boolean}
	 * @param revisar {@link boolean}
	 */
	private void armaDocumento(final boolean visar, final boolean firmar, final boolean revisar, final Boolean anulado) {
		if (this.getFirmado()) { return; }
		if (this.getAnulado()) { return; }
		this.resolucion.setPlazo(null);
		this.configurarArchivosAdjuntos();
		this.imagenEstadoResolucion();

		this.resolucion.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		// TipoDocumento: 6,'Resolucion'
		this.resolucion.setTipoDocumento(new TipoDocumento(TipoDocumento.RESOLUCION));

		final boolean modificarVisaFirmas = this.modificarVisaFirmas();
		// if (modificarVisaFirmas) {
		// if (this.resolucion.getVisacionesEstructuradas() != null
		// && !this.resolucion.getVisacionesEstructuradas().isEmpty()) {
		// this.eliminarVisasEstructuradas();
		// }
		// if (this.resolucion.getFirmasEstructuradas() != null
		// && !this.resolucion.getFirmasEstructuradas().isEmpty()) {
		// this.eliminarFirmasEstructuradas();
		// }
		// }

		if (!visar && !firmar && !revisar && !anulado) {
			if (this.resolucion.getId() != null) {
				this.resolucion.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
				this.resolucion.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.resolucion, this.usuario));
				this.getNextDestinatario();
//				final List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
//				final List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
//				final List<RevisarEstructuradaDocumento> revisarLis = resolucion.getRevisarEstructuradas();
//				boolean entro = false;
//				listDestinatarios.clear();
//				if (revisarLis != null) {
//					List<Long> idsValidos = new LinkedList<Long>();
//					for(RevisarEstructuradaDocumento rev: revisarLis)
//						if(rev.getId()!=null)
//							idsValidos.add(rev.getId());
//					if(idsValidos.size()==0)
//						idsValidos.add(new Long(-1L));
//						
//						
//					md.limpiarRevisionesEstructuradasBorradas(this.resolucion.getId(),idsValidos);
//					if(revisarLis.size() > 0){
//						listDestinatarios.clear();
//						listDestinatarios.add(revisarLis.get(0).getPersona());
//						this.resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));
//						entro = true;
//					}
//				}
//				if (visaciones != null) {
//					List<Long> idsValidos = new LinkedList<Long>();
//					for(VisacionEstructuradaDocumento rev: visaciones)
//						if(rev.getId()!=null)
//							idsValidos.add(rev.getId());
//					if(idsValidos.size()==0)
//						idsValidos.add(new Long(-1L));
//					md.limpiarVisacionesEstructuradasBorradas(this.resolucion.getId(),idsValidos);
//					if(visaciones.size() > 0 && !entro){
//						listDestinatarios.clear();
//						listDestinatarios.add(visaciones.get(0).getPersona());
//						entro = true;
//					}
//				}
//				if (firmas != null) {
//					List<Long> idsValidos = new LinkedList<Long>();
//					for(FirmaEstructuradaDocumento rev: firmas)
//						if(rev.getId()!=null)
//							idsValidos.add(rev.getId());
//
//					if(idsValidos.size()==0)
//						idsValidos.add(new Long(-1L));
//					md.limpiarFirmasEstructuradasBorradas(this.resolucion.getId(),idsValidos);
//					if(firmas.size() > 0&& !entro){
//						listDestinatarios.clear();
//						listDestinatarios.add(firmas.get(0).getPersona());
//						entro = true;
//					}
//				}
			} else if (!revisar) {
				this.resolucion.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
				this.resolucion.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.resolucion, this.usuario));
			} else if (revisar && !this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_RECHAZADO)
					&& !this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)) {
				this.resolucion.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.BORRADOR));
				this.resolucion.addBitacora(new Bitacora(EstadoDocumento.BORRADOR, this.resolucion, this.usuario));
			} else if (revisar && this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)) {
				this.resolucion.addBitacora(new Bitacora(EstadoDocumento.BORRADOR_APROBADO, this.resolucion,
						this.usuario));
			}
		}
		if (firmar) {
			this.repositorio.firmar(this.resolucion, usuario);
			this.resolucion.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.resolucion, this.usuario));
			// Pruebas mias
			final List<Expediente> padre = me.obtienePrimerExpediene(expediente);
			final List<Persona> dest = me.buscarDestinatarios(padre);
			for (Persona p : dest) {
				me.enviarMailPorDestinatarios(p, 1, this.expediente);
			}
		}
		if (visar) {
			this.repositorio.visar(this.resolucion, usuario);
			this.resolucion.addBitacora(new Bitacora(EstadoDocumento.VISADO, this.resolucion, this.usuario));
		}
		if (revisar) {
			this.repositorio.revisar(this.resolucion, usuario, this.aprobado, this.comentarios);
			if (this.aprobado)
			this.resolucion.addBitacora(new Bitacora(EstadoDocumento.BORRADOR_APROBADO, this.resolucion, this.usuario));
			else
				this.resolucion.addBitacora(new Bitacora(EstadoDocumento.BORRADOR_RECHAZADO, this.resolucion, this.usuario));
		}
		if (anulado) {
			this.resolucion.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.BORRADOR));
			this.resolucion.addBitacora(new Bitacora(EstadoDocumento.ANULADO, this.resolucion, this.usuario));
		}

		this.resolucion.setReservado(false);
		if(tipoResolucion == null) tipoResolucion = TipoRazon.EXENTO;
		this.resolucion.setTipo(new TipoRazon(tipoResolucion));

		this.limpiarDestinatarios(this.resolucion.getId());
		this.setDistribucionDocumento(this.resolucion);
		
		this.actualizaListasDocumentos();

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		if (modificarVisaFirmas) {
			this.actualizaVisaFirma();
		}
		
		if (this.listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico doc : this.listaArchivosEliminados) {
				if (doc.getId() != null)
					this.eliminaAntecedente(doc.getId());
			}
		}
		this.listaArchivosEliminados = null;

		this.persistirDocumento();
	}

	/**
	 * @param idTipo {@link Integer}
	 * @return {@link String}
	 */
	private String getDescripcionTipo(final Integer idTipo) {
		// if (idTipo != null && idTipo != -1) {
		// return (String)
		// em.createNativeQuery("select descripcion from clasificacion_tipo where id = :id").setParameter("id",
		// idTipo).getSingleResult();
		// }
		if (idTipo != null && idTipo != -1) { return (String) em
				.createNativeQuery("select ct.descripcion from ClasificacionTipo ct where ct.id = :id")
				.setParameter("id", idTipo).getSingleResult(); }
		return "";
	}

	private String getDescripcionSubtipo(final Integer idSubtipo) {
		// if (idSubtipo != null && idSubtipo != -1) {
		// return (String)
		// em.createNativeQuery("select descripcion from clasificacion_subtipo where id = :id").setParameter("id",
		// idSubtipo).getSingleResult();
		// }
		if (idSubtipo != null && idSubtipo != -1) { return (String) em
				.createNativeQuery("select cs.descripcion from ClasificacionSubtipo cs where cs.id = :id")
				.setParameter("id", idSubtipo).getSingleResult(); }
		return "";
	}

	private void configurarArchivosAdjuntos() {
		if (listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico a : listaArchivosEliminados) {
				// String sqlBorra = "DELETE FROM ARCHIVOS WHERE id = " +
				// a.getId();
				// em.createNativeQuery(sqlBorra).executeUpdate();
				final String sqlBorra = "DELETE FROM Archivo a WHERE  a.id = " + a.getId();
				em.createQuery(sqlBorra).executeUpdate();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizaVisaFirma() {
		if (this.resolucion.getId() != null) {
			// final String sqlVisasActuales =
			// "SELECT ve FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = "
			// + resolucion.getId() + " order by orden asc";
			// final List<VisacionEstructuradaDocumento> visas =
			// em.createQuery(sqlVisasActuales).getResultList();
			// this.resolucion.getVisacionesEstructuradas().clear();
			// this.resolucion.getVisacionesEstructuradas().addAll(visas);
			//
			// final String sqlFirmasActuales =
			// "SELECT fe FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = "
			// + resolucion.getId() + " order by orden asc";
			// final List<FirmaEstructuradaDocumento> firmas =
			// em.createQuery(sqlFirmasActuales).getResultList();
			// this.resolucion.getFirmasEstructuradas().clear();
			// this.resolucion.getFirmasEstructuradas().addAll(firmas);
		}
	}

	private void actualizaListasDocumentos() {
		if (this.resolucion.getTipoDocumentoExpediente() != null) {
			if (!this.resolucion.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
				if (!existeDocumento(listDocumentosRespuesta)) {
					this.resolucion.setEliminable(true);
					if(this.expediente != null && (this.resolucion.getId() == null || !this.resolucion.getId().equals(this.documentoOriginal.getId())))
						listDocumentosRespuesta.add(this.resolucion);
					resAuxiliar = this.resolucion;
				} else {
					if (this.resolucion.getId() != null) {
						listDocumentosRespuesta.remove(this.resolucion);
						listDocumentosRespuesta.add(this.resolucion);
					} else {
						this.reemplazaDocumento(listDocumentosRespuesta);
					}
				}
			}/*
			 * else if (this.resolucion.getTipoDocumentoExpediente().getId().equals (TipoDocumentoExpediente.ANEXO)) {
			 * if (!existeDocumento(listDocumentosAnexo)) { listDocumentosAnexo.add(this.resolucion); } else { if
			 * (this.resolucion.getId() != null) { listDocumentosAnexo.remove(this.resolucion);
			 * listDocumentosAnexo.add(this.resolucion); } else { this.reemplazaDocumento(listDocumentosAnexo); } } }
			 */
		}
	}

	private void persistirDocumento() {
		try {
			if (this.resolucion.getId() != null) {
				md.actualizarDocumento(this.resolucion);

			} else if (this.expediente != null && this.expediente.getId() != null) {
				me.agregarRespuestaAExpediente(expediente, this.resolucion, true);
			}
		} catch (DocumentNotUploadedException e) {
			e.printStackTrace();
		}
	}

	private void eliminarVisasEstructuradas() {
		if (this.resolucion.getId() != null) {
			// String sqlBorra =
			// "DELETE FROM VISACIONES_ESTRUCTURADAS WHERE id_documento = " +
			// resolucion.getId();
			// em.createNativeQuery(sqlBorra).executeUpdate();
			final String sqlBorra = "DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = "
					+ resolucion.getId();
			em.createQuery(sqlBorra).executeUpdate();
		}
	}

	private void eliminarFirmasEstructuradas() {
		if (this.resolucion.getId() != null) {
			// String sqlBorra =
			// "DELETE FROM FIRMAS_ESTRUCTURADAS WHERE id_documento = " +
			// resolucion.getId();
			// em.createNativeQuery(sqlBorra).executeUpdate();
			final String sqlBorra = "DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = "
					+ resolucion.getId();
			em.createQuery(sqlBorra).executeUpdate();
		}
	}

	private boolean existeDocumento(final List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.resolucion.getIdNuevoDocumento())) { return true; }
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		final List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			final Documento docTmp = doc.next();
			final Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.resolucion.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.resolucion);
			}
		}
		listaDocumentos = lista;
	}

	/**
	 * 
	 */
	private void setDistribucionDocumento(Documento dc) {
		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			final DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDistribucion(ddList);

	}

	public boolean modificarVisaFirmas() {

		for (Rol r : usuario.getRoles()) {
			if (r.equals(Rol.INGRESAR_VISA_FIRMA)) { return true; }
		}
//		return false;
		return true;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Resolucion getResolucion() {
		if (resolucion == null) {
			resolucion = new Resolucion();
			final Calendar ahora = new GregorianCalendar();
			// resolucion.setFechaDocumentoOrigen(ahora.getTime());
			resolucion.setFechaCreacion(ahora.getTime());
			ahora.add(Calendar.DATE, 5);
			resolucion.setPlazo(ahora.getTime());

			resolucion.setAutor(usuario);
			// tipo temporal, Siempre se toma como original el primero.
			resolucion.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
			// id temporal para borrar un documento
			resolucion.setIdNuevoDocumento(Integer.MAX_VALUE);
			resolucion.setReservado(false);
			resolucion.setEnEdicion(true);
		}
		//else resolucion.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.ORIGINAL));
		return resolucion;
	}

	public void setResolucion(final Resolucion resolucion) {
		this.resolucion = resolucion;
	}

	public String getVistos() {
		String vistos = "";
		if (resolucion.getVistos() == null || resolucion.getVistos().isEmpty()) {
			final Vistos parrafo = new Vistos();
			parrafo.setCuerpo(vistos);
			parrafo.setDocumento(this.resolucion);
			parrafo.setNumero(1L);
			final List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.resolucion.setVistos(parrafos);
		}
		vistos = this.resolucion.getVistos().get(0).getCuerpo();
		return vistos;
	}

	public void setVistos(final String vistos) {
		final List<Parrafo> vis = this.resolucion.getVistos();
		vis.get(0).setCuerpo(vistos);
	}

	public String getConsiderando() {
		String considerando = "";
		final List<Parrafo> par = resolucion.getConsiderandos();
		if (resolucion.getConsiderandos() == null || resolucion.getConsiderandos().isEmpty()) {
			final Considerandos parrafo = new Considerandos();
			parrafo.setCuerpo(considerando);
			parrafo.setDocumento(this.resolucion);
			parrafo.setNumero(1L);
			final List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.resolucion.setConsiderandos(parrafos);
		}
		final List<Parrafo> cons = this.resolucion.getConsiderandos();
		final Parrafo parr = cons.get(0);
		considerando = parr.getCuerpo();
		return considerando;
	}

	public void setConsiderando(final String considerando) {
		final List<Parrafo> cons = this.resolucion.getConsiderandos();
		cons.get(0).setCuerpo(considerando);
	}

	public String getResuelvo() {
		String resuelvo = "";
		if (resolucion.getResuelvo() == null || resolucion.getResuelvo().isEmpty()) {
			final Resuelvo parrafo = new Resuelvo();
			parrafo.setCuerpo(resuelvo);
			parrafo.setDocumento(this.resolucion);
			parrafo.setNumero(1L);
			final List<Parrafo> parrafos = new ArrayList<Parrafo>();
			parrafos.add(parrafo);
			this.resolucion.setResuelvo(parrafos);
		}
		final List<Parrafo> res = this.resolucion.getResuelvo();
		final Parrafo parr = res.get(0);
		resuelvo = parr.getCuerpo();
		return resuelvo;
	}

	public void setResuelvo(final String resuelvo) {
		final List<Parrafo> res = this.resolucion.getResuelvo();
		res.get(0).setCuerpo(resuelvo);
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}

	public void setDistribucionDocumento(final List<SelectPersonas> distribucionDocumento) {
		this.distribucionDocumento = distribucionDocumento;
	}

	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos() {
		if (this.resolucion.getArchivosAdjuntos() != null) {
			int cont = 0;

			for (ArchivoAdjuntoDocumentoElectronico a : this.resolucion.getArchivosAdjuntos()) {
				a.setIdNuevoArchivo(cont++);
			}
			return this.resolucion.getArchivosAdjuntos();
		}
		return new ArrayList<ArchivoAdjuntoDocumentoElectronico>();

	}

	@Override
	public Boolean getAnulado() {
		anulado = false;
		if (this.resolucion != null && this.resolucion.getEstado() != null
				&& this.resolucion.getEstado().getId().equals(EstadoDocumento.ANULADO)) {
			anulado = Boolean.TRUE;
		} else {
			anulado = Boolean.FALSE;
		}
		return anulado;
	}

	public Boolean getFirmado() {
		firmado = false;
		if (this.resolucion != null && this.resolucion.getFirmas() != null && this.resolucion.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public Boolean getVisado() {
		visado = false;
		if (this.resolucion != null && this.resolucion.getVisaciones() != null
				&& this.resolucion.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public String getVisaciones() {
		if (this.resolucion.getVisacionesEstructuradas() == null) {
			this.resolucion.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
		}
		final List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		Collections.sort(visaciones, ComparatorUtils.visasComparator);

		final StringBuilder sb = new StringBuilder();
		for (int i = visaciones.size() - 1; i >= 0; i--) {
			sb.append(visaciones.get(i).getPersona().getNombreApellido() + STRING + BLANK);
		}
		// sb.append(resolucion.getAutor().getIniciales().toLowerCase());

		return sb.toString();
	}

	@Override
	public String getRevisarBorrador() {

		try {
			if (this.resolucion.getRevisarEstructuradas() == null) {
				this.resolucion.setRevisarEstructuradas(new ArrayList<RevisarEstructuradaDocumento>());
			}
			final List<RevisarEstructuradaDocumento> revisadores = resolucion.getRevisarEstructuradas();
			Collections.sort(revisadores, ComparatorUtils.revisarComparator);

			final StringBuilder sb = new StringBuilder();
			for (int i = revisadores.size() - 1; i >= 0; i--) {
				sb.append(revisadores.get(i).getPersona().getNombreApellido() + STRING + BLANK);
			}

			return sb.toString();
		} catch (Exception e) {
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RevisarDocumentos> obtenerComentariosBorrador() {
		
		List<RevisarDocumentos> comentarios = new ArrayList<RevisarDocumentos>();
		
		if (this.resolucion != null && this.resolucion.getRevisar()!= null)
			comentarios.addAll(this.resolucion.getRevisar());
		
		List<RevisarDocumentosHistorial> historial = new ArrayList<RevisarDocumentosHistorial>();
		try {
			Query query = em.createNamedQuery("RevisarDocumentosHistorial.findByDocumento");
			query.setParameter("idDoc", resolucion.getId());
			historial = query.getResultList();
		} catch (Exception e) {
			log.error("No se encontro historial de comentarios.");
		}
		
		if (historial != null) {
			RevisarDocumentos rdTemp = null;
			for(RevisarDocumentosHistorial his:historial) {
				rdTemp = new RevisarDocumentos();
				rdTemp.setAprobado(his.getAprobado());
				rdTemp.setComentario(his.getComentario());
				rdTemp.setDocumento(his.getDocumento());
				rdTemp.setFechaFirma(his.getFechaFirma());
				rdTemp.setPersona(his.getPersona());
				
				comentarios.add(rdTemp);
			}
		}
		
		Collections.sort(comentarios, ComparatorUtils.revisarDocumentosComparatorByDate);
		
		return comentarios;
	}

	public List<VisacionDocumento> getVisacionesV() {
		List<VisacionDocumento> visaciones = null;
		if (this.resolucion != null && this.resolucion.getVisaciones() != null) {
			visaciones = resolucion.getVisaciones();
			Collections.sort(visaciones, ComparatorUtils.visasDocumentosComparatorByDateInvert);
		}
		return visaciones;
	}

	public Integer getTipoResolucion() {
		if (resolucion != null && resolucion.getTipo() != null) {
			tipoResolucion = resolucion.getTipo().getId();
		}
		return tipoResolucion;
	}

	public void setTipoResolucion(final Integer tipoResolucion) {
		this.tipoResolucion = tipoResolucion;
	}

	public List<FirmaEstructuradaDocumento> getFirmas() {
		if (resolucion.getFirmasEstructuradas() == null) {
			resolucion.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
		Collections.sort(firmas, ComparatorUtils.firmasComparator);
		return firmas;
	}

	@Override
	public boolean isRenderedFirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (resolucion != null && resolucion.getId() == null) { return false; }
		if (resolucion != null && resolucion.getFirmasEstructuradas() != null
				&& resolucion.getFirmasEstructuradas().size() != 0) {
			final List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
			Collections.sort(firmas);
			if (resolucion.getVisacionesEstructuradas() != null && resolucion.getVisacionesEstructuradas().size() == 0) {
				if (firmas.get(0).getPersona().equals(usuario)) { return true; }
			}
		}
		return false;
	}

	public boolean isRenderedFirmarExento() {
		return isRenderedFirmar() && this.tipoResolucion.equals(TipoRazon.EXENTO);
	}

	public boolean isRenderedFirmarContraloria() {
		return isRenderedFirmar() && !this.tipoResolucion.equals(TipoRazon.EXENTO);
	}

	public boolean isRenderedVisar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.resolucion != null && this.resolucion.getId() != null) {
			if (resolucion != null && resolucion.getVisacionesEstructuradas() != null
					&& resolucion.getVisacionesEstructuradas().size() != 0) {
				final List<VisacionEstructuradaDocumento> visas = resolucion.getVisacionesEstructuradas();
				for (VisacionEstructuradaDocumento visacionEstructuradaDocumento : visas) {
					if(visacionEstructuradaDocumento.getPersona().equals(usuario)) return Boolean.TRUE;
				}
				Collections.sort(visas);
			}
		}
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) { return true; }
		return false;
	}

	public boolean isRenderedPlantillaList() {
		if (this.resolucion != null) {
			if (this.resolucion.getId() != null) {
				final Long idSolDiaAdm = existeSolDiaAdm();
				final Long idSolVaca = existeSolVacaciones();
				if (idSolDiaAdm != null || idSolVaca != null) { return true; }
			}
		}
		return false;
	}

	public boolean isDisabledGuardar() {
		return guardado;
	}

//	public String getUrlFirma() {
//		  Properties defaultProps = new Properties();
//	        try {
//	            defaultProps.load(getArchivoStream("customizacion.properties"));
//	        }
//	        catch (Exception e) {
//	            log.error("No se pudo cargar archivo de configuracion customizacion.properties");
//	        }
//
//			final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
//			return urlDocumento;
//	}
	
	@Override
	public String getUrlFirma() {
	  Properties defaultProps = new Properties();
      try {
         // defaultProps.load(getArchivoStream("customizacion.properties"));
          final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("exedoc/customizacion.properties");
          defaultProps.load(resourceAsStream);

      }
      catch (Exception e) {
          log.error("No se pudo cargar archivo de configuracion customizacion.properties");
      }
      String default_port = "8080";
      String port = defaultProps.getProperty("default_port", default_port.toString());
      String IP = defaultProps.getProperty("IP", System.getProperty("jboss.bind.address"));
      
		//final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
      final String urlDocumento = "http://" + IP  + ":" + port + STRING;
		return urlDocumento;
	}
	
	private InputStream getArchivoStream(String nombre) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResourceAsStream(nombre);
	}

	@Override
	public String despacharExpediente() {
		this.desdeDespacho = true;
		if (!guardado) {
			this.armaDocumento(false, false, false, false);
		}
		if (expediente == null || expediente.getId() == null) {
			this.distribuirDocumentos();
		}
		if (this.guardarExpediente(false, true)) {
			idPlantillaSelec = null;
			this.end();
			Conversation.instance().end();
			return "bandejaSalida";
		} else {
			if (expediente == null || expediente.getId() == null) {
				this.noDistribuirDocumentos();
			}
			return "";
		}

	}
	@Override
	public boolean soyRevisor(){
		if (resolucion.getRevisarEstructuradas() != null && !resolucion.getRevisarEstructuradas().isEmpty())
			for (RevisarEstructuradaDocumento re : resolucion.getRevisarEstructuradas()) 
				if (re.getPersona().equals(usuario)) { return true; }
		return false;
	}
	
	@Override
	public boolean soyVisador(){
		if (resolucion.getVisacionesEstructuradas() != null && !resolucion.getVisacionesEstructuradas().isEmpty())
			for (VisacionEstructuradaDocumento re : resolucion.getVisacionesEstructuradas()) 
				if (re.getPersona().equals(usuario)) { return true; }//los visadores pueden rechazar
		return false;
	}
	
	@Override
   public boolean soyFirmante(){
		if (resolucion.getFirmasEstructuradas() != null && !resolucion.getFirmasEstructuradas().isEmpty())
			for (FirmaEstructuradaDocumento re : resolucion.getFirmasEstructuradas()) 
				if (re.getPersona().equals(usuario)) { return true; }//los visadores pueden rechazar
		return false;
	}
	@Override
	public String despacharYRechazar() {
		if(observacionRechazo== null){
			observacionRechazo ="";
		}
		this.desdeDespacho = true;	
	
		if(observacionRechazo.length() < 999){
			if (!guardado) {
				this.aprobado = false;
				if(soyRevisor()){
					despacharYRechazarRevisor();
					//resolucion.getRevisarEstructuradas().clear();
				}else if(soyVisador()){
					despacharYRechazarVisador();
				}else if(soyFirmante()){
					despacharYRechazarFirmante();
				}
			}
			if (this.guardarExpediente(true, false)) {
				idPlantillaSelec = null;
				this.end();
				Conversation.instance().end();
				observacionRechazo="";
				return "bandejaSalida";
			} else {
				if (expediente == null || expediente.getId() == null) {
					this.noDistribuirDocumentos();
				}
				observacionRechazo="";
				return "";
			}
		}
		else{
			FacesMessages.instance().add("La Observación debe tener menos de 1000 Carácteres.");
			observacionRechazo="";
			return"";
			
		}
		
	}
	private void despacharYRechazarRevisor(){
		this.armaDocumento(false, false, true, false);

//		for (RevisarEstructuradaDocumento ve : resolucion.getRevisarEstructuradas()) {
//			if (ve != null && ve.getId() != null) {
//				try {
//					final Query query = em.createQuery(DELETE_FROM_REVISAR_ESTRUCTURADA_WHERE_ID);
//					query.setParameter(1, ve.getId());
//					query.executeUpdate();
//				} catch (PersistenceException e) {
//					log.error("PersistenceException: ", e);
//				}
//			}
//		}
	}
	private void despacharYRechazarFirmante(){ //firmante o visador

		this.armaDocumentoRechazo();

	}
	private void despacharYRechazarVisador(){ //firmante o visador
		this.armaDocumentoRechazo();
	}

	@Override
	public String despacharYAprobar() {

		if (!guardado) {
			this.aprobado = true;
			this.armaDocumento(false, false, true, false);

			RevisarEstructuradaDocumento rev = null;
			for (RevisarEstructuradaDocumento ve : resolucion.getRevisarEstructuradas()) {
				if (ve.getPersona().equals(usuario)) {
					rev = ve;
					break;
				}
			}

			if (rev != null && rev.getId() != null) {
				final Query query = em.createQuery(DELETE_FROM_REVISAR_ESTRUCTURADA_WHERE_ID);
				query.setParameter(1, rev.getId());
				query.executeUpdate();
				resolucion.getRevisarEstructuradas().remove(rev);
			}

			salida: for (DocumentoExpediente de : expediente.getDocumentos()) {

				try {
					if (de.getDocumento().getId().equals(resolucion.getId())) {
						for (RevisarEstructuradaDocumento ve : de.getDocumento().getRevisarEstructuradas()) {
							if (ve.getPersona().equals(usuario)) {
								de.getDocumento().getRevisarEstructuradas().remove(ve);
								break salida;
							}
						}
					}
				} catch (LazyInitializationException e) {
					log.error("LazyInitializationException: ", e);
				}
			}
		}
		if (expediente == null || expediente.getId() == null) {
			this.distribuirDocumentos();
		}
//		if (resolucion.getRevisarEstructuradas() == null || resolucion.getRevisarEstructuradas().size() == 0) {
//			this.resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR_APROBADO));
//			try {
//				md.actualizarDocumento(this.resolucion);
//			} catch (DocumentNotUploadedException e) {
//				e.printStackTrace();
//			}
//		}
		if (this.guardarExpediente(false, false)) {
			idPlantillaSelec = null;
			this.end();
			Conversation.instance().end();
			
			this.desdeDespacho = true;
			
			return "bandejaSalida";
		} else {
			if (expediente == null || expediente.getId() == null) {
				this.noDistribuirDocumentos();
			}
			return "";
		}
	}

	@Override
	public boolean isRenderedBotonDespachar() {
//		if (vistoDesdeReporte != null) {
//			if (vistoDesdeReporte) { return false; }
//		}
//		if (resolucion != null && resolucion.getId() != null) {
//			if (!resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR) && !resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_RECHAZADO) && !resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)
//				&& !resolucion.getEstado().getId().equals(EstadoDocumento.VISADO) && !resolucion.getEstado().getId().equals(EstadoDocumento.FIRMADO)) {
//				if ((resolucion.getFirmasEstructuradas() != null && resolucion.getFirmasEstructuradas().size() > 0) || (resolucion.getVisacionesEstructuradas() != null && resolucion.getVisacionesEstructuradas().size() > 0)) {
//					for (VisacionEstructuradaDocumento ve : resolucion.getVisacionesEstructuradas()) {
//						if (ve.getPersona().equals(usuario)) { return false; }
//					}
//					for (FirmaEstructuradaDocumento fe : resolucion.getFirmasEstructuradas()) {
//						if (fe.getPersona().equals(usuario)) { return false; }
//					}
//					return true;
//				}
//			}
//		}
		return false;
	}

	@Override
	public boolean isRenderedBotonAprobarDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (expediente != null && expediente.getFechaDespacho() != null)
			return false;
		try {
			if (resolucion != null && resolucion.getId() != null) {
				if (resolucion.getRevisarEstructuradas() != null && !resolucion.getRevisarEstructuradas().isEmpty()) {
					
					for (RevisarEstructuradaDocumento re : resolucion.getRevisarEstructuradas()) {
						if (re.getPersona().equals(usuario)) { return true; }
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	@Override
	public boolean isRenderedBotonRechazarDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (expediente != null && expediente.getFechaDespacho() != null)
			return false;
		try {
			if (resolucion != null && resolucion.getId() != null) {
				
				if (resolucion.getRevisarEstructuradas() != null && !resolucion.getRevisarEstructuradas().isEmpty())
					for (RevisarEstructuradaDocumento re : resolucion.getRevisarEstructuradas()) 
						if (re.getPersona().equals(usuario)) { return true; }
				if (resolucion.getVisacionesEstructuradas() != null && !resolucion.getVisacionesEstructuradas().isEmpty())
					for (VisacionEstructuradaDocumento re : resolucion.getVisacionesEstructuradas()) 
						if (re.getPersona().equals(usuario) && isRenderedVisar()) { return true; }//los visadores pueden rechazar

				if (resolucion.getFirmasEstructuradas() != null && !resolucion.getFirmasEstructuradas().isEmpty())
					for (FirmaEstructuradaDocumento re : resolucion.getFirmasEstructuradas()) 
						if (re.getPersona().equals(usuario) && isRenderedFirmar()) { return true; }//los firmadores pueden rechazar
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean isDespachable() {
		if (this.modificarVisaFirmas() || this.usuarioTieneVisas() || this.usuarioTieneFirmas()) { return true; }
		return false;
	}

	/**
	 * @return {@link boolean}
	 */
	private boolean usuarioTieneVisas() {
		if (resolucion.getVisaciones() != null && resolucion.getVisaciones().size() > 0) {
			final List<VisacionDocumento> listVisaciones = resolucion.getVisaciones();
			for (VisacionDocumento v : listVisaciones) {
				if (v.getPersona().equals(usuario)) { return true; }
			}
		}
		return false;
	}

	/**
	 * @return {@link boolean}
	 */
	private boolean usuarioTieneFirmas() {
		if (resolucion.getFirmas() != null && resolucion.getFirmas().size() > 0) {
			final List<FirmaDocumento> listFirmas = resolucion.getFirmas();
			for (FirmaDocumento f : listFirmas) {
				if (f.getPersona().equals(usuario)) { return true; }
			}
		}
		return false;
	}

	private boolean guardarExpediente(boolean rechanzado, boolean fromDespachar) {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
				me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
			} catch (DocumentNotUploadedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			if(expediente.getFechaDespachoHistorico() == null)me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo al documento anterior
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado()
					&& !this.documentoOriginal.getId().equals(this.resolucion.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(
						new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
					md.actualizarDocumento(this.documentoOriginal);
				} catch (DocumentNotUploadedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
			try {
				if (doc.getId() == null) {
					me.agregarRespuestaAExpediente(expediente, doc, true);
				} else {
					// Cerrar plazo al documento anterior
					if (doc.getCompletado() != null && !doc.getCompletado()
							&& !doc.getId().equals(this.resolucion.getId())) {
						doc.setCompletado(true);
						doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
						md.actualizarDocumento(doc);
					}
				}
			} catch (DocumentNotUploadedException e) {
				e.printStackTrace();
			}
		}

		this.guardarObservaciones();
		idExpedientes = new ArrayList<Long>();
		final Date fechaIngreso = new Date();

		Persona de = null;
		if(!rechanzado){//CAMINO NORMAL
			if (this.resolucion.getRevisarEstructuradas() != null && this.resolucion.getRevisarEstructuradas().size() != 0
					&& !this.resolucion.getEstado().getId().equals(EstadoDocumento.RECHAZADO)) {
				Collections.sort(this.resolucion.getRevisarEstructuradas(), new Comparator<RevisarEstructuradaDocumento>() {
					public int compare(final RevisarEstructuradaDocumento o1, final RevisarEstructuradaDocumento o2) {
						return o1.getOrden().compareTo(o2.getOrden());
					}
				});
				de = this.resolucion.getRevisarEstructuradas().get(0).getPersona();
			} else if (this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR)
					|| this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_RECHAZADO)
					|| (this.resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO) 
							&& !fromDespachar)) {
				de = this.resolucion.getAutor();
			} else if (this.resolucion.getVisacionesEstructuradas() != null
					&& this.resolucion.getVisacionesEstructuradas().size() != 0) {
				Collections.sort(this.resolucion.getVisacionesEstructuradas(),
						new Comparator<VisacionEstructuradaDocumento>() {
							public int compare(final VisacionEstructuradaDocumento o1,
									final VisacionEstructuradaDocumento o2) {
								return o1.getOrden().compareTo(o2.getOrden());
							}
						});
				de = this.resolucion.getVisacionesEstructuradas().get(0).getPersona();
				if (de.equals(usuario)) {
					FacesMessages.instance().add("Debe visar el documento antes de Despacharlo");
					return false;
				}
			} else if (this.resolucion.getFirmasEstructuradas() != null
					&& this.resolucion.getFirmasEstructuradas().size() != 0) {
				Collections.sort(this.resolucion.getFirmasEstructuradas(), new Comparator<FirmaEstructuradaDocumento>() {
					public int compare(final FirmaEstructuradaDocumento o1, final FirmaEstructuradaDocumento o2) {
						return o1.getOrden().compareTo(o2.getOrden());
					}
				});
				de = this.resolucion.getFirmasEstructuradas().get(0).getPersona();
				if (de.equals(usuario)) {
					FacesMessages.instance().add("Debe firmar el documento antes de Despacharlo");
					return false;
				}
			}
		}else{
			de = this.resolucion.getAutor();//Siempre que es un rechazo, vuelve al autor
		}
		if (de != null) {
			log.info("Despachando a : #0", de.getUsuario());
			try {
				idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
			} catch (DocumentNotUploadedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			if (this.resolucion.getFirmas() != null && this.resolucion.getFirmas().size() != 0) {
				agregarListaDestinatariosExpediente(fechaIngreso);
			}
		}

		final List<Expediente> expedientes = new ArrayList<Expediente>();
		if (!idExpedientes.isEmpty()) {
			me.despacharExpediente(expediente);
			for (Long id : idExpedientes) {
				final Expediente e = me.buscarExpediente(id);
				me.despacharExpediente(e);
				expedientes.add(e);
			}
			//me.despacharNotificacionPorEmail(expedientes);

			FacesMessages.instance().add("Expediente Despachado");
			return true;
		} else {
			FacesMessages.instance().add("No hay destinatarios a quien despachar");
			return false;
		}
	}

	private void agregarListaDestinatariosExpediente(final Date fechaIngreso) {
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		// TODO MODELO ROLES UCHILE

		for (SelectPersonas sp : distribucionDocumento) {
			final Persona p = em.find(Persona.class, sp.getPersona().getId());
			/*
			 * if (p.getRoles().contains(new Rol(Rol.JEFE)) && p.getRoles().contains(new Rol(Rol.ABASTECIMIENTO))) {
			 * p.setDestinatarioConCopia(false); } else {
			 */
			p.setDestinatarioConCopia(true);
			// }
			listDestinatarios.add(p);
			try {
				idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, p, fechaIngreso));
			} catch (DocumentNotUploadedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean documentoValido;
	
	@Override
	public boolean isDocumentoValido() {
		return documentoValido;
	}

	@Override
	public void guardarYCrearExpediente() {
		// TODO faltan condiciones
		documentoValido = true;
		if (resolucion.getMateria() == null || (resolucion.getMateria() != null && resolucion.getMateria().trim().isEmpty())) {
			FacesMessages.instance().add("La Materia es obligatoria");
			documentoValido = false;
			return;
		}
		this.armaDocumento(false, false, false, false);
		documentoOriginal = resAuxiliar;
		//documentoOriginal = listDocumentosRespuesta.get(0);
		//listDocumentosRespuesta.remove(0);

		final List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		final List<FirmaEstructuradaDocumento> firmas = resolucion.getFirmasEstructuradas();
		final List<RevisarEstructuradaDocumento> revisar = resolucion.getRevisarEstructuradas();
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente", "inicio", new Date());

		if (revisar != null && revisar.size() > 0) {
			if (!revisar.get(0).getPersona().getId().equals(usuario.getId())) {
				listDestinatarios.add(revisar.get(0).getPersona());
			}
			documentoOriginal.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));
		} else if (visaciones != null && visaciones.size() > 0) {
			if (!visaciones.get(0).getPersona().getId().equals(usuario.getId())) {
				listDestinatarios.add(visaciones.get(0).getPersona());
			}
		} else if (firmas != null && firmas.size() > 0) {
			if (!firmas.get(0).getPersona().getId().equals(usuario.getId())) {
				listDestinatarios.add(firmas.get(0).getPersona());
			}
		}

	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente == null || this.expediente.getId() == null) { return true; }
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente != null && this.expediente.getId() != null) { return true; }
		return false;
	}

	public boolean isEditableSinVisa() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return false; }
		if (getFirmado()) { return false; }
		if (this.getAnulado()) { return false; }
		return true;
	}

	public boolean isRenderedBotonVisar() {
		if (this.resolucion.getId() == null) { return false; }
		return !isRenderedBotonFirmar();
	}

	public boolean isRenderedBotonFirmar() {
		if (!isEditableSinVisa()) { return false; }
		if (usuario.getId().equals(resolucion.getIdEmisor())) { return true; }
		return false;
	}

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	private void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

	private void noDistribuirDocumentos() {
		final List<Documento> list = new ArrayList<Documento>();
		list.add(documentoOriginal);
		list.addAll(listDocumentosRespuesta);
		listDocumentosRespuesta.clear();
		listDocumentosRespuesta.addAll(list);
	}

	/***************************************************************************
	 * PLANTILLAS
	 **************************************************************************/
	private Integer idPlantilla;

	@SuppressWarnings("unchecked")
	public List<SelectItem> obtenerPlantillas() {
		// if (!flujoEstructurado) {
		// if (documentoAsociadoEsDiaAdminCreando() != null ||
		// documentoAsociadoEsVacacionCreando() != null) {
		// if (resolucion.getFirmasEstructuradas() == null ||
		// resolucion.getFirmasEstructuradas().isEmpty()) {
		// obtenerVisasEstructSolicitud();
		// setearDistribucionDefecto();
		// }
		// }
		// }
		List<Plantilla> plantillas;
		final List<SelectItem> plantillasItem = new ArrayList<SelectItem>();
		if (idPlantillaSelec == null || idPlantillaSelec == 0) {

			plantillas = em.createNamedQuery("Plantilla.findByTipoPlantilla")
					.setParameter("tp", TipoPlantilla.RESOLUCION_DECRETO).getResultList();
			plantillasItem.add(new SelectItem(-1, "<<Seleccionar Plantilla>>"));
			for (Plantilla pr : plantillas) {
				plantillasItem.add(new SelectItem(pr.getId(), pr.getNombre()));
			}
		} else {
			final Query query = em.createNamedQuery("Plantilla.findById");
			query.setParameter("id", idPlantillaSelec);
			plantillas = query.getResultList();
			if (plantillas != null && plantillas.size() > 0) {
				plantillasItem.add(new SelectItem(plantillas.get(0).getId(), plantillas.get(0).getNombre()));
			}
			setearPlantilla(idPlantillaSelec);
		}
		if (this.resolucion.getId() == null && idPlantillaSelec != null) {
			agregarDocumento();
		}
		return plantillasItem;
	}

	public Integer getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla(final Integer idPlantilla) {
		this.idPlantilla = idPlantilla;
	}

	public void plantillaListener(final ValueChangeEvent event) {
		final Integer id = (Integer) event.getNewValue();
		if (!id.equals(-1)) {
			aplicaPlantilla(id);
		}
	}

	private void setearDistribucionDefecto() {
		if (this.distribucionDocumento == null) {
			this.distribucionDocumento = new ArrayList<SelectPersonas>();
		}
		for (SelectItem si : buscarPersonasItem()) {
			agregarDistribucion(si);
		}
	}

	public List<SelectItem> buscarPersonasItem() {
		final List<SelectItem> listPersonas = new ArrayList<SelectItem>();
		listPersonas.addAll(jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, ID_CARGO_JEFATURA_RR_HH));
		listPersonas.addAll(jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, ID_CARGO_JEFE_UNIDAD_OF_PARTES));
		return listPersonas;
	}

	public void agregarDistribucion(final SelectItem personaItem) {
		try {
			final Long personaItemL = (Long) personaItem.getValue();
			if (!personaItemL.equals(JerarquiasLocal.INICIO)) {
				final Persona persona = em.find(Persona.class, personaItemL);

				String value = persona.getNombres() + BLANK + persona.getApellidoPaterno();

				final Cargo cargo = persona.getCargo();
				if (cargo.getDescripcion().equals("Jefe Division")
						|| cargo.getDescripcion().equals("Jefe Departamento")
						|| cargo.getDescripcion().equals("Jefe Unidad")) {
					value = cargo.getDescripcion();
				} else {
					value += " - " + cargo.getDescripcion();
				}

				if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					final UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
					value += " - " + uo.getDescripcion();
				} else if (!persona.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
					final Departamento depto = persona.getCargo().getUnidadOrganizacional().getDepartamento();
					value += " - " + depto.getDescripcion();
				} else {
					final Division d = persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
					value += " - " + d.getDescripcion();
				}
				if (!estaDestinatario(value)) {
					persona.setDestinatarioConCopia(true);
					this.distribucionDocumento.add(new SelectPersonas(value, persona));
				}
			}
		} catch (Exception e) {
			log.info("Cargando Campos");
		}
	}

	@Override
	public boolean mostrarBorrador() {

		boolean status = true;

		status = this.modificarVisaFirmas();

		try {
			status = status
					&& (!this.resolucion.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA) || this.expediente == null);
		} catch (NullPointerException e) {
			status = status && true;
		}

		return status;
	}

	private boolean estaDestinatario(final String nombre) {
		for (SelectPersonas si : this.distribucionDocumento) {
			if (si.getDescripcion().equals(nombre)) { return true; }
		}
		return false;
	}

	public void setearPlantilla(final Integer idPlantilla) {
		if (!idPlantilla.equals(-1)) {
			aplicaPlantilla(idPlantilla);
		}
	}

	public String cambiar() {
		return CREAR_RESOLUCION;
	}

	private void aplicaPlantilla(final Integer idPlantilla) {
		Plantilla plantilla;
		try {
			plantilla = (Plantilla) em.createNamedQuery("Plantilla.findById").setParameter("id", idPlantilla)
					.getSingleResult();
		} catch (NoResultException nre) {
			plantilla = null;
		}
		if (plantilla != null) {
			for (CamposPlantilla cpr : plantilla.getCamposPlantilla()) {
				if (cpr.getTipoContenido().getId().equals(TipoContenido.TIPO)) {
					if (cpr.getContenido().equals("EXENTO")) {
						tipoResolucion = 1;
					} else if (cpr.getContenido().equals("TOMA DE RAZON")) {
						tipoResolucion = 2;
					} else {
						tipoResolucion = 3;
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					final String materia = cpr.getContenido();
					// if (documentoAsociadoEsDiaAdminCreando() != null) {
					// materia = obtenerMateriaDiaAdm(cpr.getContenido());
					// } else if (documentoAsociadoEsVacacionCreando() != null)
					// {
					// materia = obtenerMateriaVacaciones(cpr.getContenido());
					// }
					this.resolucion.setMateria(materia);
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.VISTOS)) {
					if (this.resolucion.getVistos() != null && !this.resolucion.getVistos().isEmpty()) {
						this.resolucion.getVistos().get(0).setCuerpo(cpr.getContenido());
					} else {
						final Vistos parrafo = new Vistos();
						parrafo.setCuerpo(cpr.getContenido());
						parrafo.setDocumento(this.resolucion);
						parrafo.setNumero(1L);
						final List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.resolucion.setVistos(parrafos);
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					if (this.resolucion.getConsiderandos() != null && !this.resolucion.getConsiderandos().isEmpty()) {
						this.resolucion.getConsiderandos().get(0).setCuerpo(cpr.getContenido());
					} else {
						final Considerandos parrafo = new Considerandos();
						parrafo.setCuerpo(cpr.getContenido());
						parrafo.setDocumento(this.resolucion);
						parrafo.setNumero(1L);
						final List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.resolucion.setConsiderandos(parrafos);
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.RESUELVO)) {
					String resuelvo = "";
					// if (documentoAsociadoEsDiaAdminCreando() != null) {
					// resuelvo = obtenerResuelvoDiaAdm(cpr.getContenido());
					// } else if (documentoAsociadoEsVacacionCreando() != null)
					// {
					// resuelvo = obtenerResuelvoVacaciones(cpr.getContenido());
					// } else {
					resuelvo = cpr.getContenido();
					// }
					if (this.resolucion.getResuelvo() != null && !this.resolucion.getResuelvo().isEmpty()) {
						this.resolucion.getResuelvo().get(0).setCuerpo(resuelvo);
					} else {
						final Resuelvo parrafo = new Resuelvo();
						parrafo.setCuerpo(resuelvo);
						parrafo.setDocumento(this.resolucion);
						parrafo.setNumero(1L);
						final List<Parrafo> parrafos = new ArrayList<Parrafo>();
						parrafos.add(parrafo);
						this.resolucion.setResuelvo(parrafos);
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.INDICACION)) {
					this.resolucion.setIndicaciones(cpr.getContenido());
				}
			}
		}
	}

	private void obtenerVisasEstructSolicitud() {
		final List<VisacionEstructuradaDocumento> visaciones = new ArrayList<VisacionEstructuradaDocumento>();
		final List<FirmaEstructuradaDocumento> firmas = new ArrayList<FirmaEstructuradaDocumento>();

		final VisacionEstructuradaDocumento ved2 = new VisacionEstructuradaDocumento();
		ved2.setOrden(visaciones.size() + 1);
		Persona jefeRRHH = null;
		for (Persona p : listDestinatarios) {
			jefeRRHH = p;
			break;
		}
		ved2.setPersona(jefeRRHH);
		ved2.setDocumento(this.resolucion);
		visaciones.add(ved2);

		final List<Persona> jefesDivisionAdmFinanzas = obtenerJefeDivisionAdmFinanzas();
		for (Persona p : jefesDivisionAdmFinanzas) {
			final FirmaEstructuradaDocumento fed = new FirmaEstructuradaDocumento();
			fed.setOrden(firmas.size() + 1);
			fed.setPersona(p);
			fed.setDocumento(this.resolucion);
			firmas.add(fed);
		}

		this.getResolucion().setVisacionesEstructuradas(visaciones);
		this.getResolucion().setFirmasEstructuradas(firmas);

	}

	@SuppressWarnings("unchecked")
	private List<Persona> obtenerJefeUnidadAbastecimiento() {
		final String hql = "select p from Persona p where p.cargo.id= ?";
		final Query query = em.createQuery(hql);
		query.setParameter(1, ID_CARGO_JEFE_UNIDAD_ABASTECIMIENTO);
		final List<Persona> jefeUnidadAbastecimiento = query.getResultList();
		return jefeUnidadAbastecimiento;
	}

	@SuppressWarnings("unchecked")
	private List<Persona> obtenerJefeDivisionAdmFinanzas() {
		final String hql = "select p from Persona p where p.cargo.id= ?";
		final Query query = em.createQuery(hql);
		query.setParameter(1, ID_CARGO_JEFATURA_DIVISION_ADM_FINANZAS);
		final List<Persona> jefesDivisionAdmFinanzas = query.getResultList();
		return jefesDivisionAdmFinanzas;
	}

	public String desvisarYDesfirmar(final Resolucion res) {
		if (res.getFirmas() == null || res.getFirmas().isEmpty()) {
			if (res.getVisaciones() != null && !res.getVisaciones().isEmpty()) {
				for (VisacionDocumento visa : res.getVisaciones()) {
					final VisacionDocumento visaElim = em.find(VisacionDocumento.class, visa.getId());
					em.remove(visaElim);
				}
				res.getVisaciones().clear();
				res.setVisaciones(null);
			}

			if (res.getVisacionesEstructuradas() != null && !res.getVisacionesEstructuradas().isEmpty()) {
				for (VisacionEstructuradaDocumento visaEstructurada : res.getVisacionesEstructuradas()) {
					final VisacionEstructuradaDocumento visaEstructElim = em.find(VisacionEstructuradaDocumento.class,
							visaEstructurada.getId());
					em.remove(visaEstructElim);
				}
				res.getVisacionesEstructuradas().clear();
				res.setVisacionesEstructuradas(null);
			}

			if (res.getFirmasEstructuradas() != null && !res.getFirmasEstructuradas().isEmpty()) {
				for (FirmaEstructuradaDocumento fed : res.getFirmasEstructuradas()) {
					final FirmaEstructuradaDocumento fedElim = em.find(FirmaEstructuradaDocumento.class, fed.getId());
					em.remove(fedElim);
				}
				res.getFirmasEstructuradas().clear();
				res.setFirmasEstructuradas(null);
			}

			res.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
			res.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, res, usuario));
			res.setCmsId(null);
			expediente.setFechaDespacho(null);
			persistirDocumento();

			FacesMessages.instance().add("Resolucion desvisada y desfirmada");
		} else {
			FacesMessages.instance().add("La resolucion no se puede desvisar, pues ya esta firmada");
		}
		return CREAR_RESOLUCION;
	}

	public boolean renderedBotonDesvisarDesfirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		return usuario.getId().equals(ID_CARGO_FISCAL)
				&& (resolucion.getFirmas() == null || resolucion.getFirmas().isEmpty())
				&& (resolucion.getVisaciones() != null && !resolucion.getVisaciones().isEmpty());
	}

	public void tipoRazonListener(final ValueChangeEvent event) {
		final PhaseId phaseId = event.getPhaseId();
		tipoResolucion = (Integer) event.getNewValue();
		if (phaseId.equals(PhaseId.ANY_PHASE)) {
			event.setPhaseId(PhaseId.UPDATE_MODEL_VALUES);
			event.queue();
		} else if (phaseId.equals(PhaseId.UPDATE_MODEL_VALUES)) {
			if (tipoResolucion != null) {
				if (tipoResolucion.equals(TipoRazon.EXENTO)) {
					tipoArchivo = "*";
				} else {
					final List<ArchivoAdjuntoDocumentoElectronico> adjuntos = this.resolucion.getArchivosAdjuntos();
					if (adjuntos != null) {
						for (ArchivoAdjuntoDocumentoElectronico ar : adjuntos) {
							if (ar != null) {
								if (!ar.getContentType().equals("application/pdf")) {
									FacesMessages
											.instance()
											.add("No se puede cambiar el tipo de resolución, pues ya existe un archivo que no es PDF");
									this.setTipoResolucion(TipoRazon.EXENTO);
									return;
								}
							}
						}
					}
					tipoArchivo = "pdf";
				}
				if (resolucion != null) {
					if (resolucion.getId() != null) {
						resolucion.setTipo(new TipoRazon(tipoResolucion));
						em.merge(resolucion);
					}
				}
			}
		}
	}

	@Override
	public void aceptarAnulacion() {
		documentoOriginal.setNumeroDocumento("s/n");
		em.merge(documentoOriginal);
	}

	@Override
	public String imagenEstadoResolucion() {
		String imagen = "";
		if (EstadoDocumento.ANULADO.equals(resolucion.getEstado())) {
			imagen = "/imagenes/anulado.png";
		} else if (EstadoDocumento.FIRMADO.equals(resolucion.getEstado())) {
			imagen = "/imagenes/firmado.gif";
		}
		return imagen;
	}

	public Integer getIdPlantillaSelec() {
		return idPlantillaSelec;
	}

	public void setIdPlantillaSelec(final Integer idPlantillaSelec) {
		this.idPlantillaSelec = idPlantillaSelec;
	}

	/*
	 * public List<SelectItem> getTipos1() { if (tipos1 == null) { tipos1 = new ArrayList<SelectItem>(); } if
	 * (tipos1.isEmpty()) { tipos1.addAll(obtenerClasificacionTipo()); } return tipos1; } public void
	 * setTipos1(List<SelectItem> tipos1) { this.tipos1 = tipos1; } public List<SelectItem> getTipos2() { if (tipos2 ==
	 * null) { tipos2 = new ArrayList<SelectItem>(); } if (tipos2.isEmpty()) {
	 * tipos2.addAll(obtenerClasificacionSubTipo()); } return tipos2; }
	 */

	/*
	 * public void setTipos2(List<SelectItem> tipos2) { this.tipos2 = tipos2; }
	 */

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(final String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	@Override
	public final String getSessionId() {
		final FacesContext fCtx = FacesContext.getCurrentInstance();
		final HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		final String sessionId = session.getId();
		return sessionId;
	}

	@Override
	public String getComentarios() {
		return comentarios;
	}

	@Override
	public void setComentarios(final String comentarios) {
		this.comentarios = comentarios;
	}

	@Override
	public Long getIdDepartamento() {
		this.idDepartamento = usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId();
		return idDepartamento;
	}

	@Override
	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	@Override
	public String cacheRandom(){
		
		String idCache = "0";
		try{
			idCache = String.valueOf(Math.floor((Math.random()*1000))); 
		  }catch(Exception ex){
		    ex.printStackTrace();
		  }
		return(idCache.replace(".", ""));
	}
	
	public Long getRand() {
		try{
			rand = Long.valueOf(cacheRandom()); 
		  }catch(Exception ex){
		    ex.printStackTrace();
		  }
		
		return rand;
	}

	public void setRand(Long rand) {
		this.rand = rand;
	}

	@Override
	public final Long getIdAlerta() {
		return idAlerta;
	}

	@Override
	public final void setIdAlerta(final Long idAlerta) {
		this.idAlerta = idAlerta;
	}
	private void cargarNivelesUrgencia() {

		this.setIdAlerta(new Long(0));

		final List<Alerta> alertas = administradorAlertas.buscarAlerta();
		this.setNivelesUrgencia(new ArrayList<SelectItem>());

		this.getNivelesUrgencia().add(
				new SelectItem(0, SELECCIONAR, SELECCIONAR, false, true));

		if (alertas != null) {
			for (Alerta a : alertas) {
				this.getNivelesUrgencia().add(
						new SelectItem(a.getId(), a.getNombre()));
			}
		}
	}
	@Override
	public final List<SelectItem> getNivelesUrgencia() {
		if(nivelesUrgencia == null)
			cargarNivelesUrgencia();
		return nivelesUrgencia;
	}

	@Override
	public final void setNivelesUrgencia(final List<SelectItem> nivelesUrgencia) {
		this.nivelesUrgencia = nivelesUrgencia;
	}

	@Override
	public String getObservacionRechazo() {
		return observacionRechazo;
	}

	@Override
	public void setObservacionRechazo(String observacionRechazo) {
		this.observacionRechazo = observacionRechazo;
	}
	
	@Override
	public void limpiaObservaciones() {
		this.observacionRechazo = "";
	}

	@Override
	public Boolean getDesdeDespacho() {
		return desdeDespacho;
	}

	@Override
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}
	
	@Override
	public boolean isEditable2() {
		Boolean estado = true;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return Boolean.FALSE;
			}
		}
		if (resolucion != null && resolucion.getId() == null) {
			return Boolean.TRUE;
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			estado = estado && Boolean.FALSE;
		}
		
		if (resolucion != null && resolucion.getAutor().equals(usuario)) {
			//estado = estado && Boolean.TRUE;
			return Boolean.TRUE;
		}
		
		List<RevisarEstructuradaDocumento> revisores = resolucion.getRevisarEstructuradas();
		if(revisores != null){
			for (RevisarEstructuradaDocumento revisarEstructuradaDocumento : revisores) {
				if(revisarEstructuradaDocumento.getPersona().equals(usuario)){
					estado = estado && Boolean.TRUE;
					break;
				}
			}
		}
		
		List<VisacionEstructuradaDocumento> visaciones = resolucion.getVisacionesEstructuradas();
		if(visaciones != null){
			for (VisacionEstructuradaDocumento visacionEstructuradaDocumento : visaciones) {
				if(visacionEstructuradaDocumento.getPersona().equals(usuario)){
					estado = estado && Boolean.FALSE;
					break;
				}
			}
		}
		
		List<FirmaEstructuradaDocumento> firmantes = resolucion.getFirmasEstructuradas();
		if(firmantes != null){
			for (FirmaEstructuradaDocumento firmaEstructuradaDocumento : firmantes) {
				if(firmaEstructuradaDocumento.getPersona().equals(usuario)){
					estado = estado && Boolean.FALSE;
					break;
				}
			}
		}
		
		if (resolucion.getEstado() != null) {
			if (resolucion.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)) {
				estado = estado && Boolean.FALSE;
			}
		}
		
		if (this.getVisado()) {
			estado = estado && Boolean.FALSE;
		}
		if (this.getFirmado()) {
			estado = estado && Boolean.FALSE;
		}
		if (this.getAnulado()) {
			estado = estado && Boolean.FALSE;
		}
		
		return estado;
	}

//	@Override
//	public void agregarRelacionResolucion(Long id){
//		if (resolucion != null && resolucion.getResoluciones() == null) {
//			resolucion.setResoluciones(new ArrayList<Resolucion>());
//		}
//		if (!estaResolucionEnLista(id)) {
//			Resolucion resolucionRelacionada = em.find(Resolucion.class, id);
//			if (resolucionRelacionada != null) {
//				if (this.hijas.size() == 0 || this.hijas==null) {
//					resolucion.getResoluciones().add(resolucionRelacionada);
//					hijas.add(resolucionRelacionada);
//					FacesMessages.instance().add("Resolución relacionada agregada con éxito.");
//					return;
//				} else {
//					FacesMessages.instance().add("Solo se puede relacionar una Resolución");
//					return;
//				}
//			}
//		}else {
//			FacesMessages.instance().add("La resolución ya esta asociada.");
//			log.error("La resolución ya esta asociada.", id);
//		}
//		
//	}
//	
//	private boolean estaResolucionEnLista(final Long id) {
//		for (Resolucion res : resolucion.getResoluciones()) {
//			if (res.getId().equals(id)) {
//				return Boolean.TRUE;
//			}
//		}
//		return Boolean.FALSE;
//	}
//	
//	@Override
//	public void quitarRelacionResolucion(Resolucion hija){
//		log.info("Resolucion {0}, quitandole asociada {1}", resolucion.getNumeroDocumento(), hija.getNumeroDocumento());
//		this.hijas.remove(hija); 
//		resolucion.getResoluciones().remove(hija);
//		FacesMessages.instance().add("Resolucion relacionada eliminada.");
//	}
	
	private boolean existeDocumentoEntreRespuestas(Documento revisar){
		for(Documento doc: this.listDocumentosRespuesta)
			if(doc.getId().equals(revisar.getId()))
				return true;
		return false;
	}
	
	private int getNuevoDocumentoId(){
		int min = Integer.MAX_VALUE;
		for(Documento doc: this.listDocumentosRespuesta)
			min = Math.min(min, doc.getIdNuevoDocumento());
		return min-1;
	}
	
//	@Override
//	public void agregarResolucionHijaAExpediente(){
//		List<Resolucion> hijas = this.resolucion.getResoluciones();
//		for (Resolucion hija : hijas) {
//			if(hija == null || existeDocumentoEntreRespuestas(hija))
//				return;
//			DocumentoExpediente docExp = new DocumentoExpediente();
//			docExp.setDocumento(hija);
//			docExp.setEnEdicion(false);
//			docExp.setExpediente(this.expediente);
//			TipoDocumentoExpediente tipoDocExp = em.find(TipoDocumentoExpediente.class, TipoDocumentoExpediente.RESPUESTA);
//			docExp.setTipoDocumentoExpediente(tipoDocExp);
//			em.persist(docExp);
//			hija.setTipoDocumentoExpediente(tipoDocExp);
//			this.expediente.getDocumentos().add(docExp);
//			hija.setIdNuevoDocumento(getNuevoDocumentoId());
//			hija.setEnEdicion(false);
//			this.listDocumentosRespuesta.add(hija);
//		}
//	}
	
	private void getNextDestinatario() {
		final List<RevisarEstructuradaDocumento> revisionesE = resolucion.getRevisarEstructuradas();
		final List<VisacionEstructuradaDocumento> visacionesE = resolucion.getVisacionesEstructuradas();
		final List<FirmaEstructuradaDocumento> firmasE = resolucion.getFirmasEstructuradas();
		final List<RevisarDocumentos> revisiones = resolucion.getRevisar();
		final List<VisacionDocumento> visaciones = resolucion.getVisaciones();
		final List<FirmaDocumento> firmas = resolucion.getFirmas();
		boolean entro = false;
		listDestinatarios = new HashSet<Persona>();
		if (revisionesE != null) {
			List<Long> idsValidos = new LinkedList<Long>();
			for (RevisarEstructuradaDocumento rev : revisionesE) {
				if (rev.getId() != null) {
					idsValidos.add(rev.getId());
				}
			}
			if (idsValidos.size() == 0) {
				idsValidos.add(new Long(-1L));
			}
			md.limpiarRevisionesEstructuradasBorradas(this.resolucion.getId(),idsValidos);
			if(revisionesE.size() > 0){
				listDestinatarios.clear();
				Collections.sort(revisionesE, ComparatorUtils.revisarComparator);
				listDestinatarios.add(revisionesE.get(0).getPersona());
				this.resolucion.setEstado(new EstadoDocumento(EstadoDocumento.BORRADOR));
				entro = true;
			}
		}
		//!RE && !V && !F && not me
		if ((revisionesE == null || (revisionesE != null && revisionesE.size() == 0)) && 
				(visaciones == null ||(visaciones != null && visaciones.size() == 0)) && 
				(firmas == null || (firmas != null && firmas.size() == 0)) && 
				!this.resolucion.getAutor().equals(usuario) && 
				!entro) {
			listDestinatarios.clear();
			if (!this.resolucion.getAutor().getId().equals(usuario.getId())) {
				listDestinatarios.add(this.resolucion.getAutor());
			}
			entro = true;
		}
//		if (revisionesE == null || (visaciones != null && visaciones.size() == 0) && 
//				this.resolucion.getAutor().equals(usuario) && 
//				((visacionesE != null && visacionesE.size() > 0) || (firmasE != null && firmasE.size() > 0)) &&
//				!entro) {
//			listDestinatarios.clear();
//			listDestinatarios.add(visacionesE.get(0).getPersona());
//			entro = true;
//		}
		if (visacionesE != null) {
			List<Long> idsValidos = new LinkedList<Long>();
			for (VisacionEstructuradaDocumento rev : visacionesE) {
				if (rev.getId() != null) {
					idsValidos.add(rev.getId());
				}
			}
			if (idsValidos.size() == 0) {
				idsValidos.add(new Long(-1L));
			}
			md.limpiarVisacionesEstructuradasBorradas(this.resolucion.getId(),idsValidos);
			if(visacionesE.size() > 0 && !entro){
				listDestinatarios.clear();
				Collections.sort(visacionesE, ComparatorUtils.visasComparator);
				if (!usuario.getId().equals((visacionesE.get(0).getPersona()).getId())) {
					listDestinatarios.add(visacionesE.get(0).getPersona());
				}
				entro = true;
			}
		}
		if (firmasE != null) {
			List<Long> idsValidos = new LinkedList<Long>();
			for (FirmaEstructuradaDocumento rev : firmasE) {
				if (rev.getId() != null) {
					idsValidos.add(rev.getId());
				}
			}
			if (idsValidos.size() == 0) {
				idsValidos.add(new Long(-1L));
			}
			md.limpiarFirmasEstructuradasBorradas(this.resolucion.getId(),idsValidos);
			if(firmasE.size() > 0&& !entro){
				listDestinatarios.clear();
				Collections.sort(firmasE, ComparatorUtils.firmasComparator);
				if (!usuario.getId().equals(firmasE.get(0).getPersona().getId())) {
					listDestinatarios.add(firmasE.get(0).getPersona());
				}
				entro = true;
			}
		}
	}

	@Override
	public List<Resolucion> getHijas() {
		return hijas;
	}

	@Override
	public void setHijas(List<Resolucion> hijas) {
		this.hijas = hijas;
	}
	
	@Transactional
	private void limpiarDestinatarios(final Long id) {
		try {
			Query query = em.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}
	
	@Transactional
	private void eliminaAntecedente(Long id){
		ArchivoAdjuntoDocumentoElectronico archivo = em.find(ArchivoAdjuntoDocumentoElectronico.class, id);
		em.remove(archivo);
	}
	
	@Transactional
	private void removeRevisarEstructurada(Long id) {
		RevisarEstructuradaDocumento revisor = em.find(RevisarEstructuradaDocumento.class, id);
		em.remove(revisor);
	}
	
	@Override
	public void previsualizar() {
		resolucion.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.RESOLUCION));
		if (tipoResolucion == null) {
			tipoResolucion = getTipoResolucion();
		}
		resolucion.setTipo(em.find(TipoRazon.class, tipoResolucion));
		resolucion.setListaPersonas(new ArrayList<ListaPersonasDocumento>());
		setDistribucionDocumento(resolucion);
		List<ArchivoAdjuntoDocumentoElectronico> listaArchivos = getListaArchivos();
		resolucion.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		for (ArchivoAdjuntoDocumentoElectronico a : listaArchivos) {
			resolucion.getArchivosAdjuntos().add(a);
		}
	}
}
