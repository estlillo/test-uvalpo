package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface SeleccionarDistribucionDocumento {

	void destroy();

	Long getDivision();

	void setDivision(Long division);

	Long getDepartamento();

	void setDepartamento(Long departamento);

	Long getUnidadOrganizacional();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getCargo();

	void setCargo(Long cargo);

	List<SelectItem> getListDivision();

	List<SelectItem> getListDepartamento();

	List<SelectItem> getListUnidadesOrganizacionales();

	List<SelectItem> getListCargos();

	List<SelectItem> getListPersonas();

	void buscarDepartamentos();

	void buscarUnidadesOrganizacionales();

	void buscarCargos();

	void buscarPersonas();

	void agregarDistribucion();
	
	void agregarDistribucionUnidad();

	void agregarDestinatarioExterno();

	List<SelectPersonas> getDistribucionDocumento();

	Long getPersona();

	void setPersona(Long persona);

	void eliminarDistribucionDocumento(String destinatario);

	void buscarPersonasExternas();

	Long getDependencia();

	void setDependencia(Long dependencia);

	Long getPersonaExterna();

	void setPersonaExterna(Long personaExterna);

	List<SelectItem> getListDependencia();

	List<SelectItem> getListPersonasExternas();

	List<DestinatariosFrecuentes> getListListas();

	void setListListas(List<DestinatariosFrecuentes> listListas);

	/**
	 * @param idLista {@link Long}
	 */
	void agregarListaDistribucion(Long idLista);

	/**
	 * @return {@link Long}
	 */
	Long getGrupo();

	/**
	 * @param grupo {@link Long}
	 */
	void setGrupo(Long grupo);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDestFrec();

	/**
	 * @param listDestFrec {@link List} of {@link SelectItem}
	 */
	void setListDestFrec(List<SelectItem> listDestFrec);

	void buscarDivisiones();

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion {@link Long}
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion {@link List} of {@link SelectItem}
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);


	void agregarPorId(Long id);

	void limpiarDestinatario();

}
