package cl.exe.exedoc.session;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Persona;

/**
 * Interface {@link CambioClaveBean}.
 * 
 * @author Ricardo Fuentes
 */
@Local
public interface CambioClave {

	/**
	 * Metodo que inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * Metodo que termina la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Metodo que destruye la converzacion.
	 */
	void destroy();

	/**
	 * @return {@link Persona}
	 */
	Persona getPersona();

	/**
	 * @param persona {@link Persona}
	 */
	void setPersona(Persona persona);

	/**
	 * @return {@link String}
	 */
	String getClaveActual();

	/**
	 * @param claveActual {@link String}
	 */
	void setClaveActual(String claveActual);

	/**
	 * @return {@link String}
	 */
	String getClaveNueva();

	/**
	 * @param claveNueva {@link String}
	 */
	void setClaveNueva(String claveNueva);

	/**
	 * @return {@link String}
	 */
	String getRepetirClave();

	/**
	 * @param repetirClave {@link String}
	 */
	void setRepetirClave(String repetirClave);

	/**
	 * Metodo que cambia la clave del usuario logeado.
	 */
	String cambiarClave();
}
