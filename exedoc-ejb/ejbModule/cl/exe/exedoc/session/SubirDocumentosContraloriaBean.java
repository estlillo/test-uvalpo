package cl.exe.exedoc.session;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;

@Name("subirDocumentosContraloria")
@Stateful
public class SubirDocumentosContraloriaBean implements SubirDocumentosContraloria {

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager entityManager;

	private String ulrSubirDocumentoContraloria = "subirDocumentosContraloria";

	private Integer tipoRespuestaContraloria;
	private Integer tipoRespuestaContraloriaDefault = 1;
	private byte[] archivo;

	@Begin(join = true)
	public String begin() {
		log.info("Comenzando Subir Documentos a Contraloría");
		this.limpiar();
		return ulrSubirDocumentoContraloria;
	}

	private void limpiar() {
		tipoRespuestaContraloria = tipoRespuestaContraloriaDefault;
		archivo = null;
	}

	public String ingresarDocumento() {
		if (archivo == null) {
			FacesMessages.instance().add("Debe Ingresar un documento");
			return ulrSubirDocumentoContraloria;
		}
		//log.info(new String(archivo));
		if (this.revisaDocumento()) {
			FacesMessages.instance().add("Documento Ingresado");
			this.limpiar();
			return ulrSubirDocumentoContraloria;
		}
		return ulrSubirDocumentoContraloria;
	}

	private boolean revisaDocumento() {
		String numeroDocumento = null;
		String agnoDocumento = null;

		Class<?> clase = discriminaDocumento(archivo);
		if (clase == null) {
			FacesMessages.instance().add("No se ha podido rescatar el tipo de documento");
			return false;
		}

		try {
			numeroDocumento = evaluaXpath(archivo, "/*/*/c:Folio/c:Numero/text()");
			agnoDocumento = evaluaXpath(archivo, "/*/*/c:Folio/c:Agno/text()");
		} catch (XPathExpressionException e) {
			FacesMessages.instance().add("No se ha podido rescatar el folio de documento");
			e.printStackTrace();
			return false;
		}
		if (numeroDocumento == null || agnoDocumento == null) {
			FacesMessages.instance().add("No se ha podido rescatar el numero de documento");
			return false;
		}

		return true;
	}

	NamespaceContext contraloriaNS = new NamespaceContext() {
		public String getNamespaceURI(String prefix) {
			if (prefix.equals("c"))
				return "http://www.contraloria.cl/2005/05/CGRDoc";
			else
				return XMLConstants.DEFAULT_NS_PREFIX;
		}

		public String getPrefix(String namespace) {
			return null;
		}

		public Iterator<?> getPrefixes(String namespace) {
			return null;
		}
	};

	private String evaluaXpath(byte[] xml, String expXpath) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		ByteArrayInputStream is = new ByteArrayInputStream(xml);
		InputSource inputSource = new InputSource(is);

		xpath.setNamespaceContext(contraloriaNS);
		XPathExpression xp = xpath.compile(expXpath);
		return xp.evaluate(inputSource);

	}

	private Class<?> discriminaDocumento(byte[] xml) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(contraloriaNS);
		String expressionResolucion = "/c:Documento/c:Contenido/c:Resolucion";
		String expressionDecreto = "/c:Documento/c:Contenido/c:Decreto";
		ByteArrayInputStream is = new ByteArrayInputStream(xml);
		InputSource inputSource = new InputSource(is);
		// Resolucion
		try {
			NodeList nodes = (NodeList) xpath.evaluate(expressionResolucion, inputSource, XPathConstants.NODESET);
			if (nodes.getLength() != 0) {
				return Resolucion.class;
			}
		} catch (XPathExpressionException e) {
		}
		// Decreto
		try {
			NodeList nodes = (NodeList) xpath.evaluate(expressionDecreto, inputSource, XPathConstants.NODESET);
			if (nodes.getLength() != 0) {
				return Decreto.class;
			}
		} catch (XPathExpressionException e) {
		}
		return null;
	}

	public void uploadListener(UploadEvent event) throws IOException {
		UploadItem item = event.getUploadItem();
		// String fileName = item.getFileName();
		String contentType = item.getContentType();
		if (!contentType.equals("text/xml")) {
			FacesMessages.instance().add("Documento no válido");
			return;
		}
		this.archivo = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
	}

	@End
	public String end() {
		return "home";
	}

	public Integer getTipoRespuestaContraloria() {
		return tipoRespuestaContraloria;
	}

	public void setTipoRespuestaContraloria(Integer tipoRespuestaContraloria) {
		this.tipoRespuestaContraloria = tipoRespuestaContraloria;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

}
