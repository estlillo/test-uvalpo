package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearConvenio {

	public void destroy();

	public String end();

	public String buscaDocumento();

	public void visualizar();

	public void firmar();

	public void visar();

	public String desvisarYDesfirmar(Convenio dec);

	public boolean renderedBotonDesvisarDesfirmar();

	public String getTipoArchivo();

	public void setTipoArchivo(String tipoArchivo);

	public void agregarDocumento();

	public void agregarAntecedente();

	public void listener(UploadEvent event) throws Exception;

	public void salirPanel();

	public void eliminarArchivoAntecedente(Integer idNuevoArchivo);

	public void plantillaListener(ValueChangeEvent event);
	
	public void plantillaResolucionListener(ValueChangeEvent event);

	public List<SelectItem> obtenerPlantillas();
	
	public List<SelectItem> obtenerPlantillasResolucion();
	
	public String generarResolucion();

	public String cambiar();

	public Convenio getConvenio();

	public String getConsiderando();

	public void setConsiderando(String considerando);

	public String getTeniendoPresente();

	public void setTeniendoPresente(String teniendoPresente);

	public String getConvienen();

	public void setConvienen(String convienen);

	public List<SelectPersonas> getDistribucionDocumento();

	public void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento);

	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	public void verArchivo(Long id);

	public Boolean getFirmado();

	public Boolean getVisado();

	public String getVisaciones();

	public List<VisacionDocumento> getVisacionesV();

	public List<FirmaEstructuradaDocumento> getFirmas();

	public boolean modificarVisaFirmas();

	public String despacharExpediente();

	public boolean isRenderedFirmar();

	public boolean isRenderedVisar();

	public boolean isDisabledGuardar();

	public boolean isRenderedBotonDespachar();

	public boolean isDespachable();

	public void guardarYCrearExpediente();

//	public List<SelectItem> obtenerClasificacionSubTipoOtros();
//
//	public List<SelectItem> obtenerClasificacionTipoOtros();

	public boolean getEditable();

	public boolean isRenderedBotonGuardarCrearExpediente();

	public boolean isEditable();

	public boolean isEditableSinVisa();

	public boolean isRenderedBotonVisar();

	public boolean isRenderedBotonGuardar();

	public boolean isRenderedBotonVisualizar();

	public String getUrlFirma();

	public String getMateriaArchivo();

	public void setMateriaArchivo(String materiaArchivo);

	public void limpiarAnexos();

	public int getMaxFilesQuantity();

//	public Integer getIdClasificacionSubTipoOtrosSelec();
//
//	public void setIdClasificacionSubTipoOtrosSelec(Integer idClasificacionSubTipoOtrosSelec);
//
//	public Integer getIdClasificacionTipoOtrosSelec();
//
//	public void setIdClasificacionTipoOtrosSelec(Integer idClasificacionTipoOtrosSelec);
//
//	public List<SelectItem> getTipos1Otros();
//
//	public void setTipos1Otros(List<SelectItem> tipos1Otros);
//
//	public List<SelectItem> getTipos2Otros();
//
//	public void setTipos2Otros(List<SelectItem> tipos2Otros);

	public Integer getIdPlantilla();

	public void setIdPlantilla(Integer idPlantilla);
	
	public Integer getIdPlantillaResolucion();
	
	public void setIdPlantillaResolucion(Integer idPlantillaResolucion);
}
