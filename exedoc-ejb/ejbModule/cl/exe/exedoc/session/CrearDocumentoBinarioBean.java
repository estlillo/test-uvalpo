package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.hibernate.validator.Length;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoBinario;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.mantenedores.AdministradorPersona;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;

@Stateful
@Name("crearDocumentoBinario")
@Scope(ScopeType.SESSION)
public class CrearDocumentoBinarioBean implements CrearDocumentoBinario {

	@Logger
	private Log log;
	
	private static final String SELECCIONAR = JerarquiasLocal.TEXTO_INICIAL;

	private static final String NO_PUEDE_AGREGAR = "No se puede agregar a esta persona como destinatario, ";
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;
	
	@EJB
	private AdministradorPersona adminPersona;
	
	@EJB
	private AdministradorAlertas administradorAlertas;

	@In(required = false)
	private String homeCrear;
	
	@In(required = false, scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrearDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	// Revisar documentos anexos
	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private
	 * List<Documento> listDocumentosAnexo;
	 */

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, value = "documento")
	private DocumentoBinario documento;

	// private ArchivoDocumentoBinario archivo;
	
	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	// Datos Documento
//	@Length(max = 2000)
//	private String materia;
	private List<SelectItem> listTipoDocumentos;
	private Integer tipoDocumentoID;
	
	// DestinatarioDocumento
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<SelectPersonas> distribucionDocumento;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	private List<SelectItem> nivelesUrgencia = new ArrayList<SelectItem>();
	private Long idAlerta;

	private String destinatario = "";
	private boolean deshabilitarUpload=false;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean estaBandejaEntradaCopia;
	
//	private List<ArchivoAdjuntoDocumentoBinario> archivosAntecedentes;
	
	@Begin(join = true)
	@Override
	public void begin() {
//		if (archivosAntecedentes == null) {
//			this.inicializaArchivosAntecedentes();
//			if (documento.getArchivosAdjuntos() != null) {
//				for (ArchivoAdjuntoDocumentoBinario a : documento.getArchivosAdjuntos()) {
//					a.setIdNuevoArchivo(archivosAntecedentes.size());
//					archivosAntecedentes.add(a);
//				}
//			}
//		}
		this.cargarNivelesUrgencia();
	}
	
	public String end() {
		listDestinatarios = new java.util.HashSet<Persona>();
//		this.materia = "";
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		//this.distribucionDocumento = new ArrayList<SelectPersonas>();
		this.tipoDocumentoID = 0;
		//archivosAntecedentes = null;
		//adjuntoEliminar = null;
		if (this.vistoDesdeReporte != null && this.vistoDesdeReporte) {
			if (this.homeCrearDesdeReporte != null ) { //&& "reporte".equalsIgnoreCase(this.homeCrear.substring(0, 7))
				return this.homeCrearDesdeReporte;
			}
		}
		if (documento.getId() != null) {
			documento.setEliminable(md.isEliminable(documento, expediente, usuario));
		}
		return homeCrear;
	}
		
	@Override
	public String validaCampos()
	{
		boolean validado = true;
		
		if (this.getIdAlerta() != null && !this.getIdAlerta().equals(JerarquiasLocal.INICIO)) {
			this.getDocumento().setAlerta(
					administradorAlertas.buscarAlerta(this.getIdAlerta()));
		}
		
		if (tipoDocumentoID == JerarquiasLocal.INICIO.intValue()) {
			FacesMessages.instance().add("Debe seleccionar el Tipo de Documento.");
			validado = false;
		}
		if (documento.getNumeroDocumento() == null || 
				(documento.getNumeroDocumento() != null && documento.getNumeroDocumento().trim().length() == 0)) {
			FacesMessages.instance().add("El Número de Documento es obligatorio");
			validado = false;
		}
		if (documento.getAntecedentes() != null) {
			documento.setAntecedentes(documento.getAntecedentes().trim());
			if (documento.getAntecedentes().length() > 2000) {
				FacesMessages.instance().add("El Antecedente debe ser menor a 2000 caracteres");
				validado = false;
			}
		}
		if (documento.getMateria() == null || 
				(documento.getMateria() != null && documento.getMateria().trim().length() == 0)) {
			FacesMessages.instance().add("La Materia es obligatoria");
			validado = false;
		} else {
			documento.setMateria(documento.getMateria().trim());
			if (documento.getMateria().length() > 2000) {
				FacesMessages.instance().add("La Materia debe ser menor a 2000 caracteres");
				validado = false;
			}
		}
		if (documento.getFechaDocumentoOrigen() == null) {
			FacesMessages.instance().add("La Fecha de Origen es obligatoria");
			validado = false;
		}
		if(documento.getEmisor() == null || (documento.getEmisor() != null && documento.getEmisor().trim().length() == 0)) {
			FacesMessages.instance().add("El campo \"De\" es obligatorio");
			validado = false;
		} else {
			documento.setEmisor(documento.getEmisor().trim());
			if (documento.getEmisor() != null && documento.getEmisor().length() > 255){
				FacesMessages.instance().add("El campo \"De\" debe ser menor a 255 caracteres.");
				validado = false;
			}
		}
		if(this.destinatario != null && this.destinatario.trim().length() > 255){
			FacesMessages.instance().add("El campo \"A\" debe ser menor a 255 caracteres.");
			validado = false;
		}
		if ((destinatariosDocumento == null || destinatariosDocumento.size() == 0)) {

			if (this.getDestinatario() == null || this.getDestinatario().trim().equals("")) {
				FacesMessages.instance().add("Debe ingresar un Destinatario al Documento");
				validado = false;
			}
		}
		if (getArchivo() == null || getArchivo().getNombreArchivo() == null) {
			FacesMessages.instance().add(
					"Debe Adjuntar un Documento Digitalizado");
			validado = false; 
		}
		if (documento.getPlazo() != null) {
			if (documento.getPlazo().compareTo(documento.getFechaDocumentoOrigen()) < 0) {
				FacesMessages.instance().add("El Plazo no puede ser anterior a la Fecha Origen del Documento");
				validado = false;
			}
		}
		if(validado){
			this.armaDocumento();
			return end();
		}else{
			deshabilitarUpload=false;
			return "";
		}		
	}
	
	public void agregarDocumento() {
		
		long start = System.currentTimeMillis();			
			LogTiempos.mostrarLog(this.usuario.getUsuario(),
					"Documento Binario", System.currentTimeMillis() - start);
			this.armaDocumento();
	}

	private void armaDocumento() {
//		if (!this.documento.getReservado()) {
//			this.documento.setMateria(this.materia);
//		}
		documento.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.DIGITAL));
		this.removeDestinatarios(documento.getId());
		this.setDestinatarioDocumento(documento);
		//this.setDistribucionDocumento(documento);
		final TipoDocumento tipo = em.find(TipoDocumento.class, this.tipoDocumentoID);
		documento.setTipoDocumento(tipo);
		
//		if (adjuntoEliminar != null) {
//			Iterator<ArchivoAdjuntoDocumentoBinario> a = adjuntoEliminar.iterator();
//			while (a.hasNext()) {
//				ArchivoAdjuntoDocumentoBinario aa = new ArchivoAdjuntoDocumentoBinario(); 
//				aa = a.next();
//				this.eliminarArchivoAntecedente(aa.getId());
//			}
//		}
//		
//		documento.setArchivosAdjuntos(archivosAntecedentes);
		
		/*
		 * if (archivo != null) { archivo.setFecha(new Date());
		 * documento.setArchivo(archivo); }
		 */
		if(this.documento.getArchivo()!=null)
			this.documento.getArchivo().setFecha(new Date());

		if (documento.getEstado() == null) {
			documento.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
			//documento.setEliminable(true);
		}

		if (this.documento.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.documento);
			} else {
				if (this.documento.getId() != null) {
					listDocumentosRespuesta.remove(this.documento);
					listDocumentosRespuesta.add(this.documento);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		}/*
		 * else if(this.documento.getTipoDocumentoExpediente().getId().equals(
		 * TipoDocumentoExpediente.ANEXO)) { if
		 * (!existeDocumento(listDocumentosAnexo)) {
		 * listDocumentosAnexo.add(this.documento); } else { if
		 * (this.documento.getId() != null) {
		 * listDocumentosAnexo.remove(this.documento);
		 * listDocumentosAnexo.add(this.documento); } else {
		 * this.reemplazaDocumento(listDocumentosAnexo); } } }
		 */else if (this.documento.getTipoDocumentoExpediente().getId()
				.equals(TipoDocumentoExpediente.ORIGINAL)) {
			setDocumentoOriginal(documento);
		}
		persistirDocumento();
	}

	private void persistirDocumento() {
		try {
			if (this.documento.getId() != null) {
				md.actualizarDocumento(this.documento);
				FacesMessages.instance().add("Documento Modificado Exitosamente.");

			} else if (this.expediente != null && this.expediente.getId() != null) {
				me.agregarRespuestaAExpediente(expediente, this.documento, true);
				FacesMessages.instance().add("Documento Guardado Exitosamente.");
			}
		} catch (DocumentNotUploadedException e) {
			e.printStackTrace();
		}
		/*
		 * if (this.expediente != null) { for (Documento doc :
		 * listDocumentosAnexo) { if (doc.getId() == null) { if (doc instanceof
		 * DocumentoBinario) { DocumentoBinario docBin = (DocumentoBinario) doc;
		 * if(docBin.getIdDocumentoReferencia().equals(this.documento.
		 * getIdNuevoDocumento())) { me.agregarAnexoAExpediente(expediente, doc,
		 * true, this.documento.getId()); } doc.setEnEdicion(true); } } } }
		 */
	}
	@Override
	public void listener(final UploadEvent event) throws Exception {
		final ArchivoDocumentoBinario archivo = new ArchivoDocumentoBinario();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
		documento.setArchivo(archivo);
		
		
//		this.fileUploadCurrent.clear();
//		this.fileUploadCurrent.add(event.getUploadItem());
	}
	
	@Override
	public void listenerQuitar() {
		this.deshabilitarUpload=false;
		this.documento.setArchivo(null);   
	}
	
	@Override
	public void clearUploadData() {
		log.info("[CrearDocumentoBinario] Limpiando archivo");
		documento.setArchivo(null);
	}


	public void verArchivo() {
		// byte[] data =
		// repositorio.recuperarArchivo(documento.getArchivo().getCmsId());
		try {
			final byte[] data = repositorio.getFile(documento.getArchivo()
					.getCmsId());
			if (data != null) {
				final FacesContext facesContext = FacesContext
						.getCurrentInstance();
				try {
					final HttpServletResponse response = (HttpServletResponse) facesContext
							.getExternalContext().getResponse();
					response.setContentType(documento.getArchivo()
							.getContentType());
					response.setHeader("Content-disposition",
							"attachment; filename=\""
									+ documento.getArchivo().getNombreArchivo()
									+ "\"");
					// .replace(' ',
					// '_'));
					response.getOutputStream().write(data);
					response.getOutputStream().flush();
					facesContext.responseComplete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ documento.getArchivo().getId() + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean existeDocumento(final List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(
					this.documento.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		final List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc
				.hasNext();) {
			final Documento docTmp = doc.next();
			final Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.documento.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.documento);
			}
		}
		listaDocumentos = lista;
	}

	private void setDestinatarioDocumento(final Documento dc) {
		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		if (destinatariosDocumento.size() == 0) {
			// List<ListaPersonasDocumento> list = new
			// ArrayList<ListaPersonasDocumento>();
			DestinatarioDocumento personadocumento = new DestinatarioDocumento();
			personadocumento.setDestinatario(this.getDestinatario());
			personadocumento.setDocumento(dc);
			ddList.add(personadocumento);
			// documento.setDestinatarios(ddList);
		}
		else{
			if (dc != null && dc.getId() != null)
				this.limpiarDestinatarios(dc.getId());
			for (SelectPersonas item : this.destinatariosDocumento) {
				final DestinatarioDocumento dd = new DestinatarioDocumento();
				dd.setDestinatario(item.getDescripcion());
				dd.setDestinatarioPersona(item.getPersona());
				dd.setDocumento(dc);
				ddList.add(dd);
			}
		}
		dc.setDestinatarios(ddList);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void limpiarDestinatarios(final Long id) {

		try {
			Query query = em
					.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}
	
//	private void setDistribucionDocumento(final Documento dc) {
//		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
//		for (SelectPersonas item : this.distribucionDocumento) {
//			final DistribucionDocumento dd = new DistribucionDocumento();
//			dd.setDestinatario(item.getDescripcion());
//			dd.setDestinatarioPersona(item.getPersona());
//			dd.setDocumento(dc);
//			ddList.add(dd);
//
//		}
//		dc.setDistribucion(ddList);
//	}

	private List<SelectItem> buscarTipoDocumentos() {
		return TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoNoElectronicoVisible(SELECCIONAR);
	}

	@Override
	public void cargarNivelesUrgencia() {
		//this.inicializaArchivosAntecedentes();
			
		if (this.getDocumento() != null
				&& this.getDocumento().getAlerta() != null
				&& this.getDocumento().getId() != null) {
			this.setIdAlerta(this.getDocumento().getAlerta().getId());
		} else {
			this.setIdAlerta(JerarquiasLocal.INICIO);
		}

		final List<Alerta> alertas = administradorAlertas.buscarAlerta();
		this.setNivelesUrgencia(new ArrayList<SelectItem>());

		this.getNivelesUrgencia().add(
				new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR, SELECCIONAR, false, true));

		if (alertas != null) {
			for (Alerta a : alertas) {
				this.getNivelesUrgencia().add(
						new SelectItem(a.getId(), a.getNombre()));
			}
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(final Expediente expediente) {
		this.expediente = expediente;
	}

	public List<Documento> getListDocumentosRespuesta() {
		return listDocumentosRespuesta;
	}

	public void setListDocumentosRespuesta(
			final List<Documento> listDocumentosRespuesta) {
		this.listDocumentosRespuesta = listDocumentosRespuesta;
	}

//	public String getMateria() {
//		if (this.documento != null) {
//			if (this.documento.getReservado()) {
//				materia = "Materia Reservada";
//			} else {
//				materia = this.documento.getMateria();
//			}
//		}
//		return materia;
//	}
//
//	public void setMateria(final String materia) {
//		this.materia = materia;
//	}

	public List<SelectItem> getListTipoDocumentos() {
		if (listTipoDocumentos == null) {
			listTipoDocumentos = this.buscarTipoDocumentos();
		}
		return listTipoDocumentos;
	}

	public Integer getTipoDocumentoID() {
		if (this.documento != null && this.documento.getTipoDocumento() != null) {
			this.tipoDocumentoID = this.documento.getTipoDocumento().getId();
		}
		return tipoDocumentoID;
	}

	public void setTipoDocumentoID(final Integer tipoDocumentoID) {
		this.tipoDocumentoID = tipoDocumentoID;
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		/*
		 * destinatariosDocumento = new ArrayList<SelectPersonas>(); if
		 * (this.documento != null && this.documento.getDestinatarios() != null
		 * && this.documento.getDestinatarios().size() != 0) { for
		 * (ListaPersonasDocumento lpd : this.documento.getDestinatarios()) {
		 * destinatariosDocumento.add(new SelectPersonas(lpd.getDestinatario(),
		 * lpd.getDestinatarioPersona())); } }
		 */
		return destinatariosDocumento;
	}

//	public List<SelectPersonas> getDistribucionDocumento() {
//		/*
//		 * distribucionDocumento = new ArrayList<SelectPersonas>(); if
//		 * (this.documento != null && this.documento.getDistribucionDocumento()
//		 * != null && this.documento.getDistribucionDocumento().size() != 0) {
//		 * for (ListaPersonasDocumento lpd :
//		 * this.documento.getDistribucionDocumento()) {
//		 * distribucionDocumento.add(new SelectPersonas(lpd.getDestinatario(),
//		 * lpd.getDestinatarioPersona())); } }
//		 */
//		return distribucionDocumento;
//	}

	public DocumentoBinario getDocumento() {
		return documento;
	}

	public void setDocumento(final DocumentoBinario documento) {
		this.documento = documento;
	}

	public ArchivoDocumentoBinario getArchivo() {
		// if (documento != null && documento.getArchivo() != null) {
		return documento.getArchivo();
		// }
		// return archivo;
	}

	/***************************************************************************
	 * Archivos Antecedentes
	 **************************************************************************/

	// private List<ArchivoAdjuntoDocumentoBinario> archivosAntecedentes;
	//private String materiaArchivo;
	//private ArchivoAdjuntoDocumentoBinario archivoAdjuntoTemporal;

	@In(required = true)
	private Persona usuario;

//	private void limpiarAnexos() {
//		archivoAdjuntoTemporal = null;
//		materiaArchivo = null;
//	}

	public int getMaxFilesQuantity() {
		final int valor = 20;
		return valor;
	}

//	@Override
//	public void listenerAntecedentes(final UploadEvent event) throws Exception {
//		this.inicializaArchivosAntecedentes();
//		archivoAdjuntoTemporal = new ArchivoAdjuntoDocumentoBinario();
//		final UploadItem item = event.getUploadItem();
//		String fileName = item.getFileName();
//		final StringTokenizer st = new StringTokenizer(fileName, "\\");
//		while (st.hasMoreElements()) {
//			fileName = st.nextToken();
//		}
//		final String contentType = item.getContentType();
//		archivoAdjuntoTemporal.setNombreArchivo(fileName);
//		archivoAdjuntoTemporal.setContentType(contentType);
//		final byte[] data = org.apache.commons.io.FileUtils
//				.readFileToByteArray(item.getFile());
//		archivoAdjuntoTemporal.setArchivo(data);
//	}

//	public void agregarAntecedentes() {
//		if(this.archivoAdjuntoTemporal==null){
//			FacesMessages.instance().add(
//					"Debe adjuntar un archivo para los antecedentes");
//			return;
//		}
//		if (!existeNombreArchivo(archivoAdjuntoTemporal)) {
//			archivoAdjuntoTemporal.setFecha(new Date());
//			archivoAdjuntoTemporal.setIdNuevoArchivo(archivosAntecedentes.size());
//			archivoAdjuntoTemporal.setAdjuntadoPor(usuario);
//			archivoAdjuntoTemporal.setMateria(materiaArchivo);
//			archivoAdjuntoTemporal.setDocumentoBinario(this.documento);
//			archivosAntecedentes.add(archivoAdjuntoTemporal);
//			archivoAdjuntoTemporal = null;
//		} else {
//			FacesMessages.instance().add(
//					"El Nombre de Archivo ya existe como adjunto");
//		}
//		limpiarAnexos();
//	}
	
//	@Override
//	public ArchivoAdjuntoDocumentoBinario getArchivoAdjuntoTemporal() {
//		return archivoAdjuntoTemporal;
//	}
//
//	@Override
//	public void setArchivoAdjuntoTemporal(ArchivoAdjuntoDocumentoBinario doc) {
//		archivoAdjuntoTemporal = doc;
//	}
//	@Override
//	public void clearUploadAntecedente() {
//		archivoAdjuntoTemporal = null;
//	}
//	private boolean existeNombreArchivo(
//			final ArchivoAdjuntoDocumentoBinario archivo) {
//		return this.archivosAntecedentes.contains(archivo);
//	}

//	public void verArchivoAntecedente(final Long idArchivo) {
//		final ArchivoAdjuntoDocumentoBinario a = em.find(
//				ArchivoAdjuntoDocumentoBinario.class, idArchivo);
//		buscarArchivoRepositorio(a);
//	}

	private void buscarArchivoRepositorio(final Archivo a) {
		// byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		try {
			final byte[] data = repositorio.getFile(a.getCmsId());
			if (data != null) {
				final FacesContext facesContext = FacesContext
						.getCurrentInstance();
				try {
					final HttpServletResponse response = (HttpServletResponse) facesContext
							.getExternalContext().getResponse();
					response.setContentType(a.getContentType());
					response.setHeader("Content-disposition",
							"attachment; filename=\"" + a.getNombreArchivo()
									+ "\"");
					response.getOutputStream().write(data);
					response.getOutputStream().flush();
					facesContext.responseComplete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ a.getId() + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//private List<ArchivoAdjuntoDocumentoBinario> adjuntoEliminar; 
	
//	public void eliminarArchivoAntecedente(final Integer idNuevoArchivo) {
//		this.inicializaArchivosAntecedentes();
//
//		if (adjuntoEliminar == null)
//			adjuntoEliminar = new ArrayList<ArchivoAdjuntoDocumentoBinario>();
//		Iterator<ArchivoAdjuntoDocumentoBinario> a = archivosAntecedentes.iterator();
//		int i = 0;
//		while (a.hasNext()) {
//			ArchivoAdjuntoDocumentoBinario aa = new ArchivoAdjuntoDocumentoBinario(); 
//			aa = a.next();
//			if (aa.getIdNuevoArchivo().equals(idNuevoArchivo)){
//				System.out.println("idNuevoArchivo:" + idNuevoArchivo);
//				if (aa.getId() != null) {
//					adjuntoEliminar.add(aa);
//					System.out.println("id:" + aa.getId());
//				}
//				else {
//					System.out.println("id:null");
//				}
//				archivosAntecedentes.remove(i);
//				break;
//			}
//			i++;
//		}
//	}
//	
//	@Transactional
//    private void eliminarArchivoAntecedente(Long id){
//		ArchivoAdjuntoDocumentoBinario mv = em.find(ArchivoAdjuntoDocumentoBinario.class, id);
//        em.remove(mv);
//    }
	
	/**
	 * @return {@link List} of {@link ArchivoAdjuntoDocumentoBinario}
	 */
//	@Override
//	public List<ArchivoAdjuntoDocumentoBinario> getArchivosAntecedentes() {
//		return archivosAntecedentes;
//	}
//
//	public List<ArchivoAdjuntoDocumentoBinario> getListaArchivosAntecedente() {
//		return archivosAntecedentes;
//	}
//
//	public String getMateriaArchivo() {
//		limpiarAnexos();
//		return materiaArchivo;
//	}
//
//	public void setMateriaArchivo(final String materiaArchivo) {
//		this.materiaArchivo = materiaArchivo;
//	}

	@Override
	public List<SelectItem> getNivelesUrgencia() {
		return nivelesUrgencia;
	}

	@Override
	public void setNivelesUrgencia(final List<SelectItem> nivelesUrgencia) {
		this.nivelesUrgencia = nivelesUrgencia;
	}

	@Override
	public Long getIdAlerta() {
		return idAlerta;
	}

	@Override
	public void setIdAlerta(final Long idAlerta) {
		this.idAlerta = idAlerta;
	}

	@Override
	public void setDocumentoOriginal(final Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}

	@Override
	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}

	@Override
	public List<Persona> autocomplete(Object suggest) {
		
		final String pref = (String) suggest;
		
		List<Persona> personas = adminPersona.getPersonas(pref, destinatariosDocumento);
		
		return personas;
	}

	
	
	public void seleccionarDest(Persona p) {
		
		SelectPersonas select = new SelectPersonas(p.getNombreApellido()+" - "+p.getCargo().getDescripcion()+" - "+p.getCargo().getUnidadOrganizacional().getDescripcion(), p);
		
		if(!destinatariosDocumento.contains(select)){
			destinatariosDocumento.add(select);
		}
		else{
			FacesMessages.instance().add(NO_PUEDE_AGREGAR);
		}
	}

	@Override
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;

	}

	@Override
	public String getDestinatario() {
		// TODO Auto-generated method stub
		return this.destinatario;
	}
	@Override
	public void removeDestinatarios(Long id) {
		md.removeDestinatarios(id);
	}
	@Override
	public boolean getDeshabilitarUpload(){
		return this.deshabilitarUpload;
	}
	
//	/**
//	 * 
//	 */
//	private List<org.richfaces.model.UploadItem> fileUploadCurrent = new LinkedList<org.richfaces.model.UploadItem>();
//
//	public List<org.richfaces.model.UploadItem> getFileUploadCurrent() {
//		return fileUploadCurrent;
//	}
//
//	public void setFileUploadCurrent(List<org.richfaces.model.UploadItem> fileUploadCurrent) {
//		this.fileUploadCurrent = fileUploadCurrent;
//	}
	
//	@Override
//	public void verArchivoAntecedentes(final Long id) {
//		try {
//			if (!DocUtils.getFile(id, em)) {
//				FacesMessages.instance().add(
//						"No existe el archivo, consulte con el administrador. (Codigo Archivo: " + id + ")");
//			}
//		} catch (XPathExpressionException e) {
//			log.error("No existe el archivo, consulte con el administrador", e);
//		} catch (IOException e) {
//			log.error("No existe el archivo, ", e);
//		}
//	}

	@Override
	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
//		no permite modificar
//		if(this.expediente.getDestinatarioHistorico() != null){
//			if(documento.getId() == null){
//				return true;
//			}else{
//				return false;
//			}
//		}
		if (this.expediente != null && this.expediente.getId() != null) { return true; }
		return false;
	}
	
	/**
	 * Inicializa Archivos Antecedentes.
	 */
	
//	public void setArchivosAntecedentes(List<ArchivoAdjuntoDocumentoBinario> archivosAntecedentes) {
//		this.archivosAntecedentes = archivosAntecedentes;
//	}
	
//	/**
//	 * @param docBinario
//	 *            {@link DocumentoBinario}
//	 */
//	public void setAntecedentesDocumento(final DocumentoBinario docBinario) {
//		if (archivosAntecedentes != null) {
//			for (ArchivoAdjuntoDocumentoBinario a : archivosAntecedentes) {
//				a.setDocumentoBinario(docBinario);
//			}
//			docBinario.setArchivosAdjuntos(archivosAntecedentes);
//		}
//	}
//	
//	/**
//	 * Inicializa Archivos Antecedentes.
//	 */
//	private void inicializaArchivosAntecedentes() {
//		if (archivosAntecedentes == null) {
//			archivosAntecedentes = new ArrayList<ArchivoAdjuntoDocumentoBinario>();
//		}
//	}
}
