package cl.exe.exedoc.session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.repositorio.Repositorio;

// clase para probar generacion de xml
@Name("documentDisplayAction")
@Stateless
public class Doc2XmlBean implements Doc2Xml {

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager entityManager;

	public DocumentoElectronico getDocumento(Long id) {
		log.info("pidieron documento #0", id);
		DocumentoElectronico doc;
		try {
			doc = entityManager.find(DocumentoElectronico.class, id);
			doc.getFirmas().size();
			doc.getVisaciones().size();
			doc.getDistribucionDocumento().size();
			doc.getArchivosAdjuntos().size();
			doc.getParrafos().size();
			
			if (doc instanceof Providencia) {
				((Providencia) doc).getAcciones().size();
			}
		} catch (Exception ex) {
			doc = null;
		}
		
		return doc;
	}

	public byte[] getXmlData(DocumentoElectronico doc) {
		if (doc == null)
			return null;
		Repositorio repositorio = new Repositorio();
		return repositorio.transformarDocumentoElectronico(doc, false);
	}

}
