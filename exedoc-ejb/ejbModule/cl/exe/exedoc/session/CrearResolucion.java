package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.RevisarDocumentos;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Administrator
 */
@Local
public interface CrearResolucion {

	public void destroy();

	public String end();

	public String buscaDocumento();

	public void visualizar();

	public void firmar();

	public String visar();

	public String agregarDocumento();

	public void agregarAntecedente();

	public void listener(UploadEvent event) throws Exception;

	public void salirPanel();

	public void eliminarArchivoAntecedente(Integer idNuevoArchivo);

	public Resolucion getResolucion();

	public void setResolucion(Resolucion resolucion);

	public String getVistos();

	public void setVistos(String vistos);

	public String getConsiderando();

	public void setConsiderando(String considerando);

	public String getResuelvo();

	public void setResuelvo(String resuelvo);

	public List<SelectPersonas> getDistribucionDocumento();

	public void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento);

	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	public void verArchivo(Long id);

	public Boolean getFirmado();

	public Boolean getVisado();

	public String getVisaciones();

	public List<VisacionDocumento> getVisacionesV();

	public List<FirmaEstructuradaDocumento> getFirmas();

	public Integer getTipoResolucion();

	public void setTipoResolucion(Integer tipoResolucion);

	public boolean modificarVisaFirmas();

	public String despacharExpediente();

	public boolean isRenderedFirmarExento();

	public boolean isRenderedFirmarContraloria();

	public boolean isRenderedVisar();

	public boolean isDisabledGuardar();

	public boolean isRenderedBotonDespachar();

	public boolean isDespachable();

	public void guardarYCrearExpediente();

	public boolean isEditable();

	public boolean isRenderedBotonGuardarCrearExpediente();

	public boolean isEditableSinVisa();

	public boolean isRenderedBotonVisar();

	public boolean isRenderedBotonGuardar();

	public boolean isRenderedBotonVisualizar();

	public String getUrlFirma();

	public void plantillaListener(ValueChangeEvent event);

	public List<SelectItem> obtenerPlantillas();

	public Integer getIdPlantilla();

	public void setIdPlantilla(Integer idPlantilla);

	public String cambiar();

	public Integer getIdPlantillaSelec();

	public void setIdPlantillaSelec(Integer idPlantillaSelec);

	public String getTipoArchivo();

	public void setTipoArchivo(String tipoArchivo);

	public boolean isRenderedPlantillaList();

	public String desvisarYDesfirmar(Resolucion res);

	public boolean renderedBotonDesvisarDesfirmar();

	public void tipoRazonListener(ValueChangeEvent event);

	public String getMateriaArchivo();

	public void setMateriaArchivo(String materiaArchivo);

	public void limpiarAnexos();

	public int getMaxFilesQuantity();

	String getSessionId();

	String getRevisarBorrador();

	String despacharYAprobar();

	boolean isRenderedBotonAprobarDespachar();

	String despacharYRechazar();

	boolean isRechazado();

	boolean isAprobado();

	boolean isCompletadoBorrador();

	boolean isBorrador();

	String getComentarios();

	void setComentarios(String comentarios);

	/**
	 * Metodo que valida url de imgen para mostrar FIRMADO o ANULADO.
	 * 
	 * @return {@link String}
	 */
	String imagenEstadoResolucion();

	/**
	 * Metodo que verifica el estado de anulaion de una Resolucion.
	 * 
	 * @return {@link String}
	 */
	Boolean getAnulado();
	
	/**
	 * Acepta la anulacion de una resolucion, lo cual eliminara el numero de la resolucion.
	 */
	void aceptarAnulacion();

	/**
	 * anula una resolucion.
	 */
	void anularResolucion();
	
	/**
	 * @return {@link Boolean}
	 */
	boolean mostrarBorrador();

	Long getIdDepartamento();

	void setIdDepartamento(Long idDepartamento);

	String buscaDocumento2();
	
	public String cacheRandom();
	
	public Long getRand();

	public void setRand(Long rand) ;

	Long getIdAlerta();

	void setIdAlerta(Long idAlerta);

	List<SelectItem> getNivelesUrgencia();

	void setNivelesUrgencia(List<SelectItem> nivelesUrgencia);

//	void agregarRelacionResolucion(Long id);

//	void quitarRelacionResolucion(Resolucion hija);
	
//	void agregarResolucionHijaAExpediente();

	void setObservacionRechazo(String observacionRechazo);
	
	public void limpiaObservaciones();

	Boolean getDesdeDespacho();

	void setDesdeDespacho(Boolean desdeDespacho);
	
	 boolean soyFirmante();

	List<RevisarDocumentos> obtenerComentariosBorrador();

	boolean isEditable2();

	boolean soyRevisor();

	boolean soyVisador();

	boolean isRenderedFirmar();

	boolean isBorradorRevisadoYSoyRevisor();

	boolean isRenderedBotonRechazarDespachar();

	String getObservacionRechazo();

	List<Resolucion> getHijas();

	void setHijas(List<Resolucion> hijas);

	void clearUploadData();

	boolean isDocumentoValido();

	void previsualizar();
	
}
