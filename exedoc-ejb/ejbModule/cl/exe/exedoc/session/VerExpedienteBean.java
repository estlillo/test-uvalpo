package cl.exe.exedoc.session;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.config.Roles;
import cl.exe.exedoc.config.Roles.PropiedadesRoles;
import cl.exe.exedoc.entity.AccionProvidencia;
import cl.exe.exedoc.entity.AnulacionResolucion;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DiaAdministrativo;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.HistorialDespacho;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.SolicitudFolio;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.Vacaciones;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.mantenedores.AdministradorGrupoUsuario;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.ObservacionValidator;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;

@Stateful
@Name("verExpediente")
@Scope(ScopeType.CONVERSATION)
public class VerExpedienteBean implements VerExpediente {

	@Logger
	private Log log;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private RepositorioLocal repositorio;
	
	@EJB
	private AdministradorGrupoUsuario adminGrupos;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	// Esto viene de la solicitud de dia administrativo y de vacaciones
	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Persona
	 * solicitante;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private String
	 * grado;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Double
	 * diasAdministrativosPendientes;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Double
	 * diasVacacionesPendientes;
	 * 
	 * @In(required = false, value = "calidadJuridica", scope =
	 * ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, value = "calidadJuridica", scope =
	 * ScopeType.CONVERSATION) private String calidadJuridica;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private String
	 * datosObtenidos;
	 */

	private static final String SELECCIONAR_INICIO = JerarquiasLocal.TEXTO_INICIAL;
	private List<SelectItem> listGrupos = new ArrayList<SelectItem>();
	private List<SelectItem> fiscalias = new ArrayList<SelectItem>();

	private List<GrupoUsuario> listGrupoUsuarios = new ArrayList<GrupoUsuario>();
	
	private Long fiscalia = JerarquiasLocal.INICIO;
	private Long grupoUsuario = JerarquiasLocal.INICIO;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	
	@Out(required = true)
	private String homeCrear;

	@RequestParameter
	private Long idExpediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private
	 * List<Documento> listDocumentosAnexo;
	 */

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> destinatariosDocumentoOriginal;

	// Datos Observaciones
	private String observacion;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	// private List<Observacion> observacionesEliminadas;

	// Datos Destinatario Expediente
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long organizacion = JerarquiasLocal.INICIO;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long division = JerarquiasLocal.INICIO;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long departamento = JerarquiasLocal.INICIO;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long cargo = JerarquiasLocal.INICIO;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listOrganizacion;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listDivision;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listDepartamento;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listUnidadesOrganizacionales;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listCargos;
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listPersonas;

	private List<Long> idExpedientes;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	Documento documento;

	private Boolean firmado;
	private Boolean visado;

	private boolean guardado;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento consulta;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento respuesta;

	private ArchivoDocumentoBinario archivo;
	private String materiaArchivo;

	private ObservacionArchivo archivoObs;

	// Documentos Respuesta
	private Integer tipoDocumentoAdjuntar;
	private List<SelectItem> listaDocumentosAdjuntar;
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;

	private int contadorObservaciones;
	private int contadorDocumentos;

	private int copiaDirectoFlag;
	private Integer copiaDirecto;
	private Boolean flagFoliador = false;

	private Long codigoBarra;

	private List<DestinatariosFrecuentes> listListas;

	// Grupo Destinatarios Frecuentes
	private Long grupo;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listDestFrec;

	private String visualizar;
	private String verArchivoTitulo;
	private String verArchivoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;
	
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<DetalleDias> diasSolicitados;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false,scope = ScopeType.CONVERSATION)
//	private InformacionPersonal informacionPersonal;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private InformacionPersonalSrv informacionPersonalSrv;

	@SuppressWarnings("unchecked")
	@Begin(join = true)
	public String begin() {
		long start = System.currentTimeMillis();
		vistoDesdeReporte = null;
		listGrupoUsuarios = new ArrayList<GrupoUsuario>();
		this.buscarGrupos();
		
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		log.info("beginning conversation: verExpediente");

		visualizar = JerarquiasLocal.VISUALIZAR;
		verArchivoTitulo = JerarquiasLocal.DESCARGAR_ARCHIVO;
		verArchivoOriginal = JerarquiasLocal.VER_ARCHIVO_ORIGINAL;

		copiaDirectoFlag = 0;

		// listDocumentosAnexo = new ArrayList<Documento>();
		listDocumentosRespuesta = new ArrayList<Documento>();
		listDestinatarios = new HashSet<Persona>();
		destinatariosDocumentoOriginal = new ArrayList<SelectItem>();
		observaciones = new ArrayList<Observacion>();

		listDocumentosEliminados = new ArrayList<Documento>();

		if (listDivision == null) {
			listDestFrec = new ArrayList<SelectItem>();
			listDivision = new ArrayList<SelectItem>();
			listDepartamento = new ArrayList<SelectItem>();
			listUnidadesOrganizacionales = new ArrayList<SelectItem>();
			listCargos = new ArrayList<SelectItem>();
			listPersonas = new ArrayList<SelectItem>();
		}

		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
//		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidadesOrganizacionales();

		idExpedientes = new ArrayList<Long>();

		homeCrear = "verExpediente";

		tipoDocumentoAdjuntar = 0;
		listaDocumentosAdjuntar = new ArrayList<SelectItem>();
		listaDocumentosAdjuntar.addAll(buscarDocumentosAdjuntar());

		this.expediente = em.find(Expediente.class, idExpediente);
		
		//captura de reasignacion
		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja de Entrada");
			desdeDespacho = true;
			return "bandejaEntrada";
		}
		
		contadorObservaciones = 0;
		contadorDocumentos = 0;

		for (DocumentoExpediente de : expediente.getDocumentos()) {
			/*
			 * if (de.getTipoDocumentoExpediente().getId().equals(
			 * TipoDocumentoExpediente.ANEXO)) {
			 * de.getDocumento().setTipoDocumentoExpediente(new
			 * TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO)); if
			 * (de.getDocumento().getEliminado() == null ||
			 * !de.getDocumento().getEliminado()) {
			 * this.listDocumentosAnexo.add(de.getDocumento()); } if
			 * (de.getDocumento() instanceof DocumentoBinario) { if
			 * (de.getIdDocumentoReferencia() != null) { ((DocumentoBinario)
			 * de.getDocumento
			 * ()).setIdDocumentoReferencia(de.getIdDocumentoReferencia
			 * ().intValue()); } } } else
			 */if (!de.getTipoDocumentoExpediente().getId()
					.equals(TipoDocumentoExpediente.ORIGINAL)) {
				de.getDocumento().setTipoDocumentoExpediente(
						new TipoDocumentoExpediente(
								de.getTipoDocumentoExpediente().getId()));
				if (de.getDocumento().getEliminado() == null
						|| !de.getDocumento().getEliminado()) {
					this.listDocumentosRespuesta.add(de.getDocumento());
				}
//				if (((de.getDocumento().getAutor().getId().equals(usuario
//						.getId())) || (de.getDocumento().getIdEmisor() != null && de
//						.getDocumento().getIdEmisor().equals(usuario.getId())))
//						&& (de.getDocumento().getFirmas() == null || (de
//								.getDocumento().getFirmas() != null && de
//								.getDocumento().getFirmas().size() == 0))) {
//					de.getDocumento().setEliminable(true);
//				} else {
//					de.getDocumento().setEliminable(false);
//				}
				de.getDocumento().setEliminable(md.isEliminable(de.getDocumento(), expediente, usuario));
			} else if (de.getTipoDocumentoExpediente().getId()
					.equals(TipoDocumentoExpediente.ORIGINAL)) {
				this.documentoOriginal = de.getDocumento();
				this.documentoOriginal
						.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
								TipoDocumentoExpediente.ORIGINAL));
				//documentoOriginal = em.find(Documento.class, documentoOriginal.getId());
				if (this.documentoOriginal instanceof DocumentoElectronico) {
					((DocumentoElectronico) documentoOriginal).getParrafos()
							.size();
					if (this.documentoOriginal instanceof Providencia) {
						((Providencia) documentoOriginal).getAcciones().size();
					}
					if (this.documentoOriginal instanceof Resolucion) {
						((Resolucion) documentoOriginal).getArchivosAdjuntos()
								.size();
					}
					if (this.documentoOriginal instanceof Decreto) {
						((Decreto) documentoOriginal).getArchivosAdjuntos()
								.size();
					}
					// if (this.documentoOriginal instanceof DiaAdministrativo)
					// {
					// ((DiaAdministrativo)
					// documentoOriginal).getListDetalleDias().size();
					// }
					// if
					// (getDocumentoOriginal().getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
					// ||
					// getDocumentoOriginal().getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES))
					// {
					// for (SelectItem doc : listaDocumentosAdjuntar) {
					// if (doc.getValue().equals(TipoDocumento.RESOLUCION)) {
					// listaDocumentosAdjuntar.remove(doc);
					// break;
					// }
					// if (doc.getValue().equals(TipoDocumento.DECRETO)) {
					// listaDocumentosAdjuntar.remove(doc);
					// break;
					// }
					// }
					// }
				}
				documentoOriginal.getDistribucionDocumento().size();
				documentoOriginal.getVisaciones().size();
				documentoOriginal.getFirmas().size();
				documentoOriginal.getFirmasEstructuradas().size();
				documentoOriginal.getVisacionesEstructuradas().size();
				documentoOriginal.getBitacoras().size();
			}
			if (de.getEnEdicion()) {
				de.getDocumento().setEnEdicion(true);
			} else {
				de.getDocumento().setEnEdicion(false);
			}
			de.getDocumento().setIdNuevoDocumento(contadorDocumentos++);
			de.getDocumento().getFirmas().size();
			de.getDocumento().getVisaciones().size();
			de.getDocumento().getFirmasEstructuradas().size();
			de.getDocumento().getVisacionesEstructuradas().size();
			de.getDocumento().getBitacoras().size();
		}
		/*if (this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION)) {

			if (this.documentoOriginal.getRevisarEstructuradas() != null && this.documentoOriginal.getRevisarEstructuradas().size() > 0	&& !this.documentoOriginal.getRevisarEstructuradas().get(0).getPersona().getId().equals(this.usuario.getId())) {

				this.listDestinatarios.add(this.documentoOriginal.getRevisarEstructuradas().get(0).getPersona());
				
			} else if (this.documentoOriginal.getVisacionesEstructuradas() != null 	&& this.documentoOriginal.getVisacionesEstructuradas().size() > 0 && this.documentoOriginal.getEstado().getId().equals(EstadoDocumento.BORRADOR_APROBADO)) {

				if (this.documentoOriginal.getVisaciones() == null	|| this.documentoOriginal.getVisaciones().size() == 0) {

					
					for (VisacionEstructuradaDocumento visacion : this.documentoOriginal.getVisacionesEstructuradas()) {
						if(!visacion.getPersona().equals(usuario)){
							this.listDestinatarios.add(visacion.getPersona());
							break;
						}
					}
				}
			} else if (this.documentoOriginal.getFirmasEstructuradas() != null	&& this.documentoOriginal.getFirmasEstructuradas().size() > 0 && this.documentoOriginal.getEstado().getId().equals(EstadoDocumento.VISADO)) { // EstadoDocumento.BORRADOR_APROBADO
				if (this.documentoOriginal.getFirmas() == null || this.documentoOriginal.getFirmas().size() == 0) {

					this.listDestinatarios.add(this.documentoOriginal.getFirmasEstructuradas().get(0).getPersona());
				}
			}
		}*/

		this.buscarDestinatarios();
		// this.relacionarAnexoConDocumento();

		observaciones.addAll(expediente.getObservaciones());

		java.util.Collections.sort(observaciones);

		this.setDestinatarioDocumentoOriginal();

		listListas = this.cargarListasExistentes();
		listDestFrec = this.buscarDestFrec();
		//informacionPersonal = new InformacionPersonal();
		
		try {
			final Object doc = this.getListDocumentosRespuesta().get(
					this.getListDocumentosRespuesta().size() - 1);

			if (doc instanceof SolicitudFolio) {
				final SolicitudFolio docSol = (SolicitudFolio) doc;
				final List<Rol> roles = this.usuario.getRoles();
				final PropiedadesRoles r = Roles.PropiedadesRoles.ROL_FOLIADOR;
				final String rol = r.name();
				for (int i = 0; i < roles.size(); i++) {
					final Rol array_element = roles.get(i);
					if (rol.equalsIgnoreCase(array_element.getDescripcion())) {
						flagFoliador = true;
					}
				}
				if (flagFoliador) {
					final Persona dest = docSol.getSolicitante();
					this.listDestinatarios.add(dest);
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			flagFoliador = false;

		}
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(),
				"Recupera Expediente Bandeja de Entrada", System.currentTimeMillis() - start);

		return "verExpediente";
	}

	@Override
	public boolean renderedDestintarioExpediente() {

		return !this.documentoOriginal.getTipoDocumento().getId()
			.equals(TipoDocumento.RESOLUCION);
	}

	@SuppressWarnings("unchecked")
	private List<DestinatariosFrecuentes> cargarListasExistentes() {
		final StringBuffer misListasJPQL = new StringBuffer(
				"SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
		misListasJPQL
				.append("ldf.propietario.id = ? and (ldf.esGrupo is null or ldf.esGrupo = false)");
		final Query misListasQuery = em.createQuery(misListasJPQL.toString());
		misListasQuery.setParameter(1, usuario.getId());
		final List<DestinatariosFrecuentes> misListas = misListasQuery
				.getResultList();

		return misListas;
	}

	@Override
	public void agregarListaDestinatarios(final Long idLista,
			final Boolean tipoEnvio) {
		final DestinatariosFrecuentes lista = em.find(
				DestinatariosFrecuentes.class, idLista);
		final List<Persona> destinatariosDesdeLista = lista
				.getDestinatariosFrecuentes();
		Boolean sw = new Boolean(Boolean.FALSE);
		final StringBuilder nombre = new StringBuilder();
		for (Persona destinatario : destinatariosDesdeLista) {
			if (!listDestinatarios.contains(destinatario)) {
				destinatario.setDestinatarioConCopia(tipoEnvio);
				destinatario.setBorrable(true);
				listDestinatarios.add(destinatario);
			} else {
				if (nombre.length() > 0) {
					nombre.append(", \n");
				}
				nombre.append(MessageFormat.format(" {0} {1}",
						destinatario.getNombres(),
						destinatario.getApellidoPaterno()));
				sw = Boolean.TRUE;

			}
		}
		if (sw) {
			final String msg = MessageFormat
					.format("La lista {0}, ya esta agregada a la lista de destinatarios frecuentes. \n{1}",
							lista.getNombreLista(), nombre.toString());
			FacesMessages.instance().add(msg);
		}
	}

	private List<SelectItem> buscarDestFrec() {
		final DestinatariosFrecuentes grupoDestFrec = obtenerGrupo();
		if (grupoDestFrec != null) {
			final List<Persona> destinatariosFrecuentes = grupoDestFrec
					.getDestinatariosFrecuentes();
			final List<SelectItem> listDestinatariosFrecuentes = new ArrayList<SelectItem>();
			listDestinatariosFrecuentes.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			for (Persona p : destinatariosFrecuentes) {
				listDestinatariosFrecuentes.add(new SelectItem(p.getId(), p
						.getNombreApellido()));
			}
			return listDestinatariosFrecuentes;
		} else {
			return new ArrayList<SelectItem>();
		}
	}

	private DestinatariosFrecuentes obtenerGrupo() {
		DestinatariosFrecuentes miGrupo;
		try {
			final StringBuffer miGrupoJPQL = new StringBuffer(
					"SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
			miGrupoJPQL.append("ldf.propietario.id = ? and ldf.esGrupo = true");
			final Query miGrupoQuery = em.createQuery(miGrupoJPQL.toString());
			miGrupoQuery.setParameter(1, usuario.getId());
			miGrupo = (DestinatariosFrecuentes) miGrupoQuery.getSingleResult();
		} catch (NoResultException nre) {
			miGrupo = null;
		}

		return miGrupo;
	}

	public boolean renderedVisualizarExpediente() {
		boolean valido = false;
		if (this.documentoOriginal != null && 
				this.documentoOriginal.getFormatoDocumento() != null &&
				this.documentoOriginal.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO)) {
			if (documentoOriginal instanceof Carta || 
					documentoOriginal instanceof Memorandum ||
					documentoOriginal instanceof Oficio ||
					documentoOriginal instanceof Resolucion){
				valido = true;
			}
		}
		return valido;
	}

	/*
	 * private void relacionarAnexoConDocumento() { for (Documento doc :
	 * listDocumentosAnexo) { if (doc instanceof DocumentoBinario) {
	 * DocumentoBinario docBin = (DocumentoBinario) doc; if
	 * (docBin.getIdDocumentoReferencia() != null) { for (Documento docResp :
	 * listDocumentosRespuesta) { if (docResp.getId().equals(new
	 * Long(docBin.getIdDocumentoReferencia()))) {
	 * docBin.setIdDocumentoReferencia(docResp.getIdNuevoDocumento()); break; }
	 * } if (documentoOriginal.getId().equals(new
	 * Long(docBin.getIdDocumentoReferencia()))) {
	 * docBin.setIdDocumentoReferencia(documentoOriginal.getIdNuevoDocumento());
	 * } } } } }
	 */

	private void setDestinatarioDocumentoOriginal() {
		if (destinatariosDocumentoOriginal == null) {
			destinatariosDocumentoOriginal = new ArrayList<SelectItem>();
		} else {
			destinatariosDocumentoOriginal.clear();
		}
		if (this.documentoOriginal.getTipoDocumento() != null && (
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
		// ||
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES)
		// ||
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADQUISICION)
		// ||
				this.documentoOriginal.getTipoDocumento().getId()
						.equals(TipoDocumento.RESOLUCION))) {
			if (this.expediente.getDestinatario() != null) {
				destinatariosDocumentoOriginal.add(new SelectItem(
						destinatariosDocumentoOriginal.size(), this.expediente
								.getDestinatario().getNombreApellido()));
			} else {
				if (this.expediente.getDestinatarioHistorico() != null) {
					destinatariosDocumentoOriginal.add(new SelectItem(
							destinatariosDocumentoOriginal.size(),
							this.expediente.getDestinatarioHistorico()
									.getNombreApellido()));
				}
			}
		} else {
			for (ListaPersonasDocumento dd : this.documentoOriginal
					.getDestinatarios()) {
				if (dd instanceof DestinatarioDocumento) {
					destinatariosDocumentoOriginal.add(new SelectItem(
							destinatariosDocumentoOriginal.size(), dd
									.getDestinatario()));
				}
			}
		}
	}
	
	public void guardarHistorialDespacho(Expediente e){
		HistorialDespacho guardar = new HistorialDespacho();
		guardar.setExpediente(e);
		guardar.setEmisor(e.getEmisor());
		guardar.setDestinatario(e.getDestinatario());
		guardar.setFechaDespacho(e.getFechaDespacho());
		//adminDpacho.guardarHistorial(guardar);
		em.persist(guardar);
		
	}
	public String despacharExpediente() throws DocumentNotUploadedException {
		long start = System.currentTimeMillis();

		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja de Entrada");
			desdeDespacho = true;
			return "bandejaEntrada";
		}
		String rutaDevolucion = "";

//		if (archivo != null && archivo.getArchivo() == null) {
//			FacesMessages.instance().add("No hay archivo binario");
//			rutaDevolucion = "";
//		}
		if (listDestinatarios.size() > 0 || listGrupoUsuarios.size() > 0) {
			me.borrarExpedienteDeCache(this.expediente);
			this.guardarExpediente(true, false);
			//final List<Expediente> expedientes = new ArrayList<Expediente>();
			me.despacharExpediente(expediente);
			for (Long id : idExpedientes) {
				Expediente e = null;
				e = me.buscarExpediente(id);
				log.info("Expediente despachando: " + e.getId() + " "
						+ e.getNumeroExpediente());
				if (e != null) {
					//em.merge(e);
					me.despacharExpediente(e);
					guardarHistorialDespacho(e);
					//expedientes.add(e);
				}
			}
			// TODO DEPRECATED TRACK!!!
			//me.despacharNotificacionPorEmail(expedientes);
			observaciones = null;
			desdeDespacho = true;
			rutaDevolucion = "bandejaSalida";
			
			LogTiempos.mostrarLog(this.usuario.getUsuario(),
					"Despachar Expediente", System.currentTimeMillis() - start);
			
		} else {
			FacesMessages.instance().add(
					"Debe Seleccionar un Destinatario del Expediente");
			rutaDevolucion = "";
		}
		desdeDespacho = true;
		return rutaDevolucion;
	}

	public void registrarExpediente() throws DocumentNotUploadedException {
		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja de Entrada");
			desdeDespacho = true;
			return;
		}
		this.guardarExpediente(false, false);
	}

	private void registrarExpedienteConArchivado() {

	}
	/**
	 * @param despachar
	 *            {@link Boolean}
	 * @param archivar
	 *            {@link Boolean}
	 * @throws DocumentNotUploadedException 
	 */
	
	private void guardarExpediente(final boolean despachar,
			final boolean archivar) throws DocumentNotUploadedException {
		expediente.setObservaciones(null);
		me.modificarExpediente(expediente);
		for (Documento doc : listDocumentosEliminados) {
			md.eliminarDocumento(doc.getId(), false);
		}
		//expediente = em.find(Expediente.class, expediente.getId());
		List<Expediente> expedientes = me.buscarExpedientesHijos(this.expediente);
		if (expedientes != null) {
			expedientes.size();
			Iterator<Expediente> itExpedientes = expedientes.iterator();
			while (itExpedientes.hasNext()) {
				Expediente i = itExpedientes.next();
				me.eliminarExpediente(i.getId());
			}
		}

		this.guardarObservaciones();

		final Date fechaIngreso = new Date();
		idExpedientes.clear();
		for (Persona de : listDestinatarios) {
				idExpedientes.add(me.agregarDestinatarioAExpediente(expediente,
						usuario, de, fechaIngreso));
		}
		
		Iterator<GrupoUsuario> ig = listGrupoUsuarios.iterator();
		while (ig.hasNext()) {
			GrupoUsuario grupo = ig.next();
			idExpedientes.add(me.agregarDestinatarioAExpediente(
					expediente, usuario, grupo, fechaIngreso));
		}
//		for (GrupoUsuario grupo : listGrupoUsuarios) {
//				idExpedientes.add(me.agregarDestinatarioAExpediente(
//						expediente, usuario, grupo, fechaIngreso));
//		}
		//Generamos expedientes asociados a solicitudes de respuesta
		/*for (SolicitudDeRespuesta de : expediente.getSolicitudesDeRespuesta()) {
			try {
				Long idExpHijo = me.agregarDestinatarioAExpediente(expediente,
						usuario, de.getDestinatario(), fechaIngreso);
				idExpedientes.add(idExpHijo);
				de.setExpedienteSolicitud(em.find(Expediente.class, idExpHijo));
				em.merge(de);
			}catch (DocumentNotUploadedException e) {
				log.error("DocumentNotUploadedException", e);
			}
		}*/
		if (!despachar && !archivar) {
			FacesMessages.instance().add("Expediente Guardado");
		} else if (despachar) {
			FacesMessages.instance().add("Expediente Despachado");
		}
	}

	public void agregarObservacion() {
		observacion = observacion.trim();
		if (!observacion.equals("")) {
			final Observacion obs = new Observacion();
			if (observacion.length() > ObservacionValidator.LARGO_MAXIMO) {
				obs.setObservacion(observacion.substring(0,
						ObservacionValidator.LARGO_MAXIMO));
			} else {
				obs.setObservacion(observacion);
			}
			if (!observaciones.contains(obs)) {
				final Date hoy = new Date();
				obs.setExpediente(this.expediente);
				obs.setAutor(usuario);
				obs.setFecha(hoy);
				obs.setIdNuevaObservacion(contadorObservaciones++);
				if (archivoObs != null) {
					archivoObs.setFecha(hoy);
					archivoObs.setIdNuevoArchivo(contadorObservaciones++);
					obs.setObservacionArchivo(archivoObs);
				}
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				this.observaciones.add(obs);
			}
			this.observacion = null;
			this.archivoObs = null;
			java.util.Collections.sort(observaciones);
		} else {
			FacesMessages.instance().add("El campo de observaciones esta vacio");
		}
	}

	private void guardarObservaciones() {

		for (Observacion obs : observaciones) {
			if (obs.getId() == null) {
				em.persist(obs);

				final ObservacionArchivo oa = obs.getObservacionArchivo();
				if (oa != null && oa.getNombreArchivo() != null) {
					if (oa.getId() == null)
						em.persist(oa);
					final String nombreArchivoObservacion = getNombreArchivo(
							oa.getNombreArchivo(), oa.getId().toString());
//					final String cmsId = repositorio.almacenarArchivo(
//							nombreArchivoObservacion, oa.getArchivo(),
//							oa.getContentType());
					Documento d = new Documento();
					d.setAutor(usuario);
					d.setFormatoDocumento(new FormatoDocumento(0));
					String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
					obs.getObservacionArchivo().setCmsId(cmsId);
					em.merge(oa);
				}
			}
			em.merge(obs);
		}
		expediente.setObservaciones(observaciones);
	}

	private String getNombreArchivo(String nombreArchivo, final String id) {
		String extension = "";
		final StringTokenizer st = new StringTokenizer(nombreArchivo, ".");
		while (st.hasMoreElements()) {
			extension = st.nextToken();
		}
		nombreArchivo = id + "." + extension.toLowerCase();
		return nombreArchivo;
	}

	public void listenerObs(final UploadEvent event) throws Exception {
		archivoObs = new ObservacionArchivo();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivoObs.setNombreArchivo(fileName);
		archivoObs.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivoObs.setArchivo(data);
	}

	public void verArchivoObservacion(final Archivo a) {
		// buscarArchivoRepositorio(a);
		this.verArchivo(a.getId());
	}

	public String archivarExpediente() {
		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja de Entrada");
			desdeDespacho = true;
			return "bandejaEntrada";
		}
		
		long start = System.currentTimeMillis();
		
		this.registrarExpedienteConArchivado();

		me.archivarExpediente(expediente);
		FacesMessages.instance().add("Expediente Archivado");
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(),
				"Archivar Expediente", System.currentTimeMillis() - start);
		desdeDespacho = true;
		return "bandejaEntrada";
	}

	@Override
	public String cancelarExpediente() throws DocumentNotUploadedException {
		
		long start = System.currentTimeMillis();

		this.guardarExpediente(false, true);

		me.cancelarExpediente(expediente);
		FacesMessages.instance().add("Expediente Cancelado");
		
		LogTiempos.mostrarLog(this.usuario.getUsuario(),
				"Anular Expediente", System.currentTimeMillis() - start);
		desdeDespacho = true;
		return "bandejaEntrada";
	}

	public String end() {
		desdeDespacho = true;
		final String rutaDevolucion = "bandejaEntrada";
		log.info("ending conversation: verExpediente");
		observaciones = null;
		guardado = false;
		listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		
		return rutaDevolucion;
	}

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@Begin(join = true)
	public String adjuntarDocumento(final int tipoAdjuntar) {
		destinatariosDocumento = null;
		distribucionDocumento = null;

		final int tipo = tipoDocumentoAdjuntar.intValue();
		tipoDocumentoAdjuntar = 0;
		String homeDistribucion = "";
		if (tipo == JerarquiasLocal.INICIO.intValue()) {
			return "";
		}

		switch (tipo) {
		case 1:
			documento = new Oficio();
			homeDistribucion = "crearOficioOrdinario";
			break;
		case 2:
			documento = new Oficio();
			homeDistribucion = "crearOficioCircular";
			break;
		case 3:
			documento = new Oficio();
			homeDistribucion = "crearOficioReservado";
			break;
		case 6:
			documento = new Resolucion();
			final Vistos parrafo1 = new Vistos();
			parrafo1.setCuerpo("");
			parrafo1.setDocumento((Resolucion) documento);
			parrafo1.setNumero(1L);
			final Considerandos parrafo2 = new Considerandos();
			parrafo2.setCuerpo("");
			parrafo2.setDocumento((Resolucion) documento);
			parrafo2.setNumero(1L);
			final Resuelvo parrafo3 = new Resuelvo();
			parrafo3.setCuerpo("");
			parrafo3.setDocumento((Resolucion) documento);
			parrafo3.setNumero(1L);
			final List<Parrafo> parrafos1 = new ArrayList<Parrafo>();
			parrafos1.add(parrafo1);
			final List<Parrafo> parrafos2 = new ArrayList<Parrafo>();
			parrafos2.add(parrafo2);
			final List<Parrafo> parrafos3 = new ArrayList<Parrafo>();
			parrafos3.add(parrafo3);
			((Resolucion) documento).setVistos(parrafos1);
			((Resolucion) documento).setConsiderandos(parrafos2);
			((Resolucion) documento).setResuelvo(parrafos3);
			homeDistribucion = "crearResolucion";
			break;
		case 7:
			documento = new Memorandum();
			homeDistribucion = "crearMemorandum";
			break;
		case 8:
			documento = new Providencia();
			final List<AccionProvidencia> acciones = new ArrayList<AccionProvidencia>();
			acciones.add(new AccionProvidencia(-1));
			((Providencia) documento).setAcciones(acciones);
			homeDistribucion = "crearProvidencia";
			break;
		case 9:
			documento = new Decreto();
			final Vistos parrafo1Decreto = new Vistos();
			parrafo1Decreto.setCuerpo("");
			parrafo1Decreto.setDocumento((Decreto) documento);
			parrafo1Decreto.setNumero(1L);
			final Considerandos parrafo2Decreto = new Considerandos();
			parrafo2Decreto.setCuerpo("");
			parrafo2Decreto.setDocumento((Decreto) documento);
			parrafo2Decreto.setNumero(1L);
			final Resuelvo parrafo3Decreto = new Resuelvo();
			parrafo3Decreto.setCuerpo("");
			parrafo3Decreto.setDocumento((Decreto) documento);
			parrafo3Decreto.setNumero(1L);
			final List<Parrafo> parrafos1Decreto = new ArrayList<Parrafo>();
			parrafos1Decreto.add(parrafo1Decreto);
			final List<Parrafo> parrafos2Decreto = new ArrayList<Parrafo>();
			parrafos2Decreto.add(parrafo2Decreto);
			final List<Parrafo> parrafos3Decreto = new ArrayList<Parrafo>();
			parrafos3Decreto.add(parrafo3Decreto);
			((Decreto) documento).setVistos(parrafos1Decreto);
			((Decreto) documento).setConsiderandos(parrafos2Decreto);
			((Decreto) documento).setResuelvo(parrafos3Decreto);
			homeDistribucion = "crearDecreto";
			break;
		case 10:
			documento = new Carta();
			homeDistribucion = "crearCarta";
			break;
		// case 11:
		// documento = new Factura();
		// homeDistribucion = "crearFactura";
		// break;
		 case 15:
			 documento = new DiaAdministrativo();
			 ((DiaAdministrativo)documento).setListDetalleDias(new ArrayList<DetalleDias>()); 
			 homeDistribucion = "crearSolDiaAdministrativo";
			 break;
		 case 16:
			 documento = new Vacaciones();
			 homeDistribucion = "crearSolVacaciones";
			 break;
		// case 17:
		// documento = new Adquisicion();
		// ((Adquisicion) documento).setTipoAdquisicion(new TipoAdquisicion(1));
		// ((Adquisicion) documento).setTipoFinanciamientoAdquisicion(new
		// TipoFinanciamientoAdquisicion(1));
		// ((Adquisicion) documento).setConvenioMarco(false);
		// ((Adquisicion) documento).setLicitacionPublica(false);
		// ((Adquisicion) documento).setLicitacionPrivada(true);
		// ((Adquisicion) documento).setTratoDirecto(false);
		// ((Adquisicion) documento).setAsistentesEventoAdquisicion(new
		// ArrayList<AsistenteEventoAdquisicion>());
		// homeDistribucion = "crearSolicitudAdquisicion";
		// break;
		case 22:
			documento = new Convenio();
			homeDistribucion = "crearConvenio";
			break;
		case 23:
			documento = new Contrato();
			homeDistribucion = "crearContrato";
			break;
		// case 24:
		// documento = new ActaAdjudicacion();
		// ((ActaAdjudicacion)documento).setComisionActaAdjudicacion(new
		// ArrayList<ComisionActaAdjudicacion>());
		// homeDistribucion = "crearActaAdjudicacion";
		// break;
		/*
		 * case 58: documento = new DocumentoPapel(); homeDistribucion =
		 * "crearDocumentoPapel"; break;
		 */
		case 98:
			documento = new AnulacionResolucion();
			homeDistribucion = CrearAnulacionResolucionBean.NAME;
			
			break;
		case 99:
			documento = new SolicitudFolio();
			homeDistribucion = CrearSolicitudFolioBean.NAME;
			break;
		case 100:
			documento = new DocumentoPapel();
			homeDistribucion = "crearDocumentoPapel";
			break;
		case 200:
			documento = new DocumentoBinario();
			homeDistribucion = "crearDocumentoBinario";
			break;
		default:
			documento = new Documento();
			homeDistribucion = "";
			break;
		}

		final Calendar ahora = new GregorianCalendar();

		if (tipo == 11 || tipo == 15 || tipo == 16 || tipo == 17 || tipo == 100 || tipo == 200) {
			documento.setFechaDocumentoOrigen(ahora.getTime());
		}
		
		documento.setFechaCreacion(ahora.getTime());
		ahora.add(Calendar.DATE, 5);
		//documento.setPlazo(ahora.getTime());

		setEmisorActual(documento);
		documento.setIdNuevoDocumento(this.contadorDocumentos++);
		documento.setAutor(usuario);
		if (tipoAdjuntar == 1) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
					TipoDocumentoExpediente.RESPUESTA));
		}
		if (tipoAdjuntar == 3) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
					TipoDocumentoExpediente.RESPUESTA));
		}/*
		 * else if (tipoAdjuntar == 2) {
		 * documento.setTipoDocumentoExpediente(new
		 * TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO)); }
		 */
		documento.setReservado(false);
		documento.setEnEdicion(true);
		
		return homeDistribucion;
	}

	private void setEmisorActual(final Documento documento) {
		if (!(documento instanceof Providencia)) {
			this.setNombreEmisorActual(documento, this.usuario);
		} else {
			if (jerarquias.esJefe(this.usuario)) {
				this.setNombreEmisorActual(documento, this.usuario);
			} else {
				final List<Persona> jefes = jerarquias.getJefe(this.usuario);
				this.setNombreEmisorActual(documento, jefes.get(0));
			}
		}
	}

	private void setNombreEmisorActual(final Documento documento,
			final Persona persona) {
		if (persona != null) {
			String value = persona.getNombres() + " "
					+ persona.getApellidoPaterno();
			final Cargo cargo = persona.getCargo();
			if (cargo.getDescripcion().equals("Jefe Division")
					|| cargo.getDescripcion().equals("Jefe Departamento")
					|| cargo.getDescripcion().equals("Jefe Unidad")) {
				value = cargo.getDescripcion();
			} else {
				value += " - " + cargo.getDescripcion();
			}
			if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
				final UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
				value += " - " + uo.getDescripcion();
			} else if (!persona.getCargo().getUnidadOrganizacional()
					.getDepartamento().getVirtual()) {
				final Departamento depto = persona.getCargo()
						.getUnidadOrganizacional().getDepartamento();
				value += " - " + depto.getDescripcion();
			} else {
				final Division d = persona.getCargo().getUnidadOrganizacional()
						.getDepartamento().getDivision();
				value += " - " + d.getDescripcion();
			}
			documento.setEmisor(value);
			documento.setIdEmisor(persona.getId());
		}
	}

	private void getDocumentoDeLista(final List<Documento> lista,
			final Integer numDoc) {
		for (Iterator<Documento> doc = lista.iterator(); doc.hasNext();) {
			final Documento docTmp = doc.next();
			final Integer id = docTmp.getIdNuevoDocumento();
			if (id.equals(numDoc)) {
				documento = docTmp;
				break;
			}
		}
	}

	@Begin(join = true)
	public String modificarDocumento(final Integer numDoc, final Integer lista) {
		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja de Entrada");
			desdeDespacho = true;
			return "bandejaEntrada";
		}
		if (lista.equals(1)) {
			getDocumentoDeLista(listDocumentosRespuesta, numDoc);
		} /*
		 * else if (lista.equals(2)) { getDocumentoDeLista(listDocumentosAnexo,
		 * numDoc); }
		 */else if (lista.equals(3)) {
			documento = documentoOriginal;
		}

		String homeDistribucion = "";
		final TipoDocumentoExpediente t = documento
				.getTipoDocumentoExpediente();
		final Integer id = documento.getIdNuevoDocumento();
		//final boolean enEdicion = documento.getEnEdicion(); //viene en null
		Boolean enEdicion = Boolean.FALSE;
		if (expediente.getFechaDespacho() == null && documento.getAutor().equals(usuario))
			enEdicion = Boolean.TRUE;
		if (documento != null && documento.getId() != null)
			documento = em.find(Documento.class, documento.getId());

		if (documento instanceof DocumentoElectronico) {
			if (documento.getId() != null) {
				try {
					documento = em.find(Documento.class, documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento).getArchivosAdjuntos()
							.size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
					((DocumentoElectronico) documento).getRevisar().size();
//					if (documento instanceof Resolucion) {
//						((Resolucion) documento).getResoluciones().size();
//					}
					// if (documento instanceof DiaAdministrativo) {
					// ((DiaAdministrativo)
					// documento).getListDetalleDias().size();
					// }
				} catch (Exception e) {
					documento = em.find(DocumentoElectronico.class,
							documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
					((DocumentoElectronico) documento).getRevisar().size();
//					if (documento instanceof Resolucion) {
//						((Resolucion) documento).getResoluciones().size();
//					}
					// if (documento instanceof DiaAdministrativo) {
					// ((DiaAdministrativo)
					// documento).getListDetalleDias().size();
					// }
				}
			}

			switch (documento.getTipoDocumento().getId()) {
			case 1:
				homeDistribucion = "crearOficioOrdinario";
				break;
			case 2:
				homeDistribucion = "crearOficioCircular";
				break;
			case 3:
				homeDistribucion = "crearOficioReservado";
				break;
			case 6:
				homeDistribucion = "crearResolucion";
				break;
			case 7:
				homeDistribucion = "crearMemorandum";
				break;
			case 8:
				if (documento.getId() != null) {
					try {
						((Providencia) documento).getAcciones().size();
					} catch (Exception e) {
						documento = em.find(Providencia.class,
								documento.getId());
						((Providencia) documento).getAcciones().size();
					}
				}
				homeDistribucion = "crearProvidencia";
				break;
			case 9:
				homeDistribucion = "crearDecreto";
				break;
			case 10:
				homeDistribucion = "crearCarta";
				break;
			case 15:
//				solicitante = null;
//				calidadJuridica = "";
//				grado = "";
//				diasAdministrativosPendientes = null;
//				datosObtenidos = null;
				//diasSolicitados = null;
				if (((DiaAdministrativo)documento).getListDetalleDias() != null)
					((DiaAdministrativo)documento).getListDetalleDias().size();
				else
					((DiaAdministrativo)documento).setListDetalleDias(new ArrayList<DetalleDias>());
				homeDistribucion = "crearSolDiaAdministrativo";
				break;
			case 16:
//				solicitante = null;
//				calidadJuridica = "";
//				grado = "";
//				diasVacacionesPendientes = null;
//				datosObtenidos = null;
				if (((Vacaciones)documento).getSolicitudVacaciones() != null)
					 if (((Vacaciones)documento).getSolicitudVacaciones().getListDetalleDiasVacaciones() != null)
						 ((Vacaciones)documento).getSolicitudVacaciones().getListDetalleDiasVacaciones().size();
				homeDistribucion = "crearSolVacaciones";
				break;
			// case 17:
			// ((Adquisicion) documento).getCriteriosEvaluacion().size();
			// ((Adquisicion)
			// documento).getAsistentesEventoAdquisicion().size();
			// homeDistribucion = "crearSolicitudAdquisicion";
			// break;
			case 20:
				consulta = documento;
				homeDistribucion = "verDocumentoConsulta";
				break;
			case 21:
				respuesta = documento;
				respuesta.getFirmas().size();
				homeDistribucion = "crearRespuestaDocumentoConsulta";
				break;
			case 22:
				homeDistribucion = "crearConvenio";
				break;
			case 23:
				homeDistribucion = "crearContrato";
				break;
			case 98:
				homeDistribucion = CrearAnulacionResolucionBean.NAME;
				break;
			case 99:
				homeDistribucion = CrearSolicitudFolioBean.NAME;
				break;
			// case 24:
			// ((ActaAdjudicacion)documento).getComisionActaAdjudicacion().size();
			// homeDistribucion = "crearActaAdjudicacion";
			// break;
			default:
				break;
			}
			// Pasar flujo a documento.
			// TODO: El try - catch es provisorio, pues aun no se ha
			// implementado completamente
			// el tema de los flujos estructurados.
			// try {
			// flujo = FlujoUtils.getFlujoParaDocumento(documento.getId(),
			// usuario.getId(), em);
			// } catch (NoResultException e) {
			// log.info("Documento sin flujo estructurado.");
			// }
		} else {
			if (documento instanceof DocumentoPapel) {
				homeDistribucion = "crearDocumentoPapel";
				// } else if (documento instanceof Factura) {
				// boolean debeIngresarPago = false;
				// boolean debeAprobarPago = false;
				// Persona u = em.find(Persona.class, usuario.getId());
				// for (Rol rol : u.getRoles()) {
				// if (rol.getId().equals(Rol.GESTIONAR_PAGO)) {
				// debeIngresarPago = true;
				// }
				// if (rol.getId().equals(Rol.APROBAR_PAGO)) {
				// Factura factura = (Factura) documento;
				// if (factura.getEditado()) {
				// debeAprobarPago = true;
				// }
				// }
				// }
				//
				// // el orden de estos if es importante
				// homeDistribucion = "visualizarFactura";
				// if (debeIngresarPago)
				// homeDistribucion = "ingresarDatosFactura";
				// if (debeAprobarPago)
				// homeDistribucion = "visualizarDataDeFactura";

			} else if (documento instanceof DocumentoBinario) {
				try {
					((DocumentoBinario) documento).getArchivosAdjuntos().size();
				} catch (Exception e) {
					documento = em.find(DocumentoBinario.class,
							documento.getId());
					((DocumentoBinario) documento).getArchivosAdjuntos().size();
				}
				homeDistribucion = "crearDocumentoBinario";
			} else {
				homeDistribucion = "";
			}
		}

		vistoDesdeReporte = null;
		if (lista.equals(3)) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
					TipoDocumentoExpediente.ORIGINAL));
		} else {
			documento.setTipoDocumentoExpediente(t);
		}

		documento.setIdNuevoDocumento(id);
		documento.setEnEdicion(enEdicion);
		destinatariosDocumento = new ArrayList<SelectPersonas>();
		for (ListaPersonasDocumento lpd : this.documento.getDestinatarios()) {
			destinatariosDocumento.add(new SelectPersonas(
					lpd.getDestinatario(), lpd.getDestinatarioPersona()));
		}
		
		
		distribucionDocumento = new ArrayList<SelectPersonas>();
		for (ListaPersonasDocumento lpd : this.documento.getDistribucionDocumento()) {
			SelectPersonas pers = new SelectPersonas(lpd.getDestinatario(),lpd.getDestinatarioPersona());
			if(!distribucionDocumento.contains(pers)){
				System.out.println("persona: "+pers.toString());
				distribucionDocumento.add(pers);
			}
		}

		return homeDistribucion;
	}

	public void listener(final UploadEvent event) throws Exception {
		archivo = new ArchivoDocumentoBinario();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
	}

	// TODO corresponde a la conversion de papel => digital
	/*
	 * public void agregarAntecedenteArchivo() { if (archivo != null) { Date hoy
	 * = new Date(); DocumentoBinario doc = new DocumentoBinario();
	 * doc.setMateria(this.materiaArchivo); doc.setAutor(usuario);
	 * doc.setFechaCreacion(hoy);
	 * doc.setIdNuevoDocumento(listDocumentosAnexo.size());
	 * doc.setEnEdicion(true); doc.setReservado(false); archivo.setFecha(hoy);
	 * archivo.setIdNuevoArchivo(listDocumentosAnexo.size());
	 * doc.setArchivo(archivo); listDocumentosAnexo.add(doc); archivo = null; }
	 * }
	 */

	public void verArchivo() {
		final Archivo archivo = ((DocumentoBinario) documentoOriginal)
				.getArchivo();
		// buscarArchivoRepositorio(archivo);
		this.verArchivo(archivo.getId());
	}

	public void verArchivo(final Long idArchivo) {
		/*
		 * ArchivoDocumentoBinario a = em.find(ArchivoDocumentoBinario.class,
		 * idArchivo); buscarArchivoRepositorio(a);
		 */
		try {
			if (!DocUtils.getFile(idArchivo, em)) {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ idArchivo + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void buscarArchivoRepositorio(final Archivo a) {
		final byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		if (data != null) {
			final FacesContext facesContext = FacesContext.getCurrentInstance();
			try {
				final HttpServletResponse response = (HttpServletResponse) facesContext
						.getExternalContext().getResponse();
				response.setContentType(a.getContentType());
				response.setHeader("Content-disposition",
						"attachment; filename=\"" + a.getNombreArchivo() + "\"");
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
				facesContext.responseComplete();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			FacesMessages.instance().add(
					"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
							+ a.getId() + ")");
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	private List<SelectItem> buscarOrganizaciones() {
		limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento departamento = departamentos.get(0);
			if (departamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	public void agregarDestinatario() {
		try {
			// por la actualizacion de ajax
			if (!cargo.equals(JerarquiasLocal.INICIO)) {
				if (persona.equals(JerarquiasLocal.TODOS)) {
					final Query query = em.createNamedQuery("Persona.findById");
					query.setParameter("id", this.cargo);
					this.listDestinatarios.addAll(query.getResultList());
				} else if (persona.equals(JerarquiasLocal.CUALQUIERA)) {
					final Query query = em
							.createQuery("select p from Persona p where p.cargo.id= ? ");
					query.setParameter(1, this.cargo);
					final List<Persona> personasListTmp = query.getResultList();
					for (Persona p : personasListTmp) {
						p.setDestinatarioConRecepcion(true);
						this.listDestinatarios.add(p);
					}
				} else {
					final Persona p = (Persona) em
							.createQuery(
									"select p from Persona p where p.id= :param")
							.setParameter("param", this.persona)
							.getSingleResult();
					this.compruebaExpedientes(p);

					if (copiaDirectoFlag == 0) {
						p.setDestinatarioConCopia(false);
					} else {
						p.setDestinatarioConCopia(true);
					}
					this.listDestinatarios.add(p);
				}
			}

			if (!grupo.equals(JerarquiasLocal.INICIO)) {
				final Persona p = em.find(Persona.class, grupo);
				this.compruebaExpedientes(p);
				if (copiaDirectoFlag == 0) {
					p.setDestinatarioConCopia(false);
				} else {
					p.setDestinatarioConCopia(true);
				}
				p.setBorrable(true);
				this.listDestinatarios.add(p);
			}
			// no se envia a el mismo el expediente.
			if (this.listDestinatarios.contains(usuario)) {
				this.listDestinatarios.remove(usuario);
			}
			listOrganizacion = this.buscarOrganizaciones();
			organizacion = (Long) listOrganizacion.get(1).getValue();
			this.buscarDivisiones();
			division = (Long) listDivision.get(1).getValue();
			this.buscarDepartamentos();
			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
			this.buscarUnidadesOrganizacionales();
			listDestFrec = this.buscarDestFrec();
		} catch (Exception e) {
			log.info("cargando datos");
		}
	}

	@SuppressWarnings("unchecked")
	private void compruebaExpedientes(final Persona p) {
		final StringBuilder existeEnBandejaQL = new StringBuilder(
				"SELECT e.copia FROM Expediente e WHERE ");
		existeEnBandejaQL.append("e.numeroExpediente = '"
				+ this.expediente.getNumeroExpediente() + "' AND ");
		existeEnBandejaQL
				.append("((e.destinatario.id = "
						+ p.getId()
						+ " AND e.fechaDespacho IS NOT NULL AND e.fechaAcuseRecibo IS NULL) ");
		existeEnBandejaQL
				.append("OR (e.emisor.id = "
						+ p.getId()
						+ " AND e.fechaDespacho IS NULL AND e.fechaAcuseRecibo IS NOT NULL)) ");
		existeEnBandejaQL
				.append("AND e.archivado IS NULL ");
		Boolean copia = null;
		try {
			// List<Boolean> copias =
			// em.createNativeQuery(existeEnBandejaQL.toString()).getResultList();
			final List<Boolean> copias = em.createQuery(
					existeEnBandejaQL.toString()).getResultList();
			if (copias != null && !copias.isEmpty()) {
				copia = copias.get(0);
			}
			if (copia != null) {
				if (!copia) {
					FacesMessages.instance().add(
							"El expediente ya se encuentra en la Bandeja de entrada Directo de: "
									+ p.getNombreApellido());
				} else {
					FacesMessages.instance().add(
							"El expediente ya se encuentra en la Bandeja de entrada Copia de: "
									+ p.getNombreApellido());
				}
			}
		} catch (NoResultException nre) {
		}
	}

	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		this.grupo = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public boolean renderedGrupoDestinatariosFrecuentes() {
		if (listDestFrec.isEmpty()
				|| listDestFrec.contains(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL))) {
			return false;
		}
		return true;
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarDocumentosAdjuntar() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		lista.addAll(TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoRespuestaList());
		return lista;
	}

	public void eliminarDocumento(final Integer numDoc) {
		int contador = 0;
		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getIdNuevoDocumento() != null
					&& doc.getIdNuevoDocumento().equals(numDoc)) {
				if (doc.getId() != null) {
					listDocumentosEliminados.add(doc);
				}
				listDocumentosRespuesta.remove(contador);
				break;
			}
			contador++;
		}
	}

	/*
	 * public void eliminarAnexoDocumento(Integer numDoc) { int contador = 0;
	 * for (Documento doc : listDocumentosAnexo) { if (doc.getIdNuevoDocumento()
	 * != null && doc.getIdNuevoDocumento().equals(numDoc)) { if (doc.getId() !=
	 * null) { listDocumentosEliminados.add(doc); }
	 * listDocumentosAnexo.remove(contador); break; } contador++; } }
	 */

	public void eliminarDestinatariosExpediente(final Persona destinatario) {
		listDestinatarios.remove(destinatario);
	}

	public void eliminarObservaciones(final Integer idNuevaObservacion) {
		for (Observacion o : observaciones) {
			final Integer id = o.getIdNuevaObservacion();
			if (id != null && id.equals(idNuevaObservacion)) {
				observaciones.remove(o);
				break;
			}
		}
	}

	public boolean esArchivable() {
		final boolean archivable = true;
		if (this.expediente.getCopia() != null && this.expediente.getCopia()) {
			return archivable;
		}

		// if
		// (this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
		// ||
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES))
		// {
		// archivable = false;
		// for (Documento doc : listDocumentosRespuesta) {
		// if (doc.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION))
		// {
		// if (doc.getFirmas() != null && !doc.getFirmas().isEmpty()) {
		// archivable = true;
		// break;
		// }
		// }
		// if (doc.getTipoDocumento().getId().equals(TipoDocumento.DECRETO)) {
		// if (doc.getFirmas() != null && !doc.getFirmas().isEmpty()) {
		// archivable = true;
		// break;
		// }
		// }
		// }
		// return archivable;
		// }
		return archivable;
	}

	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(final Expediente expediente) {
		this.expediente = expediente;
	}

	public Long getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(final Long idExpediente) {
		this.idExpediente = idExpediente;
	}

	public Object[] getListDestinatarios() {
		if (listDestinatarios != null) {
			return listDestinatarios.toArray();
		} else {
			return new HashSet<Persona>().toArray();
		}
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	/*
	 * public List<Documento> getListDocumentosAnexo() { return
	 * listDocumentosAnexo; } public void setListDocumentosAnexo(List<Documento>
	 * listDocumentosAnexo) { this.listDocumentosAnexo = listDocumentosAnexo; }
	 */

	public List<Documento> getListDocumentosRespuesta() {
		//int contadorDocumentos
		return listDocumentosRespuesta;
	}

	public void setListDocumentosRespuesta(
			final List<Documento> listDocumentosRespuesta) {
		this.listDocumentosRespuesta = listDocumentosRespuesta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(final String observacion) {
		this.observacion = observacion;
	}

	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(final List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}

	public List<SelectItem> getDestinatariosDocumentoOriginal() {
		this.setDestinatarioDocumentoOriginal();
		return destinatariosDocumentoOriginal;
	}

	public Integer getTipoDocumentoAdjuntar() {
		return tipoDocumentoAdjuntar;
	}

	public void setTipoDocumentoAdjuntar(final Integer tipoDocumentoAdjuntar) {
		this.tipoDocumentoAdjuntar = tipoDocumentoAdjuntar;
	}

	public List<SelectItem> getListaDocumentosAdjuntar() {
		return listaDocumentosAdjuntar;
	}

	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}

	public void setDocumentoOriginal(final Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(final Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public void setMateriaArchivo(final String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	public int getCopiaDirectoFlag() {
		return copiaDirectoFlag;
	}

	public void setCopiaDirectoFlag(final int copiaDirectoFlag) {
		this.copiaDirectoFlag = copiaDirectoFlag;
	}

	public List<DestinatariosFrecuentes> getListListas() {
		return listListas;
	}

	public void setListListas(final List<DestinatariosFrecuentes> listListas) {
		this.listListas = listListas;
	}

	public Long getGrupo() {
		return grupo;
	}

	public void setGrupo(final Long grupo) {
		this.grupo = grupo;
	}

	public List<SelectItem> getListDestFrec() {
		return listDestFrec;
	}

	public void setListDestFrec(final List<SelectItem> listDestFrec) {
		this.listDestFrec = listDestFrec;
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public Boolean getFirmado() {
		if (this.documentoOriginal != null
				&& this.documentoOriginal.getFirmas() != null
				&& this.documentoOriginal.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public boolean isRenderedFirmar() {
		if (documentoOriginal != null
				&& documentoOriginal.getFirmasEstructuradas() != null
				&& documentoOriginal.getFirmasEstructuradas().size() != 0) {
			final List<FirmaEstructuradaDocumento> firmas = documentoOriginal
					.getFirmasEstructuradas();
			Collections.sort(firmas);
			if (documentoOriginal.getVisacionesEstructuradas() != null
					&& documentoOriginal.getVisacionesEstructuradas().size() == 0) {
				if (firmas.get(0).getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	public String getUrlFirma() {
		final String urlDocumento = "http://"
				+ System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}

	private List<Persona> obtenerFiscal() {
		final List<Persona> listPersonasFiscales = new ArrayList<Persona>();
		// Persona fiscal = null;
		// TODO MODELO ROLES UCHILE
		/*
		 * String sqlRolFiscal =
		 * "select rp.persona.id from RolPersona rp where rp.rol.id= " +
		 * Rol.FISCAL.intValue() + " "; // List<BigInteger> listFiscales =
		 * em.createNativeQuery(sqlRolFiscal).getResultList(); List<BigInteger>
		 * listFiscales = em.createQuery(sqlRolFiscal).getResultList(); for
		 * (BigInteger f : listFiscales) { if (f != null) { fiscal =
		 * em.find(Persona.class, f.longValue());
		 * fiscal.setDestinatarioConCopia(false);
		 * listPersonasFiscales.add(fiscal); } }
		 */
		return listPersonasFiscales;
	}

	private List<Persona> obtenerDistribucionDocumento() {
		final List<Persona> listDistribucion = new ArrayList<Persona>();
		Persona dest = null;
		if (this.documentoOriginal.getDistribucionDocumento() != null) {
			for (ListaPersonasDocumento lpd : this.documentoOriginal
					.getDistribucionDocumento()) {
				dest = lpd.getDestinatarioPersona();
				dest.setDestinatarioConCopia(true);
				listDistribucion.add(dest);
			}
		}
		return listDistribucion;
	}

	private String buscarDestinatariosFirma() throws DocumentNotUploadedException {
		listDestinatarios.addAll(obtenerFiscal());
		listDestinatarios.addAll(obtenerDistribucionDocumento());

		return despacharExpediente();
	}

	public String buscaDocumento() throws DocumentNotUploadedException {
		log.info("buscando doc..." + documentoOriginal.getId());
		documentoOriginal = em.find(Documento.class, documentoOriginal.getId());

		firmado = true;
		if (documentoOriginal instanceof DocumentoElectronico) {
			((DocumentoElectronico) documentoOriginal).getParrafos().size();
		}
		documentoOriginal.getDistribucionDocumento().size();
		documentoOriginal.getVisaciones().size();
		documentoOriginal.getFirmas().size();
		documentoOriginal.getBitacoras().size();
		documentoOriginal.getVisacionesEstructuradas().size();
		documentoOriginal.getFirmasEstructuradas().size();
		if (documentoOriginal instanceof Resolucion) {
			((Resolucion) documentoOriginal).getArchivosAdjuntos().size();
		}
		if (documentoOriginal instanceof Decreto) {
			((Decreto) documentoOriginal).getArchivosAdjuntos().size();
		}
		return buscarDestinatariosFirma();
	}

	public List<FirmaEstructuradaDocumento> getFirmas() {
		if (documentoOriginal.getFirmasEstructuradas() == null) {
			documentoOriginal
					.setFirmasEstructuradas(new ArrayList<FirmaEstructuradaDocumento>());
		}
		return documentoOriginal.getFirmasEstructuradas();
	}

	public boolean isEditable() {
		if (getVisado()) {
			return false;
		}
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	public void acusarRecibo() {
		if (this.expediente != null && this.expediente.getId() != null
				&& this.expediente.getId() > 0) {
			acusarRecibo(expediente);
		}
	}

	private void acusarRecibo(final Expediente expediente) {
		if (expediente.getAcuseRecibo() == null
				|| expediente.getAcuseRecibo() == false) {
			me.acusarReciboExpediente(expediente, usuario);
		}
	}

	public Boolean getVisado() {
		if (this.documentoOriginal != null
				&& this.documentoOriginal.getVisaciones() != null
				&& this.documentoOriginal.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public String getVisaciones() {
		if (this.documentoOriginal.getVisacionesEstructuradas() == null) {
			this.documentoOriginal
					.setVisacionesEstructuradas(new ArrayList<VisacionEstructuradaDocumento>());
		}
		final List<VisacionEstructuradaDocumento> visaciones = documentoOriginal
				.getVisacionesEstructuradas();

		final StringBuilder sb = new StringBuilder();
		for (int i = visaciones.size() - 1; i >= 0; i--) {
			sb.append(visaciones.get(i).getPersona().getIniciales() + "/");
		}
		sb.append(documentoOriginal.getAutor().getIniciales().toLowerCase());

		return sb.toString();
	}

	public String visar() throws DocumentNotUploadedException {
		boolean permisoVisa = false;
		for (VisacionEstructuradaDocumento ved : documentoOriginal
				.getVisacionesEstructuradas()) {
			if (ved.getPersona().equals(usuario)) {
				permisoVisa = true;
			}
		}
		guardado = true;
		if (permisoVisa) {
			this.armaDocumento(true, false);
			VisacionEstructuradaDocumento ved = null;
			for (VisacionEstructuradaDocumento ve : documentoOriginal
					.getVisacionesEstructuradas()) {
				if (ve.getPersona().equals(usuario)) {
					ved = ve;
					break;
				}
			}

			quitarVisadorCadena(ved);
			agregarDestinatariosVisaFirma();
			FacesMessages.instance().add("Resolución visada.");

			return despacharExpediente();
		} else {
			FacesMessages.instance().add("Ud. No puede Visar la Resolución");
		}
		return "";
	}

	private void agregarDestinatariosVisaFirma() {
		if (!agregarDestinatarioVisaEstructurada()) {
			if (!agregarDestinatarioFirmaEstructurada()) {
				FacesMessages.instance().add("Fin de cadena de Visa/Firma");
			}
		}
	}

	private boolean agregarDestinatarioVisaEstructurada() {
		boolean ok = false;
		Persona dest = null;
		VisacionEstructuradaDocumento ved = null;
		if (documentoOriginal.getVisacionesEstructuradas() != null
				&& !documentoOriginal.getVisacionesEstructuradas().isEmpty()) {
			ved = documentoOriginal.getVisacionesEstructuradas().get(0);
		}
		if (ved != null && ved.getPersona() != null) {
			ok = true;
			dest = ved.getPersona();
			dest.setDestinatarioConCopia(false);
			listDestinatarios.add(ved.getPersona());
		}
		return ok;
	}

	private boolean agregarDestinatarioFirmaEstructurada() {
		boolean ok = false;
		Persona dest = null;
		FirmaEstructuradaDocumento fed = null;
		if (documentoOriginal.getFirmasEstructuradas() != null
				&& !documentoOriginal.getFirmasEstructuradas().isEmpty()) {
			fed = documentoOriginal.getFirmasEstructuradas().get(0);
		}
		if (fed != null && fed.getPersona() != null) {
			ok = true;
			dest = fed.getPersona();
			dest.setDestinatarioConCopia(false);
			listDestinatarios.add(fed.getPersona());
		}
		return ok;
	}

	private void quitarVisadorCadena(final VisacionEstructuradaDocumento ved) {
		final Query query = em
				.createQuery("DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.id = ?");
		query.setParameter(1, ved.getId());
		query.executeUpdate();
		documentoOriginal.getVisacionesEstructuradas().remove(ved);

		salida: for (DocumentoExpediente de : expediente.getDocumentos()) {
			if (de.getDocumento().getId().equals(documentoOriginal.getId())) {
				for (VisacionEstructuradaDocumento ve : de.getDocumento()
						.getVisacionesEstructuradas()) {
					if (ve.getPersona().equals(usuario)) {
						de.getDocumento().getVisacionesEstructuradas()
								.remove(ve);
						break salida;
					}
				}
			}
		}
	}

	private void armaDocumento(final boolean visar, final boolean firmar) {
		if (this.getFirmado()) {
			return;
		}

		final boolean modificarVisaFirmas = modificarVisaFirmas();
		if (modificarVisaFirmas) {
			if (this.documentoOriginal.getVisacionesEstructuradas() != null
					&& !this.documentoOriginal.getVisacionesEstructuradas()
							.isEmpty()) {
				eliminarVisasEstructuradas();
			}
			if (this.documentoOriginal.getFirmasEstructuradas() != null
					&& !this.documentoOriginal.getFirmasEstructuradas()
							.isEmpty()) {
				eliminarFirmasEstructuradas();
			}
		}

		if (!visar && !firmar) {
			if (this.documentoOriginal.getId() != null) {
				this.documentoOriginal.setEstado(em.find(EstadoDocumento.class,
						EstadoDocumento.GUARDADO));
				this.documentoOriginal.addBitacora(new Bitacora(
						EstadoDocumento.GUARDADO, this.documentoOriginal,
						this.usuario));
			} else {
				this.documentoOriginal.setEstado(em.find(EstadoDocumento.class,
						EstadoDocumento.CREADO));
				this.documentoOriginal.addBitacora(new Bitacora(
						EstadoDocumento.CREADO, this.documentoOriginal,
						this.usuario));
			}
		}
		if (firmar) {

			this.repositorio.firmar(this.documentoOriginal, usuario);
			this.documentoOriginal.addBitacora(new Bitacora(
					EstadoDocumento.FIRMADO, this.documentoOriginal,
					this.usuario));

			// Enviar mail a todos
			final List<Expediente> padre = me
					.obtienePrimerExpediene(expediente);
			final List<Persona> dest = me.buscarDestinatarios(padre);
			for (Persona expediente : dest) {
				me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
			}

		}
		if (visar) {
			this.repositorio.visar(this.documentoOriginal, usuario);
			this.documentoOriginal.addBitacora(new Bitacora(
					EstadoDocumento.VISADO, this.documentoOriginal,
					this.usuario));
		}
		persistirDocumento();

		if (modificarVisaFirmas) {
			actualizaVisaFirma();
		}
	}

	public boolean isDisabledGuardar() {
		return guardado;
	}

	public boolean modificarVisaFirmasMinisterio() {
		// TODO MODELO ROLES UCHILE
		/*
		 * for (Rol r : usuario.getRoles()) { if (r.getId().equals(Rol.MINISTRO)
		 * || r.getId().equals(Rol.ASESOR)) { return modificarVisaFirmas(); } }
		 */
		return false;
	}

	public boolean modificarVisaFirmas() {
		// TODO MODELO ROLES UCHILE
		/*
		 * for (Rol r : usuario.getRoles()) { if
		 * (r.getId().equals(Rol.INGRESAR_VISA_FIRMA)) { return true; } }
		 */
		return false;

	}

	private void eliminarVisasEstructuradas() {
		if (this.documentoOriginal.getId() != null) {
			final String sqlBorra = "DELETE FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = "
					+ documentoOriginal.getId();
			em.createQuery(sqlBorra).executeUpdate();
			// em.createNativeQuery(sqlBorra).executeUpdate();
		}
	}

	private void eliminarFirmasEstructuradas() {
		if (this.documentoOriginal.getId() != null) {
			// String sqlBorra =
			// "DELETE FROM FIRMAS_ESTRUCTURADAS WHERE id_documento = " +
			// documentoOriginal.getId();
			// em.createNativeQuery(sqlBorra).executeUpdate();
			final String sqlBorra = "DELETE FROM FirmaEstructuradaDocumento fe WHERE fe.documento.id = "
					+ documentoOriginal.getId();
			em.createNativeQuery(sqlBorra).executeUpdate();
		}
	}

	private void persistirDocumento() {
		try {
			if (this.documentoOriginal.getId() != null) {
				md.actualizarDocumento(this.documentoOriginal);

			} else if (this.expediente != null
					&& this.expediente.getId() != null) {
				me.agregarRespuestaAExpediente(expediente,
						this.documentoOriginal, true);
			}
		} catch (DocumentNotUploadedException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void actualizaVisaFirma() {
		if (this.documentoOriginal.getId() != null) {
			final String sqlVisasActuales = "SELECT ve.* FROM VisacionEstructuradaDocumento ve WHERE ve.documento.id = "
					+ documentoOriginal.getId() + " order by ve.orden asc";
			final List<VisacionEstructuradaDocumento> visas = em.createQuery(
					sqlVisasActuales).getResultList();
			this.documentoOriginal.getVisacionesEstructuradas().clear();
			this.documentoOriginal.getVisacionesEstructuradas().addAll(visas);

			final String sqlFirmasActuales = "SELECT fe.* FROM FirmaEstructuradaDocumento WHERE fe.documento.id = "
					+ documentoOriginal.getId() + " order by fe.orden asc";
			final List<FirmaEstructuradaDocumento> firmas = em.createQuery(
					sqlFirmasActuales).getResultList();
			this.documentoOriginal.getFirmasEstructuradas().clear();
			this.documentoOriginal.getFirmasEstructuradas().addAll(firmas);
		}
	}

	public boolean isEditableSinVisa() {
		if (getFirmado()) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isRenderedVisar() {
		if (this.documentoOriginal != null
				&& this.documentoOriginal.getId() != null) {
			if (documentoOriginal != null
					&& documentoOriginal.getVisacionesEstructuradas() != null
					&& documentoOriginal.getVisacionesEstructuradas().size() != 0) {
				final List<VisacionEstructuradaDocumento> visas = documentoOriginal
						.getVisacionesEstructuradas();
				Collections.sort(visas);
				if (visas.get(0).getPersona().equals(usuario)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Integer getCopiaDirecto() {
		return copiaDirecto;
	}

	@Override
	public void setCopiaDirecto(final Integer copiaDirecto) {
		this.copiaDirecto = copiaDirecto;
	}

	public boolean obtienePermisoEditFolio(final int id) {
		return true;
	}

	@Override
	public Long getCodigoBarra() {
		return codigoBarra;
	}

	@Override
	public void setCodigoBarra(final Long codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	@Override
	public void cargarCodigoBarra(final Documento doc) {
		log.info("codigo barra : {0}, {1}", doc.getCodigoBarra(), doc.getId());
		this.setCodigoBarra(doc.getCodigoBarra());
	}

	@Override
	public String getVisualizar() {
		return visualizar;
	}

	@Override
	public void setVisualizar(final String visualizar) {
		this.visualizar = visualizar;
	}

	@Override
	public Boolean getFlagFoliador() {
		return flagFoliador;
	}

	@Override
	public void setFlagFoliador(Boolean flagFoliador) {
		this.flagFoliador = flagFoliador;
	}

	@Override
	public String getVerArchivoTitulo() {
		return verArchivoTitulo;
	}

	@Override
	public void setVerArchivoTitulo(final String verArchivoTitulo) {
		this.verArchivoTitulo = verArchivoTitulo;
	}

	@Override
	public String getVerArchivoOriginal() {
		return verArchivoOriginal;
	}

	@Override
	public void setVerArchivoOriginal(final String verArchivoOriginal) {
		this.verArchivoOriginal = verArchivoOriginal;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void buscarGrupos() {
		// TODO Auto-generated method stub
		this.listGrupos.clear();
		Query query = em.createNamedQuery("GrupoUsuario.findByAll");
		SelectItem select = new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL);
		if(this.listGrupos.size() == 0){
			this.listGrupos.add(select);
		}
		List<GrupoUsuario> result = query.getResultList();
		for (GrupoUsuario grupo : result) {
		    select = new SelectItem();
			select.setValue(grupo.getId());
			select.setLabel(grupo.getNombre());
			this.listGrupos.add(select);
		}
	}
	@Override
	public Long getFiscalia() {
		return fiscalia;
	}
	@Override
	public void setFiscalia(Long fiscalia) {
		this.fiscalia = fiscalia;
	}
	@Override
	public List<SelectItem> getFiscalias() {
		return fiscalias;
	}
	@Override
	public void setFiscalias(List<SelectItem> fiscalias) {
		this.fiscalias = fiscalias;
	}
	@Override
	public List<SelectItem> getListGrupos() {
		SelectItem select = new SelectItem(-1, "<< Seleccionar >>");
		if(this.listGrupos.size() == 0){
			this.listGrupos.add(select);
		}
		return listGrupos;
	}
	@Override
	public void setListGrupos(List<SelectItem> listGrupos) {
		
		this.listGrupos = listGrupos;
	}
	@Override
	public List<GrupoUsuario> getListGrupoUsuarios() {
		
		return listGrupoUsuarios;
	}
	@Override
	public void setListGrupoUsuarios(List<GrupoUsuario> listGrupoUsuarios) {
		this.listGrupoUsuarios = listGrupoUsuarios;
	}
	@Override
	public void agregarGrupoUsuario() {
	
		if (this.grupoUsuario != -1) {
			GrupoUsuario grupoUsuarios = adminGrupos.getGrupoUsuario(this.grupoUsuario);
			if (grupoUsuarios.getUsuarios().size() > 0) {
				boolean esta = false;
				for (GrupoUsuario grupo : listGrupoUsuarios) {
					if (grupoUsuarios.getId().equals(grupo.getId())) {
						esta = true;
					}
				}
				if (!esta) {
					this.listGrupoUsuarios.add(grupoUsuarios);
				} else {
					FacesMessages.instance().add("El grupo ya esta en la lista");
				}
			} else {
				FacesMessages.instance().add("El grupo no tiene usuarios asociados");
			}
		}
		else{
			FacesMessages.instance().add("Debe seleccionar un grupo");
		}
	}
	@Override
	public Long getGrupoUsuario() {
		return grupoUsuario;
	}
	@Override
	public void setGrupoUsuario(Long grupoUsuario) {
		this.grupoUsuario = grupoUsuario;
	}
	
	@Override
	public void eliminarGrupoUsuario(GrupoUsuario g) {
		listGrupoUsuarios.remove(g);
//		for (int i=0; i < listGrupoUsuarios.size(); i++) {
//			GrupoUsuario grupoUsuario = listGrupoUsuarios.get(i);
//			if(grupoUsuario.getNombre().equals(nombre)){
//				listGrupoUsuarios.remove(i);
//			}
//		}
	}
	
	private void buscarDestinatarios() {
		listGrupoUsuarios = new ArrayList<GrupoUsuario>();
		listDestinatarios = new HashSet<Persona>();
		final List<Expediente> expedientes = me.buscarExpedientesHijos(this.expediente);
		for (Expediente e : expedientes) {
			try {
//				if (e.getEliminado() == null) {
//					e.setEliminado(false);
//				}
//				if (!e.getEliminado()) {
					if (e.getGrupo() != null) {
						listGrupoUsuarios.add(e.getGrupo());
					} else {
						if (e.getDestinatario() != null) {
							if (!e.getAcuseRecibo()) {
								e.getDestinatario().setBorrable(true);
							}
							e.getDestinatario().setDestinatarioConCopia(
									e.getCopia());
							listDestinatarios.add(e.getDestinatario());
						} else {
							if (!e.getAcuseRecibo()) {
								e.getDestinatario().setBorrable(true);
							}
							e.getDestinatarioHistorico()
									.setDestinatarioConCopia(e.getCopia());
							listDestinatarios.add(e.getDestinatarioHistorico());
						}
					}
//				}
			} catch (Exception e1) {
			}

		}
	}
	
	private boolean tieneRol(Rol rol){
		for(Rol rolusuario: usuario.getRoles())
			if(rolusuario.getId().equals(rol.getId()))
				return true;
		return false;
	}
	
	@Override
	public boolean obtenerTipoDocumento() {
		
		if(documentoOriginal != null) {
			if(documentoOriginal instanceof Resolucion){
			if(documentoOriginal.getFirmas() != null && documentoOriginal.getFirmas().size() > 0) return false;	
			if(documentoOriginal.getFirmas() != null && documentoOriginal.getFirmas().size() > 0 && expediente.getDestinatarioHistorico() != null && expediente.getDestinatarioHistorico().equals(usuario))return false;
				return true;
			}
			
		}
		return false;
	}
	
}
