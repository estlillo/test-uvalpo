package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearDecreto {

	void destroy();

	String end();

	/**
	 * Metodo para obtener un documento una vez firmado.
	 * 
	 * @return la vista de redireccion (al despachar expediente).
	 */
	String buscaDocumento();

	void visualizar();

	void firmar();

	/**
	 * Metodo para visar el documento sobre el que se esta trabajando.
	 * 
	 * @return la vista a la que redirigira al despachar el documento una vez visado.
	 */
	String visar();

	String desvisarYDesfirmar(Decreto dec);

	boolean renderedBotonDesvisarDesfirmar();

	void tipoRazonListener(ValueChangeEvent event);

	String getTipoArchivo();

	void setTipoArchivo(String tipoArchivo);

	void agregarDocumento();

	void agregarAntecedente();

	void listener(UploadEvent event) throws Exception;

	void salirPanel();

	void eliminarArchivoAntecedente(Integer idNuevoArchivo);

	Decreto getDecreto();

	void setDecreto(Decreto decreto);

	String getVistos();

	void setVistos(String vistos);

	String getConsiderando();

	void setConsiderando(String considerando);

	String getResuelvo();

	void setResuelvo(String resuelvo);

	List<SelectPersonas> getDistribucionDocumento();

	void setDistribucionDocumento(List<SelectPersonas> distribucionDocumento);

	List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	void verArchivo(Long id);

	Boolean getFirmado();

	Boolean getVisado();

	String getVisaciones();

	List<VisacionDocumento> getVisacionesV();

	List<FirmaEstructuradaDocumento> getFirmas();

	Integer getTipoDecreto();

	void setTipoDecreto(Integer tipoDecreto);

	boolean modificarVisaFirmas();

	/**
	 * Metodo para despachar un expediente.
	 * 
	 * @return la vista a la que redirigira el despacho.
	 */
	String despacharExpediente();

	boolean isRenderedFirmar();

	boolean isRenderedFirmarExento();

	boolean isRenderedFirmarContraloria();

	boolean isRenderedVisar();

	boolean isDisabledGuardar();

	boolean isRenderedBotonDespachar();

	boolean isDespachable();

	void guardarYCrearExpediente();

	boolean isEditable();

	boolean isRenderedBotonGuardarCrearExpediente();

	boolean isEditableSinVisa();

	boolean isRenderedBotonVisar();

	boolean isRenderedBotonGuardar();

	boolean isRenderedBotonVisualizar();

	String getUrlFirma();

	List<SelectItem> obtenerPlantillas();

	void plantillaListener(ValueChangeEvent event);

	/*
	 * public List<SelectItem> obtenerClasificacionSubTipo(); public List<SelectItem> obtenerClasificacionTipo();
	 */
	Integer getIdPlantilla();

	void setIdPlantilla(Integer idPlantilla);

	/*
	 * public Integer getIdClasificacionSubTipoSelec(); public void setIdClasificacionSubTipoSelec(Integer
	 * idClasificacionSubTipoSelec); public Integer getIdClasificacionTipoSelec(); public void
	 * setIdClasificacionTipoSelec(Integer idClasificacionTipoSelec);
	 */
	String cambiar();

	Integer getIdPlantillaSelec();

	void setIdPlantillaSelec(Integer idPlantillaSelec);

	/*
	 * public List<SelectItem> getTipos1(); public void setTipos1(List<SelectItem> tipos1); public List<SelectItem>
	 * getTipos2(); public void setTipos2(List<SelectItem> tipos2);
	 */
	boolean isRenderedPlantillaList();

	// public String desvisarYDesfirmar(Decreto dec);
	// public boolean renderedBotonDesvisarDesfirmar();

	String getMateriaArchivo();

	void setMateriaArchivo(String materiaArchivo);

	void limpiarAnexos();

	int getMaxFilesQuantity();

	/**
	 * <p>
	 * Metodo para rechazar un decreto. Rechazar un decreto implica:
	 * </p>
	 * <ul>
	 * <li>Llamar a anularDecreto en los servicios externos.</li>
	 * <li>Decreto debe regresar a SISPER.</li>
	 * <li>En EXEDOC, el expediente se debe archivar.</li>
	 * <li>Los flujos asignados al documento, con estado null, deben cambiarse a KO.</li>
	 * </ul>
	 * 
	 * @return la vista a la que redirigira el metodo.
	 */
	String rechazar();

	/**
	 * Metodo para obtener el id de la sesion de JBoss. Usado principalmente en el applet de firma para no perder el
	 * contexto.
	 * 
	 * @return el id de la sesion de JBoss.
	 */
	String getSessionId();

	/**
	 * Metodo para revisar cuando el boton de rechazar se pueda visualizar.
	 * 
	 * @return true si el boton se puede visualizar. false en caso contrario.
	 */
	boolean isRenderedRechazar();
}
