package cl.exe.exedoc.session;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;

import cl.exe.exedoc.entity.Cargo;

@Name("cargoList")
public class CargoList extends EntityQuery<Cargo> {

	private static final long serialVersionUID = 9023502699752804295L;

	@Override
	public String getEjbql() {
		return "select cargo from Cargo cargo";
	}
}
