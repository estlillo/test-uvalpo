package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Query;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;

@Local
public interface VerExpediente {

	String begin();

	String end();

	void destroy();

	Expediente getExpediente();

	void setExpediente(Expediente expediente);

	Long getDivision();

	void setDivision(Long division);

	Long getIdExpediente();

	void setIdExpediente(Long idExpediente);

	Object[] getListDestinatarios();

	boolean renderedGrupoDestinatariosFrecuentes();

	Long getUnidadOrganizacional();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getCargo();

	void setCargo(Long cargo);

	Long getDepartamento();

	void setDepartamento(Long departamento);

	List<SelectItem> getListDivision();

	List<SelectItem> getListDepartamento();

	List<SelectItem> getListUnidadesOrganizacionales();

	List<SelectItem> getListCargos();

	List<SelectItem> getListPersonas();

	void buscarDivisiones();

	void buscarDepartamentos();

	void buscarUnidadesOrganizacionales();

	void buscarCargos();

	void buscarPersonas();

	Long getPersona();

	void setPersona(Long persona);

	void agregarDestinatario();

	void registrarExpediente() throws DocumentNotUploadedException;

	void agregarObservacion();

	/*
	 * List<Documento> getListDocumentosAnexo(); void setListDocumentosAnexo(List<Documento> listDocumentosAnexo);
	 */

	List<Documento> getListDocumentosRespuesta();

	void setListDocumentosRespuesta(List<Documento> listDocumentosRespuesta);

	String getObservacion();

	void setObservacion(String observacion);

	List<Observacion> getObservaciones();

	void setObservaciones(List<Observacion> observaciones);

	String despacharExpediente() throws DocumentNotUploadedException;

	String archivarExpediente();

	List<SelectItem> getDestinatariosDocumentoOriginal();

	Integer getTipoDocumentoAdjuntar();

	void setTipoDocumentoAdjuntar(Integer tipoDocumentoAdjuntar);

	String adjuntarDocumento(int tipoAdjuntar);

	List<SelectItem> getListaDocumentosAdjuntar();

	void eliminarDocumento(Integer numDoc);

	void eliminarDestinatariosExpediente(Persona destinatario);

	void eliminarObservaciones(Integer idNuevaObservacion);

	// void eliminarAnexoDocumento(Integer numDoc);

	boolean esArchivable();

	String modificarDocumento(Integer numDoc, Integer lista);

	Documento getDocumentoOriginal();

	void setDocumentoOriginal(Documento documentoOriginal);

	void verArchivo();

	void verArchivo(Long idArchivo);

	void verArchivoObservacion(Archivo a);

	void listener(UploadEvent event) throws Exception;

	void listenerObs(UploadEvent event) throws Exception;

	String getMateriaArchivo();

	void setMateriaArchivo(String materiaArchivo);

	int getCopiaDirectoFlag();

	void setCopiaDirectoFlag(int copiaDirectoFlag);

	// void agregarAntecedenteArchivo();

	boolean renderedVisualizarExpediente();

	/**
	 * Metodo que agrega lista de destinatarios.
	 * 
	 * @param idLista {@link Long}
	 * @param tipoEnvio {@link Boolean}
	 */
	void agregarListaDestinatarios(final Long idLista, final Boolean tipoEnvio);

	List<DestinatariosFrecuentes> getListListas();

	void setListListas(List<DestinatariosFrecuentes> listListas);

	Long getGrupo();

	void setGrupo(Long grupo);

	List<SelectItem> getListDestFrec();

	void setListDestFrec(List<SelectItem> listDestFrec);

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	List<SelectItem> getListOrganizacion();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	Boolean getFirmado();

	boolean isRenderedFirmar();

	String getUrlFirma();

	String buscaDocumento() throws DocumentNotUploadedException;

	String visar() throws DocumentNotUploadedException;

	boolean isDisabledGuardar();

	boolean modificarVisaFirmas();

	boolean modificarVisaFirmasMinisterio();

	boolean isEditableSinVisa();

	boolean isRenderedVisar();

	boolean isEditable();

	Boolean getVisado();

	String getVisaciones();

	List<FirmaEstructuradaDocumento> getFirmas();

	Integer getCopiaDirecto();

	Boolean getFlagFoliador();

	void setFlagFoliador(Boolean flagFoliador);

	void setCopiaDirecto(final Integer copiaDirecto);

	boolean obtienePermisoEditFolio(int id);

	/**
	 * @return {@link Long}
	 */
	Long getCodigoBarra();

	/**
	 * @param codigoBarra {@link Long}
	 */
	void setCodigoBarra(final Long codigoBarra);

	/**
	 * @param doc {@link Documento}
	 */
	void cargarCodigoBarra(final Documento doc);

	/**
	 * @return {@link String}
	 * @throws DocumentNotUploadedException 
	 */
	String cancelarExpediente() throws DocumentNotUploadedException;

	/**
	 * Titulo del boton vizualizar.
	 * 
	 * @return {@link String}
	 */
	String getVisualizar();

	/**
	 * Titulo del boton vizualizar.
	 * 
	 * @param visualizar {@link String}
	 */
	void setVisualizar(String visualizar);

	/**
	 * Titulo del boton descargar Archivo.
	 * 
	 * @return {@link String}
	 */
	String getVerArchivoTitulo();

	/**
	 * Titulo del boton descargar Archivo.
	 * 
	 * @param verArchivoTitulo {@link String}
	 */
	void setVerArchivoTitulo(String verArchivoTitulo);

	/**
	 * Titulo del boton ver Archivo Original.
	 * 
	 * @return {@link String} 
	 */
	String getVerArchivoOriginal();

	/**
	 * Titulo del boton ver Archivo Original.
	 * 
	 * @param verArchivoOriginal {@link String}
	 */
	void setVerArchivoOriginal(String verArchivoOriginal);

	boolean renderedDestintarioExpediente();

	Long getFiscalia();

	void setFiscalia(Long fiscalia);

	List<SelectItem> getFiscalias();

	void setFiscalias(List<SelectItem> fiscalias);

	List<SelectItem> getListGrupos();

	void setListGrupos(List<SelectItem> listGrupos);

	List<GrupoUsuario> getListGrupoUsuarios();

	void setListGrupoUsuarios(List<GrupoUsuario> listGrupoUsuarios);

	void agregarGrupoUsuario();

	Long getGrupoUsuario();

	void setGrupoUsuario(Long grupoUsuario);

	//void eliminarGrupoUsuario(String nombre);

	void buscarGrupos();

	public boolean obtenerTipoDocumento();

	void eliminarGrupoUsuario(GrupoUsuario g);


}
