package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface SeleccionarDestinatarioDocumento {

	void destroy();

	Long getDivision();

	void setDivision(Long division);

	Long getDepartamento();

	void setDepartamento(Long departamento);

	Long getUnidadOrganizacional();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getCargo();

	void setCargo(Long cargo);

	List<SelectItem> getListDivision();

	List<SelectItem> getListDepartamento();

	List<SelectItem> getListUnidadesOrganizacionales();

	List<SelectItem> getListCargos();

	List<SelectItem> getListPersonas();

	void buscarDepartamentos();

	void buscarUnidadesOrganizacionales();

	void buscarCargos();

	void buscarPersonas();

	void agregarDestinatario();

	void agregarDestinatarioExterno();

	void buscarPersonasExternas();

	List<SelectPersonas> getDestinatariosDocumento();

	Long getPersona();

	void setPersona(Long persona);

	void eliminarDestinatarioDocumento(SelectPersonas persona);

	void agregarListaDestinatarios(Long idLista);

	// Long getIdAlias();
	// void setIdAlias(Long idAlias);
	// List<SelectItem> getAlias();
	// void agregarDestinatariosFiltro();

	Long getDependencia();

	void setDependencia(Long dependencia);

	Long getPersonaExterna();

	void setPersonaExterna(Long personaExterna);

	List<SelectItem> getListDependencia();

	List<SelectItem> getListPersonasExternas();

	List<DestinatariosFrecuentes> getListListas();

	void setListListas(List<DestinatariosFrecuentes> listListas);

	Long getGrupo();

	void setGrupo(Long grupo);

	List<SelectItem> getListDestFrec();

	void setListDestFrec(List<SelectItem> listDestFrec);

	void buscarDivisiones();

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	List<SelectItem> getListOrganizacion();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	boolean tieneEmisor();

	boolean isVisibleDivision();

	boolean isVisibleDepartamento();

	boolean isVisibleUnidad();

	boolean isVisibleDptoDivision();

	List<SelectItem> getListTodosDepartamento();

	List<SelectItem> getListTodosUnidades();

	boolean isVisibleUnidadDpto();

	/**
	 * Metodo que agrega destinatario al documento.
	 * 
	 * @param id {@link Long} identificador de persona
	 */
	void agregarDestinatarioById(Long id);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getAgregarDestinatarioDocumento();
	
	/**
	 * @param agregarDestinatarioDocumento {@link Boolean}
	 */
	void setAgregarDestinatarioDocumento(final Boolean agregarDestinatarioDocumento);

	/**
	 * @param id {@link Long}
	 * @param copia {@link Boolean}
	 */
	void agregar(Long id, Boolean copia);
	
	/**
	 * 
	 */
	void limpiarDestinatario();

	String getUsername();

	void setUsername(String username);

	String getNombres();

	void setNombres(String nombres);

	String getApellido();

	void setApellido(String apellido);

	void crear();

	void agregarModificar(Long id, Boolean copia);

	

	
	
}
