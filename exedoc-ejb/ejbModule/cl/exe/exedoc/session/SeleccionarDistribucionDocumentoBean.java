package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DependenciaExterna;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.PersonaExterna;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("seleccionarDistribucionDocumento")
public class SeleccionarDistribucionDocumentoBean implements
		SeleccionarDistribucionDocumento {

	private static final String SELECT_DESTINATARIOS_FRECUENTES = "SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<SelectPersonas> destinatariosDocumento;

	
	private static final String SELECCIONAR_INICIO = "<<Seleccionar>>";
	private final static String ERROR_DUPLICIDAD_DATOS  = "No se permite repetir destinatario";

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	// Externos
	private Long dependencia = JerarquiasLocal.INICIO;
	private Long personaExterna = JerarquiasLocal.INICIO;
	private List<SelectItem> listDependencia = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonasExternas = new ArrayList<SelectItem>();

	private List<DestinatariosFrecuentes> listListas = new ArrayList<DestinatariosFrecuentes>();

	// Grupo Destinatarios Frecuentes
	private Long grupo = JerarquiasLocal.INICIO;
	private List<SelectItem> listDestFrec = new ArrayList<SelectItem>();

	private List<SelectItem> buscarOrganizaciones() {
		limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listUnidadesOrganizacionales.clear();
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento departamento = departamentos.get(0);
			if (departamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}
	@Override
	public void agregarPorId(Long id){
		Persona persona = em.find(Persona.class, id);
		if(distribucionDocumento != null){
			for(SelectPersonas duplicados: distribucionDocumento){
				if(duplicados.getPersona() != null){
					if(duplicados.getPersona().getId().equals(id)){
						FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
						return;
					}
				}
			}
		}
		String value = persona.getNombres() + " "
				+ persona.getApellidoPaterno();

		Cargo cargo = persona.getCargo();
		if (cargo != null) {
			value += " - " + cargo.getDescripcion();
		}

		if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
			UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
			value += " - " + uo.getDescripcion();
		} else if (!persona.getCargo().getUnidadOrganizacional()
				.getDepartamento().getVirtual()) {
			Departamento depto = persona.getCargo().getUnidadOrganizacional()
					.getDepartamento();
			value += " - " + depto.getDescripcion();
		} else {
			Division d = persona.getCargo().getUnidadOrganizacional()
					.getDepartamento().getDivision();
			value += " - " + d.getDescripcion();
		}
		if (!estaDestinatario(value)) {
			persona.setDestinatarioConCopia(true);
			this.distribucionDocumento.add(new SelectPersonas(value,
					persona));
		}
		
		
	}
	public void agregarDistribucion() {
		try {
			if (!this.persona.equals(JerarquiasLocal.INICIO)) {
				final Persona persona = em.find(Persona.class, this.persona);
				String value = persona.getNombres() + " "
						+ persona.getApellidoPaterno();

				final Cargo cargo = em.find(Cargo.class, this.cargo);
				if (cargo != null) {
					value += " - " + cargo.getDescripcion();
				}

				if (!persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					final UnidadOrganizacional uo = em.find(
							UnidadOrganizacional.class,
							this.unidadOrganizacional);
					value += " - " + uo.getDescripcion();
				} else if (!persona.getCargo().getUnidadOrganizacional()
						.getDepartamento().getVirtual()) {
					final Departamento depto = em.find(Departamento.class,
							this.departamento);
					value += " - " + depto.getDescripcion();
				} else {
					final Division d = em.find(Division.class, this.division);
					value += " - " + d.getDescripcion();
				}
				if (!estaDestinatario(value)) {
					persona.setDestinatarioConCopia(true);
					this.distribucionDocumento.add(new SelectPersonas(value,
							persona));
				}
				this.limpiarDestinatario();
			} else if (!grupo.equals(JerarquiasLocal.INICIO)) {
				final Persona persona = em.find(Persona.class, this.grupo);
				String value = persona.getNombres() + " "
						+ persona.getApellidoPaterno();

				final Cargo cargo = persona.getCargo();
				if (cargo != null) {
					value += " - " + cargo.getDescripcion();
				}

				if (!persona.getCargo().getUnidadOrganizacional()
						.getVirtual()) {
					final UnidadOrganizacional uo = persona.getCargo()
							.getUnidadOrganizacional();
					value += " - " + uo.getDescripcion();
				} else if (!persona.getCargo().getUnidadOrganizacional()
						.getDepartamento().getVirtual()) {
					final Departamento depto = persona.getCargo()
							.getUnidadOrganizacional().getDepartamento();
					value += " - " + depto.getDescripcion();
				} else {
					final Division d = persona.getCargo()
							.getUnidadOrganizacional().getDepartamento()
							.getDivision();
					value += " - " + d.getDescripcion();
				}
				if (!estaDestinatario(value)) {
					persona.setDestinatarioConCopia(true);
					this.distribucionDocumento.add(new SelectPersonas(
							value, persona));
				}
				limpiarDestinatario();
			}
		} catch (Exception e) {
			log.info("Error agregando Destinatario de distribucion");
		}
	}

	public List<Persona> getPersonasFiscalias(){
		List<Persona> distribuir = new ArrayList<Persona>();
		
		final StringBuffer sql = new StringBuffer();
		// buscar personas para distribucion
		
		if(this.unidadOrganizacional.equals(jerarquias.INICIO)){
			sql.append(" select p from Persona p left join p.cargo c left join c.unidadOrganizacional u left join u.departamento d where d.id = :fiscalia and p.distribucion = true ");
		}
		else{
			sql.append(" select p from Persona p left join p.cargo c left join c.unidadOrganizacional u where u.id = :unidad and p.distribucion = true ");
		}
		
		final Query query = em.createQuery(sql.toString());
		if(this.unidadOrganizacional.equals(jerarquias.INICIO))query.setParameter("fiscalia", this.departamento);
		else query.setParameter("unidad", this.unidadOrganizacional);
		
		distribuir = query.getResultList();
		
		return distribuir;
	}
	
	public void agregarDistribucionUnidad(){
		try {
			
			List<Persona> distriucion = getPersonasFiscalias();
			for (Persona persona : distriucion) {
//				if (destinatariosDocumento != null
//						&& !destinatariosDocumento.isEmpty()) {
//					for (SelectPersonas sp : destinatariosDocumento) {
//						if (sp.getPersona().getId().equals(persona.getId())) {
//							FacesMessages
//									.instance()
//									.add("No se puede agregar a esta persona a la distribucion. Ya se encuentra en el documento como directo");
//							return;
//						}
//					}
//				}
				String value = persona.getNombres() + " "
						+ persona.getApellidoPaterno();

				final Cargo cargo = persona.getCargo();
				if (cargo != null) {
					value += " - " + cargo.getDescripcion();
				}

				if (cargo != null && !persona.getCargo().getUnidadOrganizacional().getVirtual()) {
					final UnidadOrganizacional uo = persona.getCargo().getUnidadOrganizacional();
					value += " - " + uo.getDescripcion();
				} else if (cargo != null && !persona.getCargo().getUnidadOrganizacional()
						.getDepartamento().getVirtual()) {
					final Departamento depto = em.find(Departamento.class,
							this.departamento);
					value += " - " + depto.getDescripcion();
				} else {
					final Division d = persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
					value += " - " + d.getDescripcion();
				}
				if (!estaDestinatario(value)) {
					persona.setDestinatarioConCopia(true);
					this.distribucionDocumento.add(new SelectPersonas(value,
							persona));
				}
				listOrganizacion.clear();
				listOrganizacion.addAll(this.buscarOrganizaciones());
				organizacion = (Long) listOrganizacion.get(1).getValue();
				this.buscarDivisiones();
				division = (Long) listDivision.get(1).getValue();
				this.buscarDepartamentos();
			}
			
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void agregarDestinatarioExterno() {
		if (!personaExterna.equals(JerarquiasLocal.INICIO)) {
			final PersonaExterna pe = em.find(PersonaExterna.class,
					personaExterna);
			String value = pe.getNombres() + " - " + pe.getDependencia().getDescripcion();
			if (!estaDestinatario(value)) {
				this.distribucionDocumento.add(new SelectPersonas(value));
			}
		}
		this.limpiarDestinatario();
	}

	private boolean estaDestinatario(final String nombre) {
		SelectPersonas s = new SelectPersonas(nombre);
		for (SelectPersonas si : this.distribucionDocumento) {
			if (si.getDescripcion().equals(s.getDescripcion())) {
				FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
				return true;
			}
		}
		return false;
	}

	public void eliminarDistribucionDocumento(final String destinatario) {
		int index = 0;
		for (SelectPersonas sp : this.distribucionDocumento) {
			if (sp.getDescripcion().equals(destinatario)) {
				this.distribucionDocumento.remove(index);
				break;
			}
			index++;
		}
	}

	@Override
	public void limpiarDestinatario() {
		dependencia = JerarquiasLocal.INICIO;
		personaExterna = JerarquiasLocal.INICIO;
		listPersonasExternas.clear();
		listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDependencia.clear();
		listDependencia.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));

		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		/*if (listOrganizacion.size() == 2) {
			organizacion = (Long) listOrganizacion.get(1).getValue();
			buscarDivisiones();
		}*/
		grupo = JerarquiasLocal.INICIO;
		listDestFrec.clear();
		listDestFrec.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarPersonasExternas() {
		listPersonasExternas.clear();
		listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		if (!dependencia.equals(JerarquiasLocal.INICIO)) {
			final Query query = em
					.createNamedQuery("PersonaExterna.findByIdDependencia");
			query.setParameter("id", dependencia);
			final List<PersonaExterna> personasExternas = query.getResultList();
			if (personasExternas != null) {
				for (PersonaExterna pe : personasExternas) {
					listPersonasExternas.add(new SelectItem(pe.getId(), pe
							.getNombres()));
				}
			}
		}
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(final Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		if (this.distribucionDocumento == null) {
			this.distribucionDocumento = new ArrayList<SelectPersonas>();
		}
		return distribucionDocumento;
	}

	public Long getDependencia() {
		return dependencia;
	}

	public void setDependencia(final Long dependencia) {
		this.dependencia = dependencia;
	}

	public Long getPersonaExterna() {
		return personaExterna;
	}

	public void setPersonaExterna(final Long personaExterna) {
		this.personaExterna = personaExterna;
	}

	@SuppressWarnings("unchecked")
	public List<SelectItem> getListDependencia() {
		if (listDependencia.size() == 1) {
			listPersonasExternas.clear();
			listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
			listDependencia.clear();
			listDependencia.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));

			Query query = em
					.createNamedQuery("DependenciaExterna.findByAll");

			final List<DependenciaExterna> dependencias = query.getResultList();
			if (dependencias != null) {
				for (DependenciaExterna de : dependencias) {
					listDependencia.add(new SelectItem(de.getId(), de
							.getDescripcion()));
				}
			}
		}
		return listDependencia;
	}

	public List<SelectItem> getListPersonasExternas() {
		return listPersonasExternas;
	}

	public List<DestinatariosFrecuentes> getListListas() {
		listListas = cargarListasExistentes();
		return listListas;
	}

	public void setListListas(final List<DestinatariosFrecuentes> listListas) {
		this.listListas = listListas;
	}

	/**
	 * @return {@link List} of {@link DestinatariosFrecuentes}
	 */
	@SuppressWarnings("unchecked")
	private List<DestinatariosFrecuentes> cargarListasExistentes() {
		final StringBuffer misListasJPQL = new StringBuffer(
				SELECT_DESTINATARIOS_FRECUENTES);
		misListasJPQL
				.append("ldf.propietario.id = ? and (ldf.esGrupo is null or ldf.esGrupo = false)");
		final Query misListasQuery = em.createQuery(misListasJPQL.toString());
		misListasQuery.setParameter(1, usuario.getId());
		final List<DestinatariosFrecuentes> misListas = misListasQuery
				.getResultList();

		return misListas;
	}

	@Override
	public void agregarListaDistribucion(final Long idLista) {
		final DestinatariosFrecuentes lista = em.find(
				DestinatariosFrecuentes.class, idLista);
		final List<Persona> destinatariosDesdeLista = lista
				.getDestinatariosFrecuentes();

		for (Persona destinatario : destinatariosDesdeLista) {
			this.persona = destinatario.getId();
			this.cargo = destinatario.getCargo().getId();
			this.unidadOrganizacional = destinatario.getCargo()
					.getUnidadOrganizacional().getId();
			this.departamento = destinatario.getCargo()
					.getUnidadOrganizacional().getDepartamento().getId();
			this.division = destinatario.getCargo().getUnidadOrganizacional()
					.getDepartamento().getDivision().getId();
			this.agregarDistribucion();
		}
	}

	@Override
	public Long getGrupo() {
		return grupo;
	}

	@Override
	public void setGrupo(final Long grupo) {
		this.grupo = grupo;
	}

	@Override
	public List<SelectItem> getListDestFrec() {
		listDestFrec = this.buscarDestFrec();
		return listDestFrec;
	}

	@Override
	public void setListDestFrec(final List<SelectItem> listDestFrec) {
		this.listDestFrec = listDestFrec;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			listOrganizacion.addAll(this.buscarOrganizaciones());
		}
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	/**
	 * Metodo que busca los destinatarios frecuentes.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarDestFrec() {
		final DestinatariosFrecuentes grupoDestFrec = this.obtenerGrupo();
		if (grupoDestFrec != null) {
			final List<Persona> destinatariosFrecuentes = grupoDestFrec
					.getDestinatariosFrecuentes();
			final List<SelectItem> listDestinatariosFrecuentes = new ArrayList<SelectItem>();
			listDestinatariosFrecuentes.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			for (Persona p : destinatariosFrecuentes) {
				listDestinatariosFrecuentes.add(new SelectItem(p.getId(), p
						.getNombreApellido()));
			}
			return listDestinatariosFrecuentes;
		} else {
			return new ArrayList<SelectItem>();
		}
	}

	/**
	 * @return {@link DestinatariosFrecuentes}
	 */
	private DestinatariosFrecuentes obtenerGrupo() {
		DestinatariosFrecuentes miGrupo;
		try {
			final StringBuffer miGrupoJPQL = new StringBuffer(
					SELECT_DESTINATARIOS_FRECUENTES);
			miGrupoJPQL.append("ldf.propietario.id = ? and ldf.esGrupo = true");
			final Query miGrupoQuery = em.createQuery(miGrupoJPQL.toString());
			miGrupoQuery.setParameter(1, usuario.getId());
			miGrupo = (DestinatariosFrecuentes) miGrupoQuery.getSingleResult();
		} catch (NoResultException nre) {
			miGrupo = null;
		}

		return miGrupo;
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

}
