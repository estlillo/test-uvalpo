package cl.exe.exedoc.session;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.util.SelectPersonas;

@Local
public interface CrearProvidencia {

	public void destroy();
	public String end();
	public Providencia getProvidencia();
	
	public void guardar();
	public void visualizar();
	public void firmar();
	public String agregarDocumento();
	public void guardarYCrearExpediente();
		
	public Map<Long, Boolean> getAccionesProvidencia();
	
	public List<SelectPersonas> getDestinatariosDocumento();
	
	public Boolean getFirmado();
	
	public String completar();
	public Boolean getCompletado();
	public boolean isEditable();
	public boolean isDespachable();
	public boolean isRenderedBotonFirmar();
	public boolean isRenderedBotonDespachar();
	public boolean isRenderedBotonGuardar();
	public boolean isRenderedBotonGuardarCrearExpediente();
	public boolean isRenderedBotonVisualizar();
	
	/* **************************************************************************
	 * Archivos Anexos - Antecedentes
	 ************************************************************************* */
	public void listener(UploadEvent event) throws Exception;
	public void agregarAntecedenteArchivo();
	public void verArchivo(Long idArchivo);
	//public void eliminarAnexoDocumento(Integer numDoc);
	public String getMateriaArchivo();
	public void setMateriaArchivo(String materiaArchivo);
	//public List<Documento> getListDocumentosAnexo();
	
	/* **************************************************************************
	 * DESPACHAR EXPEDIENTES
	 ************************************************************************* */	
	public String despacharExpediente();
	
}
