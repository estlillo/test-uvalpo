package cl.exe.exedoc.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.util.Conversions.IntegerConverter;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.FolioDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * @author Sergio
 * 
 */

@Stateful
@Name("mantenedorFolios")
@Scope(ScopeType.CONVERSATION)
public class MantenedorFoliosBean implements MantenedorFolios, Serializable {

	/**
	 * 
	 */
	public static final String MANTENEDOR_FOLIOSDOCUMENTO = "mantenedorFolios";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String SELECCIONAR = "<<Seleccionar>>";

	private static final String ERROR_AL_OBTENER_FOLIOS = "Error al obtener folios: ";

	private static final String OPERACIONREALIZADA = "Operación realizada correctamente.";

	private FolioDocumento folioDocumento;
	private List<FolioDocumento> listFolios;

	@EJB
	private JerarquiasLocal jerarquias;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	private List<SelectItem> fiscalias = new ArrayList<SelectItem>();
	private List<SelectItem> unidades = new ArrayList<SelectItem>();
	private List<SelectItem> listTipoDocumentos;

	private Long fiscalia = JerarquiasLocal.INICIO;
	private Long unidad = JerarquiasLocal.INICIO;
	private Integer tipoDocumento;
	private String unidadTmp;
	private Boolean habilitaUnidad;
	private Boolean editable;
	private String valorInicial;
	private String valorAgno;
	
	/**
	 * Constructor.
	 */
	public MantenedorFoliosBean() {
		super();
	}

	@Remove
	@Override
	public void remove() {
	}

	@Destroy
	@Override
	public void destroy() {
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation: mantenedorFolios");

		this.editable = false;
		this.setHabilitaUnidad(false);
		this.folioDocumento = new FolioDocumento();

		this.setFiscalias(jerarquias.getFiscalias());
		this.setFiscalia(this.usuario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getId());

		this.setUnidades(jerarquias.getUnidadesOrganzacionales(SELECCIONAR,
				this.usuario.getCargo().getUnidadOrganizacional()
						.getDepartamento().getId()));
		this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional()
				.getId());

		this.setListTipoDocumentos(TipoDocumentosList
				.getInstanceTipoDocumentoList(em).getTiposDocumentoListVisible(
						SELECCIONAR));

		folioDocumento.setNivel(false);
		final Calendar toDay = Calendar.getInstance();
		this.valorAgno = Integer.toString(toDay.get(Calendar.YEAR));
		this.valorInicial = "0";
		this.cargarFolios();
		return MANTENEDOR_FOLIOSDOCUMENTO;
	}

	/**
	 * 
	 */
	private void cargarFolios() {
		this.setListFolios(this.buscarFolio());
		if (this.getListFolios() == null) {
			this.setListFolios(new ArrayList<FolioDocumento>());
		}
	}

	@Override
	public FolioDocumento getFolioDocumento() {
		return folioDocumento;
	}

	@Override
	public void setFolioDocumento(final FolioDocumento folioDocumento) {
		this.folioDocumento = folioDocumento;
	}

	@Override
	public List<FolioDocumento> getListFolios() {
		return listFolios;
	}

	@Override
	public void setListFolios(final List<FolioDocumento> listFolios) {
		this.listFolios = listFolios;
	}

	@SuppressWarnings("unchecked")
	public List<FolioDocumento> buscarFolio() {

		List<FolioDocumento> folios = null;

		Query query = em.createNamedQuery("FolioDocumento.findByAll");

		try {
			folios = query.getResultList();
		} catch (PersistenceException e) {
			log.error(FolioDocumento.ERROR_AL_OBTENER_FOLIOS, e);
		}
		return folios;
	}

	@Override
	public Long getFiscalia() {
		return fiscalia;
	}

	@Override
	public void setFiscalia(Long fiscalia) {
		this.fiscalia = fiscalia;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public List<SelectItem> getFiscalias() {
		return fiscalias;
	}

	@Override
	public void setFiscalias(List<SelectItem> fiscalias) {
		this.fiscalias = fiscalias;
	}

	@Override
	public List<SelectItem> getUnidades() {
		return unidades;
	}

	@Override
	public void setUnidades(List<SelectItem> unidades) {
		this.unidades = unidades;
	}

	@Override
	public void setListTipoDocumentos(List<SelectItem> listTipoDocumentos) {
		this.listTipoDocumentos = listTipoDocumentos;
	}

	@Override
	public List<SelectItem> getListTipoDocumentos() {
		return listTipoDocumentos;
	}

	@Override
	public void setTipoDocumento(Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Integer getTipoDocumento() {
		return tipoDocumento;
	}

	@Override
	public void guardarFolio() {
		final boolean status;
		boolean ok = true;

		if (this.fiscalias != null && this.fiscalia.longValue() > 0) {
			folioDocumento.setFiscalia(em.find(Departamento.class,
					this.fiscalia));
		} else {
			FacesMessages.instance().add("Debe seleccionar una fiscalía.");
			ok = false;
		}

		if (this.unidades != null && this.unidad.longValue() > 0) {
			folioDocumento.setUnidad(em.find(UnidadOrganizacional.class,
					this.unidad));
		} else {
			if (!folioDocumento.getNivel()) {
				FacesMessages.instance().add("Debe seleccionar una unidad.");
				ok = false;
			}
		}

		if (this.tipoDocumento != null && this.tipoDocumento.longValue() > 0) {
			folioDocumento.setTipoDocumento(em.find(TipoDocumento.class,
					this.tipoDocumento));
		} else {
			FacesMessages.instance().add(
					"Debe seleccionar un tipo de documento.");
			ok = false;
		}

		//Try Valor inicial Folio
		try {
			Long valorIni = Long.parseLong(this.valorInicial);
			if (valorIni != null && valorIni < 0) {
				FacesMessages.instance().add(
						"El valor inicial no puede ser negativo");
				ok = false;
			} else {
				folioDocumento.setNumeroActual(valorIni);
			}
		} catch (Exception exception) {
			FacesMessages.instance().add("Valor inicial debe ser numérico");
			ok = false;
		}
		
		//Try Año
		try {
			Long valorAgno = Long.parseLong(this.valorAgno);
			if (valorAgno != null && valorAgno < 0) {
				FacesMessages.instance().add(
						"El año no puede ser negativo");
				ok = false;
			} else {
				this.folioDocumento.setAgnoActual(valorAgno.intValue());
				final Calendar toDay = Calendar.getInstance();
				if (this.folioDocumento.getAgnoActual() < toDay
						.get(Calendar.YEAR)) {
					FacesMessages.instance().add(
							"El año no puede ser anterior al actual");
					ok = false;
				}
				
				if (this.folioDocumento.getAgnoActual() > (toDay.get(Calendar.YEAR) + 10) ) {
					FacesMessages.instance().add(
							"El año no puede ser mayor a " + (toDay.get(Calendar.YEAR) + 10));
					ok = false;
				}
			}
		} catch (Exception exception) {
			FacesMessages.instance().add("Año debe ser numérico");
			ok = false;
		}
		
		if (ok) {
			if (this.validarFolio() && !this.editable) {
				FacesMessages.instance().add(
						"Ya existe folio para la selección.");
				ok = false;
			}

			if (ok) {
				if (this.folioDocumento.getId() == null) {
					log.info("Creando Folio: ");
					folioDocumento.setFechaCreacion(new Date());
					status = this.crear(folioDocumento);
				} else {
					log.info("Actualizando Folio: ");
					folioDocumento.setFechaCreacion(new Date());
					status = this.actualizar(folioDocumento);
				}

				if (status) {
					this.cargarFolios();
					this.setFolioDocumento(new FolioDocumento());
					this.limpiarFolio();
					FacesMessages.instance().add(OPERACIONREALIZADA);
				} else {
					FacesMessages.instance().add(
							"Ha ocurrido un error al guardar el folio.");
				}
			}
		}
	}

	@Override
	public boolean crear(final FolioDocumento folio) {
		try {
			em.persist(folio);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al persistir folio: ", e);
		}

		return false;
	}

	@Override
	public boolean actualizar(final FolioDocumento folio) {

		try {
			em.merge(folio);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al actualizar folio: ", e);
		}

		return false;
	}

	@Override
	public void editarFolio(final Long id) {

		final FolioDocumento fd = em.find(FolioDocumento.class, id);

		if (fd != null) {
			this.fiscalia = fd.getFiscalia().getId();
			if (fd.getUnidad() != null && fd.getUnidad().getId() > 0) {
				this.unidad = fd.getUnidad().getId();
			} else {
				this.unidad = jerarquias.INICIO;
				this.setHabilitaUnidad(true);
				folioDocumento.setNivel(true);
				fd.getUnidadTmp();
			}
			this.tipoDocumento = fd.getTipoDocumento().getId();
			this.valorAgno = String.valueOf(fd.getAgnoActual());
			this.valorInicial = String.valueOf(fd.getNumeroActual());
			this.folioDocumento.setId(fd.getId());
			this.editable = true;
			FacesMessages.instance().add(OPERACIONREALIZADA);
		} else {
			FacesMessages.instance().add(ERROR_AL_OBTENER_FOLIOS);
		}
	}

	@Override
	public Boolean validarFolio() {

		final Query query;
		final StringBuilder sql = new StringBuilder();
		List<FolioDocumento> listFolio = new ArrayList<FolioDocumento>();

		if (folioDocumento.getNivel()) {
			sql.append("select fd from FolioDocumento fd where fd.fiscalia.id = :fiscalia and fd.unidad IS NULL and fd.tipoDocumento.id = :tipodocumento");
			query = em.createQuery(sql.toString());
		} else {
			sql.append("select fd from FolioDocumento fd where fd.fiscalia.id = :fiscalia and fd.unidad.id = :unidad and fd.tipoDocumento.id = :tipodocumento");
			query = em.createQuery(sql.toString());
			query.setParameter("unidad", folioDocumento.getUnidad().getId());
		}
		query.setParameter("fiscalia", folioDocumento.getFiscalia().getId());
		query.setParameter("tipodocumento", folioDocumento.getTipoDocumento()
				.getId());

		try {
			listFolio = query.getResultList();
		} catch (NoResultException e) {
			return false;
		}

		if (listFolio.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public void nivelFiscalia() {
		if (folioDocumento.getNivel()) {
			this.unidad = jerarquias.INICIO;
			this.setHabilitaUnidad(true);
		} else {
			this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional()
					.getId());
			this.setHabilitaUnidad(false);
		}
	}

	@Override
	public void setHabilitaUnidad(boolean habilitaUnidad) {
		this.habilitaUnidad = habilitaUnidad;
	}

	@Override
	public boolean getHabilitaUnidad() {
		return habilitaUnidad;
	}

	@Override
	public void limpiarFolio() {
		this.setHabilitaUnidad(false);
		this.folioDocumento = new FolioDocumento();

		this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getId());

		this.setTipoDocumento(jerarquias.INICIO.intValue());

		folioDocumento.setNivel(false);
		final Calendar toDay = Calendar.getInstance();
		this.valorAgno = Integer.toString(toDay.get(Calendar.YEAR));
		this.valorInicial = "0";
		this.editable = false;
	}

	@Override
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	@Override
	public Boolean getEditable() {
		return editable;
	}

	@Override
	public void setValorInicial(String valorInicial) {
		this.valorInicial = valorInicial;
	}

	@Override
	public String getValorInicial() {
		return valorInicial;
	}

	@Override
	public void setValorAgno(String valorAgno) {
		this.valorAgno = valorAgno;
	}

	@Override
	public String getValorAgno() {
		return valorAgno;
	}
}
