package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.AccionProvidencia;
import cl.exe.exedoc.entity.AnulacionResolucion;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DiaAdministrativo;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.HistorialDespacho;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.SolicitudFolio;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.Vacaciones;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.mantenedores.AdministradorGrupoUsuario;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.ObservacionValidator;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * Clase ModificarExpedienteBean.
 * 
 * @author Ricardo
 */
@Stateful
@Name("modificarExpediente")
@Scope(ScopeType.CONVERSATION)
public class ModificarExpedienteBean implements ModificarExpediente {

	private static final String EXPEDIENTE_DESPACHADO = "Expediente Despachado";

	private static final String BANDEJA_SALIDA = "bandejaSalida";

	private static final String CREAR_EXPEDIENTE = "crearExpediente";

	private static final String SELECCIONAR_INICIO = JerarquiasLocal.TEXTO_INICIAL;
	@Logger
	private Log log;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private AdministradorGrupoUsuario adminGrupos;
	
	@EJB
	private AdministrarHistorialDespacho adminDpacho; //24-02

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	@Out(required = true)
	private String homeCrear = "modificarExpediente";

	@Out(required = false)
	private String homeDistribucion;

	@In(required = false, value = "esSolicitud", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "esSolicitud", scope = ScopeType.CONVERSATION)
	private Boolean esSolicitud;

	// Esto viene de la solicitud de dia administrativo y de vacaciones
	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Persona
	 * solicitante;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private String
	 * grado;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Double
	 * diasAdministrativosPendientes;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private Double
	 * diasVacacionesPendientes;
	 * 
	 * @In(required = false, value = "calidadJuridica", scope =
	 * ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, value = "calidadJuridica", scope =
	 * ScopeType.CONVERSATION) private String calidadJuridica;
	 * 
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private String
	 * datosObtenidos;
	 */
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@RequestParameter
	private Long idExpediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	// TODO revisar documentos anexos
	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * 
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private
	 * List<Documento> listDocumentosAnexo;
	 */

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	private Set<Persona> listDestinatariosTmp;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatariosOriginal;

	private List<SelectItem> destinatariosDocumentoOriginal = new ArrayList<SelectItem>();

	private ArchivoDocumentoBinario archivo;
	private String materiaArchivo;

	// Datos Observaciones
	private String observacion;
	private ObservacionArchivo archivoObs;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	private List<SelectItem> listGrupos = new ArrayList<SelectItem>();
	private List<SelectItem> fiscalias = new ArrayList<SelectItem>();

	private List<GrupoUsuario> listGrupoUsuarios = new ArrayList<GrupoUsuario>(0);
	private List<GrupoUsuario> listGrupoUsuariosTmp;

	private Long fiscalia = JerarquiasLocal.INICIO;
	private Long grupoUsuario = JerarquiasLocal.INICIO;

	private List<Long> idExpedientes;
	private List<Long> idExpedientesFinales;
	private boolean docValido;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	Documento documento;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento consulta;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento respuesta;

	// Documentos Respuesta
	private Integer tipoDocumentoAdjuntar = 0;
	private List<SelectItem> listaDocumentosAdjuntar;
	private List<SelectItem> listaDocumentosAnexar = new ArrayList<SelectItem>();

	private int contadorObservaciones;
	private int contadorDocumentos;

	private int copiaDirectoFlag;

	private List<DestinatariosFrecuentes> listListas;

	// Grupo Destinatarios Frecuentes
	private Long grupo;
	private List<SelectItem> listDestFrec;

	private Long codigoBarra;
	
	private String visualizar = JerarquiasLocal.VISUALIZAR;
	private String verArchivoTitulo = JerarquiasLocal.DESCARGAR_ARCHIVO;
	private String verArchivoOriginal = JerarquiasLocal.VER_ARCHIVO_ORIGINAL;

//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<DetalleDias> diasSolicitados;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false,scope = ScopeType.CONVERSATION)
//	private InformacionPersonal informacionPersonal;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private InformacionPersonalSrv informacionPersonalSrv;
	
	/**
	 * Constructor.
	 */
	public ModificarExpedienteBean() {
		super();
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}
	
	@SuppressWarnings("unchecked")
	@Begin(join = true)
	@Override
	public String begin() throws DocumentNotUploadedException {
		long start = System.currentTimeMillis();
		boolean mostrarTiempo = true;
		copiaDirectoFlag = 0;
		this.listGrupoUsuarios = new ArrayList<GrupoUsuario>(0);
		this.buscarGrupos();

		listDestFrec = new ArrayList<SelectItem>();

		destinatariosDocumento = new ArrayList<SelectPersonas>();

		if (idExpediente != -1L) {
			// listDocumentosAnexo = new ArrayList<Documento>();
			listDocumentosRespuesta = new ArrayList<Documento>();
			listDestinatarios = new HashSet<Persona>();
			mostrarTiempo = false;
		} else {
			if (!this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.RESOLUCION)) {
				listDestinatarios = new HashSet<Persona>();
			}
		}

		// if (documentoOriginal instanceof Adquisicion) {
		// if (((Adquisicion) documentoOriginal).getEmailResponsable().length()
		// != 0) {
		// Pattern p =
		// Pattern.compile("^[a-zA-Z0-9._%+-]+\\@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$");
		// Matcher m = p.matcher(((Adquisicion)
		// documentoOriginal).getEmailResponsable());
		// if (!m.find()) {
		// FacesMessages.instance().add("Debe ser un correo electrónico");
		// return "crearSolicitudAdquisicion";
		// }
		// }
		// }

		destinatariosDocumentoOriginal = new ArrayList<SelectItem>();

		observaciones = new ArrayList<Observacion>();

		listDocumentosEliminados = new ArrayList<Documento>();

		if (listOrganizacion == null || listOrganizacion.size() == 0) {
			listDivision = new ArrayList<SelectItem>();
			listDepartamento = new ArrayList<SelectItem>();
			listUnidadesOrganizacionales = new ArrayList<SelectItem>();
			listCargos = new ArrayList<SelectItem>();
			listPersonas = new ArrayList<SelectItem>();
			listOrganizacion = this.buscarOrganizaciones();
			
			/*22-06-2012: Se reactivan todos los niveles de ExeDoc*/
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
//	        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//	        this.buscarUnidadesOrganizacionales();
			/**/
		}

		idExpedientes = new ArrayList<Long>();
		idExpedientesFinales = new ArrayList<Long>();
		
		homeCrear = "modificarExpediente";

		tipoDocumentoAdjuntar = 0;
		listaDocumentosAdjuntar = new ArrayList<SelectItem>();
		listaDocumentosAdjuntar.addAll(this.buscarDocumentosAdjuntar());
		listaDocumentosAnexar = new ArrayList<SelectItem>();
		listaDocumentosAnexar.addAll(this.buscarDocumentosAnexar());

		contadorObservaciones = 0;
		contadorDocumentos = 0;

		if (idExpediente == -1L) {
			
			mostrarTiempo = false;
			
			if (documentoOriginal == null) {
				FacesMessages
						.instance()
						.add("Modificar Expediente: Debe Ingresar Datos Obligatorios");
				if (esSolicitud == null || !esSolicitud) {
					return CREAR_EXPEDIENTE;
				}
			} else {
				try {
					this.guardarExpediente();
				} catch (DocumentNotUploadedException e) {
					log.error("Error: ", e);
					return null;
				}
			}
		} else {
			this.expediente = em.find(Expediente.class, idExpediente);
			if (expediente.getVersionPadre() != null) {
				this.expediente = em.find(Expediente.class, expediente.getVersionPadre().getId());
			}
			if(this.expediente != null){
				this.buscarDatosExpedienteDocumentos();
			}
		}

		listListas = this.cargarListasExistentes();
		listDestFrec = this.buscarDestFrec();

		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente",
				"fin", new Date());
		
		if (esSolicitud == null || !esSolicitud) {

			if (mostrarTiempo)
				LogTiempos.mostrarLog(this.usuario.getUsuario(),
						"Recuperar Expediente Bandeja de Salida",
						System.currentTimeMillis() - start);

			return "modificarExpediente";
		} else {
			esSolicitud = null;
			return this.despacharExpediente();
		}
		

	}

	@SuppressWarnings("unchecked")
	private List<DestinatariosFrecuentes> cargarListasExistentes() {
		final StringBuffer misListasJPQL = new StringBuffer();
		misListasJPQL
				.append("SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
		misListasJPQL
				.append("ldf.propietario.id = ? and (ldf.esGrupo is null or ldf.esGrupo = false)");
		final Query misListasQuery = em.createQuery(misListasJPQL.toString());
		misListasQuery.setParameter(1, usuario.getId());
		final List<DestinatariosFrecuentes> misListas = misListasQuery
				.getResultList();

		return misListas;
	}

	/**
	 * Metodo que busca la lista de destinatarios frecuentes.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarDestFrec() {
		final DestinatariosFrecuentes grupoDestFrec = this.obtenerGrupo();
		if (grupoDestFrec != null) {
			final List<Persona> destinatariosFrecuentes = grupoDestFrec
					.getDestinatariosFrecuentes();
			final List<SelectItem> listDestinatariosFrecuentes = new ArrayList<SelectItem>();
			listDestinatariosFrecuentes.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			for (Persona p : destinatariosFrecuentes) {
				listDestinatariosFrecuentes.add(new SelectItem(p.getId(), p
						.getNombreApellido()));
			}
			return listDestinatariosFrecuentes;
		} else {
			return new ArrayList<SelectItem>();
		}
	}

	/**
	 * Metodo que obtiene grupo de destinatarios frecuentes.
	 * 
	 * @return {@link DestinatariosFrecuentes}
	 */
	private DestinatariosFrecuentes obtenerGrupo() {
		DestinatariosFrecuentes miGrupo;
		try {
			final StringBuffer miGrupoJPQL = new StringBuffer();
			miGrupoJPQL
					.append("SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
			miGrupoJPQL.append("ldf.propietario.id = ? and ldf.esGrupo = true");
			final Query miGrupoQuery = em.createQuery(miGrupoJPQL.toString());
			miGrupoQuery.setParameter(1, usuario.getId());
			miGrupo = (DestinatariosFrecuentes) miGrupoQuery.getSingleResult();
		} catch (NoResultException nre) {
			miGrupo = null;
		}

		return miGrupo;
	}

	@Override
	public boolean renderedGrupoDestinatariosFrecuentes() {
		final SelectItem selectItem = new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL);
		if (listDestFrec == null || listDestFrec.isEmpty()
				|| listDestFrec.contains(selectItem)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean renderedVisualizarExpediente() {
		boolean valido = false;
		if (this.documentoOriginal != null && 
				this.documentoOriginal.getFormatoDocumento() != null &&
				this.documentoOriginal.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO)) {
			if (documentoOriginal instanceof Carta || 
					documentoOriginal instanceof Memorandum ||
					documentoOriginal instanceof Oficio ||
					documentoOriginal instanceof Resolucion){
				valido = true;
			}
		}
		return valido;
	}

	/**
	 * buscar Datos Expediente Documentos.
	 */
	private void buscarDatosExpedienteDocumentos() {
		expediente = em.find(Expediente.class, expediente.getId());
		for (DocumentoExpediente de : expediente.getDocumentos()) {

			if (!de.getTipoDocumentoExpediente().getId()
					.equals(TipoDocumentoExpediente.ORIGINAL)) {
				de.getDocumento().setTipoDocumentoExpediente(
						new TipoDocumentoExpediente(
								de.getTipoDocumentoExpediente().getId()));
				if (de.getDocumento().getEliminado() == null
						|| !de.getDocumento().getEliminado()) {
					this.listDocumentosRespuesta.add(de.getDocumento());
				}
				// TODO CUAC
//				if (((de.getDocumento().getAutor().getId().equals(usuario
//						.getId())) || (de.getDocumento().getEmisor() != null
//						&& de.getDocumento().getIdEmisor() != null && de
//						.getDocumento().getIdEmisor().equals(usuario.getId())))
//						&& (de.getDocumento().getFirmas() == null || (de
//								.getDocumento().getFirmas() != null && de
//								.getDocumento().getFirmas().size() == 0))) {
//					de.getDocumento().setEliminable(true);
//				} else {
//					de.getDocumento().setEliminable(false);
//				}
				de.getDocumento().setEliminable(md.isEliminable(de.getDocumento(), expediente, usuario));
			} else if (de.getTipoDocumentoExpediente().getId()
					.equals(TipoDocumentoExpediente.ORIGINAL)) {
				this.documentoOriginal = de.getDocumento();
				this.documentoOriginal
						.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
								TipoDocumentoExpediente.ORIGINAL));
				if (this.documentoOriginal instanceof DocumentoElectronico) {
					((DocumentoElectronico) documentoOriginal).getParrafos()
							.size();
					if (this.documentoOriginal instanceof Providencia) {
						((Providencia) documentoOriginal).getAcciones().size();
					}
					if (this.documentoOriginal instanceof Resolucion) {
						((Resolucion) documentoOriginal).getArchivosAdjuntos()
								.size();
//						((Resolucion) documentoOriginal).getResoluciones().size();
					}
					if (this.documentoOriginal instanceof Decreto) {
						((Decreto) documentoOriginal).getArchivosAdjuntos()
								.size();
					}
				}
				if (documentoOriginal.getDistribucionDocumento() != null) {
					documentoOriginal.getDistribucionDocumento().size();
				}
				if (documentoOriginal.getRevisar() != null) {
					documentoOriginal.getRevisar().size();
				}
				if (documentoOriginal.getVisaciones() != null) {
					documentoOriginal.getVisaciones().size();
				}
				if (documentoOriginal.getFirmas() != null) {
					documentoOriginal.getFirmas().size();
				}
				if (documentoOriginal.getBitacoras() != null) {
					documentoOriginal.getBitacoras().size();
				}

				if (this.documentoOriginal instanceof DocumentoBinario) {
					if (((DocumentoBinario) documentoOriginal)
							.getArchivosAdjuntos() != null)
						((DocumentoBinario) documentoOriginal)
								.getArchivosAdjuntos().size();
				}
			}
			if (de.getEnEdicion() != null && de.getEnEdicion()) {
				de.getDocumento().setEnEdicion(true);
			} else {
				de.getDocumento().setEnEdicion(false);
			}
			de.getDocumento().setIdNuevoDocumento(contadorDocumentos++);
			de.getDocumento().getRevisar().size();
			de.getDocumento().getFirmas().size();
			de.getDocumento().getVisaciones().size();
			de.getDocumento().getFirmasEstructuradas().size();
			de.getDocumento().getVisacionesEstructuradas().size();
			de.getDocumento().getRevisarEstructuradas().size();
			de.getDocumento().getBitacoras().size();

			if (de.getDocumento() instanceof DocumentoBinario) {
				if (((DocumentoBinario) de.getDocumento())
						.getArchivosAdjuntos() != null)
					((DocumentoBinario) de.getDocumento())
							.getArchivosAdjuntos().size();
			}
		}
		
		
		//listGrupoUsuarios = new ArrayList<GrupoUsuario>(0);
		this.buscarDestinatarios();

		if (expediente.getObservaciones() != null) {
			observaciones.addAll(expediente.getObservaciones());
			java.util.Collections.sort(observaciones);
		}
		this.obtenerDestinatariosDocumentoOriginal();
	}

	private void obtenerDestinatariosDocumentoOriginal() {
		if (this.documentoOriginal.getTipoDocumento() != null
				&& (this.documentoOriginal.getTipoDocumento().getId()
						.equals(TipoDocumento.RESOLUCION))) {
			if (this.expediente.getDestinatario() != null) {
				destinatariosDocumentoOriginal.add(new SelectItem(
						destinatariosDocumentoOriginal.size(), this.expediente
								.getDestinatario().getNombreApellido()));
			} else {
				if (this.expediente.getDestinatarioHistorico() != null) {
					destinatariosDocumentoOriginal.add(new SelectItem(
							destinatariosDocumentoOriginal.size(),
							this.expediente.getDestinatarioHistorico()
									.getNombreApellido()));
				}
			}
		} else {
			if (this.documentoOriginal.getDestinatarios() != null) {
				for (ListaPersonasDocumento dd : this.documentoOriginal
						.getDestinatarios()) {
					if (dd instanceof DestinatarioDocumento) {
						destinatariosDocumentoOriginal.add(new SelectItem(
								destinatariosDocumentoOriginal.size(), dd
										.getDestinatario()));
					}
				}
			}
		}
		if (this.documentoOriginal.getFormatoDocumento().getId()
				.equals(FormatoDocumento.PAPEL)) {
			destinatariosDocumentoOriginal.clear();
			try {
				for (Persona dd : this.listDestinatariosOriginal) {
					if (dd instanceof Persona) {
						destinatariosDocumentoOriginal.add(new SelectItem(
								destinatariosDocumentoOriginal.size(), dd
										.getNombres()
										+ " "
										+ dd.getApellidoPaterno()
										+ " "
										+ dd.getApellidoMaterno()));
					}
				}
			} catch (Exception e) {
			}
		}
		if (this.documentoOriginal.getFormatoDocumento().getId()
				.equals(FormatoDocumento.DIGITAL)) {
			destinatariosDocumentoOriginal.clear();
			try {
				for (Persona dd : this.listDestinatariosOriginal) {
					if (dd instanceof Persona) {
						destinatariosDocumentoOriginal.add(new SelectItem(
								destinatariosDocumentoOriginal.size(), dd
										.getNombres()
										+ " "
										+ dd.getApellidoPaterno()
										+ " "
										+ dd.getApellidoMaterno()));
					}
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Metodo que guarda un expediente.
	 * 
	 * @throws DocumentNotUploadedException
	 *             documento no Upload.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void guardarExpediente() throws DocumentNotUploadedException {

		long start = System.currentTimeMillis();

		expediente = new Expediente();
		expediente.setFechaIngreso(new Date());
		expediente.setEmisor(usuario);
		me.crearExpediente(expediente);

		try {
			me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
		} catch (DocumentNotUploadedException e) {
			// Borrado de id's generados en memoria.
			documentoOriginal.setId(null);
			for (ListaPersonasDocumento lpd : documentoOriginal
					.getListaPersonas()) {
				lpd.setId(null);
			}
			expediente = null;
			if (FormatoDocumento.DIGITAL.equals(documentoOriginal
					.getFormatoDocumento().getId())) {
				((DocumentoBinario) documentoOriginal).getArchivo().setId(null);
			}
			throw new DocumentNotUploadedException();
		}
		documentoOriginal.setEnEdicion(true);

		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getId() == null) {
				me.agregarRespuestaAExpediente(expediente, doc, true);
			} else {
				md.actualizarDocumento(doc);
			}

			doc.setEnEdicion(true);
		}

		final Date fechaIngreso = new Date();
		idExpedientes.clear();
		for (Persona de : listDestinatarios) {
			idExpedientes.add(me.agregarDestinatarioAExpediente(expediente,
					usuario, de, fechaIngreso));
		}

		this.obtenerDestinatariosDocumentoOriginal();

		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Expediente",
				System.currentTimeMillis() - start);

	}

	private void buscarDestinatarios() {
		listGrupoUsuarios = new ArrayList<GrupoUsuario>();
		listGrupoUsuariosTmp = new ArrayList<GrupoUsuario>();
		listDestinatarios = new HashSet<Persona>();
		listDestinatariosTmp = new HashSet<Persona>();
		List<Expediente> expedientes = new ArrayList<Expediente>();
//		if(this.expediente.getVersionPadre() == null) expedientes = me.buscarExpediente(this.expediente, false);
//		else  expedientes = me.buscarExpediente(this.expediente.getVersionPadre(), false);
		expedientes = me.buscarExpedientesHijos(this.expediente);
		for (Expediente e : expedientes) {
			try {
//				if (e.getEliminado() == null) {
//					e.setEliminado(false);
//				}
//				if (!e.getEliminado()) {
					if (e.getGrupo() != null) {
						if (e.getFechaDespacho() == null) {
							e.getGrupo().setBorrable(true);
						}
						listGrupoUsuarios.add(e.getGrupo());
					} else {
						if (e.getDestinatario() != null) {
							if (e.getFechaDespacho() == null) {
								e.getDestinatario().setBorrable(true);
							}
							e.getDestinatario().setDestinatarioConCopia(
									e.getCopia());
							listDestinatarios.add(e.getDestinatario());
						} else {
//							if (e.getFechaDespacho() != null) {
//								e.getDestinatarioHistorico().setBorrable(true);
//							}
							e.getDestinatarioHistorico()
									.setDestinatarioConCopia(e.getCopia());
							listDestinatarios.add(e.getDestinatarioHistorico());
						}
					}
					if (listGrupoUsuarios.size() > 0) {
						listGrupoUsuariosTmp.addAll(listGrupoUsuarios);
					}
					if (listDestinatarios.size() > 0) {
						listDestinatariosTmp.addAll(listDestinatarios);
					}
//				}
			} catch (Exception e1) {
				log.info(e1.getMessage());
			}
		}
	}
	
	
	public void guardarHistorialDespacho(Expediente e){
		HistorialDespacho guardar = new HistorialDespacho();
		guardar.setExpediente(e);
		guardar.setEmisor(e.getEmisor());
		guardar.setDestinatario(e.getDestinatario());
		guardar.setFechaDespacho(e.getFechaDespacho());
	//	adminDpacho.guardarHistorial(guardar);
		em.persist(guardar);
		
	}
	@Override
	public String despacharExpediente() throws DocumentNotUploadedException {
		if (listDestinatarios.size() > 0 || listGrupoUsuarios.size() > 0) {
			long start = System.currentTimeMillis();
			this.guardarExpediente(true);
			me.despacharExpediente(expediente);
			//final List<Expediente> expedientes = new ArrayList<Expediente>();
			for (Long id : idExpedientes) {
				Expediente e = null;
				e = me.buscarExpediente(id);
				log.info("Expediente despachando: {0} {1}", e.getId(),e.getNumeroExpediente());
				if(e != null){
					me.actualizarDocumentoExpediente(e);
					me.despacharExpediente(e);
					this.guardarHistorialDespacho(e);
					//expedientes.add(e);
				}
			}

			
			// TODO DEPRECATED TRACK!!!!
			//me.despacharNotificacionPorEmail(expedientes);
			observaciones = null;
			desdeDespacho = true;

			LogTiempos.mostrarLog(this.usuario.getUsuario(),
					"Despachar Expediente", System.currentTimeMillis() - start);

			return BANDEJA_SALIDA;
		} else {
			FacesMessages.instance().add(
					"Debe Seleccionar un Destinatario del Expediente");
			return "";
		}
	}

	private Set<Persona> getDestinatarios() {

		final Set<Persona> personasSet = new HashSet<Persona>();
		final List<Expediente> expedientes;
		
		if(expediente.getVersionPadre()==null){
			
			 expedientes = me.buscarExpedientesHijos(
					this.expediente);
		}else{			
				 expedientes = me.buscarExpedientesHijos(
						this.expediente.getVersionPadre());
		}
			
			for (Expediente e : expedientes) {
			if (e.getDestinatario() != null) {

				e.getDestinatario().setDestinatarioConCopia(e.getCopia());
				personasSet.add(e.getDestinatario());
			} else {
				if (e.getDestinatarioHistorico() != null) {
					e.getDestinatarioHistorico().setDestinatarioConCopia(
							e.getCopia());
					personasSet.add(e.getDestinatarioHistorico());
				}
			}
		}

		return personasSet;
	}

	@Override
	public String reenviarExpediente() throws DocumentNotUploadedException {

		// Se puede realizar el reenvio del expediente cuando
		// el estado es despachado

		// Debe haber destinatarios
		if (listDestinatarios.size() > 0 || listGrupoUsuarios.size() > 0) {

			final Date fechaIngreso = new Date();

			// Los destinatarios nuevos a los cuales se reenviara
			Set<Persona> listDestinatariosNuevos = new HashSet<Persona>();
			List<GrupoUsuario> listGrupoUsuariosNuevos = new ArrayList<GrupoUsuario>();

			// listDestinatarios contiene todos, los nuevos y los "previos", se
			// necesita saber los nuevos

			for (Persona de1 : listDestinatarios) {
				boolean found = false;
				for (Persona de2 : listDestinatariosTmp) {
					if (de1.getId().equals(de2.getId())) {
						found = true;
					}
				}
				if (!found) {
					listDestinatariosNuevos.add(de1);
				}
			}
			for (GrupoUsuario gu1 : listGrupoUsuarios) {
				boolean found = false;
				for (GrupoUsuario gu2 : listGrupoUsuariosTmp) {
					if (gu1.getId().equals(gu2.getId())) {
						found = true;
					}
				}
				if (!found) {
					listGrupoUsuariosNuevos.add(gu1);
				}
			}
			
			if (listDestinatariosNuevos.size() == 0 && listGrupoUsuariosNuevos.size() == 0) {
				FacesMessages.instance().add("Debe Seleccionar un Destinatario del Expediente para reenviar");
				return "";
			}

			for (GrupoUsuario grupo : listGrupoUsuariosNuevos) {
				try {
					idExpedientes.add(me.agregarDestinatarioAExpediente(
							expediente, usuario, grupo, fechaIngreso));
				} catch (DocumentNotUploadedException e) {
					e.printStackTrace();
				}
			}

			for (Persona de : listDestinatariosNuevos) {
				try {
					idExpedientes.add(me.agregarDestinatarioAExpediente(
							expediente, usuario, de, fechaIngreso));
				} catch (DocumentNotUploadedException e) {
					e.printStackTrace();
				}
			}
			
			final List<Expediente> expedientes = new ArrayList<Expediente>();
			
//			if(listDestinatariosNuevos.size() == 0){
//				for (Long id : idExpedientes) {
//					final Expediente e = me.buscarExpediente(id);
//					for (Persona de : listDestinatariosNuevos){
//						if(de.getId().equals(e.getDestinatario().getId())){
//							FacesMessages.instance().add("No se puede Reenviar un expediente a un mismo destinatario");
//							return "";
//						}
//					}	
//				}
//			}
			
			if(idExpedientes.size() > 0){
			
				for (Long id : idExpedientes) {
					final Expediente e = me.buscarExpediente(id);
					e.setReenvio(true);
					me.despacharExpediente(e);
					HistorialDespacho guardar = new HistorialDespacho();
					guardar.setExpediente(e);
					guardar.setEmisor(e.getEmisor());
					guardar.setDestinatario(e.getDestinatario());
					guardar.setFechaDespacho(e.getFechaDespacho());
					//adminDpacho.guardarHistorial(guardar);
					em.persist(guardar);
					expedientes.add(e);
				}
				//innecesario
				//me.despacharExpediente(expediente);
				//deprecated
				//if(expedientes.size() > 0){
				//	me.despacharNotificacionPorEmail(expedientes);
				//}
				FacesMessages.instance().add(EXPEDIENTE_DESPACHADO);
				desdeDespacho = true;
				return BANDEJA_SALIDA;
			} else {
				FacesMessages.instance().add("No se puede Reenviar un expediente a un mismo destinatario");
				return "";
			}
		} else {
			FacesMessages
					.instance()
					.add("Debe Seleccionar un Destinatario del Expediente para reenviar");
			return "";
		}
	}

	public void registrarExpediente() throws DocumentNotUploadedException {
		this.guardarExpediente(false);
		idExpediente = expediente.getId();
	}
	
	private void guardarExpediente(final boolean despachar)
			throws DocumentNotUploadedException {
		long start = System.currentTimeMillis();
		
		expediente.setObservaciones(null);

		me.modificarExpediente(expediente);

		for (Documento doc : listDocumentosEliminados) {
			md.eliminarDocumento(doc.getId(), false);
		}
		
		List<Expediente> expedientes = me.buscarExpedientesHijos(this.expediente);
	    if (expedientes != null) {
	      Iterator<Expediente> itExpedientes = expedientes.iterator();
	      while (itExpedientes.hasNext()) {
	        Expediente i = itExpedientes.next();
	        me.eliminarExpediente(i.getId());
	      }
	    }

		this.guardarObservaciones();

		final Date fechaIngreso = new Date();
		idExpedientes.clear();
				
		for (Persona de : listDestinatarios) {
			idExpedientes.add(me.agregarDestinatarioAExpediente(expediente,
					usuario, de, fechaIngreso));
		}

		for (GrupoUsuario grupo : listGrupoUsuarios) {
			idExpedientes.add(me.agregarDestinatarioAExpediente(expediente,
					usuario, grupo, fechaIngreso));
		}
		
		
		if (!despachar) {
			FacesMessages.instance().add("Expediente Guardado");
		} else {
			FacesMessages.instance().add(EXPEDIENTE_DESPACHADO);
		}

		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Guardar Expediente",
				System.currentTimeMillis() - start);
	}

	// private Long buscaDocumentoReferencia(Integer idDocumento) {
	// Long id = null;
	// for (Documento doc : listDocumentosRespuesta) {
	// if (doc.getIdNuevoDocumento().equals(idDocumento)) {
	// id = doc.getId();
	// break;
	// }
	// }
	// if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
	// id = documentoOriginal.getId();
	// }
	// return id;
	// }

	public void agregarObservacion() {
		observacion = observacion.trim();
		if (!"".equals(observacion)) {
			final Observacion obs = new Observacion();
			if (observacion.length() > ObservacionValidator.LARGO_MAXIMO) {
				obs.setObservacion(observacion.substring(0,
						ObservacionValidator.LARGO_MAXIMO));
			} else {
				obs.setObservacion(observacion);
			}
			if (!observaciones.contains(obs)) {
				final Date hoy = new Date();
				obs.setExpediente(this.expediente);
				obs.setAutor(usuario);
				obs.setFecha(hoy);
				obs.setIdNuevaObservacion(contadorObservaciones++);
				if (archivoObs != null) {
					archivoObs.setFecha(hoy);
					archivoObs.setIdNuevoArchivo(contadorObservaciones++);
					obs.setObservacionArchivo(archivoObs);
				}
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_NORMAL));
				this.observaciones.add(obs);
			}
			this.observacion = null;
			this.archivoObs = null;
			java.util.Collections.sort(observaciones);
		} else {
			FacesMessages.instance().add("El campo de observaciones esta vacio");
		}
	}

	private void guardarObservaciones() {
		for (Observacion obs : observaciones) {
			if (obs.getId() == null) {
				em.persist(obs);

				final ObservacionArchivo oa = obs.getObservacionArchivo();
				if (oa != null && oa.getNombreArchivo() != null) {
					if (oa.getId() == null)
						em.persist(oa);
					final String nombreArchivoObservacion = getNombreArchivo(
							oa.getNombreArchivo(), oa.getId().toString());
//					final String cmsId = repositorio.almacenarArchivo(
//							nombreArchivoObservacion, oa.getArchivo(),
//							oa.getContentType());
					Documento d = new Documento();
					d.setAutor(usuario);
					d.setFormatoDocumento(new FormatoDocumento(0));
					String cmsId = repositorio.almacenarArchivo(nombreArchivoObservacion, oa, d);
					obs.getObservacionArchivo().setCmsId(cmsId);
					em.merge(oa);
				}
			}
			em.merge(obs);
		}
		expediente.setObservaciones(observaciones);
	}

	private String getNombreArchivo(final String nombreArchivo, final String id) {
		String extension = "";
		final StringTokenizer st = new StringTokenizer(nombreArchivo, ".");
		while (st.hasMoreElements()) {
			extension = st.nextToken();
		}
		final String nombre = id + "." + extension.toLowerCase();
		return nombre;
	}

	public String verNombreArchivoObservacion(final Observacion obs) {
		try {
			final ObservacionArchivo obsArch = (ObservacionArchivo) em
					.createNamedQuery("ObservacionArchivo.findByObs")
					.setParameter("id", obs.getId()).getSingleResult();
			return obsArch.getNombreArchivo();
		} catch (javax.persistence.NoResultException nre) {
			return "";
		}
	}

	public void verArchivoObservacion(final Archivo a) {
		// buscarArchivoRepositorio(a);
		this.verArchivo(a.getId());
	}

	public String archivarExpediente() throws DocumentNotUploadedException {

		long start = System.currentTimeMillis();

		this.registrarExpediente();
		this.expediente.setArchivado(new Date());
		this.expediente.setDestinatarioHistorico(usuario);
		me.modificarExpediente(this.expediente);
		desdeDespacho = true;

		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Archivar Expediente",
				System.currentTimeMillis() - start);

		return BANDEJA_SALIDA;
	}
	
	public void verArchivo() {
		final Archivo archivo = ((DocumentoBinario) documentoOriginal)
				.getArchivo();
		// buscarArchivoRepositorio(archivo);
		this.verArchivo(archivo.getId());
		docValido = false;
	}

	public void verArchivo(final Long idArchivo) {
		/*
		 * ArchivoDocumentoBinario a = em.find(ArchivoDocumentoBinario.class,
		 * idArchivo); buscarArchivoRepositorio(a);
		 */
		try {
			if (!DocUtils.getFile(idArchivo, em)) {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ idArchivo + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void buscarArchivoRepositorio(final Archivo a) {
		// byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		/*
		 * byte[] data = repositorio.getFile(a.getCmsId()); if (data != null) {
		 * FacesContext facesContext = FacesContext.getCurrentInstance(); try {
		 * HttpServletResponse response = (HttpServletResponse)
		 * facesContext.getExternalContext().getResponse();
		 * response.setContentType(a.getContentType());
		 * response.setHeader("Content-disposition", "attachment; filename=\"" +
		 * a.getNombreArchivo() + "\""); response.getOutputStream().write(data);
		 * response.getOutputStream().flush(); facesContext.responseComplete();
		 * } catch (IOException e) { e.printStackTrace(); } } else {
		 * FacesMessages.instance().add(
		 * "No existe el archivo, consulte con el administrador. (Codigo Archivo: "
		 * + a.getId() + ")"); }
		 */
	}

	public void getDocumentoOriginalBinario() {
		documentoOriginal = em.find(DocumentoBinario.class,
				documentoOriginal.getId());
		log.info("Es nulo? : " + documentoOriginal == null);
	}

	public String end() {
		log.info("ending conversation: modificarExpediente");
		observaciones = null;
		desdeDespacho = true;
		return BANDEJA_SALIDA;
	}

	@Begin(join = true)
	public String adjuntarDocumento(final int tipoAdjuntar) {
		destinatariosDocumento = null;
		distribucionDocumento = null;

		final int tipo = tipoDocumentoAdjuntar.intValue();
		tipoDocumentoAdjuntar = 0;

		if (tipo == JerarquiasLocal.INICIO.intValue()) {
			return "";
		}

		switch (tipo) {
		case 1:
			documento = new Oficio();
			homeDistribucion = "crearOficioOrdinario";
			break;
		case 2:
			documento = new Oficio();
			homeDistribucion = "crearOficioCircular";
			break;
		case 3:
			documento = new Oficio();
			homeDistribucion = "crearOficioReservado";
			break;
		case 6:
			documento = new Resolucion();
			final Vistos parrafo1 = new Vistos();
			parrafo1.setCuerpo("");
			parrafo1.setDocumento((Resolucion) documento);
			parrafo1.setNumero(1L);
			final Considerandos parrafo2 = new Considerandos();
			parrafo2.setCuerpo("");
			parrafo2.setDocumento((Resolucion) documento);
			parrafo2.setNumero(1L);
			final Resuelvo parrafo3 = new Resuelvo();
			parrafo3.setCuerpo("");
			parrafo3.setDocumento((Resolucion) documento);
			parrafo3.setNumero(1L);
			final List<Parrafo> parrafos1 = new ArrayList<Parrafo>();
			parrafos1.add(parrafo1);
			final List<Parrafo> parrafos2 = new ArrayList<Parrafo>();
			parrafos2.add(parrafo2);
			final List<Parrafo> parrafos3 = new ArrayList<Parrafo>();
			parrafos3.add(parrafo3);
			((Resolucion) documento).setVistos(parrafos1);
			((Resolucion) documento).setConsiderandos(parrafos2);
			((Resolucion) documento).setResuelvo(parrafos3);
			homeDistribucion = "crearResolucion";
			break;
		case 7:
			documento = new Memorandum();
			homeDistribucion = "crearMemorandum";
			break;
		case 8:
			documento = new Providencia();
			final List<AccionProvidencia> acciones = new ArrayList<AccionProvidencia>();
			acciones.add(new AccionProvidencia(-1));
			((Providencia) documento).setAcciones(acciones);
			homeDistribucion = "crearProvidencia";
			break;
		case 9:
			documento = new Decreto();
			final Vistos parrafo1Decreto = new Vistos();
			parrafo1Decreto.setCuerpo("");
			parrafo1Decreto.setDocumento((Decreto) documento);
			parrafo1Decreto.setNumero(1L);
			final Considerandos parrafo2Decreto = new Considerandos();
			parrafo2Decreto.setCuerpo("");
			parrafo2Decreto.setDocumento((Decreto) documento);
			parrafo2Decreto.setNumero(1L);
			final Resuelvo parrafo3Decreto = new Resuelvo();
			parrafo3Decreto.setCuerpo("");
			parrafo3Decreto.setDocumento((Decreto) documento);
			parrafo3Decreto.setNumero(1L);
			final List<Parrafo> parrafos1Decreto = new ArrayList<Parrafo>();
			parrafos1Decreto.add(parrafo1Decreto);
			final List<Parrafo> parrafos2Decreto = new ArrayList<Parrafo>();
			parrafos2Decreto.add(parrafo2Decreto);
			final List<Parrafo> parrafos3Decreto = new ArrayList<Parrafo>();
			parrafos3Decreto.add(parrafo3Decreto);
			((Decreto) documento).setVistos(parrafos1Decreto);
			((Decreto) documento).setConsiderandos(parrafos2Decreto);
			((Decreto) documento).setResuelvo(parrafos3Decreto);
			homeDistribucion = "crearDecreto";
			break;
		case 10:
			documento = new Carta();
			homeDistribucion = "crearCarta";
			break;
		// case 11:
		// documento = new Factura();
		// homeDistribucion = "crearFactura";
		// break;
		 case 15:
			 documento = new DiaAdministrativo();
			 ((DiaAdministrativo)documento).setListDetalleDias(new ArrayList<DetalleDias>());
			 homeDistribucion = "crearSolDiaAdministrativo";
			 break;
		 case 16:
			 documento = new Vacaciones();
			 homeDistribucion = "crearSolVacaciones";
			 break;
		// case 17:
		// documento = new Adquisicion();
		// ((Adquisicion) documento).setTipoAdquisicion(new TipoAdquisicion(1));
		// ((Adquisicion) documento).setTipoFinanciamientoAdquisicion(new
		// TipoFinanciamientoAdquisicion(1));
		// ((Adquisicion) documento).setConvenioMarco(false);
		// ((Adquisicion) documento).setLicitacionPublica(false);
		// ((Adquisicion) documento).setLicitacionPrivada(true);
		// ((Adquisicion) documento).setTratoDirecto(false);
		// ((Adquisicion) documento).setAsistentesEventoAdquisicion(new
		// ArrayList<AsistenteEventoAdquisicion>());
		// homeDistribucion = "crearSolicitudAdquisicion";
		// break;
		// case 22:
		// documento = new Convenio();
		// homeDistribucion = "crearConvenio";
		// break;
		// case 23:
		// documento = new Contrato();
		// homeDistribucion = "crearContrato";
		// break;
		// case 24:
		// ((ActaAdjudicacion) documento).setComisionActaAdjudicacion(new
		// ArrayList<ComisionActaAdjudicacion>());
		// documento = new ActaAdjudicacion();
		// homeDistribucion = "crearActaAdjudicacion";
		// break;
		case 98:
			documento = new AnulacionResolucion();
			homeDistribucion = CrearAnulacionResolucionBean.NAME;
			break;
		case 99:
			documento = new SolicitudFolio();
			homeDistribucion = CrearSolicitudFolioBean.NAME;
			break;
		case 100:
			documento = new DocumentoPapel();
			homeDistribucion = "crearDocumentoPapel";
			break;
		case 200:
			documento = new DocumentoBinario();
			homeDistribucion = "crearDocumentoBinario";
			break;
		default:
			break;
		}

		final Calendar ahora = new GregorianCalendar();

		if (tipo == 11 || tipo == 15 || tipo == 16 || tipo == 17 || tipo == 100 || tipo == 200) {
			documento.setFechaDocumentoOrigen(ahora.getTime());
		}

		documento.setFechaCreacion(ahora.getTime());
		ahora.add(Calendar.DATE, 5);
		//documento.setPlazo(ahora.getTime());

		this.setEmisorActual(documento);
		documento.setIdNuevoDocumento(this.contadorDocumentos++);
		documento.setAutor(usuario);
		if (tipoAdjuntar == 1) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
					TipoDocumentoExpediente.RESPUESTA));
		} /*
		 * else if (tipoAdjuntar == 2) {
		 * documento.setTipoDocumentoExpediente(new
		 * TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO)); }
		 */
		documento.setReservado(false);
		documento.setEnEdicion(true);
		return homeDistribucion;
	}

	private void setEmisorActual(final Documento documento) {
		if (!(documento instanceof Providencia)) {
			this.setNombreEmisorActual(documento, this.usuario);
		} else {
			if (jerarquias.esJefe(this.usuario)) {
				this.setNombreEmisorActual(documento, this.usuario);
			} else {
				final List<Persona> jefes = jerarquias.getJefe(this.usuario);
				this.setNombreEmisorActual(documento, jefes.get(0));
			}
		}
	}

	private void setNombreEmisorActual(final Documento documento,
			final Persona persona) {
		if (persona != null) {
			String value = persona.getNombres() + " "
					+ persona.getApellidoPaterno();
			final Cargo cargo = persona.getCargo();
			value += " - " + cargo.getDescripcion();
			final UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
			value += " - " + uo.getDescripcion();
			documento.setEmisor(value);
			documento.setIdEmisor(persona.getId());
		}
	}

	private void getDocumentoDeLista(final List<Documento> lista,
			final Integer numDoc) {
		for (final Iterator<Documento> doc = lista.iterator(); doc.hasNext();) {
			final Documento docTmp = doc.next();
			final Integer id = docTmp.getIdNuevoDocumento();
			if (id.equals(numDoc)) {
				documento = docTmp;
				break;
			}
		}
	}

	@Begin(join = true)
	public String modificarDocumento(final Integer numDoc, final Integer lista) {

		//listDestinatariosTmp = listDestinatarios;

		if (lista.equals(1)) {
			this.getDocumentoDeLista(listDocumentosRespuesta, numDoc);
		}/*
		 * else if (lista.equals(2)) { getDocumentoDeLista(listDocumentosAnexo,
		 * numDoc); }
		 */else if (lista.equals(3)) {
			documento = documentoOriginal;
		}
		String homeDistribucion = "";
		final TipoDocumentoExpediente t = documento.getTipoDocumentoExpediente();
		final Integer id = documento.getIdNuevoDocumento();
		//final boolean enEdicion = documento.getEnEdicion();
		Boolean enEdicion = Boolean.FALSE;
		if (expediente.getFechaDespacho() == null && documento.getAutor().equals(usuario))
			enEdicion = Boolean.TRUE;
		if (documento != null && documento.getId() != null)
			documento = em.find(Documento.class, documento.getId());

		if (documento instanceof DocumentoElectronico) {
			if (documento.getId() != null) {
				try {
					documento = em.find(Documento.class, documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getRevisar().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento).getArchivosAdjuntos()
							.size();
//					if (documento instanceof Resolucion) {
//						((Resolucion) documento).getResoluciones().size();
//					}
					// if (documento instanceof DiaAdministrativo) {
					// ((DiaAdministrativo)
					// documento).getListDetalleDias().size();
					// }
				} catch (Exception e) {
					documento = em.find(DocumentoElectronico.class,
							documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getRevisar().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
//					if (documento instanceof Resolucion) {
//						((Resolucion) documento).getResoluciones().size();
//					}
					// if (documento instanceof DiaAdministrativo) {
					// ((DiaAdministrativo)
					// documento).getListDetalleDias().size();
					// }
				}
			}
			switch (documento.getTipoDocumento().getId()) {
			case 1:
				homeDistribucion = "crearOficioOrdinario";
				break;
			case 2:
				homeDistribucion = "crearOficioCircular";
				break;
			case 3:
				homeDistribucion = "crearOficioReservado";
				break;
			case 6:
				homeDistribucion = "crearResolucion";
				break;
			case 7:
				homeDistribucion = "crearMemorandum";
				break;
			case 8:
				if (documento.getId() != null) {
					try {
						((Providencia) documento).getAcciones().size();
					} catch (Exception e) {
						documento = em.find(Providencia.class,
								documento.getId());
						((Providencia) documento).getAcciones().size();
					}
				}
				homeDistribucion = "crearProvidencia";
				break;
			case 9:
				homeDistribucion = "crearDecreto";
				break;
			case 10:
				homeDistribucion = "crearCarta";
				break;
			 case 15:
//				 solicitante = null;
//				 calidadJuridica = "";
//				 grado = "";
//				 diasAdministrativosPendientes = null;
//				 datosObtenidos = null;
				 if (((DiaAdministrativo)documento).getListDetalleDias() != null)
					((DiaAdministrativo)documento).getListDetalleDias().size();
				 else
					 ((DiaAdministrativo)documento).setListDetalleDias(new ArrayList<DetalleDias>()); 
				 homeDistribucion = "crearSolDiaAdministrativo";
				 break;
			 case 16:
//				 solicitante = null;
//				 calidadJuridica = "";
//				 grado = "";
//				 diasVacacionesPendientes = null;
//				 datosObtenidos = null;
				 if (((Vacaciones)documento).getSolicitudVacaciones() != null)
					 if (((Vacaciones)documento).getSolicitudVacaciones().getListDetalleDiasVacaciones() != null)
						 ((Vacaciones)documento).getSolicitudVacaciones().getListDetalleDiasVacaciones().size();
				 homeDistribucion = "crearSolVacaciones";
				 break;
			// case 17:
			// ((Adquisicion) documento).getCriteriosEvaluacion().size();
			// ((Adquisicion)
			// documento).getAsistentesEventoAdquisicion().size();
			// vistoDesdeReporte = null;
			// homeDistribucion = "crearSolicitudAdquisicion";
			// break;
			case 20:
				consulta = documento;
				homeDistribucion = "verDocumentoConsulta";
				break;
			case 21:
				respuesta = documento;
				homeDistribucion = "crearRespuestaDocumentoConsulta";
				break;
			case 22:
				homeDistribucion = "crearConvenio";
				break;
			case 23:
				homeDistribucion = "crearContrato";
				break;
			// case 24:
			// ((ActaAdjudicacion)
			// documento).getComisionActaAdjudicacion().size();
			// homeDistribucion = "crearActaAdjudicacion";
			// break;
			case 98:
				homeDistribucion = CrearAnulacionResolucionBean.NAME;
				break;
			case 99:
				homeDistribucion = CrearSolicitudFolioBean.NAME;
				break;
			default:
				break;
			}
		} else {
			if (documento instanceof DocumentoPapel) {
				homeDistribucion = "crearDocumentoPapel";
				// } else if (documento instanceof Factura) {
				// boolean debeIngresarPago = false;
				// boolean debeAprobarPago = false;
				// Persona u = em.find(Persona.class, usuario.getId());
				// for (Rol rol : u.getRoles()) {
				// if (rol.getId().equals(Rol.GESTIONAR_PAGO)) {
				// debeIngresarPago = true;
				// }
				// if (rol.getId().equals(Rol.APROBAR_PAGO)) {
				// debeAprobarPago = true;
				// }
				// }
				//
				// // el orden de estos if es importante
				// homeDistribucion = "visualizarFactura";
				// if (debeIngresarPago)
				// homeDistribucion = "ingresarDatosFactura";
				// if (debeAprobarPago)
				// homeDistribucion = "visualizarDataDeFactura";

			} else if (documento instanceof DocumentoBinario) {
				try {
					((DocumentoBinario) documento).getArchivosAdjuntos().size();
				} catch (Exception e) {
					documento = em.find(DocumentoBinario.class,
							documento.getId());
					((DocumentoBinario) documento).getArchivosAdjuntos().size();
				}
				homeDistribucion = "crearDocumentoBinario";
			} else {
				homeDistribucion = "";
			}
		}

		vistoDesdeReporte = null;
		if (lista.equals(3)) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
					TipoDocumentoExpediente.ORIGINAL));
		} else {
			documento.setTipoDocumentoExpediente(t);
		}
		documento.setIdNuevoDocumento(id);
		documento.setEnEdicion(enEdicion);

		if (documento.getTipoDocumento().getId() != 6) {
			destinatariosDocumento = new ArrayList<SelectPersonas>();
			for (ListaPersonasDocumento lpd : this.documento.getDestinatarios()) {
				destinatariosDocumento.add(new SelectPersonas(lpd
						.getDestinatario(), lpd.getDestinatarioPersona()));
			}
		}
		distribucionDocumento = new ArrayList<SelectPersonas>();
//		for (ListaPersonasDocumento lpd : this.documento.getDistribucionDocumento()) {
//			distribucionDocumento.add(new SelectPersonas(lpd.getDestinatario(),
//					lpd.getDestinatarioPersona()));
//		}
		for (ListaPersonasDocumento lpd : this.documento.getDistribucionDocumento()) {
			SelectPersonas pers = new SelectPersonas(lpd.getDestinatario(),lpd.getDestinatarioPersona());
			if(!distribucionDocumento.contains(pers)){
				System.out.println("persona: "+pers.toString());
				distribucionDocumento.add(pers);
			}
		}
		//this.obtenerDestinatariosDocumentoOriginal();
		return homeDistribucion;
	}

	public void agregarArchivoOriginal() {
		if (documentoOriginal instanceof DocumentoPapel) {
			if (archivo != null) {
				final DocumentoBinario documentoNuevo = documentoOriginal
						.getDocumentoBinario();
				documentoNuevo.setFormatoDocumento(new FormatoDocumento(
						FormatoDocumento.DIGITAL));
				archivo.setFecha(new Date());
				documentoNuevo.setArchivo(archivo);
				md.cambiaOriginalBinario((DocumentoPapel) documentoOriginal,
						documentoNuevo);
				documentoOriginal = documentoNuevo;
			}
		}
	}

	public void listener(final UploadEvent event) throws Exception {
		archivo = new ArchivoDocumentoBinario();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
	}

	public void listenerObs(final UploadEvent event) throws Exception {
		archivoObs = new ObservacionArchivo();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivoObs.setNombreArchivo(fileName);
		archivoObs.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivoObs.setArchivo(data);
	}

	public void agregarAntecedenteArchivo() {
		if (archivo != null) {
			final Date hoy = new Date();
			final DocumentoBinario doc = new DocumentoBinario();
			doc.setMateria(this.materiaArchivo);
			doc.setAutor(usuario);
			doc.setFechaCreacion(hoy);
			doc.setIdNuevoDocumento(contadorDocumentos++);
			doc.setEnEdicion(true);
			doc.setReservado(false);
			archivo.setFecha(hoy);
			archivo.setIdNuevoArchivo(contadorDocumentos++);
			doc.setArchivo(archivo);

			// listDocumentosAnexo.add(doc);
			archivo = null;
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento dpto = departamentos.get(0);
			if (dpto.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	public void agregarDestinatario() {
		
		try {
			// por la actualizacion de ajax
			if (!cargo.equals(JerarquiasLocal.INICIO)) {
				if (persona.equals(JerarquiasLocal.TODOS)) {
					final Query query = em.createNamedQuery("Persona.findById");
					query.setParameter("id", this.cargo);
					this.listDestinatarios.addAll(query.getResultList());
				} else if (persona.equals(JerarquiasLocal.CUALQUIERA)) {
					final Query query = em
							.createQuery("select p from Persona p where p.cargo.id= ? ");
					query.setParameter(1, this.cargo);
					final List<Persona> personasListTmp = query.getResultList();
					for (Persona p : personasListTmp) {
						p.setDestinatarioConRecepcion(true);
						p.setBorrable(true);
						this.listDestinatarios.add(p);
					}
				} else {
					final Persona p = (Persona) em
							.createQuery(
									"select p from Persona p where p.id= :param")
							.setParameter("param", this.persona)
							.getSingleResult();

					if (documentoOriginal != null
							&& documentoOriginal.getTipoDocumento().getId()
									.equals(TipoDocumento.PROVIDENCIA)) {
						if (!this.esJefeDirecto(p, usuario)) {
							FacesMessages.instance().add("No es subalterno");
							listOrganizacion = this.buscarOrganizaciones();
							organizacion = (Long) listOrganizacion.get(1)
									.getValue();
							this.buscarDivisiones();
							division = (Long) listDivision.get(1).getValue();
							this.buscarDepartamentos();
							return;
						}
					}
					if (copiaDirectoFlag == 0) {
						p.setDestinatarioConCopia(false);
					} else {
						p.setDestinatarioConCopia(true);
					}
					p.setBorrable(true);
					this.listDestinatarios.add(p);
				}
			}

			if (!grupo.equals(JerarquiasLocal.INICIO)) {
				final Persona p = em.find(Persona.class, grupo);
				if (documentoOriginal != null
						&& documentoOriginal.getTipoDocumento().getId()
								.equals(TipoDocumento.PROVIDENCIA)) {
					if (!this.esJefeDirecto(p, usuario)) {
						FacesMessages.instance().add("No es subalterno");
						listOrganizacion = this.buscarOrganizaciones();
						organizacion = (Long) listOrganizacion.get(1)
								.getValue();
						this.buscarDivisiones();
						division = (Long) listDivision.get(1).getValue();
						this.buscarDepartamentos();
						return;
					}
				}
				if (copiaDirectoFlag == 0) {
					p.setDestinatarioConCopia(false);
				} else {
					p.setDestinatarioConCopia(true);
				}
				p.setBorrable(true);
				this.listDestinatarios.add(p);
			}
			// no se envia a el mismo el expediente.
			if (this.listDestinatarios.contains(usuario)) {
				this.listDestinatarios.remove(usuario);
			}

			listOrganizacion = this.buscarOrganizaciones();
			
			/*22-06-2012 Sergio: Se reactivan todos los niveles de ExeDoc*/
//			organizacion = (Long) listOrganizacion.get(1).getValue();
//			this.buscarDivisiones();
//			division = (Long) listDivision.get(1).getValue();
//			this.buscarDepartamentos();
//			listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//			this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			this.buscarUnidadesOrganizacionales();
			/**/
			
			listDestFrec = this.buscarDestFrec();
		} catch (Exception e) {
			log.info("cargando datos");
		}
		
	}

	private boolean esJefeDirecto(final Persona persona,
			final Persona posibleJefe) {
		boolean esSubalterno = false;

		if (!posibleJefe.getCargo().getUnidadOrganizacional().getVirtual()) {
			if (posibleJefe
					.getCargo()
					.getUnidadOrganizacional()
					.getId()
					.equals(persona.getCargo().getUnidadOrganizacional()
							.getId())) {
				esSubalterno = true;
				return esSubalterno;
			} else {
				return esSubalterno;
			}
		} else if (!posibleJefe.getCargo().getUnidadOrganizacional()
				.getDepartamento().getVirtual()) {
			if (posibleJefe
					.getCargo()
					.getUnidadOrganizacional()
					.getDepartamento()
					.getId()
					.equals(persona.getCargo().getUnidadOrganizacional()
							.getDepartamento().getId())) {
				esSubalterno = true;
				return esSubalterno;
			} else {
				return esSubalterno;
			}
		} else {
			if (posibleJefe
					.getCargo()
					.getUnidadOrganizacional()
					.getDepartamento()
					.getDivision()
					.getId()
					.equals(persona.getCargo().getUnidadOrganizacional()
							.getDepartamento().getDivision().getId())) {
				esSubalterno = true;
				return esSubalterno;
			} else {
				return esSubalterno;
			}
		}
	}

	public void agregarListaDestinatarios(final Long idLista) {
		final DestinatariosFrecuentes lista = em.find(
				DestinatariosFrecuentes.class, idLista);
		final List<Persona> destinatariosDesdeLista = lista
				.getDestinatariosFrecuentes();

		for (Persona destinatario : destinatariosDesdeLista) {
			if (!listDestinatarios.contains(destinatario)) {
				destinatario.setDestinatarioConCopia(false);
				destinatario.setBorrable(true);
				listDestinatarios.add(destinatario);
			}
		}
	}

	@End
	@Override
	public String cancelarExpediente() {

		long start = System.currentTimeMillis();

		me.cancelarExpediente(expediente);
		FacesMessages.instance().add("Expediente Cancelado");

		desdeDespacho = true;

		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Anular Expediente",
				System.currentTimeMillis() - start);

		return BANDEJA_SALIDA;
	}

	/**
	 * Limpiar Destinatarios.
	 */
	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		this.grupo = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarDocumentosAnexar() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		lista.add(new SelectItem(TipoDocumentosList.PAPEL,
				TipoDocumentosList.TXT_PAPEL));
		lista.add(new SelectItem(TipoDocumentosList.DIGITALIZADO,
				TipoDocumentosList.TXT_DIGITALIZADO));
		return lista;
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarDocumentosAdjuntar() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		lista.addAll(TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoRespuestaList());
		return lista;
	}

	public void eliminarDocumento(final Integer numDoc) {
		int contador = 0;
		for (Documento doc : listDocumentosRespuesta) {
			if (doc.getIdNuevoDocumento() != null
					&& doc.getIdNuevoDocumento().equals(numDoc)) {
				if (doc.getId() != null) {
					listDocumentosEliminados.add(doc);
				}
				listDocumentosRespuesta.remove(contador);
				break;
			}
			contador++;
		}
	}

	

	public void eliminarDestinatariosExpediente(final Persona destinatario) {

		listDestinatarios.remove(destinatario);
		// eliminar el expediente de la bandeja de salida del destinatario
//		final List<Expediente> desExpedientes = me
//				.obtenerExpedientesHijos(expediente);
//		for (int i = 0; i < desExpedientes.size(); i++) {
//			final Expediente arrayElement = desExpedientes.get(i);
//			if (!arrayElement.getAcuseRecibo()
//					&& arrayElement.getDestinatario().equals(destinatario)) {
//				arrayElement.setEliminado(true);
//				me.modificarExpediente(arrayElement);
//				// FacesMessages.instance().add("El expediente ya tiene acuse de Recibo");
//			}
//		}
//		me.modificarExpediente(this.expediente);

	}

	public void eliminarObservaciones(final Integer idNuevaObservacion) {
		for (Observacion o : observaciones) {
			final Integer id = o.getIdNuevaObservacion();
			if (id != null && id.equals(idNuevaObservacion)) {
				observaciones.remove(o);
				break;
			}
		}
	}

	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(final Expediente expediente) {
		this.expediente = expediente;
	}

	public Long getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(final Long idExpediente) {
		this.idExpediente = idExpediente;
	}

	public Object[] getListDestinatarios() {

//		if (listDestinatarios == null || listDestinatarios.size() == 0) {
//			this.buscarDestinatarios();
//		}

		if (listDestinatarios != null) {
			return listDestinatarios.toArray();
		} else {
			return new HashSet<Persona>().toArray();
		}
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	/*
	 * public List<Documento> getListDocumentosAnexo() { return
	 * listDocumentosAnexo; } public void setListDocumentosAnexo(List<Documento>
	 * listDocumentosAnexo) { this.listDocumentosAnexo = listDocumentosAnexo; }
	 */

	public List<Documento> getListDocumentosRespuesta() {
		return listDocumentosRespuesta;
	}
	
	public void setListDocumentosRespuesta(
			final List<Documento> listDocumentosRespuesta) {
		this.listDocumentosRespuesta = listDocumentosRespuesta;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(final String observacion) {
		this.observacion = observacion;
	}

	public void valueChangeObservacion(final ValueChangeEvent event) {
		final String a = (String) event.getNewValue();
		System.out.println("VALOR: " + a);
	}

	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(final List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}

	public List<SelectItem> getDestinatariosDocumentoOriginal() {
		this.setDestinatarioDocumentoOriginal();
		return destinatariosDocumentoOriginal;
	}

	public Integer getTipoDocumentoAdjuntar() {
		return tipoDocumentoAdjuntar;
	}

	public void setTipoDocumentoAdjuntar(final Integer tipoDocumentoAdjuntar) {
		this.tipoDocumentoAdjuntar = tipoDocumentoAdjuntar;
	}

	public List<SelectItem> getListaDocumentosAdjuntar() {

		if (this.listaDocumentosAdjuntar == null) {
			listaDocumentosAdjuntar = new ArrayList<SelectItem>();
			listaDocumentosAdjuntar.addAll(this.buscarDocumentosAdjuntar());
		}

		return listaDocumentosAdjuntar;
	}

	public List<SelectItem> getListaDocumentosAnexar() {
		return listaDocumentosAnexar;
	}

	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}

	public void setDocumentoOriginal(final Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(final Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public void setMateriaArchivo(final String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	public int getCopiaDirectoFlag() {
		return copiaDirectoFlag;
	}

	public void setCopiaDirectoFlag(final int copiaDirectoFlag) {
		this.copiaDirectoFlag = copiaDirectoFlag;
	}

	public List<DestinatariosFrecuentes> getListListas() {
		return listListas;
	}

	public void setListListas(final List<DestinatariosFrecuentes> listListas) {
		this.listListas = listListas;
	}

	public Long getGrupo() {
		return grupo;
	}

	public void setGrupo(final Long grupo) {
		this.grupo = grupo;
	}

	public List<SelectItem> getListDestFrec() {
		return listDestFrec;
	}

	public void setListDestFrec(final List<SelectItem> listDestFrec) {
		this.listDestFrec = listDestFrec;
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public boolean isDocValido() {
		return docValido;
	}

	public void setDocValido(final boolean docValido) {
		this.docValido = docValido;
	}

	public void marcarDoc() {
		docValido = true;
	}

	@Override
	public Long getCodigoBarra() {
		return codigoBarra;
	}

	@Override
	public void setCodigoBarra(final Long codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	@Override
	public void cargarCodigoBarra(final Documento doc) {
		log.info("codigo barra : {0}, {1}", doc.getCodigoBarra(), doc.getId());
		this.setCodigoBarra(doc.getCodigoBarra());
	}

	// grupos de usuarios

	@Override
	public void buscarGrupos() {
		// TODO Auto-generated method stub
		this.listGrupos.clear();
		Query query = em.createNamedQuery("GrupoUsuario.findByAll");
		SelectItem select = new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL);
		if (this.listGrupos.size() == 0) {
			this.listGrupos.add(select);
		}
		List result = query.getResultList();
		for (Object object : result) {
			select = new SelectItem();
			GrupoUsuario grupo = (GrupoUsuario) object;
			select.setValue(grupo.getId());
			select.setLabel(grupo.getNombre());
			this.listGrupos.add(select);
		}
	}

	@Override
	public Long getFiscalia() {
		return fiscalia;
	}

	@Override
	public void setFiscalia(Long fiscalia) {
		this.fiscalia = fiscalia;
	}

	@Override
	public List<SelectItem> getFiscalias() {
		return fiscalias;
	}

	@Override
	public void setFiscalias(List<SelectItem> fiscalias) {
		this.fiscalias = fiscalias;
	}

	@Override
	public List<SelectItem> getListGrupos() {
		SelectItem select = new SelectItem(-1, "<< Seleccionar >>");
		if (this.listGrupos.size() == 0) {
			this.listGrupos.add(select);
		}
		return listGrupos;
	}

	@Override
	public void setListGrupos(List<SelectItem> listGrupos) {

		this.listGrupos = listGrupos;
	}

	@Override
	public List<GrupoUsuario> getListGrupoUsuarios() {

		return listGrupoUsuarios;
	}

	@Override
	public void setListGrupoUsuarios(List<GrupoUsuario> listGrupoUsuarios) {
		this.listGrupoUsuarios = listGrupoUsuarios;
	}

	@Override
	public void agregarGrupoUsuario() {

		if (this.grupoUsuario != -1) {
			GrupoUsuario grupoUsuarios = adminGrupos
					.getGrupoUsuario(this.grupoUsuario);
			if (grupoUsuarios.getUsuarios().size() > 0) {
				boolean esta = false;
				for (GrupoUsuario grupo : listGrupoUsuarios) {
					if (grupoUsuarios.getId().equals(grupo.getId())) {
						esta = true;
					}
				}
				if (!esta) {
					grupoUsuarios.setBorrable(true);
					this.listGrupoUsuarios.add(grupoUsuarios);
				} else {
					FacesMessages.instance()
							.add("El grupo ya esta en la lista");
				}
			} else {
				FacesMessages.instance().add(
						"El grupo no tiene usuarios asociados");
			}
		} else {
			FacesMessages.instance().add(
					"Debe seleccionar un grupo");
		}
	}

	@Override
	public Long getGrupoUsuario() {
		return grupoUsuario;
	}

	@Override
	public void setGrupoUsuario(Long grupoUsuario) {
		this.grupoUsuario = grupoUsuario;
	}

	@Override
	public void eliminarGrupoUsuario(GrupoUsuario g) {
		listGrupoUsuarios.remove(g);
//		for (int i = 0; i < listGrupoUsuarios.size(); i++) {
//			GrupoUsuario grupoUsuario = listGrupoUsuarios.get(i);
//			if (grupoUsuario.getNombre().equals(nombre)) {
//				listGrupoUsuarios.remove(i);
//			}
//		}
	}
	
	@Override
	public boolean esArchivable() {
		//final boolean archivable = true;
		if (this.expediente.getCopia() != null && this.expediente.getCopia()) {
			return false;
		}
		if ( this.expediente.getFechaDespacho() != null ) {
			return false;
		}
		return true;
	}
	
	private void setDestinatarioDocumentoOriginal() {
		if (destinatariosDocumentoOriginal == null) {
			destinatariosDocumentoOriginal = new ArrayList<SelectItem>();
		} else {
			destinatariosDocumentoOriginal.clear();
		}
		if (this.documentoOriginal != null && this.documentoOriginal.getTipoDocumento() != null && (
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
		// ||
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES)
		// ||
		// this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADQUISICION)
		// ||
				this.documentoOriginal.getTipoDocumento().getId()
						.equals(TipoDocumento.RESOLUCION))) {
			if (this.expediente.getDestinatario() != null) {
				destinatariosDocumentoOriginal.add(new SelectItem(
						destinatariosDocumentoOriginal.size(), this.expediente
								.getDestinatario().getNombreApellido()));
			} else {
				if (this.expediente.getDestinatarioHistorico() != null) {
					destinatariosDocumentoOriginal.add(new SelectItem(
							destinatariosDocumentoOriginal.size(),
							this.expediente.getDestinatarioHistorico()
									.getNombreApellido()));
				}
			}
		} else {
			if(this.documentoOriginal != null){
				for (ListaPersonasDocumento dd : this.documentoOriginal
						.getDestinatarios()) {
					if (dd instanceof DestinatarioDocumento) {
						destinatariosDocumentoOriginal.add(new SelectItem(
								destinatariosDocumentoOriginal.size(), dd
										.getDestinatario()));
					}
				}
			}
		}
	}

	private boolean tieneRol(Rol rol){
		for(Rol rolusuario: usuario.getRoles())
			if(rolusuario.getId().equals(rol.getId()))
				return true;
		return false;
	}
	
	@Override
	public boolean obtenerTipoDocumento() {
		
		if(documentoOriginal != null) {
			if(documentoOriginal instanceof Resolucion){
				if(documentoOriginal.getFirmas() != null && documentoOriginal.getFirmas().size() > 0) return false;
				if(documentoOriginal.getFirmas() != null && documentoOriginal.getFirmas().size() > 0 && expediente.getDestinatarioHistorico() != null && expediente.getDestinatarioHistorico().equals(usuario))return false;
				return true;
			}
			
		}
		return false;
	}
	
	@Override
	public boolean logicaDespacho(){
		if(expediente.getFechaDespacho() == null && expediente.getFechaDespachoHistorico() == null)return false;
		return true;
	}
	
	@Override
	public boolean logicaDespachoReenviar(){
		if(expediente.getFechaDespachoHistorico() != null || expediente.getFechaDespacho() != null)return false;
		return true;
		
	}
	
	@Override
	public String getVerArchivoTitulo() {
		return verArchivoTitulo;
	}

	@Override
	public void setVerArchivoTitulo(final String verArchivoTitulo) {
		this.verArchivoTitulo = verArchivoTitulo;
	}

	@Override
	public String getVerArchivoOriginal() {
		return verArchivoOriginal;
	}

	@Override
	public void setVerArchivoOriginal(final String verArchivoOriginal) {
		this.verArchivoOriginal = verArchivoOriginal;
	}

	@Override
	public String getVisualizar() {
		return visualizar;
	}

	@Override
	public void setVisualizar(String visualizar) {
		this.visualizar = visualizar;
	}
	
}
