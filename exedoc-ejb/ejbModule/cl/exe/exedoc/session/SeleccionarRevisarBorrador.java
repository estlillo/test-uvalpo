package cl.exe.exedoc.session;

import java.util.List;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.RevisarEstructuradaDocumento;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * @author Pablo
 * 
 */
public interface SeleccionarRevisarBorrador {

	/**
	 * 
	 */
	void destroy();

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * 
	 */
	void buscarDepartamentos();

	/**
	 * 
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * 
	 */
	void buscarCargos();

	/**
	 * 
	 */
	void buscarPersonas();

	/**
	 * 
	 */
	void agregar();

	/**
	 * @param orden
	 *            {@link Integer}
	 */
	void eliminar(Integer orden);

	/**
	 * @return {@link JerarquiasLocal}
	 */
	JerarquiasLocal getJerarquias();

	/**
	 * @param jerarquias
	 *            {@link JerarquiasLocal}
	 */
	void setJerarquias(JerarquiasLocal jerarquias);

	/**
	 * @return {@link Documento}
	 */
	Documento getDocumento();

	/**
	 * @param documento
	 *            {@link Documento}
	 */
	void setDocumento(Documento documento);

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion
	 *            {@link Long}
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return {@link Long}
	 */
	Long getDivision();

	/**
	 * @param division
	 *            {@link Long}
	 */
	void setDivision(Long division);

	/**
	 * @return {@link Long}
	 */
	Long getDepartamento();

	/**
	 * @param departamento
	 *            {@link Long}
	 */
	void setDepartamento(Long departamento);

	/**
	 * @return {@link Long}
	 */
	Long getUnidadOrganizacional();

	/**
	 * @param unidadOrganizacional
	 *            {@link Long}
	 */
	void setUnidadOrganizacional(Long unidadOrganizacional);

	/**
	 * @return {@link Long}
	 */
	Long getCargo();

	/**
	 * @param cargo
	 *            {@link Long}
	 */
	void setCargo(Long cargo);

	/**
	 * @return {@link Long}
	 */
	Long getPersona();

	/**
	 * @param persona
	 *            {@link Long}
	 */
	void setPersona(Long persona);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivision();

	/**
	 * @param listDivision
	 *            {@link List} of {@link SelectItem}
	 */
	void setListDivision(List<SelectItem> listDivision);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @param listDepartamento
	 *            {@link List} of {@link SelectItem}
	 */
	void setListDepartamento(List<SelectItem> listDepartamento);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListUnidadesOrganizacionales();

	/**
	 * @param listUnidadesOrganizacionales
	 *            {@link List} of {@link SelectItem}
	 */
	void setListUnidadesOrganizacionales(
			List<SelectItem> listUnidadesOrganizacionales);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListCargos();

	/**
	 * @param listCargos
	 *            {@link List} of {@link SelectItem}
	 */
	void setListCargos(List<SelectItem> listCargos);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListPersonas();

	/**
	 * @param listPersonas
	 *            {@link List} of {@link SelectItem}
	 */
	void setListPersonas(List<SelectItem> listPersonas);

	/**
	 * @return {@link RevisarEstructuradaDocumento}
	 */
	List<RevisarEstructuradaDocumento> getRevisar();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacion();

	void agregarPorId(Long id);

}
