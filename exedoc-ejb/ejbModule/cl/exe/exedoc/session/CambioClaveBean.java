package cl.exe.exedoc.session;

import java.io.Serializable;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Persona;

/**
 * Class utilizada para cambiar clave de usuario.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(CambioClaveBean.NAME)
public class CambioClaveBean implements CambioClave, Serializable {

	/**
	 * NAME de la clase.
	 */
	public static final String NAME = "cambioClave";
	private static final long serialVersionUID = 6521187284218543529L;

	@In(required = true)
	private Persona usuario;

	private Persona persona;

	private String claveActual;
	private String claveNueva;
	private String repetirClave;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation");
		persona = usuario;
		return NAME;
	}

	@Override
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public String cambiarClave() {
		String r = "";
		log.info("inicio cambio clave a usuario : {0}", usuario.getUsuario());
		if (persona.getKeyStorePassword().equals(claveActual)) {
			if (claveNueva.equals(repetirClave)) {
				persona.setKeyStorePassword(claveNueva);
				em.merge(persona);
			} else {
				FacesMessages.instance().add("La claves no son iguales.");
				r = NAME;
			}
		} else {
			FacesMessages.instance().add("La clave actual no es valida.");
			r = NAME;
		}
		log.info("fin cambio clave a usuario : {0}", usuario.getUsuario());
		if (r.length() == 0) {
			FacesMessages.instance().add("Cambio exitoso.");
			r = this.end();
		}
		return r;
	}

	@Override
	public Persona getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Persona persona) {
		this.persona = persona;
	}

	@Override
	public String getClaveActual() {
		return claveActual;
	}

	@Override
	public void setClaveActual(final String claveActual) {
		this.claveActual = claveActual;
	}

	@Override
	public String getClaveNueva() {
		return claveNueva;
	}

	@Override
	public void setClaveNueva(final String claveNueva) {
		this.claveNueva = claveNueva;
	}

	@Override
	public String getRepetirClave() {
		return repetirClave;
	}

	@Override
	public void setRepetirClave(final String repetirClave) {
		this.repetirClave = repetirClave;
	}

}
