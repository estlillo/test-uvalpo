package cl.exe.exedoc.session;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.DatosUsuarioBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("reasignarExpedientesMasivo")
@Scope(ScopeType.SESSION)
public class ReasignarExpedientesMasivoBean implements ReasignarExpedientesMasivo {

	private static final String DIVISION_FIND_BY_ID = "Division.findById";
	private static final String SIN_SOLICITUD = "SS";
	private static final String CON_SOLICITUD = "CS";
	private static final String ORGANIZACION_FIND_BY_ID = "Organizacion.findById";
	private static final String ID = "id";
	private String tipoDocumentoBusqueda;
	private String numeroExpedienteBusqueda;
	private String numeroDocumentoBusqueda;
	private String emisorDocumentoBusqueda;
	private Date fechaInicioDocumentoBusqueda;
	private Date fechaTerminoDocumentoBusqueda;
	private String materiaDocumentoBusqueda;
	private String rutProveedorBusqueda;
	private Integer numeroOrdenCompraBusqueda;

	private Long organizacion;
	private Long idDivision;
	private Long idDepto;
	private Long idUnidad;
	private Long idCargo;
	private Long idPersona;

	private Long organizacionReasignar;
	private Long idDivisionReasignar;
	private Long idDeptoReasignar;
	private Long idUnidadReasignar;
	private Long idCargoReasignar;
	private Long idPersonaReasignar;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDeptos;
	private List<SelectItem> listUnidades;
	private List<SelectItem> listCargos;
	private List<SelectItem> listPersonas;

	private List<SelectItem> listOrganizacionReasignar;
	private List<SelectItem> listDivisionesReasignar;
	private List<SelectItem> listDeptosReasignar;
	private List<SelectItem> listUnidadesReasignar;
	private List<SelectItem> listCargosReasignar;
	private List<SelectItem> listPersonasReasignar;

	private boolean factura;
	private int copiaDirectoFlag;
	
	StringBuilder consultaSolicitud = new StringBuilder();

	private List<ExpedienteBandejaSalida> resultadosBusqueda;
	private Set<Persona> listDestinatarios;
	private Map<Long, Boolean> selectedReasignados;

	private List<Long> idExpedientesReasignar;

	private static final Locale locale = new Locale("es", "CL");
	private static final String TIMEZONE = "America/Santiago";

	@Logger
	private Log logger;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private ManejarExpedienteInterface me;

	@PersistenceContext
	protected EntityManager em;

	@In(required = true)
	private Persona usuario;

	@Out(required = true)
	private String homeCrear;

	@Factory(value = "resultadosBusqueda", scope = ScopeType.SESSION)
	@Override
	public String begin() {
		logger.info("Iniciando modulo...");

		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;

		this.buscarPersonas();
		this.buscarPersonasReasignar();

		resultadosBusqueda = new ArrayList<ExpedienteBandejaSalida>();
		listDestinatarios = new HashSet<Persona>();
		selectedReasignados = new HashMap<Long, Boolean>();

		idExpedientesReasignar = new ArrayList<Long>();

		listDivisionesReasignar = new ArrayList<SelectItem>();
		listDeptosReasignar = new ArrayList<SelectItem>();
		listUnidadesReasignar = new ArrayList<SelectItem>();
		listCargosReasignar = new ArrayList<SelectItem>();
		listPersonasReasignar = new ArrayList<SelectItem>();

		this.limpiarPersonaReasignar();

		this.cargarListas();

		this.cargarListasReasignar();

		homeCrear = "reasignarExpedientesMasivo";
		return homeCrear;
	}

	/**
	 * Metodo que carga las listas default que son utilizdas para la reasignacion de expedientes.
	 */
	private void cargarListasReasignar() {
		listOrganizacionReasignar = this.buscarOrganizacionesReasignar();
//		organizacionReasignar = (Long) listOrganizacionReasignar.get(1).getValue();
//		this.buscarDivisionesReasignar();
//		idDivisionReasignar = (Long) listDivisionesReasignar.get(1).getValue();
//		this.buscarDeptosReasignar();
//		listDeptosReasignar = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setIdDeptoReasignar(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidadesReasignar();
	}

	/**
	 * Metodo que carga las listas default.
	 */
	private void cargarListas() {
		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		idDivision = (Long) listDivisiones.get(1).getValue();
//		this.buscarDeptos();
//		listDeptos = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setIdDepto(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidades();
	}

	/**
	 * Metodo que llena la lista de Organizacion.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarPersonaBusqueda();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * Metodo que llena la lista de organizacion para reasignar.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizacionesReasignar() {
		this.limpiarPersonaReasignar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarPersonaBusqueda();
		organizacion = idOrganizacion;
		final Query query = em.createNamedQuery(ORGANIZACION_FIND_BY_ID);
		query.setParameter(ID, organizacion);
		final List<Organizacion> organizaciones = query.getResultList();
		if (organizaciones != null && organizaciones.size() == 1) {
			Organizacion organizacion = organizaciones.get(0);
		}
		listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void buscarDivisionesReasignar() {
		final long idOrganizacion = organizacionReasignar;
		this.limpiarPersonaReasignar();
		organizacionReasignar = idOrganizacion;
		final Query query = em.createNamedQuery(ORGANIZACION_FIND_BY_ID);
		query.setParameter(ID, organizacionReasignar);
		final List<Organizacion> organizaciones = query.getResultList();
		if (organizaciones != null && organizaciones.size() == 1) {
			Organizacion organizacion = organizaciones.get(0);
		}
		listDivisionesReasignar = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacionReasignar);
		listPersonasReasignar.clear();
		if (listUnidadesReasignar.size() == 0) {
			listUnidadesReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void limpiarPersonaBusqueda() {
		organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;
		idDepto = JerarquiasLocal.INICIO;
		idUnidad = JerarquiasLocal.INICIO;
		idCargo = JerarquiasLocal.INICIO;
		idPersona = JerarquiasLocal.INICIO;

		listDivisiones = new ArrayList<SelectItem>();
		listDeptos = new ArrayList<SelectItem>();
		listUnidades = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		listPersonas = new ArrayList<SelectItem>();

		listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDeptos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDeptos() {
		final List<Division> divisiones = em.createNamedQuery(DIVISION_FIND_BY_ID).setParameter(ID, idDivision)
				.getResultList();
		boolean conCargo = false;

		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDivision(idDivision);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDeptos = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidades.clear();
		listUnidades = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidades() {
		List<Departamento> deptos = em.createNamedQuery("Departamento.findById").setParameter(ID, idDepto)
				.getResultList();
		boolean conCargo = false;

		if (deptos != null && deptos.size() == 1) {
			Departamento depto = deptos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDepartamento(idDivision, idDepto);
				buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidades = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, idDepto);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, idUnidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonasReasignar(JerarquiasLocal.TEXTO_INICIAL, idCargo);
		if (listPersonas.size() == 1) {
			idPersona = (Long) listPersonas.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	public void buscarDeptosReasignar() {
		List<Division> divisionesReasignar = em.createNamedQuery(DIVISION_FIND_BY_ID)
				.setParameter(ID, idDivisionReasignar).getResultList();
		boolean conCargo = false;

		if (divisionesReasignar != null && divisionesReasignar.size() == 1) {
			Division divisionReasignar = divisionesReasignar.get(0);
			if (divisionReasignar.getConCargo()) {
				conCargo = true;
				idUnidadReasignar = jerarquias.getIdUnidadVirtualDivision(idDivisionReasignar);
				buscarCargosReasignar();
				buscarUnidadesOrganizacionalesDepartamentoReasignar();
			}
		}
		if (!conCargo) {
			listCargosReasignar.clear();
			listCargosReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDeptosReasignar = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivisionReasignar);
		listPersonasReasignar.clear();
		if (listUnidadesReasignar.size() == 0) {
			listUnidadesReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	private void buscarUnidadesOrganizacionalesDepartamentoReasignar() {
		listUnidadesReasignar.clear();
		listUnidadesReasignar = jerarquias
				.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, idDivisionReasignar);
		listPersonasReasignar.clear();
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	public void buscarUnidadesReasignar() {
		List<Departamento> deptosReasignar = em.createNamedQuery("Departamento.findById")
				.setParameter(ID, idDeptoReasignar).getResultList();
		boolean conCargo = false;

		if (deptosReasignar != null && deptosReasignar.size() == 1) {
			Departamento deptoReasignar = deptosReasignar.get(0);
			if (deptoReasignar.getConCargo()) {
				conCargo = true;
				idUnidadReasignar = jerarquias.getIdUnidadVirtualDepartamento(idDivisionReasignar, idDeptoReasignar);
				buscarCargosReasignar();
			}
		}
		if (!conCargo) {
			listCargosReasignar.clear();
			listCargosReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesReasignar = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, idDeptoReasignar);
		listPersonasReasignar.clear();
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarCargosReasignar() {
		listCargosReasignar = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, idUnidadReasignar);
		listPersonasReasignar.clear();
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void buscarPersonasReasignar() {
		listPersonasReasignar = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, idCargoReasignar);
		if (listPersonasReasignar.size() == 1) {
			idPersonaReasignar = (Long) listPersonasReasignar.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	public void buscarExpedientes() {
		logger.info("[buscarExpedientes] Iniciando método...");
		long init = System.currentTimeMillis();
		resultadosBusqueda.clear();

		if (numeroExpedienteBusqueda.isEmpty() && numeroDocumentoBusqueda.isEmpty() && idPersona.equals(-1)) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add("Se debe incluir mínimo el rango de fechas");
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add("Debe incluir la fecha de termino");
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add("Debe incluir la fecha de inicio");
				return;
			}
		}
		final StringBuilder consultaSQL = armarConsultaSQL();

		logger.info("[buscarExpedientes] Consulta SQL a ejecutar: " + consultaSQL);
		Query query = em.createQuery(consultaSQL.toString());
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			query.setParameter("fechaArchivadoInicioBusqueda", FechaUtil.inicio(fechaInicioDocumentoBusqueda));
			query.setParameter("fechaArchivadoTerminoBusqueda", FechaUtil.fin(fechaTerminoDocumentoBusqueda));
		}
		
		final List<Object[]> objectArray = query.getResultList();
		logger.info("[buscarExpedientes] Registros obtenidos desde la BD: " + objectArray.size());
		logger.info("[buscarExpedientes] Tiempo de ejecución de la consulta: " + (System.currentTimeMillis() - init) + " ms.");

		init = System.currentTimeMillis();
		this.obtenerResultadosBusqueda(objectArray);

		logger.info("[buscarExpedientes] Total de registros a desplegar: " + resultadosBusqueda.size());
		logger.info("[buscarExpedientes] Tiempo total de ejecución: " + (System.currentTimeMillis() - init) + " ms.");
		this.cargarListas();
	}

	private StringBuilder armarConsultaSQL() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		StringBuilder consulta = new StringBuilder();
		consulta.append("select e.id as id_exp, e.numeroExpediente, e.fechaIngreso, e.fechaAcuseRecibo, ");
		consulta.append("p.nombres as dest_nombre, p.apellidoPaterno as dest_apellido, ");
		consulta.append("c.descripcion as dest_cargo, u.descripcion as dest_unidad, ");
		consulta.append("p_hist.nombres as dest_hist_nombre, p_hist.apellidoPaterno as dest_hist_apellido, ");
		consulta.append("c_hist.descripcion as dest_hist_cargo, u_hist.descripcion as dest_hist_cargo, ");
		consulta.append("d.id as id_doc, d.numeroDocumento, d.fechaDocumentoOrigen, d.fechaCreacion, ");
		consulta.append("d.materia, td.descripcion as doc_tipo, fd.descripcion as doc_formato ");		
		consulta.append(" from Expediente e ");
		consulta.append(" inner join e.documentos de ");
		consulta.append(" inner join de.documento d ");
		consulta.append(" inner join d.tipoDocumento td ");
		consulta.append( "inner join d.formatoDocumento fd ");
		consulta.append(" left join e.destinatario p ");
		consulta.append(" left join p.cargo c ");
		consulta.append(" left join c.unidadOrganizacional u ");
		consulta.append(" left join u.departamento dep ");
		consulta.append(" left join dep.division div ");
		consulta.append(" left join div.organizacion o ");
		consulta.append(" left join e.destinatarioHistorico p_hist ");
		consulta.append(" left join p_hist.cargo c_hist ");
		consulta.append(" left join c_hist.unidadOrganizacional u_hist  ");
		consulta.append(" left join u_hist.departamento dep_hist ");
		consulta.append(" left join dep_hist.division div_hist  ");
		consulta.append(" left join div_hist.organizacion o_hist ");
		consulta.append(" where ( ");
		consulta.append(tipoDocumentoBusqueda);
		consulta.append(" = -1 or td.id = ");
		consulta.append(tipoDocumentoBusqueda).append(") ");
		if (numeroDocumentoBusqueda.length() > 0) {
			consulta.append(" and d.numeroDocumento = '").append(numeroDocumentoBusqueda).append("' ");
		}
		if (!emisorDocumentoBusqueda.equals("")) {
			consulta.append(" and upper(d.emisor) like upper('%");
			consulta.append(emisorDocumentoBusqueda);
			consulta.append("%')");
		}
		if (materiaDocumentoBusqueda.length() > 0) {
			consulta.append(" and upper(d.materia) like upper('%");
			consulta.append(materiaDocumentoBusqueda);
			consulta.append("%')");
		}
		if (numeroExpedienteBusqueda.length() > 0) {
			consulta.append(" and e.numeroExpediente = '").append(numeroExpedienteBusqueda).append("' ");
		}

		consulta.append(" and e.reasignado is null ");
		consulta.append(" and e.archivado is null ");
		consulta.append(" and ((e.destinatario is not null ");
		consulta.append(" and e.fechaDespacho is not null ");
		consulta.append(" and e.fechaAcuseRecibo is null ");

		if (idPersona != null && idPersona != -1) {
			consulta.append(" and p.id = " + idPersona);
		} else {
			if (idCargo != null && idCargo != -1) {
				consulta.append(" and c.id= " + idCargo);
			} else {
				if (idUnidad != null && idUnidad != -1) {
					consulta.append(" and u.id= " + idUnidad);
				} else {
					if (idDepto != null && idDepto != -1) {
						consulta.append(" and dep.id= " + idDepto);
					} else {
						if (idDivision != null && idDivision != -1) {
							consulta.append(" and div.id = " + idDivision);
						} else {
							if (organizacion != null && organizacion != -1) {
								consulta.append(" and o.id = " + organizacion);
							}
						}
					}
				}
			}
		}
		consulta.append(")");
		consulta.append(" OR (e.destinatarioHistorico is not null ");
		consulta.append(" and e.fechaDespacho is null ");
		consulta.append(" and e.fechaAcuseRecibo is not null ");

		if (idPersona != null && idPersona != -1) {
			consulta.append(" and p_hist.id = " + idPersona);
		} else {
			if (idCargo != null && idCargo != -1) {
				consulta.append(" and c_hist.id = " + idCargo);
			} else {
				if (idUnidad != null && idUnidad != -1) {
					consulta.append(" and u_hist.id = " + idUnidad);
				} else {
					if (idDepto != null && idDepto != -1) {
						consulta.append(" and dep_hist.id = " + idDepto);
					} else {
						if (idDivision != null && idDivision != -1) {
							consulta.append(" and div_hist.id = " + idDivision);
						} else {
							if (organizacion != null && organizacion != -1) {
								consulta.append(" and o_hist.id = " + organizacion);
							}
						}
					}
				}
			}
		}

		consulta.append("))");
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			consulta.append(" and e.fechaIngreso between :fechaArchivadoInicioBusqueda ");
			consulta.append(" and :fechaArchivadoTerminoBusqueda ");
		}
		
		consulta.append(" and (d.eliminado is null or d.eliminado = false) ");
		consulta.append(" and d.fechaCreacion = (");
		consulta.append("   select max(d2.fechaCreacion) "); 
		consulta.append("   from Expediente e2 ");
		consulta.append("   inner join e2.documentos ed2 ");
		consulta.append("   inner join ed2.documento d2 ");
		consulta.append("   where e2.id = e.id ");
		consulta.append("   and (d2.eliminado is null or d2.eliminado = false) )");
		consulta.append(" order by e.fechaIngreso, e.id ");
		System.out.println(consulta.toString());
		return consulta;
	}

	private void obtenerResultadosBusqueda(List<Object[]> array) {
		resultadosBusqueda.clear();
		for (Object[] o : array) {
			ExpedienteBandejaSalida row = new ExpedienteBandejaSalida();
			//e.id as id_exp
			row.setId((Long)o[0]);
			//e.numeroExpediente
			row.setNumeroExpediente((String)o[1]);
			//e.fechaIngreso
			row.setFechaIngresoBandeja((Date)o[2]);
			//e.fechaAcuseRecibo
			Persona persona = new Persona();
			Cargo cargo = new Cargo();
			UnidadOrganizacional unidad = new UnidadOrganizacional();
			if (o[3] == null) {
				//p.nombres as dest_nombre
				persona.setNombres(o[4] != null ? (String)o[4] : "");
				//p.apellidoPaterno as dest_apellido
				persona.setApellidoPaterno(o[5] != null ? (String)o[5] : "");
				//c.descripcion as dest_cargo
				cargo.setDescripcion(o[6] != null ? (String)o[6] : "");
				//u.descripcion as dest_unidad
				unidad.setDescripcion(o[7] != null ? (String)o[7] : "");
			} else {
				row.setFechaAcuseRecibo((Date)o[3]);
				//p_hist.nombres as dest_hist_nombre
				persona.setNombres(o[8] != null ? (String)o[8] : "");
				//p_hist.apellidoPaterno as dest_hist_apellido
				persona.setApellidoPaterno(o[9] != null ? (String)o[9] : "");
				//c_hist.descripcion as dest_hist_cargo
				cargo.setDescripcion(o[10] != null ? (String)o[10] : "");
				//u_hist.descripcion as dest_hist_cargo
				unidad.setDescripcion(o[11] != null ? (String)o[11] : "");
			}
			cargo.setUnidadOrganizacional(unidad);
			persona.setCargo(cargo);
			row.setDestinatarioExpediente(persona);
			//d.id as id_doc
			row.setIdDocumento((Long)o[12]);
			//d.numeroDocumento
			row.setNumeroDocumento(o[13] != null ? (String)o[13] : "");
			//d.fechaDocumentoOrigen
			row.setFechaDocumentoOrigen(o[14] != null ? (Date)o[14] : null);
			//d.fechaCreacion
			row.setFechaCreacionDocumento((Date)o[15]);
			//d.materia
			row.setMateria(o[16] != null ? (String)o[16] : "");
			//td.descripcion as doc_tipo
			row.setTipoDocumento(o[17] != null ? (String)o[17] : "");
			//fd.descripcion as doc_formato
			row.setFormatoDocumento(o[18] != null ? (String)o[18] : "");
			
			resultadosBusqueda.add(row);
		}
	}
	
	private int indiceDocEnBandejaDeEntrada(Long idDoc, List<? extends ExpedienteBandeja> documentoEnBandeja) {
		for (int i = 0; i < documentoEnBandeja.size(); i++) {
			if (documentoEnBandeja.get(i).getId().equals(idDoc)) { return i; }
		}
		return -1;
	}

	private DatosUsuarioBandejaSalida buscarDatosUsuario(Expediente expediente) {
		DatosUsuarioBandejaSalida datosUsuario = new DatosUsuarioBandejaSalida();

		datosUsuario.setIdExpediente(expediente.getId());
		datosUsuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
		if (datosUsuario.getFechaAcuseRecibo() != null && !expediente.getAcuseRecibo()) {
			if (expediente.getFechaDespacho() != null) {
				datosUsuario.setFechaDespacho(expediente.getFechaDespacho());
			} else {
				datosUsuario.setFechaDespacho(expediente.getFechaIngreso());
			}
		} else {
			datosUsuario.setFechaDespacho(null);
		}
		if (expediente.getFechaDespacho() != null) {
			datosUsuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
		} else {
			datosUsuario.setFechaIngresoBandeja(expediente.getFechaDespachoHistorico());
		}

		Persona destinatario = expediente.getDestinatario();
		if (destinatario != null) {
			if (destinatario.getCargo() != null) datosUsuario.setDestinatarioCargo(destinatario.getCargo()
					.getDescripcion());
			if (destinatario.getCargo().getUnidadOrganizacional() != null) datosUsuario
					.setDestinatarioUnidadOrganizacional(destinatario.getCargo().getUnidadOrganizacional()
							.getDescripcion());
			datosUsuario.setDestinatarioUsuario(destinatario.getNombreApellido());
		}

		datosUsuario.setFechaArchivado(expediente.getArchivado());
		return datosUsuario;
	}

	@SuppressWarnings("unchecked")
	public Long obtenerIdExpediente(String numeroExpediente) {
		StringBuffer hql;
		Long idExpediente = null;
		hql = new StringBuffer("SELECT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = '" + numeroExpediente + "' ");
		hql.append("and e.cancelado is null ");
		hql.append("order by e.id desc limit 1");
		Query query = em.createQuery(hql.toString());
		List<Long> idExpedientes = query.getResultList();
		if (idExpedientes.size() >= 1) {
			idExpediente = idExpedientes.get(0);
		}
		return idExpediente;
	}

	public void setExpedientesAReasignar(Long idExpediente) {
		if (selectedReasignados.containsKey(idExpediente)) {
			idExpedientesReasignar.remove(idExpediente);
			selectedReasignados.remove(idExpediente);
		} else {
			idExpedientesReasignar.add(idExpediente);
			selectedReasignados.put(idExpediente, true);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void agregarPersona() {
		if (!idCargoReasignar.equals(JerarquiasLocal.INICIO)) {
			if (idPersonaReasignar.equals(JerarquiasLocal.TODOS)) {
				listDestinatarios.addAll(em.createNamedQuery("Persona.findById").setParameter(ID, idCargoReasignar)
						.getResultList());
			} else if (idPersonaReasignar.equals(JerarquiasLocal.CUALQUIERA)) {
				final List<Persona> personasReasignarTmp = em
						.createQuery("select p from Persona p where p.cargo.id = ?").setParameter(1, idCargoReasignar)
						.getResultList();
				for (Persona p : personasReasignarTmp) {
					p.setDestinatarioConRecepcion(true);
					listDestinatarios.add(p);
				}
			} else {
				final Persona p = (Persona) em.createQuery("select p from Persona p where p.id = :param")
						.setParameter("param", idPersonaReasignar).getSingleResult();
				if (copiaDirectoFlag == 0) {
					p.setDestinatarioConCopia(false);
				} else {
					p.setDestinatarioConCopia(true);
				}
				listDestinatarios.add(p);
			}
		}
		this.cargarListasReasignar();
	}

	private void limpiarPersonaReasignar() {
		organizacionReasignar = JerarquiasLocal.INICIO;
		idDivisionReasignar = JerarquiasLocal.INICIO;
		idDeptoReasignar = JerarquiasLocal.INICIO;
		idUnidadReasignar = JerarquiasLocal.INICIO;
		idCargoReasignar = JerarquiasLocal.INICIO;
		idPersonaReasignar = JerarquiasLocal.INICIO;

		listDivisionesReasignar.clear();
		listDeptosReasignar.clear();
		listUnidadesReasignar.clear();
		listCargosReasignar.clear();
		listPersonasReasignar.clear();

		listDivisionesReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDeptosReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargosReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonasReasignar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	public void eliminarPersona(Persona persona) {
		listDestinatarios.remove(persona);
	}

	// se saca logica de instrucciones
	public String reasignar() {
		Expediente expediente = null;
		if (idExpedientesReasignar == null || idExpedientesReasignar.isEmpty()) {
			FacesMessages.instance().add("Debe selecionar un Expediente para Reasignar.");
			return "";
		}
		if (listDestinatarios.size() > 0) {
			Date fechaIngreso = new Date();
			for (Long idExpediente : idExpedientesReasignar) {
				expediente = em.find(Expediente.class, idExpediente);
				
				// borrar destinatarios previos
				List<Expediente> expedientes = me.buscarExpedientesHijos(expediente);
				if (expedientes != null) {
					Iterator<Expediente> itExpedientes = expedientes.iterator();
					while (itExpedientes.hasNext()) {
						Expediente i = (Expediente)itExpedientes.next();
						me.eliminarExpediente(i.getId());
					}
				}
//				if (expediente.getAcuseRecibo() != null && expediente.getAcuseRecibo() == false ) {
//					me.acusarReciboExpediente(expediente, usuario);
//				}
				Observacion obs = new Observacion();
				obs.setAutor(usuario);
				obs.setExpediente(expediente);
				obs.setFecha(fechaIngreso);
				obs.setObservacion("Reasignado por " + usuario.getNombreApellido());
				obs.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_SISTEMA));
				em.merge(obs);
				
				expediente.setReasignado(fechaIngreso);
				em.merge(expediente);
				logger.info("Expediente [" + expediente.getId() + ", " + expediente.getNumeroExpediente()
						+ "] reasignado");
				for (Persona destinatario : listDestinatarios) {
					try {
						Long idExp = me.agregarDestinatarioAExpediente(expediente, usuario, destinatario, fechaIngreso);
						Expediente exp = me.buscarExpediente(idExp);
						logger.info("Reasignando expediente: " + exp.getId() + ", Nº: " + exp.getNumeroExpediente());
						me.despacharExpediente(exp);
					} catch (DocumentNotUploadedException e) {
						e.printStackTrace();
					}
				}
			}
			FacesMessages.instance().add("Expediente(s) reasignado(s)");
		} else {
			FacesMessages.instance().add("Debe seleccionar un destinatario antes de reasignar");
			return "";
		}
		return end();
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String end() {
		resultadosBusqueda = null;
		selectedReasignados = null;
		listDestinatarios = null;

		logger.info("Finalizando módulo...");

		return "home";
	}

	// Getters & Setters
	public String getTipoDocumentoBusqueda() {
		return tipoDocumentoBusqueda;
	}

	public void setTipoDocumentoBusqueda(String tipoDocumentoBusqueda) {
		this.tipoDocumentoBusqueda = tipoDocumentoBusqueda;
	}

	public String getNumeroExpedienteBusqueda() {
		return numeroExpedienteBusqueda;
	}

	public void setNumeroExpedienteBusqueda(String numeroExpedienteBusqueda) {
		this.numeroExpedienteBusqueda = numeroExpedienteBusqueda;
	}

	public String getNumeroDocumentoBusqueda() {
		return numeroDocumentoBusqueda;
	}

	public void setNumeroDocumentoBusqueda(String numeroDocumentoBusqueda) {
		this.numeroDocumentoBusqueda = numeroDocumentoBusqueda;
	}

	public String getEmisorDocumentoBusqueda() {
		return emisorDocumentoBusqueda;
	}

	public void setEmisorDocumentoBusqueda(String emisorDocumentoBusqueda) {
		this.emisorDocumentoBusqueda = emisorDocumentoBusqueda;
	}

	public Date getFechaInicioDocumentoBusqueda() {
		return fechaInicioDocumentoBusqueda;
	}

	public void setFechaInicioDocumentoBusqueda(Date fechaInicioDocumentoBusqueda) {
		this.fechaInicioDocumentoBusqueda = fechaInicioDocumentoBusqueda;
	}

	public Date getFechaTerminoDocumentoBusqueda() {
		return fechaTerminoDocumentoBusqueda;
	}

	public void setFechaTerminoDocumentoBusqueda(Date fechaTerminoDocumentoBusqueda) {
		this.fechaTerminoDocumentoBusqueda = fechaTerminoDocumentoBusqueda;
	}

	public String getMateriaDocumentoBusqueda() {
		return materiaDocumentoBusqueda;
	}

	public void setMateriaDocumentoBusqueda(String materiaDocumentoBusqueda) {
		this.materiaDocumentoBusqueda = materiaDocumentoBusqueda;
	}

	public String getRutProveedorBusqueda() {
		return rutProveedorBusqueda;
	}

	public void setRutProveedorBusqueda(String rutProveedorBusqueda) {
		this.rutProveedorBusqueda = rutProveedorBusqueda;
	}

	public Integer getNumeroOrdenCompraBusqueda() {
		return numeroOrdenCompraBusqueda;
	}

	public void setNumeroOrdenCompraBusqueda(Integer numeroOrdenCompraBusqueda) {
		this.numeroOrdenCompraBusqueda = numeroOrdenCompraBusqueda;
	}

	public Long getIdDivision() {
		return idDivision;
	}

	public void setIdDivision(Long idDivision) {
		this.idDivision = idDivision;
	}

	public Long getIdDepto() {
		return idDepto;
	}

	public void setIdDepto(Long idDepto) {
		this.idDepto = idDepto;
	}

	public Long getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getIdDivisionReasignar() {
		return idDivisionReasignar;
	}

	public void setIdDivisionReasignar(Long idDivisionReasignar) {
		this.idDivisionReasignar = idDivisionReasignar;
	}

	public Long getIdDeptoReasignar() {
		return idDeptoReasignar;
	}

	public void setIdDeptoReasignar(Long idDeptoReasignar) {
		this.idDeptoReasignar = idDeptoReasignar;
	}

	public Long getIdUnidadReasignar() {
		return idUnidadReasignar;
	}

	public void setIdUnidadReasignar(Long idUnidadReasignar) {
		this.idUnidadReasignar = idUnidadReasignar;
	}

	public Long getIdCargoReasignar() {
		return idCargoReasignar;
	}

	public void setIdCargoReasignar(Long idCargoReasignar) {
		this.idCargoReasignar = idCargoReasignar;
	}

	public Long getIdPersonaReasignar() {
		return idPersonaReasignar;
	}

	public void setIdPersonaReasignar(Long idPersonaReasignar) {
		this.idPersonaReasignar = idPersonaReasignar;
	}

	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	public void setListDivisiones(List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	public List<SelectItem> getListDeptos() {
		return listDeptos;
	}

	public void setListDeptos(List<SelectItem> listDeptos) {
		this.listDeptos = listDeptos;
	}

	public List<SelectItem> getListUnidades() {
		return listUnidades;
	}

	public void setListUnidades(List<SelectItem> listUnidades) {
		this.listUnidades = listUnidades;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public void setListCargos(List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public void setListPersonas(List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	public List<SelectItem> getListDivisionesReasignar() {
		return listDivisionesReasignar;
	}

	public void setListDivisionesReasignar(List<SelectItem> listDivisionesReasignar) {
		this.listDivisionesReasignar = listDivisionesReasignar;
	}

	public List<SelectItem> getListDeptosReasignar() {
		return listDeptosReasignar;
	}

	public void setListDeptosReasignar(List<SelectItem> listDeptosReasignar) {
		this.listDeptosReasignar = listDeptosReasignar;
	}

	public List<SelectItem> getListUnidadesReasignar() {
		return listUnidadesReasignar;
	}

	public void setListUnidadesReasignar(List<SelectItem> listUnidadesReasignar) {
		this.listUnidadesReasignar = listUnidadesReasignar;
	}

	public List<SelectItem> getListCargosReasignar() {
		return listCargosReasignar;
	}

	public void setListCargosReasignar(List<SelectItem> listCargosReasignar) {
		this.listCargosReasignar = listCargosReasignar;
	}

	public List<SelectItem> getListPersonasReasignar() {
		return listPersonasReasignar;
	}

	public void setListPersonasReasignar(List<SelectItem> listPersonasReasignar) {
		this.listPersonasReasignar = listPersonasReasignar;
	}

	public void setListDestinatarios(Set<Persona> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}

	public Object[] getListDestinatarios() {
		return listDestinatarios.toArray();
	}

	public List<ExpedienteBandejaSalida> getResultadosBusqueda() {
		return resultadosBusqueda;
	}

	public void setResultadosBusqueda(List<ExpedienteBandejaSalida> resultadosBusqueda) {
		this.resultadosBusqueda = resultadosBusqueda;
	}

	public Map<Long, Boolean> getSelectedReasignados() {
		return selectedReasignados;
	}

	public void setSelectedReasignados(Map<Long, Boolean> selectedReasignados) {
		this.selectedReasignados = selectedReasignados;
	}

	public Locale getLocale() {
		return locale;
	}

	public String getTIMEZONE() {
		return TIMEZONE;
	}

	public boolean isFactura() {
		return factura;
	}

	public void setFactura(boolean factura) {
		this.factura = factura;
	}

	public int getCopiaDirectoFlag() {
		return copiaDirectoFlag;
	}

	public void setCopiaDirectoFlag(int copiaDirectoFlag) {
		this.copiaDirectoFlag = copiaDirectoFlag;
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}

	public Long getOrganizacionReasignar() {
		return organizacionReasignar;
	}

	public void setOrganizacionReasignar(Long organizacionReasignar) {
		this.organizacionReasignar = organizacionReasignar;
	}

	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public List<SelectItem> getListOrganizacionReasignar() {
		return listOrganizacionReasignar;
	}

	public void setListOrganizacionReasignar(List<SelectItem> listOrganizacionReasignar) {
		this.listOrganizacionReasignar = listOrganizacionReasignar;
	}
}
