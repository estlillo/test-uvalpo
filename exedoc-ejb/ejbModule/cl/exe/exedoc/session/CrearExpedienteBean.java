package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.AccionProvidencia;
import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoBinario;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Considerandos;
import cl.exe.exedoc.entity.Contrato;
import cl.exe.exedoc.entity.Convenio;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DiaAdministrativo;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.Resuelvo;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.Vacaciones;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.entity.Vistos;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.mantenedores.AdministradorPersona;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.utils.InformacionPersonal;
import cl.exe.exedoc.session.utils.ListaDocumentos;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * Implementacion de {@link CrearExpediente}.
 */
@Stateful
@Name("crearExpediente")
@Scope(ScopeType.CONVERSATION)
public class CrearExpedienteBean implements CrearExpediente {

	private static final String SELECCIONAR = JerarquiasLocal.TEXTO_INICIAL;

	private static final String ERROR = "Error: ";

	private static final String GUION = " - ";

	private static final String DBS = "\\";

	private static final String CREAR_EXPEDIENTE = "crearExpediente";

	private static final String INGRESAR_DESTINATARIO = "Debe Ingresar un Destinatario al Documento";

	private static final String EXISTE_DOCUMENTO = "El Documento ya ha sido ingresado";

	private static final String NO_PUEDE_AGREGAR = "No se puede agregar a esta persona como destinatario, ";

	private static final int LARGO_MAXIMO = 255;
	
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private AdministradorAlertas administradorAlertas;

	@EJB
	private AdministradorPersona adminPersona;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	public List<Resolucion> hijas = new ArrayList<Resolucion>();
	
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatariosOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	// Datos Expediente
	private Date fechaIngreso;

	private boolean esFactura;
	private Long montoFactura;

	private String formato;

	private boolean guardarExpediente;

	// Datos Documento
	private DocumentoPapel documentoPapel;
	private DocumentoBinario documentoBinario;

	
	private ArchivoDocumentoBinario archivo;

	private List<SelectItem> listTipoDocumentos;
	private List<SelectItem> listDocumentosElectronicos;
	private List<SelectItem> listDocumentosNoElectronicos;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Integer tipoDocumentoID;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	// Documentos Electronicos
	private Integer tipoDocumentoAdjuntar;
	private List<SelectItem> listaDocumentosAdjuntar;
	
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documento;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	private String homeCrear = CREAR_EXPEDIENTE;

	private int contadorDocumentos;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean agregarDestinatarioDocumento;

	/*
	 * DESPACHAR EXPEDIENTES.
	 */
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	private List<Long> idExpedientes;

	@EJB
	private ManejarExpedienteInterface me;

	// Archivos Anexos - Antecedentes.
	private List<ArchivoAdjuntoDocumentoBinario> archivosAntecedentes;
	private String materiaArchivo;
	private ArchivoAdjuntoDocumentoBinario archivoAdjuntoTemporal;

	@EJB
	private RepositorioLocal repositorio;

	private List<SelectItem> nivelesUrgencia;
	private Long idAlerta;

	private boolean validarComplete;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private String destinatario;

	/*Datos para solicitudes vacaciones y dias Administrativos*/
	private Persona solicitante;

	private String calidadJuridica;

	private String grado;

	private Double diasVacacionesPendientes;

	private String datosObtenidos;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false,scope = ScopeType.CONVERSATION)
//	private InformacionPersonal informacionPersonal;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<DetalleDias> diasSolicitados;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false,scope = ScopeType.CONVERSATION)
	private ListaDocumentos listaDocumentos;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private InformacionPersonalSrv informacionPersonalSrv;
	

	/**
	 * Constructor.
	 */
	public CrearExpedienteBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation: crearExpediente");

		agregarDestinatarioDocumento = true;

		homeCrear = CREAR_EXPEDIENTE;
		fechaIngreso = new Date();
		esFactura = false;

		formato = "papel";
		destinatariosDocumento = new ArrayList<SelectPersonas>();
		distribucionDocumento = new ArrayList<SelectPersonas>();

		listDocumentosRespuesta = new ArrayList<Documento>();
		listTipoDocumentos = this.buscarTipoDocumentos();
		// listDocumentosElectronicos =
		// this.buscarDocumentosElectronicosResolucion();
		listDocumentosElectronicos = this.buscarDocumentoselectronicos();
		listDocumentosNoElectronicos = this.buscarDocumentosNoElectronicos();

//		/*SdelaF*/
//		listDocumentosNoElectronicos.clear();
//		listDocumentosNoElectronicos.add(new SelectItem(JerarquiasLocal.INICIO,
//				JerarquiasLocal.TEXTO_INICIAL));
//
//		final Query query = em.createNamedQuery("tipoDocumentoActivo.yNoEsElectronico");
//		final List<TipoDocumento> tipoDoc = query.getResultList();
//
//		if (tipoDoc != null) {
//			for (TipoDocumento de : tipoDoc) {
//				listDocumentosNoElectronicos.add(new SelectItem(de.getId(), de
//						.getDescripcion()));
//			}
//		}
//		/*SdelaF*/
		listaDocumentos = new ListaDocumentos();
		archivosAntecedentes = new ArrayList<ArchivoAdjuntoDocumentoBinario>();
		
		listaDocumentosAdjuntar = this.buscarDocumentosAdjuntar();
		tipoDocumentoAdjuntar = 0;

		archivo = new ArchivoDocumentoBinario();

		contadorDocumentos = 0;
		tipoDocumentoID = 0;
		listDestinatarios = new HashSet<Persona>();
		listDestinatariosOriginal = new HashSet<Persona>();

		expediente = null;
		documentoOriginal = null;
		
		this.validarComplete = false;
		
		this.destinatario="";
		
		//TODO Revisar SOlicitud de dias
		//informacionPersonal = new InformacionPersonal();
		
		this.cargarNivelesUrgencia();

		this.limpiarDocumento();

		return CREAR_EXPEDIENTE;
	}

	/**
	 * Metodo que carga la lista con los documentos electronicos.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	@SuppressWarnings("unchecked")
	private List<SelectItem> buscarDocumentoselectronicos() {
		final Query query = em
				.createNamedQuery("tipoDocumentoActivo.yEsElectronicoCreacion");
		final List<TipoDocumento> docs = query.getResultList();
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem("0", JerarquiasLocal.TEXTO_INICIAL));

		for (TipoDocumento td : docs) {
			if (td.getVisible()) {
				lista.add(new SelectItem(td.getId(), td.getDescripcion()));
			}
		}
		return lista;
	}

	@Override
	public void distribuirDocumentos() {
		if (listDocumentosRespuesta == null
				|| listDocumentosRespuesta.size() == 0) {
			FacesMessages.instance().add("Debe Ingresar al menos un Documento");
		} else {
			documentoOriginal = listDocumentosRespuesta.get(0);
			listDocumentosRespuesta.remove(0);
		}
	}

	/**
	 * Metodo que agrega documento a la lista de documentos del expediente.
	 * 
	 * @param doc
	 *            {@link Documento}
	 */
	private void agregarDocumento(final Documento doc) {
		doc.setFechaCreacion(new Date());
		final TipoDocumento td = em.find(TipoDocumento.class, tipoDocumentoID);
		doc.setTipoDocumento(td);
		doc.setAutor(usuario);
		doc.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
		this.setDestinatarioDocumento(doc);
		this.setDistribucionDocumento(doc);
		doc.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.ORIGINAL));
		doc.setIdNuevoDocumento(contadorDocumentos++);
		listDocumentosRespuesta = new ArrayList<Documento>();
		listDocumentosRespuesta.add(doc);
		guardarExpediente = true;
	}

	@Override
	public void agregarDocumentoPapel() {
		boolean ok = true;
		this.validarComplete = true;
		
		long start = System.currentTimeMillis();
		if (this.getIdAlerta() != null && !this.getIdAlerta().equals(JerarquiasLocal.INICIO)) {
			this.getDocumentoPapel().setAlerta(
					administradorAlertas.buscarAlerta(this.getIdAlerta()));
		}
		
		if ((destinatariosDocumento == null || destinatariosDocumento.size() == 0)) {
			if (this.getDestinatario() == null
					|| this.getDestinatario().trim().equals("")) {
				FacesMessages.instance().add(
						"Debe ingresar un Destinatario al Documento");
				ok = false;
				this.validarComplete = false;
			}
		}

		if (this.documentoPapel.getEmisor() == null
				|| this.documentoPapel.getEmisor().trim().equals("")) {
			FacesMessages.instance().add(
					"Debe ingresar un Emisor al Documento");
			ok = false;
			this.validarComplete = false;
		}
		
		if(this.tipoDocumentoID == JerarquiasLocal.INICIO.intValue()) {
			FacesMessages.instance().add("Debe ingresar el tipo de Documento");
			ok = false;
			this.validarComplete = false;
		}
		if(documentoPapel.getNumeroDocumento() == null || documentoPapel.getNumeroDocumento().trim().length() == 0){
			FacesMessages.instance().add("Debe ingresar el Número de Documento");
			ok = false;
			this.validarComplete = false;
		}

		if(documentoPapel.getFechaDocumentoOrigen() == null || documentoPapel.getFechaDocumentoOrigen().toString().trim().length() == 0){
			FacesMessages.instance().add("Debe ingresar la Fecha de Origen");
			ok = false;
			this.validarComplete = false;
		}
        
		documentoPapel.setMateria(documentoPapel.getMateria().trim());
		if(documentoPapel.getMateria() == null || documentoPapel.getMateria().length() == 0){
			FacesMessages.instance().add("Debe ingresar la Materia");
			ok = false;
			this.validarComplete = false;
		}
		if(documentoPapel.getMateria().length() > 2000){
			FacesMessages.instance().add("La Materia debe ser menor a 2.000 caracteres.");
			ok = false;
			this.validarComplete = false;
		}
		documentoPapel.setAntecedentes(documentoPapel.getAntecedentes().trim());
		if(documentoPapel.getAntecedentes().length() > 2000){
			FacesMessages.instance().add("El campo \"Antecedente\" debe ser menor a 2.000 caracteres.");
			ok = false;
			this.validarComplete = false;
		}

		String valorDe = this.documentoPapel.getEmisor();
		int largoDe = valorDe.trim().length();
		if (valorDe != null && !valorDe.trim().isEmpty() && largoDe >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s caracteres, en el campo \"De\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			ok = false;
			this.validarComplete = false;
		}
		if ((destinatariosDocumento == null || destinatariosDocumento.size() == 0)) {
			if (this.getDestinatario() == null
					|| this.getDestinatario().trim().equals("")) {
				FacesMessages.instance().add(
						"Debe ingresar un Destinatario al Documento");
				ok = false;
				this.validarComplete = false;
			} else {
				this.setDestinatario(this.getDestinatario().trim());
				if (this.getDestinatario().length() >= LARGO_MAXIMO) {
					String detalle = String.format("No puede ingresar más de %s caracteres, en el campo \"Destinatario\"", LARGO_MAXIMO);
					FacesMessages.instance().add(detalle);
					ok = false;
					this.validarComplete = false;
				}
			}
		}
		if (documentoPapel.getPlazo() != null) {
			if (documentoPapel.getPlazo().compareTo(documentoPapel.getFechaDocumentoOrigen()) < 0) {
				FacesMessages.instance().add("El Plazo no puede ser anterior a la Fecha Origen del Documento");
				ok = false;
				this.validarComplete = false;
			}
		}
		if (ok) {
			LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente", "inicio", new Date());
			this.agregarDocumento(documentoPapel);
			documentoPapel.setFormatoDocumento(new FormatoDocumento(
					FormatoDocumento.PAPEL));
			this.limpiarDocumento();
			
			LogTiempos.mostrarLog(this.usuario.getUsuario(),
					"Documento Papel", System.currentTimeMillis() - start);
		}
	}

	@Override
	public void guardarYCrearExpediente() {
		this.agregarDocumentoPapel();
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

	@Override
	public void agregarDocumentoBinario() {

		boolean ok = true;
		this.validarComplete = true;

		if (this.getIdAlerta() != null && !this.getIdAlerta().equals(JerarquiasLocal.INICIO)) {
			this.getDocumentoBinario().setAlerta(
					administradorAlertas.buscarAlerta(this.getIdAlerta()));
		}
//		if(!documentoBinario.getNumeroDocumento().trim().equals("s/n")){ modificado porque se lanzo una incidencia del ministerio publico 
//			if (md.existeDocumento(documentoBinario.getNumeroDocumento(),tipoDocumentoID)) {
//				FacesMessages.instance().add(EXISTE_DOCUMENTO);
//				ok = false;
//				this.validarComplete = false;
//			}
//		}
		String valorDe = this.documentoBinario.getEmisor();
		int largoDe = valorDe.trim().length();
		if (valorDe != null && !valorDe.trim().isEmpty() && largoDe >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s caracteres, en el campo \"De\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			ok = false;
			this.validarComplete = false;
		}
		if (archivo == null || archivo.getNombreArchivo() == null) {
			FacesMessages.instance().add(
					"Debe adjuntar un Documento Digitalizado");
			ok = false;
			this.validarComplete = false;
		}
		if ((destinatariosDocumento == null || destinatariosDocumento.size() == 0)) {
			if (this.getDestinatario() == null
					|| this.getDestinatario().trim().equals("")) {
				FacesMessages.instance().add(
						"Debe ingresar un Destinatario al Documento");
				ok = false;
				this.validarComplete = false;
			} else {
				this.setDestinatario(this.getDestinatario().trim());
				if (this.getDestinatario().length() >= LARGO_MAXIMO) {
					String detalle = String.format("No puede ingresar más de %s caracteres, en el campo \"Destinatario\"", LARGO_MAXIMO);
					FacesMessages.instance().add(detalle);
					ok = false;
					this.validarComplete = false;
				}
			}
		}
		if(this.tipoDocumentoID == -1){
			FacesMessages.instance().add("Debe ingresar el Tipo de Documento");
			ok = false;
			this.validarComplete = false;
		}
		
		if(documentoBinario.getNumeroDocumento() == null || documentoBinario.getNumeroDocumento().trim().length() == 0){
			FacesMessages.instance().add("Debe ingresar el Número de Documento");
			ok = false;
			this.validarComplete = false;
		}

		if(documentoBinario.getFechaDocumentoOrigen() == null || documentoBinario.getFechaDocumentoOrigen().toString().trim().length() == 0){
			FacesMessages.instance().add("Debe ingresar la Fecha de Origen");
			ok = false;
			this.validarComplete = false;
		}
        
		documentoBinario.setMateria(documentoBinario.getMateria().trim());
		if(documentoBinario.getMateria() == null || documentoBinario.getMateria().length() == 0){
			FacesMessages.instance().add("Debe ingresar la Materia");
			ok = false;
			this.validarComplete = false;
		}
		if(documentoBinario.getMateria().length() > 2000){
			FacesMessages.instance().add("La Materia debe ser menor a 2.000 caracteres.");
			ok = false;
			this.validarComplete = false;
		}
		documentoBinario.setAntecedentes(documentoBinario.getAntecedentes().trim());
		if(documentoBinario.getAntecedentes().length() > 2000){
			FacesMessages.instance().add("El campo \"Antecedente\" debe ser menor a 2.000 caracteres.");
			ok = false;
			this.validarComplete = false;
		}
		
		if (documentoBinario.getPlazo() != null) {
			if (documentoBinario.getPlazo().compareTo(documentoBinario.getFechaDocumentoOrigen()) < 0) {
				FacesMessages.instance().add("El Plazo no puede ser anterior a la Fecha Origen del Documento");
				ok = false;
				this.validarComplete = false;
			}
		}
		
		if (ok) {
			
			LogTiempos.mostrarLog(this.usuario.getUsuario(), "Crear Expediente", "inicio", new Date());
			
			this.agregarDocumento(documentoBinario);
			documentoBinario.setFormatoDocumento(new FormatoDocumento(
					FormatoDocumento.DIGITAL));
			archivo.setFecha(new Date());
			documentoBinario.setArchivo(archivo);
			this.setAntecedentesDocumento(documentoBinario);
			this.limpiarDocumento();
		}
	}

	/**
	 * @param docBinario
	 *            {@link DocumentoBinario}
	 */
	public void setAntecedentesDocumento(final DocumentoBinario docBinario) {
		if (archivosAntecedentes != null) {
			for (ArchivoAdjuntoDocumentoBinario a : archivosAntecedentes) {
				a.setDocumentoBinario(docBinario);
			}
			docBinario.setArchivosAdjuntos(archivosAntecedentes);
		}
	}

	@Override
	public void eliminarDocumento(final Integer numDoc) {
		for (final Iterator<Documento> doc = listDocumentosRespuesta.iterator(); doc
				.hasNext();) {
			if (doc.next().getIdNuevoDocumento().equals(numDoc)) {
				doc.remove();
			}
		}
		if (listDocumentosRespuesta.size() == 0) {
			guardarExpediente = false;
		}
	}

	/**
	 * @param dc
	 *            {@link Documento}
	 */
	private void setDestinatarioDocumento(final Documento dc) {
		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		if (destinatariosDocumento.size() == 0) {
			// List<ListaPersonasDocumento> list = new
			// ArrayList<ListaPersonasDocumento>();
			DestinatarioDocumento personadocumento = new DestinatarioDocumento();
			personadocumento.setDestinatario(this.getDestinatario());
			personadocumento.setDocumento(dc);
			ddList.add(personadocumento);
			// documento.setDestinatarios(ddList);
		}
		else{
			for (SelectPersonas item : this.destinatariosDocumento) {
				final DestinatarioDocumento dd = new DestinatarioDocumento();
				dd.setDestinatario(item.getDescripcion());
				dd.setDestinatarioPersona(item.getPersona());
				dd.setDocumento(dc);
				ddList.add(dd);
			}
		}
		dc.addDestinatarios(ddList);
	}

	/**
	 * @param dc
	 *            {@link Documento}
	 */
	private void setDistribucionDocumento(final Documento dc) {
		final List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			final DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);

		}
		dc.setDistribucion(ddList);
	}

	@Override
	public void listener(final UploadEvent event) throws Exception {
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, DBS);
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivo.setNombreArchivo(fileName);
		archivo.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivo.setArchivo(data);
	}
	
	@Override
	public void listenerQuitar() {
		this.archivo = new ArchivoDocumentoBinario();
	}
	@Override
	public void clearUploadData() {
		log.info("[CrearExpediente] Limpiando archivo");
		archivo = new ArchivoDocumentoBinario();
	}
	/**
	 * Limpiar documento.
	 */
	private void limpiarDocumento() {
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		this.distribucionDocumento = new ArrayList<SelectPersonas>();
		this.archivo = new ArchivoDocumentoBinario();
		this.documentoBinario = new DocumentoBinario();
		this.documentoPapel = new DocumentoPapel();
		this.documentoPapel.setFechaDocumentoOrigen(null);		
		this.documentoBinario.setFechaDocumentoOrigen(null);
		final Calendar ahora = new GregorianCalendar();
		this.documentoBinario.setFechaDocumentoOrigen(ahora.getTime());
		this.documentoPapel.setFechaDocumentoOrigen(ahora.getTime());
		ahora.add(Calendar.DATE, 5);
		this.documentoPapel.setPlazo(null);
		this.documentoBinario.setPlazo(null);
		
		String emisor = this.usuario.getNombreApellido() + " - " +
				this.usuario.getCargo().getDescripcion() + " - " +
				this.usuario.getCargo().getUnidadOrganizacional().getDescripcion();
		this.documentoPapel.setEmisor(emisor);
		this.documentoBinario.setEmisor(emisor);
		
		this.tipoDocumentoID = 0;
		this.montoFactura = null;
	}

	/**
	 * @return {@link List}
	 */
	private List<SelectItem> buscarTipoDocumentos() {

		return TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoListVisible(null);
	}

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	private List<SelectItem> buscarDocumentosElectronicos() {
		return TipoDocumentosList.getInstanceTipoDocumentoElectronico(em)
				.getTiposDocumentoElectronicoVisible(SELECCIONAR);
	}

	/**
	 * carlos
	 * 
	 * @return {@link List}<{@link SelectItem}>
	 */
	private List<SelectItem> buscarDocumentosElectronicosResolucion() {
		return TipoDocumentosList.getInstanceTipoDocumentoElectronico(em)
				.getTiposDocumentoElectronicoVisibleComboCreacion(SELECCIONAR);
	}

	/**
	 * @return {@link List}<{@link SelectItem}>
	 */
	private List<SelectItem> buscarDocumentosNoElectronicos() {
		return TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoNoElectronicoVisible(SELECCIONAR);
	}

	/**
	 * 
	 */
	private void cargarNivelesUrgencia() {

		this.setIdAlerta(JerarquiasLocal.INICIO);

		final List<Alerta> alertas = administradorAlertas.buscarAlerta();
		this.setNivelesUrgencia(new ArrayList<SelectItem>());

		this.getNivelesUrgencia().add(
				new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR, SELECCIONAR, false, true));

		if (alertas != null) {
			for (Alerta a : alertas) {
				this.getNivelesUrgencia().add(
						new SelectItem(a.getId(), a.getNombre()));
			}
		}
	}

	/**
	 * @return {@link List}
	 */
	private List<SelectItem> buscarDocumentosAdjuntar() {
		return TipoDocumentosList.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoElectronicoList(SELECCIONAR);
	}

	@Begin(join = true)
	@Override
	public String adjuntarDocumentoElectronico() {
		// listDocumentosAnexo.clear();
		final int tipo = tipoDocumentoAdjuntar.intValue();
		tipoDocumentoAdjuntar = 0;
		String homeDistribucion = "";
		log.info("adjuntando Documento electronico: " + tipo);
		switch (tipo) {
		case 1:
			documento = new Oficio();
			homeDistribucion = "crearOficioOrdinario";
			break;
		case 2:
			documento = new Oficio();
			homeDistribucion = "crearOficioCircular";
			break;
		case 3:
			documento = new Oficio();
			homeDistribucion = "crearOficioReservado";
			break;
		case 6:
			documento = new Resolucion();
			hijas = new ArrayList<Resolucion>();
			final Vistos parrafo1 = new Vistos();
			parrafo1.setCuerpo("");
			parrafo1.setDocumento((Resolucion) documento);
			parrafo1.setNumero(1L);
			final Considerandos parrafo2 = new Considerandos();
			parrafo2.setCuerpo("");
			parrafo2.setDocumento((Resolucion) documento);
			parrafo2.setNumero(1L);
			final Resuelvo parrafo3 = new Resuelvo();
			parrafo3.setCuerpo("");
			parrafo3.setDocumento((Resolucion) documento);
			parrafo3.setNumero(1L);
			final List<Parrafo> parrafos1 = new ArrayList<Parrafo>();
			parrafos1.add(parrafo1);
			final List<Parrafo> parrafos2 = new ArrayList<Parrafo>();
			parrafos2.add(parrafo2);
			final List<Parrafo> parrafos3 = new ArrayList<Parrafo>();
			parrafos3.add(parrafo3);
			((Resolucion) documento).setVistos(parrafos1);
			((Resolucion) documento).setConsiderandos(parrafos2);
			((Resolucion) documento).setResuelvo(parrafos3);
			
			if(usuario.getCargo().getUnidadOrganizacional().getDepartamento()!= null &&
				usuario.getCargo().getUnidadOrganizacional().getDepartamento().getCiudad() != null)((Resolucion) documento).setCiudad(usuario.getCargo().getUnidadOrganizacional().getDepartamento().getCiudad());
			homeDistribucion = "crearResolucion"; 
			break;
		case 7:
			documento = new Memorandum();
			homeDistribucion = "crearMemorandum";
			break;
		case 8:
			documento = new Providencia();
			final List<AccionProvidencia> acciones = new ArrayList<AccionProvidencia>();
			acciones.add(new AccionProvidencia(-1));
			((Providencia) documento).setAcciones(acciones);
			homeDistribucion = "crearProvidencia";
			break;
		case 9:
			documento = new Decreto();
			final Vistos parrafo1Decreto = new Vistos();
			parrafo1Decreto.setCuerpo("");
			parrafo1Decreto.setDocumento((Decreto) documento);
			parrafo1Decreto.setNumero(1L);
			final Considerandos parrafo2Decreto = new Considerandos();
			parrafo2Decreto.setCuerpo("");
			parrafo2Decreto.setDocumento((Decreto) documento);
			parrafo2Decreto.setNumero(1L);
			final Resuelvo parrafo3Decreto = new Resuelvo();
			parrafo3Decreto.setCuerpo("");
			parrafo3Decreto.setDocumento((Decreto) documento);
			parrafo3Decreto.setNumero(1L);
			final List<Parrafo> parrafos1Decreto = new ArrayList<Parrafo>();
			parrafos1Decreto.add(parrafo1Decreto);
			final List<Parrafo> parrafos2Decreto = new ArrayList<Parrafo>();
			parrafos2Decreto.add(parrafo2Decreto);
			final List<Parrafo> parrafos3Decreto = new ArrayList<Parrafo>();
			parrafos3Decreto.add(parrafo3Decreto);
			((Decreto) documento).setVistos(parrafos1Decreto);
			((Decreto) documento).setConsiderandos(parrafos2Decreto);
			((Decreto) documento).setResuelvo(parrafos3Decreto);
			// idClasificacionSubTipoSelec = null;
			// idClasificacionTipoSelec = null;
			homeDistribucion = "crearDecreto";
			break;
		case 10:
			documento = new Carta();
			homeDistribucion = "crearCarta";
			break;
		// case 11:
		// documento = new Factura();
		// homeDistribucion = "crearFactura";
		// break;
		 case 15:
			 documento = new DiaAdministrativo();
//			 solicitante = null;
//			 calidadJuridica = "";
//			 grado = "";
//			 diasVacacionesPendientes = null;
//			 diasSolicitados = new ArrayList<DetalleDias>();
//			 datosObtenidos = null;
			 //((DiaAdministrativo)documento).setListDetalleDias(new ArrayList<DetalleDias>());
			 homeDistribucion = "crearSolDiaAdministrativo";
			 break;
		 case 16:
			 documento = new Vacaciones();
//			 solicitante = null;
//			 calidadJuridica = "";
//			 grado = "";
//			 diasVacacionesPendientes = null;
//			 diasSolicitados = new ArrayList<DetalleDias>();
//			 datosObtenidos = null;
			 homeDistribucion = "crearSolVacaciones";
			 break;
		// case 17:
		// documento = new Adquisicion();
		// ((Adquisicion) documento).setTipoAdquisicion(new TipoAdquisicion(1));
		// ((Adquisicion) documento).setTipoFinanciamientoAdquisicion(new
		// TipoFinanciamientoAdquisicion(1));
		// ((Adquisicion) documento).setConvenioMarco(false);
		// ((Adquisicion) documento).setOpcionSinConvenio(new
		// OpcionSinConvenioMarco(OpcionSinConvenioMarco.LICITACION));
		// ((Adquisicion) documento).setLicitacionPublica(false);
		// ((Adquisicion) documento).setLicitacionPrivada(true);
		// ((Adquisicion) documento).setTratoDirecto(false);
		// ((Adquisicion) documento).setAsistentesEventoAdquisicion(new
		// ArrayList<AsistenteEventoAdquisicion>());
		// ((Adquisicion) documento).setCriteriosEvaluacion(new
		// ArrayList<CriterioEvaluacion>());
		// homeDistribucion = "crearSolicitudAdquisicion";
		// break;
		case 22:
			documento = new Convenio();
			homeDistribucion = "crearConvenio";
			break;
		case 23:
			documento = new Contrato();
			homeDistribucion = "crearContrato";
			break;
		// case 24:
		// documento = new ActaAdjudicacion();
		// ((ActaAdjudicacion)documento).setComisionActaAdjudicacion(new
		// ArrayList<ComisionActaAdjudicacion>());
		// homeDistribucion = "crearActaAdjudicacion";
		// break;
		case 100:
			documento = new DocumentoPapel();
			homeDistribucion = "crearDocumentoPapel";
			break;
		case 200:
			documento = new DocumentoBinario();
			homeDistribucion = "crearDocumentoBinario";
			break;
		default:
			documento = new Documento();
			homeDistribucion = "";
			break;
		}

		observaciones = null;
		vistoDesdeReporte = null;

		final Calendar ahora = new GregorianCalendar();

		if (tipo == 11 || tipo == 15 || tipo == 16 || tipo == 17 || tipo == 200) {
			documento.setFechaDocumentoOrigen(ahora.getTime());
		}

		this.documentoBinario.setFechaDocumentoOrigen(ahora.getTime());
		documento.setFechaCreacion(ahora.getTime());
		ahora.add(Calendar.DATE, 5);
		//documento.setPlazo(ahora.getTime());

		documento.setAutor(usuario);
		this.setEmisorActual(documento);
		// tipo temporal, Siempre se toma como original el primero.
		documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(
				TipoDocumentoExpediente.RESPUESTA));
		// id temporal para borrar un documento
		documento.setIdNuevoDocumento(contadorDocumentos++);
		documento.setReservado(false);
		guardarExpediente = true;
		return homeDistribucion;
	}

	/**
	 * @param doc
	 *            {@link Documento}
	 */
	private void setEmisorActual(final Documento doc) {
		if (!(doc instanceof Providencia)) {
			this.setNombreEmisorActual(doc, this.usuario);
		} else {
			if (jerarquias.esJefe(this.usuario)) {
				this.setNombreEmisorActual(doc, this.usuario);
			} else {
				final List<Persona> jefes = jerarquias.getJefe(this.usuario);
				this.setNombreEmisorActual(doc, jefes.get(0));
			}
		}
	}

	/**
	 * @param doc
	 *            {@link Documento}
	 * @param per
	 *            {@link Persona}
	 */
	private void setNombreEmisorActual(final Documento doc, final Persona per) {
		if (per != null) {
			String value = per.getNombres() + " " + per.getApellidoPaterno();
			final Cargo cargo = per.getCargo();
			if ("Jefe Division".equals(cargo.getDescripcion())
					|| "Jefe Departamento".equals(cargo.getDescripcion())
					|| "Jefe Unidad".equals(cargo.getDescripcion())) {
				value = cargo.getDescripcion();
			} else {
				value += GUION + cargo.getDescripcion();
			}
			if (!per.getCargo().getUnidadOrganizacional().getVirtual()) {
				final UnidadOrganizacional uo = cargo.getUnidadOrganizacional();
				value += GUION + uo.getDescripcion();
			} else if (!per.getCargo().getUnidadOrganizacional()
					.getDepartamento().getVirtual()) {
				final Departamento depto = per.getCargo()
						.getUnidadOrganizacional().getDepartamento();
				value += GUION + depto.getDescripcion();
			} else {
				final Division d = per.getCargo().getUnidadOrganizacional()
						.getDepartamento().getDivision();
				value += GUION + d.getDescripcion();
			}
			doc.setEmisor(value);
			doc.setIdEmisor(per.getId());
		}
	}

	@Override
	public void valueChanged(final ValueChangeEvent event) {
		formato = (String) event.getNewValue();
		this.limpiarDocumento();
	}

	@Override
	public void verificaTipoDocumento(final ValueChangeEvent event) {
		tipoDocumentoID = (Integer) event.getNewValue();
		esFactura = false;
	}

	@End
	@Override
	public String end() {
		log.info("ending conversation: crearExpediente");
		return "home";
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public String getFormato() {
		return formato;
	}

	@Override
	public void setFormato(final String formato) {
		this.formato = formato;
	}

	@Override
	public List<SelectItem> getListTipoDocumentos() {
		return listTipoDocumentos;
	}

	@Override
	public List<SelectItem> getListDocumentosElectronicos() {
		return listDocumentosElectronicos;
	}

	@Override
	public List<SelectItem> getListDocumentosNoElectronicos() {
		return listDocumentosNoElectronicos;
	}

	@Override
	public Integer getTipoDocumentoID() {
		return tipoDocumentoID;
	}

	@Override
	public void setTipoDocumentoID(final Integer tipoDocumentoID) {
		this.tipoDocumentoID = tipoDocumentoID;
	}

	@Override
	public List<SelectPersonas> getDestinatariosDocumento() {
		return destinatariosDocumento;
	}

	@Override
	public List<SelectPersonas> getDistribucionDocumento() {
		return distribucionDocumento;
	}

	@Override
	public Integer getTipoDocumentoAdjuntar() {
		return tipoDocumentoAdjuntar;
	}

	@Override
	public void setTipoDocumentoAdjuntar(final Integer tipoDocumentoAdjuntar) {
		this.tipoDocumentoAdjuntar = tipoDocumentoAdjuntar;
	}

	@Override
	public List<SelectItem> getListaDocumentosAdjuntar() {
		return listaDocumentosAdjuntar;
	}

	@Override
	public ArchivoDocumentoBinario getArchivo() {
		return archivo;
	}

	@Override
	public void setArchivo(final ArchivoDocumentoBinario archivo) {
		this.archivo = archivo;
	}

	@Override
	public DocumentoPapel getDocumentoPapel() {
		return documentoPapel;
	}

	@Override
	public void setDocumentoPapel(final DocumentoPapel documentoPapel) {
		this.documentoPapel = documentoPapel;
	}

	@Override
	public DocumentoBinario getDocumentoBinario() {
		return documentoBinario;
	}

	@Override
	public void setDocumentoBinario(final DocumentoBinario documentoBinario) {
		this.documentoBinario = documentoBinario;
	}

	@Override
	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	@Override
	public void setFechaIngreso(final Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	@Override
	public boolean getGuardarExpediente() {
		return guardarExpediente;
	}

	@Override
	public List<Documento> getListDocumentosRespuesta() {
		return listDocumentosRespuesta;
	}

	@Override
	public boolean getEsFactura() {
		return esFactura;
	}

	@Override
	public Long getMontoFactura() {
		return montoFactura;
	}

	@Override
	public void setMontoFactura(final Long montoFactura) {
		this.montoFactura = montoFactura;
	}

	@Override
	public String despacharExpediente() {
		if (listDestinatarios.size() != 0) {
			this.agregarDocumentoBinario();
			if (expediente == null || expediente.getId() == null) {
				this.distribuirDocumentos();
			}
			this.guardarExpediente();
			this.end();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			FacesMessages
					.instance()
					.getCurrentMessages()
					.add(new FacesMessage(
							"Debe Seleccionar un Destinatario del Expediente"));
			return "";
		}
	}

	/**
	 * Guardar expediente.
	 */
	@End
	private void guardarExpediente() {
		expediente = new Expediente();
		expediente.setFechaIngreso(new Date());
		expediente.setEmisor(usuario);
		expediente.setObservaciones(null);
		me.crearExpediente(expediente);
		try {
			me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
		} catch (DocumentNotUploadedException e1) {
			log.error(ERROR, e1);
		}

		for (Documento doc : listDocumentosRespuesta) {
			try {
				if (doc.getId() == null) {
					me.agregarRespuestaAExpediente(expediente, doc, true);
				} else {
					md.actualizarDocumento(doc);
				}
			} catch (DocumentNotUploadedException e) {
				log.error(ERROR, e);
			}
		}

		idExpedientes = new ArrayList<Long>();
		final Date fechaIngresoExp = new Date();

		for (Persona de : listDestinatarios) {
			try {
				idExpedientes.add(me.agregarDestinatarioAExpediente(
						expediente, usuario, de, fechaIngresoExp));
			} catch (DocumentNotUploadedException e) {
				log.error(ERROR, e);
			}
		}

		final List<Expediente> expedientes = new ArrayList<Expediente>();

		for (Long id : idExpedientes) {
			final Expediente e = me.buscarExpediente(id);
			me.despacharExpediente(e);
			expedientes.add(e);
		}
		me.despacharExpediente(expediente);
		//me.despacharNotificacionPorEmail(expedientes);

		FacesMessages.instance().add("Expediente Despachado");
	}

	/*
	 * private Long buscaDocumentoReferencia(Integer idDocumento) { Long id =
	 * null; for (Documento doc : listDocumentosRespuesta) { if
	 * (doc.getIdNuevoDocumento().equals(idDocumento)) { id = doc.getId();
	 * break; } } if
	 * (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) { id =
	 * documentoOriginal.getId(); } return id; }
	 */

	/**
	 * Inicializa Archivos Antecedentes.
	 */
	private void inicializaArchivosAntecedentes() {
		if (archivosAntecedentes == null) {
			archivosAntecedentes = new ArrayList<ArchivoAdjuntoDocumentoBinario>();
		}
	}

	/**
	 * Limpiar anexos.
	 */
	private void limpiarAnexos() {
		archivoAdjuntoTemporal = null;
		materiaArchivo = null;
	}

	/**
	 * @return {@link Integer}
	 */
	public int getMaxFilesQuantity() {
		// int valor = 20;
		return 20;
	}

	@Override
	public void listenerAntecedentes(final UploadEvent event) throws Exception {
		this.inicializaArchivosAntecedentes();
		archivoAdjuntoTemporal = new ArchivoAdjuntoDocumentoBinario();
		final UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		final StringTokenizer st = new StringTokenizer(fileName, DBS);
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		final String contentType = item.getContentType();
		archivoAdjuntoTemporal.setNombreArchivo(fileName);
		archivoAdjuntoTemporal.setContentType(contentType);
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		archivoAdjuntoTemporal.setArchivo(data);
	}

	@Override
	public void clearUploadAntecedente() {
		archivoAdjuntoTemporal = null;
	}

	@Override
	public void agregarAntecedentes() {
		if (archivoAdjuntoTemporal == null) {
			FacesMessages.instance().add(
					"Debe adjuntar un archivo para los antecedentes");
			return;
		}
		if (!this.existeNombreArchivo(archivoAdjuntoTemporal)) {
			archivoAdjuntoTemporal.setFecha(new Date());
			archivoAdjuntoTemporal.setIdNuevoArchivo(archivosAntecedentes
					.size());
			archivoAdjuntoTemporal.setAdjuntadoPor(usuario);
			archivoAdjuntoTemporal.setMateria(materiaArchivo);
			archivosAntecedentes.add(archivoAdjuntoTemporal);
		} else {
			FacesMessages.instance().add(
					"El Nombre de Archivo ya existe como adjunto");
		}
		this.limpiarAnexos();
	}
	@Override
	public ArchivoAdjuntoDocumentoBinario getArchivoAdjuntoTemporal() {
		return archivoAdjuntoTemporal;
	}

	@Override
	public void setArchivoAdjuntoTemporal(ArchivoAdjuntoDocumentoBinario doc) {
		archivoAdjuntoTemporal = doc;
	}
	/**
	 * Metodo que valida la existencia del nombre del archivo.
	 * 
	 * @param archivoAdjunto
	 *            {@link ArchivoAdjuntoDocumentoBinario}
	 * @return {@link Boolean}
	 */
	private boolean existeNombreArchivo(
			final ArchivoAdjuntoDocumentoBinario archivoAdjunto) {
		return archivosAntecedentes.contains(archivoAdjunto);
	}

	@Override
	public void verArchivoAntecedente(final Long idArchivo) {
		final ArchivoAdjuntoDocumentoBinario a = em.find(
				ArchivoAdjuntoDocumentoBinario.class, idArchivo);
		this.buscarArchivoRepositorio(a);
	}

	/**
	 * @param a
	 *            {@link Archivo}
	 */
	private void buscarArchivoRepositorio(final Archivo a) {
		// byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		try {
			final byte[] data = repositorio.getFile(a.getCmsId());
			if (data != null) {
				final FacesContext facesContext = FacesContext
						.getCurrentInstance();
				try {
					final HttpServletResponse response = (HttpServletResponse) facesContext
							.getExternalContext().getResponse();
					response.setContentType(a.getContentType());
					response.setHeader("Content-disposition",
							"attachment; filename=\"" + a.getNombreArchivo()
									+ "\"");
					response.getOutputStream().write(data);
					response.getOutputStream().flush();
					facesContext.responseComplete();
				} catch (IOException e) {
					log.error(ERROR, e);
				}
			} else {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: "
								+ a.getId() + ")");
			}
		} catch (XPathExpressionException e) {
			log.error(ERROR, e);
		} catch (IOException e) {
			log.error(ERROR, e);
		}
	}

	@Override
	public void eliminarArchivoAntecedente(final Integer idNuevoArchivo) {
		/*
		 * if (this.archivosAntecedentesEliminados == null) {
		 * this.archivosAntecedentesEliminados = new
		 * ArrayList<ArchivoAdjuntoDocumentoBinario>(); }
		 */

		for (ArchivoAdjuntoDocumentoBinario a : archivosAntecedentes) {
			if (a.getIdNuevoArchivo().equals(idNuevoArchivo)) {
				/*
				 * if (a.getId() != null) {
				 * this.archivosAntecedentesEliminados.add(a); }
				 */
				archivosAntecedentes.remove(a);
				break;
			}
		}

		if (this.documento != null) {
			if (this.documento.getId() != null) {
				em.merge(this.documento);
				em.flush();
			}
		}
	}

	@Override
	public List<ArchivoAdjuntoDocumentoBinario> getListaArchivosAntecedente() {
		/*
		 * if (archivosAntecedentes != null) { int cont = 0; for
		 * (ArchivoAdjuntoDocumentoBinario a : archivosAntecedentes) {
		 * a.setIdNuevoArchivo(cont++); }
		 */
		return archivosAntecedentes;
		/*
		 * } return new ArrayList<ArchivoAdjuntoDocumentoBinario>();
		 */
	}

	@Override
	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	@Override
	public void setMateriaArchivo(final String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	@Override
	public void validarContraFechaDoc(final DocumentoBinario docBin) {
		if (docBin.getPlazo() != null && docBin.getPlazo().before(docBin.getFechaDocumentoOrigen())) {
			FacesMessages.instance().add(
					"El plazo no puede ser menor a la fecha del documento");
			return;
		}
	}

	/**
	 * @return {@link List} of {@link ArchivoAdjuntoDocumentoBinario}
	 */
	public List<ArchivoAdjuntoDocumentoBinario> getArchivosAntecedentes() {
		return archivosAntecedentes;
	}

	@Override
	public Boolean getAgregarDestinatarioDocumento() {
		return agregarDestinatarioDocumento;
	}

	@Override
	public void setAgregarDestinatarioDocumento(
			final Boolean agregarDestinatarioDocumento) {
		this.agregarDestinatarioDocumento = agregarDestinatarioDocumento;
	}

	@Override
	public final List<SelectItem> getNivelesUrgencia() {
		return nivelesUrgencia;
	}

	@Override
	public final void setNivelesUrgencia(final List<SelectItem> nivelesUrgencia) {
		this.nivelesUrgencia = nivelesUrgencia;
	}

	@Override
	public final Long getIdAlerta() {
		return idAlerta;
	}

	@Override
	public final void setIdAlerta(final Long idAlerta) {
		this.idAlerta = idAlerta;
	}

	@Override
	public void setValidarComplete(final boolean validarComplete) {
		this.validarComplete = validarComplete;
	}

	@Override
	public boolean isValidarComplete() {
		return validarComplete;
	}
	@Override
	public List<Persona> autocomplete(Object suggest) {
		
		final String pref = (String) suggest;
		
		List<Persona> personas = adminPersona.getPersonas(pref, destinatariosDocumento);
		
		return personas;
	}

	
	public void seleccionarDest(Persona p) {
		log.info("destinatario"+destinatario);
		SelectPersonas select = new SelectPersonas(p.getNombreApellido()+" - "+p.getCargo().getDescripcion()+" - "+p.getCargo().getUnidadOrganizacional().getDescripcion(), p);
		if(!destinatariosDocumento.contains(select)){
			destinatariosDocumento.add(select);
		}
		else{
			FacesMessages.instance().add(NO_PUEDE_AGREGAR);
			return;
		}
	}
	
	public void validarTexto(ValueChangeEvent event) {
		         String texto = ((String)event.getNewValue());
		         
		        log.info("texto: "+texto);
	 }
	@Override
	public String getDestinatario() {
		return destinatario;
	}
	@Override
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	
}
