package cl.exe.exedoc.session.exception;

public class RUTException extends java.lang.Exception {

    /**
     * Creates new <code>RUTException</code> without detail message.
     */
    public RUTException() {
        super("Hay problemas con el RUT");
    }


    /**
     * Constructs an <code>RUTException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public RUTException(String msg) {
        super(msg);
    }
}
