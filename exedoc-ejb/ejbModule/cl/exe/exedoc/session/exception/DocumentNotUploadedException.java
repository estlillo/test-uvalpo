/**
 * 
 */
package cl.exe.exedoc.session.exception;

import javax.ejb.ApplicationException;

/**
 * @author jnova
 *
 */
@ApplicationException(rollback = true)
public class DocumentNotUploadedException extends Exception {

   private static final long serialVersionUID = -946029637812116942L;

    /**
     * Constructor.
     */
    public DocumentNotUploadedException() {
        super("Documento no subido a Alfresco");
    }

    /**
     * @param message
     */
    public DocumentNotUploadedException(final String message) {
        
    }

    /**
     * @param cause
     */
    public DocumentNotUploadedException(final Throwable cause) {
    }

    /**
     * @param message
     * @param cause
     */
    public DocumentNotUploadedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
