package cl.exe.exedoc.session.exception;

public class FoliadorException extends Exception {

	private static final long serialVersionUID = 3788969028497668081L;

	public FoliadorException(String msg) {
		super(msg);
	}

}
