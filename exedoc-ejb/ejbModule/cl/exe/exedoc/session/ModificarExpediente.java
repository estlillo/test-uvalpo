package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * Interface Modificar Expediente.
 * 
 * @author Ricardo
 * 
 */
@Local
public interface ModificarExpediente {

	/**
	 * Inicia el EJB, con la configuración basica.
	 * 
	 * @return {@link String}
	 * @throws DocumentNotUploadedException
	 *             documento no Upload.
	 */
	String begin() throws DocumentNotUploadedException;

	public String end();

	public void destroy();

	public void getDocumentoOriginalBinario();

	public Expediente getExpediente();

	public void setExpediente(Expediente expediente);

	public Long getIdExpediente();

	public void setIdExpediente(Long idExpediente);

	public Object[] getListDestinatarios();

	public Long getDivision();

	public void setDivision(Long division);

	public Long getDepartamento();

	public void setDepartamento(Long departamento);

	public Long getUnidadOrganizacional();

	public void setUnidadOrganizacional(Long unidadOrganizacional);

	public Long getCargo();

	public void setCargo(Long cargo);

	public List<SelectItem> getListDivision();

	public List<SelectItem> getListDepartamento();

	public List<SelectItem> getListUnidadesOrganizacionales();

	public List<SelectItem> getListCargos();

	public List<SelectItem> getListPersonas();

	public void buscarDepartamentos();

	public void buscarUnidadesOrganizacionales();

	public void buscarCargos();

	public void buscarPersonas();

	public Long getPersona();

	public void setPersona(Long persona);

	public void agregarDestinatario();

	public void registrarExpediente() throws DocumentNotUploadedException;

	public void agregarObservacion();

	// public List<Documento> getListDocumentosAnexo();
	// public void setListDocumentosAnexo(
	// List<Documento> listDocumentosAnexo);
	public List<Documento> getListDocumentosRespuesta();

	public void setListDocumentosRespuesta(
			List<Documento> listDocumentosRespuesta);

	public String getObservacion();

	public void setObservacion(String observacion);

	public List<Observacion> getObservaciones();

	public void setObservaciones(List<Observacion> observaciones);

	/**
	 * Metodo que despacha expediente.
	 * 
	 * @return {@link String}
	 * @throws DocumentNotUploadedException
	 *             no upload archivo alfresco.
	 */
	public String despacharExpediente() throws DocumentNotUploadedException;

	public String archivarExpediente() throws DocumentNotUploadedException;

	public List<SelectItem> getDestinatariosDocumentoOriginal();

	public Integer getTipoDocumentoAdjuntar();

	public void setTipoDocumentoAdjuntar(Integer tipoDocumentoAdjuntar);

	public String adjuntarDocumento(int tipoAdjuntar);

	public List<SelectItem> getListaDocumentosAdjuntar();

	public List<SelectItem> getListaDocumentosAnexar();

	public void eliminarDocumento(Integer numDoc);

	public void eliminarDestinatariosExpediente(Persona destinatario);

	public void eliminarObservaciones(Integer idNuevaObservacion);

	// public void eliminarAnexoDocumento(Integer numDoc);

	public String modificarDocumento(Integer numDoc, Integer lista);

	public Documento getDocumentoOriginal();

	public void setDocumentoOriginal(Documento documentoOriginal);

	public void verArchivo();

	public void verArchivo(Long idArchivo);

	public String verNombreArchivoObservacion(Observacion obs);

	public void verArchivoObservacion(Archivo a);

	public void agregarArchivoOriginal();

	public void listener(UploadEvent event) throws Exception;

	public void listenerObs(UploadEvent event) throws Exception;

	public String getMateriaArchivo();

	public void setMateriaArchivo(String materiaArchivo);

	public int getCopiaDirectoFlag();

	public void setCopiaDirectoFlag(int copiaDirectoFlag);

	public List<DestinatariosFrecuentes> getListListas();

	public void setListListas(List<DestinatariosFrecuentes> listListas);

	public void agregarListaDestinatarios(Long idLista);

	public void agregarAntecedenteArchivo();

	public void valueChangeObservacion(ValueChangeEvent event);

	public boolean renderedVisualizarExpediente();

	public Long getGrupo();

	public void setGrupo(Long grupo);

	public List<SelectItem> getListDestFrec();

	public void setListDestFrec(List<SelectItem> listDestFrec);

	public boolean renderedGrupoDestinatariosFrecuentes();

	public void buscarDivisiones();

	public Long getOrganizacion();

	public void setOrganizacion(Long organizacion);

	public List<SelectItem> getListOrganizacion();

	public void setListOrganizacion(List<SelectItem> listOrganizacion);

	// Reenvio de expedientes
	public List<SelectPersonas> getDestinatariosDocumento();

	public String reenviarExpediente() throws DocumentNotUploadedException;

	public boolean isDocValido();

	public void setDocValido(boolean docValido);

	public void marcarDoc();

	/**
	 * @return {@link Long}
	 */
	Long getCodigoBarra();

	/**
	 * @param codigoBarra
	 *            {@link Long}
	 */
	void setCodigoBarra(final Long codigoBarra);

	/**
	 * @param doc
	 *            {@link Documento}
	 */
	void cargarCodigoBarra(final Documento doc);

	/**
	 * @return {@link String}
	 */
	String cancelarExpediente();

	void buscarGrupos();

	List<SelectItem> getFiscalias();

	void setFiscalias(List<SelectItem> fiscalias);

	Long getFiscalia();

	void setFiscalia(Long fiscalia);

	void setListGrupos(List<SelectItem> listGrupos);

	List<SelectItem> getListGrupos();

	Long getGrupoUsuario();

	void setGrupoUsuario(Long grupoUsuario);

	//void eliminarGrupoUsuario(String nombre);

	List<GrupoUsuario> getListGrupoUsuarios();

	void setListGrupoUsuarios(List<GrupoUsuario> listGrupoUsuarios);

	void agregarGrupoUsuario();

	boolean esArchivable();

	boolean obtenerTipoDocumento();
	
	public boolean logicaDespacho();

	boolean logicaDespachoReenviar();

	void eliminarGrupoUsuario(GrupoUsuario g);

	String getVerArchivoTitulo();

	void setVerArchivoTitulo(String verArchivoTitulo);

	String getVerArchivoOriginal();

	void setVerArchivoOriginal(String verArchivoOriginal);

	String getVisualizar();

	void setVisualizar(String visualizar);


}
