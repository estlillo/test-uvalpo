package cl.exe.exedoc.session;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.SolicitudDiaAdministrativo;
import cl.exe.exedoc.entity.SolicitudVacaciones;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.entity.Vacaciones;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.session.utils.InformacionPersonal;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;
import cl.exe.externos.DatosDiasAdministrativosManager;

@Stateful
@Name("crearSolVacaciones")
public class CrearSolVacacionesBean implements CrearSolVacaciones {

	@In(required = false, scope = ScopeType.PAGE)
	@Out(required = false, scope = ScopeType.PAGE)
	private InformacionPersonal informacionPersonal;

	@In(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "idPlantilla", scope = ScopeType.CONVERSATION)
	private Integer idPlantillaSelec;
	
	private InformacionPersonalSrv informacionPersonalSrv;
	
	private Persona funcionario;

	private boolean diasOk;
	private String tipoDia;

	private DetalleDiasVacaciones detalleDiasVacaciones;
	
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

	@EJB
	private JerarquiasLocal jerarquias;
	
	@EJB
	private DatosDiasAdministrativosManager datosDiasAdministrativos;
	
//	@EJB
//	private Authenticator auth;

	@In(required = false)
	private Customizacion customizacion;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

//	private Long cantidadIntentos;
	
//	@In(required = false, scope = ScopeType.CONVERSATION)
//	private ListaDocumentos listaDocumentos;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	private Editor editor;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private ExpedienteDocumento expedienteDocumento;
//
//	private ArrayList<Long> idExpedientes;
//
//	private boolean guardado;
//	
//
//	private List<ArchivoAdjuntoDocumentoElectronico> archivosAnexos;
//	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;
//	private String materiaArchivo;
//	
//	private Long timeout;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Vacaciones vacaciones;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private String parrafo;

	private Boolean firmado;
	private Boolean visado;
	private Boolean completado;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	private List<Long> idExpedientes;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;
	
	private boolean flagInformacion = false;
	
	private double obtenerDias(Date fechaInicio, Date fechaTermino) {
		double count = 0D;
		long dia = 1000 * 60 * 60 * 24;
		long timeInicio = fechaInicio.getTime();
		long timeFin = fechaTermino.getTime();

		Calendar cale = Calendar.getInstance();

		while (timeInicio <= timeFin) {
			cale.setTimeInMillis(timeInicio);
			int day = cale.get(Calendar.DAY_OF_WEEK);
			Date fecha = cale.getTime();
			System.out.println(fecha);
			if (!(day == Calendar.SATURDAY || day == Calendar.SUNDAY)) {
				if (!datosDiasAdministrativos.esFeriado(fecha)) {
					if (vacacionesDiaAdminYaPedido(fecha)) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
						FacesMessages.instance().add("El dia " + sdf.format(fecha) + " ya esta pedido como día administrativo");
					}
					count++;
				}
			}
			System.out.println(count);
			timeInicio += dia;
		}

		log.info("Dias pedidos: " + count);
		return count;
	}
	
	/*@SuppressWarnings("unchecked")
	private boolean esFeriado(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		Query query = em.createNamedQuery("Feriados.esFeriado");
		query.setParameter("dia", cal.get(Calendar.DAY_OF_MONTH));
		query.setParameter("mes", cal.get(Calendar.MONTH) + 1);
		query.setParameter("agno", cal.get(Calendar.YEAR));
		List<Feriados> feriados = query.getResultList();
		return (feriados.size() > 0) ? true : false;
//		StringBuilder feriadosQuery = new StringBuilder("SELECT fecha FROM feriados");
//		List<Date> feriados = em.createNativeQuery(feriadosQuery.toString()).getResultList();
//		for(Date dia:feriados){
//			System.out.println("");
//			System.out.println("Comparando");
//			System.out.println(dia);
//			System.out.println("con");
//			System.out.println(fecha);
//			System.out.println("Resultado:");
//			System.out.println(dia.compareTo(fecha)==0);
//			System.out.println("Resultado Anterior:");
//			System.out.println(feriados.contains(fecha));
//			System.out.println("");
//
//			if(dia.compareTo(fecha)==0){
//				return true;
//			}
//		}
//		
//		return feriados.contains(fecha);
	}*/
	
	@SuppressWarnings("unchecked")
	private boolean vacacionesDiaAdminYaPedido (Date dia) {
		if (usuario.getId() != null) {
			String hql = null;
			Query query = em.createNamedQuery("SolicitudDiaAdministrativo.findActiveSolicitud");
			query.setParameter("solicitante", usuario);
			List<SolicitudDiaAdministrativo> solicitudes = query.getResultList();
			for (SolicitudDiaAdministrativo solDiaAdm : solicitudes) {
				if (solDiaAdm.getDiaAdministrativoSolicitud() != null && 
						solDiaAdm.getDiaAdministrativoSolicitud().getListDetalleDias() != null)
				for (DetalleDias diaAdmPedido : solDiaAdm.getDiaAdministrativoSolicitud().getListDetalleDias()) {
					if (diaAdmPedido.getFechaDia().equals(dia)) {
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	public String agregarDocumento() {
		boolean estabaRechazado = isRenderedMensajeRechazado();
		if (validarSolicitud(vacaciones.getSolicitudVacaciones())) {
			armaDocumento(false, false);
			diasOk = true;
			if (estabaRechazado || expediente == null || expediente.getId() == null) {
				return despacharExpediente();
			}
			return end();
		}
		return "";
	}

	private SolicitudVacaciones crearSolicitudVacaciones() {
		SolicitudVacaciones solicitud = new SolicitudVacaciones();
		solicitud.setCantDiasSolicitados(vacaciones.getDiasSolicitadosVacaciones());
		solicitud.setSolicitante(vacaciones.getFuncionario());
		solicitud.setVacaciones(vacaciones);
		return solicitud;
	}

	private void modificarSolicitudVacaciones(SolicitudVacaciones solicitud) {
		solicitud.setCantDiasSolicitados(vacaciones.getDiasSolicitadosVacaciones());
		solicitud.setSolicitante(vacaciones.getFuncionario());
		solicitud.setRechazado(null);
		solicitud.setAceptado(null);
	}

	public Integer getTipoDocumento(){
		return TipoDocumento.SOLICITUD_VACACIONES;
	}
	
	public void poblarDocumento(){
		
	}
	
	@SuppressWarnings("unchecked")
	private void armaDocumento(boolean visar, boolean firmar) {
		if (!validarSolicitud(null))
			return;
		//obtenerJefatura(null);
		obtenerJefatura(usuario);
		vacaciones.setMateria("Solicitud de Vacaciones - " + informacionPersonal.getSolicitante().getNombreApellido());

		vacaciones.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		vacaciones.setTipoDocumento(new TipoDocumento(TipoDocumento.SOLICITUD_VACACIONES));

		bitacora(visar,firmar);
		
		if (vacaciones.getCompletado() == null) {
			vacaciones.setCompletado(false);
		}

		vacaciones.setReservado(false);

		this.limpiarDestinatarios(this.vacaciones.getId());
		this.setDestinatarioDocumento(vacaciones);

		vacaciones.setFuncionario(usuario);
		vacaciones.setRechazadoVacaciones(null);
		vacaciones.setAceptadoVacaciones(null);
		vacaciones.setAceptadoVacacionesRRHH(null);

//		/* TODO */
//		Documento tmp=listaDocumentos.actualizaListasDocumentos2(getDocumento());
//		if(tmp!=null) {
//			documentoOriginal(tmp);
//		}

		if (vacaciones.getId() == null) {
			SolicitudVacaciones solVacaciones = crearSolicitudVacaciones();
			//em.persist(solVacaciones);
			vacaciones.setSolicitudVacaciones(solVacaciones);
			//em.persist(vacaciones);
		} else {
			//String hql = "select sv from SolicitudVacaciones sv where sv.id= ?";
			//Query query = em.createQuery(hql);
			//query.setParameter(1, vacaciones.getSolicitudVacaciones().getId());
			//SolicitudVacaciones solVaca = (SolicitudVacaciones) query.getSingleResult();
			

			//String hql2 = "select ddv from DetalleDiasVacaciones ddv where ddv.solicitudVacaciones= ?";
			//Query query2 = em.createQuery(hql2);
			//query2.setParameter(1, solVaca);
			//List<DetalleDiasVacaciones> listDetalleDias = query2.getResultList();
			//for (DetalleDiasVacaciones dd : listDetalleDias) {
			//	em.remove(dd);
			//}
			
			SolicitudVacaciones solVaca = vacaciones.getSolicitudVacaciones();
			solVaca.setCantDiasSolicitados(vacaciones.getDiasSolicitadosVacaciones());
			solVaca.setSolicitante(vacaciones.getFuncionario());
			solVaca.setRechazado(null);
			solVaca.setAceptado(null);
			//modificarSolicitudVacaciones(solVaca);
			if (solVaca.getListDetalleDiasVacaciones() != null) {
				Iterator<DetalleDiasVacaciones> iddv = solVaca.getListDetalleDiasVacaciones().iterator();
				while (iddv.hasNext()) 
					eliminaDetalleDiasVacaciones(iddv.next().getId());
			}
			//em.merge(solVaca);
			vacaciones.setSolicitudVacaciones(solVaca);
		}

		this.detalleDiasVacaciones = new DetalleDiasVacaciones();
		this.detalleDiasVacaciones.setFechaInicio(vacaciones.getFechaInicioVacaciones());
		this.detalleDiasVacaciones.setFechaTermino(vacaciones.getFechaTerminoVacaciones());
		this.detalleDiasVacaciones.setSolicitudVacaciones(vacaciones.getSolicitudVacaciones());
		vacaciones.getSolicitudVacaciones().setListDetalleDiasVacaciones(new ArrayList<DetalleDiasVacaciones>());
		vacaciones.getSolicitudVacaciones().getListDetalleDiasVacaciones().add(this.detalleDiasVacaciones);
		this.informacionPersonal.setSolicitud(true);
		
		if (this.vacaciones.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.vacaciones);
			} else {
				if (this.vacaciones.getId() != null) {
					listDocumentosRespuesta.remove(this.vacaciones);
					listDocumentosRespuesta.add(this.vacaciones);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} else if (this.vacaciones.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = vacaciones;
		}
		
		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		
		persistirDocumento();
	}

	
	private void obtenerJefatura(Persona solicitante) {
		Persona persona = null;
		if (solicitante != null) {
			persona = solicitante;
		}
		else {
			if (informacionPersonal.getSolicitante() != null) {
				persona = informacionPersonal.getSolicitante();
			}
			else{
				persona = this.usuario;
			}
		}
		//Persona aprobadorActosAdministrativos = persona.getAprobadorActosAdministrativos();
		Persona aprobadorActosAdministrativos = null;
		List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
		if (listJefesRRHH != null) {
			for (Persona p : listJefesRRHH) {
				if (!this.usuario.getId().equals(p.getId())) {
					aprobadorActosAdministrativos = p;
					break;
				}
			}
			if (aprobadorActosAdministrativos != null) {
				listDestinatarios = new HashSet<Persona>();
				listDestinatarios.add(aprobadorActosAdministrativos);
				destinatariosDocumento = new LinkedList<SelectPersonas>();
				agregarDestinatarioDocumento(aprobadorActosAdministrativos);
			}
		}
	}
	
	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (informacionPersonal != null) {
			if (informacionPersonal.getDiasVacacionesPendientes() != null) {
				if (informacionPersonal.getDiasVacacionesPendientes().doubleValue() < 0) {
					return false;
				}
			}
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) {
			return false;
		}
		if (expediente != null) {
			if (vacaciones.getId() != null) {
				if (vacaciones.getRechazadoVacaciones() == null || 
						(vacaciones.getRechazadoVacaciones() != null && vacaciones.getRechazadoVacaciones())) {
					if (!vacaciones.getAutor().getId().equals(usuario.getId())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public boolean validarSolicitud(SolicitudVacaciones solicitudVacaciones) {
		if (!tieneSolicitudEnCurso(solicitudVacaciones)) {
			preProceso();
			/*
			 * SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); Date
			 * hoy = null; try { hoy = sdf.parse(sdf.format(new Date())); }
			 * catch (ParseException e) { e.printStackTrace(); }
			 */
			if (this.detalleDiasVacaciones != null && vacaciones.getFechaInicioVacaciones() != null) {
				if (vacaciones.getFechaTerminoVacaciones().before(vacaciones.getFechaInicioVacaciones())) {
					FacesMessages.instance().add("La fecha de término no puede ser anterior a la fecha de inicio.");
					return false;
				}
				
				double cantDiasSol = 0;

				if (obtenerDias(this.detalleDiasVacaciones.getFechaInicio(), this.detalleDiasVacaciones.getFechaTermino()) <= 15) {
					cantDiasSol = obtenerDias(this.detalleDiasVacaciones.getFechaInicio(), this.detalleDiasVacaciones.getFechaTermino());
				}
				/*
				if ((this.diasVacacionesPendientes - cantDiasSol) >= 0) {
					if (cantDiasSol >= 10) {
						diasOk = true;
						return true;
					} else if (this.diasVacacionesPendientes - cantDiasSol >= 10) {
						diasOk = true;
						return true;
					} else if (!vacacionesMinimasYaPedidas()) {
						
					}
				}*/
				



                                /* 
				if(this.diasVacacionesPendientes>=10 && cantDiasSol<10){
					FacesMessages.instance().add("Su solicitud no debe ser menor a 10 días");
					diasOk = false;
					return false;
				}
                                */
				
				if (cantDiasSol <= 15 && (informacionPersonal.getDiasVacacionesPendientes() - cantDiasSol) >= 0) {
					diasOk = true;
					return true;
				} else {
					FacesMessages.instance().add("La cantidad de días solicitados supera el máximo o los disponibles");
					diasOk = false;
					return false;
				}
			} else {
				// FacesMessages.instance().add("La fecha de inicio no puede ser anterior a la fecha actual.");
				return false;
			}
		} else {
			FacesMessages.instance().add("Usted no puede generar una solicitud, mientras mantenga otra en curso");
			diasOk = false;
			return false;
		}
	}
	
	/*private boolean vacacionesMinimasYaPedidas () {
		return false;
	}*/


	
	public void preProceso(){
		if (this.detalleDiasVacaciones == null) {
			this.detalleDiasVacaciones = new DetalleDiasVacaciones();
		}
		this.detalleDiasVacaciones.setFechaInicio(vacaciones.getFechaInicioVacaciones());
		this.detalleDiasVacaciones.setFechaTermino(vacaciones.getFechaTerminoVacaciones());
	}
	
	public void anularVariables(){
		diasOk=true;
	}
	
	public boolean validacion(){
		return validarSolicitud(null);
	}

	@SuppressWarnings("unchecked")
	private boolean tieneSolicitudEnCurso(SolicitudVacaciones solicitudVacaciones) {
		if (usuario.getId() != null) {
			Query query;
			if (solicitudVacaciones == null || solicitudVacaciones.getId() == null) {
				query = em.createNamedQuery("SolicitudVacaciones.findActiveSolicitud");
				query.setParameter("solicitante", usuario);
			} else {
				query = em.createNamedQuery("SolicitudVacaciones.findActiveSolicitudExcludeId");
				query.setParameter("solicitante", usuario);
				query.setParameter("id", solicitudVacaciones.getId());
			}
			List<SolicitudVacaciones> solicitudes = query.getResultList();
			return (solicitudes.size() > 0) ? true : false;
		}
		return false;
	}

	public boolean isRenderedBotonAceptarRechazar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (expediente != null && expediente.getId() != null  && vacaciones!=null) {
			Vacaciones vacacionesDoc = new Vacaciones();
			vacacionesDoc = vacaciones;
			//System.out.println(vacacionesDoc.getAceptadoVacaciones());
			//System.out.println(vacacionesDoc.getRechazadoVacaciones());
			
			boolean esJefeRRHH = false;
			List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
			for (Persona p : listJefesRRHH) {
				if (this.usuario.getId().equals(p.getId())) {
					esJefeRRHH = true;
					break;
				}
			}
			
			/*Si: No es jefe de RRHH y no ha sido ni aceptado ni rechazado por el(ambos null)
			 *    o si es jefe de RRHH y no ha sido ni aceptado ni rechazado por el(ambos null)*/
			if ( (!esJefeRRHH && vacacionesDoc.getRechazadoVacaciones() == null && vacacionesDoc.getAceptadoVacaciones() == null)
					|| (esJefeRRHH && vacacionesDoc.getRechazadoVacaciones() == null && vacacionesDoc.getAceptadoVacacionesRRHH() == null)) {
				for (Documento doc : listDocumentosRespuesta) {
					if (doc instanceof Resolucion) {
						//System.out.println("Ya existe resolución");
						return false;
					}
				}
				/**
				 * Valida que el usuario sea el jefe del solicitante.
				 */
				obtenerJefatura(vacacionesDoc.getFuncionario());
				if (listDestinatarios != null) {
					for (Persona p : listDestinatarios) {
						if (p.getId().equals(usuario.getId())) {
							System.out.println("Es jefe directo");
							return true;
						}
					}
					//listDestinatarios.clear();
				}
				List<Persona> listJefeRRHH = jerarquias.buscarJefeRRHH();
				if (listJefeRRHH != null) {
					for (Persona p : listJefeRRHH) {
						if (p.getId().equals(usuario.getId())) {
							//System.out.println("Es jefe de RRHH");
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean isRenderedMensajeRechazado() {
		if (vacaciones != null && 
				vacaciones.getRechazadoVacaciones() != null && 
				vacaciones.getRechazadoVacaciones()) {
			if (vacaciones.getAutor().getId().equals(usuario.getId())) {
				return true;
			}
		}
		return false;
	}

	public boolean existePersona() {
		return (vacaciones.getFuncionario() != null);
	}

	//@End
	public String end() {
		log.info("ending conversation");
//		funcionario = null;
//		vacaciones = null;
//		detalleDiasVacaciones = null;
//		flagInformacion = false;
//		informacionPersonal.init();
		return homeCrear;
	}

	//@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
//	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
//	private Resolucion resolucion;
	public String endAndSendToRRHH() {
		log.info("ending conversation");
//		funcionario = null;
//		vacaciones = null;
//		
//		detalleDiasVacaciones = null;
//		flagInformacion = false;
//		informacionPersonal.init();
		
//		resolucion = new Resolucion();
//		
//		Calendar ahora = new GregorianCalendar();
//		// resolucion.setFechaDocumentoOrigen(ahora.getTime());
//		resolucion.setFechaCreacion(ahora.getTime());
//		ahora.add(Calendar.DATE, 5);
//		resolucion.setPlazo(ahora.getTime());
//
//		resolucion.setAutor(usuario);
//		// tipo temporal, Siempre se toma como original el primero.
//		resolucion.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
//		// id temporal para borrar un documento
//		resolucion.setIdNuevoDocumento(Integer.MAX_VALUE);
//		resolucion.setReservado(false);
//		resolucion.setEnEdicion(true);
		vacaciones = null;
		homeCrear = "crearSolVacaciones";
		//return "crearResolucion";
		return homeCrear;
	}

	public String aprobarSolVacaciones() {
		//String hql = "select sv from SolicitudVacaciones sv where sv.id= ? "; /*and sv.aceptado is null and sv.rechazado is null";*/
		//Query query = em.createQuery(hql);
		
		Vacaciones vacacionesDoc = new Vacaciones();
		vacacionesDoc = vacaciones;
		//query.setParameter(1, vacacionesDoc.getSolicitudVacaciones().getId());
		//SolicitudVacaciones solic = (SolicitudVacaciones) query.getSingleResult();
		SolicitudVacaciones solic = vacacionesDoc.getSolicitudVacaciones();
		solic.setAceptado(Boolean.TRUE);
		solic.setRechazado(Boolean.FALSE);
		em.merge(solic);
		
		boolean esJefeRRHH = false;
		listDestinatarios = new HashSet<Persona>();
		List<Persona> listJefesRRHH = jerarquias.buscarJefeRRHH();
		if (listJefesRRHH != null) {
			for (Persona p : listJefesRRHH) {
				if (this.usuario.getId().equals(p.getId())) {
					esJefeRRHH = true;
					break;
				}
			}
			listDestinatarios.addAll(listJefesRRHH);
		}
		if (esJefeRRHH) {
			vacacionesDoc.setAceptadoVacacionesRRHH(true);
			//idPlantillaSelec = Plantilla.PLANTILLA_FERIADO;
			
			vacacionesDoc.setAceptadoVacaciones(true);
			DatosSolicitud datosSol = new DatosSolicitud();
			datosSol.setDestinatario(vacacionesDoc.getFuncionario());
			datosSol.setFechaIngreso(new Date());
			datosSol.setEstadoSolicitud(SolicitudVacaciones.APROBADO + ", por RRHH ");
			datosSol.setTipoSolicitud("Vacaciones. Debe esperar la generación y aprobación de la Resolución");
			datosSol.setDiasSolicitados((vacacionesDoc.getDiasSolicitadosVacaciones() != null) ? vacacionesDoc.getDiasSolicitadosVacaciones() : 0);
			datosSol.setFechaInicioDias(vacacionesDoc.getFechaInicioVacaciones());
			datosSol.setFechaTerminoDias(vacacionesDoc.getFechaTerminoVacaciones());

			//me.despacharNotificacionEstadoSolVaca(datosSol);
			if (datosSol.getDestinatario().getEmail() != null) {
				// TODO SEND MAIL
				//me.despacharNotificacionEstadoSolVaca(datosSol);
//				Bitacora bc = new Bitacora(datosSol.getDestinatario().getId(), 
//						datosSol.getDestinatario().getNombreApellido(), 
//						datosSol.getDestinatario().getEmail(), null, 
//						getDocumento().getNumeroDocumento(), new Date(), 
//						OpcionesCorreo.APROBADO.key());
//				em.persist(bc);
			}
			
			//informacionPersonal.setSolicitudVacaciones(solic);
			return endAndSendToRRHH();
		}
		
		if (!esJefeRRHH) {
			vacacionesDoc.setAceptadoVacaciones(true);
			DatosSolicitud datosSol = new DatosSolicitud();
			datosSol.setDestinatario(vacacionesDoc.getFuncionario());
			datosSol.setFechaIngreso(new Date());
			datosSol.setEstadoSolicitud(SolicitudVacaciones.APROBADO + ", por su jefe directo ");
			datosSol.setTipoSolicitud("Vacaciones. Debe esperar la generación y aprobación de la Resolución");
			datosSol.setDiasSolicitados((vacacionesDoc.getDiasSolicitadosVacaciones() != null) ? vacacionesDoc.getDiasSolicitadosVacaciones() : 0);
			datosSol.setFechaInicioDias(vacacionesDoc.getFechaInicioVacaciones());
			datosSol.setFechaTerminoDias(vacacionesDoc.getFechaTerminoVacaciones());

			//me.despacharNotificacionEstadoSolVaca(datosSol);
			if (datosSol.getDestinatario().getEmail() != null) {
				// TODO SEND MAIL
				//me.despacharNotificacionEstadoSolVaca(datosSol);
//				Bitacora bc = new Bitacora(datosSol.getDestinatario().getId(), 
//						datosSol.getDestinatario().getNombreApellido(), 
//						datosSol.getDestinatario().getEmail(), null, 
//						getDocumento().getNumeroDocumento(), new Date(), 
//						OpcionesCorreo.APROBADO.key());
//				em.persist(bc);
			}
		}
		return despacharExpediente();
	}
	
	/*private Integer getIdPlantillaFeriado() {
		Integer idPlantilla = null;
		StringBuilder idPlantillaQuery = new StringBuilder("SELECT pr.id FROM plantilla_resolucion pr ");
		idPlantillaQuery.append("WHERE pr.nombre LIKE '%feriado%' OR pr.nombre LIKE '%FERIADO%'");
		idPlantilla = (Integer) em.createNativeQuery(idPlantillaQuery.toString()).getSingleResult();
		return idPlantilla;
	}*/



	public String rechazarSolVacaciones() {
		//String hql = "select sv from SolicitudVacaciones sv where sv.id= ? "; /*and sv.aceptado is null and sv.rechazado is null";*/
		//Query query = em.createQuery(hql);
		Vacaciones vacacionesDoc = new Vacaciones();
		vacacionesDoc = vacaciones;
		//query.setParameter(1, vacacionesDoc.getIdSolicitudVacaciones());
		//SolicitudVacaciones solic = (SolicitudVacaciones) query.getSingleResult();
		SolicitudVacaciones solic = vacacionesDoc.getSolicitudVacaciones();
		solic.setAceptado(false);
		solic.setRechazado(true);
		em.merge(solic);

		vacacionesDoc.setRechazadoVacaciones(true);

		vacaciones.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.RECHAZADO));
		vacaciones.addBitacora(new Bitacora(EstadoDocumento.RECHAZADO, vacaciones, this.usuario));

		persistirDocumento();

		DatosSolicitud datosSol = new DatosSolicitud();
		datosSol.setDestinatario(vacacionesDoc.getFuncionario());
		datosSol.setFechaIngreso(new Date());
		datosSol.setDiasSolicitados((vacacionesDoc.getDiasSolicitadosVacaciones() != null) ? vacacionesDoc.getDiasSolicitadosVacaciones() : 0);
		datosSol.setEstadoSolicitud(SolicitudVacaciones.RECHAZADO);
		datosSol.setTipoSolicitud("Vacaciones");

		//me.despacharNotificacionEstadoSolVaca(datosSol);
		if (datosSol.getDestinatario().getEmail() != null) {
			// TODO SEND MAIL
			//me.despacharNotificacionEstadoSolVaca(datosSol);
//			BitacoraCorreo bc = new BitacoraCorreo(datosSol.getDestinatario().getId(), 
//					datosSol.getDestinatario().getNombreApellido(), 
//					datosSol.getDestinatario().getEmail(), null, 
//					getDocumento().getNumeroDocumento(), new Date(), 
//					OpcionesCorreo.RECHAZADO.key());
//			em.persist(bc);
		}

		listDestinatarios = new HashSet<Persona>();
		listDestinatarios.add(vacaciones.getFuncionario());
		return despacharExpediente();
	}

	public String despacharExpediente() {
		if (listDestinatarios != null && listDestinatarios.size() > 0) {
			if (expediente == null || expediente.getId() == null) {
				distribuirDocumentos();
			}
			guardarExpediente();
			desdeDespacho = true;
			return "bandejaSalida";
		} else {
			FacesMessages.instance().add("Debe Existir un Destinatario de RRHH");
			return "";
		}
	}

	public Persona getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Persona funcionario) {
		this.funcionario = funcionario;
	}

	public String getGrado() {
		return informacionPersonal.getGrado();
	}

	public void setGrado(String grado) {
		informacionPersonal.setGrado(grado);
	}

	public Vacaciones getVacaciones() {
		return vacaciones;
	}

	public void setVacaciones(Vacaciones vacaciones) {
		this.vacaciones = vacaciones;
	}

	public Double getDiasVacacionesPendientes() {
		return informacionPersonal.getDiasVacacionesPendientes();
	}

	public void setDiasVacacionesPendientes(Double diasVacacionesPendientes) {
		informacionPersonal.setDiasVacacionesPendientes(diasVacacionesPendientes);
	}

	public String getCalidadJuridica() {
		return informacionPersonal.getCalidadJuridica();
	}

	public void setCalidadJuridica(String calidadJuridica) {
		informacionPersonal.setCalidadJuridica(calidadJuridica);
	}

	public boolean isDiasOk() {
		return diasOk;
	}

	public void setDiasOk(boolean diasOk) {
		this.diasOk = diasOk;
	}

	public Persona getSolicitante() {
		if (informacionPersonal == null) {
			informacionPersonalSrv = new InformacionPersonalSrv();
			informacionPersonalSrv.setDocumento(vacaciones);
			informacionPersonalSrv.setUsuario(usuario);
			informacionPersonalSrv.init();
			informacionPersonal = informacionPersonalSrv.getInformacionPersonal();
		}
		return informacionPersonal.getSolicitante();
	}

	public void setSolicitante(Persona solicitante) {
		informacionPersonal.setSolicitante(solicitante);
	}

	public String getTipoDia() {
		return tipoDia;
	}

	public void setTipoDia(String tipoDia) {
		this.tipoDia = tipoDia;
	}

	public String getDatosObtenidos() {
		if(informacionPersonal==null) return null;
		return informacionPersonal.getDatosObtenidos();
	}

	public void setDatosObtenidos(String datosObtenidos) {
		informacionPersonal.setDatosObtenidos(datosObtenidos);
	}

	public DetalleDiasVacaciones getDetalleDiasVacaciones() {
		if(detalleDiasVacaciones==null){
			cargarDetalleDias();
		}
		return detalleDiasVacaciones;
	}

	public void setDetalleDiasVacaciones(DetalleDiasVacaciones detalleDiasVacaciones) {
		this.detalleDiasVacaciones = detalleDiasVacaciones;
	}

	private void cargarListaSolicitados() {
		this.detalleDiasVacaciones = cargarDetalleDias();
	}

	private DetalleDiasVacaciones cargarDetalleDias() {
		DetalleDiasVacaciones detalleDias = null;
		if (vacaciones.getSolicitudVacaciones() != null) {
			if (vacaciones.getSolicitudVacaciones().getListDetalleDiasVacaciones() != null)
				detalleDias = vacaciones.getSolicitudVacaciones().getListDetalleDiasVacaciones().get(0);
			/* is this right?*/
		}
		return detalleDias;
	}

	public Integer getIdPlantillaSelec() {
		return idPlantillaSelec;
	}

	public void setIdPlantillaSelec(Integer idPlantillaSelec) {
		this.idPlantillaSelec = idPlantillaSelec;
	}

	@Destroy
	@Remove
	public final void destroy() {
	}
	
	/*********************************************/
	
	@Override
	public final Boolean getVisado() {
		//boolean visado;
		if (vacaciones != null && vacaciones.getVisaciones() != null && vacaciones.getVisaciones().size() > 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	@Override
	public final Boolean getFirmado() {
		//boolean firmado;
		if (vacaciones != null && vacaciones.getFirmas() != null && !vacaciones.getFirmas().isEmpty()) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}
	
	private void bitacora(boolean visar, boolean firmar) {
		if (!this.getFirmado()) {
			if (!visar && !firmar) {
				if (vacaciones.getId() != null) {
					vacaciones.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
					vacaciones.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, vacaciones, this.usuario));
				} else {
					vacaciones.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
					vacaciones.addBitacora(new Bitacora(EstadoDocumento.CREADO, vacaciones, this.usuario));
				}
			}
			if (firmar) {
				this.repositorio.firmar(vacaciones, usuario);
				// Estado del documento debe setearse despues de firmarlo.
				vacaciones.setEstado(new EstadoDocumento(EstadoDocumento.FIRMADO));
				vacaciones.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, vacaciones, this.usuario));
			}
			if (visar) {
				this.repositorio.visar(vacaciones, usuario);
				vacaciones.addBitacora(new Bitacora(EstadoDocumento.VISADO, vacaciones, this.usuario));
			}
		}
	}
	
	private void setDestinatarioDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDestinatarios(ddList);
	}
	
	private void persistirDocumento() {
	    try {
    		if (this.vacaciones.getId() != null) {
    			md.actualizarDocumento(this.vacaciones);
    
    		} else if (this.expediente != null && this.expediente.getId() != null) {
    			me.agregarRespuestaAExpediente(expediente, this.vacaciones, true);
    		}
	    } catch (DocumentNotUploadedException e) {
	        e.printStackTrace();
	    }
	}
	
	private void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}
	
	// TODO WTF!!!
	@End
	private void guardarExpediente() {
		if (expediente == null) {
			expediente = new Expediente();
			expediente.setFechaIngreso(new Date());
			expediente.setEmisor(usuario);
			expediente.setObservaciones(null);
			me.crearExpediente(expediente);
			try {
                me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		} else {
			me.eliminarExpedientes(expediente, false);
			expediente.setObservaciones(null);
			me.modificarExpediente(expediente);

			// Cerrar plazo al documento original.
			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado() && !this.documentoOriginal.getId().equals(this.vacaciones.getId())) {
				this.documentoOriginal.setCompletado(true);
				this.documentoOriginal.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
				try {
                    md.actualizarDocumento(this.documentoOriginal);
                } catch (DocumentNotUploadedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
			}

		}

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}

		for (Documento doc : listDocumentosRespuesta) {
		    try {
    			if (doc.getId() == null) {
    				me.agregarRespuestaAExpediente(expediente, doc, true);
    			} else {
    				// Cerrar plazo a los documentos respuesta.
    				if (doc.getCompletado() != null && !doc.getCompletado() && !doc.getId().equals(this.vacaciones.getId())) {
    					doc.setCompletado(true);
    					doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
    					md.actualizarDocumento(doc);
    				}
    			}
		    } catch (DocumentNotUploadedException e) {
		        e.printStackTrace();
		    }
		}

		/*for (Documento doc : listDocumentosAnexo) {
			if (doc.getId() == null) {
				if (doc instanceof DocumentoBinario) {
					DocumentoBinario docBin = (DocumentoBinario) doc;
					Long idDocumentoReferencia = buscaDocumentoReferencia(docBin.getIdDocumentoReferencia());
					if (idDocumentoReferencia != null) {
						me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia);
					} else {
						me.agregarAnexoAExpediente(expediente, doc, true);
					}
				} else {
					me.agregarAnexoAExpediente(expediente, doc, true);
				}
			} else {
				md.actualizarDocumento(doc);
			}
		}*/

		//this.guardarObservaciones();
		idExpedientes = new ArrayList<Long>();
		Date fechaIngreso = new Date();

		for (Persona de : listDestinatarios) {
			try {
                idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
            } catch (DocumentNotUploadedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
		}

		List<Expediente> expedientes = new ArrayList<Expediente>();

		for (Long id : idExpedientes) {
			Expediente e = me.buscarExpediente(id);
			me.despacharExpediente(e);
			expedientes.add(e);
		}
		me.despacharExpediente(expediente);
		// DEPRECATED
		//me.despacharNotificacionPorEmail(expedientes);

		FacesMessages.instance().add("Expediente Despachado");
	}

	@Override
	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente != null 
				&& this.expediente.getId() != null
				&& this.expediente.getFechaDespacho() == null 
				&& this.vacaciones.getAutor().equals(usuario)) {
			return true;
		}
		return false;
	}

	@Override
	public void guardarYCrearExpediente() {
		if (validarSolicitud(vacaciones.getSolicitudVacaciones())) {
			armaDocumento(false, false);
			documentoOriginal = listDocumentosRespuesta.get(0);
			listDocumentosRespuesta.remove(0);
		}
	}

	@Override
	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.expediente == null || this.expediente.getId() == null) {
			return true;
		}
		return false;
	}
	
	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.vacaciones.getIdNuevoDocumento())) {
				return true;
			}
		}
		return false;
	}
	
	@Transactional
	private void limpiarDestinatarios(final Long id) {
		try {
			Query query = em.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.vacaciones.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.vacaciones);
			}
		}
		listaDocumentos = lista;
	}
	
	private void agregarDestinatarioDocumento(Persona destinatario) {
		if (destinatariosDocumento == null) {
			destinatariosDocumento = new ArrayList<SelectPersonas>();
		}
		String value = destinatario.getNombres() + " "
				+ destinatario.getApellidoPaterno();
		final Cargo car = destinatario.getCargo();
		final UnidadOrganizacional uo = car.getUnidadOrganizacional();
		final Departamento depto = uo.getDepartamento();
		final Division d = depto.getDivision();
		if ("Jefe Division".equals(car.getDescripcion())
				|| "Jefe Departamento".equals(car.getDescripcion())
				|| "Jefe Unidad".equals(car.getDescripcion())) {
			value = car.getDescripcion();
		} else {
			value += " - " + car.getDescripcion();
		}
		if (!destinatario.getCargo().getUnidadOrganizacional().getVirtual()) {
			value += " - " + uo.getDescripcion();
		} else if (!destinatario.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual()) {
			value += " - " + depto.getDescripcion();
		} else {
			value += " - " + d.getDescripcion();
		}
		this.destinatariosDocumento.add(new SelectPersonas(value, destinatario));
	}
	
	@Override
	public void obtenerDatosExternos() {
//		if (!flagInformacion) {
//			flagInformacion = true;
//			informacionPersonalSrv = new InformacionPersonalSrv();
//			informacionPersonalSrv.cargarInformacionPersonal();
//		}
		
//		informacionPersonalSrv = new InformacionPersonalSrv();
//		informacionPersonalSrv.cargarInformacionPersonal();
		if (informacionPersonal == null || 
				(informacionPersonal != null && informacionPersonal.getDatosObtenidos() == null)) {
			informacionPersonal = new InformacionPersonal();
			//informacionPersonal.init();
			informacionPersonalSrv = new InformacionPersonalSrv();
			informacionPersonalSrv.setDocumento(vacaciones);
			informacionPersonalSrv.setUsuario(usuario);
			informacionPersonalSrv.setInformacionPersonal(informacionPersonal);
			informacionPersonalSrv.init();
			informacionPersonal = informacionPersonalSrv.getInformacionPersonal();
		}
	}
	
	@Transactional
	private void eliminaDetalleDiasVacaciones(Long id) {
		DetalleDiasVacaciones ddv = em.find(DetalleDiasVacaciones.class, id);
		em.remove(ddv);
	}
	
	@Override
	public String guardarDocumento() {
		if (validarSolicitud(vacaciones.getSolicitudVacaciones())) {
			armaDocumento(false, false);
			diasOk = true;
			return end();
		}
		return "";
	}
}
