package cl.exe.exedoc.session;

import java.util.Date;

/**
 * @author Pablo
 *
 */
public interface ProcesoAutomatico {

	/**
	 * @param fecha {@link Date}
	 */
	void iniciarProceso(Date fecha);

}
