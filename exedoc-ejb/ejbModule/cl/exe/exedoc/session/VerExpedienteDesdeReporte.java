package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.ObservacionArchivo;

/**
 * Interface {@link VerExpedienteDesdeReporte}.
 * @author Ricardo Fuentes
 *
 */
@Local
public interface VerExpedienteDesdeReporte {

	String begin();

	String end();

	void destroy();

	boolean renderedVerDocumento();

	boolean renderedVerDocumento(Long idDocumento);

	void verArchivo();

	boolean renderedVerArchivo();

	String obtenerMateria();

	boolean renderedDestinatarioUnico();

	Integer calcularSizeListaDestinatarios();

	boolean renderedListaDestinatarios();

	String modificarDocumento(Integer numDoc, Integer lista);

	// Getters & Setters
	Expediente getExpediente();

	void setExpediente(Expediente expediente);

	Documento getDocumentoOriginal();

	void setDocumentoOriginal(Documento documentoOriginal);

	List<Documento> getListDocumentosRespuesta();

	void setListDocumentosRespuesta(List<Documento> listDocumentosRespuesta);

	// List<Documento> getListDocumentosAnexo();

	// void setListDocumentosAnexo(List<Documento> listDocumentosAnexo);

	List<Observacion> getObservaciones();

	void setObservaciones(List<Observacion> observaciones);

	List<SelectItem> getDestinatariosDocumentoOriginal();

	void setDestinatariosDocumentoOriginal(List<SelectItem> destinatariosDocumentoOriginal);

	boolean getBandejaEntradaCopia();

	String archivarExpediente();

	String volver();

	void verArchivoObservacion(ObservacionArchivo a);

	String begin(String homeCrear);

	boolean getVistoDesdeReporte();
}
