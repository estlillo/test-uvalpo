package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Ricardo Fuentes
 *
 */
@Local
public interface CrearDocumentoPapel {

	/**
	 * Inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * Termina la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Destruye la converzacion.
	 */
	void destroy();

	void ini();

	/**
	 * @return {@link Expediente}
	 */
	Expediente getExpediente();

	/**
	 * @param expediente {@link Expediente}
	 */
	void setExpediente(final Expediente expediente);

	/**
	 * @return {@link List} of {@link Documento}
	 */
	List<Documento> getListDocumentosRespuesta();

	/**
	 * @param listDocumentosRespuesta {@link List} {@link Documento}
	 */
	void setListDocumentosRespuesta(List<Documento> listDocumentosRespuesta);

//	/**
//	 * @return {@link String}
//	 */
//	String getMateria();
//
//	/**
//	 * @param materia {@link String}
//	 */
//	void setMateria(final String materia);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListTipoDocumentosPapel();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListTipoDocumentos();

	/**
	 * @return {@link Long}
	 */
	Long getTipoDocumentoID();

	/**
	 * @param tipoDocumentoID {@link Integer}
	 */
	void setTipoDocumentoID(final Long tipoDocumentoID);

	/**
	 * @return {@link String}
	 */
	String agregarDocumento();

	/**
	 * @return {@link List} of {@link SelectPersonas}
	 */
	List<SelectPersonas> getDestinatariosDocumento();

	/**
	 * @return {@link DocumentoPapel}
	 */
	DocumentoPapel getDocumento();

	/**
	 * @param documento {@link DocumentoPapel}
	 */
	void setDocumento(final DocumentoPapel documento);

	/**
	 * @return {@link Documento}
	 */
	Documento getDocumentoOriginal();

	/**
	 * @param documentoOriginal {@link Documento}
	 */
	void setDocumentoOriginal(final Documento documentoOriginal);

	/**
	 * @return {@link Long}
	 */
	Long getCodigoBarra();

	/**
	 * @param codigoBarra {@link Long}
	 */
	void setCodigoBarra(Long codigoBarra);

	List<Persona> autocomplete(Object suggest);
	
	void seleccionarDest(Persona p);
	
	void setDestinatario(String destinatario);
	
	String getDestinatario();

	void removeDestinatarios(Long id);

	List<SelectItem> getNivelesUrgencia();

	void setNivelesUrgencia(List<SelectItem> nivelesUrgencia);

	Long getIdAlerta();

	void setIdAlerta(Long idAlerta);
	
	boolean isRenderedBotonGuardar();

}
