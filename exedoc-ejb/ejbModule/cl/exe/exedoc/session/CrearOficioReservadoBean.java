package cl.exe.exedoc.session;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Bitacora;
import cl.exe.exedoc.entity.Contenido;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DistribucionDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Parrafo;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.TipoFirma;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.pojo.documento.ManejarDocumentoInterface;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;
import cl.exe.exedoc.util.DestinatariosFirmaDocumentoMail;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("crearOficioReservado")
public class CrearOficioReservadoBean implements CrearOficioReservado {
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@In(required = true)
	private Persona usuario;

	@In(required = true)
	private String homeCrear;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	// TODO revisar documentos anexos
	/*
	 * @In(required = false, scope = ScopeType.CONVERSATION)
	 * @Out(required = false, scope = ScopeType.CONVERSATION) private List<Documento> listDocumentosAnexo;
	 */

	@In(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "documento", scope = ScopeType.CONVERSATION)
	private Oficio oficioReservado;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;

	private Boolean guardadoTemporal = true;

	private TipoDocumentoExpediente tipoDocExp;

	private String parrafo;

	private Boolean firmado;
	private Boolean visado;
	private Boolean completado;

	private Boolean validarPanelApplet;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;
	
	private static final String STRING = "/";
	
	private List<ArchivoAdjuntoDocumentoElectronico> listaArchivosEliminados;

	public void visualizar() {
		if (!firmado && !visado) {
			armaDocumento(false, false, false);
		}
	}

	public void guardar() {
		armaDocumento(false, false, false);
		FacesMessages.instance().add("Documento Guardado");
	}

	public void firmar() {
		if (verifica()) {
			if (usuario.getId().equals(oficioReservado.getIdEmisor())) {
				oficioReservado.setFechaDocumentoOrigen(new Date());
				this.armaDocumento(false, true, false);
				//agregarListaDestinatariosExpediente();
				List<Persona> destinatariosEmail = new ArrayList<Persona>();
				for (SelectPersonas sp : distribucionDocumento) {
					destinatariosEmail.add(sp.getPersona());
				}
				me.enviarMailNotificacionFirma(expediente, oficioReservado, destinatariosEmail);
				FacesMessages.instance().add("Documento Firmado");
//				if (expediente != null && expediente.getId() != null) {
//					DestinatariosFirmaDocumentoMail destMail = me.obtenerDatosNotificacion(oficioReservado,
//							expediente.getNumeroExpediente());
//					me.firmarNotificacionPorEmail(destMail);
//				}
			} else {
				FacesMessages.instance().add("Ud. No puede Firmar este Documento, solo lo puede hacer el Emisor (DE)");
			}
		}
	}

	public void agregarListaDestinatariosExpediente() {
		if (listDestinatarios == null) {
			listDestinatarios = new HashSet<Persona>();
		}
		for (SelectPersonas sp : destinatariosDocumento) {
			sp.getPersona().setDestinatarioConCopia(false);
			sp.getPersona().setBorrable(false);
			listDestinatarios.add(sp.getPersona());
		}
		for (SelectPersonas sp : distribucionDocumento) {
			sp.getPersona().setDestinatarioConCopia(true);
			listDestinatarios.add(sp.getPersona());
		}
	}

	public void visar() {
		if (verifica()) {
			if (!yaVisado()) {
				if (autorNoVisa()) {
					this.armaDocumento(true, false, false);
					FacesMessages.instance().add("Documento Visado");
				} else {
					FacesMessages.instance().add("El autor no puede visar el documento");
				}
			} else {
				FacesMessages.instance().add("Usted ya viso el Documento");
			}
		}
	}

	/**
	 * Permite validar que el Usuario que intenta visar el Documento, no sea el Autor. Retorna true en caso de ser
	 * inválido y false en caso de ser válido el usuario.
	 * 
	 * @return
	 */
	public boolean autorNoVisa() {
		if (!oficioReservado.getAutor().getId().equals(usuario.getId())) { return true; }
		return false;
	}

	public boolean yaVisado() {
		if (oficioReservado != null && oficioReservado.getVisaciones() != null) {
			for (VisacionDocumento visa : oficioReservado.getVisaciones()) {
				if (visa.getPersona().getId().equals(usuario.getId())) { return true; }
			}
		}
		return false;
	}

	public String end() {
		log.info("ending conversation");
//		if (firmado && this.expediente == null) {
//			guardarYCrearExpediente();
//			return "";
//		}
		parrafo = "";
		destinatariosDocumento = null;
		distribucionDocumento = null;
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				vistoDesdeReporte = null;
				return "verExpedienteDesdeReporte";
			}
		}
		if (oficioReservado.getId() != null && expediente != null && expediente.getId() != null) {
			oficioReservado.setEliminable(md.isEliminable(oficioReservado, expediente, usuario));
		}
		return homeCrear;
	}

	public String completar() {
		this.oficioReservado.setCompletado(true);
		this.oficioReservado.getBitacoras().add(
				new Bitacora(EstadoDocumento.COMPLETADO, this.oficioReservado, this.usuario));
		return end();
	}
	
	private boolean documentoValido;
	
	@Override
	public boolean isDocumentoValido() {
		return documentoValido;
	}

	public String agregarDocumento() {
		documentoValido = verifica();
		if (documentoValido) {
			armaDocumento(false, false, false);
			return end();
		}
		return "";
	}

	public void guardarYCrearExpediente() {
		documentoValido = verifica();
		if (documentoValido) {
			armaDocumento(false, false, false);
			documentoOriginal = listDocumentosRespuesta.get(0);
			listDocumentosRespuesta.remove(0);
		}
	}

	private boolean verifica() {
		boolean guardar = true;
		if (oficioReservado.getMateria() == null || (oficioReservado.getMateria() != null && oficioReservado.getMateria().trim().isEmpty())) {
			FacesMessages.instance().add("La Materia es obligatoria");
			guardar = false;
		}
		if (oficioReservado.getEmisor() == null) {
			FacesMessages.instance().add("Debe Seleccionar un Emisor (DE) al Documento");
			guardar = false;
		}
		if (destinatariosDocumento == null || destinatariosDocumento.size() == 0) {
			FacesMessages.instance().add("Debe Seleccionar por lo menos un Destinatario (A) al Documento");
			guardar = false;
		}
		return guardar;
	}

	private void persistirDocumento() {
		try {
			if (this.oficioReservado.getId() != null) {
				md.actualizarDocumento(this.oficioReservado);

			} else if (this.expediente != null && this.expediente.getId() != null) {
				me.agregarRespuestaAExpediente(expediente, this.oficioReservado, true);
			}
		} catch (DocumentNotUploadedException e) {
			e.printStackTrace();
		}
		/*
		 * if (this.expediente != null) { for (Documento doc : listDocumentosAnexo) { if (doc.getId() == null) { if (doc
		 * instanceof DocumentoBinario) { DocumentoBinario docBin = (DocumentoBinario) doc; if
		 * (docBin.getIdDocumentoReferencia().equals(this.oficioReservado.getIdNuevoDocumento())) {
		 * me.agregarAnexoAExpediente(expediente, doc, true, this.oficioReservado.getId()); } doc.setEnEdicion(true); }
		 * } } }
		 */
	}

	private void armaDocumento(boolean visar, boolean firmar, boolean soloArmar) {
		this.oficioReservado.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		// TipoDocumento: 2,'Oficio Circular'
		this.oficioReservado.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.OFICIO_RESERVADO));
		if (!soloArmar) {
			if (!this.getFirmado()) {
				if (!visar && !firmar) {
					if (this.oficioReservado.getId() != null) {
						this.oficioReservado.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.GUARDADO));
						this.oficioReservado.addBitacora(new Bitacora(EstadoDocumento.GUARDADO, this.oficioReservado,
								this.usuario));
					} else {
						this.oficioReservado.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.CREADO));
						this.oficioReservado.addBitacora(new Bitacora(EstadoDocumento.CREADO, this.oficioReservado,
								this.usuario));
					}
				}
				if (firmar) {
					this.repositorio.firmar(this.oficioReservado, usuario);
					this.oficioReservado.addBitacora(new Bitacora(EstadoDocumento.FIRMADO, this.oficioReservado,
							this.usuario));
					// Pruebas mias
					List<Expediente> padre = me.obtienePrimerExpediene(expediente);
					List<Persona> dest = me.buscarDestinatarios(padre);
					for (Persona expediente : dest) {
						me.enviarMailPorDestinatarios(expediente, 1, this.expediente);
					}
				}
				if (visar) {
					this.repositorio.visar(this.oficioReservado, usuario);
					this.oficioReservado.addBitacora(new Bitacora(EstadoDocumento.VISADO, this.oficioReservado,
							this.usuario));
				}
			}
		}

		if (this.oficioReservado.getCompletado() == null) {
			this.oficioReservado.setCompletado(false);
		}

		this.oficioReservado.setReservado(true);

		this.setParrafo();

		this.limpiarDestinatarios(this.oficioReservado.getId());
		this.setDistribucionDocumento(this.oficioReservado);
		this.setDestinatarioDocumento(this.oficioReservado);
		
		setTipoDocumentoExpediente();

		if (this.oficioReservado.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
			if (!existeDocumento(listDocumentosRespuesta)) {
				listDocumentosRespuesta.add(this.oficioReservado);
			} else {
				if (this.oficioReservado.getId() != null) {
					listDocumentosRespuesta.remove(this.oficioReservado);
					listDocumentosRespuesta.add(this.oficioReservado);
				} else {
					this.reemplazaDocumento(listDocumentosRespuesta);
				}
			}
		} /*
		 * else if (this.oficioReservado.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
		 * if (!existeDocumento(listDocumentosAnexo)) { listDocumentosAnexo.add(this.oficioReservado); } else { if
		 * (this.oficioReservado.getId() != null) { listDocumentosAnexo.remove(this.oficioReservado);
		 * listDocumentosAnexo.add(this.oficioReservado); } else { this.reemplazaDocumento(listDocumentosAnexo); } } }
		 */else if (this.oficioReservado.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
			documentoOriginal = oficioReservado;
		}

		if (listDocumentosEliminados != null) {
			for (Documento doc : listDocumentosEliminados) {
				md.eliminarDocumento(doc.getId());
			}
		}
		
		if (this.listaArchivosEliminados != null) {
			for (ArchivoAdjuntoDocumentoElectronico doc : this.listaArchivosEliminados) {
				if (doc.getId() != null)
					this.eliminaAntecedente(doc.getId());
			}
		}
		this.listaArchivosEliminados = null;
		
		persistirDocumento();
	}

	private void setTipoDocumentoExpediente() {
		if (this.oficioReservado.getTipoDocumentoExpediente() == null
				|| this.oficioReservado.getTipoDocumentoExpediente().getId() == null) {
			this.oficioReservado.setTipoDocumentoExpediente(this.tipoDocExp);
		}
	}

	private void setDestinatarioDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.destinatariosDocumento) {
			DestinatarioDocumento dd = new DestinatarioDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);
		}
		dc.setDestinatarios(ddList);
	}

	private void setDistribucionDocumento(Documento dc) {
		List<ListaPersonasDocumento> ddList = new LinkedList<ListaPersonasDocumento>();
		for (SelectPersonas item : this.distribucionDocumento) {
			DistribucionDocumento dd = new DistribucionDocumento();
			dd.setDestinatario(item.getDescripcion());
			dd.setDestinatarioPersona(item.getPersona());
			dd.setDocumento(dc);
			ddList.add(dd);

		}
		dc.setDistribucion(ddList);
	}

	private boolean existeDocumento(List<Documento> listaDocumentos) {
		for (Documento d : listaDocumentos) {
			if (d.getIdNuevoDocumento().equals(this.oficioReservado.getIdNuevoDocumento())) { return true; }
		}
		return false;
	}

	private void reemplazaDocumento(List<Documento> listaDocumentos) {
		List<Documento> lista = new ArrayList<Documento>();
		for (Iterator<Documento> doc = listaDocumentos.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (!id.equals(this.oficioReservado.getIdNuevoDocumento())) {
				lista.add(docTmp);
			} else {
				lista.add(this.oficioReservado);
			}
		}
		listaDocumentos = lista;
	}

	private void setParrafo() {
		if (this.oficioReservado.getContenido() == null) {
			List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
			Contenido parrafo = new Contenido();
			parrafo.setNumero(1L);
			parrafo.setDocumento(this.oficioReservado);
			listaParrafos.add(parrafo);
			this.oficioReservado.setContenido(listaParrafos);
		}
		this.oficioReservado.getContenido().get(0).setCuerpo(this.parrafo);
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String getParrafo() {
		parrafo = "";
		if (this.oficioReservado != null && this.oficioReservado.getContenido() != null
				&& this.oficioReservado.getContenido().size() != 0) {
			parrafo = this.oficioReservado.getContenido().get(0).getCuerpo();
		}
		return parrafo;
	}

	public void setParrafo(String parrafo) {
		if (this.oficioReservado.getContenido() == null) {
			List<Parrafo> listaParrafos = new ArrayList<Parrafo>();
			Contenido cont = new Contenido();
			cont.setNumero(1L);
			cont.setDocumento(this.oficioReservado);
			listaParrafos.add(cont);
			this.oficioReservado.setContenido(listaParrafos);
		}
		this.oficioReservado.getContenido().get(0).setCuerpo(parrafo);
		this.parrafo = parrafo;
	}

	public Oficio getOficioReservado() {
		return oficioReservado;
	}

	public Boolean getFirmado() {
		if (this.oficioReservado != null && this.oficioReservado.getFirmas() != null
				&& this.oficioReservado.getFirmas().size() != 0) {
			firmado = true;
		} else {
			firmado = false;
		}
		return firmado;
	}

	public Boolean getVisado() {
		if (this.oficioReservado != null && this.oficioReservado.getVisaciones() != null
				&& this.oficioReservado.getVisaciones().size() != 0) {
			visado = true;
		} else {
			visado = false;
		}
		return visado;
	}

	public List<VisacionDocumento> getVisaciones() {
		List<VisacionDocumento> visaciones = null;
		if (this.oficioReservado != null && this.oficioReservado.getVisaciones() != null) {
			visaciones = oficioReservado.getVisaciones();
			Collections.sort(visaciones, new Comparator<VisacionDocumento>() {
				public int compare(VisacionDocumento o1, VisacionDocumento o2) {
					return o2.getFechaVisacion().compareTo(o1.getFechaVisacion());
				}
			});
		}
		return visaciones;
	}

	public List<SelectPersonas> getDestinatariosDocumento() {
		if (destinatariosDocumento == null || destinatariosDocumento.size() == 0) {
			destinatariosDocumento = new ArrayList<SelectPersonas>();
			if (this.oficioReservado != null && this.oficioReservado.getDestinatarios() != null
					&& this.oficioReservado.getDestinatarios().size() != 0) {
				for (ListaPersonasDocumento lpd : this.oficioReservado.getDestinatarios()) {
					destinatariosDocumento.add(new SelectPersonas(lpd.getDestinatario(), lpd.getDestinatarioPersona()));
				}
			}
		}
		return destinatariosDocumento;
	}

	public List<SelectPersonas> getDistribucionDocumento() {
		if (distribucionDocumento == null || distribucionDocumento.size() == 0) {
			distribucionDocumento = new ArrayList<SelectPersonas>();
			if (this.oficioReservado != null && this.oficioReservado.getDistribucionDocumento() != null
					&& this.oficioReservado.getDistribucionDocumento().size() != 0) {
				for (ListaPersonasDocumento lpd : this.oficioReservado.getDistribucionDocumento()) {
					distribucionDocumento.add(new SelectPersonas(lpd.getDestinatario(), lpd.getDestinatarioPersona()));
				}
			}
		}
		return distribucionDocumento;
	}

	public Boolean getCompletado() {
		if (this.oficioReservado != null && this.oficioReservado.getCompletado() != null
				&& !this.oficioReservado.getCompletado()) {
			completado = true;
		} else {
			completado = false;
		}
		return completado;
	}

	public boolean isEditable() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return false; }
		if (getVisado()) { return false; }
		if (getFirmado()) { return false; }
		return true;
	}

	public boolean isRenderedBotonVolver() {
		boolean valor = !isEditable() && this.expediente != null;
		return valor;
	}

	public boolean isEditableSinVisa() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return false; }
		if (getFirmado()) { return false; }
		return true;
	}

	public boolean isDespachable() {
		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return false; }
		return true;
	}

	public boolean isRenderedBotonVisar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (this.oficioReservado.getId() == null) {
			return false;
		}
		if (!autorNoVisa()) {
			return false;
		}
		if (!usuario.getRoles().contains(Rol.VISADOR)) {
			return false;
		}
		if (yaVisado()) {
			return false;
		}
		return !isRenderedBotonFirmar();
	}

	private boolean tieneFirmaAvanzada() {
		if (usuario != null && usuario.getTipoFirma() != null
				&& usuario.getTipoFirma().getId().equals(TipoFirma.AVANZADA)) { return true; }
		return false;
	}

	public boolean isRenderedBotonFirmar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) {
				return false;
			}
		}
		if (!isEditableSinVisa()) {
			return false;
		}
		if (usuario.getId().equals(oficioReservado.getIdEmisor()) && this.oficioReservado.getId() != null) {
			return true;
		}
		return false;
	}

	public boolean isRenderedBotonFirmarAvanzado() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		return tieneFirmaAvanzada() && (usuario.getId().equals(oficioReservado.getIdEmisor()))
				&& (oficioReservado.getFirmas() == null || oficioReservado.getFirmas().isEmpty())
				&& (oficioReservado.getId() != null);
	}

//	public String getUrlFirma() {
//		String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
//		return urlDocumento;
//	}
	
	@Override
	public String getUrlFirma() {
		  Properties defaultProps = new Properties();
	      try {
	         // defaultProps.load(getArchivoStream("customizacion.properties"));
	          final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream("exedoc/customizacion.properties");
	          defaultProps.load(resourceAsStream);

	      }
	      catch (Exception e) {
	          log.error("No se pudo cargar archivo de configuracion customizacion.properties");
	      }
	      String default_port = "8080";
	      String port = defaultProps.getProperty("default_port", default_port.toString());
	      String IP = defaultProps.getProperty("IP", System.getProperty("jboss.bind.address"));
	      
			//final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
	      final String urlDocumento = "http://" + IP  + ":" + port + STRING;
			return urlDocumento;
	}

	public boolean isRenderedBotonDespachar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (getFirmado()) {
			if (listDestinatarios.size() != 0) { return true; }
		}
		return false;
	}

	public boolean isRenderedBotonGuardar() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente != null && this.expediente.getId() != null) { return true; }
		return false;
	}

	public boolean isRenderedBotonGuardarCrearExpediente() {
		if (vistoDesdeReporte != null) {
			if (vistoDesdeReporte) { return false; }
		}
		if (this.expediente == null || this.expediente.getId() == null) { return true; }
		return false;
	}

	public boolean isRenderedBotonVisualizar() {
		if (this.expediente != null && this.expediente.getId() != null) { return true; }
		return false;
	}

	public String guardaParaFirma() {
		String retorno = "crearOficioReservado";
		if (verifica()) {
			if (usuario.getId().equals(oficioReservado.getIdEmisor())) {
				this.armaDocumento(false, false, false);
				em.persist(oficioReservado);

				agregarListaDestinatariosExpediente();

				/*
				 * retorno= despacharExpediente(); if (expediente != null && expediente.getId() != null &&
				 * (guardadoTemporal!= null && guardadoTemporal)) { DestinatariosFirmaDocumentoMail destMail =
				 * me.obtenerDatosNotificacion(oficioReservado, expediente.getNumeroExpediente());
				 * me.firmarNotificacionPorEmail(destMail); }
				 */
				documentoOriginal = listDocumentosRespuesta.get(0);
				listDocumentosRespuesta.remove(0);
			} else {
				FacesMessages.instance().add("Ud. No puede Firmar este Documento, solo lo puede hacer el Emisor (DE)");
			}
		}
		return retorno;
	}

	public String buscaDocumento() {
		log.info("buscando doc...");
		String retorno = /* (!guardadoTemporal)?"bandejaSalida": */"crearOficioReservado";

		if (oficioReservado.getId() == null && oficioReservado.getFirmas() == null) {
			retorno = guardaParaFirma();
		}

		int idNuevoDoc = this.oficioReservado.getIdNuevoDocumento();
		oficioReservado = em.find(Oficio.class, oficioReservado.getId());
		this.oficioReservado.setIdNuevoDocumento(idNuevoDoc);

		firmado = true;
		oficioReservado.setFechaDocumentoOrigen(new Date());
		oficioReservado.getParrafos().size();
		oficioReservado.getDistribucionDocumento().size();
		if (oficioReservado.getVisaciones() != null) {
			oficioReservado.getVisaciones().size();
		}
		if (oficioReservado.getFirmas() != null) {
			oficioReservado.getFirmas().size();
		}
		oficioReservado.getBitacoras().size();
		oficioReservado.getVisacionesEstructuradas().size();
		oficioReservado.getFirmasEstructuradas().size();

		for (Documento d : listDocumentosRespuesta) {
			if (d.getId().equals(oficioReservado.getId())) {
				d.setNumeroDocumento(oficioReservado.getNumeroDocumento());
				d.setEstado(oficioReservado.getEstado());
				d.setFirmas(oficioReservado.getFirmas());
				break;
			}
		}

		validarPanelApplet = false;
		return retorno;
	}

	private boolean validarCamposObligatorios() {
		if (this.oficioReservado.getMateria() != null) { return true; }
		return false;
	}

	public boolean verificaFirmaAvanzada() {
		if (this.getFirmado()) { return false; }
		if (verifica() && validarCamposObligatorios()) { return true; }
		return false;
	}

	public void validar() {
		if (this.oficioReservado != null && this.oficioReservado.getTipoDocumentoExpediente() != null) {
			this.tipoDocExp = this.oficioReservado.getTipoDocumentoExpediente();
		}
		if (validarPanelApplet = validarFirmaAvanzada()) {
			if (this.oficioReservado.getId() == null) {
				guardadoTemporal = false;
				guardaParaFirma();
			} else {
				armaDocumento(false, false, false);
				agregarListaDestinatariosExpediente();
			}
		}
	}

	public void resetValidar() {
		validarPanelApplet = false;
	}

	public boolean validarFirmaAvanzada() {
		if (verificaFirmaAvanzada()) {
			FacesMessages.instance().clear();
			return true;
		}
		FacesMessages.instance().clear();
		FacesMessages.instance().add("Asegurese de ingresar todos los campos requeridos.");
		return false;
	}

	/***************************************************************************
	 * Archivos Anexos - Antecedentes
	 **************************************************************************/

	private ArchivoAdjuntoDocumentoElectronico archivoAnexo;
	private String materiaArchivo;
	private int contadorDocumentos;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosEliminados;

	public void limpiarAnexos() {
		archivoAnexo = null;
		materiaArchivo = null;
	}

	public int getMaxFilesQuantity() {
		int valor = 20;
		/*
		 * if (archivosAnexos == null || archivosAnexos.isEmpty()) { valor = 1; } else { valor = archivosAnexos.size();
		 * }
		 */
		return valor;
	}

	public void listener(UploadEvent event) throws Exception {
		archivoAnexo = new ArchivoAdjuntoDocumentoElectronico();
		UploadItem item = event.getUploadItem();
		String fileName = item.getFileName();
		StringTokenizer st = new StringTokenizer(fileName, "\\");
		while (st.hasMoreElements()) {
			fileName = st.nextToken();
		}
		String contentType = item.getContentType();
		archivoAnexo.setNombreArchivo(fileName);
		archivoAnexo.setContentType(contentType);
		byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(item.getFile());
		archivoAnexo.setArchivo(data);
	}
	
	boolean validarAntecedente;
	
	@Override
	public boolean isValidarAntecedente() {
		return validarAntecedente;
	}
	
	@Override
	public void removeArchivoAnexo() {
		archivoAnexo = null;
	}

	@Override
	public void agregarAntecedenteArchivo() {
		validarAntecedente = true;
		if (materiaArchivo == null || (materiaArchivo != null && materiaArchivo.trim().isEmpty())) {
			FacesMessages.instance().add("Debe ingresar Materia");
			validarAntecedente = false;
		}
		if (archivoAnexo == null || (archivoAnexo != null && archivoAnexo.getArchivo() == null)) {
			FacesMessages.instance().add("Debe ingresar Archivo");
			validarAntecedente = false;
		}
		if (validarAntecedente) {
			if (this.oficioReservado.getArchivosAdjuntos() == null) {
				this.oficioReservado.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
			}
			archivoAnexo.setDocumentoElectronico(this.oficioReservado);
			archivoAnexo.setFecha(new Date());
			archivoAnexo.setAdjuntadoPor(usuario);
			archivoAnexo.setMateria(materiaArchivo);
			this.oficioReservado.getArchivosAdjuntos().add(archivoAnexo);
		}
	}

	/*
	 * private boolean existeNombreArchivo(String nombreArchivo) { for (Documento doc : listDocumentosAnexo) { if (doc
	 * instanceof DocumentoBinario) { DocumentoBinario docBin = (DocumentoBinario) doc; if (docBin.getArchivo() != null
	 * && docBin.getArchivo().getNombreArchivo().equals(nombreArchivo)) { return true; } } } return false; }
	 */

	public void verArchivo(Long idArchivo) {
		/*
		 * ArchivoDocumentoBinario a = em.find(ArchivoDocumentoBinario.class, idArchivo); buscarArchivoRepositorio(a);
		 */
		try {
			if (!DocUtils.getFile(idArchivo, em)) {
				FacesMessages.instance().add(
						"No existe el archivo, consulte con el administrador. (Codigo Archivo: " + idArchivo + ")");
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void buscarArchivoRepositorio(Archivo a) {
		byte[] data = repositorio.recuperarArchivo(a.getCmsId());
		if (data != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			try {
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
				response.setContentType(a.getContentType());
				response.setHeader("Content-disposition",
						"attachment; filename=" + a.getNombreArchivo().replace(' ', '_'));
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
				facesContext.responseComplete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			FacesMessages.instance().add(
					"No existe el archivo, consulte con el administrador. (Codigo Archivo: " + a.getId() + ")");
		}
	}

	/*
	 * public void eliminarAnexoDocumento(Integer numDoc) { int contador = 0; for (Documento doc : listDocumentosAnexo)
	 * { if (doc.getIdNuevoDocumento() != null && doc.getIdNuevoDocumento().equals(numDoc)) { if (doc.getId() != null) {
	 * listDocumentosEliminados.add(doc); } listDocumentosAnexo.remove(contador); break; } contador++; } }
	 */

	public String getMateriaArchivo() {
		return materiaArchivo;
	}

	public void setMateriaArchivo(String materiaArchivo) {
		this.materiaArchivo = materiaArchivo;
	}

	/*
	 * public List<Documento> getListDocumentosAnexo() { List<Documento> documentos = new ArrayList<Documento>(); for
	 * (Documento doc : listDocumentosAnexo) { if (doc instanceof DocumentoBinario) { DocumentoBinario docBin =
	 * (DocumentoBinario) doc; if (oficioReservado.getIdNuevoDocumento().equals(docBin.getIdDocumentoReferencia())) {
	 * documentos.add(docBin); } } } return documentos; }
	 */

	/***************************************************************************
	 * DESPACHAR EXPEDIENTES
	 **************************************************************************/
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	private List<Long> idExpedientes;

	@EJB
	private ManejarExpedienteInterface me;

	@EJB
	private ManejarDocumentoInterface md;

//	public String despacharExpediente() {
//		if (listDestinatarios.size() != 0) {
//			// armaDocumento(false, false, false);
//			if (expediente == null || expediente.getId() == null) {
//				listDocumentosRespuesta.add(this.oficioReservado);
//				distribuirDocumentos();
//			}
//			this.guardarExpediente();
//			desdeDespacho = true;
//			return "bandejaSalida";
//		} else {
//			FacesMessages.instance().add("Debe Seleccionar un Destinatario del Expediente");
//			return "";
//		}
//	}
//
//	@End
//	private void guardarExpediente() {
//		if (expediente == null) {
//			expediente = new Expediente();
//			expediente.setFechaIngreso(new Date());
//			expediente.setEmisor(usuario);
//			expediente.setObservaciones(null);
//			me.crearExpediente(expediente);
//			try {
//				me.agregarOriginalAExpediente(expediente, documentoOriginal, true);
//			} catch (DocumentNotUploadedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else {
//			me.eliminarExpedientes(expediente, false);
//			expediente.setObservaciones(null);
//			me.modificarExpediente(expediente);
//
//			// Cerrar plazo
//			if (this.documentoOriginal.getCompletado() != null && !this.documentoOriginal.getCompletado()
//					&& !this.documentoOriginal.getId().equals(this.oficioReservado.getId())) {
//				this.documentoOriginal.setCompletado(true);
//				this.documentoOriginal.getBitacoras().add(
//						new Bitacora(EstadoDocumento.COMPLETADO, this.documentoOriginal, this.usuario));
//				try {
//					md.actualizarDocumento(this.documentoOriginal);
//				} catch (DocumentNotUploadedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//
//		if (listDocumentosEliminados != null) {
//			for (Documento doc : listDocumentosEliminados) {
//				md.eliminarDocumento(doc.getId());
//			}
//		}
//
//		for (Documento doc : listDocumentosRespuesta) {
//			try {
//				if (doc.getId() == null) {
//					me.agregarRespuestaAExpediente(expediente, doc, true);
//				} else {
//					// Cerrar plazo
//					if (doc.getCompletado() != null && !doc.getCompletado()
//							&& !doc.getId().equals(this.oficioReservado.getId())) {
//						doc.setCompletado(true);
//						doc.getBitacoras().add(new Bitacora(EstadoDocumento.COMPLETADO, doc, this.usuario));
//						md.actualizarDocumento(doc);
//					}
//
//				}
//			} catch (DocumentNotUploadedException e) {
//				e.printStackTrace();
//			}
//		}
//
//		/*
//		 * for (Documento doc : listDocumentosAnexo) { if (doc.getId() == null) { if (doc instanceof DocumentoBinario) {
//		 * DocumentoBinario docBin = (DocumentoBinario) doc; Long idDocumentoReferencia =
//		 * buscaDocumentoReferencia(docBin.getIdDocumentoReferencia()); if (idDocumentoReferencia != null) {
//		 * me.agregarAnexoAExpediente(expediente, doc, true, idDocumentoReferencia); } else {
//		 * me.agregarAnexoAExpediente(expediente, doc, true); } } else { me.agregarAnexoAExpediente(expediente, doc,
//		 * true); } } else { md.actualizarDocumento(doc); } }
//		 */
//
//		this.guardarObservaciones();
//		idExpedientes = new ArrayList<Long>();
//		Date fechaIngreso = new Date();
//
//		for (Persona de : listDestinatarios) {
//			try {
//				idExpedientes.add(me.agregarDestinatarioAExpediente(expediente, usuario, de, fechaIngreso));
//			} catch (DocumentNotUploadedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		List<Expediente> expedientes = new ArrayList<Expediente>();
//
//		for (Long id : idExpedientes) {
//			Expediente e = me.buscarExpediente(id);
//			me.despacharExpediente(e);
//			expedientes.add(e);
//		}
//		me.despacharExpediente(expediente);
//		me.despacharNotificacionPorEmail(expedientes);
//
//		FacesMessages.instance().add("Expediente Despachado");
//	}

	// private Long buscaDocumentoReferencia(Integer idDocumento) {
	// Long id = null;
	// for (Documento doc : listDocumentosRespuesta) {
	// if (doc.getIdNuevoDocumento().equals(idDocumento)) {
	// id = doc.getId();
	// break;
	// }
	// }
	// if (documentoOriginal.getIdNuevoDocumento().equals(idDocumento)) {
	// id = documentoOriginal.getId();
	// }
	// return id;
	// }

	private void guardarObservaciones() {
		if (observaciones != null) {
			for (Observacion obs : observaciones) {
				if (obs.getId() == null) {
					em.persist(obs);
				}
			}
			expediente.setObservaciones(observaciones);
		}
	}

	public void distribuirDocumentos() {
		documentoOriginal = listDocumentosRespuesta.get(0);
		listDocumentosRespuesta.remove(0);
	}

	public Boolean getValidarPanelApplet() {
		return validarPanelApplet;
	}

	public void setValidarPanelApplet(Boolean validarPanelApplet) {
		this.validarPanelApplet = validarPanelApplet;
	}
	
	@Override
	public List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos() {
		if (this.oficioReservado.getArchivosAdjuntos() != null) {
			int cont = 0;
			for (ArchivoAdjuntoDocumentoElectronico a : this.oficioReservado.getArchivosAdjuntos()) {
				a.setIdNuevoArchivo(cont++);
			}
			return this.oficioReservado.getArchivosAdjuntos();
		}
		return new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
	}
	
	@Override
	public final String getSessionId() {
		final FacesContext fCtx = FacesContext.getCurrentInstance();
		final HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
		final String sessionId = session.getId();
		return sessionId;
	}
	
	@Override
	public String buscaDocumento1() {   
		log.info("buscando doc..." + oficioReservado.getId());

		oficioReservado = em.find(Oficio.class, oficioReservado.getId());
		if (oficioReservado.getFirmas() == null || oficioReservado.getFirmas().isEmpty()) {
			this.armaDocumento(false, false, false);
		}

		//firmado = true;
		oficioReservado.getParrafos().size();
		oficioReservado.getDistribucionDocumento().size();
		oficioReservado.getVisaciones().size();
		oficioReservado.getFirmas().size();
		oficioReservado.getBitacoras().size();
		oficioReservado.getRevisarEstructuradas().size();
		oficioReservado.getVisacionesEstructuradas().size();
		oficioReservado.getFirmasEstructuradas().size();
		oficioReservado.getArchivosAdjuntos().size();
		
		if (oficioReservado.getCmsId() != null) {
			if (oficioReservado.getDistribucionDocumento() != null && 
					oficioReservado.getDistribucionDocumento().size() > 0) {
				List<Persona> lista = new ArrayList<Persona>();
				for (ListaPersonasDocumento p : oficioReservado.getDistribucionDocumento()) {
					if (p.getDestinatarioPersona() != null) {
						lista.add(p.getDestinatarioPersona());
					}
				}
				if (lista.size() > 0) {
					me.enviarMailNotificacionFirma(expediente, oficioReservado, lista);
				}
			}
		}

		if (documentoOriginal.getId().equals(oficioReservado.getId())) {
			documentoOriginal.setNumeroDocumento(oficioReservado.getNumeroDocumento());
			documentoOriginal.setFechaDocumentoOrigen(oficioReservado.getFechaDocumentoOrigen());
			documentoOriginal.setEstado(oficioReservado.getEstado());
			documentoOriginal.setFirmas(oficioReservado.getFirmas());
		} else {
			for (Documento d : listDocumentosRespuesta) {
				if (d.getId().equals(oficioReservado.getId())) {
					d.setNumeroDocumento(oficioReservado.getNumeroDocumento());
					d.setFechaDocumentoOrigen(oficioReservado.getFechaDocumentoOrigen());
					d.setEstado(oficioReservado.getEstado());
					d.setFirmas(oficioReservado.getFirmas());
					d.setEliminable(false);
					break;
				}
			}
		}

		return "";
	}
	
	@Override
	public String buscaDocumento2() {
		this.buscaDocumento1();
		return "crearOficioReservado";
	}
	
	@Transactional
	private void limpiarDestinatarios(final Long id) {
		try {
			Query query = em.createNamedQuery("ListaPersonasDocumento.DeleteAll");
			query.setParameter("idDocumento", id);
			query.executeUpdate();
		} catch (Exception e) {
			log.error("Error al eliminar la lista de documentos");
		}
	}
	
	@Transactional
	private void eliminaAntecedente(Long id){
		ArchivoAdjuntoDocumentoElectronico archivo = em.find(ArchivoAdjuntoDocumentoElectronico.class, id);
		em.remove(archivo);
	}
	
	@Override
	public void eliminarArchivoAntecedente(ArchivoAdjuntoDocumentoElectronico archivo) {
		if (this.listaArchivosEliminados == null) {
			this.listaArchivosEliminados = new ArrayList<ArchivoAdjuntoDocumentoElectronico>();
		}
		if (archivo.getId() != null) {
			this.listaArchivosEliminados.add(archivo);
		}
		this.oficioReservado.getArchivosAdjuntos().remove(archivo);
	}
	
	@Override
	public void previsualizar() {
		oficioReservado.setTipoDocumento(em.find(TipoDocumento.class, TipoDocumento.OFICIO_RESERVADO));
		oficioReservado.setListaPersonas(new ArrayList<ListaPersonasDocumento>());
		setDistribucionDocumento(oficioReservado);
		setDestinatarioDocumento(oficioReservado);
		List<ArchivoAdjuntoDocumentoElectronico> listaArchivos = getListaArchivos();
		oficioReservado.setArchivosAdjuntos(new ArrayList<ArchivoAdjuntoDocumentoElectronico>());
		for (ArchivoAdjuntoDocumentoElectronico a : listaArchivos) {
			oficioReservado.getArchivosAdjuntos().add(a);
		}
	}
}
