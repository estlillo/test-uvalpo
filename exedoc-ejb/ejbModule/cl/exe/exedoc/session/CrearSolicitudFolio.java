package cl.exe.exedoc.session;

import java.util.Locale;

import javax.ejb.Local;

import cl.exe.exedoc.entity.SolicitudFolio;

/**
 * Interface {@link CrearSolicitudFolio}.
 * 
 * @author Ricardo Fuentes
 */
@Local
public interface CrearSolicitudFolio {

	/**
	 * Metodo que inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * Metodo que termina la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Metodo que destruye la converzacion.
	 */
	void destroy();

	/**
	 * limpiar formulario.
	 * 
	 * @return {@link String}
	 */
	String limpiarFormulario();

	/**
	 * Metodo que valida si la propuesta de Metas puede ser editada.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean isEditable();

	/**
	 * Metodo que valida, si un documento se encuentra firmado.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean getVisado();

	/**
	 * Metodo que valida, si un documento se encuentra firmado.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean getFirmado();

	/**
	 * Metodo que guarda una solicitud.
	 * 
	 * @return {@link String}
	 */
	String guardarSolicitud();

	/**
	 * @return {@link SolicitudFolio}
	 */
	SolicitudFolio getSolicitudFolio();

	/**
	 * @param solicitudFolio {@link SolicitudFolio}
	 */
	void setSolicitudFolio(final SolicitudFolio solicitudFolio);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getLoaded();

	/**
	 * @param loaded {@link Boolean}
	 */
	void setLoaded(final Boolean loaded);

	/**
	 * @return {@link Locale}
	 */
	Locale getLocate();
	
	public Boolean getFlagFoliador();

	public void setFlagFoliador(Boolean flagFoliador);
}
