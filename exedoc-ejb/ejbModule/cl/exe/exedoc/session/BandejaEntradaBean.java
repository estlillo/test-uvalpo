package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.ExpedienteUnido;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoObservacion;
import cl.exe.exedoc.log.LogTiempos;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.session.exception.DocumentNotUploadedException;

@Stateful
@Scope(ScopeType.SESSION)
@Name("bandejaEntrada")
public class BandejaEntradaBean implements BandejaEntrada {

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManejarExpedienteInterface me;
	
//	@EJB
//	private AdministradorAlertas administradorAlertas;

	private Map<Long, Boolean> selectedRecibos;

	private Map<Long, Boolean> selectedUnir;
	private List<ExpedienteBandejaEntrada> expedientesUnir;
	private Map<Long, Boolean> selectedPadre;

	@In(required = false, scope = ScopeType.SESSION)
	@Out(scope = ScopeType.SESSION)
	private BandejaEntradaData data;

	private Long idExpedientePadre;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;
	
	public BandejaEntradaData getData() {
		return data;
	}

	public void buscarExpedientes() {
		log.info("Buscando expedientes de: " + usuario.getId());
		List<ExpedienteBandejaEntrada> lista;
		
		lista = me.listarExpedienteBandejaEntradaUsuario(usuario);
		data = new BandejaEntradaData(lista);
		desdeDespacho = Boolean.FALSE;
	}

	@Begin(join = true)
	public String begin() {
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja de Entrada", "inicio", new Date());
		log.info("beginning conversation: bandejaEntrada");
		selectedRecibos = new HashMap<Long, Boolean>();
		selectedUnir = new HashMap<Long, Boolean>();
		expedientesUnir = new ArrayList<ExpedienteBandejaEntrada>();
		
		selectedUnir.clear();
		buscarExpedientes();
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Bandeja de Entrada", "fin", new Date());
		return "bandejaEntrada";
	}
	
	public String acusarRecibo() {
		log.info("acusar recibo ...");
		this.expedientesUnir = new ArrayList<ExpedienteBandejaEntrada>();
		long ini = System.currentTimeMillis();
		Iterator<Long> iter = selectedRecibos.keySet().iterator();
		boolean accionAcusaRecibo = false;

		//acuso de recibo ok
		String msjAcuseRecibo = "Expedientes con acuse de recibo exitoso : ";
		boolean flagAcuse = false;
		// eliminados o reasignados
		String msjEliminado = "Existen Expedientes retirados de su Bandeja";
		boolean flagEliminado = false;
		
		while (iter.hasNext()) {
			Long id = iter.next();
			if (((Boolean) selectedRecibos.get(id))) {
				accionAcusaRecibo = true;
				Expediente expediente = me.buscarExpediente(id);
				if (expediente == null || 
						(expediente != null && expediente.getCancelado() != null) || 
						(expediente != null && expediente.getReasignado() != null)) {
					if(!flagEliminado) {
						if (expediente != null) {
							if (!msjEliminado.substring(msjEliminado.length()-1).equals(" ")) {
								msjEliminado += " : " + expediente.getNumeroExpediente();
							} else {
								if (!msjEliminado.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
									msjEliminado += " - " + expediente.getNumeroExpediente();
								}
							}
						}
					}
					flagEliminado = true;
				} else {
					if(!flagAcuse){
						msjAcuseRecibo += expediente.getNumeroExpediente();
					} else {
						if (!msjAcuseRecibo.matches(Pattern.quote(expediente.getNumeroExpediente()))) {
							msjAcuseRecibo += " - " + expediente.getNumeroExpediente();
						}
					}
					flagAcuse = true;
					me.acusarReciboExpediente(expediente, usuario);
					data.acusarRecibo(expediente.getId());
				}
			}
		}
		
		if(flagAcuse){
			FacesMessages.instance().add(msjAcuseRecibo);
		}
		if(flagEliminado){
			FacesMessages.instance().add(msjEliminado);
		}
		if (!accionAcusaRecibo) {
			FacesMessages.instance().add("Debe seleccionar al menos un Expediente.");
		}
		long fin = System.currentTimeMillis();
		log.info("acuso recibo en #0 milisegundos", (fin - ini));
		LogTiempos.mostrarLog(this.usuario.getUsuario(), "Acusar Recibo", fin - ini);
		desdeDespacho = Boolean.TRUE;
		return "bandejaEntrada";
	}

	public void setListaUnir(Long id) {
		limpiarListaUnir(selectedUnir);
		if (selectedUnir.containsKey(id)) {
			expedientesUnir.remove(getExpedienteDesdeLista(id));
			selectedUnir.remove(id);
		} else {
			expedientesUnir.add(getExpedienteDesdeLista(id));
			selectedUnir.put(id, true);
		}
	}

	/*
	 * Permite limpiar la lista, dado que la utilidad de ordenamiento de columnas de RichFaces y el Actualizar agrega
	 * elementos a dicha lista con valor false, lo que provocaba inconsistencia. Input: Map<Long, Boolean>
	 */
	private void limpiarListaUnir(Map<Long, Boolean> mapLong) {
		Set<Long> b = new HashSet<Long>();
		for (Long id : mapLong.keySet()) {
			if (mapLong.get(id) == false) {
				b.add(id);
			}
		}
		for (Long id : b) {
			mapLong.remove(id);
		}
	}
	
	@Override
	public void limpiarUnir() {
		if (idExpedientePadre != null) {
			if (selectedPadre.get(idExpedientePadre) != null) {
				if (selectedPadre.get(idExpedientePadre)) {
					selectedPadre.put(idExpedientePadre, Boolean.FALSE);
				}
			}
		}
		idExpedientePadre = null;
	}

	public boolean isRenderedBotonUnir() {
		return expedientesUnir.size() >= 2;
	}

	private ExpedienteBandejaEntrada getExpedienteDesdeLista(Long idExpediente) {
		for (ExpedienteBandejaEntrada exp : data.getListExpedientes()) {
			if (exp.getId().equals(idExpediente)) { return exp; }
		}
		return null;
	}

	public String unirExpedientes() throws DocumentNotUploadedException {
		/*
		 * Se limpia lista ya que trae elementos "false" que corresponden a basura.
		 */
		limpiarListaUnir(selectedUnir);
		// obtener expediente padre
		Expediente expPadre = me.buscarExpediente(idExpedientePadre);
		if (expPadre.getAcuseRecibo() == null || expPadre.getAcuseRecibo() == false) {
			me.acusarReciboExpediente(expPadre, usuario);
			//data.acusarRecibo(expPadre.getId());
		}
		selectedUnir.remove(idExpedientePadre);
		
		List<Long> idDocumentosPadre = new ArrayList<Long>();
		Iterator<DocumentoExpediente> iDocExpPadre = expPadre.getDocumentos().iterator();
		while (iDocExpPadre.hasNext()) {
			DocumentoExpediente docExpPadre = iDocExpPadre.next();
			idDocumentosPadre.add(docExpPadre.getDocumento().getId());
		}
		Map<Long, Documento> documentosNuevos = new HashMap<Long, Documento>();
		
		List<Observacion> observacionesPadre = new ArrayList<Observacion>();
		if (expPadre.getObservaciones() != null && expPadre.getObservaciones().size() > 0) {
			Iterator<Observacion> iObsPadre = expPadre.getObservaciones().iterator();
			while (iObsPadre.hasNext()) {
				Observacion obsPadre = iObsPadre.next();
				Observacion o = new Observacion();
				o.setObservacion(obsPadre.getObservacion());
				o.setFecha(obsPadre.getFecha());
				o.setAutor(obsPadre.getAutor());
				o.setTipoObservacion(obsPadre.getTipoObservacion());
				o.setObservacionArchivo(obsPadre.getObservacionArchivo());
				observacionesPadre.add(o);
			}
		}
		List<Observacion> observacionesNuevas = new ArrayList<Observacion>();
		
		// copiar documentos de los otros expedientes a expediente padre
		for (Long idExp : selectedUnir.keySet()) {
			if (!idExp.equals(idExpedientePadre)) {
				Expediente exp = me.buscarExpediente(idExp);
				if (exp.getAcuseRecibo() == null || exp.getAcuseRecibo() == false) {
					me.acusarReciboExpediente(exp, usuario);
				}
				// borrar destinatarios previos
				List<Expediente> expedientes = me.buscarExpedientesHijos(exp);
				if (expedientes != null) {
					Iterator<Expediente> itExpedientes = expedientes.iterator();
					while (itExpedientes.hasNext()) {
						Expediente i = (Expediente)itExpedientes.next();
						me.eliminarExpediente(i.getId());
					}
				}
				Iterator<DocumentoExpediente> iDocExp = exp.getDocumentos().iterator();
				while(iDocExp.hasNext()) {
					DocumentoExpediente docExp = iDocExp.next();
					if (!idDocumentosPadre.contains(docExp.getDocumento().getId()) && 
							!documentosNuevos.keySet().contains(docExp.getDocumento().getId())) { 
						documentosNuevos.put(docExp.getDocumento().getId(), docExp.getDocumento());
					}
				}
				
				if (exp.getObservaciones() != null && exp.getObservaciones().size() > 0) {
					Iterator<Observacion> iObs = exp.getObservaciones().iterator();
					while (iObs.hasNext()) {
						Observacion obs = iObs.next();
						Observacion o = new Observacion();
						o.setObservacion(obs.getObservacion());
						o.setFecha(obs.getFecha());
						o.setAutor(obs.getAutor());
						o.setTipoObservacion(obs.getTipoObservacion());
						o.setObservacionArchivo(obs.getObservacionArchivo());
						
						if (!observacionesPadre.contains(o) &&
								!observacionesNuevas.contains(o)) {
							observacionesNuevas.add(o);
						}
					}
				}
				
				Observacion obsUnion = new Observacion();
				obsUnion.setObservacion("EXPEDIENTE UNIDO CON " + exp.getNumeroExpediente());
				obsUnion.setFecha(new Date());
				obsUnion.setAutor(usuario);
				obsUnion.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_SISTEMA));
				observacionesNuevas.add(obsUnion);

				Observacion obsArchivado = new Observacion();
				obsArchivado.setObservacion("EXPEDIENTE ARCHIVADO AL SER UNIDO CON " + expPadre.getNumeroExpediente());
				obsArchivado.setExpediente(exp);
				obsArchivado.setFecha(new Date());
				obsArchivado.setAutor(usuario);
				obsArchivado.setTipoObservacion(new TipoObservacion(TipoObservacion.OBSERVACION_SISTEMA));
				em.merge(obsArchivado);
				ExpedienteUnido union = new ExpedienteUnido();
				union.setFecha(new Date());
				union.setExpediente(exp);
				union.setExpedientePadre(expPadre);
				em.persist(union);
				me.archivarExpediente(exp);
			}
		}
		
		// TODO me eche el metodo recuperar
		for (Documento d : documentosNuevos.values()) {
			me.agregarRespuestaAExpediente(expPadre, d, false);
		}
		for (Observacion o : observacionesNuevas) {
			o.setExpediente(expPadre);
			em.merge(o);
		}
		
		em.merge(expPadre);

		// TODO cache???
//		for (Long idExp : selectedUnir.keySet()) {
//			data.acusarRecibo(idExp);
//		}

		FacesMessages.instance().add("Expedientes unidos exitosamente");
		desdeDespacho = Boolean.TRUE;
		return "bandejaEntrada";
	}

	private void guardarObservaciones(Expediente expediente, Expediente expedienteOriginal) {
		List<Observacion> observaciones = new ArrayList<Observacion>();
		if (expedienteOriginal.getObservaciones() != null && expedienteOriginal.getObservaciones().size() > 0) {
			if (expediente.getObservaciones() != null && expediente.getObservaciones().size() > 0) {
				//filtrar repetidos
				Iterator<Observacion> iObsOriginal = expedienteOriginal.getObservaciones().iterator();
				while(iObsOriginal.hasNext()) {
					Observacion obsOriginal = iObsOriginal.next();
					Iterator<Observacion> iObs = expediente.getObservaciones().iterator();
					while(iObs.hasNext()) {
						Observacion obs = iObs.next();
						if (!(obsOriginal.getFecha().equals(obs.getFecha()) && 
								obsOriginal.getObservacion().equals(obs.getObservacion()) && 
								obsOriginal.getAutor().getId().equals(obs.getAutor().getId()) && 
								((obsOriginal.getObservacionArchivo() == null && obs.getObservacionArchivo() == null) || 
										(obsOriginal.getObservacionArchivo().getId().equals(obs.getObservacionArchivo().getId()))))) {
						
							Observacion o = new Observacion();
							o.setObservacion(obsOriginal.getObservacion());
							o.setExpediente(expediente);
							o.setFecha(obsOriginal.getFecha());
							o.setAutor(obsOriginal.getAutor());
							o.setObservacionArchivo(obsOriginal.getObservacionArchivo());
							em.persist(o);
							observaciones.add(o);
						}
					}
				}
			} else {
				Iterator <Observacion> iObsOriginal = expedienteOriginal.getObservaciones().iterator();
				while(iObsOriginal.hasNext()) {
					Observacion obsOriginal = iObsOriginal.next();
					Observacion o = new Observacion();
					o.setObservacion(obsOriginal.getObservacion());
					o.setExpediente(expediente);
					o.setFecha(obsOriginal.getFecha());
					o.setAutor(obsOriginal.getAutor());
					o.setObservacionArchivo(obsOriginal.getObservacionArchivo());
					em.persist(o);
					observaciones.add(o);
				}
			}
		}
		Observacion obsUnion = new Observacion();
		obsUnion.setObservacion("EXPEDIENTE UNIDO CON " + expedienteOriginal.getNumeroExpediente());
		obsUnion.setExpediente(expediente);
		obsUnion.setFecha(new Date());
		obsUnion.setAutor(usuario);
		em.persist(obsUnion);
		observaciones.add(obsUnion);

		if (expediente.getObservaciones() == null) {
			expediente.setObservaciones(observaciones);
		} else {
			expediente.getObservaciones().addAll(observaciones);
		}
		em.merge(expediente);
		
		Observacion obsUnionOriginal = new Observacion();
		obsUnionOriginal.setObservacion("EXPEDIENTE ARCHIVADO AL SER UNIDO CON " + expediente.getNumeroExpediente());
		obsUnionOriginal.setExpediente(expedienteOriginal);
		obsUnionOriginal.setFecha(new Date());
		obsUnionOriginal.setAutor(usuario);
		em.persist(obsUnionOriginal);
	}

	public void seleccionarExpedientePadre(Long idExpedientePadre) {
		if (selectedPadre.get(idExpedientePadre).equals(false)) {
			this.idExpedientePadre = null;
		} else {
			this.idExpedientePadre = idExpedientePadre;
			for (Long idExpedienteUnir : selectedPadre.keySet()) {
				if (!idExpedienteUnir.equals(idExpedientePadre)) {
					selectedPadre.put(idExpedienteUnir, false);
				}
			}
		}
	}

	@End
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public Map<Long, Boolean> getSelectedRecibos() {
		return selectedRecibos;
	}

	public void setSelectedRecibos(Map<Long, Boolean> selectedRecibos) {
		this.selectedRecibos = selectedRecibos;
	}

	public Map<Long, Boolean> getSelectedUnir() {
		return selectedUnir;
	}

	public void setSelectedUnir(Map<Long, Boolean> selectedUnir) {
		this.selectedUnir = selectedUnir;
	}

	public List<ExpedienteBandejaEntrada> getExpedientesUnir() {
		return expedientesUnir;
	}

	public void setExpedientesUnir(List<ExpedienteBandejaEntrada> expedientesUnir) {
		this.expedientesUnir = expedientesUnir;
	}

	public Long getIdExpedientePadre() {
		return idExpedientePadre;
	}

	public void setIdExpedientePadre(Long idExpedientePadre) {
		this.idExpedientePadre = idExpedientePadre;
	}

	public Map<Long, Boolean> getSelectedPadre() {
		if (selectedPadre == null) {
			selectedPadre = inicializarSelectedPadre();
		}
		return selectedPadre;
	}

	public void setSelectedPadre(Map<Long, Boolean> selectedPadre) {
		this.selectedPadre = selectedPadre;
	}

	public String obtenerEstilo(ExpedienteBandejaEntrada exp) {
		String estilo = "";

		if (exp.getTipoDocumento() != null && (exp.getTipoDocumento().equals("Consulta Ciudadana") || exp.getTipoDocumento().equals("Consulta"))) {
			estilo = "consulta";
		}

		if (exp.getTipoDocumento().equals("Respuesta Consulta Ciudadana")
				|| exp.getTipoDocumento().equals("Respuesta Consulta")) {
			estilo = "respuesta";
		}

		if (data.expRepetidos(exp)) {
			estilo = "repetidos";
		}

		return estilo;
	}

	private Map<Long, Boolean> inicializarSelectedPadre() {
		Map<Long, Boolean> map = new HashMap<Long, Boolean>();

		for (ExpedienteBandejaEntrada ebe : expedientesUnir) {
			map.put(ebe.getId(), false);
		}

		return map;
	}

	@Override
	public Boolean getDesdeDespacho() {
		if (desdeDespacho == null) {
			desdeDespacho = Boolean.FALSE;
		}
		return desdeDespacho;
	}

	@Override
	public void setDesdeDespacho(Boolean desdeDespacho) {
		this.desdeDespacho = desdeDespacho;
	}
	
}
