package cl.exe.exedoc.session;

/**
 *
 */
public interface Customizacion {
	
	/**
	 * 
	 */
	String TIME_ZONE = "timezone";
	
	/**
	 * @param key {@link String}
	 * @return {@link String}
	 */
	String getPropiedad(String key);

	Long getDefaultTimeout();

	boolean getEnviadorCorreo();

	String getNotificacionEnviadorCorreo();

}
