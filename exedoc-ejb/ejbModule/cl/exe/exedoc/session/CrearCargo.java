package cl.exe.exedoc.session;

import java.util.ArrayList;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

@Local
public interface CrearCargo {  
	
	public String begin();
	public String end();
	public void destroy();
	public String registrarCargo();
	
	public Integer getCargoId();
	
	public void setCargoId(Integer cargoId);
	
	public Object[] getUnidadOrganizacional();
	
	public void setUnidadOrganizacional(Object[] unidadOrganizacional);
	
	public ArrayList<SelectItem> getUnidadesOrganizacionalesList();
	
	public void setUnidadesOrganizacionalesList(ArrayList<SelectItem> unidadesOrganizacionalesList);
	
	public Integer getUnidadOrg();
	
	public void setUnidadOrg(Integer unidadOrg);
	
	public ArrayList<SelectItem> getUnidOrgList();
	
	public void setUnidOrgList(ArrayList<SelectItem> unidOrgList);
	
	public String getDescripcion();
	
	public void setDescripcion(String descripcion);
	
	public ArrayList<SelectItem> getCargoList();
	
	public void setCargoList(ArrayList<SelectItem> cargoList);
	
	public Integer getCargoPadre();
	
	public void setCargoPadre(Integer cargoPadre);
	
	public Boolean getCargoDefault();
	
	public void setCargoDefault(Boolean cargodefault);
	
}
