package cl.exe.exedoc.session;

import javax.ejb.Local;



@Local
public interface CrearPersona {  
	
	//seam-gen methods
	public String begin();
	public String end();
	public void destroy();	
   //add additional interface methods here	
	
	public String guardarPersona();
	
	public String getRut();
	public void setRut(String rut);
	public String getNombres();
	public void setNombres(String nombres);
	public String getApellidoPaterno();
	public void setApellidoPaterno(String apellidoPaterno);
	public String getApellidoMaterno();
	public void setApellidoMaterno(String apellidoMaterno);
	public String getEmail();
	public void setEmail(String email);
	public String getUsuario();
	public void setUsuario(String usuario);
	public Long getIdCargo();
	public void setIdCargo(Long idCargo);
	public Boolean getPersonaDefault();
	public void setPersonaDefault(Boolean personaDefault);
}
