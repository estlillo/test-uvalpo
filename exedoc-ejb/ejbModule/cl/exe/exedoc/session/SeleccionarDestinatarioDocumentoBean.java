package cl.exe.exedoc.session;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DependenciaExterna;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.PersonaExterna;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.mantenedores.AdministradorGrupoUsuario;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author
 */
@Stateful
@Name("seleccionarDestinatarioDocumento")
public class SeleccionarDestinatarioDocumentoBean implements
		SeleccionarDestinatarioDocumento {

	private static final String ID = "id";

	private static final String NO_PUEDE_AGREGAR = "No se puede agregar a esta persona como destinatario, ";

	private static final String NO_PUEDE_AGREGAR_JERARQUIA = NO_PUEDE_AGREGAR
			+ "pues no pertenece a su jerarquía";

	private static final String NO_PUEDE_AGREGAR_DISTRIBUCION = NO_PUEDE_AGREGAR
			+ "pues ya se encuentra en la distribucion";

	private static final String SELECT_DESTINATARIOS_FRECUENTES = "SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ";

	private static final String SELECCIONAR_INICIO = "<<Seleccionar>>";
	private static final String MODIFICAR_EXPEDIENTE = "modificarExpediente";
	private final static String ERROR_DUPLICIDAD_DATOS  = "No se permite repetir destinatario";
	private final static String ERROR_EMISOR = "El emisor no puede ser destinatario";
	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;
	
	@EJB
	private JerarquiasLocal jerarquias;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private Set<Persona> listDestinatarios;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private Set<Persona> listDestinatariosOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

//	@In(required = false, scope = ScopeType.CONVERSATION)
//	@Out(required = false, scope = ScopeType.CONVERSATION)
//	private List<SelectPersonas> distribucionDocumento;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();
	private List<SelectItem> listTodosDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listTodosUnidades = new ArrayList<SelectItem>();
	
	// Externos
	private Long dependencia = JerarquiasLocal.INICIO;
	private Long personaExterna = JerarquiasLocal.INICIO;
	private List<SelectItem> listDependencia = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonasExternas = new ArrayList<SelectItem>();

	private List<SelectItem> alias;
	private Long idAlias;

	@In(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	@Out(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	private Documento documento;

	private List<DestinatariosFrecuentes> listListas = new ArrayList<DestinatariosFrecuentes>();

	// Grupo Destinatarios Frecuentes
	private Long grupo = JerarquiasLocal.INICIO;
	private List<SelectItem> listDestFrec = new ArrayList<SelectItem>();

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean agregarDestinatarioDocumento;

	private Boolean rendererCambioTipoEnvio;

	private String username;
	private String nombres;
	private String apellido;
	
	/**
	 * Constructor.
	 */
	public SeleccionarDestinatarioDocumentoBean() {
		super();
	}

	/**
	 * @return {@link List}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		
		listDepartamento.clear();
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarDepartamentos() {
		/*22-06-2012: Sergio->Se reactivan todos los niveles de ExeDoc*/
//		final Query query = em.createNamedQuery("Division.findById");
//		query.setParameter(ID, division);
//		final List<Division> divisiones = query.getResultList();
//		boolean conCargo = false;
//		if (divisiones != null && divisiones.size() == 1) {
//			final Division div = divisiones.get(0);
//			if (div.getConCargo()) {
//				conCargo = true;
//				this.unidadOrganizacional = jerarquias
//						.getIdUnidadVirtualDivision(this.division);
//				this.buscarCargos();
//				this.buscarUnidadesOrganizacionalesDepartamento();
//			}
//		}
//		if (!conCargo) {
//			listCargos.clear();
//			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
//					JerarquiasLocal.TEXTO_INICIAL));
//		}
		
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);

		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.clear();
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * buscar Unidades Organizacionales Departamento.
	 */
	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento dpto = departamentos.get(0);
			if (dpto.getConCargo()) {
				conCargo = true;
				if (documento instanceof Providencia) {
					this.unidadOrganizacional = jerarquias
							.getIdUnidadVirtual(this.departamento);
				} else {
					this.unidadOrganizacional = jerarquias
							.getIdUnidadVirtualDepartamento(this.division,
									this.departamento);
				}
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	@Override
	public void agregarDestinatario() {
		if (!this.persona.equals(JerarquiasLocal.INICIO)) {
			final Persona per = em.find(Persona.class, this.persona);
			this.agregaDestinatario(per);

		} else if (grupo != null && !grupo.equals(JerarquiasLocal.INICIO)) {
			final Persona per = em.find(Persona.class, this.grupo);
			this.agregaDestinatario(per);
		} else {
			FacesMessages.instance().add("Debe Seleccionar una Opción");
		}
	}

	/**
	 * Metodo que agrega {@link Persona} como destinatario.
	 * 
	 * @param destinatario
	 *            {@link Persona}
	 */
	private void agregaDestinatario(final Persona destinatario) {

		String value = destinatario.getNombres() + " "
				+ destinatario.getApellidoPaterno();
		final Cargo car = destinatario.getCargo();
		final UnidadOrganizacional uo = car.getUnidadOrganizacional();
		final Departamento depto = uo.getDepartamento();
		final Division d = depto.getDivision();
		value += " - " + car.getDescripcion();
		if (!destinatario.getCargo().getUnidadOrganizacional().getVirtual()) {
			value += " - " + uo.getDescripcion();
		} else if (!destinatario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getVirtual()) {
			value += " - " + depto.getDescripcion();
		} else {
			value += " - " + d.getDescripcion();
		}
		
		if (!esEmisor(destinatario.getId())) {
			if (!this.estaDestinatario(destinatario.getId())) {
				destinatario.setDestinatarioConCopia(false);
				this.destinatariosDocumento.add(new SelectPersonas(value,
						destinatario));
			}
		}
		limpiarDestinatario();
	}

	@Override
	public void agregar(final Long id, final Boolean copia) {
		
//		//validacion de usuario que no deja agregar si es el mismo usuario logeado
//				if(id.equals(usuario.getId())){
//					FacesMessages.instance()
//					.add("El Usuario "+usuario.getNombreApellido()+" no puede ser Destinatario y Remitente.");
//					return;
//				}
//				
//		log.debug("Buscando a id {0}", id);
//		final Persona destinatario = em.find(Persona.class, id);
//
//		destinatario.setDestinatarioConCopia(copia);
//		destinatario.setBorrable(true);
//
//		if (distribucionDocumento != null && !distribucionDocumento.isEmpty()) {
//			for (SelectPersonas sp : distribucionDocumento) {
//				if (sp.getPersona().getId().equals(destinatario.getId())) {
//					FacesMessages.instance().add(NO_PUEDE_AGREGAR_DISTRIBUCION);
//					return;
//				}
//			}
//		}
//		String value = destinatario.getNombres() + " "
//				+ destinatario.getApellidoPaterno();
//		final Cargo car = destinatario.getCargo();
//		final UnidadOrganizacional uo = car.getUnidadOrganizacional();
//		value += " - " + car.getDescripcion();
//		value += " - " + uo.getDescripcion();
//		/*final Departamento depto = uo.getDepartamento();
//		final Division d = depto.getDivision();
//		value += " - " + car.getDescripcion();
//		if ("Jefe Division".equals(car.getDescripcion())
//				|| "Jefe Departamento".equals(car.getDescripcion())
//				|| "Jefe Unidad".equals(car.getDescripcion())) {
//			value = car.getDescripcion();
//		} else {
//			value += " - " + car.getDescripcion();
//		}
//		if (!destinatario.getCargo().getUnidadOrganizacional().getVirtual()) {
//			value += " - " + uo.getDescripcion();
//		} else if (!destinatario.getCargo().getUnidadOrganizacional()
//				.getDepartamento().getVirtual()) {
//			value += " - " + depto.getDescripcion();
//		} else {
//			value += " - " + d.getDescripcion();
//		}*/
//		if (!this.estaDestinatario(value)) {
//			if (documento != null && documento.getEmisor() != null
//					&& value.equals(documento.getEmisor())) {
//				FacesMessages
//						.instance()
//						.add("El destinatario no puede ser el mismo emisor del documento");
//				return;
//			} else {
//				destinatario.setDestinatarioConCopia(false);
//				this.destinatariosDocumento.add(new SelectPersonas(value,
//						destinatario));
//
//			}
//		}
//		if (this.estaDestinatarioSearch(destinatario.getId())) {
//			FacesMessages.instance().add("El destinatario no puede repetirse");
//			return;
//		} else {
////			if (usuario.getId().equals(destinatario.getId())) {
////				FacesMessages
////						.instance()
////						.add("El destinatario no puede ser el mismo emisor del documento");
////				return;
////			}
//
//			// this.listDestinatariosOriginal.add(destinatario);
//			destinatario.setDestinatarioConCopia(copia);
//			destinatario.setBorrable(true);
//			this.listDestinatarios.add(destinatario);
//
//		}
//
//		listOrganizacion.addAll(this.buscarOrganizaciones());
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
	}
	

	@Override
	public void agregarDestinatarioExterno() {
		if (!personaExterna.equals(JerarquiasLocal.INICIO)) {
			final PersonaExterna pe = em.find(PersonaExterna.class,	personaExterna);
			String value = pe.getNombres() + " - " + pe.getDependencia().getDescripcion();
			if (!estaDestinatario(value)) {
				destinatariosDocumento.add(new SelectPersonas(value));
			}
			limpiarDestinatario();
		}
	}

	/**
	 * Matodo que valida si una persona esta dentro de la lista de
	 * destinatarios.
	 * 
	 * @param nombre
	 *            {@link String}
	 * @return {@link Boolean}
	 */
	private boolean estaDestinatario(final String nombre) {
		if(this.destinatariosDocumento == null) {
			this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		}
		SelectPersonas s = new SelectPersonas(nombre);
		for (SelectPersonas si : this.destinatariosDocumento) {
			if (si.getDescripcion().equals(s.getDescripcion())) {
				FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
				return true;
			}
		}
		return false;
	}
	
	private boolean estaDestinatario(final Long id) {
		if(this.destinatariosDocumento == null) {
			this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		}
		for (SelectPersonas si : this.destinatariosDocumento) {
			if (si.getPersona() != null){
				if (si.getPersona().getId().equals(id)) {
					FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean esEmisor(final Long id) {
		if (documento != null && documento.getIdEmisor() != null) {
			if (documento.getIdEmisor().equals(id)) {
				FacesMessages.instance().add(ERROR_EMISOR);
				return true;
			}
		}
		return false;
	}

	/**
	 * Matodo que valida si una persona esta dentro de la lista de
	 * destinatarios.
	 * 
	 * @param nombre
	 *            {@link String}
	 * @return {@link Boolean}
	 */
//	private boolean estaDestinatarioSearch(final Long nombre) {
//		if(listDestinatarios != null){
//		for (Persona si : this.listDestinatarios) {
//			if (si.getId().equals(nombre)) {
//				return true;
//			}
//			si.getId();
//			}
//		}
//		return false;
//	}

	@Override
	public void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		this.grupo = JerarquiasLocal.INICIO;
		this.dependencia = JerarquiasLocal.INICIO;
		this.personaExterna = JerarquiasLocal.INICIO;
		listPersonasExternas.clear();
		listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void eliminarDestinatarioDocumento(SelectPersonas persona) {
		int index=0;
		for (SelectPersonas sp : destinatariosDocumento) {
			if (sp.getPersona() != null && persona.getPersona() != null) {
				if (sp.getPersona().getId().equals(persona.getPersona().getId())) {			
					destinatariosDocumento.remove(index);
					break;
				}
			} else {
				if (sp.getPersona() == null && persona.getPersona() == null) {
					if (sp.getDescripcion().equals(persona.getDescripcion())) {
						destinatariosDocumento.remove(index);
						break;
					}
				}
			}
			index++;
		}
		if (destinatariosDocumento.size() == 0) {
			
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarPersonasExternas() {
		listPersonasExternas.clear();
		listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		if (!dependencia.equals(JerarquiasLocal.INICIO)) {
			final Query query = em.createNamedQuery("PersonaExterna.findByIdDependencia");
			query.setParameter(ID, dependencia);
			final List<PersonaExterna> personasExternas = query.getResultList();
			if (personasExternas != null) {
				for (PersonaExterna pe : personasExternas) {
					listPersonasExternas.add(new SelectItem(pe.getId(), pe
							.getNombres()));
				}
			}
		}
	}

	//@Create
	@Override
	public void crear() {
		log.info("Inicio carga dependencias externas.");
		listPersonasExternas.clear();
		listPersonasExternas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDependencia.clear();
		listDependencia.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));

		Query query = em.createNamedQuery("DependenciaExterna.findByAll");

//		query.setParameter("idDepartamento", usuario.getCargo()
//				.getUnidadOrganizacional().getDepartamento().getId());

		final List<DependenciaExterna> dependencias = query.getResultList();

		if (dependencias != null) {
			for (DependenciaExterna de : dependencias) {
				listDependencia.add(new SelectItem(de.getId(), de
						.getDescripcion()));
			}
		}
//		buscarDepartamentos();
//		listDepartamento = jerarquias.getDepartamentos(SELECCIONAR_INICIO);
//		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//		this.buscarUnidadesOrganizacionales();
	}

	/**
	 * @return {@link List} of {@link DestinatariosFrecuentes}
	 */
	@SuppressWarnings("unchecked")
	private List<DestinatariosFrecuentes> cargarListasExistentes() {
		final StringBuffer misListasJPQL = new StringBuffer(
				SELECT_DESTINATARIOS_FRECUENTES);
		misListasJPQL
				.append("ldf.propietario.id = ? and (ldf.esGrupo is null or ldf.esGrupo = false)");
		final Query misListasQuery = em.createQuery(misListasJPQL.toString());
		misListasQuery.setParameter(1, usuario.getId());
		final List<DestinatariosFrecuentes> misListas = misListasQuery
				.getResultList();

		return misListas;
	}

	@Override
	public void agregarListaDestinatarios(final Long idLista) {
		final DestinatariosFrecuentes lista = em.find(
				DestinatariosFrecuentes.class, idLista);
		final List<Persona> destinatariosDesdeLista = lista
				.getDestinatariosFrecuentes();

		for (Persona destinatario : destinatariosDesdeLista) {
			this.persona = destinatario.getId();
			this.cargo = destinatario.getCargo().getId();
			this.unidadOrganizacional = destinatario.getCargo()
					.getUnidadOrganizacional().getId();
			this.departamento = destinatario.getCargo()
					.getUnidadOrganizacional().getDepartamento().getId();
			this.division = destinatario.getCargo().getUnidadOrganizacional()
					.getDepartamento().getDivision().getId();
			this.agregarDestinatario();
		}
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
		log.info("Destroy and Remove of the bean.");
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}
	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectPersonas> getDestinatariosDocumento() {
		if (this.destinatariosDocumento == null) {
			this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		}
		return destinatariosDocumento;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getIdAlias() {
		return idAlias;
	}

	/**
	 * @param idAlias
	 *            {@link List}
	 */
	public void setIdAlias(final Long idAlias) {
		this.idAlias = idAlias;
	}

	/**
	 * @return {@link List} lista de Alias.
	 */
	public List<SelectItem> getAlias() {
		return alias;
	}

	@Override
	public List<SelectItem> getListDivision() {
		if (documento instanceof Providencia) {
			if (listDivision == null) {
				listDivision = new ArrayList<SelectItem>();
			}
			if (listDivision.size() <= 1) {
				this.buscarDivisiones();
			}
		}
		return listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getDependencia() {
		return dependencia;
	}

	@Override
	public void setDependencia(final Long dependencia) {
		this.dependencia = dependencia;
	}

	@Override
	public Long getPersonaExterna() {
		return personaExterna;
	}

	@Override
	public void setPersonaExterna(final Long personaExterna) {
		this.personaExterna = personaExterna;
	}

	@Override
	public List<SelectItem> getListDependencia() {
		return listDependencia;
	}

	@Override
	public List<SelectItem> getListPersonasExternas() {
		return listPersonasExternas;
	}

	@Override
	public List<DestinatariosFrecuentes> getListListas() {
		listListas = this.cargarListasExistentes();
		return listListas;
	}

	@Override
	public void setListListas(final List<DestinatariosFrecuentes> listListas) {
		this.listListas = listListas;
	}

	@Override
	public Long getGrupo() {
		return grupo;
	}

	@Override
	public void setGrupo(final Long grupo) {
		this.grupo = grupo;
	}

	@Override
	public List<SelectItem> getListDestFrec() {
		listDestFrec = this.buscarDestFrec();
		return listDestFrec;
	}

	@Override
	public void setListDestFrec(final List<SelectItem> listDestFrec) {
		this.listDestFrec = listDestFrec;
	}

	/**
	 * @return {@link List} {@link SelectItem}
	 */
	private List<SelectItem> buscarDestFrec() {
		final DestinatariosFrecuentes grupoDestFrec = this.obtenerGrupo();
		if (grupoDestFrec != null) {
			final List<Persona> destinatariosFrecuentes = grupoDestFrec
					.getDestinatariosFrecuentes();
			final List<SelectItem> listDestinatariosFrecuentes = new ArrayList<SelectItem>();
			listDestinatariosFrecuentes.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			for (Persona p : destinatariosFrecuentes) {
				listDestinatariosFrecuentes.add(new SelectItem(p.getId(), p
						.getNombreApellido()));
			}
			return listDestinatariosFrecuentes;
		} else {
			final List<SelectItem> listDestinatariosFrecuentes = new ArrayList<SelectItem>();
			listDestinatariosFrecuentes.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			return listDestinatariosFrecuentes;
			
		}
	}

	/**
	 * @return {@link DestinatariosFrecuentes}
	 */
	private DestinatariosFrecuentes obtenerGrupo() {
		DestinatariosFrecuentes miGrupo;
		try {
			final StringBuffer miGrupoJPQL = new StringBuffer(
					SELECT_DESTINATARIOS_FRECUENTES);
			miGrupoJPQL.append("ldf.propietario.id = ? and ldf.esGrupo = true");
			final Query miGrupoQuery = em.createQuery(miGrupoJPQL.toString());
			miGrupoQuery.setParameter(1, usuario.getId());
			miGrupo = (DestinatariosFrecuentes) miGrupoQuery.getSingleResult();
		} catch (NoResultException nre) {
			miGrupo = null;
		}

		return miGrupo;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		if (!(documento instanceof Providencia)) {
			if (listOrganizacion == null) {
				listOrganizacion = new ArrayList<SelectItem>();
			}
			if (listOrganizacion.size() == 0) {
				listOrganizacion.addAll(this.buscarOrganizaciones());
				
				listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL));
				listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL));
				listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL));
				listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL));
				listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
						JerarquiasLocal.TEXTO_INICIAL));
			}
			
		}
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public boolean tieneEmisor() {
		return documento.getIdEmisor() != null;
	}

	@Override
	public boolean isVisibleDivision() {
		if (jerarquias.isJefeGabinete(documento.getIdEmisor())
				|| jerarquias.isSubsecretario(documento.getIdEmisor())
				|| jerarquias.isJefeDivision(documento.getIdEmisor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isVisibleDepartamento() {
		if (this.isVisibleDivision()
				|| jerarquias.isJefeDepartamento(documento.getIdEmisor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isVisibleDptoDivision() {
		if (!this.isVisibleDivision()
				&& jerarquias.isJefeDepartamento(documento.getIdEmisor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isVisibleUnidad() {
		if (this.isVisibleDepartamento()
				|| jerarquias.isJefeUnidad(documento.getIdEmisor())) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isVisibleUnidadDpto() {
		if (!this.isVisibleDepartamento()
				&& jerarquias.isJefeUnidad(documento.getIdEmisor())) {
			return true;
		}
		return false;
	}

	@Override
	public List<SelectItem> getListTodosDepartamento() {
		listTodosDepartamento = jerarquias
				.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL);
		return listTodosDepartamento;
	}

	@Override
	public List<SelectItem> getListTodosUnidades() {
		listTodosUnidades = jerarquias
				.getUnidades(JerarquiasLocal.TEXTO_INICIAL);
		return listTodosUnidades;
	}

	@Override
	public void agregarDestinatarioById(final Long id) {
		final Persona destinatario = em.find(Persona.class, id);
		this.agregaDestinatario(destinatario);
	}

	@Override
	public Boolean getAgregarDestinatarioDocumento() {
		return agregarDestinatarioDocumento;
	}

	@Override
	public void setAgregarDestinatarioDocumento(
			final Boolean agregarDestinatarioDocumento) {
		this.agregarDestinatarioDocumento = agregarDestinatarioDocumento;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getNombres() {
		return nombres;
	}

	@Override
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	@Override
	public String getApellido() {
		return apellido;
	}

	@Override
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public void agregarModificar(Long id, Boolean copia) {
		// TODO Auto-generated method stub
		
	}
	
}
