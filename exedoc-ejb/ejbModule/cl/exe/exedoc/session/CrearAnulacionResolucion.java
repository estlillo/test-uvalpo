package cl.exe.exedoc.session;

import java.util.Locale;

import javax.ejb.Local;

import cl.exe.exedoc.entity.AnulacionResolucion;

/**
 * Interface CrearAnulacionResolucion.
 * 
 * @author Ricardo Fuentes
 */
@Local
public interface CrearAnulacionResolucion {

	/**
	 * Inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * Finaliza la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Destruye la converzacion.
	 */
	void destroy();

	/**
	 * @return {@link Boolean}
	 */
	Boolean getReload();

	/**
	 * @param reload {@link Boolean}
	 */
	void setReload(final Boolean reload);

	/**
	 * @return {@link AnulacionResolucion}
	 */
	AnulacionResolucion getAnulacionResolucion();

	/**
	 * @param anulacionResolucion {@link AnulacionResolucion}
	 */
	void setAnulacionResolucion(final AnulacionResolucion anulacionResolucion);

	/**
	 * @return {@link Locale}
	 */
	Locale getLocale();

	/**
	 * Metodo que guarda una solicitud de anulacion de una resolicion.
	 * 
	 * @return {@link String}
	 */
	String guardarAnulacion();

	/**
	 * Metodo que valida si la propuesta de Metas puede ser editada.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean isEditable();

	/**
	 * Metodo que valida, si un documento se encuentra firmado.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean getVisado();

	/**
	 * Metodo que valida, si un documento se encuentra firmado.
	 * 
	 * @return {@link Boolean}
	 */
	Boolean getFirmado();

	/**
	 * limpiar formulario.
	 * 
	 * @return {@link String}
	 */
	String limpiarFormulario();

	/**
	 * Acepta la anulacion de una resolucion, lo cual eliminara el numero de la resolucion.
	 */
	void aceptarAnulacion();

}
