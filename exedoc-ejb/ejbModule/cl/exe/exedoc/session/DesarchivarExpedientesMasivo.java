package cl.exe.exedoc.session;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

@Local
public interface DesarchivarExpedientesMasivo {

	String begin(boolean administrador, boolean owner);

	String end();

	void destroy();

	/**
	 * Buscar Expedientes del usuario actual del sistema (logeado).
	 */
	void buscarExpedienteDelUsuario();

	/**
	 * busca Expedientes.
	 * 
	 * @param isAdmin {@link Boolean}
	 */
	void buscarExpedientes(final Boolean isAdmin);

	/**
	 * Metodo que carga las personas a las cuales se le reasignara un expediente.
	 */
	void agregarPersona();

	String desarchivarYAsignar();

	boolean renderedVerExpediente(Long idDocumento);

	void buscarDeptos();

	void buscarUnidades();

	void buscarCargos();

	void buscarPersonas();

	Long obtenerIdExpediente(String numeroExpediente);

	void setExpedientesADesarchivar(Long idExpediente);

	void eliminarPersona(Persona persona);

	// public void buscarClasificaciones();

	/*
	 * Getters & Setters
	 */
	String getTipoDocumentoBusqueda();

	void setTipoDocumentoBusqueda(String tipoDocumentoBusqueda);

	String getNumeroExpedienteBusqueda();

	void setNumeroExpedienteBusqueda(String numeroExpedienteBusqueda);

	String getNumeroDocumentoBusqueda();

	void setNumeroDocumentoBusqueda(String numeroDocumentoBusqueda);

	String getEmisorDocumentoBusqueda();

	void setEmisorDocumentoBusqueda(String emisorDocumentoBusqueda);

	Date getFechaArchivadoInicioBusqueda();

	void setFechaArchivadoInicioBusqueda(Date fechaArchivadoInicioBusqueda);

	Date getFechaArchivadoTerminoBusqueda();

	void setFechaArchivadoTerminoBusqueda(Date fechaArchivadoTerminoBusqueda);

	String getMateriaDocumentoBusqueda();

	void setMateriaDocumentoBusqueda(String materiaDocumentoBusqueda);

	Long getIdDivision();

	void setIdDivision(Long idDivision);

	Long getIdDepto();

	void setIdDepto(Long idDepto);

	Long getIdUnidad();

	void setIdUnidad(Long idUnidad);

	Long getIdCargo();

	void setIdCargo(Long idCargo);

	Long getIdPersona();

	void setIdPersona(Long idPersona);

	List<SelectItem> getListDivisiones();

	void setListDivisiones(List<SelectItem> listDivisiones);

	List<SelectItem> getListDeptos();

	void setListDeptos(List<SelectItem> listDeptos);

	List<SelectItem> getListUnidades();

	void setListUnidades(List<SelectItem> listUnidades);

	List<SelectItem> getListCargos();

	void setListCargos(List<SelectItem> listCargos);

	List<SelectItem> getListPersonas();

	void setListPersonas(List<SelectItem> listPersonas);

	List<ExpedienteBandejaSalida> getExpedientesArchivados();

	void setExpedientesArchivados(List<ExpedienteBandejaSalida> expedientesArchivados);

	Object[] getListDestinatarios();

	void setListDestinatarios(Set<Persona> listDestinatarios);

	Map<Long, Boolean> getSelectedDesarchivados();

	void setSelectedDesarchivados(Map<Long, Boolean> selectedDesarchivados);

	List<Long> getIdExpedientes();

	void setIdExpedientes(List<Long> idExpedientes);

	int getCopiaDirectoFlag();

	void setCopiaDirectoFlag(int copiaDirectoFlag);

	Locale getLOCALE();

	String getTIMEZONE();

	String getClasificacionTipoBusqueda();

	void setClasificacionTipoBusqueda(String clasificacionTipoBusqueda);

	String getClasificacionSubtipoBusqueda();

	void setClasificacionSubtipoBusqueda(String clasificacionSubtipoBusqueda);

	String getClasificacionMateriaBusqueda();

	void setClasificacionMateriaBusqueda(String clasificacionMateriaBusqueda);

	List<SelectItem> getListClasificacionTipo();

	void setListClasificacionTipo(List<SelectItem> listClasificacionTipo);

	List<SelectItem> getListClasificacionSubtipo();

	void setListClasificacionSubtipo(List<SelectItem> listClasificacionSubtipo);

	List<SelectItem> getListClasificacionMateria();

	void setListClasificacionMateria(List<SelectItem> listClasificacionMateria);

	int getFlagMostrarClasificaciones();

	void setFlagMostrarClasificaciones(int flagMostrarClasificaciones);

	void buscarDivisiones();

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	List<SelectItem> getListOrganizacion();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	/**
	 * @param organismoBusqueda {@link Long}
	 */
	void setOrganismoBusqueda(Long organismoBusqueda);

	/**
	 * @return {@link Long}
	 */
	Long getOrganismoBusqueda();

	String desarchivar();

}
