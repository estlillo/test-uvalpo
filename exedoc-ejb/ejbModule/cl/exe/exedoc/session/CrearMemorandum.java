package cl.exe.exedoc.session;

import java.util.List;

import javax.ejb.Local;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.VisacionDocumento;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Administrator
 *
 */
@Local
public interface CrearMemorandum {

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return {@link String}
	 */
	String end();

	/**
	 * 
	 */
	void guardar();

	/**
	 * 
	 */
	void visualizar();

	/**
	 * 
	 */
	void firmar();

	/**
	 * 
	 */
	void visar();

	/**
	 * @return {@link String}
	 */
	String agregarDocumento();

	/**
	 * 
	 */
	void guardarYCrearExpediente();

	/**
	 * @return {@link Memorandum}
	 */
	Memorandum getMemorandum();

	/**
	 * @return {@link String}
	 */
	String getParrafo();

	/**
	 * @param parrafo {@link String}
	 */
	void setParrafo(String parrafo);

	/**
	 * @return {@link List}<{@link SelectPersonas}>
	 */
	List<SelectPersonas> getDistribucionDocumento();

	/**
	 * @return {@link List}<{@link SelectPersonas}>
	 */
	List<SelectPersonas> getDestinatariosDocumento();

	/**
	 * @return {@link List}<{@link ArchivoAdjuntoDocumentoElectronico}>
	 */
	List<ArchivoAdjuntoDocumentoElectronico> getListaArchivos();

	/**
	 * @return {@link Boolean}
	 */
	Boolean getFirmado();

	/**
	 * @return {@link Boolean}
	 */
	Boolean getVisado();

	/**
	 * @return {@link List}<{@link VisacionDocumento}>
	 */
	List<VisacionDocumento> getVisaciones();

	/**
	 * @return {@link String}
	 */
	String completar();

	/**
	 * @return {@link Boolean}
	 */
	Boolean getCompletado();

	/**
	 * @return {@link boolean}
	 */
	boolean isEditable();

	/**
	 * @return {@link boolean}
	 */
	boolean isEditableSinVisa();

	/**
	 * @return {@link boolean}
	 */
	boolean isDespachable();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonFirmar();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonDespachar();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonGuardar();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonGuardarCrearExpediente();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonVisar();

	/**
	 * @return {@link boolean}
	 */
	boolean isRenderedBotonVisualizar();

	/* **************************************************************************
	 * Archivos Anexos - Antecedentes************************************************************************
	 */
	/**
	 * 
	 */
	void limpiarAnexos();

	/**
	 * @return {@link int}
	 */
	int getMaxFilesQuantity();

	/**
	 * @param event {@link UploadEvent}
	 * @throws Exception 
	 */
	void listener(UploadEvent event) throws Exception;

	/**
	 * 
	 */
	void agregarAntecedenteArchivo();

	/**
	 * @param idArchivo {@link Long}
	 */
	void verArchivo(Long idArchivo);
	
	/**
	 * @return {@link String}
	 */
	String getMateriaArchivo();

	/**
	 * @param materiaArchivo {@link String}
	 */
	void setMateriaArchivo(String materiaArchivo);

	// public List<Documento> getListDocumentosAnexo();

	/* **************************************************************************
	 * DESPACHAR EXPEDIENTES************************************************************************
	 */
	/**
	 * @return {@link String}
	 */
//	String despacharExpediente();

	/**
	 * @param idNuevoArchiv {@link Integer}
	 */
	void eliminarArchivoAntecedente(ArchivoAdjuntoDocumentoElectronico archivo);

	//String getVisaciones();

	List<FirmaEstructuradaDocumento> getFirmas();

	Long getRand();

	void setRand(Long rand);

	String getUrlFirma();

	String getSessionId();

	String buscaDocumento();

	String buscaDocumento2();

	boolean isDocumentoValido();

	boolean isValidarAntecedente();

	void removeArchivoAnexo();

	void previsualizar();

}
