package cl.exe.exedoc.session;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DiaAdministrativo;
import cl.exe.exedoc.entity.Persona;

@Local
public interface CrearSolDiaAdministrativo {

    public String end();
    public void destroy();
    public String aprobarSolDiaAdministrativo();
    public String rechazarSolDiaAdministrativo();
    public void agregarDia();   
    public void eliminarDia(Date fecha, Integer parteDia);  
    public String agregarDocumento();
    
    public Persona getFuncionario();
    public void setFuncionario(Persona funcionario);

    public DiaAdministrativo getDiaAdministrativo();
    public void setDiaAdministrativo(DiaAdministrativo diaAdministrativo);
    public String getNombreFuncionario();
    public void setNombreFuncionario(String nombreFuncionario);
    public boolean existePersona();
    public boolean isEditable();
    public boolean isRenderedBotonGuardar();
    public boolean isRenderedMensajeRechazado();
    public void guardarYCrearExpediente();
    public boolean isRenderedBotonGuardarCrearExpediente();
    public Persona getUsuario();
    public void setUsuario(Persona usuario);
    public String getCalidadJuridica();
    public void setCalidadJuridica(String calidadJuridica);
    public boolean isDiasOk();
    public void setDiasOk(boolean diasOk);
    public Double getFeriadosLegalesPendientes();
    public void setFeriadosLegalesPendientes(Double feriadosLegalesPendientes);
    public Double getDiasAdministrativosPendientes();
    public void setDiasAdministrativosPendientes(Double diasAdministrativosPendientes);
    public String getGrado();
    public void setGrado(String grado);
    public String getTipoDia();
    public void setTipoDia(String tipoDia);
    public Persona getSolicitante();
    public void setSolicitante(Persona solicitante);
    public boolean isRenderedBotonAceptarRechazar();
    public List<DetalleDias> getDiasSolicitados();
    public String getDatosObtenidos();
    public void setDatosObtenidos(String datosObtenidos);
    public DetalleDias getDetalleDias();
    public void setDetalleDias(DetalleDias detalleDias);
    public String getPeriodoDia();
    public void setPeriodoDia(String periodoDia);
    public void setFechaDia(Date fechaDia);
    public Date getFechaDia();
    public Integer getIdPlantillaSelec();
    public void setIdPlantillaSelec(Integer idPlantillaSelec);
    Boolean getVisado();
	Boolean getFirmado();
	void obtenerDatosExternos();
	void limpiarDiasSolicitados();
	String guardarDocumento();
}