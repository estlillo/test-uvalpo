package cl.exe.exedoc.session;

import java.util.Map;

import javax.ejb.Local;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;

@Local
public interface BandejaEntradaCopia {

	String begin();

	String end();

	void destroy();

	Map<Long, Boolean> getSelectedRecibosCopia();

	void setSelectedRecibosCopia(Map<Long, Boolean> selectedRecibosCopia);

	String acusarRecibo();

	void buscarExpedientes();

	String obtenerEstilo(ExpedienteBandejaEntrada exp);

	Boolean getDesdeDespacho();

	void setDesdeDespacho(Boolean desdeDespacho);
}
