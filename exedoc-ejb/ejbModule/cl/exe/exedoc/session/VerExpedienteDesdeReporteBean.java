package cl.exe.exedoc.session;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.DestinatarioDocumento;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.DocumentoPapel;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Observacion;
import cl.exe.exedoc.entity.ObservacionArchivo;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Providencia;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.util.DocUtils;
import cl.exe.exedoc.util.SelectPersonas;

@Stateful
@Name("verExpedienteDesdeReporte")
public class VerExpedienteDesdeReporteBean implements VerExpedienteDesdeReporte {

	@EJB
	private ManejarExpedienteInterface me;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosAnexo;*/

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Observacion> observaciones;

	// Esto viene de la solicitud de dia administrativo y de vacaciones
	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Persona solicitante;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private String grado;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Double diasAdministrativosPendientes;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Double diasVacacionesPendientes;

	@In(required = false, value = "calidadJuridica", scope = ScopeType.CONVERSATION)
	@Out(required = false, value = "calidadJuridica", scope = ScopeType.CONVERSATION)
	private String calidadJuridica;*/

	/*@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private String datosObtenidos;*/

	/*@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento consulta;*/

	/*@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento respuesta;*/

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> distribucionDocumento;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean vistoDesdeReporte;

	@Out(required = false, scope = ScopeType.CONVERSATION)
	Documento documento;

	@In(required = false)
	private Persona usuario;

	@In(required = false, scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;
	
	@In(required = false, scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrearDesdeReporte;

	@RequestParameter
	private Long idExpediente;

	@RequestParameter
	private Boolean bandejaEntradaCopia;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean estaBandejaEntradaCopia;

	@EJB
	private RepositorioLocal repositorio;

	private List<SelectItem> destinatariosDocumentoOriginal;
	private int contadorDocumentos;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Boolean desdeDespacho;
	
	@Override
	public String begin(String homeCrear) {
		this.homeCrear = homeCrear;
		return this.begin();
	}

	@Begin(join = true)
	public String begin() {
//		homeCrear = "verExpedienteDesdeReporte";
		//homeCrear = homeCrearDesdeReporte;
		log.info("Comenzando conversacion: verExpedienteDesdeReporte");
		log.info("homeCrear: {0}", homeCrear);

		if (bandejaEntradaCopia != null) {
			estaBandejaEntradaCopia = bandejaEntradaCopia;
		}

		listDocumentosRespuesta = new ArrayList<Documento>();
		//listDocumentosAnexo = new ArrayList<Documento>();
		observaciones = new ArrayList<Observacion>();
		destinatariosDocumentoOriginal = new ArrayList<SelectItem>();

		// homeCrear = "verExpedienteDesdeReporte";

		expediente = em.find(Expediente.class, idExpediente);

		contadorDocumentos = 0;

		for (DocumentoExpediente de : expediente.getDocumentos()) {
			/*if (de.getDocumento() instanceof Resolucion) {
				Resolucion r = (Resolucion) de.getDocumento();
				for (ArchivoAdjuntoDocumentoElectronico ar : r.getArchivosAdjuntos()) {
					DocumentoBinario db = new DocumentoBinario();
					ArchivoDocumentoBinario adb = new ArchivoDocumentoBinario();
					adb.setNombreArchivo(ar.getNombreArchivo());
					adb.setCmsId(ar.getCmsId());
					adb.setFecha(ar.getFecha());
					adb.setContentType(ar.getContentType());
					db.setIdDocumentoReferencia(r.getId().intValue());
					db.setArchivo(adb);
					db.setMateria(ar.getMateria());
					db.setNumDocumentoReferencia(r.getNumeroDocumento());
					this.listDocumentosAnexo.add(db);
				}
			}
			if (de.getDocumento() instanceof Decreto) {
				Decreto d = (Decreto) de.getDocumento();
				for (ArchivoAdjuntoDocumentoElectronico ad : d.getArchivosAdjuntos()) {
					DocumentoBinario db = new DocumentoBinario();
					ArchivoDocumentoBinario adb = new ArchivoDocumentoBinario();
					adb.setNombreArchivo(ad.getNombreArchivo());
					adb.setCmsId(ad.getCmsId());
					adb.setFecha(ad.getFecha());
					adb.setContentType(ad.getContentType());
					db.setIdDocumentoReferencia(d.getId().intValue());
					db.setArchivo(adb);
					db.setMateria(ad.getMateria());
					db.setNumDocumentoReferencia(d.getNumeroDocumento());
					this.listDocumentosAnexo.add(db);
				}
			}*/
			if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.RESPUESTA)) {
				de.getDocumento().setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
				if (de.getDocumento().getEliminado() == null || !de.getDocumento().getEliminado()) {
					this.listDocumentosRespuesta.add(de.getDocumento());
				}
			}/* else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ANEXO)) {
				de.getDocumento().setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.ANEXO));
				if (de.getDocumento() instanceof DocumentoBinario) {
					if (!de.getIdDocumentoReferencia().equals(-1L)) {
						((DocumentoBinario) de.getDocumento()).setIdDocumentoReferencia(de.getIdDocumentoReferencia().intValue());
					}
				}
				this.listDocumentosAnexo.add((DocumentoBinario) de.getDocumento());
			}*/ else if (de.getTipoDocumentoExpediente().getId().equals(TipoDocumentoExpediente.ORIGINAL)) {
				documentoOriginal = de.getDocumento();
				documentoOriginal.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.ORIGINAL));
				if (documentoOriginal instanceof DocumentoElectronico) {
					((DocumentoElectronico) documentoOriginal).getParrafos().size();
					if (documentoOriginal instanceof Providencia) {
						((Providencia) documentoOriginal).getAcciones().size();
					}
					if (documentoOriginal instanceof Resolucion) {
						((Resolucion) documentoOriginal).getArchivosAdjuntos().size();
					}
					if (documentoOriginal instanceof Decreto) {
						((Decreto) documentoOriginal).getArchivosAdjuntos().size();
					}
//					if (documentoOriginal instanceof DiaAdministrativo) {
//						((DiaAdministrativo) documentoOriginal).getListDetalleDias().size();
//					}
				}
				documentoOriginal.getDistribucionDocumento().size();
				documentoOriginal.getVisaciones().size();
				documentoOriginal.getFirmas().size();
				documentoOriginal.getBitacoras().size();
			}
			if (de.getEnEdicion()) {
				de.getDocumento().setEnEdicion(false);
			}
			de.getDocumento().setIdNuevoDocumento(contadorDocumentos++);
			de.getDocumento().getFirmas().size();
			de.getDocumento().getVisaciones().size();
			de.getDocumento().getFirmasEstructuradas().size();
			de.getDocumento().getVisacionesEstructuradas().size();
			de.getDocumento().getBitacoras().size();
		}

		//relacionarAnexoConDocumento();

		observaciones.addAll(expediente.getObservaciones());
		java.util.Collections.sort(observaciones);

		setDestinatarioDocumentoOriginal();

		return "verExpedienteDesdeReporte";
	}

	/*private void relacionarAnexoConDocumento() {
		for (Documento doc : listDocumentosAnexo) {
			if (doc instanceof DocumentoBinario) {
				DocumentoBinario docBin = (DocumentoBinario) doc;
				if (docBin.getIdDocumentoReferencia() != null) {
					for (Documento docResp : listDocumentosRespuesta) {
						if (docResp.getId().equals(docBin.getIdDocumentoReferencia().longValue())) {
							docBin.setIdDocumentoReferencia(docResp.getIdNuevoDocumento());
							docBin.setNumDocumentoReferencia(docResp.getNumeroDocumento());
							break;
						}
					}
					if (documentoOriginal.getId().equals(docBin.getIdDocumentoReferencia().longValue())) {
						docBin.setIdDocumentoReferencia(documentoOriginal.getIdNuevoDocumento());
						docBin.setNumDocumentoReferencia(documentoOriginal.getNumeroDocumento());
					}
				}
			}
		}
	}*/

	private void setDestinatarioDocumentoOriginal() {
		if (destinatariosDocumentoOriginal == null) {
			destinatariosDocumentoOriginal = new ArrayList<SelectItem>();
		} else {
			destinatariosDocumentoOriginal.clear();
		}
		if (this.documentoOriginal.getTipoDocumento() != null
				&& (
//						this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADMINISTRATIVO)
//						|| this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_VACACIONES)
//						|| this.documentoOriginal.getTipoDocumento().getId().equals(TipoDocumento.SOLICITUD_ADQUISICION) || 
						this.documentoOriginal.getTipoDocumento().getId().equals(
						TipoDocumento.RESOLUCION))) {
			if (this.expediente.getDestinatario() != null) {
				destinatariosDocumentoOriginal.add(new SelectItem(destinatariosDocumentoOriginal.size(), this.expediente.getDestinatario().getNombreApellido()));
			} else {
				if (this.expediente.getDestinatarioHistorico() != null) {
					destinatariosDocumentoOriginal.add(new SelectItem(destinatariosDocumentoOriginal.size(), this.expediente.getDestinatarioHistorico().getNombreApellido()));
				}
			}
		} else {
			for (ListaPersonasDocumento dd : this.documentoOriginal.getDestinatarios()) {
				if (dd instanceof DestinatarioDocumento) {
					destinatariosDocumentoOriginal.add(new SelectItem(destinatariosDocumentoOriginal.size(), dd.getDestinatario()));
				}
			}
		}
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public String end() {
		log.info("terminando conversacion: verExpedienteDesdeReporte");
		return "bandejaEntrada";
	}

	public boolean renderedVerArchivo() {
		boolean valido = false;
		if (this.documentoOriginal != null && this.documentoOriginal.getFormatoDocumento() != null) {
			if (documentoOriginal.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL) && 
					documentoOriginal.getCmsId() != null) {
				if (!this.documentoOriginal.getReservado()) {
					valido = true;
				} else {
					if (vistoDesdeReporte == null || (vistoDesdeReporte != null && !vistoDesdeReporte)) {
						valido = true;
					} else {
						for (ListaPersonasDocumento destinatario : documentoOriginal.getListaPersonas()) {
							if (destinatario.getDestinatarioPersona() != null) {
								if (destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
									valido = true;
									break;
								}
							}
						}
					}
				}
			}
		}
		return valido;
	}

	public boolean renderedVerDocumento() {
		boolean valido = false;

		if (this.documentoOriginal != null && this.documentoOriginal.getFormatoDocumento() != null) {
			if (this.documentoOriginal.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO)) {
				if (this.documentoOriginal instanceof Carta ||
						this.documentoOriginal instanceof Memorandum ||
						this.documentoOriginal instanceof Oficio ||
						this.documentoOriginal instanceof Resolucion) {
					if (!this.documentoOriginal.getReservado()) {
						valido = true;
					} else {
						if (vistoDesdeReporte == null || (vistoDesdeReporte != null && !vistoDesdeReporte)) {
							valido = true;
						} else {
							for (ListaPersonasDocumento destinatario : documentoOriginal.getListaPersonas()) {
								if (destinatario.getDestinatarioPersona() != null) {
									if (destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
										valido = true;
										break;
									}
								}
							}
						}
					}
				}
			}
		}

		return valido;
	}

	public boolean renderedVerDocumento(Long idDocumento) {
		boolean valido = false;
		Documento doc = em.find(Documento.class, idDocumento);

		if (doc != null && doc.getFormatoDocumento() != null) {
			if ((doc.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL) && 
						doc.getCmsId() != null) || 
					(doc.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO) &&
						(doc instanceof Carta ||
								doc instanceof Memorandum ||
								doc instanceof Oficio ||
								doc instanceof Resolucion))) {
				if (!doc.getReservado()) {
					valido = true;
				} else {
					if (vistoDesdeReporte == null || (vistoDesdeReporte != null && !vistoDesdeReporte)) {
						valido = true;
					} else {
						for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
							if (destinatario.getDestinatarioPersona() != null) {
								if (destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
									valido = true;
									break;
								}
							}
						}
					}
				}
			}
		}
		return valido;
	}

	@Override
	public void verArchivo() {
		Archivo archivo = ((DocumentoBinario) documentoOriginal).getArchivo();
		//buscarArchivoDesdeRepositorio(archivo);
		try {
            if (!DocUtils.getFile(archivo.getId(), em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + archivo.getId() + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public String modificarDocumento(Integer numDoc, Integer lista) {
		log.info("homeCrear --> " + this.homeCrear);
		this.homeCrearDesdeReporte = "verExpedienteDesdeReporte";
		if (lista.equals(1)) {
			getDocumentoDeLista(listDocumentosRespuesta, numDoc);
		} /*else if (lista.equals(2)) {
			getDocumentoDeLista(listDocumentosAnexo, numDoc);
		}*/ else if (lista.equals(3)) {
			documento = documentoOriginal;
		}

		String homeDistribucion = "";
		TipoDocumentoExpediente t = documento.getTipoDocumentoExpediente();
		Integer id = documento.getIdNuevoDocumento();
		boolean enEdicion = false;//documento.getEnEdicion();

		documento = em.find(Documento.class, documento.getId());

		if (documento instanceof DocumentoElectronico) {
			if (documento.getId() != null) {
				try {
					documento = em.find(Documento.class, documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getRevisar().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento).getArchivosAdjuntos()
							.size();
//					if (documento instanceof DiaAdministrativo) {
//						((DiaAdministrativo) documento).getListDetalleDias().size();
//					}
				} catch (Exception e) {
					documento = em.find(DocumentoElectronico.class,
							documento.getId());
					((DocumentoElectronico) documento).getParrafos().size();
					((DocumentoElectronico) documento)
							.getDistribucionDocumento().size();
					((DocumentoElectronico) documento).getRevisar().size();
					((DocumentoElectronico) documento).getVisaciones().size();
					((DocumentoElectronico) documento).getFirmas().size();
					((DocumentoElectronico) documento).getBitacoras().size();
					((DocumentoElectronico) documento)
							.getVisacionesEstructuradas().size();
					((DocumentoElectronico) documento).getFirmasEstructuradas()
							.size();
					((DocumentoElectronico) documento)
							.getRevisarEstructuradas().size();
//					if (documento instanceof DiaAdministrativo) {
//						((DiaAdministrativo) documento).getListDetalleDias().size();
//					}
				}
			}

			switch (documento.getTipoDocumento().getId()) {
			case 1:
				vistoDesdeReporte = true;
				homeDistribucion = "crearOficioOrdinario";
				break;
			case 2:
				vistoDesdeReporte = true;
				homeDistribucion = "crearOficioCircular";
				break;
			case 3:
				vistoDesdeReporte = true;
				homeDistribucion = "crearOficioReservado";
				break;
			case 6:
				if (documento.getId() != null) {
					try {
						((Resolucion) documento).getArchivosAdjuntos().size();
					} catch (Exception e) {
						documento = em.find(Resolucion.class, documento.getId());
						((Resolucion) documento).getArchivosAdjuntos().size();
					}
				}
				vistoDesdeReporte = true;
				homeDistribucion = "crearResolucion";
				break;
			case 7:
				vistoDesdeReporte = true;
				homeDistribucion = "crearMemorandum";
				break;
			case 8:
				if (documento.getId() != null) {
					try {
						((Providencia) documento).getAcciones().size();
					} catch (Exception e) {
						documento = em.find(Providencia.class, documento.getId());
						((Providencia) documento).getAcciones().size();
					}
				}
				vistoDesdeReporte = true;
				homeDistribucion = "crearProvidencia";
				break;
			case 9:
				if (documento.getId() != null) {
					try {
						((Decreto) documento).getArchivosAdjuntos().size();
					} catch (Exception e) {
						documento = em.find(Decreto.class, documento.getId());
						((Decreto) documento).getArchivosAdjuntos().size();
					}
				}
				vistoDesdeReporte = true;
				homeDistribucion = "crearDecreto";
				break;
			case 10:
				vistoDesdeReporte = true;
				homeDistribucion = "crearCarta";
				break;
			/*
			 * vistoDesdeReporte = true; homeDistribucion = "crearCarta"; break;
			 */
			/*case 15:
				solicitante = null;
				calidadJuridica = "";
				grado = "";
				diasAdministrativosPendientes = null;
				datosObtenidos = null;
				vistoDesdeReporte = true;
				homeDistribucion = "crearSolDiaAdministrativo";
				break;
			case 16:
				solicitante = null;
				calidadJuridica = "";
				grado = "";
				diasVacacionesPendientes = null;
				datosObtenidos = null;
				vistoDesdeReporte = true;
				homeDistribucion = "crearSolVacaciones";
				break;
//			case 17:
//				((Adquisicion) documento).getCriteriosEvaluacion().size();
//				((Adquisicion) documento).getAsistentesEventoAdquisicion().size();
//				vistoDesdeReporte = true;
//				homeDistribucion = "crearSolicitudAdquisicion";
//				break;
			case 20:
				consulta = documento;
				homeDistribucion = "verDocumentoConsulta";
				break;
			case 21:
				respuesta = documento;
				respuesta.getFirmas().size();
				homeDistribucion = "crearRespuestaDocumentoConsulta";
				break;*/
			}
		} else {
			if (documento instanceof DocumentoPapel) {
				vistoDesdeReporte = true;
				homeDistribucion = "crearDocumentoPapel";
//			} else if (documento instanceof Factura) {
//				boolean debeIngresarPago = false;
//				boolean debeAprobarPago = false;
//				Persona u = em.find(Persona.class, usuario.getId());
//				for (Rol rol : u.getRoles()) {
//					if (rol.getId().equals(Rol.GESTIONAR_PAGO)) {
//						debeIngresarPago = true;
//					}
//					if (rol.getId().equals(Rol.APROBAR_PAGO)) {
//						Factura factura = (Factura) documento;
//						if (factura.getEditado()) {
//							debeAprobarPago = true;
//						}
//					}
//				}
//
//				// el orden de estos if es importante
//				homeDistribucion = "visualizarFactura";
//				if (debeIngresarPago)
//					homeDistribucion = "ingresarDatosFactura";
//				if (debeAprobarPago)
//					homeDistribucion = "visualizarDataDeFactura";

			} else if (documento instanceof DocumentoBinario) {
				try {
					((DocumentoBinario)documento).getArchivosAdjuntos().size();
				} catch (Exception e) {
					documento = em.find(DocumentoBinario.class, documento.getId());
					((DocumentoBinario)documento).getArchivosAdjuntos().size();
				}
				vistoDesdeReporte = true;
				homeDistribucion = "crearDocumentoBinario";
			} else {
				homeDistribucion = "";
			}
		}
		if (lista.equals(3)) {
			documento.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.ORIGINAL));
		} else {
			documento.setTipoDocumentoExpediente(t);
		}

		documento.setIdNuevoDocumento(id);
		documento.setEnEdicion(enEdicion);

		destinatariosDocumento = new ArrayList<SelectPersonas>();
		for (ListaPersonasDocumento lpd : this.documento.getDestinatarios()) {
			destinatariosDocumento.add(new SelectPersonas(lpd.getDestinatario(), lpd.getDestinatarioPersona()));
		}
		distribucionDocumento = new ArrayList<SelectPersonas>();
		for (ListaPersonasDocumento lpd : this.documento.getDistribucionDocumento()) {
			distribucionDocumento.add(new SelectPersonas(lpd.getDestinatario(), lpd.getDestinatarioPersona()));
		}

		return homeDistribucion;
	}

	private void getDocumentoDeLista(List<Documento> lista, Integer numDoc) {
		for (Iterator<Documento> doc = lista.iterator(); doc.hasNext();) {
			Documento docTmp = doc.next();
			Integer id = docTmp.getIdNuevoDocumento();
			if (id.equals(numDoc)) {
				documento = docTmp;
				break;
			}
		}
	}

	private void buscarArchivoDesdeRepositorio(ObservacionArchivo o) {
		//byte[] data = repositorio.recuperarArchivo(cmsId);
	    try {
    	    byte[] data = repositorio.getFile(o.getCmsId());
    	    if (data != null) {
    			FacesContext facesContext = FacesContext.getCurrentInstance();
    			try {
    				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
    				response.setContentType(o.getContentType());
    				response.setHeader("Content-disposition", "attachment; filename=\"" + o.getNombreArchivo() + "\"");
    				response.getOutputStream().write(data);
    				response.getOutputStream().flush();
    				facesContext.responseComplete();
    			} catch (IOException ioe) {
    				ioe.printStackTrace();
    			}
		} else {
			FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + o.getCmsId() + ")");
		}
	    } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*try {
            if (!DocUtils.getFile(a.getId(), em)) {
                FacesMessages.instance().add(
                        "No existe el archivo, consulte con el administrador. (Codigo Archivo: " + a.getId() + ")");
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
	}

	public String archivarExpediente() {
		desdeDespacho = true;
		if (expediente == null || (expediente != null && expediente.getReasignado() != null)) {
			FacesMessages.instance().add("El expediente ya no se encuentra en su Bandeja Copia");
		} else {
			me.archivarExpediente(expediente);
			FacesMessages.instance().add("Expediente Archivado");
		}
		return "bandejaEntradaCopia";
	}

	public String volver() {
		return homeCrear;
	}

	public boolean getBandejaEntradaCopia() {
		if (estaBandejaEntradaCopia == null) return false;
		return estaBandejaEntradaCopia;
	}

	public String obtenerMateria() {
		return documentoOriginal.getReservado() ? "MATERIA RESERVADA" : documentoOriginal.getMateria();
	}

	public boolean renderedDestinatarioUnico() {
		return !destinatariosDocumentoOriginal.isEmpty() && destinatariosDocumentoOriginal.size() == 1;
	}

	public Integer calcularSizeListaDestinatarios() {
		return destinatariosDocumentoOriginal.size() <= 1 ? 1 : 2;
	}

	public boolean renderedListaDestinatarios() {
		return !destinatariosDocumentoOriginal.isEmpty() && destinatariosDocumentoOriginal.size() > 1;
	}

	// Getters & Setters
	public Expediente getExpediente() {
		return expediente;
	}

	public void setExpediente(Expediente expediente) {
		this.expediente = expediente;
	}

	public Documento getDocumentoOriginal() {
		return documentoOriginal;
	}

	public void setDocumentoOriginal(Documento documentoOriginal) {
		this.documentoOriginal = documentoOriginal;
	}

	public Long getIdExpediente() {
		return idExpediente;
	}

	public void setIdExpediente(Long idExpediente) {
		this.idExpediente = idExpediente;
	}

	public List<Documento> getListDocumentosRespuesta() {
		return listDocumentosRespuesta;
	}

	public void setListDocumentosRespuesta(List<Documento> listDocumentosRespuesta) {
		this.listDocumentosRespuesta = listDocumentosRespuesta;
	}

	/*public List<Documento> getListDocumentosAnexo() {
		return listDocumentosAnexo;
	}

	public void setListDocumentosAnexo(List<Documento> listDocumentosAnexo) {
		this.listDocumentosAnexo = listDocumentosAnexo;
	}*/

	public List<Observacion> getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(List<Observacion> observaciones) {
		this.observaciones = observaciones;
	}

	public List<SelectItem> getDestinatariosDocumentoOriginal() {
		setDestinatarioDocumentoOriginal();
		return destinatariosDocumentoOriginal;
	}

	public void setDestinatariosDocumentoOriginal(List<SelectItem> destinatariosDocumentoOriginal) {
		this.destinatariosDocumentoOriginal = destinatariosDocumentoOriginal;
	}

	@Override
	public void verArchivoObservacion(ObservacionArchivo a) {
		buscarArchivoDesdeRepositorio(a);
	}
	
	@Override
	public boolean getVistoDesdeReporte() {
		if (vistoDesdeReporte != null && vistoDesdeReporte) {
			return true;
		}
		return false;
	}

}
