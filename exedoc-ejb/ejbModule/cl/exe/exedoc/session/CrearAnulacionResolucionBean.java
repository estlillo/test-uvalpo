package cl.exe.exedoc.session;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.AnulacionResolucion;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.EstadoDocumento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.util.FechaUtil;

/**
 * Class CrearAnulacionResolucionBean, utilizada para el documento electronico de anulacion de una resolucion.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(CrearAnulacionResolucionBean.NAME)
public class CrearAnulacionResolucionBean implements CrearAnulacionResolucion, Serializable {

	/**
	 * NAME nombre de la clase.
	 */
	public static final String NAME = "crearAnulacionResolucion";

	private static final long serialVersionUID = -4259003409666653230L;

	private static final Locale LOCALE = FechaUtil.LOCALE;

	@Logger
	private Log log;

	private Boolean reload = Boolean.TRUE;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Expediente expediente;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<Documento> listDocumentosRespuesta;

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Documento documentoOriginal;

	@In(required = false, scope = ScopeType.CONVERSATION, value = "documento")
	@Out(required = false, scope = ScopeType.SESSION, value = "documento")
	private AnulacionResolucion anulacionResolucion;

	
	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;
	
	

	@In(required = false)
	private String homeCrear;

	private Boolean visado;

	private Boolean firmado;

	/**
	 * Constructor.
	 */
	public CrearAnulacionResolucionBean() {
		super();
		if(anulacionResolucion != null && this.anulacionResolucion.getId() == null)anulacionResolucion.setSolicitante(usuario);
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("Inicia la converzacion {0}", CrearAnulacionResolucionBean.NAME);
		this.reload = Boolean.FALSE;

		if (this.anulacionResolucion.getId() == null) {
			this.limpiar();
		} else {
			this.completarFormulario();
		}

		return CrearAnulacionResolucionBean.NAME;
	}

	/**
	 * Completa los datos del formulario con los que se encuentran guardados en el objeto {@link AnulacionResolucion}.
	 */
	private void completarFormulario() {

	}

	/**
	 * Limpia el objeto que sera utilizado para guardar guardar datos del formulario.
	 */
	private void limpiar() {
		final Persona persona = em.find(Persona.class, usuario.getId());
		this.anulacionResolucion.setSolicitante(persona);
	}

	@Override
	public String end() {
		this.reload = Boolean.TRUE;
		log.info("Finaliza la conversacion {0}....", CrearAnulacionResolucionBean.NAME);
		return homeCrear;
	}

	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public Boolean isEditable() {
		Boolean isEditable = Boolean.TRUE;

		if (this.expediente != null && this.expediente.getFechaDespacho() != null) { return Boolean.FALSE; }
		if (this.getVisado()) {
			isEditable = Boolean.FALSE;
		}
		if (this.getFirmado()) {
			isEditable = Boolean.FALSE;
		}

		return isEditable;
	}

	@Override
	public Boolean getVisado() {
		if (this.anulacionResolucion != null && this.anulacionResolucion.getVisaciones() != null
				&& this.anulacionResolucion.getVisaciones().size() != 0) {
			visado = Boolean.TRUE;
		} else {
			visado = Boolean.FALSE;
		}
		return visado;
	}

	@Override
	public Boolean getFirmado() {
		if (this.anulacionResolucion != null && this.anulacionResolucion.getFirmas() != null
				&& this.anulacionResolucion.getFirmas().size() != 0) {
			firmado = Boolean.TRUE;
		} else {
			firmado = Boolean.FALSE;
		}
		return firmado;
	}

	@Override
	public String guardarAnulacion() {
		final int cont = listDocumentosRespuesta.size() + 1;
		this.anulacionResolucion.setNumeroDocumento(this.documentoOriginal.getNumeroNumeroDocumento());
		this.anulacionResolucion.setAutor(usuario);
		this.anulacionResolucion.setFechaCreacion(new Date());
		this.anulacionResolucion.setEnEdicion(false);
		this.anulacionResolucion.setReservado(false);
		this.anulacionResolucion.setEliminable(false);
		this.anulacionResolucion.setEmisor(usuario.getNombreApellido());
		this.anulacionResolucion.setFormatoDocumento(new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		this.anulacionResolucion.setTipoDocumento(new TipoDocumento(TipoDocumento.ANULACION_RESOLUCION));
		this.anulacionResolucion.setIdNuevoDocumento(cont);

		if (this.anulacionResolucion.getId() != null) {
			this.anulacionResolucion.setEstado(new EstadoDocumento(EstadoDocumento.GUARDADO));
			em.merge(this.anulacionResolucion);
		} else {
			this.anulacionResolucion.setEstado(new EstadoDocumento(EstadoDocumento.CREADO));
			
			em.persist(this.anulacionResolucion);
			final Long id = this.anulacionResolucion.getId();
			this.anulacionResolucion.setCodigoBarra(id);
			em.merge(this.anulacionResolucion);
			this.actualizarDocumentoExpediente();
		}
		return this.end();
	}

	/**
	 * Actualizar documento de respuesta del expediente actual, permite agregar mas documentos de respuestas el
	 * expediente.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	private void actualizarDocumentoExpediente() {
		final DocumentoExpediente de = new DocumentoExpediente();

		de.setDocumento(this.anulacionResolucion);
		de.setExpediente(this.expediente);
		de.setEnEdicion(false);
		de.setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));

		em.persist(de);

		de.getDocumento().setTipoDocumentoExpediente(new TipoDocumentoExpediente(TipoDocumentoExpediente.RESPUESTA));
		if (de.getDocumento().getEliminado() == null || !de.getDocumento().getEliminado()) {
			this.listDocumentosRespuesta.add(de.getDocumento());
		}
		de.getDocumento().setEliminable(true);
	}

	@Override
	public void aceptarAnulacion() {
		documentoOriginal.setNumeroDocumento("s/n");
		documentoOriginal.setEstado(em.find(EstadoDocumento.class, EstadoDocumento.ANULADO));
		em.merge(documentoOriginal);
		// this.resolucion.anularResolucion();
		this.end();
	}

	@Override
	public String limpiarFormulario() {
		return CrearAnulacionResolucionBean.NAME;
	}

	@Override
	public Boolean getReload() {
		return reload;
	}

	@Override
	public void setReload(final Boolean reload) {
		this.reload = reload;
	}

	@Override
	public AnulacionResolucion getAnulacionResolucion() {
		return anulacionResolucion;
	}

	@Override
	public void setAnulacionResolucion(final AnulacionResolucion anulacionResolucion) {
		this.anulacionResolucion = anulacionResolucion;
	}

	@Override
	public Locale getLocale() {
		return LOCALE;
	}

}
