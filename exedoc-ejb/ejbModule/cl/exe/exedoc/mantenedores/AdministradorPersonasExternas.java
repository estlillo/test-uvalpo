package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.PersonaExterna;

/**
 * @author Administrator
 *
 */
@Local
public interface AdministradorPersonasExternas {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void crearNuevo();
	
	/**
	 * Editar persona externa.
	 * @return {@link String}
	 */
	void editar();
	
	/**
	 * Limpiar formulario.
	 */
	void limpiar();

	/**
	 * @param id Long
	 */
	void modificar(Long id);

	/**
	 * @return Long
	 */
	Long getDependencia();

	/**
	 * @param dependencia Long
	 */
	void setDependencia(Long dependencia);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getDependencias();

	/**
	 * @return PersonaExterna
	 */
	PersonaExterna getPersonaExterna();

	/**
	 * @return List<PersonaExterna>
	 */
	List<PersonaExterna> getPersonasExternas();

	/**
	 * @return {@link Boolean}, estado para identificar, si el dato es para edición.
	 */
	boolean isEditando();

	/**
	 * @param editando {@link Boolean}
	 */
	void setEditando(final boolean editando);

	
	
}
