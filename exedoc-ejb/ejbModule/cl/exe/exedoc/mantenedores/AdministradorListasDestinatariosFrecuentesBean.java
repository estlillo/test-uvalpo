package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link AdministradorListasDestinatariosFrecuentes}.
 */
@Stateful
@Name("administradorListasDestinatariosFrecuentes")
@Scope(value = ScopeType.SESSION)
public class AdministradorListasDestinatariosFrecuentesBean implements AdministradorListasDestinatariosFrecuentes {

	private static final String ID = "id";
	
	public static final int LARGO_MAXIMO = 99;

	private String nombreLista;

	private Long organizacion;
	private Long idDivision;
	private Long idDepto;
	private Long idUnidad;
	private Long idCargo;
	private Long idPersona;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDeptos;
	private List<SelectItem> listUnidades;
	private List<SelectItem> listCargos;
	private List<SelectItem> listPersonas;

	private List<Persona> listDestinatarios;
	private List<DestinatariosFrecuentes> listListas;

	private int flag;
	private Long idUltimaListaCargada;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	/**
	 * Constructor.
	 */
	public AdministradorListasDestinatariosFrecuentesBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {
		log.info("Iniciando modulo... ");

		nombreLista = "";

		this.buscarPersonas();

		listDestinatarios = new ArrayList<Persona>();

		this.cargaListas();

		listListas = this.cargarListasExistentes();

		flag = 0;
		idUltimaListaCargada = null;

		return "administradorListasDestinatariosFrecuentes";
	}

	/**
	 * Metodo que carga las listas utilizadas con la carga inicial.
	 */
	private void cargaListas() {
		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		idDivision = (Long) listDivisiones.get(1).getValue();
//		this.buscarDeptos();
//		listDeptos = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setIdDepto(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidades();
		
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarPersona();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarPersona();
		organizacion = idOrganizacion;
		listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void limpiarPersona() {
		organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;
		idDepto = JerarquiasLocal.INICIO;
		idUnidad = JerarquiasLocal.INICIO;
		idCargo = JerarquiasLocal.INICIO;
		idPersona = JerarquiasLocal.INICIO;

		listDivisiones = new ArrayList<SelectItem>();
		listDeptos = new ArrayList<SelectItem>();
		listUnidades = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		listPersonas = new ArrayList<SelectItem>();

		listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDeptos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @return List<DestinatariosFrecuentes>
	 */
	@SuppressWarnings("unchecked")
	private List<DestinatariosFrecuentes> cargarListasExistentes() {
		final StringBuffer misListasJPQL = new StringBuffer("SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
		misListasJPQL.append("ldf.propietario.id = ? and (ldf.esGrupo is null or ldf.esGrupo = false)");
		final Query misListasQuery = em.createQuery(misListasJPQL.toString());
		misListasQuery.setParameter(1, usuario.getId());
		final List<DestinatariosFrecuentes> misListas = misListasQuery.getResultList();

		return misListas;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDeptos() {
		final List<Division> divisiones = em.createNamedQuery("Division.findById").setParameter(ID, idDivision)
				.getResultList();
		boolean conCargo = false;

		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDivision(idDivision);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDeptos = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidades.clear();
		listUnidades = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidades() {
		final List<Departamento> deptos = em.createNamedQuery("Departamento.findById").setParameter(ID, idDepto)
				.getResultList();
		boolean conCargo = false;

		if (deptos != null && deptos.size() == 1) {
			final Departamento depto = deptos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDepartamento(idDivision, idDepto);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidades = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, idDepto);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, idUnidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, idCargo);
		if (listPersonas.size() == 1) {
			idPersona = (Long) listPersonas.get(0).getValue();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void agregarPersona() {
		Boolean estado = Boolean.TRUE;
		if (nombreLista != null && nombreLista.trim().isEmpty()) {
			String detalle = String.format("Debe ingresar \"Nombre lista\"");
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		int largo = nombreLista.trim().length();
		if (nombreLista != null && !nombreLista.trim().isEmpty() && largo >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres, para el \"Nombre lista\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		if (idPersona.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Debe seleccionar la Persona.");
			estado = Boolean.FALSE;
		}		
		if (estado) {
			try {
				if (!idCargo.equals(JerarquiasLocal.INICIO)) {
					if (idPersona.equals(JerarquiasLocal.TODOS)) {
						listDestinatarios.addAll(em.createNamedQuery("Persona.findById").setParameter(ID, idCargo)
								.getResultList());
					} else if (idPersona.equals(JerarquiasLocal.CUALQUIERA)) {
						final List<Persona> personasTmp = em
								.createQuery("select p from Persona p where p.cargo.id = ?").setParameter(1, idCargo)
								.getResultList();
						for (final Persona p : personasTmp) {
							p.setDestinatarioConRecepcion(true);
							listDestinatarios.add(p);
						}
					} else {
						if(listDestinatarios != null){
							for(Persona per : listDestinatarios){
								if(per.getId().equals(idPersona)){
									FacesMessages.instance().add("Debe seleccionar una persona distinta a las agregadas");
									return;
								}
							}
						}
						final Persona p = (Persona) em.createQuery("select p from Persona p where p.id = :param")
								.setParameter("param", idPersona).getSingleResult();
						p.setDestinatarioConCopia(true);
						listDestinatarios.add(p);
					}
				}
				if (listDestinatarios.contains(usuario)) {
					FacesMessages.instance().add("Debe seleccionar un usuario distinto al que se encuentra en sesión.");
					listDestinatarios.remove(usuario);
				}
				this.cargaListas();
			} catch (Exception e) {
				log.error("Cargando datos...", e);
			}
		}
	}

	@Override
	public void limpiarFormulario() {
		nombreLista = "";
		flag = 0;
		idUltimaListaCargada = null;
		listDestinatarios.clear();
		this.cargaListas();
	}

	@Override
	@End
	public String end() {
		listDestinatarios = null;
		listListas = null;
		log.info("Finalizando modulo...");
		return "home";
	}

	@Override
	public void eliminarPersona(final Persona persona) {
		listDestinatarios.remove(persona);
	}

	@Override
	public void crearLista() {
		Boolean estado = Boolean.TRUE;
		
		if (nombreLista != null && nombreLista.trim().isEmpty()) {
			String detalle = String.format("Debe ingresar \"Nombre lista\"");
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		
		if(listListas != null){
			for(DestinatariosFrecuentes dest : listListas){
				if(dest.getNombreLista().trim().toUpperCase().equals(nombreLista.trim().toUpperCase())){
					FacesMessages.instance().add("No se puede repetir el nombre de una lista");
					estado = Boolean.FALSE;
					return;
				}
			}
		}
		
		int largo = nombreLista.trim().length();
		if (nombreLista != null && !nombreLista.trim().isEmpty() && largo >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres, para el \"Nombre lista\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		
		if(estado) {
			final List<Persona> destinatariosAPersistir = new ArrayList<Persona>();
			final DestinatariosFrecuentes lista = new DestinatariosFrecuentes();
			final Persona propietario = em.find(Persona.class, usuario.getId());
			lista.setPropietario(propietario);
			lista.setNombreLista(nombreLista);
			lista.setEsGrupo(false);
			for (Persona p : listDestinatarios) {
				destinatariosAPersistir.add(p);
			}
			lista.setDestinatariosFrecuentes(destinatariosAPersistir);
			em.persist(lista);
	
			final List<DestinatariosFrecuentes> listaUsuario = new ArrayList<DestinatariosFrecuentes>();
			if (propietario.getDestinatariosFrecuentes() == null) {
				listaUsuario.add(lista);
				propietario.setDestinatariosFrecuentes(listaUsuario);
			} else {
				propietario.getDestinatariosFrecuentes().add(lista);
			}
	
			if (listListas != null) {
				listListas.add(lista);
			}
	
			this.limpiarFormulario();
			listDestinatarios.clear();
	
			FacesMessages.instance().add("Lista creada exitosamente");
		}
		
	}

	@Override
	public void modificarLista() {
		Boolean estado = Boolean.TRUE;
		if (nombreLista != null && nombreLista.trim().isEmpty()) {
			String detalle = String.format("Debe ingresar \"Nombre lista\"");
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		int largo = nombreLista.trim().length();
		if (nombreLista != null && !nombreLista.trim().isEmpty() && largo >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres, para el \"Nombre lista\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		} 
		if (estado) {
			if (idUltimaListaCargada != null){ 
				final List<Persona> destinatariosModificar = new ArrayList<Persona>();
				final DestinatariosFrecuentes lista = em.find(DestinatariosFrecuentes.class, idUltimaListaCargada);
				if (!lista.getNombreLista().equals(nombreLista)){
						lista.setNombreLista(nombreLista);
					}
				final int desSinModificar = lista.getDestinatariosFrecuentes().size();
				for (Persona p : listDestinatarios){
					destinatariosModificar.add(p);
				}
				if (!lista.getDestinatariosFrecuentes().equals(destinatariosModificar)) {
					lista.setDestinatariosFrecuentes(destinatariosModificar);
				}
				lista.setEsGrupo(false);
				em.merge(lista);
				this.limpiarFormulario();
				listDestinatarios.clear();
				listListas.clear();
				listListas = this.cargarListasExistentes();
				if(destinatariosModificar.size() == desSinModificar){
					FacesMessages.instance().add("Ningún cambio realizado");					
				}else{
					if(destinatariosModificar.size() == 0){
						FacesMessages.instance().add("Lista esta vacia.");
					}
					FacesMessages.instance().add("Lista modificada exitosamente");	
					this.limpiarFormulario();
				}
			}	
		}
	}

	@Override
	public void cargarLista(final Long idLista) {
		limpiarFormulario();
		final DestinatariosFrecuentes lista = em.find(DestinatariosFrecuentes.class, idLista);
		nombreLista = lista.getNombreLista();
		if (listDestinatarios != null) {
			listDestinatarios.clear();
			listDestinatarios.addAll(lista.getDestinatariosFrecuentes());
		}
		flag = 1;
		idUltimaListaCargada = idLista;
	}

	@Override
	public void eliminarLista(final DestinatariosFrecuentes lista) {
		final DestinatariosFrecuentes listaEliminarBD = em.find(DestinatariosFrecuentes.class, lista.getId());
		listListas.remove(lista);
		em.remove(listaEliminarBD);
		limpiarFormulario();
		FacesMessages.instance().add("Lista eliminada");
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public String getNombreLista() {
		return nombreLista;
	}

	@Override
	public void setNombreLista(final String nombreLista) {
		this.nombreLista = nombreLista;
	}

	@Override
	public Long getIdDivision() {
		return idDivision;
	}

	@Override
	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	@Override
	public Long getIdDepto() {
		return idDepto;
	}

	@Override
	public void setIdDepto(final Long idDepto) {
		this.idDepto = idDepto;
	}

	@Override
	public Long getIdUnidad() {
		return idUnidad;
	}

	@Override
	public void setIdUnidad(final Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	@Override
	public Long getIdCargo() {
		return idCargo;
	}

	@Override
	public void setIdCargo(final Long idCargo) {
		this.idCargo = idCargo;
	}

	@Override
	public Long getIdPersona() {
		return idPersona;
	}

	@Override
	public void setIdPersona(final Long idPersona) {
		this.idPersona = idPersona;
	}

	@Override
	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	@Override
	public void setListDivisiones(final List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	@Override
	public List<SelectItem> getListDeptos() {
		return listDeptos;
	}

	@Override
	public void setListDeptos(final List<SelectItem> listDeptos) {
		this.listDeptos = listDeptos;
	}

	@Override
	public List<SelectItem> getListUnidades() {
		return listUnidades;
	}

	@Override
	public void setListUnidades(final List<SelectItem> listUnidades) {
		this.listUnidades = listUnidades;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public void setListCargos(final List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<Persona> getListDestinatarios() {
		return listDestinatarios;
	}

	@Override
	public void setListDestinatarios(final List<Persona> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}

	@Override
	public List<DestinatariosFrecuentes> getListListas() {
		return listListas;
	}

	@Override
	public void setListListas(final List<DestinatariosFrecuentes> listListas) {
		this.listListas = listListas;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	@Override
	public void setFlag(final int flag) {
		this.flag = flag;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
}
