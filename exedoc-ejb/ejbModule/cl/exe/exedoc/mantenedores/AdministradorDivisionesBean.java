package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link AdministradorDivisiones}.
 */
@Stateful
@Name("adminDivisiones")
@Scope(value = ScopeType.SESSION)
public class AdministradorDivisionesBean implements AdministradorDivisiones {

	private static final String ID_ORGANIZACION = "idOrganizacion";

	private String nombreDivision;

	private Long organizacion;
	private Long organizacionCargar;
	private Long idDivision;
	private Long idDivisionCargar;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listOrganizacionCargar;

	@SuppressWarnings("unused")
	private List<SelectItem> listDivisionesCargar;

	private List<Division> listDivisiones;

	private Long idUltimaDivisionCargada;
	private int flag;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Constructor.
	 */
	public AdministradorDivisionesBean() {
		super();
	}

	// Bean
	@Begin(join = true)
	@Override
	public String begin() {
		log.info("Iniciando modulo division...");

		nombreDivision = "";
		listDivisiones = new ArrayList<Division>();
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		listOrganizacionCargar = this.buscarOrganizacionesCargar();
		organizacionCargar = (Long) listOrganizacionCargar.get(1).getValue();
		buscarDivisiones();

		flag = 0;
		idUltimaDivisionCargada = null;
		log.info("Termino begin modulo division...");
		return "adminDivisiones";
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDivision();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizacionesCargar() {
		this.limpiarDivisionCargar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * 
	 */
	public void buscarDivisionesCargar() {
		final long idOrganizacion = organizacionCargar;
		organizacionCargar = idOrganizacion;
		listDivisionesCargar = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacionCargar);
	}

	/**
	 * 
	 */
	private void limpiarDivision() {
		organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;

		listOrganizacion = new ArrayList<SelectItem>();
		listOrganizacion.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void limpiarDivisionCargar() {
		organizacionCargar = JerarquiasLocal.INICIO;
		idDivisionCargar = JerarquiasLocal.INICIO;

		listOrganizacionCargar = new ArrayList<SelectItem>();
		listOrganizacionCargar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));

		listDivisiones.clear();
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@End
	@Override
	public String end() {
		nombreDivision = "";
		listOrganizacion = null;
		listOrganizacionCargar = null;
		listDivisiones = null;
		log.info("Finalizando modulo...");
		return "home";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDivisiones() {
		if (organizacionCargar.equals(JerarquiasLocal.INICIO)) {
			listDivisiones = em.createNamedQuery("Division.findByOrganizacion").setParameter(ID_ORGANIZACION, organizacionCargar).getResultList();
		} else {
			listDivisiones = em.createNamedQuery("Division.findByOrganizacion").setParameter(ID_ORGANIZACION, organizacionCargar).getResultList();
		}
	}

	@Override
	public void cargarDivision(final Long paramIdDivision) {
		final Division deo = em.find(Division.class, paramIdDivision);
		if (deo != null) {
			nombreDivision = deo.getDescripcion();
			organizacion = deo.getOrganizacion().getId();
			this.buscarDivisiones();
			final Long sIdDivision = deo.getId();
			idUltimaDivisionCargada = sIdDivision;
			flag = 1;
		} else {
			FacesMessages.instance().add("Error al cargar la division");
		}
	}

	@Override
	public void crearDivision() {
		nombreDivision = nombreDivision.trim();
		if (!nombreDivision.isEmpty()) {
			if (!organizacion.equals(JerarquiasLocal.INICIO)) {
				final Organizacion org = em.find(Organizacion.class, organizacion);
				final Division div = new Division();
				div.setConCargo(false);
				div.setDescripcion(nombreDivision);
				div.setOrganizacion(org);
				if (existeDivision(div)) {
					FacesMessages.instance().add("Existe una Division con ese nombre");
					return;
				}
				em.persist(div);
				FacesMessages.instance().add("Division creada exitosamente");
				organizacionCargar = organizacion;
				
			} else {
				FacesMessages.instance().add("Debe ingresar mínimo una organizacion antes de crear la División");
			}
		} else {
			FacesMessages.instance().add("Debe ingresar un nombre para la División antes de crearla");
		}

		this.limpiarFormulario();
		//listDivisiones.clear();
	}

	@Override
	public void eliminarDivision(final Division division) {
		final Division uo = em.find(Division.class, division.getId());
		if (uo.getDepartamentos() == null || uo.getDepartamentos().size() == 0) {
			listDivisiones.remove(division);
			em.remove(uo);
			this.limpiarFormulario();

			FacesMessages.instance().add("Division eliminada exitosamente ");
		} else {
			FacesMessages.instance().add("Imposible borrar Divisiones con departamentos asociados");
		}
	}

	@Override
	public void limpiarFormulario() {
		nombreDivision = "";
		listOrganizacion.clear();
		listOrganizacionCargar.clear();
		listDivisiones.clear();
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		listOrganizacionCargar = this.buscarOrganizacionesCargar();
		organizacionCargar = (Long) listOrganizacionCargar.get(1).getValue();
		buscarDivisiones();
		flag = 0;

	}

	@Override
	public void modificarDivision() {
		if (idUltimaDivisionCargada != null) {
			final Division uo = em.find(Division.class, idUltimaDivisionCargada);
			nombreDivision = nombreDivision.trim();
			if (!nombreDivision.isEmpty()) {
				if (!uo.getDescripcion().equals(nombreDivision)) {
					uo.setDescripcion(nombreDivision);
				}
				if (!organizacion.equals(JerarquiasLocal.INICIO)) {
					final Organizacion org = em.find(Organizacion.class, organizacion);
					uo.setOrganizacion(org);
					if (existeDivision(uo)) {
						FacesMessages.instance().add("Existe una Division con ese nombre");
						return;
					}
					em.merge(uo);

					this.limpiarFormulario();
					//listDivisiones.clear();
					idUltimaDivisionCargada = null;
					flag = 0;

					FacesMessages.instance().add("División modificada exitosamente");
				} else {
					FacesMessages.instance().add("La unidad debe estar mínimo dentro de una división");
				}
			} else {
				FacesMessages.instance().add("El nombre de la unidad no puede quedar en blanco");
			}
		}
	}

	@Override
	public String getNombreDivision() {
		return nombreDivision;
	}

	@Override
	public void setNombreDivision(final String nombreDivision) {
		this.nombreDivision = nombreDivision;
	}

	@Override
	public Long getIdDivision() {
		return idDivision;
	}

	@Override
	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	@Override
	public List<Division> getListDivisiones() {
		return listDivisiones;
	}

	@Override
	public void setListDivisiones(final List<Division> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	@Override
	public Long getIdDivisionCargar() {
		return idDivisionCargar;
	}

	@Override
	public void setIdDivisionCargar(final Long idDivisionCargar) {
		this.idDivisionCargar = idDivisionCargar;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	@Override
	public void setFlag(final int flag) {
		this.flag = flag;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getOrganizacionCargar() {
		return organizacionCargar;
	}

	@Override
	public void setOrganizacionCargar(final Long organizacionCargar) {
		this.organizacionCargar = organizacionCargar;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacionCargar() {
		return listOrganizacionCargar;
	}

	@Override
	public void setListOrganizacioCargarn(final List<SelectItem> parmListOrganizacionCargar) {
		this.listOrganizacionCargar = parmListOrganizacionCargar;
	}
	
	private boolean existeDivision(Division division) {
		boolean retorno = false;
		List<Division> lista;
		if (division.getId() == null) {
			lista = (List<Division>)em.createNamedQuery("Division.findByDescripcionOrganizacion")
					.setParameter(ID_ORGANIZACION, division.getOrganizacion().getId())
					.setParameter("descripcion", division.getDescripcion())
					.getResultList();
		} else {
			lista = (List<Division>)em.createNamedQuery("Division.findByDescripcionOrganizacionExcludeId")
					.setParameter(ID_ORGANIZACION, division.getOrganizacion().getId())
					.setParameter("descripcion", division.getDescripcion())
					.setParameter("id", division.getId())
					.getResultList();
		}
		if (lista != null && lista.size() > 0) {
			retorno = true;
		}
		return retorno;
	}
}
