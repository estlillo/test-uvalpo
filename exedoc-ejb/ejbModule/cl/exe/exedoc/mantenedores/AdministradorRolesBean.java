package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * clase Administrador de roles.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(AdministradorRolesBean.NAME)
@Scope(value = ScopeType.SESSION)
public class AdministradorRolesBean implements AdministradorRoles {

	/**
	 * NAME = "administradorRoles".
	 */
	public static final String NAME = "administradorRoles";
	
	private static final String ID = "id";

	private Persona persona;
	private List<Persona> personas;
	private List<Persona> personaSeleccionada;

	private List<Rol> roles;

	private Map<Integer, Boolean> selectRoles;

	@Out(required = false, scope = ScopeType.SESSION)
	@In(required = false, scope = ScopeType.SESSION)
	private String user;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false)
	private Persona usuario;
	
	// Listas desplegables
	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidadesOrganizacionales;
	private List<SelectItem> listCargos;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;

	/**
	 * Constructor.
	 */
	protected AdministradorRolesBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		listOrganizacion = new ArrayList<SelectItem>();
		listDivision = new ArrayList<SelectItem>();
		listDepartamento = new ArrayList<SelectItem>();
		listUnidadesOrganizacionales = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		personas = new ArrayList<Persona>();
		personaSeleccionada = new ArrayList<Persona>(1);
		roles = new ArrayList<Rol>();
		selectRoles = new HashMap<Integer, Boolean>();
		this.user = "";
		
		listOrganizacion = this.buscarOrganizaciones();
		
//		this.cargarListas();
//		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidadesOrganizacionales();
		this.buscarRoles();
		return AdministradorRolesBean.NAME;
	}

	/**
	 * Metodo que carga listas con datos iniciales.
	 */
	private void cargarListas() {
		listOrganizacion = this.buscarOrganizaciones();
		this.buscarRoles();
	}

	@End
	@Override
	public String end() {
		return "home";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUsuario() {
		final StringBuilder sql = new StringBuilder("SELECT p FROM Persona p WHERE ");
		String nombre = persona.getNombres();
		String apellido = persona.getApellidoPaterno();
		
		if(user.trim().length() == 0 && nombre.trim().length() == 0 && apellido.trim().length() == 0){
			FacesMessages.instance().add("Debe ingresar al menos un párametro para la busqueda");
			return;
		}
		if (user != null && user.trim().length() != 0) {
			sql.append("upper(p.usuario) like upper('%").append(user).append("%') and ");
		}
		if (nombre != null && nombre.trim().length() != 0) {
			sql.append("upper(p.nombres) like upper('%").append(nombre).append("%') and ");
		}
		if (apellido != null && apellido.trim().length() != 0) {
			sql.append("upper(p.apellidoPaterno) like upper('%").append(apellido).append("%') and ");
		}
		sql.append(" 1 = 1 ");

		final Query query = em.createQuery(sql.toString());
		List<Persona> per = query.getResultList();
		if(per.isEmpty()){
			FacesMessages.instance().add("No se encontraron usuarios para la busqueda.");
		}
		personas = query.getResultList();
		user = "";
		nombre = "";
		apellido = "";
	}

	@Override
	public void asociarRolPersona() {
		if (personaSeleccionada.size() == 0) {
			FacesMessages.instance().add("Debe Seleccionar una persona primero");
			return;
		}
		final Set<Integer> idRoles = selectRoles.keySet();
		final List<Rol> rolesPersona = new ArrayList<Rol>();
		for (Integer id : idRoles) {
			if ((Boolean) selectRoles.get(id)) {
				final Rol rol = em.find(Rol.class, id);
				rolesPersona.add(rol);
			}
		}
		persona.setRoles(rolesPersona);
		em.merge(persona);
		FacesMessages.instance().add("Se modificó el usuario en el sistema");
		this.personas = null;
		this.persona = null;
		selectRoles.clear();
		personaSeleccionada.clear();
		this.cargarListas();
		limpiarDestinatario();

	}

	/*
	 * public void guardarPersona() { if (!cargo.equals(JerarquiasLocal.INICIO)) { if (persona.getId() == null) { Cargo
	 * cargoPersona = em.find(Cargo.class, cargo); persona.setCargo(cargoPersona); if (persona.getVigente()) {
	 * persona.setFechaNoVigente(null); } persona.setKeyStoreAlias("lastudillo");
	 * persona.setKeyStorePassword("lastudillo"); Query query =
	 * em.createQuery("select p.keyStore from Persona p where p.id = 1"); byte[] key_store =
	 * (byte[])query.getSingleResult(); persona.setKeyStore(key_store); if(validarRutNoRegistrado(persona.getRut(),
	 * false, null)){ if(validarIdPersonaRegistrada(persona.getIdPersona(), false, null)){
	 * if(validarUsuarioRegistrado(persona.getUsuario(), false, null)){ em.persist(persona);
	 * FacesMessages.instance().add("Se agregó el usuario al sistema"); persona = null; limpiarDestinatario(); } } } }
	 * else { String consulta = "SELECT p FROM Persona p WHERE p.id =" + persona.getId(); Query query =
	 * em.createQuery(consulta); Long idPersona= new Long(0); Object o; try{ o = query.getSingleResult(); } catch
	 * (Exception e){ o= null; } if(o!= null){ Persona p= (Persona)o; idPersona= p.getIdPersona(); } if
	 * (persona.getVigente()) { persona.setFechaNoVigente(null); } else if (persona.getFechaNoVigente() == null) {
	 * persona.setFechaNoVigente(new Date()); } Cargo cargoPersona = em.find(Cargo.class, cargo);
	 * persona.setCargo(cargoPersona); if(validarRutNoRegistrado(persona.getRut(), true, idPersona)){
	 * if(validarIdPersonaRegistrada(persona.getIdPersona(), true, idPersona)){
	 * if(validarUsuarioRegistrado(persona.getUsuario(), true, idPersona)){ em.merge(persona);
	 * FacesMessages.instance().add("Se modificó el usuario en el sistema"); persona = null; limpiarDestinatario(); } }
	 * } } }else{ FacesMessages.instance().add("Debe ingresar el Cargo"); } }
	 */
	/*
	 * private boolean validarUsuarioRegistrado(String usuario, boolean editar, Long idPersona) {
	 * if(persona.getUsuario()!= null && !persona.getUsuario().equals("")){ String consulta= null; if(!editar){ consulta
	 * = "select p from Persona p where upper(p.usuario)= upper('" + persona.getUsuario() + "')"; }else{ consulta =
	 * "select p from Persona p where upper(p.usuario)= upper('" + persona.getUsuario() + "') and p.idPersona not in
	 * (" + idPersona + ")"; } Query query = em.createQuery(consulta); Object o; try{ o = query.getSingleResult(); }
	 * catch (Exception e){ o= null; } if(o!= null){ Persona p= (Persona)o; if(p!= null){
	 * FacesMessages.instance().add("El Usuario ingresado ya se encuentra registrado: #0", p.getUsuario()); return
	 * false; } } return true; }else{ return false; } }
	 */
	/*
	 * private boolean validarRutNoRegistrado(String rut, boolean editar, Long idPersona) { if(rut!= null &&
	 * !rut.equals("")){ String consulta= null; if(!editar){ consulta = "select p from Persona p where upper(p.rut)=
	 * upper('" + rut + "')"; }else{ consulta = "select p from Persona p where upper(p.rut)= upper('" + rut +
	 * "') and p.idPersona not in (" + idPersona + ")"; } Query query = em.createQuery(consulta); Object o; try{ o =
	 * query.getSingleResult(); } catch (Exception e){ o= null; } if(o!= null){ Persona p= (Persona)o; if(p!= null){
	 * FacesMessages.instance().add("El Rut ingresado ya se encuentra registrado: #0", p.getRut()); return false; } }
	 * return true; }else{ return false; } }
	 */
	/*
	 * private boolean validarIdPersonaRegistrada(Long idPersona, boolean editar, Long idPersonaExistente){
	 * if(persona.getIdPersona()!= null){ String consulta= null; if(!editar){ consulta = "select p from Persona p where
	 * p.idPersona= '" + idPersona + "'"; }else{ consulta = "select p from Persona p where p.idPersona= '" + idPersona +
	 * "' and p.idPersona not in (" + idPersonaExistente + ")"; } Query query = em.createQuery(consulta); Object o; try{
	 * o = query.getSingleResult(); } catch (Exception e){ o= null; } if(o!= null){ Persona p= (Persona)o; if(p!= null){
	 * FacesMessages.instance().add("El Id ingresado ya se encuentra registrado: #0", p.getIdPersona()); return false; }
	 * } return true; }else{ return false; } }
	 */

	/**
	 * Metodo que carga los roles del sistema.
	 */
	@SuppressWarnings("unchecked")
	private void buscarRoles() {
		roles = em.createNamedQuery("Rol.findByAll").getResultList();
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;

		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		this.buscarDepartamentos();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	/**
	 * metodo que busca Unidades Organizacionales Departamento.
	 */
	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento dpto = departamentos.get(0);
			if (dpto.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				departamento);
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, unidadOrganizacional);
	}

	@Override
	public void limpiarFormulario() {
		this.cargarListas();
		// limpiarDestinatario();
		this.personas = null;
		this.persona = null;
		personaSeleccionada.clear();
	}

	/**
	 * Metodo que limpia los destinatarios.
	 */
	private void limpiarDestinatario() {
		// this.roles = new ArrayList<Rol>();
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		/*
		 * this.personas = null; this.persona = null;
		 */
		this.personaSeleccionada.clear();
		this.selectRoles.clear();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarPersonas() {
		if (!cargo.equals(JerarquiasLocal.INICIO)) {
			final Query query = em.createQuery("Select p From Persona p Where p.cargo.id = ?");
			query.setParameter(1, cargo);
			personas = query.getResultList();
		}
	}

	@Override
	public void modificar(final Long idPersona) {
		/*
		 * Se realiza la búsqueda de la Persona por medio del usuario
		 */
		persona = em.find(Persona.class, idPersona);
		/*
		 * Se obtiene jerarquía a la cual pertenece aquella persona.
		 */
		final Cargo c = persona.getCargo();
		final UnidadOrganizacional uni = c.getUnidadOrganizacional();
		final Departamento dep = uni.getDepartamento();
		final Division div = dep.getDivision();
		final Organizacion org = div.getOrganizacion();

		/**
		 * Se setea la organizacion correspondiente y se carga la lista de Divisiones.
		 */
		organizacion = org.getId();
		this.buscarDivisiones();

		/*
		 * Se setea la división correspondiente y se carga la lista de Deptos.
		 */
		division = div.getId();
		this.buscarDepartamentos();

		/*
		 * Se setea el Depto correspondiente y se carga la lista de Unidades.
		 */
		departamento = dep.getId();
		this.buscarUnidadesOrganizacionales();

		/*
		 * Se setea la Unidad correspondiente y se carga la lista de Cargos.
		 */
		unidadOrganizacional = uni.getId();
		this.buscarCargos();

		cargo = c.getId();

		personaSeleccionada.clear();
		personaSeleccionada.add(persona);
		selectRoles.clear();
		for (Rol rol : persona.getRoles()) {
			selectRoles.put(rol.getId(), true);
		}

	}

	@Override
	public Persona getPersona() {
		if (persona == null) {
			persona = new Persona();
		}
		return persona;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public List<Persona> getPersonas() {
		return personas;
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@Override
	public String getUser() {
		return user;
	}

	@Override
	public void setUser(final String user) {
		this.user = user;
	}

	@Override
	public List<Persona> getPersonaSeleccionada() {
		return personaSeleccionada;
	}

	@Override
	public List<Rol> getRoles() {
		return roles;
	}

	@Override
	public Map<Integer, Boolean> getSelectRoles() {
		return selectRoles;
	}

	@Override
	public void setSelectRoles(final Map<Integer, Boolean> selectRoles) {
		this.selectRoles = selectRoles;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
}
