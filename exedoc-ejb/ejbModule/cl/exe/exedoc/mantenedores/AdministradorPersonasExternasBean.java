package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.DependenciaExterna;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.PersonaExterna;
import cl.exe.exedoc.entity.TipoFirma;
import cl.exe.exedoc.session.exception.RUTException;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.RutUtil;

/**
 * Implementacion de {@link AdministradorPersonasExternas}.
 */
@Stateful
@Name("administradorPersonasExternas")
public class AdministradorPersonasExternasBean implements AdministradorPersonasExternas {

	private static final String RUT_STR = "rut";
	private static final String ID_STR = "id";
	private Long dependencia;
	private List<SelectItem> dependencias;

	private PersonaExterna personaExterna;
	private List<PersonaExterna> personasExternas;

	private boolean editando;
	@PersistenceContext
	private EntityManager em;

	/**
	 * Constructor.
	 */
	public AdministradorPersonasExternasBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {
		this.buscar();
		this.setEditando(Boolean.FALSE);
		dependencia = JerarquiasLocal.INICIO;
		personaExterna = new PersonaExterna();
		dependencias = null;
		return "administradorPersonasExternas";
	}

	/**
	 * Se crea una nueva persona externa, se valida que el rut no exista antes de ser ingresado, si existe se envía una
	 * respuesta indicando aquello. Además la PersonaExterna no debe existir (como rut) en la entidad Persona, por lo
	 * cual también se valida esto.
	 * TODO: EXECUTABLE STATEMENT
	 */
	@Override
	public void crearNuevo() {
		
		if (dependencia.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Debe ingresar la Dependencia Externa");
			return;
		}
		
		final boolean existe = personaExterna.getId() != null;
		String rutAnterior = null;
		// Persona.getId() no es null
		final String consulta = "SELECT p FROM Persona p WHERE p.id ="
				+ personaExterna.getId();
		final Query query = em.createQuery(consulta);
		Object o;
		try {
			o = query.getSingleResult();
		} catch (Exception e) {
			o = null;
		}
		if (o != null) {
			final Persona p = (Persona) o;
			rutAnterior = p.getRut();
		}
		
		if (!this.validarExisteRut(existe, rutAnterior)) {
			if (personaExterna != null && personaExterna.getNombres().length() != 0) {
				if (personaExterna.getNombres().length() < 250) {
					final DependenciaExterna de = em.find(DependenciaExterna.class, dependencia);
					personaExterna.setDependencia(de);
					if (!existeNombreyDependencia()) {
						if (personaExterna.getId() == null) {
							final Persona persona = new Persona();
							persona.setRut(null);
							persona.setNombres(personaExterna.getNombres());
							persona.setApellidoPaterno(null);
							persona.setEmail(null);
							persona.setTipoFirma(new TipoFirma(TipoFirma.AVANZADA));
							persona.setExterno(true);
							personaExterna.setPersona(persona);
							em.persist(persona);
							if (personaExterna.getRut().isEmpty()) personaExterna.setRut(null);
							if (personaExterna.getEmail().isEmpty()) personaExterna.setEmail(null);
							em.persist(personaExterna);
							FacesMessages.instance().add("La persona se guardo con exito.");
						} else {
							if (personaExterna.getPersona() == null) {
								final Persona persona = new Persona();
								persona.setRut(null);
								persona.setNombres(personaExterna.getNombres());
								persona.setApellidoPaterno(null);
								persona.setEmail(null);
								persona.setTipoFirma(new TipoFirma(TipoFirma.AVANZADA));
								persona.setExterno(true);
								personaExterna.setPersona(persona);
								em.persist(persona);
							} else {
								final Persona persona = personaExterna.getPersona();
								persona.setRut(null);
								persona.setNombres(personaExterna.getNombres());
								persona.setEmail(null);
								//em.merge(persona);
							}
							if (personaExterna.getRut().isEmpty()) personaExterna.setRut(null);
							if (personaExterna.getEmail().isEmpty()) personaExterna.setEmail(null);
							em.merge(personaExterna);
							FacesMessages.instance().add("La persona se edito con exito.");
						}
						this.buscar();
						this.personaExterna = new PersonaExterna();
					} else { 
						FacesMessages.instance().add("Nombre y Dependencia ya existen.");
					}
				} else { 
					FacesMessages.instance().add("Nombre demasiado largo(max 250 caracteres).");
				}
			}
		}
	}
	
	@Override
	public void editar() {
		
		if (dependencia.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Debe ingresar la Dependencia Externa");
			return;
		}
		
		final boolean existe = personaExterna.getId() != null;
		Long idAnterior = null;
		// Persona.getId() no es null
		final String consulta = "SELECT p FROM PersonaExterna p WHERE p.id ="
				+ personaExterna.getId();
		final Query query = em.createQuery(consulta);
		Object o;
		try {
			o = query.getSingleResult();
		} catch (Exception e) {
			o = null;
		}
		if (o != null) {
			final PersonaExterna p = (PersonaExterna) o;
			idAnterior = p.getId();
		}
		
		
		
		Boolean estado = Boolean.TRUE;
		if (this.findPersonaByRut(this.personaExterna.getRut()) > 0) {
			if (this.personaExterna.getRut().equalsIgnoreCase(findPersonaByRutRut(this.personaExterna.getRut()))) {
				FacesMessages.instance().add("RUT ya esta registrado, con un usuario interno.");
				estado = Boolean.FALSE;
			}
		}
		if (this.findPersonaExternaByRutAndId(this.personaExterna.getRut(), this.personaExterna.getId()) > 0) {
			FacesMessages.instance().add("RUT ya esta registrado, con una persona externa.");
			estado = Boolean.FALSE;
		}
		if (existeNombreyDependencia(idAnterior)) {
			FacesMessages.instance().add("Nombre y Dependencia ya existen.");
			estado = Boolean.FALSE;
		}
		
		if (estado) {
			if (this.personaExterna.getPersona() != null) {
				this.personaExterna.getPersona().setNombres(this.personaExterna.getNombres());
				this.personaExterna.getPersona().setRut(null);
				this.personaExterna.getPersona().setEmail(null);
				em.merge(personaExterna.getPersona());
			}
			if (personaExterna.getRut().isEmpty()) personaExterna.setRut(null);
			if (personaExterna.getEmail().isEmpty()) personaExterna.setEmail(null);
			em.merge(personaExterna);
			FacesMessages.instance().add(
					"Persona Externa editada correctamente.");
		}
		this.buscar();
	}
	
	@Override
	public void limpiar() {
		this.buscar();
		this.setEditando(Boolean.FALSE);
		dependencia = JerarquiasLocal.INICIO;
		personaExterna = new PersonaExterna();
	}

	/**
	 * Metodo que valida existencia de un rut en el sistema.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarExisteRut(Boolean existe, String rutAnterior) {
		if (personaExterna.getRut() != null && !personaExterna.getRut().isEmpty()) {
			try {
				RutUtil rutFormateado = new RutUtil((String) personaExterna.getRut());
				personaExterna.setRut(rutFormateado.toString());
			} catch (RUTException e1) {
				FacesMessages.instance().add("RUT inv&aacute;lido.");
				return true;
			}
			if (!existe || (existe && personaExterna.getRut().equals(rutAnterior))) {
				if (!this.findPersonaExternaByRut(personaExterna.getRut()).equals(new Long(0)) || !this.findPersonaByRut(personaExterna.getRut()).equals(new Long(0))) {
					FacesMessages.instance().add("El RUT a ingresar ya existe en el sistema.");
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Buscar personas externas.
	 */
	@SuppressWarnings("unchecked")
	private void buscar() {
		try{
			final Query query = em.createNamedQuery("PersonaExterna.findByAll");
			this.personasExternas = query.getResultList();
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void modificar(final Long id) {
		this.setEditando(Boolean.TRUE);
		personaExterna = em.find(PersonaExterna.class, id);
		dependencia = this.personaExterna.getDependencia().getId();
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	public Long getDependencia() {
		return dependencia;
	}

	@Override
	public void setDependencia(final Long dependencia) {
		this.dependencia = dependencia;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SelectItem> getDependencias() {
		if (dependencias == null) {
			final List<DependenciaExterna> dependenciasExternas = em.createNamedQuery("DependenciaExterna.findByAll")
					.getResultList();
			dependencias = new ArrayList<SelectItem>();
			dependencias.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
			if (dependenciasExternas != null) {
				for (DependenciaExterna de : dependenciasExternas) {
					dependencias.add(new SelectItem(de.getId(), de.getDescripcion()));
				}
			}
		}
		return dependencias;
	}

	@Override
	public PersonaExterna getPersonaExterna() {
		return personaExterna;
	}

	@Override
	public List<PersonaExterna> getPersonasExternas() {
		return personasExternas;
	}

	/**
	 * Metodo que busca una persona externa, segun su rut.
	 * 
	 * @param rut {@link String}
	 * @return {@link Long}
	 */
	private Long findPersonaExternaByRut(final String rut) {
		final Query query = em.createNamedQuery("PersonaExterna.findByRut");
		query.setParameter(RUT_STR, rut);
		return (Long) query.getSingleResult();
	}
	
	private Long findPersonaExternaByRutAndId(final String rut, final Long id){
		final Query query = em.createNamedQuery("PersonaExterna.findByRutAndId");
		query.setParameter(RUT_STR, rut);
		query.setParameter(ID_STR, id);
		return (Long) query.getSingleResult();
	}

	/**
	 * Metodo que busca una persona, segun su rut.
	 * 
	 * @param rut {@link String}
	 * @return {@link Long}
	 */
	private Long findPersonaByRut(final String rut) {
		final Query query = em.createNamedQuery("Persona.findByRut");
		query.setParameter(RUT_STR, rut);
		return (Long) query.getSingleResult();
	}
	
	private String findPersonaByRutRut(final String rut) {
		final Query query = em.createNamedQuery("Persona.findByRut_rut");
		query.setParameter(RUT_STR, rut);
		return (String) query.getSingleResult();
	}
	
	/**
	 * Método para validar si existe persona externa y dependencia como clave.
	 * 
	 * @return {@link Boolean}
	 */
	private boolean existeNombreyDependencia(){
		boolean bOk = false;
		final Query query = em.createNamedQuery("PersonaExterna.findByIdDependenciaNombre");
		query.setParameter("id", personaExterna.getDependencia().getId());
		query.setParameter("nombres", personaExterna.getNombres().trim());
		if (query.getResultList().size() > 0)
			bOk = true;
		return bOk;
	}
	
	/**
	 * Método para validar si existe persona externa y dependencia como clave.
	 * 
	 * @return {@link Boolean}
	 */
	private boolean existeNombreyDependencia(Long idAnterior){
		boolean bOk = false;
		final Query query = em.createNamedQuery("PersonaExterna.findByIdDependenciaNombre");
		query.setParameter("id", personaExterna.getDependencia().getId());
		query.setParameter("nombres", personaExterna.getNombres().trim());
		if (query.getResultList().size() > 0) {
			List<PersonaExterna> listPE = query.getResultList();
			if (!listPE.get(0).getId().equals(idAnterior))
				bOk = true;
		}
		return bOk;
	}

	public boolean isEditando() {
		return editando;
	}

	public void setEditando(final boolean editando) {
		this.editando = editando;
	}
	
	

}
