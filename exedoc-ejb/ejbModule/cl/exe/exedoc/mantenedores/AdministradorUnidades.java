package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.UnidadOrganizacional;

/**
 * Iterface.
 * 
 * @author 
 *
 */
@Local
public interface AdministradorUnidades {

	/**
	 * Metodo que inicia el bean.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 *  Metodo que detrulle el bean.
	 */
	void destroy();

	/**
	 *  Metodo que termina el bean.
	 * @return {@link String}
	 */
	String end();

	/**
	 * Metodo para buscar Division.
	 */
	void buscarDivisiones();

	/**
	 * Metodo para buscar division cargadas.
	 */
	void buscarDivisionesCargar();

	/**
	 * Metodo para buscar departamento.
	 */
	void buscarDeptos();

	/**
	 * Metodo para buscar departamento cargados.
	 */
	void buscarDeptosCargar();

	/**
	 * Metodo para buscar {@link UnidadOrganizacional}.
	 */
	void buscarUnidades();

	/**
	 * Metodo que crea una unidad.
	 */
	void crearUnidad();

	/**
	 * Modificar una unidad.
	 */
	void modificarUnidad();

	/**
	 * Metodo para guardar y editar una unidad.
	 * 
	 * @param idUnidad {@link Long}
	 */
	void cargarUnidad(final Long idUnidad);

	/**
	 * Metodo para eliminar {@link UnidadOrganizacional}.
	 * 
	 * @param unidad {@link UnidadOrganizacional}
	 */
	void eliminarUnidad(final UnidadOrganizacional unidad);

	/**
	 * Limpiar Formulario.
	 */
	void limpiarFormulario();

	/**
	 * @return {@link String}
	 */
	String getNombreUnidad();

	/**
	 * @param nombreUnidad {@link String}
	 */
	void setNombreUnidad(final String nombreUnidad);

	/**
	 * @return {@link Long}
	 */
	Long getIdDivision();

	/**
	 * @param idDivision {@link Long}
	 */
	void setIdDivision(final Long idDivision);

	/**
	 * @return {@link Long}
	 */
	Long getIdDepto();

	/**
	 * @param idDepto {@link Long}
	 */
	void setIdDepto(final Long idDepto);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivisiones();

	/**
	 * @param listDivisiones {@link List} of {@link SelectItem}
	 */
	void setListDivisiones(final List<SelectItem> listDivisiones);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDeptos();

	/**
	 * @param listDeptos {@link List} of {@link SelectItem}
	 */
	void setListDeptos(final List<SelectItem> listDeptos);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<UnidadOrganizacional> getListUnidades();

	/**
	 * @param listUnidades {@link List} of {@link SelectItem}
	 */
	void setListUnidades(final List<UnidadOrganizacional> listUnidades);

	/**
	 * @return {@link Long}
	 */
	Long getIdDivisionCargar();

	/**
	 * @param idDivisionCargar {@link Long}
	 */
	void setIdDivisionCargar(final Long idDivisionCargar);

	/**
	 * @return {@link Long}
	 */
	Long getIdDeptoCargar();

	/**
	 * @param idDeptoCargar {@link Long}
	 */
	void setIdDeptoCargar(final Long idDeptoCargar);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivisionesCargar();

	/**
	 * @param listDivisionesCargar {@link List} of {@link SelectItem}
	 */
	void setListDivisionesCargar(final List<SelectItem> listDivisionesCargar);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDeptosCargar();

	/**
	 * @param listDeptosCargar {@link List} of {@link SelectItem}
	 */
	void setListDeptosCargar(final List<SelectItem> listDeptosCargar);

	/**
	 * @return {@link Integer}
	 */
	Integer getFlag();

	/**
	 * @param flag {@link Integer}
	 */
	void setFlag(final Integer flag);

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion {@link Long}
	 */
	void setOrganizacion(final Long organizacion);

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacionCargar();

	/**
	 * @param organizacionCargar {@link Long}
	 */
	void setOrganizacionCargar(final Long organizacionCargar);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion {@link List} of {@link SelectItem}
	 */
	void setListOrganizacion(final List<SelectItem> listOrganizacion);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacionCargar();

	/**
	 * @param listOrganizacionCargar {@link List} of {@link SelectItem}
	 */
	void setListOrganizacioCargarn(final List<SelectItem> listOrganizacionCargar);
}
