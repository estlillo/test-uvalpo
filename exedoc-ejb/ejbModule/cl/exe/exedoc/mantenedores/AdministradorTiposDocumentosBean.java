package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.util.TipoDocumentosList;

@Stateful
@Name("administradorTiposDocumentos")
public class AdministradorTiposDocumentosBean implements AdministradorTiposDocumentos {

	private TipoDocumento tipoDocumento = new TipoDocumento();
	private List<TipoDocumento> tiposDocumentos;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	@Begin(join = true)
	public String begin() {
		this.buscarTiposDocumentos();
		tipoDocumento = new TipoDocumento();
		return "administradorTiposDocumentos";
	}

	public void crearNuevoTipoDocumento() {
	   if(tipoDocumento.getDescripcion().length()>101){
		   FacesMessages.instance().add("No puede ingresar mas de 100 caracteres");
		   return;
	   }
		if (tipoDocumento != null && tipoDocumento.getDescripcion().trim().length() != 0) {
			tipoDocumento.setElectronico(false);
			//tipoDocumento.setVisible(true);
			tipoDocumento.setDescripcion(tipoDocumento.getDescripcion().trim());
			if (tipoDocumento.getId() == null) {
				if(!this.existeTipoDocumento(tipoDocumento.getDescripcion().trim())){
					em.persist(tipoDocumento);
					FacesMessages.instance().add("El Tipo fue creado exitosamente.");
				}else{
					FacesMessages.instance().add("El Tipo ya existe.");
				}
			} else {
				if(!this.existeTipoDocumentoId(tipoDocumento.getDescripcion(), tipoDocumento.getId())){
					em.merge(tipoDocumento);
					FacesMessages.instance().add("El Tipo fue modificado exitosamente.");
				}else{
					FacesMessages.instance().add("El nombre ya existe para otro registro.");
				}
			}
			this.buscarTiposDocumentos();
			this.tipoDocumento = new TipoDocumento();
			TipoDocumentosList.getInstanceTipoDocumentoList(em).restartTipoDocumentoNoElectronico(em);
		}		
	}

	@SuppressWarnings("unchecked")
	private void buscarTiposDocumentos() {
		Query query = em.createQuery("SELECT td FROM TipoDocumento td WHERE td.electronico = false");
		//query.setParameter(1, false);
		this.tiposDocumentos = query.getResultList();
	}

	public void modificarTipoDocumento(Integer idTipoDocumento) {
		log.info("buscando!!" + idTipoDocumento);
		tipoDocumento = em.find(TipoDocumento.class, idTipoDocumento);
		log.info("find: " + tipoDocumento.getDescripcion());

	}
	
	/**
	 * Método para validar si ya existe un tipo de documento creado en la base de datos al momento de ingresar uno nuevo
	 * 
	 * Retorna true en caso de encontrar algún registro
	 * 
	 * @param descripcion
	 * @return
	 */
	public boolean existeTipoDocumento(String descripcion){
		boolean estado = false;
		Query query = em.createQuery("select td from TipoDocumento td where td.electronico = false and td.descripcion= :descripcion");
		query.setParameter("descripcion",descripcion);
		List <TipoDocumento> doc = query.getResultList();
		
		if(!doc.isEmpty()){
			estado = true;
		}
		return estado;
	}
	
	/**
	 * Metodo para validar si ya existe un tipo de documento en la base de datos con el mismo nombre al momento de editar
	 * un tipo de documento 
	 * 
	 * retorna true en caso de encontrar un tipo documento con descripcion igual a la que se esta modificando
	 * 
	 * @param descripcion
	 * @param idTipoDocumento
	 * @return
	 */
	public boolean existeTipoDocumentoId(String descripcion, Integer idTipoDocumento){
		boolean estado = false;
		Query query = em.createQuery("select td from TipoDocumento td where td.electronico = false and td.descripcion = :descripcion and td.id != :idTipoDocumento");
		query.setParameter("descripcion",descripcion);
		query.setParameter("idTipoDocumento",idTipoDocumento);
		List <TipoDocumento> doc = query.getResultList();
		
		if(!doc.isEmpty()){
			estado = true;
		}
		return estado;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	@End
	public String end() {
		return "home";
	}

	public List<TipoDocumento> getTiposDocumentos() {
		return tiposDocumentos;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
}
