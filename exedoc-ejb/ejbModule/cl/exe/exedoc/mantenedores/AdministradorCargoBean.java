package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * @author Administrator
 * Implementacion de {@link AdministradorCargo}
 *
 */
@Stateful
@Name("administradorCargo")
@Scope(value = ScopeType.SESSION)
public class AdministradorCargoBean implements AdministradorCargo {

	private static final String ID = "id";
	private Cargo cargo;
	private List<Cargo> cargos;
	private String descripcion;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@In(required = false)
	private Persona usuario;
	
	// Listas desplegables
	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidadesOrganizacionales;

	// Datos
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;

	/**
	 * Constructor.
	 */
	public AdministradorCargoBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {

		listOrganizacion = new ArrayList<SelectItem>();
		listDivision = new ArrayList<SelectItem>();
		listDepartamento = new ArrayList<SelectItem>();
		listUnidadesOrganizacionales = new ArrayList<SelectItem>();
		
		listOrganizacion = this.buscarOrganizaciones();
		
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
		
//		this.division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
		
//		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        
		//this.setListDepartamento(jerarquias.getFiscalias());
		//this.departamento = (Long) listDepartamento.get(1).getValue();
		
//		this.buscarUnidadesOrganizacionales();
		
		return "administradorCargo";
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarCargo() {
		final StringBuilder sql = new StringBuilder("SELECT c FROM Cargo c WHERE ");
		// String descripcion = this.descripcion;

		if (descripcion != null && descripcion.length() != 0) {
			sql.append("upper(c.descripcion) like upper('%").append(descripcion).append("%') and ");
		}
		sql.append(" 1 = 1 ");

		final Query query = em.createQuery(sql.toString());
		cargos = (List<Cargo>) query.getResultList();

		descripcion = "";
	}

	@Override
	public void guardarCargo() {
		if (unidadOrganizacional.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("La Unidad Organizacional es obligatoria.");
			return;
		}

		UnidadOrganizacional unidadCargo = null;
		final boolean existe = cargo != null && cargo.getId() != null;

		if (!existe) {
			cargo = new Cargo();
		}

		
		//
		if(descripcion.trim().length() >= 99){
			FacesMessages.instance().add("El largo máximo para el campo\"Nombre\" no puede superar los 99 caracteres.");
			return;
		}
		
		final Query query = em.createQuery("SELECT c FROM Cargo c WHERE c.unidadOrganizacional.id = :idUnidad and :descripcion = c.descripcion");
		query.setParameter("idUnidad", unidadOrganizacional);
		query.setParameter("descripcion", descripcion);
		List<Cargo> iguales = query.getResultList();
		//
		if(iguales.size() == 0){
		
			unidadCargo = em.find(UnidadOrganizacional.class, unidadOrganizacional);
			cargo.setUnidadOrganizacional(unidadCargo);
			cargo.setDescripcion(descripcion);
			cargo.setCargoDefault(false);
	
			if (existe) {
				em.merge(cargo);
				FacesMessages.instance().add("Se modificó el cargo en el sistema.");
			} else {
				em.persist(cargo);
				FacesMessages.instance().add("Se agregó el cargo al sistema.");
			}
	
			this.cargos = null;
			this.cargo = null;
			listOrganizacion = this.buscarOrganizaciones();
			organizacion = (Long) listOrganizacion.get(1).getValue();
			this.buscarDivisiones();
			this.limpiarDestinatario();
		}
		else{
			FacesMessages.instance().add("Ya existe el cargo para esta unidad.");
		}
		
		/*
		 * 
		 */
		
		listOrganizacion = this.buscarOrganizaciones();
		
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		
		this.division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		
		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        
		//this.setListDepartamento(jerarquias.getFiscalias());
		//this.departamento = (Long) listDepartamento.get(1).getValue();
		this.buscarUnidadesOrganizacionales();
		if(unidadCargo != null)this.unidadOrganizacional = unidadCargo.getId();
		encontrarCargos();
		
	}
		/*
		 * 
		 */
	

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		if (divisiones != null && divisiones.size() == 1) {
			final Division sdivision = divisiones.get(0);
			if (sdivision.getConCargo()) {
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.encontrarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		listDepartamento.clear();
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento sdepartamento = departamentos.get(0);
			if (sdepartamento.getConCargo()) {
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.encontrarCargos();
			}
		}
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				departamento);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void encontrarCargos() {
		final Query query = em.createQuery("SELECT c FROM Cargo c WHERE c.unidadOrganizacional.id = :idUnidad");
		query.setParameter("idUnidad", unidadOrganizacional);
		cargos = query.getResultList();
	}

	@Override
	public void limpiarFormulario() {
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		this.limpiarDestinatario();
		this.cargos = null;
		this.cargo = null;
	}

	/**
	 * 
	 */
	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.descripcion = "";
		listDivision.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void modificar(final Long idCargo) {
		
		//Se realiza la busqueda del cargo
		cargo = em.find(Cargo.class, idCargo);
		
		//Se obtiene jerarquia a la cual pertenece aquel cargo.
		final UnidadOrganizacional uni = cargo.getUnidadOrganizacional();
		final Departamento dep = uni.getDepartamento();
		final Division div = dep.getDivision();
		final Organizacion org = div.getOrganizacion();

		//Se setea la organizacion correspondiente y se carga la lista de Divisiones.
		organizacion = org.getId();
		this.buscarDivisiones();
		
		//Se setea la division correspondiente y se carga la lista de Deptos.
		division = div.getId();
		this.buscarDepartamentos();

		//Se setea el Depto correspondiente y se carga la lista de Unidades.
		departamento = dep.getId();
		this.buscarUnidadesOrganizacionales();

		//Se setea la Unidad correspondiente y se carga la lista de Cargos.
		unidadOrganizacional = uni.getId();

		descripcion = cargo.getDescripcion();
	}

	@Override
	public void eliminar(final Long idCargo) {
		cargo = em.find(Cargo.class, idCargo);
		if (cargo.getPersonas().size() == 0) {
			cargos.remove(cargo);
			em.remove(cargo);
			this.cargo = null;
			//this.limpiarDestinatario();
			encontrarCargos();
			FacesMessages.instance().add("Se ha eliminado el cargo del sistema");
		} else {
			FacesMessages.instance().add("El cargo no se puede eliminar, pues hay personas asociadas a ese cargo");
		}
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Cargo getCargo() {
		if (cargo == null) {
			cargo = new Cargo();
		}
		return cargo;
	}

	@Override
	public void setCargo(final Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public String getDescripcion() {
		return descripcion;
	}

	@Override
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public List<Cargo> getCargos() {
		return cargos;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
}
