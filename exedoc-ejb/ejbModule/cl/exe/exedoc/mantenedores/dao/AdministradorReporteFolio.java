package cl.exe.exedoc.mantenedores.dao;

import java.util.HashMap;
import java.util.List;

import cl.exe.exedoc.entity.ReporteSolicitudFolioDocumento;

/**
 * @author Administrator
 *
 */
public interface AdministradorReporteFolio {

	/**
	 * @param reporteSolicitudFolioDocumento {@link reporteSolicitudFolioDocumento}
	 * @return {@link boolean}
	 */
	boolean crear(ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento);

	/**
	 * @param id {@link Long}
	 * @return {@link boolean}
	 */
	boolean eliminar(Long id);
	
	/**
	 * @param reporteSolicitudFolioDocumento {@link ReporteSolicitudFolioDocumento}
	 * @return {@link boolean}
	 */
	boolean actualizar(ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento);

	/**
	 * @param id {@link Long}
	 * @return {@link ReporteSolicitudFolioDocumento}
	 */
	ReporteSolicitudFolioDocumento buscarReporteSolicitudFolioDocumento(Long id);

	/**
	 * @param parametros {@link HashMap}
	 * @return {@link List} of {@link ReporteSolicitudFolioDocumento}
	 */
	List<ReporteSolicitudFolioDocumento> buscarReporteSolicitudFolioDocumento(
			HashMap<String, Object> parametros, HashMap<String, Object> fechas);

	
	
}
