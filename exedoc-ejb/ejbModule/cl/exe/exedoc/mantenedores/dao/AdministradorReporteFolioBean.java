package cl.exe.exedoc.mantenedores.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.ReporteSolicitudFolioDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.FechaUtil;

/**
 * Implementacion de {@link AdministradorAlertas}.
 * 
 * @author Administrator
 * 
 */
@Stateless
@Name(AdministradorReporteFolioBean.NAME)
@Scope(value = ScopeType.SESSION)
public class AdministradorReporteFolioBean implements AdministradorReporteFolio {

	/**
	 * NAME = "administradorReporteFolios".
	 */
	public static final String NAME = "administradorReporteFolios";

	private static final String ERROR_AL_OBTENER_ALERTAS = "Error al obtener reporte de folios: ";
	
	private static final String STRING_INICIO = "inicio";
	private static final String STRING_TERMINO = "termino";

	private static Logger log = Logger
			.getLogger(AdministradorReporteFolioBean.class);

	@PersistenceContext
	private EntityManager em;

	private Date fechaInicio;
	private Date fechaTermino;
	
	/**
	 * Constructor.
	 */
	public AdministradorReporteFolioBean() {
		super();
	}

	@Override
	public boolean crear(
			ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento) {

		try {
			em.persist(reporteSolicitudFolioDocumento);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al persistir reporteSolicitudFolioDocumento: ", e);
		}

		return false;
	}

	@Override
	public boolean actualizar(
			ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento) {

		try {
			em.merge(reporteSolicitudFolioDocumento);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al actualizar reporteSolicitudFolioDocumento: ", e);
		}
		return false;
	}

	@Override
	public boolean eliminar(final Long id) {

		final ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento = this
				.buscarReporteSolicitudFolioDocumento(id);

		if (reporteSolicitudFolioDocumento != null) {
			try {
				em.remove(reporteSolicitudFolioDocumento);
				return true;
			} catch (PersistenceException e) {
				log.error("Error al eliminar reporteSolicitudFolioDocumento: ",
						e);
			}
		}

		return false;
	}

	@Override
	public ReporteSolicitudFolioDocumento buscarReporteSolicitudFolioDocumento(
			Long id) {

		ReporteSolicitudFolioDocumento reporteSolicitudFolioDocumento = null;

		log.info("Id ReporteSolicitudFolioDocumento: " + id);

		final Query query = em
				.createNamedQuery("ReporteSolicitudFolioDocumento.findById");
		query.setParameter("id", id);

		try {
			reporteSolicitudFolioDocumento = (ReporteSolicitudFolioDocumento) query
					.getSingleResult();
		} catch (PersistenceException e) {
			log.error(ERROR_AL_OBTENER_ALERTAS, e);
		}

		return reporteSolicitudFolioDocumento;

	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<ReporteSolicitudFolioDocumento> buscarReporteSolicitudFolioDocumento(
			HashMap<String, Object> parametros, HashMap<String, Object> fechas) {
		Date inicio = null;
		Date termino = null;
		
		
		List<ReporteSolicitudFolioDocumento> result = new ArrayList<ReporteSolicitudFolioDocumento>();

		StringBuilder sb = new StringBuilder();

		sb.append("select r from ReporteSolicitudFolioDocumento r ");

		if (!parametros.isEmpty()) {

			sb.append("where ");

			int max = parametros.size();
			int curr = 0;

			Iterator iter = parametros.entrySet().iterator();

			while (iter.hasNext()) {

				Map.Entry e = (Map.Entry) iter.next();

				sb.append("r.").append(e.getKey()).append(" = :")
						.append(e.getKey()).append(" ");

				curr++;
				if (max > curr) {
					sb.append("and ");
				}
			}
		}
		
		if(!fechas.isEmpty()){
			if(parametros.isEmpty()){
				sb.append("where ");
			}
			else {
				sb.append(" and ");
			}
			if(fechas.containsKey("fechaInicio")){
				inicio = FechaUtil.inicio((Date)fechas.get("fechaInicio"));
			}
			if(fechas.containsKey("fechaTermino")){
				termino = FechaUtil.fin((Date)fechas.get("fechaTermino"));
			}
			
			if(inicio != null && termino != null ) sb.append(" r.fechaSolicitud between :inicio and :termino");
			if(inicio != null && termino == null ) sb.append(" r.fechaSolicitud > :inicio");
			if(inicio == null && termino != null ) sb.append(" r.fechaSolicitud < :termino"); 
			
			}
		      sb.append(" ORDER BY r.numeroActual DESC, r.tipoDocumento ASC");
		log.info(sb.toString());

		final Query query = em.createQuery(sb.toString());

		Iterator iter = parametros.entrySet().iterator();

		while (iter.hasNext()) {

			Map.Entry e = (Map.Entry) iter.next();
			query.setParameter((String) e.getKey(), e.getValue());
		}

		if(inicio != null )query.setParameter(STRING_INICIO, inicio, TemporalType.TIMESTAMP);
		if(termino != null )query.setParameter(STRING_TERMINO, termino, TemporalType.TIMESTAMP);
		
		
		result = query.getResultList();

		for (ReporteSolicitudFolioDocumento r : result) {

			if (r.getUnidad() != null) {
				r.setUnidadOrganizacional(em.find(UnidadOrganizacional.class,
						r.getUnidad()));
			}
			r.setTipoDocumentoEntity(em.find(TipoDocumento.class,
					r.getTipoDocumento().intValue()));
			r.setDepartamento(em.find(Departamento.class, r.getFiscalia()));
			
		}

		return result;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	
	

}
