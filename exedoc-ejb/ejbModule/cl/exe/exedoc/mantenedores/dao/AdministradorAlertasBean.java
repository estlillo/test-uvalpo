package cl.exe.exedoc.mantenedores.dao;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.email.Email;
import cl.exe.email.message.Mail;
import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.estructura.AlertaUsuario;
import cl.exe.exedoc.util.FechaUtil;

/**
 * Implementacion de {@link AdministradorAlertas}.
 * 
 * @author Administrator
 * 
 */
@Stateless
@Name(AdministradorAlertasBean.NAME)
@Scope(value = ScopeType.SESSION)
public class AdministradorAlertasBean implements AdministradorAlertas {

	/**
	 * NAME = "administradorAlertas".
	 */
	public static final String NAME = "administradorAlertas";
	
	private static final String ERROR_AL_OBTENER_ALERTAS = "Error al obtener alertas: ";
	private static final String PER = "%";
	
	private static Logger log = Logger
			.getLogger(AdministradorAlertasBean.class);
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * Constructor.
	 */
	public AdministradorAlertasBean() {
		super();
	}

	@Override
	public boolean crear(final Alerta alerta) {

		try {
			em.persist(alerta);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al persistir alerta: ", e);
		}

		return false;
	}

	@Override
	public boolean eliminar(final Long id) {

		final Alerta alerta = this.buscarAlerta(id);

		if (alerta != null) {
			try {
				alerta.setEliminado(true);
				//em.remove(alerta);
				return true;
			} catch (PersistenceException e) {
				log.error("Error al eliminar alerta: ", e);
			}
		}

		return false;
	}

	@Override
	public boolean actualizar(final Alerta alerta) {

		try {
			em.merge(alerta);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al actualizar alerta: ", e);
		}

		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Alerta> buscarAlerta() {

		List<Alerta> alertas = null;

		final Query query = em.createNamedQuery("Alerta.selectAll");

		try {
			alertas = query.getResultList();
		} catch (PersistenceException e) {
			log.error(ERROR_AL_OBTENER_ALERTAS, e);
		}

		return alertas;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Alerta> buscarAlerta(final String nombre) {

		List<Alerta> alertas = null;

		final Query query = em.createNamedQuery("Alerta.findByName");
		query.setParameter("nombre", PER + nombre + PER);

		try {
			alertas = query.getResultList();
		} catch (PersistenceException e) {
			log.error(ERROR_AL_OBTENER_ALERTAS, e);
		}

		return alertas;
	}

	@Override
	public Alerta buscarAlerta(final Long id) {

		Alerta alerta = null;

		log.debug("Id Alerta: " + id);

		final Query query = em.createNamedQuery("Alerta.findById");
		query.setParameter("id", id);

		try {
			alerta = (Alerta) query.getSingleResult();
		} catch (PersistenceException e) {
			log.error(ERROR_AL_OBTENER_ALERTAS, e);
		}

		return alerta;
	}

	@Override
	public String validarPlazo(final Long id, final Date plazo) {
		if(id == null)
			return null;
		final Alerta alerta = this.buscarAlerta(id);
		return validarPlazoContraFecha(alerta,plazo,new Date(),false);
		
	}
	private String validarPlazoContraFecha(final Alerta alerta, final Date plazo,Date fechaComp,boolean incluyeCero){
		String mensaje = null;


		if (alerta != null) {

			final int dif = FechaUtil.diasDiferencia(fechaComp, plazo);

			if (dif > 0 && dif <= alerta.getPlazo().intValue()) {
				mensaje = ((dif > 1) ? "Faltan " : "Falta ") + dif
						+ ((dif > 1) ? " días" : " día");
			}else if(incluyeCero && dif == 0){
				mensaje = "Vence hoy a las 23:59";
			}
		}

		return mensaje;
	}
	
	@Override
	public void pruebaNotificaciones() {
		obtenerDocumetosYEnviarAlerta(new Date());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void obtenerDocumetosYEnviarAlerta(final Date fechaMin) {

		log.info("Inicio busqueda de destinatarios alerta...");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
		Calendar inicioDia = new GregorianCalendar();
		inicioDia.setTime(fechaMin);
		inicioDia.set(Calendar.HOUR_OF_DAY, 0);
		inicioDia.set(Calendar.MINUTE, 0);
		inicioDia.set(Calendar.SECOND, 0);
		inicioDia.set(Calendar.MILLISECOND, 0);
		Calendar finDia = new GregorianCalendar();
		finDia.setTime(fechaMin);
		finDia.set(Calendar.HOUR_OF_DAY, 23);
		finDia.set(Calendar.MINUTE, 59);
		finDia.set(Calendar.SECOND, 59);
		finDia.set(Calendar.MILLISECOND, 999);
		
		List<Object[]> listaCorreo = new ArrayList<Object[]>();
		List<Object[]> listaCorreoGrupos = new ArrayList<Object[]>();
		
		// Documentos que no tienen alerta pero sí plazo
		// Se avisa el día del vencimiento
		// REGLA: debe estar en la bandeja de entrada o compartida
		StringBuilder sql = new StringBuilder();
		// entrada sin acuse de recibo
		sql.append("select d, e.destinatario ");
		sql.append("from Expediente e ");
		sql.append("inner join e.documentos ed ");
		sql.append("inner join ed.documento d ");
		sql.append("where e.archivado is null ");
		sql.append("and e.cancelado is null ");
		sql.append("and e.copia = false ");
		sql.append("and e.reasignado is null ");
		sql.append("and e.fechaDespacho is not null ");
		sql.append("and e.fechaAcuseRecibo is null ");
		sql.append("and e.destinatario is not null ");
		sql.append("and e.destinatario.email is not null ");
		sql.append("and (d.eliminado is null or d.eliminado = false) ");
		sql.append("and d.alerta is null ");
		sql.append("and d.plazo is not null ");
		sql.append("and d.plazo between :inicio and :fin ");
		Query query = em.createQuery(sql.toString());
		query.setParameter("inicio", inicioDia.getTime());
		query.setParameter("fin", finDia.getTime());
		List<Object[]> listaVencidos = query.getResultList();
		if (listaVencidos != null && listaVencidos.size() > 0) {
			listaCorreo.addAll(listaVencidos);
		}
		// entrada con acuse de recibo
		sql = new StringBuilder();
		sql.append("select d, e.destinatarioHistorico ");
		sql.append("from Expediente e ");
		sql.append("inner join e.documentos ed ");
		sql.append("inner join ed.documento d ");
		sql.append("where e.archivado is null ");
		sql.append("and e.cancelado is null ");
		sql.append("and e.copia = false ");
		sql.append("and e.reasignado is null ");
		sql.append("and e.fechaDespacho is null ");
		sql.append("and e.fechaAcuseRecibo is not null ");
		sql.append("and e.destinatarioHistorico is not null ");
		sql.append("and e.destinatarioHistorico.email is not null ");
		sql.append("and (d.eliminado is null or d.eliminado = false) ");
		sql.append("and d.alerta is null ");
		sql.append("and d.plazo is not null ");
		sql.append("and d.plazo between :inicio and :fin ");
		//System.out.println(sql.toString());
		//System.out.println(sdf.format(inicioDia.getTime()));
		//System.out.println(sdf.format(finDia.getTime()));
		query = em.createQuery(sql.toString());
		query.setParameter("inicio", inicioDia.getTime());
		query.setParameter("fin", finDia.getTime());
		listaVencidos = query.getResultList();
		if (listaVencidos != null && listaVencidos.size() > 0) {
			listaCorreo.addAll(listaVencidos);
		}
		
		// compartida sin acuse de recibo
		sql = new StringBuilder();
		sql.append("select d, p ");
		sql.append("from Expediente e ");
		sql.append("inner join e.documentos ed ");
		sql.append("inner join ed.documento d ");
		sql.append("inner join e.grupo g ");
		sql.append("inner join g.usuarios p ");
		sql.append("where e.archivado is null ");
		sql.append("and e.cancelado is null ");
		sql.append("and e.reasignado is null ");
		sql.append("and e.fechaDespacho is not null ");
		sql.append("and e.fechaAcuseRecibo is null ");
		sql.append("and e.grupo is not null ");
		sql.append("and p.email is not null ");
		sql.append("and (d.eliminado is null or d.eliminado = false) ");
		sql.append("and d.alerta is null ");
		sql.append("and d.plazo is not null ");
		sql.append("and d.plazo between :inicio and :fin ");
		//System.out.println(sql.toString());
		//System.out.println(sdf.format(inicioDia.getTime()));
		//System.out.println(sdf.format(finDia.getTime()));
		query = em.createQuery(sql.toString());
		query.setParameter("inicio", inicioDia.getTime());
		query.setParameter("fin", finDia.getTime());
		List<Object[]> listaVencidosGrupos = query.getResultList();
		if (listaVencidosGrupos != null && listaVencidosGrupos.size() > 0) {
			listaCorreoGrupos.addAll(listaVencidosGrupos);
		}
		
		//TODO repeat with each alert

		query = em.createNamedQuery("Alerta.selectAll");
		List<Alerta> alertas = query.getResultList();
		if (alertas != null && alertas.size() > 0) {
			for (Alerta a : alertas) {
				Calendar fechaAlerta = finDia;
				if (a.getPlazo() != null) {
					fechaAlerta.add(Calendar.DATE, a.getPlazo().intValue());
					sql = new StringBuilder();
					// entrada sin acuse de recibo
					sql.append("select d, e.destinatario ");
					sql.append("from Expediente e ");
					sql.append("inner join e.documentos ed ");
					sql.append("inner join ed.documento d ");
					sql.append("inner join d.alerta a ");
					sql.append("where e.archivado is null ");
					sql.append("and e.cancelado is null ");
					sql.append("and e.copia = false ");
					sql.append("and e.reasignado is null ");
					sql.append("and e.fechaDespacho is not null ");
					sql.append("and e.fechaAcuseRecibo is null ");
					sql.append("and e.destinatario is not null ");
					sql.append("and e.destinatario.email is not null ");
					sql.append("and a.id = :alerta ");
					sql.append("and (d.eliminado is null or d.eliminado = false) ");
					sql.append("and d.plazo is not null ");
					sql.append("and d.plazo <= :fechaAlerta ");
					//System.out.println(sql.toString());
					//System.out.println(sdf.format(fechaAlerta.getTime()));
					query = em.createQuery(sql.toString());
					query.setParameter("alerta", a.getId());
					query.setParameter("fechaAlerta", fechaAlerta.getTime());
					List<Object[]> listaAlerta = query.getResultList();
					if (listaAlerta != null && listaAlerta.size() > 0) {
						listaCorreo.addAll(listaAlerta);
					}
					// entrada con acuse de recibo
					sql = new StringBuilder();
					sql.append("select d, e.destinatarioHistorico ");
					sql.append("from Expediente e ");
					sql.append("inner join e.documentos ed ");
					sql.append("inner join ed.documento d ");
					sql.append("inner join d.alerta a ");
					sql.append("where e.archivado is null ");
					sql.append("and e.cancelado is null ");
					sql.append("and e.copia = false ");
					sql.append("and e.reasignado is null ");
					sql.append("and e.fechaDespacho is null ");
					sql.append("and e.fechaAcuseRecibo is not null ");
					sql.append("and e.destinatarioHistorico is not null ");
					sql.append("and e.destinatarioHistorico.email is not null ");
					sql.append("and a.id = :alerta ");
					sql.append("and (d.eliminado is null or d.eliminado = false) ");
					sql.append("and d.plazo is not null ");
					sql.append("and d.plazo <= :fechaAlerta ");
					//System.out.println(sql.toString());
					//System.out.println(sdf.format(fechaAlerta.getTime()));
					query = em.createQuery(sql.toString());
					query.setParameter("alerta", a.getId());
					query.setParameter("fechaAlerta", fechaAlerta.getTime());
					listaAlerta = query.getResultList();
					if (listaAlerta != null && listaAlerta.size() > 0) {
						listaCorreo.addAll(listaAlerta);
					}
					
					// compartida sin acuse de recibo
					sql = new StringBuilder();
					sql.append("select d, p ");
					sql.append("from Expediente e ");
					sql.append("inner join e.documentos ed ");
					sql.append("inner join ed.documento d ");
					sql.append("inner join d.alerta a ");
					sql.append("inner join e.grupo g ");
					sql.append("inner join g.usuarios p ");
					sql.append("where e.archivado is null ");
					sql.append("and e.cancelado is null ");
					sql.append("and e.reasignado is null ");
					sql.append("and e.fechaDespacho is not null ");
					sql.append("and e.fechaAcuseRecibo is null ");
					sql.append("and e.grupo is not null ");
					sql.append("and p.email is not null ");
					sql.append("and a.id = :alerta ");
					sql.append("and (d.eliminado is null or d.eliminado = false) ");
					sql.append("and d.plazo is not null ");
					sql.append("and d.plazo <= :fechaAlerta ");
					//System.out.println(sql.toString());
					//System.out.println(sdf.format(fechaAlerta.getTime()));
					query = em.createQuery(sql.toString());
					query.setParameter("alerta", a.getId());
					query.setParameter("fechaAlerta", fechaAlerta.getTime());
					List<Object[]> listaAlertaGrupos = query.getResultList();
					if (listaAlertaGrupos != null && listaAlertaGrupos.size() > 0) {
						listaCorreoGrupos.addAll(listaAlertaGrupos);
					}
				}
			}
		}

		this.prepararDestinatariosAlerta(listaCorreo, listaCorreoGrupos);
	}

	private void prepararDestinatariosAlerta(final List<Object[]> destinatarios, final List<Object[]> destinatariosGrupo) {

		String msj = "";
		msj = MessageFormat.format("Resultados obtenidos Alertas Destinatarios : {0}",
				(destinatarios.size() + destinatariosGrupo.size()));
		log.info(msj);

		final Map<Long, AlertaUsuario> alertas = new HashMap<Long, AlertaUsuario>();

		Persona persona = null;
		Documento documento = null;
		for (Object[] o : destinatarios) {
			persona = new Persona();
			documento = new Documento();
			documento = (Documento) o[0];
			persona = (Persona) o[1];
			if (alertas.containsKey(persona.getId())) {
				if (!alertas.get(persona.getId()).getDocumentos().contains(documento)){
					alertas.get(persona.getId()).getDocumentos().add(documento);
				}
			} else {
				final AlertaUsuario alusr = new AlertaUsuario();
				alusr.setPersona(persona);
				final List<Documento> documentos = new ArrayList<Documento>();
				documentos.add(documento);
				alusr.setDocumentos(documentos);
				alusr.setDocumentosCompartidos(new ArrayList<Documento>());
				alertas.put(persona.getId(), alusr);
			}
		}
		
		for (Object[] o : destinatariosGrupo) {
			persona = new Persona();
			documento = new Documento();
			documento = (Documento) o[0];
			persona = (Persona) o[1];
			if (alertas.containsKey(persona.getId())) {
				if (!alertas.get(persona.getId()).getDocumentosCompartidos().contains(documento)){
					alertas.get(persona.getId()).getDocumentosCompartidos().add(documento);
				}
			} else {
				final AlertaUsuario alusr = new AlertaUsuario();
				alusr.setPersona(persona);
				final List<Documento> documentos = new ArrayList<Documento>();
				documentos.add(documento);
				alusr.setDocumentosCompartidos(documentos);
				alusr.setDocumentos(new ArrayList<Documento>());
				alertas.put(persona.getId(), alusr);
			}
		}

		msj = MessageFormat.format("Total Destinatarios a notificar Alertas : {0}",
				alertas.keySet().size());
		log.info(msj);

		this.enviarCorreos(alertas);
	}

	/**
	 * @param alertas
	 *            {@link Map}
	 */
	private void enviarCorreos(final Map<Long, AlertaUsuario> alertas) {

		String msj = "";

		final Email email = new Email();

		Mail mail = null;

		final Iterator<Entry<Long, AlertaUsuario>> it = alertas.entrySet()
				.iterator();

		while (it.hasNext()) {

			final Map.Entry<Long, AlertaUsuario> e = it.next();

			if (e.getValue().getPersona().getEmail() != null) {
				try {
					mail = new Mail();
					mail.setForm(new InternetAddress(e.getValue().getPersona()
							.getEmail()));
					mail.setSubject("Recordatorio Documentos por Vencer");
					mail.setText(this.generarCuerpoMail(e.getValue()));
					email.enviarMail(mail);
				} catch (AddressException e1) {
					msj = MessageFormat.format(
							"Ocurrio un error con el siguiente mail: {0} ", e
									.getValue().getPersona().getEmail());
					log.error(msj, e1);
				}
			}
		}
	}

	/**
	 * @param value
	 *            {@link Alerta}
	 * @return {@link String}
	 */
	private String generarCuerpoMail(final AlertaUsuario value) {

		final StringBuilder cuerpo = new StringBuilder();
		cuerpo.append("Estimado (a): ").append(value.getPersona().getNombreApellido());
		cuerpo.append("\nLe informamos que tiene los siguientes documentos por vencer: \n");
		if (value.getDocumentos() != null && value.getDocumentos().size() > 0) {
			cuerpo.append("\n\nBandeja de Entrada");
			for (Documento d : value.getDocumentos()) {
				cuerpo.append("\n - Numero ").append(d.getNumeroDocumento()).append(", Tipo Documento: ").append(d.getTipoDocumento().getDescripcion()).append(", Materia: ").append(d.getMateria());
			}
		}
		if (value.getDocumentosCompartidos() != null && value.getDocumentosCompartidos().size() > 0) {
			cuerpo.append("\n\nBandeja Compartida");
			for (Documento d : value.getDocumentosCompartidos()) {
				cuerpo.append("\n - Numero ").append(d.getNumeroDocumento()).append(", Tipo Documento: ").append(d.getTipoDocumento().getDescripcion()).append(", Materia: ").append(d.getMateria());
			}
		}
		cuerpo.append(PLANTILLA_FOOTER);
		return cuerpo.toString();
	}

	private Date removerHorasMinutosSegundos(Date fechaMin){
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaMinSinHorasMinutosSegundos = simpleDateFormat.parse(simpleDateFormat.format(fechaMin));
			return fechaMinSinHorasMinutosSegundos;
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date(fechaMin.getTime()+3600*24*1000);//Para que no se caiga y considere el mismo dia
		}
	}
	
	private void enviarCorreo(String cuerpoCorreo, Persona destinatarioEmail){

		final Email email = new Email();
		Mail mail = null;
		String msj = "";
		try {
			mail = new Mail();
			mail.setForm(new InternetAddress(destinatarioEmail.getEmail()));
			mail.setSubject("Solicitud de respuesta sin respuesta");
			mail.setText(cuerpoCorreo);
			email.enviarMail(mail);
		} catch (AddressException e1) {
			msj = MessageFormat.format(
					"Ocurrio un error con el siguiente mail: {0} ", destinatarioEmail.getEmail());
			log.error(msj, e1);
		}
	}

	@Override
	public boolean validarNombre(Alerta alerta) {
		boolean retorno = true;
		List<Alerta> lista;
		if (alerta.getId() == null) {
			lista = (List<Alerta>)em.createNamedQuery("Alerta.findByLowerName").setParameter("nombre", alerta.getNombre().toLowerCase()).getResultList();
			if (lista != null && lista.size() > 0) {
				retorno = false;
			}
		} else {
			lista = (List<Alerta>)em.createNamedQuery("Alerta.findByLowerNameExcludeId").setParameter("nombre", alerta.getNombre().toLowerCase()).setParameter("id", alerta.getId()).getResultList();
			if (lista != null && lista.size() > 0) {
				retorno = false;
			}
		}
		return retorno;
	}
	
	/**
	 * ESTO NO SE DEBE HACER!!!
	 * ES POR SI ALGUN MERME SE LE OLVIDA PONER PLANTILLAS DE CORREO
	 */
	
	public static final String PLANTILLA_DESPACHO = 
			"Estimado(a).\n\n" +
			"Se ha despachado el expediente Numero {0}, el cual se puede ver en su bandeja de {1}, con fecha de envio {2}.";
	
	public static final String PLANTILLA_FOOTER =
			"\n\nAtte. A Ud.\n" + 
			"Gestor Documental.\n\n" +
			"Nota: Se han quitado los caracteres especiales y tildes intecionalmente.";
	
	public static final String PLANTILLA_DESPACHO_DOCUMENTO = 
			"Estimado(a).\n\n" +
			"Se ha despachado un expediente Numero {0}, el cual se puede ver en su bandeja {1}, con fecha de envio {2}.\n\n" +
			"El numero del documento es {3}, la materia dice {4}, el tipo del documento es {5},	el emisor es {6} y los antecedentes son {7}.";

}
