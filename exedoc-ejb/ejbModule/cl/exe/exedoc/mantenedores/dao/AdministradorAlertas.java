package cl.exe.exedoc.mantenedores.dao;

import java.util.Date;
import java.util.List;

import cl.exe.exedoc.entity.Alerta;

/**
 * @author Administrator
 *
 */
public interface AdministradorAlertas {

	/**
	 * @param alerta {@link Alerta}
	 * @return {@link boolean}
	 */
	boolean crear(Alerta alerta);

	/**
	 * @param id {@link Long}
	 * @return {@link boolean}
	 */
	boolean eliminar(Long id);
	
	/**
	 * @param alerta {@link Alerta}
	 * @return {@link boolean}
	 */
	boolean actualizar(Alerta alerta);

	/**
	 * @return {@link List}<{@link Alerta}>
	 */
	List<Alerta> buscarAlerta();

	/**
	 * @param nombre {@link String}
	 * @return {@link List}<{@link Alerta}>
	 */
	List<Alerta> buscarAlerta(String nombre);

	/**
	 * @param id {@link Long}
	 * @return {@link Alerta}
	 */
	Alerta buscarAlerta(Long id);

	/**
	 * Retorna mensaje de alerta en el caso de estar en plazo correspondiente.
	 * 
	 * @param id {@link Long}
	 * @param plazo {@link Date}
	 * @return {@link String}
	 */
	String validarPlazo(Long id, Date plazo);

	/**
	 * @param fechaMin {@link Date}
	 */
	void obtenerDocumetosYEnviarAlerta(Date fechaMin);


	boolean validarNombre(Alerta alerta);

	void pruebaNotificaciones();
	
}
