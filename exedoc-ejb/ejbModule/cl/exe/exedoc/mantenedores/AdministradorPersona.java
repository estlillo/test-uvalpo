package cl.exe.exedoc.mantenedores;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import org.richfaces.event.UploadEvent;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Administrator
 */
@Local
public interface AdministradorPersona {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	String limpiarFormulario();

	/**
	 * 
	 */
	void guardarPersona();

	/**
	 * 
	 */
	void buscarDepartamentos();

	/**
	 * 
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * 
	 */
	void buscarCargos();

	/**
	 * 
	 */
	void buscarPersonas();

	/**
	 * @param idPersona Long
	 */
	void modificar(Long idPersona);

	/**
	 * 
	 */
	void buscarUsuario();

	/**
	 * 
	 */
	void actualizar();

	/**
	 * @param event UploadEvent
	 * @throws Exception 
	 */
	void listener(UploadEvent event) throws Exception;

	/**
	 * @param event UploadEvent
	 * @throws Exception 
	 */
	void listenerImagenFirma(UploadEvent event) throws Exception;

	/**
	 * @param output OutputStream
	 * @param object Object
	 * @throws IOException 
	 */
	void verImagenFirma(OutputStream output, Object object) throws IOException;

	/**
	 * @return long
	 */
	long getTime();

	/**
	 * @return Persona
	 */
	Persona getPersona();

	/**
	 * @return List<Persona>
	 */
	List<Persona> getPersonas();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDivision();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListUnidadesOrganizacionales();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListCargos();

	/**
	 * @return Long
	 */
	Long getDivision();

	/**
	 * @param division Long
	 */
	void setDivision(Long division);

	/**
	 * @return Long
	 */
	Long getDepartamento();

	/**
	 * @param departamento Long
	 */
	void setDepartamento(Long departamento);

	/**
	 * @return Long
	 */
	Long getUnidadOrganizacional();

	/**
	 * @param unidadOrganizacional Long
	 */
	void setUnidadOrganizacional(Long unidadOrganizacional);

	/**
	 * @return Long
	 */
	Long getCargo();

	/**
	 * @param cargo Long
	 */
	void setCargo(Long cargo);

	/**
	 * @return String
	 */
	String getUser();

	/**
	 * @param user String
	 */
	void setUser(String user);

	/**
	 * @return byte[]
	 */
	byte[] getKeyStore();

	/**
	 * @param keyStore byte[]
	 */
	void setKeyStore(byte[] keyStore);

	/**
	 * @return Integer
	 */
	Integer getTipoFirma();

	/**
	 * @param tipoFirma Integer
	 */
	void setTipoFirma(Integer tipoFirma);

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * @return Lon
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion Long
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion List<SelectItem>
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);

	/**
	 * 
	 */
	void importarPersona();

	/*
	 * @param String buscado
	 */
	List<Persona> getPersonas(String buscado , List<SelectPersonas> list);

	void eliminarBlancos(Persona persona);

	String convertImage(byte[] data);

}
