package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Plantilla;

/**
 * @author Administrator
 * 
 */
@Local
public interface AdministradorPlantillasContrato {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void crearNuevo();

	/**
	 * @param id Integer
	 */
	void modificar(Integer id);

	/**
	 * @param idPlantilla Integer
	 */
	void obtenerPlantilla(Integer idPlantilla);

	/**
	 * @param p Plantilla
	 */
	void eliminarPlantilla(Plantilla p);

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * @return Plantilla
	 */
	Plantilla getPlantilla();

	/**
	 * @param plantilla Plantilla
	 */
	void setPlantilla(Plantilla plantilla);

	/**
	 * @return List<Plantilla>
	 */
	List<Plantilla> getPlantillas();

	/**
	 * @return String
	 */
	String getNombre();

	/**
	 * @param nombre String
	 */
	void setNombre(String nombre);

	/**
	 * @return String
	 */
	String getMateria();

	/**
	 * @param materia String
	 */
	void setMateria(String materia);
	
	/**
	 * @return String
	 */
	String getTitulo();
	
	/**
	 * @param titulo String
	 */
	void setTitulo(String titulo);
	
	/**
	 * @return String
	 */
	String getPrologo();
	
	/**
	 * @param prologo String
	 */
	void setPrologo(String prologo);

	/**
	 * @return String
	 */
	String getPuntos();
	
	/**
	 * @param puntos String
	 */
	void setPuntos(String puntos);
}
