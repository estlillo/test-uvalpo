package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.TipoDocumento;

@Local
public interface AdministradorTiposDocumentos {
	
	//flujo
	public String begin();
	public void destroy();
	public String end();
	
	//acciones
	public void crearNuevoTipoDocumento();
	public void modificarTipoDocumento(Integer idTipoDocumento);

	//Setters & Getters
	public List<TipoDocumento> getTiposDocumentos();
	public TipoDocumento getTipoDocumento();
	public void setTipoDocumento(TipoDocumento tipoDocumento);
	
	
}
