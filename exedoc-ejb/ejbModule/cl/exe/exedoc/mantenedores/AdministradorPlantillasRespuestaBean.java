package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.PlantillaRespuesta;

@Stateful
@Name("administradorPlantillasRespuesta")
public class AdministradorPlantillasRespuestaBean implements AdministradorPlantillasRespuesta {

	private PlantillaRespuesta plantilla;
	private List<PlantillaRespuesta> plantillas;

	private String nombre;
	private String contenido;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	@Begin(join = true)
	public String begin() {
		log.info("Comenzando conversacion...");
		plantilla = new PlantillaRespuesta();
		plantillas = new ArrayList<PlantillaRespuesta>();

		nombre = "";
		contenido = "";

		buscar();

		return "administradorPlantillasRespuesta";
	}

	@SuppressWarnings("unchecked")
	private void buscar() {
		Query query = em.createNamedQuery("PlantillaRespuesta.findByAll");
		this.plantillas = query.getResultList();
	}

	public void crear() {
		if (plantilla != null && plantilla.getId() != null) {
			log.info("Modificando plantilla: " + plantilla.getNombre() + " ...");

			actualizarCamposPlantilla(plantilla);

			FacesMessages.instance().add("Se modificó la plantilla en el sistema");
			log.info("Se modificó la plantilla en el sistema");

			limpiarFormulario();
		} else {
			log.info("Creando plantilla...");
			boolean ok = true;

			if (getNombre().equals("")) {
				FacesMessages.instance().add("Ingrese el nombre");
				ok = false;
			}
			if (getContenido().equals("")) {
				FacesMessages.instance().add("Ingrese el contenido");
				ok = false;
			}

			if (ok) {
				plantilla.setNombre(getNombre());
				plantilla.setContenido(getContenido());

				em.persist(plantilla);
				FacesMessages.instance().add("Se agregó la plantilla al sistema");
				log.info("Se agregó la plantilla al sistema");
				limpiarFormulario();
			}

			buscar();
			plantilla = new PlantillaRespuesta();
		}
	}

	private void actualizarCamposPlantilla(PlantillaRespuesta plant) {
		if (plant != null) {
			plant.setNombre(getNombre());
			plant.setContenido(getContenido());
		}
		em.merge(plant);
	}

	public void limpiarFormulario() {
		plantilla = null;
		plantillas = null;

		begin();
	}

	public void cargar(Integer id) {
		plantilla = em.find(PlantillaRespuesta.class, id);
		setNombre(plantilla.getNombre());
		setContenido(plantilla.getContenido());
	}

	@End
	public String end() {
		log.info("Terminando conversacion...");

		return "home";
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	// Getters & Setters
	public List<PlantillaRespuesta> getPlantillas() {
		return plantillas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
}
