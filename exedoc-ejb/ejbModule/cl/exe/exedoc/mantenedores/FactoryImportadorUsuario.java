package cl.exe.exedoc.mantenedores;

/**
 * Class {@link FactoryImportadorUsuario}.
 *
 */
public class FactoryImportadorUsuario {

	/**
	 * Constructor. Se declara privado por ser clase utilitaria (para no sea instanciada).
	 */
	private FactoryImportadorUsuario() {
		super();
	}

	/**
	 * Metodo factory para crear un servicio externo.
	 * 
	 * @return un servicio externo.
	 */
	public static ImportadorUsuario getImportadorUsuarios() {
		return new ImportadorUsuariosImpl();
	}
}
