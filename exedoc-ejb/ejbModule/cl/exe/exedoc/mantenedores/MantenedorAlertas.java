package cl.exe.exedoc.mantenedores;

import java.util.List;

import cl.exe.exedoc.entity.Alerta;

/**
 * @author Pablo
 * 
 */
public interface MantenedorAlertas {

	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return {@link String}
	 */
	String begin();
	
	/**
	 * 
	 */
	void guardarAlerta();
	
	/**
	 * @param id {@link Long}
	 */
	void eliminarAlerta(Long id);
	
	/**
	 * @param id {@link Long}
	 */
	void editarAlerta(Long id);

	/**
	 * @return {@link Alerta}
	 */
	Alerta getAlerta();

	/**
	 * @param alerta
	 *            {@link Alerta}
	 */
	void setAlerta(Alerta alerta);

	/**
	 * @return {@link List}<{@link Alerta}>
	 */
	List<Alerta> getListAlertas();

	/**
	 * @param listAlertas
	 *            {@link List}<{@link Alerta}>
	 */
	void setListAlertas(List<Alerta> listAlertas);

	void pruebaNotificaciones();

}
