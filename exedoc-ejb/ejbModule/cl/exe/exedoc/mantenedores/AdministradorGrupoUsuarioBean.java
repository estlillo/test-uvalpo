package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * @author Administrator
 * Implementacion de {@link AdministradorGrupoUsuario}
 *
 */
@Stateful
@Name("administradorGrupoUsuario")
@Scope(value = ScopeType.SESSION)
public class AdministradorGrupoUsuarioBean implements AdministradorGrupoUsuario {

	private String nombre;
	private Departamento unidad;
	private List<Persona> usuarios;
	private List<SelectItem> listGrupos = new ArrayList<SelectItem>();
	private List<SelectItem> Fiscalias = new ArrayList<SelectItem>();
	
	
	private Long Fiscalia = JerarquiasLocal.INICIO;
	
	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquia;

	@In(required = true)
    private Persona usuario;

	/**
	 * Constructor.
	 */
	public AdministradorGrupoUsuarioBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {
	
		return "administradorGrupoUsuario";
	}

	@Override
	@End
	public String end() {
		return "home";
	}
	

	@Override
	public void buscarUnidadesOrganizacionales() {
		
	}

	@Override
	public Departamento getUnidadOrganizacional() {
		return unidad;
	}

	@Override
	public void setUnidadOrganizacional(final Departamento unidadOrganizacional) {
		this.unidad = unidadOrganizacional;
	}


	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public void buscarGrupos() {
	//TODO cambiar a Jerarquias
		Query query = em.createNamedQuery("GrupoUsuario.findByAll");
		
		List<Object> result = query.getResultList();
		for (Object object : result) {
			SelectItem select = new SelectItem();
			GrupoUsuario grupo = (GrupoUsuario)object;
			select.setValue(grupo);
			select.setLabel(grupo.getNombre());
			this.listGrupos.add(select);
		}
	}

	@Override
	public List<Persona> getListUsuarios() {
		return usuarios;
	}

	@Override
	public void setListUsuarios(List<Persona> usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public String getNombre() {
		return this.nombre;
	}

	@Override
	public void setNombre(String nombre) {
		this.nombre = nombre;
		
	}
	@Override
	public List<SelectItem> getListGrupos() {
		
		return listGrupos;
	}
	@Override
	public void setListGrupos(List<SelectItem> listGrupos) {
		
		this.listGrupos = listGrupos;
	}

	public List<SelectItem> getFiscalias() {
		return Fiscalias;
	}

	public void setFiscalias(List<SelectItem> fiscalias) {
		Fiscalias = fiscalias;
	}

	public Long getFiscalia() {
		return Fiscalia;
	}

	public void setFiscalia(Long fiscalia) {
		Fiscalia = fiscalia;
	}
	@Override
	public GrupoUsuario getGrupoUsuario(Long id){
		
		Query query = em.createNamedQuery("GrupoUsuario.findById");
		query.setParameter("id", id);
		GrupoUsuario grupoUsuario = (GrupoUsuario) query.getSingleResult();
		
		return grupoUsuario;
				
	}
	
	
}
