package cl.exe.exedoc.mantenedores;


/**
 * @author Administrator
 *
 */
public interface AdministradorPersonaDocumento {

	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @param idDoc {@link Long}
	 * @param idPersona {@link Long}
	 * @return {@link boolean}
	 */
	boolean existePersona(Long idDoc, Long idPersona);

	/**
	 * @param idDocumento {@link Long}
	 */
	void deleteAllItem(Long idDocumento);

}
