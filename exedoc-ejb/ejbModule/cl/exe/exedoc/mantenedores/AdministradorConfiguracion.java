package cl.exe.exedoc.mantenedores;

import javax.ejb.Local;

import cl.exe.exedoc.config.MailImpl;

/**
 * @author Administrator
 */
@Local
public interface AdministradorConfiguracion {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * 
	 */
	void guardarConfiguracion();

	/**
	 * 
	 */
	void actualizar();

	/**
	 * @return MailImpl
	 */
	MailImpl getMail();

}
