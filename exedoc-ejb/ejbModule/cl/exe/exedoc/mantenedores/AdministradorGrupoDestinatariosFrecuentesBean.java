package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link AdministradorGrupoDestinatariosFrecuentes}.
 */
@Stateful
@Name("administradorGrupoDestinatariosFrecuentes")
@Scope(value = ScopeType.SESSION)
public class AdministradorGrupoDestinatariosFrecuentesBean implements AdministradorGrupoDestinatariosFrecuentes {

	private static final String ID = "id";
	private static final String DEBE_SELECCIONAR_AL_MENOS_UNA_PERSONA = "Debe seleccionar al menos una persona";
	private Long organizacion;
	private Long idDivision;
	private Long idDepto;
	private Long idUnidad;
	private Long idCargo;
	private Long idPersona;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDeptos;
	private List<SelectItem> listUnidades;
	private List<SelectItem> listCargos;
	private List<SelectItem> listPersonas;

	private List<Persona> listDestinatarios;

	private int flag;
	private boolean creado;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = true)
	private Persona usuario;

	/**
	 * Contructor.
	 */
	public AdministradorGrupoDestinatariosFrecuentesBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("Iniciando modulo...");
		creado = false;
		this.buscarPersonas();

		listDestinatarios = this.buscarDestinatariosFrecuentes();
		if (listDestinatarios.isEmpty()) {
			flag = 0;
		} else {
			flag = 1;
		}

		this.cargarListas();

		return "administradorGrupoDestinatariosFrecuentes";
	}

	/**
	 * Metodo que carga las listas utilizadas al inicializar.
	 */
	private void cargarListas() {
		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		idDivision = (Long) listDivisiones.get(1).getValue();
//		this.buscarDeptos();
//		listDeptos = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setIdDepto(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidades();
	}

	/**
	 * @return Set<Persona>
	 */
	private List<Persona> buscarDestinatariosFrecuentes() {
		final DestinatariosFrecuentes miGrupo = this.buscarGrupo();

		if (miGrupo == null) {
			return new ArrayList<Persona>();
		} else {
			final List<Persona> listPersonasDeMiGrupo = miGrupo.getDestinatariosFrecuentes();
			return listPersonasDeMiGrupo;
		}
	}

	/**
	 * @return DestinatariosFrecuentes
	 */
	private DestinatariosFrecuentes buscarGrupo() {
		DestinatariosFrecuentes miGrupo;
		try {
			final StringBuffer miGrupoJPQL = new StringBuffer("SELECT ldf FROM DestinatariosFrecuentes ldf WHERE ");
			miGrupoJPQL.append("ldf.propietario.id = ? and ldf.esGrupo = true");
			final Query miGrupoQuery = em.createQuery(miGrupoJPQL.toString());
			miGrupoQuery.setParameter(1, usuario.getId());
			miGrupo = (DestinatariosFrecuentes) miGrupoQuery.getSingleResult();
			creado = true;
		} catch (NoResultException nre) {
			miGrupo = null;
		}

		return miGrupo;
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarPersona();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarPersona();
		organizacion = idOrganizacion;
		listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void limpiarPersona() {
		this.organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;
		idDepto = JerarquiasLocal.INICIO;
		idUnidad = JerarquiasLocal.INICIO;
		idCargo = JerarquiasLocal.INICIO;
		idPersona = JerarquiasLocal.INICIO;

		listDivisiones = new ArrayList<SelectItem>();
		listDeptos = new ArrayList<SelectItem>();
		listUnidades = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		listPersonas = new ArrayList<SelectItem>();

		listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDeptos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDeptos() {
		final List<Division> divisiones = em.createNamedQuery("Division.findById").setParameter(ID, idDivision)
				.getResultList();
		boolean conCargo = false;

		if (divisiones != null && divisiones.size() == 1) {
			final Division division = divisiones.get(0);
			if (division.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDivision(idDivision);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDeptos = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		if (listUnidades.size() == 0) {
			listUnidades.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidades.clear();
		listUnidades = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, idDivision);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidades() {
		final List<Departamento> deptos = em.createNamedQuery("Departamento.findById").setParameter(ID, idDepto)
				.getResultList();
		boolean conCargo = false;

		if (deptos != null && deptos.size() == 1) {
			final Departamento depto = deptos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				idUnidad = jerarquias.getIdUnidadVirtualDepartamento(idDivision, idDepto);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidades = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL, idDepto);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, idUnidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, idCargo);
		if (listPersonas.size() == 1) {
			idPersona = (Long) listPersonas.get(0).getValue();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void agregarPersona() {
		Boolean estado = Boolean.TRUE;
		if (idPersona.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Debe seleccionar la Persona.");
			estado = Boolean.FALSE;
		}
		if (estado) {
			try {
				if (!idCargo.equals(JerarquiasLocal.INICIO)) {
					if (idPersona.equals(JerarquiasLocal.TODOS)) {
						listDestinatarios.addAll(em.createNamedQuery("Persona.findById").setParameter(ID, idCargo)
								.getResultList());
					} else if (idPersona.equals(JerarquiasLocal.CUALQUIERA)) {
						final List<Persona> personasTmp = em.createQuery("select p from Persona p where p.cargo.id = ?")
								.setParameter(1, idCargo).getResultList();
						for (Persona p : personasTmp) {
							p.setDestinatarioConRecepcion(true);
							listDestinatarios.add(p);
						}
					} else {
						final Persona p = (Persona) em.createQuery("select p from Persona p where p.id = :param")
								.setParameter("param", idPersona).getSingleResult();
						p.setDestinatarioConCopia(true);
						if(listDestinatarios.contains(p)){
							FacesMessages.instance().add("La persona que intenta agregar, ya se encuentra, por favor seleccione otra.");
						}else{
							listDestinatarios.add(p);
						}
					}
				}
				if (listDestinatarios.contains(usuario)) {
					FacesMessages.instance().add("Debe seleccionar un usuario distinto al que se encuentra en sesión.");
					listDestinatarios.remove(usuario);
				}
				this.cargarListas();
			} catch (Exception e) {
				log.info("Cargando datos...", e);
			} 
		}
	}

	@Override
	public boolean renderedBotonAgregarPersona() {
		return true;
	}

	@Override
	public void limpiarFormulario() {
		listOrganizacion.clear();
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		idDivision = (Long) listDivisiones.get(1).getValue();
		this.buscarDeptos();
	}

	@Override
	public void eliminarPersona(final Persona persona) {
		listDestinatarios.remove(persona);
	}

	@Override
	public void crearGrupo() {
		if (listDestinatarios.isEmpty()) {
			FacesMessages.instance().add(DEBE_SELECCIONAR_AL_MENOS_UNA_PERSONA);
		} else {
			final List<Persona> destinatariosAPersistir = new ArrayList<Persona>();
			final DestinatariosFrecuentes grupo = new DestinatariosFrecuentes();
			final Persona propietario = em.find(Persona.class, usuario.getId());
			grupo.setPropietario(propietario);
			grupo.setNombreLista("Grupo Destinatarios Frecuentes");
			grupo.setEsGrupo(true);
			for (Persona p : listDestinatarios) {
				destinatariosAPersistir.add(p);
			}
			grupo.setDestinatariosFrecuentes(destinatariosAPersistir);
			em.persist(grupo);

			final List<DestinatariosFrecuentes> grupoUsuario = new ArrayList<DestinatariosFrecuentes>();
			if (propietario.getDestinatariosFrecuentes() == null) {
				grupoUsuario.add(grupo);
				propietario.setDestinatariosFrecuentes(grupoUsuario);
			} else {
				propietario.getDestinatariosFrecuentes().add(grupo);
			}

			this.limpiarFormulario();
			this.cargarListas();
			flag = 1;
			creado = true;

			FacesMessages.instance().add("Grupo creado exitósamente");
		}
	}

	@Override
	public void guardarGrupo() {
//		if (listDestinatarios.isEmpty()) {//posible causa para eliminar lista completa
//			FacesMessages.instance().add(DEBE_SELECCIONAR_AL_MENOS_UNA_PERSONA);
//		} else {
			final List<Persona> destinatariosModificar = new ArrayList<Persona>();
			final DestinatariosFrecuentes grupo = this.buscarGrupo();
			for (Persona p : listDestinatarios) {
				destinatariosModificar.add(p);
			}

			if(grupo.getDestinatariosFrecuentes().size() == listDestinatarios.size()){
				
				FacesMessages.instance().add("No se ha modificado el Grupo");
				
			}else{
			
			if (!grupo.getDestinatariosFrecuentes().equals(destinatariosModificar)) {
				grupo.setDestinatariosFrecuentes(destinatariosModificar);
			}
			grupo.setEsGrupo(true);
			em.merge(grupo);

			this.limpiarFormulario();
			this.cargarListas();

			FacesMessages.instance().add("Grupo modificado exitósamente");
//		}
		}
	}

	@Override
	@End
	public String end() {
		listDestinatarios = null;
		log.info("Finalizando modulo...");
		return "home";
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public Long getIdDivision() {
		return idDivision;
	}

	@Override
	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	@Override
	public Long getIdDepto() {
		return idDepto;
	}

	@Override
	public void setIdDepto(final Long idDepto) {
		this.idDepto = idDepto;
	}

	@Override
	public Long getIdUnidad() {
		return idUnidad;
	}

	@Override
	public void setIdUnidad(final Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	@Override
	public Long getIdCargo() {
		return idCargo;
	}

	@Override
	public void setIdCargo(final Long idCargo) {
		this.idCargo = idCargo;
	}

	@Override
	public Long getIdPersona() {
		return idPersona;
	}

	@Override
	public void setIdPersona(final Long idPersona) {
		this.idPersona = idPersona;
	}

	@Override
	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	@Override
	public void setListDivisiones(final List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	@Override
	public List<SelectItem> getListDeptos() {
		return listDeptos;
	}

	@Override
	public void setListDeptos(final List<SelectItem> listDeptos) {
		this.listDeptos = listDeptos;
	}

	@Override
	public List<SelectItem> getListUnidades() {
		return listUnidades;
	}

	@Override
	public void setListUnidades(final List<SelectItem> listUnidades) {
		this.listUnidades = listUnidades;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public void setListCargos(final List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<Persona> getListDestinatarios() {
		return listDestinatarios;
	}

	@Override
	public void setListDestinatarios(final List<Persona> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	@Override
	public void setFlag(final int flag) {
		this.flag = flag;
	}

	@Override
	public boolean getCreado() {
		return this.creado;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
}
