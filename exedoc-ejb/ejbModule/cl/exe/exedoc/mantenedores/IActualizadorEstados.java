package cl.exe.exedoc.mantenedores;

import javax.ejb.Local;
import javax.ejb.Remove;

import org.jboss.seam.annotations.Destroy;

/**
 * Esta interfaz debe ser implementada para cuando se actualice estados de otros sistemas;.
 */
@Local
public interface IActualizadorEstados {

	int actualizarEstado(int nuevoEstado, int tipoOperacion, String nroDecreto, String sRutUsuario, int p_ano,
			String p_observacion, String sFecha);

	@Destroy
	@Remove
	void destroy();

}
