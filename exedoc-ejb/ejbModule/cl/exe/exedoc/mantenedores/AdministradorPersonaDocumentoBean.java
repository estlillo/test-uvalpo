package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

/**
 * Implementacion {@link AdministradorPersonaDocumento}.
 */
@Stateful
@Name("administradorPersonaDocumento")
@Scope(value = ScopeType.SESSION)
public class AdministradorPersonaDocumentoBean implements AdministradorPersonaDocumento {

	private static final String ERROR = "Error";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	/**
	 * Constructor.
	 */
	public AdministradorPersonaDocumentoBean() {
		super();
	}

	@Override
	@Remove
	public void remove() {
	}

	@Override
	@Destroy
	public void destroy() {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean existePersona(final Long idDocumento, final Long idPersona) {

		final String q = "SELECT * FROM personas_documento "
				+ "WHERE id_documento = :idDocumento AND id_destinatario = :idPersona";

		final Query query = em.createNativeQuery(q);

		query.setMaxResults(1);
		query.setParameter("idDocumento", idDocumento);
		query.setParameter("idPersona", idPersona);

		List result = new ArrayList();
		try {
			result = query.getResultList();
		} catch (NoResultException e) {
			log.error(ERROR, e);
		}

		if (result.size() > 0) { return true; }
		return false;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void deleteAllItem(final Long idDocumento) {
		
		final String q = "DELETE FROM personas_documento WHERE id_documento = :idDocumento";
		
		final Query query = em.createNativeQuery(q);
		
		query.setParameter("idDocumento", idDocumento);
		
		try {
			query.executeUpdate();
		} catch (Exception e) {
			log.error(ERROR, e);
		}
	}

}
