package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;

/**
 * @author Administrator
 */
@Local
public interface AdministradorGrupoUsuario {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	
	/**
	 * 
	 */
	void buscarGrupos();

	/**
	 * 
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * @return List<Persona>
	 */
	List<Persona> getListUsuarios();
	
	/**
	 * @param List<Persona>
	 */
	void setListUsuarios(List<Persona> usuarios);


	/**
	 * @return Long
	 */
	Departamento getUnidadOrganizacional();

	/**
	 * @param unidadOrganizacional Long
	 */
	void setUnidadOrganizacional(Departamento unidadOrganizacional);

	/**
	 * @return String
	 */
	String getNombre();

	/**
	 * @param nombre String
	 */
	void setNombre(String nombre);

	List<SelectItem> getListGrupos();

	void setListGrupos(List<SelectItem> listGrupos);

	GrupoUsuario getGrupoUsuario(Long id);

}
