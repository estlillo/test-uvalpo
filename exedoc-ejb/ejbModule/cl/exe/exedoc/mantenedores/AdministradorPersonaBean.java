package cl.exe.exedoc.mantenedores;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.jboss.seam.util.Base64;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoFirma;
import cl.exe.exedoc.session.exception.RUTException;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.RutUtil;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * Implementacion de {@link AdministradorPersona}. TODO: COMPLEXITY
 */
@Stateful
@Name("administradorPersona")
@Scope(value = ScopeType.SESSION)
public class AdministradorPersonaBean implements AdministradorPersona {

	private static final String ID = "id";
	private Persona persona;
	private List<Persona> personas;

	@Out(required = false, scope = ScopeType.SESSION)
	@In(required = false, scope = ScopeType.SESSION)
	private String user;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidadesOrganizacionales;
	private List<SelectItem> listCargos;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;// -1
	private static final String INICIO = "<<Seleccionar>>";

	// Datos KeyStore
	private byte[] keyStore;

	private Integer tipoFirma;

	@Logger
	private Log log;

	/**
	 * Constructor.
	 */
	public AdministradorPersonaBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {
		listOrganizacion = new ArrayList<SelectItem>();
		listDivision = new ArrayList<SelectItem>();
		listDepartamento = new ArrayList<SelectItem>();
		listUnidadesOrganizacionales = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		personas = new ArrayList<Persona>();
		user = "";
		persona = new Persona();
		tipoFirma = 1;
		listOrganizacion = this.buscarOrganizaciones();
		
		/*22-06-2012 Sergio: Se reactivan todos los niveles de ExeDoc*/
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
//		listDepartamento = jerarquias.getDepartamentos(INICIO);
//		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional()
//				.getDepartamento().getId());
//		this.buscarUnidadesOrganizacionales();
		/**/
		
		return "administradorPersona";
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUsuario() {
		final StringBuilder sql = new StringBuilder(
				"SELECT p FROM Persona p WHERE ");
		String nombre = persona.getNombres();
		String apellido = persona.getApellidoPaterno();

		final String strand = "%') and ";

		if (user != null && user.length() != 0) {
			sql.append("upper(p.usuario) like upper('%").append(user)
					.append(strand);
		}
		if (nombre != null && nombre.length() != 0) {
			sql.append("upper(p.nombres) like upper('%").append(nombre)
					.append(strand);
		}
		if (apellido != null && apellido.length() != 0) {
			sql.append("upper(p.apellidoPaterno) like upper('%")
					.append(apellido).append(strand);
		}
		sql.append(" (p.externo is null or p.externo = false) and ");
		sql.append(" 1 = 1 ");

		final Query query = em.createQuery(sql.toString());
		personas = query.getResultList();
		user = "";
		nombre = "";
		apellido = "";
	}

	/*
	 * Metodo permite guardar/modificar una Persona. TODO: EXCEPTION/RETURN
	 */
	@Override
	public void guardarPersona() {
		final boolean existe = persona.getId() != null;
		Long idPersona = null;
		String rutAnterior = null;
		this.eliminarBlancos(persona);
		try {
			RutUtil rutFormateado = new RutUtil((String) persona.getRut());
			persona.setRut(rutFormateado.toString());
		} catch (RUTException e1) {
			FacesMessages.instance().add("RUT inv&aacute;lido.");
			return;
		}
		if (!existe) {
			log.info("Id Persona : {0}", persona.getIdPersona());
			final Cargo cargoPersona = em.find(Cargo.class, cargo);
			persona.setCargo(cargoPersona);
			if (persona.getVigente()) {
				persona.setFechaNoVigente(null);
			}
			persona.setKeyStore(keyStore);
			persona.setTipoFirma((tipoFirma == 1) ? new TipoFirma(
					TipoFirma.SIMPLE) : new TipoFirma(TipoFirma.AVANZADA));
			persona.setExterno(false);
			
		} else {
			// Persona.getId() no es null
			final String consulta = "SELECT p FROM Persona p WHERE p.id ="
					+ persona.getId();
			final Query query = em.createQuery(consulta);
			idPersona = new Long(0);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (Exception e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				idPersona = p.getId();
				rutAnterior = p.getRut();
			}
			if (persona.getVigente()) {
				persona.setFechaNoVigente(null);
			} else if (persona.getFechaNoVigente() == null) {
				persona.setFechaNoVigente(new Date());
			}

			persona.setTipoFirma((tipoFirma == 1) ? new TipoFirma(
					TipoFirma.SIMPLE) : new TipoFirma(TipoFirma.AVANZADA));
			persona.setCargo(em.find(Cargo.class, cargo));
			if (this.keyStore != null) {
				persona.setKeyStore(this.keyStore);
			}
		}
		boolean valido = true;
		if (!this.validarRutNoRegistrado(persona.getRut(), existe, idPersona)) {
			FacesMessages.instance().add("RUT ya registrado.");
			valido = false;
			return;
		}
		
		if(!this.validaNombres(persona.getNombres())){
			FacesMessages.instance().add("El campo \"Nombres\" no puede superar los 99 caracteres.");
			valido = false;
//			return;
		}
		
		if(!this.validaNombres(persona.getApellidoPaterno())){
			FacesMessages.instance().add("El campo \"Apellido Paterno\" no puede superar los 99 caracteres.");
			valido = false;
//			return;
		}
		
		if(!this.validaNombres(persona.getApellidoMaterno())){
			FacesMessages.instance().add("El campo \"Apellido Materno\" no puede superar los 99 caracteres.");
			valido = false;
//			return;
		}
		
		if(!this.validaUsuarios(persona.getUsuario())){
			FacesMessages.instance().add("El campo \"Usuarios\" no puede superar los 255 caracteres.");
			valido = false;
//			return;
		}
		
		if(!this.validaUsuarios(persona.getIniciales())){
			FacesMessages.instance().add("El campo \"Iniciales\" no puede superar los 255 caracteres.");
			valido = false;
//			return;
		}
		
		if (this.departamento.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Departamento no ingresado");
			valido = false;
			return;
		}
		
		if (this.unidadOrganizacional.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Unidad no ingresado");
			valido = false;
			return;
		}
		
		if (this.cargo.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add("Cargo no ingresado");
			valido = false;
			return;
		}
		// if ((!existe && persona.getIdPersona() != null) || existe) {
		// if (!this.validarIdPersonaRegistrada(persona.getIdPersona(), existe,
		// idPersona)) {
		// FacesMessages.instance().add("Id persona ya registrado.");
		// return;
		// }
		// }
		if (!this.validarUsuarioRegistrado(persona.getUsuario(), existe,
				idPersona)) {
			FacesMessages.instance().add(
					"Nombre de usuario " + persona.getUsuario()
							+ " ya registrado.");
			return;
		}
		
		if (rutAnterior != null && !rutAnterior.equals(persona.getRut()))
			if (!this.validarRutRegistrado(rutAnterior, existe, idPersona)) {
				FacesMessages.instance().add(
						"El Rut ingresado ya se encuentra registrado: #0.", persona.getRut());
				return;
			}

		if (!existe && valido) {
			em.persist(persona);
			FacesMessages.instance().add("Se agregó el usuario al sistema.");
		} else {
			if(valido){
				em.merge(persona);
				FacesMessages.instance().add(
						"Se modificó el usuario en el sistema.");
			}
		}
		// personas = null;
		persona = null;
		// listOrganizacion = this.buscarOrganizaciones();
		// organizacion = (Long) listOrganizacion.get(1).getValue();
		// this.buscarDivisiones();
		// limpiarDestinatario();
	}

	/**
	 * Método para validar la cantidad de caracteres posibles a ingresar en los campos nombres, apellido paterno y apellido materno.
	 * 
	 * largo máximo 100 caracteres.
	 * 
	 * @param nombres
	 * @return
	 */
	public boolean validaNombres(String nombres){
		boolean estado = true;
		if(nombres.trim().length()>=99){
			estado = false;
		}
		return estado;
	}
	
	/**
	 * Método para validar la cantidad de caracteres posibles a ingresar en el campo usuario e iniciales.
	 * 
	 * largo maximo 255 caracteres.
	 * 
	 * @param usuario
	 * @return
	 */
	public boolean validaUsuarios(String usuario){
		boolean estado = true;
		if(usuario.trim().length()>=255){
			estado = false;
		}
		return estado;
	}
	/*
	 * (non-Javadoc) TODO: EXCEPTION/RETURN
	 */
	@Override
	public void importarPersona() {
		// Obtener RUT.
		// Normalizar RUT
		// Verificar si RUT existe
		// Si existe preguntar si desea actualizar la informacion
		// Si respuesta anterior es OK
		// Invocar Interfaz que llena persona.

		final String prmRut = persona.getRut();

		if (prmRut == null) {
			FacesMessages.instance().add("RUT : vacio ");
			return;
		}

		final ImportadorUsuario iiUsuario = FactoryImportadorUsuario
				.getImportadorUsuarios();

		final String sRut = iiUsuario.normalizarRUT(prmRut);

		if (!this.validarRutNoRegistrado(sRut, false, null)) {

			FacesMessages.instance().add("RUT ya registrado ");
			return;
		}
		Integer res = null;

		try {
			res = iiUsuario.getPersona(sRut, persona, em);
		} catch (Exception e) {
			FacesMessages.instance().add("Error al buscar usuario.");
			return;
		}
		if (res != null) {
			FacesMessages.instance().add(
					"Error al importar usuario desde el sistema de origen.");
		}

		if (persona != null && persona.getCargo() != null) {

			cargo = persona.getCargo().getId();
		}
	}

	@Override
	public void listener(final UploadEvent event) throws Exception {
		final UploadItem item = event.getUploadItem();
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		keyStore = data;
		persona.setKeyStore(keyStore);
	}

	@Override
	public void listenerImagenFirma(final UploadEvent event) throws Exception {
		final UploadItem item = event.getUploadItem();
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		persona.setImagenFirma(data);
	}

	@Override
	public void verImagenFirma(final OutputStream output, final Object object)
			throws IOException {
		if (persona.getImagenFirma() != null) {
			output.write(persona.getImagenFirma());
		}
	}

	@Override
	public long getTime() {
		return (new Date()).getTime();
	}

	/**
	 * TODO: RETURN/EXCEPTION
	 * 
	 * @param usuario
	 *            String
	 * @param editar
	 *            boolean
	 * @param idPersona
	 *            Long
	 * @return boolean
	 */
	private boolean validarUsuarioRegistrado(final String usuario,
			final boolean editar, final Long idPersona) {
		if (persona.getUsuario() != null && !persona.getUsuario().isEmpty()) {
			StringBuilder consulta = new StringBuilder();
			consulta.append("select p from Persona p where upper(p.usuario) = upper('");

			if (!editar) { 
				consulta.append(persona.getUsuario() + "') ");
			} else {
				consulta.append(persona.getUsuario()
						+ "') and p.id not in (" + persona.getId() + ") ");
			}
			final Query query = em.createQuery(consulta.toString());
			Object o;
			try {
				o = query.getSingleResult();
			} catch (Exception e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
//					FacesMessages
//							.instance()
//							.add("El Usuario ingresado ya se encuentra registrado: #0",
//									p.getUsuario());
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Metodo validar Rut Registrado.
	 * 
	 * @param rut
	 *            {@link String}
	 * @param editar
	 *            {@link Boolean}
	 * @param idPersona
	 *            {@link Long}
	 * @return {@link Boolean}
	 */
	private boolean validarRutRegistrado(final String rut,
			final boolean editar, final Long idPersona) {
		Boolean estado = Boolean.TRUE.booleanValue();
		if (rut != null && !"".equals(rut)) {
			String consulta = null;
			if (!editar) {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "')";
			} else {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "') ";
				if (idPersona != null)
					consulta+= "and p.id not in (" + idPersona + ")";
			}
			final Query query = em.createQuery(consulta);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (NoResultException e) {
				o = null;
			} catch (NonUniqueResultException e) {
				o = null;
				estado = Boolean.FALSE.booleanValue();
			} catch (IllegalStateException e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
//					FacesMessages.instance().add(
//							"El Rut ingresado ya se encuentra registrado: #0",
//							p.getRut());
					estado = Boolean.FALSE;
				}
			}
		} else {
			estado = Boolean.FALSE.booleanValue();
		}
		return estado;
	}

	/**
	 * Metodo validar Rut No Registrado. TODO: CYCLOMATIC
	 * 
	 * @param rut
	 *            {@link String}
	 * @param editar
	 *            {@link Boolean}
	 * @param idPersona
	 *            {@link Long}
	 * @return {@link Boolean}
	 */
	private boolean validarRutNoRegistrado(final String rut,
			final boolean editar, final Long idPersona) {
		Boolean estado = Boolean.TRUE.booleanValue();
		if (rut != null && !"".equals(rut)) {
			String consulta = null;
			if (!editar) {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "')";
			} else {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "') and p.id not in (" + idPersona + ")";
			}
			final Query query = em.createQuery(consulta);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (NoResultException e) {
				o = null;
			} catch (NonUniqueResultException e) {
				o = null;
				estado = Boolean.FALSE.booleanValue();
			} catch (IllegalStateException e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
//					FacesMessages.instance().add(
//							"El Rut ingresado ya se encuentra registrado: #0",
//							p.getRut());
					estado = Boolean.FALSE;
				}
			}
		} else {
			estado = Boolean.FALSE.booleanValue();
		}
		return estado;
	}

	/**
	 * TODO: EXCEPTION/RETURN.
	 * 
	 * @param idPersona
	 *            Long
	 * @param editar
	 *            boolean
	 * @param idPersonaExistente
	 *            Long
	 * @return boolean
	 */
	private boolean validarIdPersonaRegistrada(final Long idPersona,
			final boolean editar, final Long idPersonaExistente) {
		if (persona.getIdPersona() != null) {
			String consulta = "select p from Persona p where p.idPersona= '";
			if (!editar) {
				consulta += idPersona + "'";
			} else {
				consulta += idPersona + "' and p.id not in ("
						+ idPersonaExistente + ")";
			}
			log.info(consulta);
			final Query query = em.createQuery(consulta);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (Exception e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
					FacesMessages.instance().add(
							"El Id ingresado ya se encuentra registrado: #0",
							p.getIdPersona());
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division mDivision = divisiones.get(0);
			if (mDivision.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento mDepartamento = departamentos.get(0);
			if (mDepartamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
	}

	@Override
		
	public String limpiarFormulario() {
		begin();
		persona = new Persona();
		return "administradorPersona";
//		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		personas = new ArrayList<Persona>();
//		persona = new Persona();
//		tipoFirma = -1;
	}

	/**
	 * 
	 */
	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));

	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarPersonas() {
		if (!cargo.equals(JerarquiasLocal.INICIO)) {
			final Query query = em
					.createQuery("Select p From Persona p Where p.cargo.id = ?");
			query.setParameter(1, cargo);
			personas = query.getResultList();
		}
	}

	@Override
	public void modificar(final Long idPersona) {

		// Se realiza la busqueda de la Persona por medio del usuario
		persona = em.find(Persona.class, idPersona);

		listOrganizacion = this.buscarOrganizaciones();
		organizacion = persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getOrganizacion().getId();
		this.buscarDivisiones();
		division = persona.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getId();
		this.buscarDepartamentos();
		//listDepartamento = jerarquias.getDepartamentos(INICIO);
		departamento = persona.getCargo().getUnidadOrganizacional()
				.getDepartamento().getId();
		this.buscarUnidadesOrganizacionales();
		unidadOrganizacional = persona.getCargo().getUnidadOrganizacional()
				.getId();
		this.buscarCargos();
		cargo = persona.getCargo().getId();

		//
		// // Se obtiene jerarquia a la cual pertenece aquella persona.
		// final Cargo c = persona.getCargo();
		//
		// // Si no tiene cargos asociados, entonces no se busca nada asociado
		// al
		// // cargo (jerarquia)
		// if (c == null) {
		// return;
		// }
		//
		// final UnidadOrganizacional uni = c.getUnidadOrganizacional();
		// final Departamento dep = uni.getDepartamento();
		// final Division div = dep.getDivision();
		// final Organizacion org = div.getOrganizacion();
		//
		// // Se setea la organizacion correspondiente y se carga la lista de
		// // Divisiones.
		// organizacion = org.getId();
		// this.buscarDivisiones();
		//
		// // Se setea la division correspondiente y se carga la lista de
		// Deptos.
		// division = div.getId();
		// this.buscarDepartamentos();
		//
		// // Se setea el Depto correspondiente y se carga la lista de Unidades.
		// departamento = dep.getId();
		// this.buscarUnidadesOrganizacionales();
		//
		// // Se setea la Unidad correspondiente y se carga la lista de Cargos.
		// unidadOrganizacional = uni.getId();
		//
		// this.buscarCargos();
		//
		// cargo = c.getId();

		final TipoFirma tf = persona.getTipoFirma();
		if (tf != null) {
			tipoFirma = persona.getTipoFirma().getId();
		} else {
			tipoFirma = 1;
		}

	}

	@Override
	public Persona getPersona() {
		if (persona == null) {
			persona = new Persona();
		}
		return persona;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public List<Persona> getPersonas() {
		return personas;
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public String getUser() {
		return user;
	}

	@Override
	public void setUser(final String user) {
		this.user = user;
	}

	@Override
	public byte[] getKeyStore() {
		return keyStore;
	}

	@Override
	public void setKeyStore(final byte[] keyStore) {
		this.keyStore = keyStore;
	}

	@Override
	public void actualizar() {

	}

	@Override
	public Integer getTipoFirma() {
		return tipoFirma;
	}

	@Override
	public void setTipoFirma(final Integer tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getPersonas(String buscado, List<SelectPersonas> list) {

		final StringBuilder sql = new StringBuilder(
				"SELECT p FROM Persona p WHERE ");

		final String strand = "%')";

		if (buscado != null && buscado.length() != 0) {
			sql.append("upper(p.nombres) like upper('").append(buscado)
					.append(strand);
			if (list.size() > 0) {
				sql.append(" AND p.id NOT IN (");

				int i = 0;
				for (SelectPersonas persona : list) {
					if (i != 0)
						sql.append(persona.getPersona().getId() + ",");
					else
						sql.append(persona.getPersona().getId());
				}
				sql.append(")");
			}
		}

		final Query query = em.createQuery(sql.toString());

		personas = query.getResultList();
		return personas;
	}
	
	@Override
	public void eliminarBlancos(Persona persona) {
		persona.setRut(persona.getRut().trim());
		persona.setNombres(persona.getNombres().trim());
		persona.setApellidoPaterno(persona.getApellidoPaterno().trim());
		persona.setApellidoMaterno(persona.getApellidoMaterno().trim());
		persona.setEmail(persona.getEmail().trim());
		persona.setUsuario(persona.getUsuario().trim());
		persona.setIniciales(persona.getIniciales().trim());
	}
	
	@Override
	public String convertImage(byte[] data) {
		if (data != null) {
			BufferedImage image = null;
	        byte[] imageByte;
	        try {
//	            BASE64Decoder decoder = new BASE64Decoder();
//	            imageByte = decoder.decodeBuffer(imageString);
	            ByteArrayInputStream bis = new ByteArrayInputStream(data);
	            image = ImageIO.read(bis);
	            bis.close();
	            ByteArrayOutputStream bos = new ByteArrayOutputStream();
	            ImageIO.write(image, "JPG", bos);
	            return "data:image/jpeg;base64," + Base64.encodeBytes(bos.toByteArray());
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
		}
		return null;
	}
}
