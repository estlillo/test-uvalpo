package cl.exe.exedoc.mantenedores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Alerta;
import cl.exe.exedoc.mantenedores.dao.AdministradorAlertas;

/**
 * @author Pablo
 * 
 */
@Stateful
@Name(MantenedorAlertasBean.MANTENEDOR_ALERTAS)
@Scope(ScopeType.APPLICATION)
public class MantenedorAlertasBean implements MantenedorAlertas, Serializable {

	/**
	 * 
	 */
	public static final String MANTENEDOR_ALERTAS = "mantenedorAlertas";

	/**
	 * 
	 */
	private static final long serialVersionUID = 7867335858043024551L;

	private static final String OPERACIONREALIZADA = "Operación realizada correctamente.";
	private static final int LARGO_MAX = 200;

	@EJB
	private AdministradorAlertas administradorAlertas;

	private Alerta alerta;
	private List<Alerta> listAlertas;

	/**
	 * Constructor.
	 */
	public MantenedorAlertasBean() {

		super();
	}

	@Remove
	@Override
	public void remove() {
	}

	@Destroy
	@Override
	public void destroy() {
	}

	@Override
	public String begin() {

		this.cargarAlertas();
		this.setAlerta(new Alerta());

		return MANTENEDOR_ALERTAS;
	}

	@Override
	public void guardarAlerta() {

		final boolean status;
		 int cont = this.getAlerta().getNombre().trim().length();
		 
		 Long plzAct = new Long("0");
		 Long PlzAnt = new Long("1");
		 String nomAnt = "0";
		 String nombreAct = "1";
		 
		 if (this.getAlerta().getId() != null) {
			 
			  nombreAct = getAlerta().getNombre().trim();
			  plzAct = getAlerta().getPlazo();
			 Alerta id = this.getAlerta();
			 
			 Long ids = this.getAlerta().getId();
			 this.setAlerta(administradorAlertas.buscarAlerta(ids));
			 Alerta alert = getAlerta();		 
			 nomAnt= alert.getNombre().trim();
			 PlzAnt = alert.getPlazo();
			 this.setAlerta(id);
		 }	
		
		if (cont < LARGO_MAX) {
			if (administradorAlertas.validarNombre(alerta)) {
		
				if (this.getAlerta().getId() == null) {
					status = administradorAlertas.crear(alerta);
				} else {
					status = administradorAlertas.actualizar(alerta);
				}
		
				if (status) {				
					this.cargarAlertas();
					this.setAlerta(new Alerta());
					if(nombreAct.equals(nomAnt) && plzAct.equals(PlzAnt)){
						FacesMessages.instance().add("No se realizaron cambios");
					}else
					FacesMessages.instance().add(OPERACIONREALIZADA);
				} else {
					FacesMessages.instance().add(
							"Ha ocurrido un error al guardar la alerta.");
				}
			} else {
				FacesMessages.instance().add("Ya existe ese nombre para una alerta");
			}
		} else {
			FacesMessages.instance().add(
					"El largo del nombre no debe ser mayor a 200 caracteres.");
		}
	}

	@Override
	public void eliminarAlerta(final Long id) {

		if (administradorAlertas.eliminar(id)) {
			this.cargarAlertas();
			FacesMessages.instance().add(OPERACIONREALIZADA);
		} else {
			FacesMessages.instance().add(
					"Ha ocurrido un error al eliminar la alerta.");
		}
	}

	@Override
	public void editarAlerta(final Long id) {

		this.setAlerta(administradorAlertas.buscarAlerta(id));

		if (this.getAlerta() == null || this.getAlerta().getId() == null) {
			FacesMessages.instance().add(
					"Ha ocurrido un error al intentar editar.");
		} /*else {
			FacesMessages.instance().add(OPERACIONREALIZADA);
		}*/
	}

	/**
	 * 
	 */
	private void cargarAlertas() {

		this.setListAlertas(administradorAlertas.buscarAlerta());
		if (this.getListAlertas() == null) {
			this.setListAlertas(new ArrayList<Alerta>());
		}
	}


	@Override
	public Alerta getAlerta() {
		return alerta;
	}

	@Override
	public void setAlerta(final Alerta alerta) {
		this.alerta = alerta;
	}

	@Override
	public List<Alerta> getListAlertas() {
		return listAlertas;
	}

	@Override
	public void setListAlertas(final List<Alerta> listAlertas) {
		this.listAlertas = listAlertas;
	}
	
	@Override
	public void pruebaNotificaciones() {
		administradorAlertas.obtenerDocumetosYEnviarAlerta(new Date());
	}

}
