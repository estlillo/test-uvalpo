package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Class implements {@link AdministradorUnidades}.
 * 
 * @author 
 *
 */
@Stateful
@Name("adminUnidades")
@Scope(value = ScopeType.SESSION)
public class AdministradorUnidadesBean implements AdministradorUnidades {

	private static final String MSG_INGRESAR_DPTO       = "Debe ingresar el Departamento.";
	private final static String ERROR_DUPLICIDAD_DATOS  = "No se permite repetir nombre";

	private String nombreUnidad;

	private Long organizacion;
	private Long organizacionCargar;
	private Long idDivision;
	private Long idDepto;
	private Long idDivisionCargar;
	private Long idDeptoCargar;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listOrganizacionCargar;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDeptos;
	private List<SelectItem> listDivisionesCargar;
	private List<SelectItem> listDeptosCargar;

	private List<UnidadOrganizacional> listUnidades;

	private Long idUltimaUnidadCargada;
	private Integer flag;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@In(required = false)
	private Persona usuario;
	
	/**
	 * {@link EntityManager}.
	 */
	@PersistenceContext
	private EntityManager em;
	
	/**
	 * Constructor.
	 */
	public AdministradorUnidadesBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		this.log.info("Iniciando modulo...");

		this.nombreUnidad = "";
		this.listDivisiones = new ArrayList<SelectItem>();
		this.listUnidades = new ArrayList<UnidadOrganizacional>();

		this.listOrganizacion = this.buscarOrganizaciones();
		this.organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		this.idDivision = (Long) listDivisiones.get(1).getValue();
		this.buscarDeptos();
		this.listOrganizacionCargar = this.buscarOrganizacionesCargar();
		this.organizacionCargar = (Long) listOrganizacionCargar.get(1).getValue();
		this.buscarDivisionesCargar();
		this.idDivisionCargar = (Long) listDivisionesCargar.get(1).getValue();
		this.buscarDeptosCargar();

		listDeptos = jerarquias.getDepartamentos("<<Seleccionar>>");
		listDeptosCargar = jerarquias.getDepartamentos("<<Seleccionar>>");
        this.setIdDepto(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.setIdDeptoCargar(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.buscarUnidades();
		
		this.flag = 0;
		this.idUltimaUnidadCargada = null;

		return "adminUnidades";
	}

	/**
	 * Metodo que busca las organizaciones.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarUnidad();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * Metodo que busca las organizaciones.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizacionesCargar() {
		this.limpiarUnidadCargar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final Long idOrganizacion = this.organizacion;
		this.limpiarUnidad();
		this.organizacion = idOrganizacion;
		this.listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDivisionesCargar() {
		final Long idOrganizacion = organizacionCargar;
		this.limpiarUnidadCargar();
		this.organizacionCargar = idOrganizacion;
		this.listDivisionesCargar = this.jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacionCargar);
	}

	/**
	 * Limpiar formulario.
	 */
	private void limpiarUnidad() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.idDivision = JerarquiasLocal.INICIO;
		this.idDepto = JerarquiasLocal.INICIO;

		this.listDivisiones = new ArrayList<SelectItem>();
		this.listDeptos = new ArrayList<SelectItem>();
		this.listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.listDeptos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * Limpiar undad carga.
	 */
	private void limpiarUnidadCargar() {
		this.organizacionCargar = JerarquiasLocal.INICIO;
		this.idDivisionCargar = JerarquiasLocal.INICIO;
		this.idDeptoCargar = JerarquiasLocal.INICIO;

		this.listDivisionesCargar = new ArrayList<SelectItem>();
		this.listDeptosCargar = new ArrayList<SelectItem>();
		this.listDivisionesCargar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.listDeptosCargar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));

		this.listUnidades.clear();
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@End
	@Override
	public String end() {
		this.nombreUnidad = "";
		this.listOrganizacion = null;
		this.listOrganizacionCargar = null;
		this.listDivisiones = null;
		this.listDivisionesCargar = null;
		this.listDeptos = null;
		this.listDeptosCargar = null;
		this.listUnidades = null;
		this.log.info("Finalizando modulo...");
		return "home";
	}

	// Logica

	@Override
	public void buscarDeptos() {
		listDeptos = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivision);
	}

	@Override
	public void buscarDeptosCargar() {
		listDeptosCargar = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, idDivisionCargar);
		this.buscarUnidades();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUnidades() {
		if (idDepto.equals(JerarquiasLocal.INICIO)) {
			final StringBuilder sql = new StringBuilder("SELECT uo ");
			sql.append("FROM UnidadOrganizacional uo ");
			sql.append("WHERE uo.virtual = false ");
			sql.append("	and uo.departamento.virtual = true ");
			sql.append("	and uo.departamento.division.id = :idDivision ");
			final Query query = em.createQuery(sql.toString());
			query.setParameter("idDivision", idDivisionCargar);
			listUnidades = query.getResultList();
		} else {
			listUnidades = em.createQuery("SELECT uo FROM UnidadOrganizacional uo WHERE uo.departamento.id = :idDepto")
					.setParameter("idDepto", idDepto).getResultList();
		}
	}

	@Override
	public void cargarUnidad(final Long idUnidad) {
		this.limpiarFormulario();
		final UnidadOrganizacional uo = em.find(UnidadOrganizacional.class, idUnidad);
		if (uo != null) {
			this.nombreUnidad = uo.getDescripcion();
			this.organizacion = uo.getDepartamento().getDivision().getOrganizacion().getId();
			this.buscarDivisiones();
			this.idDivision = uo.getDepartamento().getDivision().getId();
			if (this.idDepto.equals(JerarquiasLocal.INICIO)) {
				this.buscarDeptos();
				this.idDepto = uo.getDepartamento().getId();
			}
			this.buscarDeptos();
			this.idUltimaUnidadCargada = idUnidad;
			this.flag = 1;
		} else {
			FacesMessages.instance().add("Error al cargar la unidad");
		}
	}

	@Override
	public void crearUnidad() {
		Boolean modificar = Boolean.TRUE;
		if (this.idDepto == null || this.idDepto.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add(MSG_INGRESAR_DPTO);
			return;
//			modificar = Boolean.TRUE;
		}
		
		if (this.nombreUnidad == null || this.nombreUnidad.isEmpty()) {
			FacesMessages.instance().add("Debe ingresar un nombre para la unidad antes de crearla");
			modificar = Boolean.TRUE;
		}
		
		if(listUnidades != null){
			for(UnidadOrganizacional uni: listUnidades){
				if(nombreUnidad.trim().toUpperCase().equals(uni.getDescripcion().trim().toUpperCase())){
					if(uni.getDepartamento().getId().equals(idDepto)){
						FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
						modificar = Boolean.FALSE;
						return;
					}
				}
			}
		}
		
		if (modificar) {
			final UnidadOrganizacional uo = new UnidadOrganizacional();
			uo.setDescripcion(nombreUnidad);
			uo.setVirtual(Boolean.FALSE);

			final Departamento d = em.find(Departamento.class, idDepto);
			uo.setDepartamento(d);

			this.em.persist(uo);
			FacesMessages.instance().add("Unidad creada exitosamente.");
			limpiarFormulario();
		}

	}

	@Override
	public void modificarUnidad() {
		Boolean modificar = Boolean.TRUE;
		if (this.idUltimaUnidadCargada == null) {
			FacesMessages.instance().add("No se puede realizar modificación.");
			modificar = Boolean.FALSE;
		}

		if (this.nombreUnidad == null || this.nombreUnidad.isEmpty()) {
			FacesMessages.instance().add("Debe ingresar el Nombre de la Unidad.");
			modificar = Boolean.FALSE;
		}

		if (this.idDepto == null || this.idDepto.equals(JerarquiasLocal.INICIO)) {
			FacesMessages.instance().add(MSG_INGRESAR_DPTO);
			modificar = Boolean.FALSE;
		}
		
		if(listUnidades != null){
			for(UnidadOrganizacional uni: listUnidades){
				if(nombreUnidad.trim().toUpperCase().equals(uni.getDescripcion().trim().toUpperCase())){
					if(uni.getDepartamento().getId().equals(idDepto)){
						FacesMessages.instance().add(ERROR_DUPLICIDAD_DATOS);
						modificar = Boolean.FALSE;
						return;
					}
				}
			}
		}

		if (modificar) {
			final UnidadOrganizacional uo = this.em.find(UnidadOrganizacional.class, this.idUltimaUnidadCargada);
			final Departamento dpto = this.em.find(Departamento.class, this.idDepto);
			uo.setDepartamento(dpto);
			uo.setDescripcion(this.nombreUnidad);

			this.em.merge(uo);
			FacesMessages.instance().add("Unidad modificada exitosamente");

			this.buscarUnidades();
			this.nombreUnidad = "";
			this.flag = 0;
		}
	}

	@Override
	public void eliminarUnidad(final UnidadOrganizacional unidad) {
		final UnidadOrganizacional uo = em.find(UnidadOrganizacional.class, unidad.getId());
		if (uo.getCargos() == null || uo.getCargos().size() == 0) {
			listUnidades.remove(unidad);
			em.remove(uo);

			//this.limpiarFormulario();

			FacesMessages.instance().add("Unidad eliminada exitosamente");
		} else {
			FacesMessages.instance().add("Imposible borrar unidades con cargos asociados");
		}
	}

	@Override
	public void limpiarFormulario() {
//		this.nombreUnidad = "";
//		this.listOrganizacion.clear();
//		this.listOrganizacionCargar.clear();
//		this.listDivisiones.clear();
//		this.listDivisionesCargar.clear();
//		this.listOrganizacion = this.buscarOrganizaciones();
//		this.organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		this.listOrganizacionCargar = this.buscarOrganizacionesCargar();
//		this.organizacionCargar = (Long) listOrganizacionCargar.get(1).getValue();
//		this.buscarDivisionesCargar();
		/*
		 * limpiarUnidad(); limpiarUnidadCargar();
		 */
		begin();
		this.flag = 0;
	}

	// Getters & Setters

	@Override
	public String getNombreUnidad() {
		return nombreUnidad;
	}

	@Override
	public void setNombreUnidad(final String nombreUnidad) {
		this.nombreUnidad = nombreUnidad;
	}

	@Override
	public Long getIdDivision() {
		return idDivision;
	}

	@Override
	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	@Override
	public Long getIdDepto() {
		return idDepto;
	}

	@Override
	public void setIdDepto(final Long idDepto) {
		this.idDepto = idDepto;
	}

	@Override
	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	@Override
	public void setListDivisiones(final List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	@Override
	public List<SelectItem> getListDeptos() {
		return listDeptos;
	}

	@Override
	public void setListDeptos(final List<SelectItem> listDeptos) {
		this.listDeptos = listDeptos;
	}

	@Override
	public List<UnidadOrganizacional> getListUnidades() {
		return listUnidades;
	}

	@Override
	public void setListUnidades(final List<UnidadOrganizacional> listUnidades) {
		this.listUnidades = listUnidades;
	}

	@Override
	public Long getIdDivisionCargar() {
		return idDivisionCargar;
	}

	@Override
	public void setIdDivisionCargar(final Long idDivisionCargar) {
		this.idDivisionCargar = idDivisionCargar;
	}

	@Override
	public Long getIdDeptoCargar() {
		return idDeptoCargar;
	}

	@Override
	public void setIdDeptoCargar(final Long idDeptoCargar) {
		this.idDeptoCargar = idDeptoCargar;
	}

	@Override
	public List<SelectItem> getListDivisionesCargar() {
		return listDivisionesCargar;
	}

	@Override
	public void setListDivisionesCargar(final List<SelectItem> listDivisionesCargar) {
		this.listDivisionesCargar = listDivisionesCargar;
	}

	@Override
	public List<SelectItem> getListDeptosCargar() {
		return listDeptosCargar;
	}

	@Override
	public void setListDeptosCargar(final List<SelectItem> listDeptosCargar) {
		this.listDeptosCargar = listDeptosCargar;
	}

	@Override
	public Integer getFlag() {
		return flag;
	}

	@Override
	public void setFlag(final Integer flag) {
		this.flag = flag;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getOrganizacionCargar() {
		return organizacionCargar;
	}

	@Override
	public void setOrganizacionCargar(final Long organizacionCargar) {
		this.organizacionCargar = organizacionCargar;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacionCargar() {
		return listOrganizacionCargar;
	}

	@Override
	public void setListOrganizacioCargarn(final List<SelectItem> listOrgCargar) {
		this.listOrganizacionCargar = listOrgCargar;
	}
}
