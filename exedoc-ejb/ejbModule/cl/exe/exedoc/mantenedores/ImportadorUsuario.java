package cl.exe.exedoc.mantenedores;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import cl.exe.exedoc.entity.Persona;

/**
 * Esta interfaz debe ser implementada para cuando se importen usuarios desde otro Sistema.
 * 
 * @author
 */
@Local
public interface ImportadorUsuario {


	/**
	 * @param sRut {@link String}
	 * @param persona {@link Persona}
	 * @param em {@link EntityManager}
	 * @return {@link Integer}
	 */
	int getPersona(final String sRut, final Persona persona, final EntityManager em);

	String normalizarRUT(String prmRut);

}
