package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoPlantilla;

/**
 * Implementacion de {@link AdministradorPlantillasContrato}.
 */
@Stateful
@Name("administradorPlantillasContrato")
public class AdministradorPlantillasContratoBean implements AdministradorPlantillasContrato {

	private static final String INGRESE_PUNTOS_CONTRATO = "Ingrese los puntos del contrato";
	private static final String INGRESE_PROLOGO = "Ingrese el prólogo";
	private static final String INGRESE_TITULO = "Ingrese el título";
	private static final String INGRESE_MATERIA = "Ingrese la materia";
	private static final String ID = "id";
	private static final String PLANTILLA_FIND_BY_ID = "Plantilla.findById";
	
	private Plantilla plantilla;
	private List<Plantilla> plantillas;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	private String nombre;
	private String materia;
	private String titulo;
	private String prologo;
	private String puntos;

	/**
	 * Constructor.
	 */
	public AdministradorPlantillasContratoBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		plantilla = new Plantilla();
		plantillas = new ArrayList<Plantilla>();

		this.nombre = "";
		this.materia = "";
		this.titulo = "";
		this.prologo = "";
		this.puntos = "";

		this.buscar();
		return "administradorPlantillasContrato";
	}

	/* (non-Javadoc)
	 * TODO: NPATH COMPLEXITY
	 */
	@Override
	public void crearNuevo() {
		if (plantilla != null && plantilla.getId() != null) {
			log.info("Modificando plantilla...");
			plantilla.setNombre(this.getNombre());
			this.actualizarCamposPlantilla(plantilla.getId());

			em.merge(plantilla);
			FacesMessages.instance().add("Se modificó la plantilla en el sistema");

			this.limpiarFormulario();
		} else {
			log.info("Creando plantilla...");
			boolean ok = true;
			if (this.getNombre().isEmpty()) {
				FacesMessages.instance().add("Ingrese el nombre");
				ok = false;
			}
			if (this.getMateria().isEmpty()) {
				FacesMessages.instance().add(INGRESE_MATERIA);
				ok = false;
			}
			if (this.getTitulo().isEmpty()) {
				FacesMessages.instance().add(INGRESE_TITULO);
				ok = false;
			}
			if (this.getPrologo().isEmpty()) {
				FacesMessages.instance().add(INGRESE_PROLOGO);
				ok = false;
			}
			if (this.getPuntos().isEmpty()) {
				FacesMessages.instance().add(INGRESE_PUNTOS_CONTRATO);
				ok = false;
			}
			if (ok) {
				plantilla.setNombre(this.getNombre());
				CamposPlantilla campos;
				final List<CamposPlantilla> listCampos = new ArrayList<CamposPlantilla>();

				if (this.getMateria() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.MATERIA));
					campos.setContenido(this.materia);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add(INGRESE_MATERIA);
				}

				if (this.getTitulo() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.TITULO));
					campos.setContenido(this.titulo);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add(INGRESE_TITULO);
				}

				if (this.getPrologo() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.PROLOGO));
					campos.setContenido(this.prologo);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add(INGRESE_PROLOGO);
				}

				if (this.getPuntos() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.PUNTOS));
					campos.setContenido(this.puntos);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add(INGRESE_PUNTOS_CONTRATO);
				}

				plantilla.setCamposPlantilla(listCampos);
				plantilla.setTipoPlantilla(new TipoPlantilla(TipoPlantilla.CONTRATO));

				em.persist(plantilla);
				FacesMessages.instance().add("Se agregó la plantilla al sistema");

				this.limpiarFormulario();
			}
			this.buscar();
			this.plantilla = new Plantilla();
		}
	}

	/**
	 * @param idPlantilla Integer
	 */
	private void actualizarCamposPlantilla(final Integer idPlantilla) {
		final Query query = em.createNamedQuery(PLANTILLA_FIND_BY_ID);
		query.setParameter(ID, idPlantilla);
		final Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpc : plant.getCamposPlantilla()) {
				if (cpc.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					cpc.setContenido(this.getMateria());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TITULO)) {
					cpc.setContenido(this.getTitulo());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PROLOGO)) {
					cpc.setContenido(this.getPrologo());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PUNTOS)) {
					cpc.setContenido(this.getPuntos());
				}
			}
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void buscar() {
		final Query query = em.createNamedQuery("Plantilla.findByTipoPlantilla");
		query.setParameter("tp", TipoPlantilla.CONTRATO);
		this.plantillas = query.getResultList();
	}

	@Override
	public void obtenerPlantilla(final Integer idPlantilla) {
		final Query query = em.createNamedQuery(PLANTILLA_FIND_BY_ID);
		query.setParameter(ID, idPlantilla);
		final Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpc : plant.getCamposPlantilla()) {

				if (cpc.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					this.setMateria(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TITULO)) {
					this.setTitulo(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PROLOGO)) {
					this.setPrologo(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PUNTOS)) {
					this.setPuntos(cpc.getContenido());
				}
			}
		}
	}

	@Override
	public void limpiarFormulario() {
		this.plantillas = null;
		this.plantilla = null;

		this.begin();
	}

	@Override
	public void modificar(final Integer id) {
		plantilla = em.find(Plantilla.class, id);
		this.setNombre(plantilla.getNombre());
		this.obtenerPlantilla(id);
	}

	@Override
	public void eliminarPlantilla(final Plantilla p) {
		final Plantilla mPlantilla = em.find(Plantilla.class, p.getId());
		for (CamposPlantilla cpc : mPlantilla.getCamposPlantilla()) {
			em.remove(cpc);
		}
		mPlantilla.getCamposPlantilla().clear();
		plantillas.remove(p);
		em.remove(mPlantilla);
		this.limpiarFormulario();
		FacesMessages.instance().add("Plantilla eliminada");
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	public Plantilla getPlantilla() {
		return plantilla;
	}

	@Override
	public void setPlantilla(final Plantilla plantilla) {
		this.plantilla = plantilla;
	}

	@Override
	public List<Plantilla> getPlantillas() {
		return plantillas;
	}

	@Override
	public String getNombre() {
		return nombre;
	}

	@Override
	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String getMateria() {
		return materia;
	}

	@Override
	public void setMateria(final String materia) {
		this.materia = materia;
	}

	@Override
	public String getTitulo() {
		return titulo;
	}

	@Override
	public void setTitulo(final String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String getPrologo() {
		return prologo;
	}

	@Override
	public void setPrologo(final String prologo) {
		this.prologo = prologo;
	}

	@Override
	public String getPuntos() {
		return puntos;
	}

	@Override
	public void setPuntos(final String puntos) {
		this.puntos = puntos;
	}

}
