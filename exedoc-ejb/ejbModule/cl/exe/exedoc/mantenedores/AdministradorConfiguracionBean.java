package cl.exe.exedoc.mantenedores;

import java.net.URL;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.config.MailImpl;

/**
 * Implementacion de {@link AdministradorConfiguracion}.
 */
@Stateful
@Name("administradorConfiguracion")
@Scope(value = ScopeType.SESSION)
public class AdministradorConfiguracionBean implements AdministradorConfiguracion {

	private MailImpl mail;

	/**
	 * Constructor.
	 */
	public AdministradorConfiguracionBean() {
		super();
	}

	@SuppressWarnings("unused")
	@Override
	@Begin(join = true)
	public String begin() {
		mail = null;

		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final URL url = classLoader.getResource("mail.properties");
		return "administradorConfiguracion";
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	public void guardarConfiguracion() {
	}

	@Override
	public void limpiarFormulario() {
		mail = null;
	}

	@Override
	public MailImpl getMail() {
		if (mail == null) {
			mail = new MailImpl();
		}
		return mail;
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public void actualizar() {

	}

}
