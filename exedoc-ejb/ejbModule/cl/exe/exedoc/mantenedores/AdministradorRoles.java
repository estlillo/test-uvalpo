package cl.exe.exedoc.mantenedores;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;

/**
 * Interface administrador de roles.
 * 
 * @author Ricardo Fuentes
 */
@Local
public interface AdministradorRoles {

	/**
	 * Metodo que inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * Metodo que destruye la converzacion.
	 */
	void destroy();

	/**
	 * Metodo que termina la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Metodo que limpia el formulario.
	 */
	void limpiarFormulario();

	// void guardarPersona();

	/**
	 * Metodo que busca los departamentos.
	 */
	void buscarDepartamentos();

	/**
	 * Metodo que busca las unidades.
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * Metodo que busca cargos.
	 */
	void buscarCargos();

	/**
	 * Metodo que busca las personas.
	 */
	void buscarPersonas();

	/**
	 * Metodo que busca las divisiones.
	 */
	void buscarDivisiones();

	/**
	 * Metodo que modifica una personas segun el Id.
	 * 
	 * @param idPersona {@link Long}
	 */
	void modificar(Long idPersona);

	/**
	 * Metodo que busca los usuarios.
	 */
	void buscarUsuario();

	/**
	 * Metodo que asocia rol al usuario.
	 */
	void asociarRolPersona();

	/**
	 * @return {@link Persona}
	 */
	Persona getPersona();

	/**
	 * @return {@link List} of {@link Persona}
	 */
	List<Persona> getPersonas();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivision();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListUnidadesOrganizacionales();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListCargos();

	/**
	 * @return {@link Long}
	 */
	Long getDivision();

	/**
	 * @param division {@link Long}
	 */
	void setDivision(Long division);

	/**
	 * @return {@link Long}
	 */
	Long getDepartamento();

	/**
	 * @param departamento {@link Long}
	 */
	void setDepartamento(Long departamento);

	/**
	 * @return {@link Long}
	 */
	Long getUnidadOrganizacional();

	/**
	 * @param unidadOrganizacional {@link Long}
	 */
	void setUnidadOrganizacional(Long unidadOrganizacional);

	/**
	 * @return {@link Long}
	 */
	Long getCargo();

	/**
	 * @param cargo {@link Long}
	 */
	void setCargo(Long cargo);

	/**
	 * @return {@link String}
	 */
	String getUser();

	/**
	 * @param user {@link String}
	 */
	void setUser(String user);

	/**
	 * @return {@link List} of {@link Persona}
	 */
	List<Persona> getPersonaSeleccionada();

	/**
	 * @return {@link List} of {@link Rol}
	 */
	List<Rol> getRoles();

	/**
	 * @return {@link Map} of <{@link Integer}, {@link Boolean}>
	 */
	Map<Integer, Boolean> getSelectRoles();

	/**
	 * @param selectRoles {@link Map} of <{@link Integer}, {@link Boolean}>
	 */
	void setSelectRoles(Map<Integer, Boolean> selectRoles);

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion {@link Long}
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return {@link Long}
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion {@link Long}
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);
}
