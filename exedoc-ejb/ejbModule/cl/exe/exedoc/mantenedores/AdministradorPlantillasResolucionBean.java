package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoPlantilla;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("administradorPlantillasResolucion")
public class AdministradorPlantillasResolucionBean implements AdministradorPlantillasResolucion {

	private static final String ID = "id";
	private Plantilla plantilla;
	private List<Plantilla> plantillas;
	private static final String INICIO = "<<Seleccionar>>";
	@PersistenceContext
	private EntityManager em;

	
	@EJB
	private JerarquiasLocal jerarquias;
	
	@In(required = true)
	private Persona usuario;
	
	@Logger
	private Log log;

	private String nombre;
	private String materia;
	private Integer tipoResolucion;
	private String vistos;
	private String considerando;
	private String resuelvo;
	private String indicacion;
	
	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidadesOrganizacionales;
	
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;

	@Begin(join = true)
	public String begin() {
		plantilla = new Plantilla();
		plantillas = new ArrayList<Plantilla>();
		
		listOrganizacion = new ArrayList<SelectItem>();
		listDivision = new ArrayList<SelectItem>();
		listDepartamento = new ArrayList<SelectItem>();
		listUnidadesOrganizacionales = new ArrayList<SelectItem>();
		
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos(INICIO);
		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		this.buscarUnidadesOrganizacionales();
		unidadOrganizacional = usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId();

		this.nombre = "";
		this.materia = "";
		this.tipoResolucion = 1;
		this.vistos = "";
		this.considerando = "";
		this.resuelvo = "";
		this.indicacion = "";

		this.buscar();
		return "administradorPlantillasResolucion";
	}
	
	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division mDivision = divisiones.get(0);
			if (mDivision.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
			
			}
		}
		
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento mDepartamento = departamentos.get(0);
			if (mDepartamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				
			}
		}
		
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
	}

	/**
	 * Limpia los campos
	 */
	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();

		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,	JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,	JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,	JerarquiasLocal.TEXTO_INICIAL));
		

	}
	
	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}
	
	public void crearNuevo() {
		if (plantilla != null && plantilla.getId() != null) {
			plantilla.setNombre(this.getNombre());
			if (!verificaNombre(plantilla)) {
				FacesMessages.instance().add("Ya existe una Plantilla con ese nombre");
				return;
			}
			log.info("Modificando plantilla...");
			this.actualizarCamposPlantilla(plantilla.getId());

			em.merge(plantilla);
			FacesMessages.instance().add("Se modificó la plantilla en el sistema");
			log.info("Se modificó la plantilla en el sistema");

			this.limpiarFormulario();
		} else {
			log.info("Creando plantilla...");
			boolean ok = true;
			if (this.getNombre().equals("")) {
				FacesMessages.instance().add("Ingrese el nombre");
				log.info("Nombre no ingresado");
				ok = false;
			}
			plantilla.setNombre(this.getNombre());
			if (!verificaNombre(plantilla)) {
				FacesMessages.instance().add("Ya existe una Plantilla con ese nombre");
				ok = false;
			}
			if (!this.getTipoResolucion().equals(1) && !this.getTipoResolucion().equals(2) && !this.getTipoResolucion().equals(3)) {
				FacesMessages.instance().add("Elija el tipo");
				log.info("Tipo no elegido");
				ok = false;
			}
			if (this.getMateria().equals("")) {
				FacesMessages.instance().add("Ingrese la materia");
				log.info("Materia no ingresada");
				ok = false;
			}
			if (this.getVistos().equals("")) {
				FacesMessages.instance().add("Ingrese los vistos");
				log.info("Vistos no ingresados");
				ok = false;
			}
			if (this.getResuelvo().equals("")) {
				FacesMessages.instance().add("Ingrese las resoluciones");
				log.info("Resoluciones no ingresadas");
				ok = false;
			}
			if (this.getIndicacion().equals("")) {
				FacesMessages.instance().add("Ingrese las indicaciones");
				log.info("Indicaciones no ingresadas");
				ok = false;
			}
			if (ok) {
				plantilla.setNombre(this.getNombre());
				CamposPlantilla campos;
				List<CamposPlantilla> listCampos = new ArrayList<CamposPlantilla>();

				if (this.getTipoResolucion() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.TIPO));
					if (this.getTipoResolucion().equals(1)) {
						campos.setContenido("EXENTO");
					} else if (this.getTipoResolucion().equals(2)) {
						campos.setContenido("TOMA DE RAZON");
					} else {
						campos.setContenido("EXENTO CON REGISTRO");
					}
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Elija un tipo");
					log.info("Elija un tipo");
				}

				if (this.getMateria() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.MATERIA));
					campos.setContenido(this.getMateria());
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese la materia");
					log.info("Ingrese la materia");
				}

				if (this.getVistos() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.VISTOS));
					campos.setContenido(this.getVistos());
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese los vistos");
					log.info("Ingrese los vistos");
				}

				if (this.getConsiderando() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.CONSIDERANDO));
					campos.setContenido(this.getConsiderando());
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				}

				if (this.getResuelvo() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.RESUELVO));
					campos.setContenido(this.getResuelvo());
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese los resuelvo");
					log.info("Ingrese los resuelvo");
				}

				if (this.getIndicacion() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.INDICACION));
					campos.setContenido(this.getIndicacion());
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese las indicaciones");
					log.info("Ingrese las indicaciones");
				}

				plantilla.setCamposPlantilla(listCampos);
				plantilla.setTipoPlantilla(new TipoPlantilla(TipoPlantilla.RESOLUCION_DECRETO));

				em.persist(plantilla);
				FacesMessages.instance().add("Se agregó la plantilla al sistema");
				log.info("Se agregó la plantilla al sistema");

				this.limpiarFormulario();
			}
			this.buscar();
			this.plantilla = new Plantilla();
		}
	}

	public void actualizarCamposPlantilla(Integer idPlantilla) {
		Query query = em.createNamedQuery("Plantilla.findById");
		query.setParameter("id", idPlantilla);
		Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpr : plant.getCamposPlantilla()) {
				if (cpr.getTipoContenido().getId().equals(TipoContenido.TIPO)) {
					if (this.getTipoResolucion().equals(1)) {
						cpr.setContenido("EXENTO");
					} else if (this.getTipoResolucion().equals(2)) {
						cpr.setContenido("TOMA DE RAZON");
					} else {
						cpr.setContenido("EXENTO CON REGISTRO");
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					cpr.setContenido(this.getMateria());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.VISTOS)) {
					cpr.setContenido(this.getVistos());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					cpr.setContenido(this.getConsiderando());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.RESUELVO)) {
					cpr.setContenido(this.getResuelvo());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.INDICACION)) {
					cpr.setContenido(this.getIndicacion());
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void buscar() {
		Query query = em.createNamedQuery("Plantilla.findByTipoPlantilla");
		query.setParameter("tp", TipoPlantilla.RESOLUCION_DECRETO);
		this.plantillas = query.getResultList();
	}

	public void obtenerPlantilla(Integer idPlantilla) {
		Query query = em.createNamedQuery("Plantilla.findById");
		query.setParameter("id", idPlantilla);
		Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpr : plant.getCamposPlantilla()) {
				if (cpr.getTipoContenido().getId().equals(TipoContenido.TIPO)) {
					if (cpr.getContenido().equals("EXENTO")) {
						tipoResolucion = 1;
					} else if (cpr.getContenido().equals("TOMA DE RAZON")) {
						tipoResolucion = 2;
					} else {
						tipoResolucion = 3;
					}
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					this.setMateria(cpr.getContenido());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.VISTOS)) {
					this.setVistos(cpr.getContenido());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					this.setConsiderando(cpr.getContenido());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.RESUELVO)) {
					this.setResuelvo(cpr.getContenido());
				}
				if (cpr.getTipoContenido().getId().equals(TipoContenido.INDICACION)) {
					this.setIndicacion(cpr.getContenido());
				}
			}
		}
	}

	public void limpiarFormulario() {
		this.plantillas = null;
		this.plantilla = null;

		this.begin();
	}

	public void modificar(Integer id) {
		plantilla = em.find(Plantilla.class, id);
		this.setNombre(plantilla.getNombre());
		this.obtenerPlantilla(id);
	}
	
	public void eliminarPlantilla(Plantilla p) {
		Plantilla plantilla = em.find(Plantilla.class, p.getId());
		for (CamposPlantilla cpr : plantilla.getCamposPlantilla()) {
			em.remove(cpr);
		}
		plantilla.getCamposPlantilla().clear();
		plantillas.remove(p);
		em.remove(plantilla);
		limpiarFormulario();
		FacesMessages.instance().add("Plantilla eliminada");
	}
	
	public boolean renderedBotonEliminar(Plantilla pr) {
//		if (pr.getId().equals(Plantilla.PLANTILLA_ADJUDICACION)) {
//			return false;
//		} else if (pr.getId().equals(Plantilla.PLANTILLA_CONCURSO)) {
//			return false;
//		} else if (pr.getId().equals(Plantilla.PLANTILLA_CONTRATACION)) {
//			return false;
//		} else if (pr.getId().equals(Plantilla.PLANTILLA_DIAS_ADMINISTRATIVOS_C_REM)) {
//			return false;
//		} else if (pr.getId().equals(Plantilla.PLANTILLA_DIAS_ADMINISTRATIVOS_S_REM)) {
//			return false;
//		} else if (pr.getId().equals(Plantilla.PLANTILLA_FERIADO)) {
//			return false;
//		}
		return true;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	@End
	public String end() {
		return "home";
	}

	public Plantilla getPlantilla() {
		return plantilla;
	}

	public void setPlantilla(Plantilla plantilla) {
		this.plantilla = plantilla;
	}

	public List<Plantilla> getPlantillas() {
		return plantillas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public String getVistos() {
		return vistos;
	}

	public void setVistos(String vistos) {
		this.vistos = vistos;
	}

	public String getConsiderando() {
		return considerando;
	}

	public void setConsiderando(String considerando) {
		this.considerando = considerando;
	}

	public String getResuelvo() {
		return resuelvo;
	}

	public void setResuelvo(String resuelvo) {
		this.resuelvo = resuelvo;
	}

	public String getIndicacion() {
		return indicacion;
	}

	public void setIndicacion(String indicacion) {
		this.indicacion = indicacion;
	}

	public Integer getTipoResolucion() {
		return tipoResolucion;
	}

	public void setTipoResolucion(Integer tipoResolucion) {
		this.tipoResolucion = tipoResolucion;
	}
	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}
	@Override
	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}
	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}
	@Override
	public void setListDivision(List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}
	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}
	@Override
	public void setListDepartamento(List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}
	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}
	@Override
	public void setListUnidadesOrganizacionales(
			List<SelectItem> listUnidadesOrganizacionales) {
		this.listUnidadesOrganizacionales = listUnidadesOrganizacionales;
	}
	@Override
	public Long getOrganizacion() {
		return organizacion;
	}
	@Override
	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}
	@Override
	public Long getDivision() {
		return division;
	}
	@Override
	public void setDivision(Long division) {
		this.division = division;
	}
	@Override
	public Long getDepartamento() {
		return departamento;
	}
	@Override
	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}
	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}
	@Override
	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}
	
	public Departamento getFiscaliaCompleta(Long id){
		Departamento fiscalia = null;
		try{
			fiscalia = em.find(Departamento.class, id);
		}
		catch(Exception ex){
			log.info("Error ", ex.getMessage());
		}
		return fiscalia;
	}
	
	private boolean verificaNombre(Plantilla plantilla) {
		boolean retorno = true;
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT p ");
		hql.append("FROM Plantilla p ");
		hql.append("WHERE p.tipoPlantilla.id = :tipo ");
		hql.append("AND upper(p.nombre) = upper(:nombre) ");
		if (plantilla.getId() != null) {
			hql.append("AND p.id != :id");
		}
		Query query = em.createQuery(hql.toString());
		query.setParameter("tipo", TipoPlantilla.RESOLUCION_DECRETO);
		query.setParameter("nombre", plantilla.getNombre());
		if (plantilla.getId() != null) {
			query.setParameter("id", plantilla.getId());
		}
		List<Object> o = (List<Object>)query.getResultList();
		if (o != null && o.size() > 0) {
			retorno = false;
		}
		return retorno;
	}
}
