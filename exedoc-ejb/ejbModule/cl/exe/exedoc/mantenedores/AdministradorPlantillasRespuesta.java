package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.PlantillaRespuesta;

@Local
public interface AdministradorPlantillasRespuesta {
	// flujo
	String begin();

	void destroy();

	String end();

	// acciones
	void crear();

	void cargar(Integer id);

	void limpiarFormulario();

	// Getters & Setters
	List<PlantillaRespuesta> getPlantillas();

	String getNombre();

	void setNombre(String nombre);

	String getContenido();

	void setContenido(String contenido);
}
