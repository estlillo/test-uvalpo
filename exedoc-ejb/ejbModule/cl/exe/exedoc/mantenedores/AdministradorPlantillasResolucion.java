package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;
import cl.exe.exedoc.entity.Plantilla;

@Local
public interface AdministradorPlantillasResolucion {

	// flujo
	String begin();

	void destroy();

	String end();

	// acciones
	void crearNuevo();

	void modificar(Integer id);

	void obtenerPlantilla(Integer idPlantilla);

	void eliminarPlantilla(Plantilla p);

	boolean renderedBotonEliminar(Plantilla pr);

	void limpiarFormulario();

	// Setters & Getters
	Plantilla getPlantilla();

	void setPlantilla(Plantilla plantilla);

	List<Plantilla> getPlantillas();

	String getNombre();

	void setNombre(String nombre);

	String getMateria();

	void setMateria(String materia);

	String getVistos();

	void setVistos(String vistos);

	String getConsiderando();

	void setConsiderando(String considerando);

	String getResuelvo();

	void setResuelvo(String resuelvo);

	String getIndicacion();

	void setIndicacion(String indicacion);

	Integer getTipoResolucion();

	void setTipoResolucion(Integer tipoResolucion);
	
	public void buscarDivisiones();
	
	public void buscarDepartamentos();
	
	public void buscarUnidadesOrganizacionales();
	
	
	public List<SelectItem> getListOrganizacion();
	
	public void setListOrganizacion(List<SelectItem> listOrganizacion);
	
	public List<SelectItem> getListDivision();
	
	public void setListDivision(List<SelectItem> listDivision);
	
	public List<SelectItem> getListDepartamento();
	
	public void setListDepartamento(List<SelectItem> listDepartamento);
	
	public List<SelectItem> getListUnidadesOrganizacionales();
	
	public void setListUnidadesOrganizacionales(List<SelectItem> listUnidadesOrganizacionales);
	
	public Long getOrganizacion();
	
	public void setOrganizacion(Long organizacion);
	
	public Long getDivision();
	
	public void setDivision(Long division);
	
	public Long getDepartamento();
	
	public void setDepartamento(Long departamento);
	
	public Long getUnidadOrganizacional();
	
	public void setUnidadOrganizacional(Long unidadOrganizacional);
}
