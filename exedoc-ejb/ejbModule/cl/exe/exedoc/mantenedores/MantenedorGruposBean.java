package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * @author Sergio
 *
 */
@Stateful
@Name("mantenedorGrupos")
public class MantenedorGruposBean implements MantenedorGrupos {

	public static final String MANTENEDOR_GRUPOS = "mantenedorGrupos";

//	private static final String SELECCIONAR = "<<Seleccionar>>";

	private static final String ERROR_AL_OBTENER_GRUPOS = "Error al obtener grupos: ";

	private static final String OPERACIONREALIZADA = "Operación realizada correctamente.";
	
	private GrupoUsuario grupoUsuario;
	private List<GrupoUsuario> listGrupos;

	@EJB
	private JerarquiasLocal jerarquias;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	// Listas desplegables
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private Set<Persona> listDestinatarios;
	
	@In(required = false, scope = ScopeType.CONVERSATION)
	@Out(required = false, scope = ScopeType.CONVERSATION)
	private List<SelectPersonas> destinatariosDocumento;
	
	private Long grupo = JerarquiasLocal.INICIO;
	private List<SelectItem> listDestFrec = new ArrayList<SelectItem>();
	
	private List<Persona> listaPersonas;
	
	private String destinatario;
	
	/**
	 * Constructor.
	 */
	public MantenedorGruposBean() {
		super();
	}

	@Remove
	@Override
	public void remove() {
	}

	@Destroy
	@Override
	public void destroy() {
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation: mantenedorGrupos");

		this.destinatario = "";
		this.grupoUsuario = new GrupoUsuario();

		listOrganizacion = this.buscarOrganizaciones();
		
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		listDestinatarios = new HashSet<Persona>();
		listaPersonas = new ArrayList<Persona>();
		this.cargarGrupos();
		
		return MANTENEDOR_GRUPOS;
	}

	/**
	 * 
	 */
	private void cargarGrupos() {
		this.setListGrupos(this.buscarGrupo());
		if (this.getListGrupos() == null) {
			this.setListGrupos(new ArrayList<GrupoUsuario>());
		}
	}

	@Override
	public GrupoUsuario getGrupoUsuario() {
		return grupoUsuario;
	}

	@Override
	public void setGrupoUsuario(final GrupoUsuario grupoUsuario) {
		this.grupoUsuario = grupoUsuario;
	}

	@Override
	public List<GrupoUsuario> getListGrupos() {
		return listGrupos;
	}

	@Override
	public void setListGrupos(final List<GrupoUsuario> listGrupos) {
		this.listGrupos = listGrupos;
	}

	@SuppressWarnings("unchecked")
	public List<GrupoUsuario> buscarGrupo() {

		List<GrupoUsuario> grupos = null;

		final Query query = em.createNamedQuery("GrupoUsuario.findByAll");

		try {
			grupos = query.getResultList();
		} catch (PersistenceException e) {
			log.error(MantenedorGruposBean.ERROR_AL_OBTENER_GRUPOS, e);
		}
		return grupos;
	}

	@Override
	public void guardarGrupo() {
		final boolean status;
		boolean ok = true;

		if (ok) {
			if (this.grupoUsuario.getNombre().length() < 1) {
				FacesMessages.instance().add("Debe ingresar un nombre.");
				ok = false;
			}
			
			if (destinatariosDocumento.size() < 1) {
				FacesMessages.instance().add("Debe ingresar al menos una persona.");
				ok = false;
			}
			
			if (this.validarGrupo() && this.grupoUsuario.getId() == null) {
				FacesMessages.instance().add(
						"Ya existe grupo.");
				ok = false;
			}

			if (ok) {
				this.listaPersonas = new ArrayList<Persona>();
				for (SelectPersonas de : destinatariosDocumento) {
					try {
						listaPersonas.add(de.getPersona());
					} catch (Exception e) {
						ok = false;
						log.error("Error: ", e);
					}
				}
				if (ok) {
					this.grupoUsuario.setUsuarios(this.listaPersonas);
					
					if (this.grupoUsuario.getId() == null) {
						log.info("Creando Grupo: ");
						status = this.crear(grupoUsuario);
					} else {
						log.info("Actualizando Grupo: ");
						status = this.actualizar(grupoUsuario);
					}
	
					if (status) {
						this.cargarGrupos();
						this.setGrupoUsuario(new GrupoUsuario());
						this.limpiarGrupo();
						FacesMessages.instance().add(OPERACIONREALIZADA);
					} else {
						FacesMessages.instance().add(
								"Ha ocurrido un error al guardar el Grupo.");
					}
				} else {
					FacesMessages.instance().add(
					"Ha ocurrido un error al guardar lista de personas.");
				}
			}
		}
	}

	@Override
	public boolean crear(final GrupoUsuario grupo) {
		try {
			em.persist(grupo);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al persistir grupo: ", e);
		}

		return false;
	}

	@Override
	public boolean actualizar(final GrupoUsuario grupo) {

		try {
			em.merge(grupo);
			return true;
		} catch (PersistenceException e) {
			log.error("Error al actualizar grupo: ", e);
		}

		return false;
	}

	@Override
	public void editarGrupo(final Long id) {

		final GrupoUsuario gu = em.find(GrupoUsuario.class, id);

		if (gu != null) {
			this.grupoUsuario = gu;
			this.destinatariosDocumento = new ArrayList<SelectPersonas>();
			for (Persona de : gu.getUsuarios() ) {
				try {
					this.destinatariosDocumento.add(new SelectPersonas(de.getNombreApellido() + 
							" - " +	de.getCargo().getUnidadOrganizacional().getDescripcion() + 
							" - " + de.getCargo().getDescripcion(), de));
				} catch (Exception e) {
					log.error("Error: ", e);
				}
			}
			FacesMessages.instance().add(OPERACIONREALIZADA);
		} else {
			FacesMessages.instance().add(ERROR_AL_OBTENER_GRUPOS);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boolean validarGrupo() {

//		final StringBuilder sql = new StringBuilder();
		List<GrupoUsuario> listGrupo = new ArrayList<GrupoUsuario>();

		final Query query = em.createNamedQuery("GrupoUsuario.findByNombre");
		query.setParameter("nombre", this.grupoUsuario.getNombre());
		
		try {
			listGrupo = query.getResultList();
		} catch (NoResultException e) {
			return false;
		}

		if (listGrupo.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public void limpiarGrupo() {
		this.grupoUsuario = new GrupoUsuario();
		this.destinatario = "";
		
		listOrganizacion = buscarOrganizaciones();
		this.destinatariosDocumento = new ArrayList<SelectPersonas>();
	}

	@Override
	public void setListaPersonas(List<Persona> listaPersonas) {
		this.listaPersonas = listaPersonas;
	}

	@Override
	public List<Persona> getListaPersonas() {
		return listaPersonas;
	}
	
	@Override
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	@Override
	public String getDestinatario() {
		return destinatario;
	}

	@Override
	public Set<Persona> getListDestinatarios() {
		return listDestinatarios;
	}

	@Override
	public void setListDestinatarios(Set<Persona> listDestinatarios) {
		this.listDestinatarios = listDestinatarios;
	}
	
	@Override
	public String end() {
		this.destinatario = "";
		listDestinatarios = new HashSet<Persona>();
		return "";
	}
	
	/**
	 * @return {@link List}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		this.division = JerarquiasLocal.INICIO;
		this.listDepartamento.clear();
		this.listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.departamento = JerarquiasLocal.INICIO;
		this.listUnidadesOrganizacionales.clear();
		this.listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.listCargos.clear();
		this.listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.cargo = JerarquiasLocal.INICIO;
		this.listPersonas.clear();
		this.listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.persona = JerarquiasLocal.INICIO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter("id", division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		this.departamento = JerarquiasLocal.INICIO;
		this.listUnidadesOrganizacionales.clear();
		this.listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.listCargos.clear();
		this.listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.cargo = JerarquiasLocal.INICIO;
		this.listPersonas.clear();
		this.listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.persona = JerarquiasLocal.INICIO;
	}

	/**
	 * buscar Unidades Organizacionales Departamento.
	 */
	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.listCargos.clear();
		this.listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.cargo = JerarquiasLocal.INICIO;
		this.listPersonas.clear();
		this.listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.persona = JerarquiasLocal.INICIO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter("id", departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento dpto = departamentos.get(0);
			if (dpto.getConCargo()) {
				conCargo = true;
					this.unidadOrganizacional = jerarquias
							.getIdUnidadVirtualDepartamento(this.division,
									this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.listCargos.clear();
		this.listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.cargo = JerarquiasLocal.INICIO;
		this.listPersonas.clear();
		this.listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		this.persona = JerarquiasLocal.INICIO;
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (!listPersonas.isEmpty()) {
			List<SelectItem> lsiPersonas = new ArrayList<SelectItem>();
			for (SelectItem si : listPersonas) {
				for (SelectPersonas sp : this.destinatariosDocumento) {
					if (Long.parseLong(si.getValue().toString()) == sp.getPersona().getId()) {
						lsiPersonas.add(si);
					}
				}
			}
//			for (SelectItem sip : lsiPersonas) {
//				listPersonas.remove(sip);
//			}
			if (listPersonas.size() == 1) {
				persona = (Long) listPersonas.get(0).getValue();
			}
		}
		
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}
	
	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public void setListUnidadesOrganizacionales(
			List<SelectItem> listUnidadesOrganizacionales) {
		this.listUnidadesOrganizacionales = listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public void setListCargos(List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public void agregarDestinatario() {
		boolean found = false;
		if (!this.persona.equals(JerarquiasLocal.INICIO)) {
			final Persona per = em.find(Persona.class, this.persona);
			if (destinatariosDocumento != null) {
				for (SelectPersonas sp : destinatariosDocumento) {
					if (sp.getPersona().getId().equals(this.persona)) {
						found = true;
						break;
					}
				}
				if (found) {
					FacesMessages.instance().add("La persona seleccionada ya pertenece al grupo");
				} else {
					this.agregaDestinatario(per);
					this.listPersonas.remove(new SelectItem(per.getId(), per
							.getNombres()
							+ " "
							+ per.getApellidoPaterno()));
				}
			}
		} else if (!grupo.equals(JerarquiasLocal.INICIO)) {
			final Persona per = em.find(Persona.class, this.grupo);
			if (this.listaPersonas.contains(per)) {
				FacesMessages.instance().add("La persona seleccionada ya pertenece al grupo");
			} else {
				this.agregaDestinatario(per);
				this.listPersonas.remove(new SelectItem(per.getId(), per
						.getNombres()
						+ " "
						+ per.getApellidoPaterno()));
			}
		} else {
			FacesMessages.instance().add("Debe seleccionar una persona");
		}
	}
	
	/**
	 * Metodo que agrega {@link Persona} como destinatario.
	 * 
	 * @param destinatario
	 *            {@link Persona}
	 */
	private void agregaDestinatario(final Persona destinatario) {
		String value = destinatario.getNombres() + " "
				+ destinatario.getApellidoPaterno();
		final Cargo car = destinatario.getCargo();
		final UnidadOrganizacional uo = car.getUnidadOrganizacional();
		final Departamento depto = uo.getDepartamento();
		final Division d = depto.getDivision();
		if ("Jefe Division".equals(car.getDescripcion())
				|| "Jefe Departamento".equals(car.getDescripcion())
				|| "Jefe Unidad".equals(car.getDescripcion())) {
			value = car.getDescripcion();
		} else {
			value += " - " + car.getDescripcion();
		}
		if (!destinatario.getCargo().getUnidadOrganizacional().getVirtual()) {
			value += " - " + uo.getDescripcion();
		} else if (!destinatario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getVirtual()) {
			value += " - " + depto.getDescripcion();
		} else {
			value += " - " + d.getDescripcion();
		}
		
		if (!this.estaDestinatario(value)) {
				destinatario.setDestinatarioConCopia(false);
				this.destinatariosDocumento.add(new SelectPersonas(value,
						destinatario));
		}
		
		if (this.estaDestinatarioSearch(destinatario.getId())) {
			FacesMessages.instance().add("El destinatario no puede repetirse");
			return;
		} 

		listOrganizacion.addAll(this.buscarOrganizaciones());
		organizacion = JerarquiasLocal.INICIO;
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
//		listDepartamento = jerarquias.getDepartamentos(SELECCIONAR);
//		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//		this.buscarUnidadesOrganizacionales();
		
		
		//unidadOrganizacional = JerarquiasLocal.INICIO;
	}

	@Override
	public void setListDestFrec(List<SelectItem> listDestFrec) {
		this.listDestFrec = listDestFrec;
	}

	@Override
	public List<SelectItem> getListDestFrec() {
		return listDestFrec;
	}
	
	/**
	 * Matodo que valida si una persona esta dentro de la lista de
	 * destinatarios.
	 * 
	 * @param nombre
	 *            {@link String}
	 * @return {@link Boolean}
	 */
	private boolean estaDestinatario(final String nombre) {
		for (SelectPersonas si : this.destinatariosDocumento) {
			if (si.getDescripcion().equals(nombre)) {
				return true;
			}

		}
		return false;
	}

	/**
	 * Matodo que valida si una persona esta dentro de la lista de
	 * destinatarios.
	 * 
	 * @param nombre
	 *            {@link String}
	 * @return {@link Boolean}
	 */
	private boolean estaDestinatarioSearch(final Long nombre) {
		for (Persona si : this.listDestinatarios) {
			if (si.getId().equals(nombre)) {
				return true;
			}
			si.getId();
		}
		return false;
	}
	
	@Override
	public Long getGrupo() {
		return grupo;
	}

	@Override
	public void setGrupo(Long grupo) {
		this.grupo = grupo;
	}
	
	@Override
	public List<SelectPersonas> getDestinatariosDocumento() {
		if (this.destinatariosDocumento == null) {
			this.destinatariosDocumento = new ArrayList<SelectPersonas>();
		}
		return destinatariosDocumento;
	}
	
	@Override
	public void eliminarDestinatarioDocumento(final String destinatario) {
		SelectPersonas spEliminar = null;
		int idx = -1;
		for (int i = 0; i < this.destinatariosDocumento.size(); i++) {
			if (this.destinatariosDocumento.get(i).getDescripcion().equals(destinatario)) {
				idx = i;
				spEliminar = this.destinatariosDocumento.get(i);
			}
		}
		if (idx > -1) {
			this.destinatariosDocumento.remove(idx);
			if (this.listPersonas.size() > 1) {
				this.listPersonas.add(new SelectItem(spEliminar.getPersona().getId(), spEliminar.getPersona()
						.getNombres()
						+ " "
						+ spEliminar.getPersona().getApellidoPaterno()));
				Collections.sort(this.listPersonas, ComparatorUtils.compararItems);
			}
		}
	}
	
	@Override
	public void eliminarGrupo(final Long id) {

		GrupoUsuario grupoUs = null;

		log.info("Id Grupo: " + id);

		final Query query = em.createNamedQuery("GrupoUsuario.findById");
		query.setParameter("id", id);

		try {
			grupoUs = (GrupoUsuario) query.getSingleResult();
		} catch (PersistenceException e) {
			log.error(ERROR_AL_OBTENER_GRUPOS, e);
		}
		
		if (grupoUs != null) {
			try {
				em.remove(grupoUs);
				this.cargarGrupos();
				FacesMessages.instance().add(OPERACIONREALIZADA);
			} catch (PersistenceException e) {
				log.error("Error al eliminar grupo: ", e);
			}
		}
	}
	
	/**
	 * 
	 */
	@Override
	public void limpiar() {
		organizacion = JerarquiasLocal.INICIO;
		division = JerarquiasLocal.INICIO;
		departamento = JerarquiasLocal.INICIO;
		unidadOrganizacional = JerarquiasLocal.INICIO;
		cargo = JerarquiasLocal.INICIO;
		persona = JerarquiasLocal.INICIO;
		listOrganizacion.clear();
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));

	}
}