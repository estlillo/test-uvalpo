package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.Plantilla;

@Local
public interface AdministradorPlantillasConvenio {

	// flujo
	public String begin();

	public void destroy();

	public String end();

	// acciones
	public void crearNuevo();

	public void modificar(Integer id);

	public void obtenerPlantilla(Integer idPlantilla);

	public void eliminarPlantilla(Plantilla p);

	public void limpiarFormulario();

	// Setters & Getters
	public Plantilla getPlantilla();

	public void setPlantilla(Plantilla plantilla);

	public List<Plantilla> getPlantillas();

	public String getNombre();

	public void setNombre(String nombre);

	public String getMateria();

	public void setMateria(String materia);
	
	public String getTitulo();
	
	public void setTitulo(String titulo);
	
	public String getPrologo();
	
	public void setPrologo(String prologo);

	public String getConsiderando();

	public void setConsiderando(String considerando);
	
	public String getTeniendoPresente();
	
	public void setTeniendoPresente(String teniendoPresente);
	
	public String getConvienen();
	
	public void setConvienen(String convienen);
}
