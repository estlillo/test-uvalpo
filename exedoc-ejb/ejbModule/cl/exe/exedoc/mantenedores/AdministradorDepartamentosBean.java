package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link AdministradorDepartamentos}.
 */
@Stateful
@Name("adminDepartamentos")
@Scope(value = ScopeType.SESSION)
public class AdministradorDepartamentosBean implements AdministradorDepartamentos {

	private static final String ID_DIVISION = "idDivision";

	private String nombreDepartamento;
	private String ciudadDepartamento;

	private Long organizacion;
	private Long organizacionCargar;
	private Long idDivision;
	private Long idDivisionCargar;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listOrganizacionCargar;
	private List<SelectItem> listDivisiones;
	private List<SelectItem> listDivisionesCargar;

	private List<Departamento> listDepartamentos;

	private Long idUltimaDepartamentoCargada;
	private int flag;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;
	
	@In(required = true)
	private Persona usuario;

	/**
	 * Constructor.
	 */
	public AdministradorDepartamentosBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("Iniciando modulo...");

		nombreDepartamento = "";
		listDivisiones = new ArrayList<SelectItem>();
		listDepartamentos = new ArrayList<Departamento>();

		listOrganizacion = this.buscarOrganizaciones();		
	
//		organizacion = usuario.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getOrganizacion().getId();
//		this.buscarDivisiones();
//		this.idDivision = usuario.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getId();
		listOrganizacionCargar = this.buscarOrganizacionesCargar();

		organizacionCargar = organizacion;
//		this.buscarDivisionesCargar();
//
//		this.idDivisionCargar = this.idDivision;
		flag = 0;
		idUltimaDepartamentoCargada = null;
		return "adminDepartamentos";
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDepartamento();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizacionesCargar() {
		this.limpiarDepartamentoCargar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final Long idOrganizacion = organizacion;
		this.limpiarDepartamento();
		organizacion = idOrganizacion;
		listDivisiones = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDivisionesCargar() {
		final long idOrganizacion = organizacionCargar;
		this.limpiarDepartamentoCargar();
		organizacionCargar = idOrganizacion;
		listDivisionesCargar = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacionCargar);
	}

	/**
	 * 
	 */
	private void limpiarDepartamento() {
		organizacion = JerarquiasLocal.INICIO;
		idDivision = JerarquiasLocal.INICIO;

		listDivisiones = new ArrayList<SelectItem>();
		listDivisiones.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void limpiarDepartamentoCargar() {
		organizacionCargar = JerarquiasLocal.INICIO;
		idDivisionCargar = JerarquiasLocal.INICIO;

		listDivisionesCargar = new ArrayList<SelectItem>();
		listDivisionesCargar.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));

		listDepartamentos.clear();
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@End
	@Override
	public String end() {
		nombreDepartamento = "";
		listOrganizacion = null;
		listOrganizacionCargar = null;
		listDivisiones = null;
		listDivisionesCargar = null;
		listDepartamentos = null;
		log.info("Finalizando modulo...");
		return "home";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDepartamentos() {
		if (idDivisionCargar.equals(JerarquiasLocal.INICIO)) {
			final StringBuilder query = new StringBuilder("SELECT do FROM Departamento do ");
			query.append("WHERE do.virtual = false and do.division.id = :idDivision");
			listDepartamentos = em.createQuery(query.toString()).setParameter(ID_DIVISION, idDivisionCargar)
					.getResultList();
		} else {
			listDepartamentos = em.createQuery("SELECT do FROM Departamento do WHERE do.division.id = :idDivision")
					.setParameter(ID_DIVISION, idDivisionCargar).getResultList();
		}
	}

	@Override
	public void buscarFicalia(){
		listDepartamentos = em.createQuery("SELECT do FROM Departamento do").getResultList();
	}
	
	@Override
	public void cargarDepartamento(final Long idDepartamento) {
		final Departamento deo = em.find(Departamento.class, idDepartamento);
		if (deo != null) {
			nombreDepartamento = deo.getDescripcion();
			ciudadDepartamento = deo.getCiudad();
			organizacion = deo.getDivision().getOrganizacion().getId();
			this.buscarDivisiones();
			idDivision = deo.getDivision().getId();
			idUltimaDepartamentoCargada = idDepartamento;
			flag = 1;
		} else {
			FacesMessages.instance().add("Error al cargar el departamento");
		}
	}
	
	@Override
	public Departamento buscar(final Long id) {
		return em.find(Departamento.class, id);
	}

	@Override
	public void crearDepartamento() {
		if (!nombreDepartamento.isEmpty()) {
			if (!idDivision.equals(JerarquiasLocal.INICIO)) {
				final Division div = em.find(Division.class, idDivision);
				final Departamento dep = new Departamento();
				dep.setConCargo(false);
				dep.setDescripcion(nombreDepartamento);
				dep.setCiudad(ciudadDepartamento);
				dep.setDivision(div);
				dep.setVirtual(false);
				em.persist(dep);
				FacesMessages.instance().add("Departamento creado exitosamente");
			} else {
				FacesMessages.instance().add("Debe ingresar mínimo una división antes de crear el departamento");
			}
		} else {
			FacesMessages.instance().add("Debe ingresar un nombre para el departamento antes de crearlo");
		}

		this.limpiarFormulario();
		listDepartamentos.clear();
	}

	@Override
	public void eliminarDepartamento(final Departamento departamento) {
		final Departamento uo = em.find(Departamento.class, departamento.getId());
		if (uo.getUnidades() == null || uo.getUnidades().size() == 0) {
			listDepartamentos.remove(departamento);
			em.remove(uo);
			this.limpiarFormulario();

			FacesMessages.instance().add("Departamento eliminado exitosamente");
		} else {
			FacesMessages.instance().add("Imposible borrar departamentos con unidades asociadas");
		}
	}

	@Override
	public void limpiarFormulario() {
		nombreDepartamento = "";
		ciudadDepartamento = "";
		listOrganizacion.clear();
		listOrganizacionCargar.clear();
		listDivisiones.clear();
		listDivisionesCargar.clear();
		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
		listOrganizacionCargar = this.buscarOrganizacionesCargar();
//		organizacionCargar = (Long) listOrganizacionCargar.get(1).getValue();
//		this.buscarDivisionesCargar();
		flag = 0;
	}

	@Override
	public void modificarDepartamento() {
		if (idUltimaDepartamentoCargada != null) {
			final Departamento uo = em.find(Departamento.class, idUltimaDepartamentoCargada);

			if (!nombreDepartamento.isEmpty()) {
				if (!uo.getDescripcion().equals(nombreDepartamento)) {
					uo.setDescripcion(nombreDepartamento);
				}
				
				if (ciudadDepartamento.trim().length() >0) {
					uo.setCiudad(ciudadDepartamento);
				}
				
				if (!idDivision.equals(JerarquiasLocal.INICIO)) {

					final Division div = em.find(Division.class, idDivision);

					uo.setDivision(div);
					em.merge(uo);

					this.limpiarFormulario();
					listDepartamentos.clear();
					idUltimaDepartamentoCargada = null;
					flag = 0;

					FacesMessages.instance().add("Departamento modificado exitosamente");
				} else {
					FacesMessages.instance().add("El Departamento debe estar en una Organización y en una División");
				}
			} else {
				FacesMessages.instance().add("El nombre del Departamento no puede quedar en blanco");
			}
		}
	}

	@Override
	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	@Override
	public void setNombreDepartamento(final String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	@Override
	public Long getIdDivision() {
		return idDivision;
	}

	@Override
	public void setIdDivision(final Long idDivision) {
		this.idDivision = idDivision;
	}

	@Override
	public List<SelectItem> getListDivisiones() {
		return listDivisiones;
	}

	@Override
	public void setListDivisiones(final List<SelectItem> listDivisiones) {
		this.listDivisiones = listDivisiones;
	}

	@Override
	public List<Departamento> getListDepartamentos() {
		return listDepartamentos;
	}

	@Override
	public void setListDepartamentos(final List<Departamento> listDepartamentos) {
		this.listDepartamentos = listDepartamentos;
	}

	@Override
	public Long getIdDivisionCargar() {
		return idDivisionCargar;
	}

	@Override
	public void setIdDivisionCargar(final Long idDivisionCargar) {
		this.idDivisionCargar = idDivisionCargar;
	}

	@Override
	public List<SelectItem> getListDivisionesCargar() {
		return listDivisionesCargar;
	}

	@Override
	public void setListDivisionesCargar(final List<SelectItem> listDivisionesCargar) {
		this.listDivisionesCargar = listDivisionesCargar;
	}

	@Override
	public int getFlag() {
		return flag;
	}

	@Override
	public void setFlag(final int flag) {
		this.flag = flag;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getOrganizacionCargar() {
		return organizacionCargar;
	}

	@Override
	public void setOrganizacionCargar(final Long organizacionCargar) {
		this.organizacionCargar = organizacionCargar;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		if (listOrganizacion == null) {
			listOrganizacion = new ArrayList<SelectItem>();
		}
		if (listOrganizacion.size() == 0) {
			listOrganizacion.addAll(this.buscarOrganizaciones());
			organizacion = (Long) listOrganizacion.get(1).getValue();
			this.buscarDivisiones();
			idDivision = (Long) listDivisiones.get(1).getValue();
			this.buscarDepartamentos();
			
		}
		
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacionCargar() {
		return listOrganizacionCargar;
	}

	@Override
	public void setListOrganizacioCargarn(final List<SelectItem> paramListOrganizacionCargar) {
		this.listOrganizacionCargar = paramListOrganizacionCargar;
	}

	@Override
	public String getCiudadDepartamento() {
		return ciudadDepartamento;
	}

	@Override
	public void setCiudadDepartamento(String ciudadDepartamento) {
		this.ciudadDepartamento = ciudadDepartamento;
	}
	
	
}
