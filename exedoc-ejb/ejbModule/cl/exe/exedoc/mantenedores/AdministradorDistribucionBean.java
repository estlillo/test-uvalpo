package cl.exe.exedoc.mantenedores;

import java.io.IOException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoFirma;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.FilaBandejaSalida;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.SelectPersonas;

/**
 * Implementacion de {@link AdministradorPersona}. TODO: COMPLEXITY
 */
@Stateful
@Name("administradorDistribucion")
@Scope(value = ScopeType.SESSION)
public class AdministradorDistribucionBean implements AdministradorDistribucion {

	private static final String ID = "id";
	private Persona persona;
	private List<Persona> personas;

	@Out(required = false, scope = ScopeType.SESSION)
	@In(required = false, scope = ScopeType.SESSION)
	private String user;

	@In(required = true)
	private Persona usuario;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidadesOrganizacionales;
	private List<SelectItem> listCargos;
	
	private HashMap<Long,Boolean> estados;

	// Datos Destinatario Expediente
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;// -1
	private static final String INICIO = "<<Seleccionar>>";
	private Map<Long, Boolean> selectedAnular = new HashMap<Long, Boolean>();

	// Datos KeyStore
	private byte[] keyStore;

	private Integer tipoFirma;

	@Logger
	private Log log;

	/**
	 * Constructor.
	 */
	public AdministradorDistribucionBean() {
		super();
	}

	@Override
	@Begin(join = true)
	public String begin() {
		estados = new HashMap<Long, Boolean>();
		listOrganizacion = new ArrayList<SelectItem>();
		listDivision = new ArrayList<SelectItem>();
		listDepartamento = new ArrayList<SelectItem>();
		listUnidadesOrganizacionales = new ArrayList<SelectItem>();
		listCargos = new ArrayList<SelectItem>();
		personas = new ArrayList<Persona>();
		user = "";
		persona = null;
		tipoFirma = 1;
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos(INICIO);
		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getId());
		this.buscarUnidadesOrganizacionales();
		return "administradorDistribucion";
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUsuario() {
		final StringBuilder sql = new StringBuilder(
				"SELECT p FROM Persona p WHERE ");
		String nombre = persona.getNombres();
		String apellido = persona.getApellidoPaterno();

		final String strand = "%') and ";

		if (user != null && user.length() != 0) {
			sql.append("upper(p.usuario) like upper('%").append(user)
					.append(strand);
		}
		if (nombre != null && nombre.length() != 0) {
			sql.append("upper(p.nombres) like upper('%").append(nombre)
					.append(strand);
		}
		if (apellido != null && apellido.length() != 0) {
			sql.append("upper(p.apellidoPaterno) like upper('%")
					.append(apellido).append(strand);
		}
		sql.append(" (p.externo is null or p.externo = false) and ");
		sql.append(" 1 = 1 ");

		final Query query = em.createQuery(sql.toString());
		personas = query.getResultList();
		user = "";
		nombre = "";
		apellido = "";
	}

	/*
	 * Metodo permite guardar/modificar una Persona. TODO: EXCEPTION/RETURN
	 */
	@Override
	public void guardarPersona() {

		try{
			for (Persona p : this.personas) {
				p.setDistribucion(this.estados.get(p.getId()));
				em.merge(p);
			}
		}
		catch(Exception ex){FacesMessages.instance().add("No se pudo guardar una persona.");}
		
		FacesMessages.instance().add("Se guardaron las personas seleccionadas");
	}

	
	
	/*
	 * (non-Javadoc) TODO: EXCEPTION/RETURN
	 */
	@Override
	public void importarPersona() {
		// Obtener RUT.
		// Normalizar RUT
		// Verificar si RUT existe
		// Si existe preguntar si desea actualizar la informacion
		// Si respuesta anterior es OK
		// Invocar Interfaz que llena persona.

		final String prmRut = persona.getRut();

		if (prmRut == null) {
			FacesMessages.instance().add("RUT : vacio ");
			return;
		}

		final ImportadorUsuario iiUsuario = FactoryImportadorUsuario
				.getImportadorUsuarios();

		final String sRut = iiUsuario.normalizarRUT(prmRut);

		if (!this.validarRutNoRegistrado(sRut, false, null)) {

			FacesMessages.instance().add("RUT ya registrado ");
			return;
		}
		Integer res = null;

		try {
			res = iiUsuario.getPersona(sRut, persona, em);
		} catch (Exception e) {
			FacesMessages.instance().add("Error al buscar usuario.");
			return;
		}
		if (res != null) {
			FacesMessages.instance().add(
					"Error al importar usuario desde el sistema de origen.");
		}

		if (persona != null && persona.getCargo() != null) {

			cargo = persona.getCargo().getId();
		}
	}

	@Override
	public void listener(final UploadEvent event) throws Exception {
		final UploadItem item = event.getUploadItem();
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		keyStore = data;
		persona.setKeyStore(keyStore);
	}

	@Override
	public void listenerImagenFirma(final UploadEvent event) throws Exception {
		final UploadItem item = event.getUploadItem();
		final byte[] data = org.apache.commons.io.FileUtils
				.readFileToByteArray(item.getFile());
		persona.setImagenFirma(data);
	}

	@Override
	public void verImagenFirma(final OutputStream output, final Object object)
			throws IOException {
		if (persona.getImagenFirma() != null) {
			output.write(persona.getImagenFirma());
		}
	}

	@Override
	public long getTime() {
		return (new Date()).getTime();
	}

	/**
	 * TODO: RETURN/EXCEPTION
	 * 
	 * @param usuario
	 *            String
	 * @param editar
	 *            boolean
	 * @param idPersona
	 *            Long
	 * @return boolean
	 */
	private boolean validarUsuarioRegistrado(final String usuario,
			final boolean editar, final Long idPersona) {
		if (persona.getUsuario() != null && !persona.getUsuario().isEmpty()) {
			StringBuilder consulta = new StringBuilder();
			consulta.append("select p from Persona p where upper(p.usuario) = upper('");

			if (!editar) {
				consulta.append(persona.getUsuario() + "')");
			} else {
				consulta.append(persona.getUsuario()
						+ "') and p.idPersona not in (" + idPersona + ")");
			}
			final Query query = em.createQuery(consulta.toString());
			Object o;
			try {
				o = query.getSingleResult();
			} catch (Exception e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
					FacesMessages
							.instance()
							.add("El Usuario ingresado ya se encuentra registrado: #0",
									p.getUsuario());
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Metodo validar Rut No Registrado. TODO: CYCLOMATIC
	 * 
	 * @param rut
	 *            {@link String}
	 * @param editar
	 *            {@link Boolean}
	 * @param idPersona
	 *            {@link Long}
	 * @return {@link Boolean}
	 */
	private boolean validarRutNoRegistrado(final String rut,
			final boolean editar, final Long idPersona) {
		Boolean estado = Boolean.TRUE.booleanValue();
		if (rut != null && !"".equals(rut)) {
			String consulta = null;
			if (!editar) {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "')";
			} else {
				consulta = "select p from Persona p where upper(p.rut)= upper('"
						+ rut + "') and p.idPersona not in (" + idPersona + ")";
			}
			final Query query = em.createQuery(consulta);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (NoResultException e) {
				o = null;
			} catch (NonUniqueResultException e) {
				o = null;
				estado = Boolean.FALSE.booleanValue();
			} catch (IllegalStateException e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
					FacesMessages.instance().add(
							"El Rut ingresado ya se encuentra registrado: #0",
							p.getRut());
					estado = Boolean.FALSE;
				}
			}
		} else {
			estado = Boolean.FALSE.booleanValue();
		}
		return estado;
	}

	/**
	 * TODO: EXCEPTION/RETURN.
	 * 
	 * @param idPersona
	 *            Long
	 * @param editar
	 *            boolean
	 * @param idPersonaExistente
	 *            Long
	 * @return boolean
	 */
	private boolean validarIdPersonaRegistrada(final Long idPersona,
			final boolean editar, final Long idPersonaExistente) {
		if (persona.getIdPersona() != null) {
			String consulta = "select p from Persona p where p.idPersona= '";
			if (!editar) {
				consulta += idPersona + "'";
			} else {
				consulta += idPersona + "' and p.idPersona not in ("
						+ idPersonaExistente + ")";
			}
			log.info(consulta);
			final Query query = em.createQuery(consulta);
			Object o;
			try {
				o = query.getSingleResult();
			} catch (Exception e) {
				o = null;
			}
			if (o != null) {
				final Persona p = (Persona) o;
				if (p != null) {
					FacesMessages.instance().add(
							"El Id ingresado ya se encuentra registrado: #0",
							p.getIdPersona());
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return List<SelectItem>
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division mDivision = divisiones.get(0);
			if (mDivision.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL, division);
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		this.personas = new ArrayList<Persona>();
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento mDepartamento = departamentos.get(0);
			if (mDepartamento.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
	}

	@Override
	public void buscarCargos() {
		this.personas = new ArrayList<Persona>();
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
	}

	@Override
	public void limpiarFormulario() {
		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		personas = null;
		persona = null;
		tipoFirma = -1;
	}

	/**
	 * 
	 */
	private void limpiarDestinatario() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));

	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarPersonas() {
		estados = new HashMap<Long, Boolean>();
		if (!cargo.equals(JerarquiasLocal.INICIO)) {
			final Query query = em
					.createQuery("Select p From Persona p Where p.cargo.id = ?");
			query.setParameter(1, cargo);
			personas = query.getResultList();
			for (Persona p : personas) {
				estados.put(p.getId(), p.getDistribucion() == null ? true: p.getDistribucion());
			}
		}
	}

	@Override
	public void modificar(final Long idPersona) {

		// Se realiza la busqueda de la Persona por medio del usuario
		persona = em.find(Persona.class, idPersona);

		listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos(INICIO);
		departamento = persona.getCargo().getUnidadOrganizacional().getDepartamento().getId();
		this.buscarUnidadesOrganizacionales();
		unidadOrganizacional = persona.getCargo().getUnidadOrganizacional().getId();
		this.buscarCargos();
		cargo = persona.getCargo().getId();

		//
		// // Se obtiene jerarquia a la cual pertenece aquella persona.
		// final Cargo c = persona.getCargo();
		//
		// // Si no tiene cargos asociados, entonces no se busca nada asociado
		// al
		// // cargo (jerarquia)
		// if (c == null) {
		// return;
		// }
		//
		// final UnidadOrganizacional uni = c.getUnidadOrganizacional();
		// final Departamento dep = uni.getDepartamento();
		// final Division div = dep.getDivision();
		// final Organizacion org = div.getOrganizacion();
		//
		// // Se setea la organizacion correspondiente y se carga la lista de
		// // Divisiones.
		// organizacion = org.getId();
		// this.buscarDivisiones();
		//
		// // Se setea la division correspondiente y se carga la lista de
		// Deptos.
		// division = div.getId();
		// this.buscarDepartamentos();
		//
		// // Se setea el Depto correspondiente y se carga la lista de Unidades.
		// departamento = dep.getId();
		// this.buscarUnidadesOrganizacionales();
		//
		// // Se setea la Unidad correspondiente y se carga la lista de Cargos.
		// unidadOrganizacional = uni.getId();
		//
		// this.buscarCargos();
		//
		// cargo = c.getId();

		final TipoFirma tf = persona.getTipoFirma();
		if (tf != null) {
			tipoFirma = persona.getTipoFirma().getId();
		} else {
			tipoFirma = 1;
		}

	}

	@Override
	public Persona getPersona() {
		if (persona == null) {
			persona = new Persona();
		}
		return persona;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public List<Persona> getPersonas() {
		return personas;
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	public String getUser() {
		return user;
	}

	@Override
	public void setUser(final String user) {
		this.user = user;
	}

	@Override
	public byte[] getKeyStore() {
		return keyStore;
	}

	@Override
	public void setKeyStore(final byte[] keyStore) {
		this.keyStore = keyStore;
	}

	@Override
	public void actualizar() {

	}

	@Override
	public Integer getTipoFirma() {
		return tipoFirma;
	}

	@Override
	public void setTipoFirma(final Integer tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<Persona> getPersonas(String buscado, List<SelectPersonas> list) {

		final StringBuilder sql = new StringBuilder(
				"SELECT p FROM Persona p WHERE ");

		final String strand = "%')";

		if (buscado != null && buscado.length() != 0) {
			sql.append("upper(p.nombres) like upper('").append(buscado)
					.append(strand);
			if (list.size() > 0) {
				sql.append(" AND p.id NOT IN (");

				int i = 0;
				for (SelectPersonas persona : list) {
					if (i != 0)
						sql.append(persona.getPersona().getId() + ",");
					else
						sql.append(persona.getPersona().getId());
				}
				sql.append(")");
			}
		}

		final Query query = em.createQuery(sql.toString());

		personas = query.getResultList();
		return personas;
	}
	
	@Override
	public HashMap<Long, Boolean> getEstados() {
		return estados;
	}
	@Override
	public void setEstados(HashMap<Long, Boolean> estados) {
		this.estados = estados;
	}
	
	@Override
	public Boolean setPersona(Long id, Boolean estado){ 
		Boolean estadoFinal = true;
		if(estado == null)estadoFinal = true;
		else estadoFinal = estado;
		if(estados.containsKey(id))estados.remove(id);
		//cambiaEstadoPersona( id, estadoFinal);
		
		estados.put(id, estadoFinal);
		return estadoFinal;
	}

	public void cambiaEstadoPersona(Long id, Boolean estado){
		List<Persona> lista = new ArrayList<Persona>();
		for (Persona p : this.personas) {
			Boolean flag = true;
			if(id.equals(p.getId())){
				p.setDistribucion(estado);
				lista.add(p);
				flag = false;
			}
			if (flag)lista.add(p);
		}
		this.personas = new ArrayList<Persona>();
		this.personas = lista;
	}

	public Map<Long, Boolean> getSelectedAnular() {
		return selectedAnular;
	}

	public void setSelectedAnular(Map<Long, Boolean> selectedAnular) {
		this.selectedAnular = selectedAnular;
	}

	
	
	
}
