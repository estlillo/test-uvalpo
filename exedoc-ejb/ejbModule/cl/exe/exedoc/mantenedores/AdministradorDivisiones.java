package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Division;

/**
 * @author Administrator
 */
@Local
public interface AdministradorDivisiones {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void crearDivision();

	/**
	 * 
	 */
	void modificarDivision();

	/**
	 * @param idDivision Long
	 */
	void cargarDivision(Long idDivision);

	/**
	 * @param idDivision Division
	 */
	void eliminarDivision(Division idDivision);

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * @return String
	 */
	String getNombreDivision();

	/**
	 * @param nombreDivision String
	 */
	void setNombreDivision(String nombreDivision);

	/**
	 * @return Long
	 */
	Long getIdDivision();

	/**
	 * @param idDivision Long
	 */
	void setIdDivision(Long idDivision);

	/**
	 * @return Long
	 */
	Long getIdDivisionCargar();

	/**
	 * @param idDivisionCargar Long
	 */
	void setIdDivisionCargar(Long idDivisionCargar);

	/**
	 * @return int
	 */
	int getFlag();

	/**
	 * @param flag int
	 */
	void setFlag(int flag);

	/**
	 * @return Long
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion Long
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return Long
	 */
	Long getOrganizacionCargar();

	/**
	 * @param organizacionCargar Long
	 */
	void setOrganizacionCargar(Long organizacionCargar);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion List<SelectItem>
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacionCargar();

	/**
	 * @param listOrganizacionCargar List<SelectItem>
	 */
	void setListOrganizacioCargarn(List<SelectItem> listOrganizacionCargar);

	/**
	 * @return List<Division>
	 */
	List<Division> getListDivisiones();

	/**
	 * @param listDivisiones List<Division>
	 */
	void setListDivisiones(List<Division> listDivisiones);

	/**
	 * 
	 */
	void buscarDivisiones();

}
