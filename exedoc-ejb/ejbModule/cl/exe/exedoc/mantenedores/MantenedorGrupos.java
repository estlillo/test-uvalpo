package cl.exe.exedoc.mantenedores;

import java.util.List;
import java.util.Set;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.GrupoUsuario;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.SelectPersonas;



/**
 * @author Sergio
 * 
 */
public interface MantenedorGrupos
{
	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return {@link String}
	 */
	String begin();

	GrupoUsuario getGrupoUsuario();

	void setGrupoUsuario(GrupoUsuario grupoUsuario);

	List<GrupoUsuario> getListGrupos();

	void setListGrupos(List<GrupoUsuario> listGrupos);

	void guardarGrupo();

	boolean crear(GrupoUsuario grupoUsuario);

	boolean actualizar(GrupoUsuario grupoUsuario);

	void editarGrupo(Long id);

	Boolean validarGrupo();

	void limpiarGrupo();

	void setListaPersonas(List<Persona> listaPersonas);

	List<Persona> getListaPersonas();

	void setDestinatario(String destinatario);

	String getDestinatario();

	Set<Persona> getListDestinatarios();

	void setListDestinatarios(Set<Persona> listDestinatarios);

	String end();

	void buscarDivisiones();

	void buscarDepartamentos();

	void buscarUnidadesOrganizacionales();

	void buscarCargos();

	void buscarPersonas();

	void setListPersonas(List<SelectItem> listPersonas);

	List<SelectItem> getListPersonas();

	void setListCargos(List<SelectItem> listCargos);

	List<SelectItem> getListCargos();

	void setListUnidadesOrganizacionales(
			List<SelectItem> listUnidadesOrganizacionales);

	List<SelectItem> getListUnidadesOrganizacionales();

	void setListDepartamento(List<SelectItem> listDepartamento);

	List<SelectItem> getListDepartamento();

	void setListDivision(List<SelectItem> listDivision);

	List<SelectItem> getListDivision();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	List<SelectItem> getListOrganizacion();

	void setPersona(Long persona);

	Long getPersona();

	void setCargo(Long cargo);

	Long getCargo();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getUnidadOrganizacional();

	void setDepartamento(Long departamento);

	Long getDepartamento();

	void setDivision(Long division);

	Long getDivision();

	void setOrganizacion(Long organizacion);

	Long getOrganizacion();

	void agregarDestinatario();

	void setListDestFrec(List<SelectItem> listDestFrec);

	List<SelectItem> getListDestFrec();

	Long getGrupo();

	void setGrupo(Long grupo);

	List<SelectPersonas> getDestinatariosDocumento();

	void eliminarDestinatarioDocumento(String destinatario);

	void eliminarGrupo(Long id);
	
	void limpiar();
}
