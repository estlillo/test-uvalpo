package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;

import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.DependenciaExterna;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

/**
 * Implementacion de {@link AdministradorDependenciasExternas}.
 */
@Stateful
@Name("administradorDependenciasExternas")
public class AdministradorDependenciasExternasBean implements
		AdministradorDependenciasExternas {

	private static final String LA_DESCRIPCION_INGRESADA_YA_EXISTE = "La descripción ingresada ya existe.";
	private static final String DESCRIPCION = "descripcion";
	private static final int LARGO_MAXIMO = 99;
	private DependenciaExterna dependencia;
	private List<DependenciaExterna> dependencias;

	@EJB
	private JerarquiasLocal jerarquias;


	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	private Persona usuario;


	/**
	 * Constructor.
	 */
	public AdministradorDependenciasExternasBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		this.buscar();
		dependencia = new DependenciaExterna();
		return "administradorDependenciasExternas";
	}

	/*
	 * (non-Javadoc) TODO: CYCLOMATIC
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void crearNuevo() {
		List<DependenciaExterna> listDe;
		Boolean estado = Boolean.TRUE;
		if (dependencia != null) {
			String valor = dependencia.getDescripcion();
			if (valor != null && valor.trim().isEmpty()) {
				String detalle = "Debe ingresar un nombre para la Dependencia Externa.";
				FacesMessages.instance().add(detalle);
				estado = Boolean.FALSE;
			}
			int largo = valor.trim().length();
			if (valor != null && !valor.trim().isEmpty() && largo >= LARGO_MAXIMO) {
				String detalle = String.format("No puede ingresar más de %s carácteres, en el campo \"Nombre\"", LARGO_MAXIMO);
				FacesMessages.instance().add(detalle);
				estado = Boolean.FALSE;
			}

			if (estado) {
				if (dependencia.getId() == null) {
					listDe = em
							.createNamedQuery("DependenciaExterna.findByDescripcion")
							.setParameter(DESCRIPCION,
									valor.trim().toUpperCase())
							.getResultList();
					if (listDe != null && !listDe.isEmpty()) {
						FacesMessages.instance()
								.add(LA_DESCRIPCION_INGRESADA_YA_EXISTE);
						return;
					}
					em.persist(dependencia);
					FacesMessages.instance().add(
							"Dependencia Externa creada con éxito.");
				} else {
					listDe = em
							.createNamedQuery(
									"DependenciaExterna.findByDescripcionDistinctId")
							.setParameter(DESCRIPCION,
									valor.trim().toUpperCase())
							.setParameter("id", dependencia.getId()).getResultList();
					if (listDe != null && !listDe.isEmpty()) {
						FacesMessages.instance()
								.add(LA_DESCRIPCION_INGRESADA_YA_EXISTE);
						return;
					}
					em.merge(dependencia);
					FacesMessages.instance().add(
							"Dependencia Externa modificada con éxito.");
				}
				this.buscar();
				this.dependencia = new DependenciaExterna();
			}
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void buscar() {
		final Query query = em.createNamedQuery("DependenciaExterna.findByAllInclusiveNotVisible");
		this.dependencias = query.getResultList();
	}

	@Override
	public void modificar(final Long id) {
		this.dependencia = em.find(DependenciaExterna.class, id);

	}

	@Override
	public void cancelar() {
		this.setDependencia(new DependenciaExterna());
	}
	
	@Override
	public void eliminarDepenedencia(final Long id) {
		final DependenciaExterna dependencia = this.buscar(id);
		
		if (dependencia != null) {
			dependencia.setVigente(false);
			em.merge(dependencia);
			this.begin();
			FacesMessages.instance().add("Dependencia Eliminada");
		} else {
			FacesMessages.instance().add("Ha ocurrido un error al eliminar la Dependencia Externa.");
		}
	}
	
	@Override
	public DependenciaExterna buscar(final Long id) {
		return em.find(DependenciaExterna.class, id);
	}

	@Override
	@Destroy
	@Remove
	public void destroy() {
	}

	@Override
	@End
	public String end() {
		return "home";
	}

	@Override
	public DependenciaExterna getDependencia() {
		return dependencia;
	}

	@Override
	public void setDependencia(final DependenciaExterna dependencia) {
		this.dependencia = dependencia;
	}

	@Override
	public List<DependenciaExterna> getDependencias() {
		return dependencias;
	}


}
