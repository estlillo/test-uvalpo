package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Cargo;

/**
 * @author Administrator
 */
@Local
public interface AdministradorCargo {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * Metodo permite guardar un Cargo, teniendo como obligatorios todos los campos desplegados por pantalla
	 * (descripcion, Organizacion, Division, Departamento, Unidad Organizacional). El campo cargoDefault se encuentra en
	 * desuso para este sistema, por lo que debe ir por defecto como false.
	 */
	void guardarCargo();

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * 
	 */
	void buscarDepartamentos();

	/**
	 * 
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * 
	 */
	void buscarCargo();

	/**
	 * 
	 */
	void encontrarCargos();

	/**
	 * @param idCargo Long
	 */
	void modificar(Long idCargo);

	/**
	 * @param idCargo Long
	 */
	void eliminar(Long idCargo);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDivision();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListUnidadesOrganizacionales();

	/**
	 * @return Long
	 */
	Long getDivision();

	/**
	 * @param division Long
	 */
	void setDivision(Long division);

	/**
	 * @return Long
	 */
	Long getDepartamento();

	/**
	 * @param departamento Long
	 */
	void setDepartamento(Long departamento);

	/**
	 * @return Long
	 */
	Long getUnidadOrganizacional();

	/**
	 * @param unidadOrganizacional Long
	 */
	void setUnidadOrganizacional(Long unidadOrganizacional);

	/**
	 * @return Cargo
	 */
	Cargo getCargo();

	/**
	 * @param cargo Cargo
	 */
	void setCargo(Cargo cargo);

	/**
	 * @return String
	 */
	String getDescripcion();

	/**
	 * @param descripcion String
	 */
	void setDescripcion(String descripcion);

	/**
	 * @return List<Cargo>
	 */
	List<Cargo> getCargos();

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * @return Long
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion Long
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion List<SelectItem>
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);
}
