package cl.exe.exedoc.mantenedores;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.DestinatariosFrecuentes;
import cl.exe.exedoc.entity.Persona;

/**
 * @author Administrator
 */
@Local
public interface AdministradorListasDestinatariosFrecuentes {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void agregarPersona();

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * @param persona Persona
	 */
	void eliminarPersona(Persona persona);

	/**
	 * @param idLista Long
	 */
	void cargarLista(Long idLista);

	/**
	 * @param lista DestinatariosFrecuentes
	 */
	void eliminarLista(DestinatariosFrecuentes lista);

	/**
	 * 
	 */
	void buscarDeptos();

	/**
	 * 
	 */
	void buscarUnidades();

	/**
	 * 
	 */
	void buscarCargos();

	/**
	 * 
	 */
	void buscarPersonas();

	/**
	 * 
	 */
	void crearLista();

	/**
	 * 
	 */
	void modificarLista();

	/**
	 * @return Sting
	 */
	String getNombreLista();

	/**
	 * @param nombreLista String
	 */
	void setNombreLista(String nombreLista);

	/**
	 * @return Long
	 */
	Long getIdDivision();

	/**
	 * @param idDivision Long
	 */
	void setIdDivision(Long idDivision);

	/**
	 * @return Long
	 */
	Long getIdDepto();

	/**
	 * @param idDepto Long
	 */
	void setIdDepto(Long idDepto);

	/**
	 * @return Long
	 */
	Long getIdUnidad();

	/**
	 * @param idUnidad Long
	 */
	void setIdUnidad(Long idUnidad);

	/**
	 * @return Long
	 */
	Long getIdCargo();

	/**
	 * @param idCargo Long
	 */
	void setIdCargo(Long idCargo);

	/**
	 * @return Long
	 */
	Long getIdPersona();

	/**
	 * @param idPersona Long
	 */
	void setIdPersona(Long idPersona);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDivisiones();

	/**
	 * @param listDivisiones List<SelectItem>
	 */
	void setListDivisiones(List<SelectItem> listDivisiones);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDeptos();

	/**
	 * @param listDeptos List<SelectItem>
	 */
	void setListDeptos(List<SelectItem> listDeptos);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListUnidades();

	/**
	 * @param listUnidades List<SelectItem>
	 */
	void setListUnidades(List<SelectItem> listUnidades);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListCargos();

	/**
	 * @param listCargos List<SelectItem>
	 */
	void setListCargos(List<SelectItem> listCargos);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListPersonas();

	/**
	 * @param listPersonas List<SelectItem>
	 */
	void setListPersonas(List<SelectItem> listPersonas);

	/**
	 * return {@link List} of {@link Persona}
	 */
	List<Persona> getListDestinatarios();

	/**
	 * @param listDestinatarios Set<Persona>
	 */
	void setListDestinatarios(List<Persona> listDestinatarios);

	/** 
	 * @return List<DestinatariosFrecuentes>
	 */
	List<DestinatariosFrecuentes> getListListas();

	/**
	 * @param listListas List<DestinatariosFrecuentes>
	 */
	void setListListas(List<DestinatariosFrecuentes> listListas);

	/**
	 * @return int
	 */
	int getFlag();

	/**
	 * @param flag int
	 */
	void setFlag(int flag);

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * @return Long
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion Long
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion List<SelectItem>
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);
}
