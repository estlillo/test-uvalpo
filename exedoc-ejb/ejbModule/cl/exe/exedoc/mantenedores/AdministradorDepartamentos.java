package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Departamento;

/**
 * @author Administrator
 */
@Local
public interface AdministradorDepartamentos {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * 
	 */
	void buscarDivisionesCargar();

	/**
	 * 
	 */
	void buscarDepartamentos();

	/**
	 * 
	 */
	void crearDepartamento();

	/**
	 * 
	 */
	void modificarDepartamento();

	/**
	 * @param idDepartamento Long
	 */
	void cargarDepartamento(Long idDepartamento);

	/**
	 * @param departamento Departamento
	 */
	void eliminarDepartamento(Departamento departamento);

	/**
	 * 
	 */
	void limpiarFormulario();

	/**
	 * @return String
	 */
	String getNombreDepartamento();

	/**
	 * @param nombreDepartamento String
	 */
	void setNombreDepartamento(String nombreDepartamento);

	/**
	 * @return Long
	 */
	Long getIdDivision();

	/**
	 * @param idDivision Long
	 */
	void setIdDivision(Long idDivision);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDivisiones();

	/**
	 * @param listDivisiones List<SelectItem>
	 */
	void setListDivisiones(List<SelectItem> listDivisiones);

	/**
	 * @return List<Departamento>
	 */
	List<Departamento> getListDepartamentos();

	/**
	 * @param listDepartamentos List<Departamento>
	 */
	void setListDepartamentos(List<Departamento> listDepartamentos);

	/**
	 * @return Long
	 */
	Long getIdDivisionCargar();

	/**
	 * @param idDivisionCargar Long
	 */
	void setIdDivisionCargar(Long idDivisionCargar);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListDivisionesCargar();

	/**
	 * @param listDivisionesCargar List<SelectItem>
	 */
	void setListDivisionesCargar(List<SelectItem> listDivisionesCargar);

	/**
	 * @return int
	 */
	int getFlag();

	/**
	 * @param flag int
	 */
	void setFlag(int flag);

	/**
	 * @return Long
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion Long
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * @return Long
	 */
	Long getOrganizacionCargar();

	/**
	 * @param organizacionCargar Long
	 */
	void setOrganizacionCargar(Long organizacionCargar);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion List<SelectItem>
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);

	/**
	 * @return List<SelectItem>
	 */
	List<SelectItem> getListOrganizacionCargar();

	/**
	 * @param listOrganizacionCargar List<SelectItem>
	 */
	void setListOrganizacioCargarn(List<SelectItem> listOrganizacionCargar);

	/**
	 * @param id {@link Long}
	 * @return {@link Departamento}
	 */
	Departamento buscar(Long id);

	void buscarFicalia();

	String getCiudadDepartamento();

	void setCiudadDepartamento(String ciudadDepartamento);
}
