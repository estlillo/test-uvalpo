package cl.exe.exedoc.mantenedores;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.CamposPlantilla;
import cl.exe.exedoc.entity.Plantilla;
import cl.exe.exedoc.entity.TipoContenido;
import cl.exe.exedoc.entity.TipoPlantilla;

@Stateful
@Name("administradorPlantillasConvenio")
public class AdministradorPlantillasConvenioBean implements AdministradorPlantillasConvenio {

	private Plantilla plantilla;
	private List<Plantilla> plantillas;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	private String nombre;
	private String materia;
	private String titulo;
	private String prologo;
	private String considerando;
	private String teniendoPresente;
	private String convienen;
	
	@Begin(join = true)
	public String begin() {
		plantilla = new Plantilla();
		plantillas = new ArrayList<Plantilla>();

		this.nombre = "";
		this.materia = "";
		this.titulo = "";
		this.prologo = "";
		this.considerando = "";
		this.teniendoPresente = "";
		this.convienen = "";

		this.buscar();
		return "administradorPlantillasConvenio";
	}

	public void crearNuevo() {
		if (plantilla != null && plantilla.getId() != null) {
			log.info("Modificando plantilla...");
			plantilla.setNombre(this.getNombre());
			this.actualizarCamposPlantilla(plantilla.getId());

			em.merge(plantilla);
			FacesMessages.instance().add("Se modificó la plantilla en el sistema");
			log.info("Se modificó la plantilla en el sistema");

			this.limpiarFormulario();
		} else {
			log.info("Creando plantilla...");
			boolean ok = true;
			if (this.getNombre().equals("")) {
				FacesMessages.instance().add("Ingrese el nombre");
				log.info("Nombre no ingresado");
				ok = false;
			}
			if (this.getMateria().equals("")) {
				FacesMessages.instance().add("Ingrese la materia");
				log.info("Materia no ingresada");
				ok = false;
			}
			if (this.getTitulo().equals("")) {
				FacesMessages.instance().add("Ingrese el título");
				log.info("Título no ingresado");
				ok = false;
			}
			if (this.getPrologo().equals("")) {
				FacesMessages.instance().add("Ingrese el prólogo");
				log.info("Prólogo no ingresado");
				ok = false;
			}
			if (this.getConsiderando().equals("")) {
				FacesMessages.instance().add("Ingrese los considerandos");
				log.info("Considerandos no ingresados");
				ok = false;
			}
			if (this.getTeniendoPresente().equals("")) {
				FacesMessages.instance().add("Ingrese los teniendo presente");
				log.info("Teniendo presentes no ingresados");
				ok = false;
			}
			if (this.getConvienen().equals("")) {
				FacesMessages.instance().add("Ingrese los convienen");
				log.info("Convienen no ingresados");
				ok = false;
			}
			if (ok) {
				plantilla.setNombre(this.getNombre());
				CamposPlantilla campos;
				List<CamposPlantilla> listCampos = new ArrayList<CamposPlantilla>();

				if (this.getMateria() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.MATERIA));
					campos.setContenido(this.materia);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese la materia");
					log.info("Ingrese la materia");
				}

				if (this.getTitulo() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.TITULO));
					campos.setContenido(this.titulo);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese el título");
					log.info("Ingrese el título");
				}
				
				if (this.getPrologo() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.PROLOGO));
					campos.setContenido(this.prologo);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese el prólogo");
					log.info("Ingrese el prólogo");
				}

				if (this.getConsiderando() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.CONSIDERANDO));
					campos.setContenido(this.considerando);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				}

				if (this.getTeniendoPresente() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.TENIENDO_PRESENTE));
					campos.setContenido(this.teniendoPresente);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese los teniendo presente");
					log.info("Ingrese los teniendo presente");
				}

				if (this.getConvienen() != null) {
					campos = new CamposPlantilla();
					campos.setTipoContenido(new TipoContenido(TipoContenido.CONVIENEN));
					campos.setContenido(this.convienen);
					campos.setPlantilla(plantilla);
					listCampos.add(campos);
					campos = null;
				} else {
					FacesMessages.instance().add("Ingrese los convienen");
					log.info("Ingrese los convienen");
				}

				plantilla.setCamposPlantilla(listCampos);
				plantilla.setTipoPlantilla(new TipoPlantilla(TipoPlantilla.CONVENIO));

				em.persist(plantilla);
				FacesMessages.instance().add("Se agregó la plantilla al sistema");
				log.info("Se agregó la plantilla al sistema");

				this.limpiarFormulario();
			}
			this.buscar();
			this.plantilla = new Plantilla();
		}
	}

	private void actualizarCamposPlantilla(Integer idPlantilla) {
		Query query = em.createNamedQuery("Plantilla.findById");
		query.setParameter("id", idPlantilla);
		Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpc : plant.getCamposPlantilla()) {
				if (cpc.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					cpc.setContenido(this.getMateria());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TITULO)) {
					cpc.setContenido(this.getTitulo());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PROLOGO)) {
					cpc.setContenido(this.getPrologo());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					cpc.setContenido(this.getConsiderando());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TENIENDO_PRESENTE)) {
					cpc.setContenido(this.getTeniendoPresente());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONVIENEN)) {
					cpc.setContenido(this.getConvienen());
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void buscar() {
		Query query = em.createNamedQuery("Plantilla.findByTipoPlantilla");
		query.setParameter("tp", TipoPlantilla.CONVENIO);
		this.plantillas = query.getResultList();
	}

	public void obtenerPlantilla(Integer idPlantilla) {
		Query query = em.createNamedQuery("Plantilla.findById");
		query.setParameter("id", idPlantilla);
		Plantilla plant = (Plantilla) query.getSingleResult();

		if (plant != null) {
			for (CamposPlantilla cpc : plant.getCamposPlantilla()) {
				
				if (cpc.getTipoContenido().getId().equals(TipoContenido.MATERIA)) {
					this.setMateria(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TITULO)) {
					this.setTitulo(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.PROLOGO)) {
					this.setPrologo(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONSIDERANDO)) {
					this.setConsiderando(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.TENIENDO_PRESENTE)) {
					this.setTeniendoPresente(cpc.getContenido());
				}
				if (cpc.getTipoContenido().getId().equals(TipoContenido.CONVIENEN)) {
					this.setConvienen(cpc.getContenido());
				}
			}
		}
	}

	public void limpiarFormulario() {
		this.plantillas = null;
		this.plantilla = null;

		this.begin();
	}

	public void modificar(Integer id) {
		plantilla = em.find(Plantilla.class, id);
		this.setNombre(plantilla.getNombre());
		this.obtenerPlantilla(id);
	}
	
	public void eliminarPlantilla(Plantilla p) {
		Plantilla plantilla = em.find(Plantilla.class, p.getId());
		for (CamposPlantilla cpc : plantilla.getCamposPlantilla()) {
			em.remove(cpc);
		}
		plantilla.getCamposPlantilla().clear();
		plantillas.remove(p);
		em.remove(plantilla);
		limpiarFormulario();
		FacesMessages.instance().add("Plantilla eliminada");
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	@End
	public String end() {
		return "home";
	}

	public Plantilla getPlantilla() {
		return plantilla;
	}

	public void setPlantilla(Plantilla plantilla) {
		this.plantilla = plantilla;
	}

	public List<Plantilla> getPlantillas() {
		return plantillas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPrologo() {
		return prologo;
	}

	public void setPrologo(String prologo) {
		this.prologo = prologo;
	}

	public String getConsiderando() {
		return considerando;
	}

	public void setConsiderando(String considerando) {
		this.considerando = considerando;
	}

	public String getTeniendoPresente() {
		return teniendoPresente;
	}

	public void setTeniendoPresente(String teniendoPresente) {
		this.teniendoPresente = teniendoPresente;
	}

	public String getConvienen() {
		return convienen;
	}

	public void setConvienen(String convienen) {
		this.convienen = convienen;
	}

}
