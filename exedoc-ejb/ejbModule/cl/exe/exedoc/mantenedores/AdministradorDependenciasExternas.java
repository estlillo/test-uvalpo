package cl.exe.exedoc.mantenedores;

import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DependenciaExterna;

/**
 * @author Administrator
 */
@Local
public interface AdministradorDependenciasExternas {

	/**
	 * @return String
	 */
	String begin();

	/**
	 * 
	 */
	void destroy();

	/**
	 * @return String
	 */
	String end();

	/**
	 * 
	 */
	void crearNuevo();

	/**
	 * @param id Long
	 */
	void modificar(Long id);

	/**
	 * @return DependenciaExterna
	 */
	DependenciaExterna getDependencia();

	/**
	 * @param dependencia DependenciaExterna
	 */
	void setDependencia(DependenciaExterna dependencia);

	/**
	 * @return List<DependenciaExterna>
	 */
	List<DependenciaExterna> getDependencias();


	/**
	 * 
	 */
	void cancelar();

	void eliminarDepenedencia(Long id);

	DependenciaExterna buscar(Long id);

}
