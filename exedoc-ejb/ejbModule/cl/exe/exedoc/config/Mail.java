package cl.exe.exedoc.config;

/**
 * @author Administrator
 *
 */
public interface Mail {

	/**
	 * @return the smtpHostname
	 */
	String getSmtpHostname();

	/**
	 * @param smtpHostname the smtpHostname to set
	 */
	void setSmtpHostname(String smtpHostname);

	/**
	 * @return the smtpAuth
	 */
	String getSmtpAuth();

	/**
	 * @param smtpAuth the smtpAuth to set
	 */
	void setSmtpAuth(String smtpAuth);

	/**
	 * @return the smtpPwd
	 */
	String getSmtpPwd();

	/**
	 * @param smtpPwd the smtpPwd to set
	 */
	void setSmtpPwd(String smtpPwd);

	/**
	 * @return the smtpCC
	 */
	String getSmtpCC();

	/**
	 * @param smtpCC the smtpCC to set
	 */
	void setSmtpCC(String smtpCC);

	/**
	 * @return the smtpCCSeparator
	 */
	String getSmtpCCSeparator();

	/**
	 * @param smtpCCSeparator the smtpCCSeparator to set
	 */
	void setSmtpCCSeparator(String smtpCCSeparator);

	/**
	 * @return the active
	 */
	Boolean getActive();

	/**
	 * @param active the active to set
	 */
	void setActive(Boolean active);
	
}
