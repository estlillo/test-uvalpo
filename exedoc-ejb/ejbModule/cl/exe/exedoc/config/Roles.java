package cl.exe.exedoc.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

/**
 * @author Administrator
 */
public class Roles {

	/**
	 * @author Administrator
	 */
	public enum PropiedadesRoles {
		/**
		 * URL_WS_ROLES.
		 */
		URL_WS_ROLES("URL_WS_ROLES"),

		/**
		 * INGRESO_DOCUMENTOS.
		 */
		INGRESO_DOCUMENTOS("INGRESO_DOCUMENTOS"),

		/**
		 * BANDEJA_ENTRADA_DOCUMENTOS.
		 */
		BANDEJA_ENTRADA_DOCUMENTOS("BANDEJA_ENTRADA_DOCUMENTOS"),

		/**
		 * BANDEJA_SALIDA_DOCUMENTOS.
		 */
		BANDEJA_SALIDA_DOCUMENTOS("BANDEJA_SALIDA_DOCUMENTOS"),

		/**
		 * FIRMA_DOCUMENTOS.
		 */
		FIRMA_DOCUMENTOS("FIRMA_DOCUMENTOS"),

		/**
		 * REPORTES.
		 */
		REPORTES("REPORTES"),

		/**
		 * ADMINISTRADOR.
		 */
		ADMINISTRADOR("ADMINISTRADOR"),

		/**
		 * ACTIVE.
		 */
		ACTIVE("ACTIVE"),

		/**
		 * USUARIOS_ADMINISTRADORES.
		 */
		USUARIOS_ADMINISTRADORES("USUARIOS_ADMINISTRADORES"),

		/**
		 * ROL_FOLIADOR.
		 */
		ROL_FOLIADOR("ROL_FOLIADOR"),

		/**
		 * ROL_FOLIADOR.
		 */
		ROL_ANULADOR("ROL_ANULADOR"),
		
		/**
		 * TRANSFORMA_PAPEL_A_DIGITAL.
		 */
		TRANSFORMA_DIGITAL_A_PAPEL("TRANSFORMA_PAPEL_A_DIGITAL");

		private String valor;

		/**
		 * @param valor String
		 */
		PropiedadesRoles(final String valor) {
			this.valor = valor;
		}

		@Override
		public String toString() {
			return valor;
		}

	};

	private static Properties propertiesRoles;
	@Logger
	private static Log log;

	private static String nombreArchivoRolesProperties = "roles.properties";

	static {
		final Roles pasaporte = new Roles();

		propertiesRoles = new Properties();
		try {
			propertiesRoles.load(pasaporte.getArchivo(nombreArchivoRolesProperties));
		} catch (IOException e) {
			log.info("Error al cargar archivo ", e);
		}
	}

	/**
	 * Constructor.
	 */
	protected Roles() {
		super();
	}

	/**
	 * @return Properties
	 */
	public static Properties getPropertiesRoles() {
		return propertiesRoles;
	}

	/**
	 * @param nombre String
	 * @return InputStream
	 */
	private InputStream getArchivo(final String nombre) {
		final ClassLoader loader = Thread.currentThread().getContextClassLoader();
		return loader.getResourceAsStream("roles/" + nombre);
	}

	/**
	 * TODO: CONVERTIR VARIABLE ESTATICA.
	 * 
	 * @return String
	 */
	public static final String urlwsroles() {
		return Roles.getPropertiesRoles().getProperty(PropiedadesRoles.URL_WS_ROLES.toString());
	}
}
