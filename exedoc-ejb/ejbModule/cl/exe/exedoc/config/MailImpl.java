package cl.exe.exedoc.config;

/**
 * Implementacion de {@link Mail}.
 */
public class MailImpl implements Mail {

	private String smtpAuth;
	private String smtpPwd;
	private String smtpCC;
	private String smtpCCSeparator;
	private String smtpHostname;
	private Boolean active;
	
	/**
	 * Constructor.
	 */
	public MailImpl() {
		super();
	}

	@Override
	public String getSmtpHostname() {
		return smtpHostname;
	}

	@Override
	public void setSmtpHostname(final String smtpHostname) {
		this.smtpHostname = smtpHostname;
	}

	@Override
	public String getSmtpAuth() {
		return smtpAuth;
	}

	@Override
	public void setSmtpAuth(final String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	@Override
	public String getSmtpPwd() {
		return smtpPwd;
	}

	@Override
	public void setSmtpPwd(final String smtpPwd) {
		this.smtpPwd = smtpPwd;
	}

	@Override
	public String getSmtpCC() {
		return smtpCC;
	}

	@Override
	public void setSmtpCC(final String smtpCC) {
		this.smtpCC = smtpCC;
	}

	@Override
	public String getSmtpCCSeparator() {
		return smtpCCSeparator;
	}

	@Override
	public void setSmtpCCSeparator(final String smtpCCSeparator) {
		this.smtpCCSeparator = smtpCCSeparator;
	}

	@Override
	public Boolean getActive() {
		return active;
	}

	@Override
	public void setActive(final Boolean active) {
		this.active = active;
	}

}
