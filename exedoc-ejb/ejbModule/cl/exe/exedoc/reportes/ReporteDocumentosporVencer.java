package cl.exe.exedoc.reportes;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

@Local
public interface ReporteDocumentosporVencer
{
    public String begin();
    public String end();
    public void destroy();
	void setListTipoDocumentos(List<SelectItem> listTipoDocumentos);
	List<SelectItem> getListTipoDocumentos();
	void setTipoDocumento(Long tipoDocumento);
	Long getTipoDocumento();
	void setFechaInicio(Date fechaInicio);
	Date getFechaInicio();
	void setFechaTermino(Date fechaTermino);
	Date getFechaTermino();
	void setNumeroDocumento(String numeroDocumento);
	String getNumeroDocumento();
	void setMateria(String materia);
	String getMateria();
	List<ExpedienteBandejaSalida> getDocumentosRelacionados();
	void setDocumentosRelacionados(
			List<ExpedienteBandejaSalida> documentosRelacionados);
	void filtrarDocumentosporVencer();
	void setPlazoDoc(String plazoDoc);
	String getPlazoDoc();

  
    // add additional interface methods here

}
