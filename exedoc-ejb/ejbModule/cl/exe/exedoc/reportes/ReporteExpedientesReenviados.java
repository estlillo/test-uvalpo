package cl.exe.exedoc.reportes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.util.DocumentoExpedienteComparador;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("reporteExpReenviados")
@Scope(ScopeType.SESSION)
public class ReporteExpedientesReenviados implements ReporteExpedientesReenviadosInterface {

	private String bNumExp;
	private Integer bIdTipoDoc;
	private Date bIFechaDoc;
	private Date bTFechaDoc;
	private String bEmisor;
	private String bFRutProveedor;
	private String bFNumOrdenCompra;
	private String clasificacionTipoBusqueda;
	private String clasificacionSubtipoBusqueda;
	private String clasificacionMateriaBusqueda;
	private List<SelectItem> listClasificacionTipo = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionSubtipo = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionMateria = new ArrayList<SelectItem>();
	private int flagMostrarClasificaciones;
	//private static final int _TIPO_DOCUMENTO_RESOLUCION = 6;

	private static final Locale locale = new Locale("es", "CL");
	private static final String TIMEZONE = "America/Cayenne";

	private static final String FWD_REPORTE_EXPEDIENTES_REENVIADOS = "busquedaExpReenviados";

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaEntrada> expReenviados;

	@Logger
	private Log log;

	@PersistenceContext
	protected EntityManager em;

	@In(required = false)
	private Persona usuario;

	@EJB
	private JerarquiasLocal jerarquias;

	@Out(required = true)
	private String homeCrear;

	// Constructor
	public ReporteExpedientesReenviados() {
		expReenviados = new ArrayList<ExpedienteBandejaEntrada>();
	}

	// Bean

	@Destroy
	@Remove
	public void destroy() {
	}

	// Logica

	@Factory(value = "expReenviados", scope = ScopeType.SESSION)
	public String initBusquedaExpReenviados() {
		log.debug("[initBusquedaExpReenviados] Iniciando metodo...");
		expReenviados.clear();
		bIdTipoDoc = new Integer(-1);
		// Date now = new Date();
		bIFechaDoc = null;
		bTFechaDoc = null;
		homeCrear = FWD_REPORTE_EXPEDIENTES_REENVIADOS;
		return FWD_REPORTE_EXPEDIENTES_REENVIADOS;
	}

	@SuppressWarnings("unchecked")
	public void buscar() {
		log.info("[buscar] Iniciando metodo...");
		if (!expReenviados.isEmpty()) {
			expReenviados.clear();
		}
		SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);

		if (bNumExp.isEmpty()) {
			if (bIFechaDoc == null && bTFechaDoc == null) {
				FacesMessages.instance().add("Se debe incluir mínimo el rango de fechas");
				return;
			} else if (bIFechaDoc != null && bTFechaDoc == null) {
				FacesMessages.instance().add("Debe incluir la fecha de termino");
				return;
			} else if (bIFechaDoc == null && bTFechaDoc != null) {
				FacesMessages.instance().add("Debe incluir la fecha de inicio");
				return;
			}
		}

		String queryStr = crearConsultaBusqueda(sDateFormat);
		log.info("[buscar] SQL a ejecutar: " + queryStr);

		Query qry = em.createQuery(queryStr);
		
		if (bIFechaDoc != null && bTFechaDoc != null) {
		
			qry.setParameter("bIFechaDoc", bIFechaDoc);
			qry.setParameter("bTFechaDoc", bTFechaDoc);
			
			
		}

		
		
		List<Object[]> oResultados = qry.getResultList();
		expReenviados = procesarResultados(oResultados);
	}

	private String crearConsultaBusqueda(SimpleDateFormat format) {
		StringBuilder sql = new StringBuilder("SELECT DISTINCT e.id, e.numeroExpediente, e.fechaIngreso, e.fechaDespachoHistorico, ");
		sql.append("e.fechaAcuseRecibo, e.fechaDespacho, 'd.rut_proveedor_factura', 'd.numero_orden_compra_factura' ");
		sql.append("FROM Expediente e, DocumentoExpediente de, ");
		sql.append("Documento d left join d.autor p  ");
		sql.append("WHERE (e.destinatario.id = " + usuario.getId() + " or e.destinatarioHistorico.id = " + usuario.getId() + ") ");
		sql.append(" and (");
		sql.append(bIdTipoDoc);
		sql.append(" = -1 or d.tipoDocumento.id = ");
		sql.append(bIdTipoDoc);
		sql.append(") and ('");
		sql.append(bEmisor.equals("") ? "-1" : bEmisor);
		sql.append("' = '-1' or p.usuario = '");
		sql.append(bEmisor);
		sql.append("') and ('");
		sql.append(bNumExp.equals("") ? "-1" : bNumExp);
		sql.append("' = '-1' or e.numeroExpediente = '");
		sql.append(bNumExp);
		sql.append("') and (e.id = de.expediente.id) and (de.documento.id = d.id) ");
//		if (esFactura()) {
//			if (this.bFRutProveedor != null && !this.bFRutProveedor.trim().equals("")) {
//				sql.append(" and upper(d.rut_proveedor_factura) like upper('");
//				sql.append(this.bFRutProveedor.trim());
//				sql.append("%')");
//			}
//			if (this.bFNumOrdenCompra != null) {
//				sql.append(" and d.numero_orden_compra_factura = '");
//				sql.append(this.bFNumOrdenCompra);
//				sql.append("' ");
//			}
//		}
		if (bIFechaDoc != null && bTFechaDoc != null) {
			sql.append("and d.fechaDocumentoOrigen between ");
			sql.append(" :bIFechaDoc " );
			sql.append(" and ");
			sql.append(":bTFechaDoc");
		}

		return sql.toString();
	}

	private List<ExpedienteBandejaEntrada> procesarResultados(List<Object[]> oArray) {
		List<ExpedienteBandejaEntrada> ebe = new ArrayList<ExpedienteBandejaEntrada>();
		ExpedienteBandejaEntrada sebe = null;
		ArrayList<DocumentoExpediente> respuestas = null;
		ArrayList<DocumentoExpediente> originales = null;

		for (Object[] o : oArray) {
			sebe = new ExpedienteBandejaEntrada();
			Expediente e = new Expediente();

			Long idExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			String numeroExp = (String) o[1];
			if (obtenerVecesEnBE(numeroExp).equals(0)) {
				continue;
			}
			Date fechaIngreso = (Date) o[2];
			Date fechaDespHist = (Date) o[3];
			Date fechaAcuseRecibo = (Date) o[4];
			Date fechaDespacho = (Date) o[5];
			String rutProveedorFactura = (String) o[6];
			String numeroOrdenCompra = (((String) o[7]) != null) ? (((String) o[7])) : "";

			e.setNumeroExpediente(numeroExp);

			sebe.setId(idExp);
			sebe.setNumeroExpediente(numeroExp);
			sebe.setFechaIngreso(fechaIngreso);
			respuestas = new ArrayList<DocumentoExpediente>();
			originales = new ArrayList<DocumentoExpediente>();
			obtenerDatosUltimoDocumento(e, sebe, respuestas, originales);

			sebe.setFechaIngresoBandeja(fechaDespHist);
			sebe.setFechaAcuseRecibo(fechaAcuseRecibo);
			sebe.setFechaDespacho(fechaDespacho);
			sebe.setRutProveedorFactura(rutProveedorFactura);
			sebe.setNumeroOrdenCompra(numeroOrdenCompra);
			ebe.add(sebe);
		}

		return ebe;
	}

	@SuppressWarnings("unchecked")
	private void obtenerDatosUltimoDocumento(Expediente expediente, ExpedienteBandeja bandejaSalida, ArrayList<DocumentoExpediente> respuestas, ArrayList<DocumentoExpediente> originales) {
		DocumentoExpediente documentoAuxiliar = null;

		StringBuilder jpQL = new StringBuilder("select de.id as id_doc_exp, de.tipoDocumentoExpediente.id, td.descripcion as desc_td, ");
		jpQL.append("d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, d.id as id_doc, ");
		jpQL.append("fd.descripcion as desc_td, d.cmsId ");
		jpQL.append("from DocumentoExpediente de, Expediente e, Documento d, ");
		jpQL.append("TipoDocumento td, FormatoDocumento fd ");
		jpQL.append("where e.id= de.expediente.id and de.documento.id= d.id ");
		jpQL.append("and d.tipoDocumento.id= td.id and d.formatoDocumento.id= fd.id ");
		jpQL.append("and e.numeroExpediente= '" + expediente.getNumeroExpediente() + "' ");

		List<Object[]> arrayObject = em.createQuery(jpQL.toString()).getResultList();
		Long idDocExp = null;
		Integer idTipoDocExp = null;
		String descTd = null;
		String numeroDoc = null;
		Date fechaDocOrigen = null;
		String materia = null;
		Long idDoc = null;
		String descFd = null;
		String cmsId = null;
		DocumentoExpediente docExp = null;
		Documento doc = null;

		for (Object[] o : arrayObject) {
			docExp = new DocumentoExpediente();
			doc = new Documento();
			idDocExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			idTipoDocExp = (Integer) o[1];
			docExp.setId(idDocExp);
			descTd = (String) o[2];
			numeroDoc = (String) o[3];
			fechaDocOrigen = (Date) o[4];
			materia = (String) o[5];
			idDoc = (((Long) o[6] != null) ? ((Long) o[6]).longValue() : null);
			descFd = (String) o[7];
			cmsId = (String) o[8];

			TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
			tipoDocExp.setId(idTipoDocExp);
			docExp.setTipoDocumentoExpediente(tipoDocExp);
			TipoDocumento tipoDoc = new TipoDocumento();
			tipoDoc.setDescripcion(descTd);
			doc.setTipoDocumento(tipoDoc);
			doc.setNumeroDocumento(numeroDoc);
			doc.setFechaDocumentoOrigen(fechaDocOrigen);
			doc.setMateria(materia);
			doc.setId(idDoc);
			FormatoDocumento formatoDoc = new FormatoDocumento();
			formatoDoc.setDescripcion(descFd);
			doc.setFormatoDocumento(formatoDoc);
			doc.setCmsId(cmsId);
			//(cmsId != null) ? new byte[1] : null);
			docExp.setDocumento(doc);

			if (idTipoDocExp.equals(TipoDocumentoExpediente.RESPUESTA)) {
				respuestas.add(docExp);
			}
		}
		if (respuestas.size() == 0) {
			for (Object[] o : arrayObject) {
				docExp = new DocumentoExpediente();
				doc = new Documento();
				idDocExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
				idTipoDocExp = (Integer) o[1];
				docExp.setId(idDocExp);
				descTd = (String) o[2];
				numeroDoc = (String) o[3];
				fechaDocOrigen = (Date) o[4];
				materia = (String) o[5];
				idDoc = (((BigInteger) o[6] != null) ? ((BigInteger) o[6]).longValue() : null);
				descFd = (String) o[7];
				cmsId = (String) o[8];

				TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
				tipoDocExp.setId(idTipoDocExp);
				docExp.setTipoDocumentoExpediente(tipoDocExp);
				TipoDocumento tipoDoc = new TipoDocumento();
				tipoDoc.setDescripcion(descTd);
				doc.setTipoDocumento(tipoDoc);
				doc.setNumeroDocumento(numeroDoc);
				doc.setFechaDocumentoOrigen(fechaDocOrigen);
				doc.setMateria(materia);
				doc.setId(idDoc);
				FormatoDocumento formatoDoc = new FormatoDocumento();
				formatoDoc.setDescripcion(descFd);
				doc.setFormatoDocumento(formatoDoc);
				doc.setCmsId(cmsId);
				//(cmsId != null) ? new byte[1] : null);
				docExp.setDocumento(doc);

				if (idTipoDocExp.equals(TipoDocumentoExpediente.ORIGINAL)) {
					originales.add(docExp);
				}
			}
			if (originales.size() != 0) {
				Collections.sort(originales, new DocumentoExpedienteComparador());
				documentoAuxiliar = originales.get(0);
				bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
				bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
				bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
				bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
				bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
				bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento().getDescripcion());
				bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
			}
		} else {
			Collections.sort(respuestas, new DocumentoExpedienteComparador());
			documentoAuxiliar = respuestas.get(0);
			bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
			bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
			bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
			bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
			bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
			bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento().getDescripcion());
			bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
		}
		respuestas = null;
		originales = null;
	}

	public Integer obtenerVecesEnBE(String numeroExpediente) {
		BigDecimal num = null;
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", locale);

		StringBuilder numQuery = new StringBuilder("SELECT COUNT(e.id) FROM expedientes e ");
		numQuery.append("LEFT JOIN personas p ON (e.id_destinatario = p.id) ");
		numQuery.append("LEFT JOIN personas p_hist ON (e.id_destinatario_historico = p_hist.id) ");
		numQuery.append("WHERE e.numero_expediente = '").append(numeroExpediente).append("' ");
		numQuery.append("AND (e.id_destinatario = ").append(usuario.getId() + " ");
		numQuery.append("OR e.id_destinatario_historico = ").append(usuario.getId() + ") ");

		if (bIFechaDoc != null && bTFechaDoc != null) {
			numQuery.append("and TO_DATE(TO_CHAR(e.fecha_despacho_historico, 'dd-MM-yyyy'),'dd-MM-yyyy') between TO_DATE('");
			numQuery.append(format.format(bIFechaDoc));
			numQuery.append("','dd-MM-yyyy') and TO_DATE('");
			numQuery.append(format.format(bTFechaDoc));
			numQuery.append("','dd-MM-yyyy')");
		}

		num = (BigDecimal) em.createNativeQuery(numQuery.toString()).getSingleResult();
		
		
		return num.intValue();
	}

//	public boolean esFactura() {
//		return bIdTipoDoc != null && bIdTipoDoc.equals(TipoDocumento.FACTURA);
//	}

//	public void buscarClasificaciones() {
//		Integer tipoDocumento = this.bIdTipoDoc;
//		if (tipoDocumento != null && tipoDocumento == _TIPO_DOCUMENTO_RESOLUCION) {
//			listClasificacionTipo = jerarquias.getClasificacionesTipo(JerarquiasLocal.TEXTO_INICIAL);
//			listClasificacionSubtipo = jerarquias.getClasificacionesSubtipo(JerarquiasLocal.TEXTO_INICIAL);
//			flagMostrarClasificaciones = 1;
//		} else {
//			flagMostrarClasificaciones = 0;
//		}
//	}

	public boolean renderedVerExpediente(Long idDocumento) {
		boolean valido = true;
		Documento doc = em.find(Documento.class, idDocumento);

		if (doc instanceof Oficio && doc.getReservado()) {
			for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
				if (!destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
					valido = false;
				} else {
					valido = true;
					break;
				}
			}
		}

		return valido;
	}

	// Getters && Setters
	public String getBNumExp() {
		return bNumExp;
	}

	public void setBNumExp(String numExp) {
		bNumExp = numExp;
	}

	public Integer getBIdTipoDoc() {
		return bIdTipoDoc;
	}

	public void setBIdTipoDoc(Integer idTipoDoc) {
		bIdTipoDoc = idTipoDoc;
	}

	public Date getBIFechaDoc() {
		return bIFechaDoc;
	}

	public void setBIFechaDoc(Date fechaDoc) {
		bIFechaDoc = fechaDoc;
	}

	public Date getBTFechaDoc() {
		return bTFechaDoc;
	}

	public void setBTFechaDoc(Date fechaDoc) {
		bTFechaDoc = fechaDoc;
	}

	public String getBEmisor() {
		return bEmisor;
	}

	public void setBEmisor(String emisor) {
		bEmisor = emisor;
	}

	public String getBFRutProveedor() {
		return bFRutProveedor;
	}

	public void setBFRutProveedor(String rutProveedor) {
		bFRutProveedor = rutProveedor;
	}

	public String getBFNumOrdenCompra() {
		return bFNumOrdenCompra;
	}

	public void setBFNumOrdenCompra(String numOrdenCompra) {
		bFNumOrdenCompra = numOrdenCompra;
	}

	public List<ExpedienteBandejaEntrada> getExpReenviados() {
		return expReenviados;
	}

	public void setExpReenviados(List<ExpedienteBandejaEntrada> expReenviados) {
		this.expReenviados = expReenviados;
	}

	public Locale getLocale() {
		return locale;
	}

	public String getTIMEZONE() {
		return TIMEZONE;
	}

	public String getClasificacionTipoBusqueda() {
		return clasificacionTipoBusqueda;
	}

	public void setClasificacionTipoBusqueda(String clasificacionTipoBusqueda) {
		this.clasificacionTipoBusqueda = clasificacionTipoBusqueda;
	}

	public String getClasificacionSubtipoBusqueda() {
		return clasificacionSubtipoBusqueda;
	}

	public void setClasificacionSubtipoBusqueda(String clasificacionSubtipoBusqueda) {
		this.clasificacionSubtipoBusqueda = clasificacionSubtipoBusqueda;
	}

	public String getClasificacionMateriaBusqueda() {
		return clasificacionMateriaBusqueda;
	}

	public void setClasificacionMateriaBusqueda(String clasificacionMateriaBusqueda) {
		this.clasificacionMateriaBusqueda = clasificacionMateriaBusqueda;
	}

	public List<SelectItem> getListClasificacionTipo() {
		return listClasificacionTipo;
	}

	public void setListClasificacionTipo(List<SelectItem> listClasificacionTipo) {
		this.listClasificacionTipo = listClasificacionTipo;
	}

	public List<SelectItem> getListClasificacionSubtipo() {
		return listClasificacionSubtipo;
	}

	public void setListClasificacionSubtipo(List<SelectItem> listClasificacionSubtipo) {
		this.listClasificacionSubtipo = listClasificacionSubtipo;
	}

	public List<SelectItem> getListClasificacionMateria() {
		return listClasificacionMateria;
	}

	public void setListClasificacionMateria(List<SelectItem> listClasificacionMateria) {
		this.listClasificacionMateria = listClasificacionMateria;
	}

	public int getFlagMostrarClasificaciones() {
		return flagMostrarClasificaciones;
	}

	public void setFlagMostrarClasificaciones(int flagMostrarClasificaciones) {
		this.flagMostrarClasificaciones = flagMostrarClasificaciones;
	}

}
