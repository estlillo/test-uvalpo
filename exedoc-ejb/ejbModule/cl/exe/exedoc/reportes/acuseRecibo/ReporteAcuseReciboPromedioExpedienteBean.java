package cl.exe.exedoc.reportes.acuseRecibo;

import static cl.exe.exedoc.util.JerarquiasLocal.INICIO;
import static cl.exe.exedoc.util.JerarquiasLocal.TEXTO_INICIAL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.bandeja.Bandeja;

/**
 * Class utilizada para crear reporte de expedientes despachados.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(ReporteAcuseReciboPromedioExpedienteBean.NAME)
public class ReporteAcuseReciboPromedioExpedienteBean implements ReporteAcuseReciboPromedioExpediente, Serializable {

	/**
	 * NAME.
	 */
	public static final String NAME = "reporteAcuseReciboPromedioExpediente";
	final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;
	private static final String DESTINATARIO_HISTORICO = "destinatarioHistorico";
	private static final String DESTINATARIO = "destinatario";

	private static final long serialVersionUID = -4056048351930327337L;

	private static final String STRING_INICIO = "inicio";
	private static final String STRING_TERMINO = "termino";
	private static final String INCLUIR_FECHA = "Debe incluir la fecha de {0}";

	private static final Locale LOCALE = FechaUtil.LOCALE;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	private Persona usuario;

	@EJB
	private JerarquiasLocal jerarquias;
	@EJB
	private Bandeja bandeja;

	private Long organizacion = INICIO;
	private Long division = INICIO;
	private Long departamento = INICIO;
	private Long unidad = INICIO;
	private Long cargo = INICIO;
	private Long persona = INICIO;

	private Date fechaInicio;
	private Date fechaTermino;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> bandejaLista;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listPersonas;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidad;
	private List<SelectItem> listCargo;

	/**
	 * Constructor.
	 */
	public ReporteAcuseReciboPromedioExpedienteBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation");
		this.listOrganizacion = this.buscarOrganizaciones();    
//		this.listDepartamento = this.iniciarLista();
//		this.listUnidad = this.iniciarLista();
//		this.listPersonas = this.iniciarLista();
//		this.listCargo = this.iniciarLista();
		this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();		
		
		listOrganizacion.addAll(this.buscarOrganizaciones());
		organizacion = (Long) listOrganizacion.get(1).getValue();
		
		this.listDivision = this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.buscarUnidades();
		
		return ReporteAcuseReciboPromedioExpedienteBean.NAME;
	}

	/**
	 * Metodo que iniciliza una lista de un formularios.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> iniciarLista() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(INICIO, TEXTO_INICIAL));
		return lista;
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		return jerarquias.getOrganizaciones(TEXTO_INICIAL);
	}

	@End
	@Override
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Remove
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdExpediente(final String numeroExpediente) {
		Long idExpediente = null;
		final StringBuffer hql = new StringBuffer(10);
		hql.append("SELECT DISTINCT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = :numeroExpediente ");
		hql.append("order by e.id desc ");
		final Query query = em.createQuery(hql.toString());
		query.setParameter("numeroExpediente", numeroExpediente);
		final List<Long> idExpedientes = (List<Long>) query.getResultList();
		if (idExpedientes.size() >= 1) {
			idExpediente = idExpedientes.get(0);
		}
		return idExpediente;
	}

	@Override
	public Boolean renderedVisualizarExpediente(final Long idDocumento) {
		Boolean valido = true;
		final Documento doc = em.find(Documento.class, idDocumento);
		if (doc != null && doc.getFormatoDocumento() != null) {
			if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.PAPEL)) {
				valido = false;
			}
			if (doc.getReservado()) {
				for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
					final Persona p = destinatario.getDestinatarioPersona();
					if (p == null) { return false; }
					if (!destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
						valido = false;
					} else {
						valido = true;
						break;
					}
				}
			}
		}
		return valido;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscar() {
		log.info("[ReporteHojaDespacho] iniciando busqueda...");
		Query query2 = generarQueryExpedientesPorUnidadesOrganizacionalesOffline();
		List<Object[]> lista2 = (List<Object[]>) query2.getResultList();
		this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();
		log.info("lista expedientes {0}", lista2.size());
		this.procesarResultadoBusqueda(lista2);

	}

	/**
	 * Metodo encargodo de validar las fechas de inicio y termino correspondientes a la fecha de despacho de un
	 * expediente.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarFechas() {
		Boolean validar = Boolean.TRUE;

		if (fechaInicio == null && fechaTermino == null) {
			FacesMessages.instance().add("Se debe incluir mínimo, el rango de fechas");
			validar = Boolean.FALSE;
		} else if (fechaInicio != null && fechaTermino == null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_TERMINO));
			validar = Boolean.FALSE;
		} else if (fechaInicio == null && fechaTermino != null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_INICIO));
			validar = Boolean.FALSE;
		}
		return validar;
	}

	/**
	 * Metodo encargado de procesar los resultado de la busqueda.
	 * 
	 * @param lista {@link List} of {@link Object[]}
	 */
	private void procesarResultadoBusqueda(final List<Object[]> lista) {
		List<Documento> expedientesHijos = null;
		ExpedienteBandejaSalida bs = new ExpedienteBandejaSalida();
		ExpedienteBandejaSalida bsAux = new ExpedienteBandejaSalida();
		List<UnidadOrganizacional> unidades = new ArrayList<UnidadOrganizacional>();
		UnidadOrganizacional aux = new UnidadOrganizacional();
		List<ExpedienteBandejaSalida> bandejaListaAux = new ArrayList<ExpedienteBandejaSalida>();
		//
		final String q = "SELECT " + " distinct id_unidad , unidad " + " FROM expedientes_offline ";
		final Query query = em.createNativeQuery(q);
		List<Object[]> lista2 = (List<Object[]>) query.getResultList();
		for (Object[] obj2 : lista2) {
			ExpedienteBandejaSalida bsUni = new ExpedienteBandejaSalida();
			BigDecimal idUni = (BigDecimal) obj2[0];
			final String unidad = (String) obj2[1];
			bsUni.setIdUnidad(idUni);
			bsUni.setUnidadOrg(unidad);
			bandejaLista.add(bsUni);

		}
		//

		for (Object[] obj : lista) {
			final BigDecimal idExp = (BigDecimal) obj[0];
			final Date fechaIngreso = (Date) obj[1];
			final Date fechaDespacho = (Date) obj[2];
			final BigDecimal idCargo = (BigDecimal) obj[3];
			final String cargo = (String) obj[4];
			final BigDecimal idUnidad = (BigDecimal) obj[5];
			final String unidad = (String) obj[6];
			final BigDecimal idDepartamento = (BigDecimal) obj[7];
			final String departamento = (String) obj[8];
			final BigDecimal idDivision = (BigDecimal) obj[9];
			final String division = (String) obj[10];
			final BigDecimal idOrganizacion = (BigDecimal) obj[11];
			final String organizacion = (String) obj[12];
			final Date fechaAcuseRecibo = (Date) obj[13];
			for (ExpedienteBandejaSalida obj2 : bandejaLista) {
				if (obj2.getIdUnidad().equals(idUnidad)) {
					long promedioEncolado = 0L;
					if (fechaAcuseRecibo != null && fechaIngreso != null) {
						promedioEncolado = obj2.getPromedioEncolado()
						+ (fechaAcuseRecibo.getTime() - fechaIngreso.getTime());
					} else {
						promedioEncolado = obj2.getPromedioEncolado();
					}
					if (fechaAcuseRecibo != null && fechaIngreso != null) {
						obj2.setTotal(obj2.getTotal() + (fechaAcuseRecibo.getTime() - fechaIngreso.getTime()));
					}
					obj2.setTotalUnidades(obj2.getTotalUnidades() + 1);
					obj2.setPromedioEncolado(promedioEncolado / obj2.getTotalUnidades());

				}

			}

			for (ExpedienteBandejaSalida obj2 : bandejaLista) {
				double totalnew = 0;
				if (obj2.getTotal() > 0) {
					totalnew = obj2.getTotal() / obj2.getTotalUnidades();
				}

				double mesLong = totalnew / (MILLSECS_PER_DAY * 30);
				double mesLongResto = totalnew % (MILLSECS_PER_DAY * 30);
				double diaLong = (mesLongResto) / MILLSECS_PER_DAY;
				double diaLongResto = (mesLongResto) % MILLSECS_PER_DAY;
				double horaLong = diaLongResto / 3600000;
				double horaLongResto = diaLongResto % 3600000;
				double minutoLong = horaLongResto / 60000;
				double minutoLongResto = horaLongResto % 60000;
				double segundosLong = minutoLongResto / 1000;

				String format = "    Meses :    " + (int) mesLong + "    Dias :     " + (int) diaLong + "   Hora :  "
				+ (int) horaLong + "   Minutos :   " + (int) minutoLong + " Segundos :" + (int) segundosLong;
				if ((int) mesLong == 0 && (int) diaLong == 0 && (int) horaLong == 0 && (int) minutoLong == 0
						&& (int) segundosLong == 0) {
					obj2.setTiempoEncolado("Unidad Organizacional no posee informacion");
				} else {
					obj2.setTiempoEncolado(format);
				}
			}
			// bandejaListaAux.add(bs);
			// obtiene el total de registros por unidades

		}
		int i = 0;
		bandejaListaAux.clear();
		for (ExpedienteBandejaSalida obj2 : bandejaLista) {
			if (obj2.getTotalUnidades().intValue() != 0) {
				bandejaListaAux.add(obj2);
			}
			i++;
		}
		bandejaLista.clear();
		bandejaLista.addAll(bandejaListaAux);
		// Collections.sort(bandejaLista, ComparatorUtils.compararListaBandejaSalida);
	}

	private List<UnidadOrganizacional> limpiaUnidades(List<UnidadOrganizacional> lista) {
		List<UnidadOrganizacional> unidades = new ArrayList<UnidadOrganizacional>();
		for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
			UnidadOrganizacional unidadOrganizacional = (UnidadOrganizacional) iterator.next();

		}
		return null;
	}

	/**
	 * Metodo que obtienes la lista de expedientes que se han despachados por expediente.
	 * 
	 * @param idPadre {@link Long} identificador del expediente padre.
	 * @return {@link List} of {@link Documento}
	 */
	@SuppressWarnings("unchecked")
	private List<Documento> obtenerDocumentosExpedientes(final String idPadre) {
		final List<Documento> listDocsHijos = new ArrayList<Documento>();
		Documento exp = null;
		final List<Object[]> lista = new ArrayList<Object[]>();

		final Query queryD = this.construirQuery(idPadre);
		final List<Object[]> listDest = queryD.getResultList();
		lista.addAll(listDest);

		// log.info("lista de expedientes relacionados: {0}", listDest.size());

		for (Object[] o : lista) {
			exp = this.construirDocumento(o);
			if (TipoDocumentoExpediente.RESPUESTA == exp.getTipoDocumentoExpediente().getId()) {
				listDocsHijos.add(exp);
			}

		}
		return listDocsHijos;
	}

	/**
	 * Metodo que contruye query para obtener expedientes despachados por unidad.
	 * 
	 * @param idPadre {@link Long}
	 * @param clazz {@link String}
	 * @return {@link Query}
	 */
	private Query construirQuery(final String idPadre) {
		// select * from expedientes e , documentos d , Documentos_expedientes de , tipos_documentos td
		// where e.numero_expediente = 'E208/2011' and e.id = de.id_expediente and d.id = de.id_documento
		// and d.id_tipo_documento = td.id

		final StringBuilder sql = new StringBuilder();
		StringBuilder jpQL = new StringBuilder(
		"select de.id as id_doc_exp, de.tipoDocumentoExpediente.id, td.descripcion as desc_td, ");
		jpQL.append("d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, d.id as id_doc , td.id , de.tipoDocumentoExpediente.descripcion , de.tipoDocumentoExpediente.id");
		jpQL.append(" from DocumentoExpediente de, Expediente e, Documento d, ");
		jpQL.append(" TipoDocumento td ");
		jpQL.append(" where e.numeroExpediente = '" + idPadre + "' and e.id = de.expediente.id ");
		jpQL.append("and d.id = de.documento.id and d.tipoDocumento.id = td.id");
		final Query query = em.createQuery(jpQL.toString());
		return query;
	}

	/**
	 * Metodo que construye un expediente a partir de un arreglo de object.
	 * 
	 * @param o {@link Object}
	 * @return {@link Documento}
	 */
	private Documento construirDocumento(final Object[] o) {
		// [103301, 1, Resolucion Electronica, s/n, null, Modifica Resolucion FN/MP N.° XXX, 101300]
		final Documento doc = new Documento();

		final Long idDoc = ((Long) o[0] != null) ? ((Long) o[0]).longValue() : null;
		final int tipoDocExp = (Integer) o[1];
		final String descTipoDocExp = ((String) o[2]);
		final String numDoc = (String) o[3];
		final Date fechaOrigen = (Date) o[4];
		final String materia = (String) o[5];
		final Long num = (Long) o[6];
		final String tipoDoc = (String) o[8];
		final int idTipoDocExp = (Integer) o[9];

		doc.setId(idDoc);
		TipoDocumento tipo = new TipoDocumento();
		tipo.setDescripcion(descTipoDocExp);
		doc.setTipoDocumento(tipo);
		doc.setFechaCreacion(fechaOrigen);
		TipoDocumentoExpediente tipoExp = new TipoDocumentoExpediente();
		tipoExp.setDescripcion(tipoDoc);
		tipoExp.setId(idTipoDocExp);
		doc.setTipoDocumentoExpediente(tipoExp);
		return doc;
	}

	/**
	 * Metodo que crea query, para rescatar de la base de datos los expedientes despachados.
	 * 
	 * @return {@link Query}
	 */
	private Query generarQuery() {
		final StringBuilder sql = new StringBuilder();
		sql.append("select DISTINCT e.id, ");
		sql.append("	e.numeroExpediente, ");
		sql.append("	e.fechaDespacho, ");
		sql.append("	e.fechaIngreso, ");
		sql.append("	p2.nombres, ");
		sql.append("	p2.apellidoPaterno, ");
		sql.append("	p2.apellidoMaterno ");
		sql.append("from Expediente e ");
		sql.append("	join e.emisor p2 ");

		sql.append("where (e.emisor.id = :idUsuario and e.destinatario.id is null) ");
		sql.append("	and e.fechaDespacho is not null ");
		// Numero de expediete utilizado para realizar las pruebas.
		// sql.append(" and e.numeroExpediente = 'E98/2011' ");
		// sql.append(" and e.numeroExpediente = 'E32/2011' ");

		final Date inicio = FechaUtil.inicio(fechaInicio);
		final Date termino = FechaUtil.fin(fechaTermino);
		log.info("inicio {0}", FechaUtil.BDSDF.format(inicio));
		log.info("termino {0}", FechaUtil.BDSDF.format(termino));

		sql.append("	and e.fechaDespacho between :inicio and :termino ");

		log.info("Query: {0}", sql.toString());

		final Query query = em.createQuery(sql.toString());
		query.setParameter("idUsuario", usuario.getId());
		query.setParameter(STRING_INICIO, inicio, TemporalType.TIMESTAMP);
		query.setParameter(STRING_TERMINO, termino, TemporalType.TIMESTAMP);

		return query;
	}

	/**
	 * Metodo que crea query, para rescatar de la base de datos los expedientes despachados.
	 * 
	 * @return {@link Query}
	 */

	private Query generarQueryExpedientesPorUnidadesOrganizacionalesOffline() {
		String q = "SELECT id_expediente , fecha_ingreso , fecha_despacho "
			+ ", id_cargo ,cargo , id_unidad , unidad , id_departamento,departamento , "
			+ "id_division ,division, id_organizacion , organizacion ,fecha_acuse_recibo FROM expedientes_offline ";
		if (!organizacion.equals(INICIO) || !division.equals(INICIO) || !departamento.equals(INICIO) || !unidad.equals(INICIO) || !cargo.equals(INICIO)  || !persona.equals(INICIO) ) {
			q = q + " where ";
		}
		if (!organizacion.equals(INICIO)) {
			q = q + " id_organizacion = " + organizacion;
			if (!division.equals(INICIO) || !departamento.equals(INICIO) || !unidad.equals(INICIO) || !cargo.equals(INICIO)  || !persona.equals(INICIO) ) {
				q = q + " and ";
			}
		}
		if (!division.equals(INICIO)) {
			q = q + " id_division = " + division;
			if (!departamento.equals(INICIO) || !unidad.equals(INICIO) || !cargo.equals(INICIO)  || !persona.equals(INICIO) ) {
				q = q + " and ";
			}
		}
		if (!departamento.equals(INICIO)) {
			q = q + " id_departamento = " + departamento;
			if (!unidad.equals(INICIO) || !cargo.equals(INICIO)  || !persona.equals(INICIO)) {
				q = q + " and ";
			}
		}
		if (!unidad.equals(INICIO)) {
			q = q + " id_unidad = " + unidad;
			if (!cargo.equals(INICIO)  || !persona.equals(INICIO)) {
				q = q + " and ";
			}
		}
		if (!cargo.equals(INICIO)) {
			q = q + " id_cargo = " + cargo;
			if (!persona.equals(INICIO)) {
				q = q + " and ";
			}
		}
		if (!persona.equals(INICIO)) {
			q = q + "  id_emisor = " + persona;
		}
		final Query query = em.createNativeQuery(q);
		return query;
	}

	@Override
	public List<SelectItem> buscarDivisiones() {
		return jerarquias.getDivisiones(TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDepartamentos() {
		this.listDepartamento = jerarquias.getDepartamentos(TEXTO_INICIAL, division);
		listUnidad =  this.iniciarLista();
		listCargo = this.iniciarLista();
		listPersonas = this.iniciarLista();
	}

	@Override
	public void buscarUnidades() {
		this.listUnidad = jerarquias.getUnidadesOrganzacionales(TEXTO_INICIAL, departamento);
		listCargo = this.iniciarLista();
		listPersonas = this.iniciarLista();
	}

	@Override
	public void buscarCargos() {
		listCargo = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
		if(listPersonas.size() == 0){
			listPersonas.clear();
			listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(final Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<ExpedienteBandejaSalida> getBandejaLista() {
		return bandejaLista;
	}

	@Override
	public void setBandejaLista(final List<ExpedienteBandejaSalida> bandeja) {
		this.bandejaLista = bandeja;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidad() {
		return listUnidad;
	}

	@Override
	public void setListUnidad(final List<SelectItem> listUnidad) {
		this.listUnidad = listUnidad;
	}

	@Override
	public List<SelectItem> getListCargo() {
		return listCargo;
	}

	@Override
	public void setListCargo(final List<SelectItem> listCargo) {
		this.listCargo = listCargo;
	}

	@Override
	public Locale getLocale() {
		return LOCALE;
	}

	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

	@Override
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Override
	public Date getFechaTermino() {
		return fechaTermino;
	}

	@Override
	public void setFechaTermino(final Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

}
