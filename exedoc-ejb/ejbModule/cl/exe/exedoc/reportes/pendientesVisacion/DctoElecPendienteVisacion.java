package cl.exe.exedoc.reportes.pendientesVisacion;

import java.util.List;

import cl.exe.exedoc.pojo.expediente.ExpedientesPendientes;
import cl.exe.exedoc.repositorio.RepositorioLocal;

public interface DctoElecPendienteVisacion {

	String begin();

	String end();

	void destroy();

	List<ExpedientesPendientes> buscarExpedientesNoVisados();

	byte[] getArchivo(String documento);

	List<ExpedientesPendientes> getListExpedientesPorVisar();

	void setListExpedientesPorVisar(List<ExpedientesPendientes> listExpedientesPorVisar);

	RepositorioLocal getRepositorio();

	void setRepositorio(RepositorioLocal repositorio);

	boolean isCampoVisador();

	void setCampoVisador(boolean campoVisador);

	boolean getCampoVisador();

	String verDocumento(Integer idDocumento);

	String getVisualizar();

	void setVisualizar(String visualizar);

	List<ExpedientesPendientes> buscarPrueba();

}
