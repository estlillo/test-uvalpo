package cl.exe.exedoc.reportes.pendientesVisacion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.xpath.XPathExpressionException;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Rol;
import cl.exe.exedoc.entity.VisacionEstructuradaDocumento;
import cl.exe.exedoc.pojo.expediente.ExpedientesPendientes;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.util.JerarquiasLocal;

@Stateful
@Name("dctoElecPendienteVisacion")
@Scope(value = ScopeType.SESSION)
public class DctoElecPendienteVisacionBean implements DctoElecPendienteVisacion {

	private List<ExpedientesPendientes> listExpedientesPorVisar;
	
	@Logger
	private Log log;

	@EJB
    private RepositorioLocal repositorio;
	
	@PersistenceContext
	private EntityManager em;
	
	@In(required = false)
	private Persona usuario;
	
	private boolean campoVisador = true;
	
	private String visualizar;
	
	@Begin(join = true)
	@Override
	public String begin() {
		log.info("begin conversation");
		visualizar = JerarquiasLocal.VISUALIZAR;
		listExpedientesPorVisar = new ArrayList<ExpedientesPendientes>();
		this.buscarExpedientesNoVisados();
		return "dctoElecPendienteVisacion";
	}
	
	@End
	@Override
	public String end() {
		log.info("ending conversation");
		
		return "home";
	}

	@Remove
	@Override
	public void destroy() {
		log.info("destroy conversation");
	}
	

	/**
	 * Método para obtener los expedientes que aún no se visan.
	 * retorna todos los expedientes pendiente de visar, simpre y cuando el usuario logeado no tenga rol VISADOR,
	 * caso contrario el usuario logeado tiene privilegios de VISADOR solo se devuelve los documentes pendientes por 
	 * ser visados por el usuario.
	 *  
	 * @return
	 */
	@Override
	public List<ExpedientesPendientes> buscarPrueba(){
//		
//		final StringBuilder sql = new StringBuilder();
//
//		sql.append("select e from VisacionEstructuradaDocumento e join e.persona p where  p.cargo.unidadOrganizacional.departamento.id = "+usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//		List<Rol> roles = usuario.getRoles();
//		
//		if(roles.contains(Rol.VISADOR)){
//			campoVisador = false;
//			sql.append("  and e.persona = "+usuario.getId()+"  and p.cargo.unidadOrganizacional.departamento.id = "+usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//			log.info("id persona: "+usuario.getId());
//		}
//		Query query = em.createQuery(sql.toString());
//		List<VisacionEstructuradaDocumento> respuesta = query.getResultList();
//		log.info("lista: "+respuesta.size());
//		List<Persona> pe = new ArrayList<Persona>();
//		if(!respuesta.isEmpty()){
//			int i = 1;
//			
//			
//			
//			
//			for (VisacionEstructuradaDocumento visaciones : respuesta) {
//				pe.add(visaciones.getPersona());
//				ExpedientesPendientes ep = new ExpedientesPendientes();
//				ep.setNumDocumento(visaciones.getDocumento().getNumeroDocumento());
//				ep.setMateria(visaciones.getDocumento().getMateria());
//				ep.setFechaDocumento(visaciones.getDocumento().getFechaCreacion());
//				ep.setPersona(visaciones.getPersona());
//				ep.setTipoDocumento(visaciones.getDocumento().getTipoDocumento());
//				ep.setIdDocumento(visaciones.getDocumento().getId());
//				ep.setNumero(i);
//				listExpedientesPorVisar.add(ep);
//				i++;
//			}
//			log.info("listaExpedientesPorVisar: "+listExpedientesPorVisar.size());
//		}
//		
		return listExpedientesPorVisar;
	}
	
	/**
	 * Método para obtener los expedientes que aún no se visan.
	 * retorna todos los expedientes pendiente de visar, simpre y cuando el usuario logeado no tenga rol VISADOR,
	 * caso contrario el usuario logeado tiene privilegios de VISADOR solo se devuelve los documentes pendientes por 
	 * ser visados por el usuario.
	 *  
	 * @return
	 */
	@Override
	public List<ExpedientesPendientes> buscarExpedientesNoVisados(){
		
		final StringBuilder sql = new StringBuilder();
		
		sql.append("Select d from DocumentoElectronico d where d.id in (select distinct e.documento.id from VisacionEstructuradaDocumento e where e.persona.cargo.unidadOrganizacional.departamento.id = "+usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		
		List<Rol> roles = usuario.getRoles();
		
		if(roles.contains(Rol.VISADOR)){
			campoVisador = false;
			sql.append("  and e.persona = "+usuario.getId());
			log.info("id persona: "+usuario.getId());
		}
		sql.append(" )");
		
		Query query = em.createQuery(sql.toString());
		List<DocumentoElectronico> respuesta = query.getResultList();
		log.info("lista 2 : "+respuesta.size());
		
		if(!respuesta.isEmpty()){
			int i = 1;
			for (DocumentoElectronico visaciones : respuesta) {
				ExpedientesPendientes ep = new ExpedientesPendientes();
				ep.setNumDocumento(visaciones.getNumeroDocumento());
				ep.setMateria(visaciones.getMateria());
				ep.setFechaDocumento(visaciones.getFechaCreacion());
				List<Persona> asd= new ArrayList<Persona>();
				for(VisacionEstructuradaDocumento vis:visaciones.getVisacionesEstructuradas())
					asd.add(vis.getPersona());
				ep.setListPersonas(asd);
				ep.setTipoDocumento(visaciones.getTipoDocumento());
				ep.setIdDocumento(visaciones.getId());
				ep.setNumero(i);
				listExpedientesPorVisar.add(ep);
				i++;
			}
			log.info("listaExpedientesPorVisar: "+listExpedientesPorVisar.size());
		}
		
		return listExpedientesPorVisar;
	}
	
	@Override
	public String verDocumento(Integer idDocumento){
		log.info("idDocumento: "+idDocumento);
		return "crearResolucion";
	}
	
	
	/**
	 * Método para obtener el documento del repositorio Alfresco
	 * 
	 * @param documento
	 * @return
	 */
	@Override
	public byte[] getArchivo(String documento) {
        byte[] data = null;
        // repositorio.recuperarArchivo(documento.getArchivo().getCmsId());
        try {
                 data = repositorio.getFile(documento);
                
                if (data != null) {
                        final FacesContext facesContext = FacesContext.getCurrentInstance();
                } else {
                        FacesMessages.instance().add("No existe el archivo, consulte con el administrador. (Codigo Archivo: " + documento + ")");
                }
        } catch (XPathExpressionException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }
        return data;
	}

	@Override
	public List<ExpedientesPendientes> getListExpedientesPorVisar() {
		return listExpedientesPorVisar;
	}

	@Override
	public void setListExpedientesPorVisar(List<ExpedientesPendientes> listExpedientesPorVisar) {
		this.listExpedientesPorVisar = listExpedientesPorVisar;
	}

	@Override
	public RepositorioLocal getRepositorio() {
		return repositorio;
	}

	@Override
	public void setRepositorio(RepositorioLocal repositorio) {
		this.repositorio = repositorio;
	}

	@Override
	public boolean isCampoVisador() {
		return campoVisador;
	}

	@Override
	public boolean getCampoVisador() {
		return campoVisador;
	}
	
	@Override
	public void setCampoVisador(boolean campoVisador) {
		this.campoVisador = campoVisador;
	}
	
	@Override
	public String getVisualizar() {
		return visualizar;
	}

	@Override
	public void setVisualizar(final String visualizar) {
		this.visualizar = visualizar;
	}
}

