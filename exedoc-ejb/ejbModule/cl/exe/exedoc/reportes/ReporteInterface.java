package cl.exe.exedoc.reportes;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

@Local
public interface ReporteInterface {

	String obtenerInformacionDeDocumentosEnBandejaDeEntrada();

	String obtenerInformacionDeRutaSeguidaPorUnDocumento();

	String obtenerHistorialDeUnDocumento();

	String obtenerReporteDeDocumentosRelacionados();

	String obtenerHistorialBandejaEntrada();

	String obtenerEstadoBandejaSalida();

	String menuBuscarDocumentos();

	void buscarDocumentos();

	Locale getLocale();

	String getTimeZone();

	List<ExpedienteBandejaSalida> getEstadoBandejaSalida();

	void setEstadoBandejaSalida(List<ExpedienteBandejaSalida> estadoBandejaSalida);

	List<ExpedienteBandejaEntrada> getHistorialBandejaEntrada();

	void setHistorialBandejaEntrada(List<ExpedienteBandejaEntrada> historialBandejaEntrada);

	List<ExpedienteBandejaEntrada> getDocumentoEnBandejaEntrada();

	void setDocumentoEnBandejaEntrada(List<ExpedienteBandejaEntrada> documentoEnBandejaEntrada);

	Persona getUsuario();

	void setUsuario(Persona usuario);

	String getTipoDocumentoBusqueda();

	void setTipoDocumentoBusqueda(String tipoDocumentoBusqueda);

	String getNumeroDocumentoBusqueda();

	void setNumeroDocumentoBusqueda(String numeroDocumentoBusqueda);

	String getNumeroInternoDocumentoBusqueda();

	void setNumeroInternoDocumentoBusqueda(String numeroDocumentoBusqueda);

	Date getFechaInicioDocumentoBusqueda();

	void setFechaInicioDocumentoBusqueda(Date fechaInicioDocumentoBusqueda);

	Date getFechaTerminoDocumentoBusqueda();

	void setFechaTerminoDocumentoBusqueda(Date fechaTerminoDocumentoBusqueda);

	String getMateriaDocumentoBusqueda();

	void setMateriaDocumentoBusqueda(String materiaDocumentoBusqueda);

	String getNombreArchivoBusqueda();

	void setNombreArchivoBusqueda(String nombreArchivoBusqueda);

	void filtrarDocumentosEnBandejaEntrada();

	void filtrarHistorialBandejaEntrada();

	void filtrarEstadoBandejaSalida();

	String getNumeroExpedienteBusqueda();

	void setNumeroExpedienteBusqueda(String numeroExpedienteBusqueda);

	List<ExpedienteBandejaSalida> getDocumentosRelacionados();

	void setDocumentosRelacionados(List<ExpedienteBandejaSalida> documentosRelacionados);

	void filtrarDocumentosRelacionados();

	List<ExpedienteBandejaSalida> getHistorialDocumento();

	List<ExpedienteBandejaSalida> getDocumentosEncontrados();

	void setHistorialDocumento(List<ExpedienteBandejaSalida> historialDocumento);

	void filtrarHistorialDocumento();

	void filtrarRutaDeDocumentos();

	List<ExpedienteBandejaSalida> getRutaSeguidaDocumento();

	void setRutaSeguidaDocumento(List<ExpedienteBandejaSalida> rutaSeguidaDocumento);

	String obtenerDocumentos();

	String obtenerDocumentosOficinaDePartes();

	String obtenerDocumentosUnidadOrganizacional();

	void filtrarDocumentos();

	void filtrarExpedientesUnidadOrganizacional();

	List<ExpedienteBandejaSalida> getReporteDocumentos();

	void setReporteDocumentos(List<ExpedienteBandejaSalida> reporteDocumentos);

	void exportarReporteDocumentosExcel();

	void exportarReporteDeDocumentosOficinaPartesAExcel();

	String getEmisorDocumentoBusqueda();

	void setEmisorDocumentoBusqueda(String emisorDocumentoBusqueda);

	String getDestinatarioDocumentoBusqueda();

	void setDestinatarioDocumentoBusqueda(String destinatarioDocumentoBusqueda);

	void destroy();

	Long getIdEmisor();

	void setIdEmisor(Long idEmisor);

	Long getIdDestinatario();

	void setIdDestinatario(Long idDestinatario);

	void setUnidadesOrganizacionales();

	List<SelectItem> getUnidadesOrganizacionales();

	void verDocumento(Long idDocumento);

	Long getRegionEmisor();

	void setRegionEmisor(Long idRegion);

	List<SelectItem> getRegionesEmisor();

	void buscarUnidadesOrganizacionalesEmisor();

	List<SelectItem> getUnidadesOrganizacionalesEmisor();

	Long getRegionDestinatario();

	void setRegionDestinatario(Long idRegion);

	List<SelectItem> getRegionesDestinatario();

	void buscarUnidadesOrganizacionalesDestinatario();

	List<SelectItem> getUnidadesOrganizacionalesDestinatario();

	List<SelectItem> getListaTipoDocumento();

	List<SelectItem> getListaFormatoDocumento();

	void setBusquedaTipoDocumento(Integer tipoDoc);

	Integer getBusquedaTipoDocumento();

	void setBusquedaFormatoDocumento(Integer formatoDoc);

	Integer getBusquedaFormatoDocumento();

	void buscarCargosEmisor();

	void buscarPersonasEmisor();

	void buscarCargosDestinatario();

	void buscarPersonasDestinatario();

	void completaJerarquia();

	Long getCargoEmisor();

	void setCargoEmisor(Long cargo);

	Long getPersonaEmisor();

	void setPersonaEmisor(Long persona);

	void buscarPersonas();

	void buscarDepartamentos();

	// void buscarClasificaciones();
	void buscarUnidadesOrganizacionales();

	void buscarCargos();

	Long getCargoDestinatario();

	void setCargoDestinatario(Long cargo);

	Long getPersonaDestinatario();

	void setPersonaDestinatario(Long persona);

	Long getIdUnidadOrganizacionalEmisor();

	void setIdUnidadOrganizacionalEmisor(Long id);

	Long getIdUnidadOrganizacionalDestinatario();

	void setIdUnidadOrganizacionalDestinatario(Long id);

	List<SelectItem> getCargosEmisor();

	List<SelectItem> getPersonasEmisor();

	List<SelectItem> getCargosDestinatario();

	List<SelectItem> getPersonasDestinatario();

	String obtenerCierreDocumentos();

	void buscarDocumentosCierre();

	List<ExpedienteBandejaSalida> getDocumentosCierre();

	Long getCargo();

	void setCargo(Long cargo);

	Long getDivision();

	void setDivision(Long division);

	Long getDepartamento();

	void setDepartamento(Long departamento);

	Long getUnidadOrganizacional();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getPersona();

	void setPersona(Long persona);

	void setUnidadesOrganizacionales(List<SelectItem> unidadesOrganizacionales);

	List<SelectItem> getListCargos();

	void setListCargos(List<SelectItem> listCargos);

	List<SelectItem> getListDepartamento();

	void setListDepartamento(List<SelectItem> listDepartamento);

	List<SelectItem> getListDivision();

	void setListDivision(List<SelectItem> listDivision);

	List<SelectItem> getListPersonas();

	void setListPersonas(List<SelectItem> listPersonas);

	List<SelectItem> getListUnidadesOrganizacionales();

	void setListUnidadesOrganizacionales(List<SelectItem> listUnidadesOrganizacionales);

	// void esFactura();
	// void buscarClasificacionesVerificarFactura();
	// boolean isFactura();
	// void setFactura(boolean factura);
	Integer getNumeroOrdenDeCompraBusqueda();

	void setNumeroOrdenDeCompraBusqueda(Integer numeroOrdenDeCompraBusqueda);

	String getRutProveedorBusqueda();

	void setRutProveedorBusqueda(String rutProveedorBusqueda);

	String getClasificacionTipoBusqueda();

	void setClasificacionTipoBusqueda(String clasificacionTipoBusqueda);

	String getClasificacionSubtipoBusqueda();

	void setClasificacionSubtipoBusqueda(String clasificacionSubtipoBusqueda);

	int getFlagMostrarClasificaciones();

	void setFlagMostrarClasificaciones(int flagMostrarClasificaciones);

	List<SelectItem> getListClasificacionTipo();

	void setListClasificacionTipo(List<SelectItem> listClasificacionTipo);

	List<SelectItem> getListClasificacionSubtipo();

	void setListClasificacionSubtipo(List<SelectItem> listClasificacionSubtipo);

	boolean renderedVisualizarExpediente(Long idDocumento);

	boolean renderedVerExpediente(Long idDocumento);

	// boolean renderedVisualizarSolicitud (Long idDocumento);
	Long obtenerIdExpediente(String numeroExpediente);

	void buscarDivisiones();

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	List<SelectItem> getListOrganizacion();

	void setListOrganizacion(List<SelectItem> listOrganizacion);

	String getUrlFirma();

	List<ExpedienteBandejaSalida> buscarDocumentosCierre(Date fechaInicio, Date fechaTermino);

	Long getFechaInicio();

	Long getFechaTermino();

	Long getFechaCierre();
}
