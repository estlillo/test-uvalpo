package cl.exe.exedoc.reportes.electronicos;

import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.estructura.ResultReporteDocumentosElectronicos;

/**
 * @author Pablo
 *
 */
public interface ReporteDocumentosElectronicos {

	/**
	 * @param tipo {@link Integer}
	 * @return {@link String}
	 */
	String begin(int tipo);

	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();
	
	/**
	 * @param titulo {@link String}
	 */
	void setTitulo(String titulo);

	/**
	 * @return {@link String}
	 */
	String getTitulo();

	/**
	 * @param inicio {@link Date}
	 */
	void setInicio(Date inicio);

	/**
	 * @return {@link Date}
	 */
	Date getInicio();

	/**
	 * @param fin {@link Date}
	 */
	void setFin(Date fin);

	/**
	 * @return {@link Date}
	 */
	Date getFin();

	/**
	 * @param tipoDocumento {@link Long}
	 */
	void setTipoDocumento(Long tipoDocumento);

	/**
	 * @return {@link Long}
	 */
	Long getTipoDocumento();

	/**
	 * @param tiposDocumentos {@link List} of {@link SelectItem}
	 */
	void setTiposDocumentos(List<SelectItem> tiposDocumentos);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getTiposDocumentos();

	/**
	 * 
	 */
	void buscarDivisiones();

	/**
	 * 
	 */
	void buscarDepartamentos();

	/**
	 * 
	 */
	void buscarUnidadesOrganizacionales();

	/**
	 * 
	 */
	void buscarCargos();

	void buscarPersonas();

	Long getOrganizacion();

	void setOrganizacion(Long organizacion);

	Long getDivision();

	void setDivision(Long division);

	Long getDepartamento();

	void setDepartamento(Long departamento);

	Long getUnidadOrganizacional();

	void setUnidadOrganizacional(Long unidadOrganizacional);

	Long getCargo();

	void setCargo(Long cargo);

	Long getPersona();

	void setPersona(Long persona);

	List<SelectItem> getListDivision();

	void setListDivision(List<SelectItem> listDivision);

	List<SelectItem> getListDepartamento();

	void setListDepartamento(List<SelectItem> listDepartamento);

	List<SelectItem> getListUnidadesOrganizacionales();

	void setListUnidadesOrganizacionales(
			List<SelectItem> listUnidadesOrganizacionales);

	List<SelectItem> getListCargos();

	void setListCargos(List<SelectItem> listCargos);

	List<SelectItem> getListPersonas();

	void setListPersonas(List<SelectItem> listPersonas);

	void buscar();

	void setResultDoc(List<ResultReporteDocumentosElectronicos> resultDoc);

	List<ResultReporteDocumentosElectronicos> getResultDoc();

	boolean isFirmado();

}
