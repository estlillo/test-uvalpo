package cl.exe.exedoc.reportes.electronicos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.estructura.ResultReporteDocumentosElectronicos;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * Implementacion de {@link ReporteDocumentosElectronicos}.
 * 
 */
@Stateful
@Name(ReporteDocumentosElectronicosBean.NAME)
@Scope(ScopeType.APPLICATION)
public class ReporteDocumentosElectronicosBean implements
		ReporteDocumentosElectronicos, Serializable {

	/**
	 * NAME = "reportesDocumentosEletronicos".
	 */
	public static final String NAME = "reportesDocumentosEletronicos";

	/**
	 * 
	 */
	private static final long serialVersionUID = 5598418620683741737L;
	
	private static final String CUALQUIERA = "Cualquiera";

	private static final String SELECCIONAR = "<<Seleccionar>>";
	private static final String ID = "id";

	/**
	 * 
	 */
	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@Logger
	private Log log;

	@In(required = false)
	private Persona usuario;
	
	private String titulo;

	private Date inicio;
	private Date fin;

	private Long tipoDocumento;

	private List<SelectItem> tiposDocumentos;

	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;
	private Long persona = JerarquiasLocal.INICIO;

	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();

	private List<ResultReporteDocumentosElectronicos> resultDoc = new ArrayList<ResultReporteDocumentosElectronicos>();

	private boolean isFirmado;

	@Override
	public boolean isFirmado() {
		return isFirmado;
	}

	/**
	 * Constructor.
	 */
	public ReporteDocumentosElectronicosBean() {
		super();
	}

	@Override
	public String begin(final int tipo) {

		if (tipo == 1) {
			this.titulo = "N° de Documentos Electrónicos Visados";
			this.isFirmado = false;
		} else {
			this.titulo = "N° de Documentos Electrónicos Firmados";
			this.isFirmado = true;
		}

		this.resultDoc = new ArrayList<ResultReporteDocumentosElectronicos>();

		this.cargarTiposDocumentos();

		listOrganizacion.addAll(this.buscarOrganizaciones());
		organizacion = (Long) listOrganizacion.get(1).getValue();
		
		this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getAllDepartamentos(SELECCIONAR);
        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.buscarUnidadesOrganizacionales();
		return NAME;
	}

	@Remove
	@Override
	public void remove() {
	}

	@Destroy
	@Override
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscar() {

		this.resultDoc = new ArrayList<ResultReporteDocumentosElectronicos>();

		String q = "SELECT * FROM expedientes_offline eo WHERE ";

		if (this.isFirmado) {
			q = q + "firmado = 1";
		} else {
			q = q + "visado = 1";
		}

		if (this.tipoDocumento.longValue() > 0) {
			q = q + " AND eo.id_tipodocumento = "
					+ this.tipoDocumento.toString();
		}

		if (this.persona.longValue() >= 0) {
			q = q + " AND eo.id_emisor = " + this.persona.toString();
		} else if (this.cargo.longValue() >= 0) {
			q = q + " AND eo.id_cargo = " + this.cargo.toString();
		} else if (this.unidadOrganizacional.longValue() >= 0) {
			q = q + " AND eo.id_unidad = "
					+ this.unidadOrganizacional.toString();
		} else if (this.departamento.longValue() >= 0) {
			q = q + " AND eo.id_departamento = " + this.departamento.toString();
		} else if (this.division.longValue() >= 0) {
			q = q + " AND eo.id_division = " + this.division.toString();
		}
		
		if (this.inicio != null && this.fin != null) {
			q = q + " AND eo.fecha_ingreso BETWEEN :inicio AND :fin";
		}

		q = q + " ORDER BY eo.id_division, eo.id_departamento, "
				+ "eo.id_unidad, eo.id_cargo, eo.id_emisor";

		log.info(q);

//		final Query query = em.createNativeQuery(q);
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT td.descripcion AS tipoDocumento, uo.descripcion AS unidad, ");
		sql.append("CONCAT(p.nombres, CONCAT(' ', p.apellidoPaterno)) AS usuario, COUNT(d.id) ");
		//sql.append("FROM Expediente e ");
		//sql.append("INNER JOIN e.documentos de ");
		//sql.append("INNER JOIN de.documento d ");
		sql.append("FROM Documento d ");
		sql.append("INNER JOIN d.tipoDocumento td ");
		if (this.isFirmado) {
			sql.append("INNER JOIN d.firmas vf ");
		} else {
			sql.append("INNER JOIN d.visaciones vf ");
		}
		sql.append("INNER JOIN vf.persona p ");
		sql.append("INNER JOIN p.cargo c ");
		sql.append("INNER JOIN c.unidadOrganizacional uo ");
		
		StringBuilder where = new StringBuilder();
		
		if (this.inicio != null && this.fin != null) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			if (this.isFirmado) {
				where.append("(vf.fechaFirma BETWEEN :inicio AND :fin) ");
			} else {
				where.append("(vf.fechaVisacion BETWEEN :inicio AND :fin) ");
			}
		}
		
		if (this.tipoDocumento.intValue() > 0) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			where.append("td = :tipoDocumento ");
		}

		if (!this.persona.equals(JerarquiasLocal.INICIO)) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			where.append("p = :persona ");
		}

		if (!this.cargo.equals(JerarquiasLocal.INICIO)) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			where.append("c = :cargo ");
		}
		
		if (!this.unidadOrganizacional.equals(JerarquiasLocal.INICIO)) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			where.append("uo = :unidad ");
		}

		if (!this.departamento.equals(JerarquiasLocal.INICIO)) {
			if (where.length() == 0) {
				where.append("WHERE ");
			} else {
				where.append("AND ");
			}
			where.append("uo.departamento = :departamento ");
		}
		
		if (where.length() > 0) {
			sql.append(where);
		}
		
		sql.append("GROUP BY td.descripcion, uo.descripcion, ");
		sql.append("CONCAT(p.nombres, CONCAT(' ', p.apellidoPaterno))");
				
		final Query query = em.createQuery(sql.toString());
		
		if (this.inicio != null && this.fin != null) {
			query.setParameter("inicio", FechaUtil.inicio(this.inicio), TemporalType.TIMESTAMP);
			query.setParameter("fin", FechaUtil.fin(this.fin), TemporalType.TIMESTAMP);
		}
		
		if (this.tipoDocumento.intValue() > 0) {
			query.setParameter("tipoDocumento", new TipoDocumento(this.tipoDocumento.intValue()));
		}

		if (!this.persona.equals(JerarquiasLocal.INICIO)) {
			query.setParameter("persona", new Persona(this.persona));
		}

		if (!this.cargo.equals(JerarquiasLocal.INICIO)) {
			query.setParameter("cargo", new Cargo(this.cargo));
		}
		
		if (!this.unidadOrganizacional.equals(JerarquiasLocal.INICIO)) {
			query.setParameter("unidad", new UnidadOrganizacional(this.unidadOrganizacional));
		}

		if (!this.departamento.equals(JerarquiasLocal.INICIO)) {
			query.setParameter("departamento", new Departamento(this.departamento));
		}

		try {
			this.procesarDatos(query.getResultList());
		} catch (PersistenceException e) {
			FacesMessages.instance().add(
					"Ha ocurrido un error al realizar la operación.");
		}
	}

	@Override
	@SuppressWarnings({ "unchecked", "unused" })
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiar();
		organizacion = idOrganizacion;
		final Query query = em.createNamedQuery("Organizacion.findById");
		query.setParameter(ID, organizacion);
		final List<Organizacion> organizaciones = query.getResultList();
		if (organizaciones != null && organizaciones.size() == 1) {
			final Organizacion org = organizaciones.get(0);
		}
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL,
				organizacion);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getAllDepartamentos(
				JerarquiasLocal.TEXTO_INICIAL);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(
					JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento depto = departamentos.get(0);
			if (depto.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias
						.getIdUnidadVirtualDepartamento(this.division,
								this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(
				JerarquiasLocal.TEXTO_INICIAL, departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	/**
	 * 
	 */
	private void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(
				JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * 
	 */
	private void limpiar() {
		this.organizacion = JerarquiasLocal.INICIO;
		this.division = JerarquiasLocal.INICIO;
		this.departamento = JerarquiasLocal.INICIO;
		this.unidadOrganizacional = JerarquiasLocal.INICIO;
		this.cargo = JerarquiasLocal.INICIO;
		this.persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiar();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	/**
	 * 
	 */
	private void cargarTiposDocumentos() {

		this.setTiposDocumentos(TipoDocumentosList
				.getInstanceTipoDocumentoList(em)
				.getTiposDocumentoElectronicoList(SELECCIONAR));
	}

	/**
	 * @param resultList
	 *            {@link List}
	 */
	private void procesarDatos(final List<Object[]> resultList) {

		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		String sTipoDocumento = "";
		String sRango = "";
		ResultReporteDocumentosElectronicos rre = null;

		if (this.inicio != null && this.fin != null) {
			sRango = sdf.format(this.inicio) + " a " + sdf.format(this.fin);
		} else {
			sRango = "Siempre";
		}

		int cont = 0;

		if (this.tipoDocumento.longValue() <= 0) {
			sTipoDocumento = CUALQUIERA;
		}
		
		for (Object[] o : resultList) {
			rre = new ResultReporteDocumentosElectronicos();
			rre.setRango(sRango);
			rre.setTipoDocumento(o[0].toString());
			rre.setUo(o[1].toString());
			rre.setUsuario(o[2].toString());
			rre.setCantidad((Long)o[3]);
			this.resultDoc.add(rre);
		
			
//			td.descripcion AS tipoDocumento, uo.descripcion AS unidad, ");
//			sql.append("CONCAT(p.nombre, CONCAT(' ', p.apellidoPaterno)) AS usuario, COUNT(d.id)" +
		}
		/*for (Object[] ob : resultList) {
			
			td.descripcion AS tipoDocumento, uo.descripcion AS unidad, ");
			sql.append("CONCAT(p.nombre, CONCAT(' ', p.apellidoPaterno)) AS usuario, COUNT(d.id)" +
					"rre = new ResultReporteDocumentosElectronicos();

			if (sTipoDocumento.isEmpty()) {
				sTipoDocumento = (String) ob[24];
			}

			if (this.persona.longValue() >= 0) {

				rre = new ResultReporteDocumentosElectronicos();
				rre.setCantidad(Long.valueOf(resultList.size()));
				rre.setTipoDocumento(sTipoDocumento);
				rre.setUo((String) ob[13]);
				rre.setUsuario((String) ob[11]);
				rre.setRango(sRango);

				this.resultDoc.add(rre);
				break;

			} else if (this.cargo.longValue() >= 0) {

				if (cont == 0) {

					rre = new ResultReporteDocumentosElectronicos();
					rre.setCantidad(Long.valueOf(resultList.size()));
					rre.setTipoDocumento(sTipoDocumento);
					rre.setIduo(((BigDecimal) ob[12]).longValue());
					rre.setUo((String) ob[13]);
					rre.setIdusuario((Long) ob[10]);
					rre.setUsuario((String) ob[11]);
					rre.setRango(sRango);

					this.resultDoc.add(rre);
				} else {

					if (this.resultDoc.get(this.resultDoc.size() - 1)
							.getIdusuario().longValue() == ((BigDecimal) ob[10])
							.longValue()) {

						this.resultDoc.get(this.resultDoc.size() - 1)
								.setCantidad(
										this.resultDoc
												.get(this.resultDoc.size() - 1)
												.getCantidad().longValue() + 1);
					} else {

						rre = new ResultReporteDocumentosElectronicos();
						rre.setCantidad(Long.valueOf(resultList.size()));
						rre.setTipoDocumento(sTipoDocumento);
						rre.setIduo(((BigDecimal) ob[12]).longValue());
						rre.setUo((String) ob[13]);
						rre.setIdusuario((Long) ob[10]);
						rre.setUsuario((String) ob[11]);
						rre.setRango(sRango);

						this.resultDoc.add(rre);
					}
				}

			} else if (this.unidadOrganizacional.longValue() >= 0) {

				if (cont == 0) {

					rre = new ResultReporteDocumentosElectronicos();
					rre.setCantidad(Long.valueOf(resultList.size()));
					rre.setTipoDocumento(sTipoDocumento);
					rre.setIduo(((BigDecimal) ob[12]).longValue());
					rre.setUo((String) ob[13]);
					rre.setIdusuario(new Long(0));
					rre.setUsuario(CUALQUIERA);
					rre.setRango(sRango);

					this.resultDoc.add(rre);
				} else {

					if (this.resultDoc.get(this.resultDoc.size() - 1).getIduo()
							.longValue() == ((BigDecimal) ob[12]).longValue()) {

						this.resultDoc.get(this.resultDoc.size() - 1)
								.setCantidad(
										this.resultDoc
												.get(this.resultDoc.size() - 1)
												.getCantidad().longValue() + 1);
					} else {

						rre = new ResultReporteDocumentosElectronicos();
						rre.setCantidad(Long.valueOf(resultList.size()));
						rre.setTipoDocumento(sTipoDocumento);
						rre.setIduo(((BigDecimal) ob[12]).longValue());
						rre.setUo((String) ob[13]);
						rre.setIdusuario(new Long(0));
						rre.setUsuario(CUALQUIERA);
						rre.setRango(sRango);

						this.resultDoc.add(rre);
					}
				}

			} else if (this.departamento.longValue() >= 0) {

				if (cont == 0) {

					rre = new ResultReporteDocumentosElectronicos();
					rre.setCantidad(Long.valueOf(resultList.size()));
					rre.setTipoDocumento(sTipoDocumento);
					rre.setIduo(((BigDecimal) ob[14]).longValue());
					rre.setUo((String) ob[15]);
					rre.setIdusuario(new Long(0));
					rre.setUsuario(CUALQUIERA);
					rre.setRango(sRango);

					this.resultDoc.add(rre);
				} else {

					if (this.resultDoc.get(this.resultDoc.size() - 1).getIduo()
							.longValue() == ((BigDecimal) ob[14]).longValue()) {

						this.resultDoc.get(this.resultDoc.size() - 1)
								.setCantidad(
										this.resultDoc
												.get(this.resultDoc.size() - 1)
												.getCantidad().longValue() + 1);
					} else {

						rre = new ResultReporteDocumentosElectronicos();
						rre.setCantidad(Long.valueOf(resultList.size()));
						rre.setTipoDocumento(sTipoDocumento);
						rre.setIduo(((BigDecimal) ob[14]).longValue());
						rre.setUo((String) ob[15]);
						rre.setIdusuario(new Long(0));
						rre.setUsuario(CUALQUIERA);
						rre.setRango(sRango);

						this.resultDoc.add(rre);
					}
				}

			} else if (this.division.longValue() >= 0) {

				if (cont == 0) {

					rre = new ResultReporteDocumentosElectronicos();
					rre.setCantidad(Long.valueOf(resultList.size()));
					rre.setTipoDocumento(sTipoDocumento);
					rre.setIduo(((BigDecimal) ob[16]).longValue());
					rre.setUo((String) ob[17]);
					rre.setIdusuario(new Long(0));
					rre.setUsuario(CUALQUIERA);
					rre.setRango(sRango);

					this.resultDoc.add(rre);
				} else {

					if (this.resultDoc.get(this.resultDoc.size() - 1).getIduo()
							.longValue() == ((BigDecimal) ob[16]).longValue()) {

						this.resultDoc.get(this.resultDoc.size() - 1)
								.setCantidad(
										this.resultDoc
												.get(this.resultDoc.size() - 1)
												.getCantidad().longValue() + 1);
					} else {

						rre = new ResultReporteDocumentosElectronicos();
						rre.setCantidad(Long.valueOf(resultList.size()));
						rre.setTipoDocumento(sTipoDocumento);
						rre.setIduo(((BigDecimal) ob[16]).longValue());
						rre.setUo((String) ob[17]);
						rre.setIdusuario(new Long(0));
						rre.setUsuario(CUALQUIERA);
						rre.setRango(sRango);

						this.resultDoc.add(rre);
					}
				}
			} else {

				if (cont == 0) {

					rre = new ResultReporteDocumentosElectronicos();
					rre.setCantidad(Long.valueOf(resultList.size()));
					rre.setTipoDocumento(sTipoDocumento);
					rre.setIduo(new Long(0));
					rre.setUo(CUALQUIERA);
					rre.setIdusuario(new Long(0));
					rre.setUsuario(CUALQUIERA);
					rre.setRango(sRango);

					this.resultDoc.add(rre);
				} else {
					this.resultDoc.get(0)
							.setCantidad(
									this.resultDoc.get(0).getCantidad()
											.longValue() + 1);
				}
			}

			cont++;
		}*/
	}

	@Override
	public void setTitulo(final String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String getTitulo() {
		return titulo;
	}

	@Override
	public void setInicio(final Date inicio) {
		this.inicio = inicio;
	}

	@Override
	public Date getInicio() {
		return inicio;
	}

	@Override
	public void setFin(final Date fin) {
		this.fin = fin;
	}

	@Override
	public Date getFin() {
		return fin;
	}

	@Override
	public void setTipoDocumento(final Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Long getTipoDocumento() {
		return tipoDocumento;
	}

	@Override
	public void setTiposDocumentos(final List<SelectItem> tiposDocumentos) {
		this.tiposDocumentos = tiposDocumentos;
	}

	@Override
	public List<SelectItem> getTiposDocumentos() {
		return tiposDocumentos;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	@Override
	public void setUnidadOrganizacional(final Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	@Override
	public void setListUnidadesOrganizacionales(
			final List<SelectItem> listUnidadesOrganizacionales) {
		this.listUnidadesOrganizacionales = listUnidadesOrganizacionales;
	}

	@Override
	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	@Override
	public void setListCargos(final List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public void setResultDoc(
			final List<ResultReporteDocumentosElectronicos> resultDoc) {
		this.resultDoc = resultDoc;
	}

	@Override
	public List<ResultReporteDocumentosElectronicos> getResultDoc() {
		return resultDoc;
	}

}
