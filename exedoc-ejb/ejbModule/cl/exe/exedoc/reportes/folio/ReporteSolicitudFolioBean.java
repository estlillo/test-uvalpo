package cl.exe.exedoc.reportes.folio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.FolioDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.ReporteSolicitudFolioDocumento;
import cl.exe.exedoc.mantenedores.dao.AdministradorReporteFolio;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * @author pumanar
 * 
 *         Implementacion de {@link ReporteSolicitudFolio}.
 * 
 */
@Stateful
@Name(ReporteSolicitudFolioBean.NAME)
public class ReporteSolicitudFolioBean implements ReporteSolicitudFolio,
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2754461969050496923L;

	/**
	 * 
	 */
	private static final String SELECCIONAR = "<<Seleccionar>>";

	/**
	 * NAME = "ReporteSolicitudFolioBean".
	 */
	public static final String NAME = "reporteSolicitudFolio";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;

	@EJB
	private AdministradorReporteFolio adminReporteFolio;
	
	@In(required = true)
	private Persona usuario;

	private List<ReporteSolicitudFolioDocumento> solicitudes = new ArrayList<ReporteSolicitudFolioDocumento>();

	private List<SelectItem> fiscalias = new ArrayList<SelectItem>();
	private List<SelectItem> unidades = new ArrayList<SelectItem>();
	private List<SelectItem> tiposDocumentos = new ArrayList<SelectItem>();

	private Long fiscalia = JerarquiasLocal.INICIO;
	private Long unidad = JerarquiasLocal.INICIO;
	private Long tipoDocumento = JerarquiasLocal.INICIO;

	private static final Locale LOCALE = FechaUtil.LOCALE;
	private Integer agno;
	private Long folio;
	private Date fechaInicio;
	private Date fechaTermino;

	/**
	 * Constructor.
	 */
	public ReporteSolicitudFolioBean() {
		super();
	}

	@Override
	@Remove
	public void remove() {
	}

	@Override
	@Destroy
	public void destroy() {
	}

	@Override
	@Begin(join = true)
	public String begin() {
		log.info("beginning conversation");
		
		this.limpiar();
		this.setFiscalias(jerarquias.getFiscalias());
		this.setFiscalia(this.usuario.getCargo().getUnidadOrganizacional()
				.getDepartamento().getId());

		this.setUnidades(jerarquias.getUnidadesOrganzacionales(SELECCIONAR,
				this.usuario.getCargo().getUnidadOrganizacional()
						.getDepartamento().getId()));
		this.setUnidad(this.usuario.getCargo().getUnidadOrganizacional().getId());
		
		return NAME;
	}

	@Override
	@End
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Override
	public void buscar() {

		HashMap<String, Object> parametros = new HashMap<String, Object>();
		
		HashMap<String, Object> fechas = new HashMap<String, Object>();
		
		if(this.getFechaInicio() != null ){
			fechas.put("fechaInicio", this.getFechaInicio());
		}
		
		if(this.getFechaTermino() != null ){
			fechas.put("fechaTermino", this.getFechaTermino());
		}
		
		if (!this.getFiscalia().equals(JerarquiasLocal.INICIO)) {
			parametros.put("fiscalia", this.getFiscalia());
		}
		if (this.getUnidad() != null
				&& !this.getUnidad().equals(JerarquiasLocal.INICIO)) {
			parametros.put("unidad", this.getUnidad());
		}
		if (!this.getTipoDocumento().equals(new Long(0))) {
			parametros.put("tipoDocumento", this.getTipoDocumento());
		}
		if (this.getAgno() != null && this.getAgno() > 999) {
			parametros.put("agnoActual", this.getAgno());
		}
		if(this.getFolio() != null){
			if (!this.getFolio().equals(new Long(0))){
				parametros.put("numeroActual", this.getFolio());
			}
		}

		this.setSolicitudes(adminReporteFolio
				.buscarReporteSolicitudFolioDocumento(parametros, fechas));
	}
	

	public void limpiar() {

		this.setFiscalia(JerarquiasLocal.INICIO);
		this.setTipoDocumento(JerarquiasLocal.INICIO);
		this.setUnidad(JerarquiasLocal.INICIO);

		this.setAgno(null);

		this.setSolicitudes(new ArrayList<ReporteSolicitudFolioDocumento>());

		this.setFiscalias(jerarquias.getDepartamentos(SELECCIONAR));

		this.setTiposDocumentos(TipoDocumentosList
				.getInstanceTipoDocumentoList(em).getTiposDocumentoListVisible(
						SELECCIONAR));

		List<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR));
		this.setUnidades(list);
		
	}

	@Override
	public void buscarUnidades() {

		this.setUnidad(JerarquiasLocal.INICIO);

		if (this.getFiscalia().equals(JerarquiasLocal.INICIO)) {
			List<SelectItem> list = new ArrayList<SelectItem>();
			list.add(new SelectItem(JerarquiasLocal.INICIO, SELECCIONAR));
			this.setUnidades(list);
		} else {
			this.setUnidades(this.jerarquias.getUnidadesOrganzacionales(
					SELECCIONAR, this.getFiscalia()));
		}
	}

	@Override
	public List<ReporteSolicitudFolioDocumento> getSolicitudes() {
		return solicitudes;
	}

	@Override
	public void setSolicitudes(List<ReporteSolicitudFolioDocumento> solicitudes) {
		this.solicitudes = solicitudes;
	}

	@Override
	public List<SelectItem> getFiscalias() {
		return fiscalias;
	}

	@Override
	public void setFiscalias(List<SelectItem> fiscalias) {
		this.fiscalias = fiscalias;
	}

	@Override
	public List<SelectItem> getUnidades() {
		return unidades;
	}

	@Override
	public void setUnidades(List<SelectItem> unidades) {
		this.unidades = unidades;
	}

	@Override
	public List<SelectItem> getTiposDocumentos() {
		return tiposDocumentos;
	}

	@Override
	public void setTiposDocumentos(List<SelectItem> tiposDocumentos) {
		this.tiposDocumentos = tiposDocumentos;
	}

	@Override
	public Long getFiscalia() {
		return fiscalia;
	}

	@Override
	public void setFiscalia(Long fiscalia) {
		this.fiscalia = fiscalia;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public Long getTipoDocumento() {
		return tipoDocumento;
	}

	@Override
	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public Integer getAgno() {
		return agno;
	}

	@Override
	public void setAgno(Integer agno) {
		this.agno = agno;
	}
	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}
	@Override
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	@Override
	public Date getFechaTermino() {
		return fechaTermino;
	}
	@Override
	public void setFechaTermino(final Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	@Override
	public Locale getLocale() {
		return LOCALE;
	}
	@Override
	public Long getFolio() {
		return folio;
	}
	@Override
	public void setFolio(Long folio) {
		this.folio = folio;
	}

	
	
}
