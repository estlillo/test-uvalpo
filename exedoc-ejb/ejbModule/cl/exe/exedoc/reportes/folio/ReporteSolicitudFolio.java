package cl.exe.exedoc.reportes.folio;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.model.SelectItem;

import cl.exe.exedoc.entity.ReporteSolicitudFolioDocumento;

/**
 * @author pumanar
 *
 */
public interface ReporteSolicitudFolio {

	/**
	 * @return {@link String}
	 */
	String begin();

	/**
	 * @return {@link String}
	 */
	String end();

	/**
	 * 
	 */
	void remove();

	/**
	 * 
	 */
	void destroy();
	
	/**
	 * 
	 */
	void buscar();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getFiscalias();

	/**
	 * @param fiscalias {@link List} of {@link SelectItem}
	 */
	void setFiscalias(List<SelectItem> fiscalias);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getUnidades();

	/**
	 * @param unidades {@link List} of {@link SelectItem}
	 */
	void setUnidades(List<SelectItem> unidades);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getTiposDocumentos();

	/**
	 * @param tiposDocumentos {@link List} of {@link SelectItem}
	 */
	void setTiposDocumentos(List<SelectItem> tiposDocumentos);

	/**
	 * @return {@link Long}
	 */
	Long getFiscalia();

	/**
	 * @param fiscalia {@link Long}
	 */
	void setFiscalia(Long fiscalia);

	/**
	 * @return {@link Long}
	 */
	Long getUnidad();

	/**
	 * @param unidad {@link Long}
	 */
	void setUnidad(Long unidad);

	/**
	 * @return {@link Long}
	 */
	Long getTipoDocumento();

	/**
	 * @param tipoDocumento {@link Long}
	 */
	void setTipoDocumento(Long tipoDocumento);

	/**
	 * @return {@link Integer}
	 */
	Integer getAgno();

	/**
	 * @param agno {@link Integer}
	 */
	void setAgno(Integer agno);

	/**
	 * @return {@link List} of {@link ReporteSolicitudFolioDocumento}
	 */
	
	Long getFolio();

	/**
	 * @param unidad {@link Long}
	 */
	void setFolio(Long folio);

	/**
	 * @return {@link Long}
	 */
	
	List<ReporteSolicitudFolioDocumento> getSolicitudes();

	/**
	 * @param solicitudes {@link List} of {@link ReporteSolicitudFolioDocumento}
	 */
	void setSolicitudes(List<ReporteSolicitudFolioDocumento> solicitudes);

	/**
	 * busca unidades relacionadas con la fiscalía
	 */
	void buscarUnidades();

	/**
	 * @return {@link Date}
	 */
	Date getFechaInicio();

	/**
	 * @param fechaInicio {@link Date} of {@link ReporteSolicitudFolioDocumento}
	 */
	void setFechaInicio(Date fechaInicio);

	/**
	 * @return {@link Date}
	 */
	Date getFechaTermino();

	/**
	 * @param fechaTermino {@link Date} of {@link ReporteSolicitudFolioDocumento}
	 */
	void setFechaTermino(Date fechaTermino);

	/**
	 * @return {@link Locale}
	 */
	Locale getLocale();

}
