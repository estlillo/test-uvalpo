package cl.exe.exedoc.reportes.unidad;

import static cl.exe.exedoc.util.JerarquiasLocal.INICIO;
import static cl.exe.exedoc.util.JerarquiasLocal.TEXTO_INICIAL;

import java.io.Serializable;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.bandeja.Bandeja;

/**
 * Class utilizada para crear reporte de expedientes despachados.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(ReporteExpedienteUnidadBean.NAME)
public class ReporteExpedienteUnidadBean implements ReporteExpedienteUnidad, Serializable {

	/**
	 * NAME.
	 */
	public static final String NAME = "reporteExpedienteUnidad";

	private static final String DESTINATARIO_HISTORICO = "destinatarioHistorico";
	private static final String DESTINATARIO = "destinatario";

	private static final long serialVersionUID = -4056048351930327337L;

	private static final String STRING_INICIO = "inicio";
	private static final String STRING_TERMINO = "termino";
	private static final String INCLUIR_FECHA = "Debe incluir la fecha de {0}";
	private static final String SELECCINAR_INICIO = "<<Seleccionar>>";
	private static final Locale LOCALE = FechaUtil.LOCALE;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	private Persona usuario;
	
	@In(required = false,  scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;      
	
	@EJB
	private JerarquiasLocal jerarquias;
	@EJB
	private Bandeja bandeja;

	private Long organizacion = INICIO;
	private Long division = INICIO;
	private Long departamento = INICIO;
	private Long unidad = INICIO;
	private Long cargo = INICIO;
	private Long persona = INICIO;

	private Date fechaInicio;
	private Date fechaTermino;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> bandejaLista;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listPersonas;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidad;
	private List<SelectItem> listCargo;

	/**
	 * Constructor.
	 */
	public ReporteExpedienteUnidadBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation");
		log.info("homeCrear reporteExpedienteUnidad: {0}", homeCrear);
		this.listOrganizacion = this.buscarOrganizaciones();    
		this.listDepartamento = this.iniciarLista();
		this.listUnidad = this.iniciarLista();
		this.listPersonas = this.iniciarLista();
		this.listCargo = this.iniciarLista();
		this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();		
		
		//listOrganizacion.addAll(this.buscarOrganizaciones());
		//organizacion = (Long) listOrganizacion.get(1).getValue();
		
		//this.listDivision = this.buscarDivisiones();
		//division = (Long) listDivision.get(1).getValue();
		organizacion = INICIO;
		division = INICIO;
		 
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos(SELECCINAR_INICIO);
		this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		this.buscarUnidades();
		this.homeCrear = ReporteExpedienteUnidadBean.NAME;
		return ReporteExpedienteUnidadBean.NAME;
	}

	/**
	 * Metodo que iniciliza una lista de un formularios.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> iniciarLista() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(INICIO, TEXTO_INICIAL));
		return lista;
	}



	@End
	@Override
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Remove
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdExpediente(final String numeroExpediente) {
		Long idExpediente = null;
		final StringBuffer hql = new StringBuffer(10);
		hql.append("SELECT DISTINCT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = :numeroExpediente ");
		hql.append("order by e.id desc ");
		final Query query = em.createQuery(hql.toString());
		query.setParameter("numeroExpediente", numeroExpediente);
		final List<Long> idExpedientes = (List<Long>) query.getResultList();
		if (idExpedientes.size() >= 1) {
			idExpediente = idExpedientes.get(0);
		}
		return idExpediente;
	}

	@Override
	public Boolean renderedVisualizarExpediente(final Long idDocumento) {
		Boolean valido = true;
		final Documento doc = em.find(Documento.class, idDocumento);
		if (doc != null && doc.getFormatoDocumento() != null) {
			if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.PAPEL)) {
				valido = false;
			}
			if (doc.getReservado()) {
				for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
					final Persona p = destinatario.getDestinatarioPersona();
					if (p == null) { return false; }
					if (!destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
						valido = false;
					} else {
						valido = true;
						break;
					}
				}
			}
		}
		return valido;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscar() {
		log.info("[ReporteHojaDespacho] iniciando busqueda...");

		if (this.validarFechas()) {
			Query query2 = generarQueryExpedientesPorUnidadesOrganizacionales();
			List<Object[]> lista2 = (List<Object[]>) query2.getResultList();
			this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();

			// final Query query = this.generarQuery();
			// final List<Object[]> lista = (List<Object[]>) query.getResultList();
			log.info("lista expedientes {0}", lista2.size());
			this.procesarResultadoBusqueda(lista2);
		}
	}

	/**
	 * Metodo encargodo de validar las fechas de inicio y termino correspondientes a la fecha de despacho de un
	 * expediente.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarFechas() {
		Boolean validar = Boolean.TRUE;

		if (fechaInicio == null && fechaTermino == null) {
			FacesMessages.instance().add("Se debe incluir mínimo, el rango de fechas");
			validar = Boolean.FALSE;
		} else if (fechaInicio != null && fechaTermino == null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_TERMINO));
			validar = Boolean.FALSE;
		} else if (fechaInicio == null && fechaTermino != null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_INICIO));
			validar = Boolean.FALSE;
		}
		return validar;
	}

	/**
	 * Metodo encargado de procesar los resultado de la busqueda.
	 * 
	 * @param lista {@link List} of {@link Object[]}
	 */
	
	private void procesarResultadoBusqueda(final List<Object[]> lista) {
		int i = 0;
		Long prevExp = 0L;
		for (Object[] o : lista) {
			if (!((Long)o[0]).equals(prevExp)) {
				ExpedienteBandejaSalida bs = new ExpedienteBandejaSalida();
				bs.setId((Long)o[0]);
				bs.setNumeroExpediente(o[1].toString());
				bs.setFechaIngreso((Date)o[2]);
				bs.setUnidadEmisor(o[3].toString());
				if (bs.getDocumentosRelacionados() == null) {
					bs.setDocumentosRelacionados(new ArrayList<Documento>());
				}
				Documento d = new Documento();
				d.setNumeroDocumento(o[4].toString());
				TipoDocumento td = new TipoDocumento(); 
				td.setDescripcion(o[5].toString());
				d.setTipoDocumento(td);
				bs.getDocumentosRelacionados().add(d);
				bandejaLista.add(bs);
			} else {
				Documento d = new Documento();
				d.setNumeroDocumento(o[4].toString());
				TipoDocumento td = new TipoDocumento(); 
				td.setDescripcion(o[5].toString());
				d.setTipoDocumento(td);
				bandejaLista.get(bandejaLista.size() - 1).getDocumentosRelacionados().add(d);
			}
			prevExp = (Long)o[0];
		}
		Collections.sort(bandejaLista, ComparatorUtils.compararListaBandejaSalida);
	}
//	private void procesarResultadoBusqueda(final List<Object[]> lista) {
//		List<Documento> expedientesHijos = null;
//		ExpedienteBandejaSalida bs = null;
//		List<UnidadOrganizacional> unidades = new ArrayList<UnidadOrganizacional>();
//
//		for (Object[] obj : lista) {
//			final Long idExp = ((Long) obj[0] != null) ? ((Long) obj[0]).longValue() : null;
//			final String numeroExp = (String) obj[1];
//			Date fechaDespacho = (Date) obj[2];
//			final Date fechaIngreso = (Date) obj[3];
//			final String nombreEmisor = (String) obj[4];
//			final String apellidoPatEmisor = (String) obj[5];
//			final String apellidoMatEmisor = (String) obj[6];
//			final String unidadOrganizacional = (String) obj[9];
//
//			final Expediente exp = new Expediente(idExp);
//			exp.setNumeroExpediente(numeroExp);
//
//			bs = new ExpedienteBandejaSalida();
//			bs.setId(idExp);
//			bs.setNumeroExpediente(numeroExp);
//			bs.setFechaIngreso(fechaIngreso);
//			bs.setFechaDespacho(fechaDespacho);
//			bs.setFechaIngresoBandeja(fechaDespacho);
//			bs.setEmisor(MessageFormat.format("{0} {1} {2}", nombreEmisor, apellidoPatEmisor, apellidoMatEmisor));
//			bs.setUnidadOrg(unidadOrganizacional);
//
//			UnidadOrganizacional aux = new UnidadOrganizacional();
//			aux.setDescripcion(unidadOrganizacional);
//
//			unidades.add(aux);
//			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
//			String s = formatter.format(fechaInicio);
//			String s2 = formatter.format(fechaTermino);
//			bs.setRangoFechas(s + " a " + s2);
//
//			// bandeja.obtenerDatosUltimoDocumento(exp, bs, respuestas, originales);
//			expedientesHijos = this.obtenerDocumentosExpedientes(exp.getNumeroExpediente());
//
//			if (expedientesHijos.size() != 0) {
//				bs.setDocumentosRelacionados(expedientesHijos);
//			}
//
//			// if (expedientesHijos.size() != 0) {
//			// fechaDespacho = expedientesHijos.get(0).getFechaDespacho() != null ? expedientesHijos.get(0)
//			// .getFechaDespacho() : expedientesHijos.get(0).getFechaDespachoHistorico();
//			// bs.setFechaDespacho(fechaDespacho);
//			// bs.setDatosUsuariosAsociados(bandeja.obtenerDatosUsuariosAsociados(expedientesHijos, numeroExp));
//			// bandejaLista.add(bs);
//			// }
//
//			bandejaLista.add(bs);
//		}
//
//		Collections.sort(bandejaLista, ComparatorUtils.compararListaBandejaSalida);
//	}

	private List<UnidadOrganizacional> limpiaUnidades(List<UnidadOrganizacional> lista) {
		List<UnidadOrganizacional> unidades = new ArrayList<UnidadOrganizacional>();
		for (Iterator<UnidadOrganizacional> iterator = lista.iterator(); iterator.hasNext();) {
			UnidadOrganizacional unidadOrganizacional = (UnidadOrganizacional) iterator.next();

		}
		return null;
	}

	/**
	 * Metodo que obtienes la lista de expedientes que se han despachados por expediente.
	 * 
	 * @param idPadre {@link Long} identificador del expediente padre.
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private List<Documento> obtenerDocumentosExpedientes(final String idPadre) {
		final List<Documento> listDocsHijos = new ArrayList<Documento>();
		Documento exp = null;
		final List<Object[]> lista = new ArrayList<Object[]>();

		final Query queryD = this.construirQuery(idPadre);
		final List<Object[]> listDest = queryD.getResultList();
		lista.addAll(listDest);

		//log.info("lista de expedientes relacionados: {0}", listDest.size());

		for (Object[] o : lista) {
			exp = this.construirDocumento(o);
			listDocsHijos.add(exp);
		}
		return listDocsHijos;
	}

	/**
	 * Metodo que contruye query para obtener expedientes despachados por unidad.
	 * 
	 * @param idPadre {@link Long}
	 * @param clazz {@link String}
	 * @return {@link Query}
	 */
	private Query construirQuery(final String idPadre) {
		
		// where e.numero_expediente = 'E208/2011' and e.id = de.id_expediente and d.id = de.id_documento
		// and d.id_tipo_documento = td.id

		final StringBuilder sql = new StringBuilder();
		StringBuilder jpQL = new StringBuilder(
				"select de.id as id_doc_exp, de.tipoDocumentoExpediente.id, td.descripcion as desc_td, ");
		jpQL.append("d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, d.id as id_doc , td.id");
		jpQL.append(" from DocumentoExpediente de, Expediente e, Documento d, ");
		jpQL.append(" TipoDocumento td ");
		jpQL.append(" where e.numeroExpediente = '" + idPadre + "' and e.id = de.expediente.id ");
		jpQL.append("and d.id = de.documento.id and d.tipoDocumento.id = td.id");
		final Query query = em.createQuery(jpQL.toString());
		return query;
	}

	/**
	 * Metodo que construye un Documeno a partir de un arreglo de object.
	 * 
	 * @param o {@link Object}
	 * @return {@link Expediente}
	 */
	private Documento construirDocumento(final Object[] o) {
		// [103301, 1, Resolucion Electronica, s/n, null, Modifica Resolucion FN/MP N.° XXX, 101300]
		final Documento doc = new Documento();

		final Long idDoc = ((Long) o[0] != null) ? ((Long) o[0]).longValue() : null;
		final int tipoDocExp = (Integer) o[1];
		final String descTipoDocExp = ((String) o[2]);
		final String numDoc = (String) o[3];
		final Date fechaOrigen = (Date) o[4];
		final String materia = (String) o[5];
		final Long num = (Long) o[6];

		doc.setId(idDoc);
		TipoDocumento tipo = new TipoDocumento();
		tipo.setDescripcion(descTipoDocExp);
		doc.setTipoDocumento(tipo);
		doc.setFechaCreacion(fechaOrigen);

		return doc;
	}
	/**
	 * Metodo que crea query, para rescatar de la base de datos los expedientes despachados.
	 * 
	 * @return {@link Query}
	 */

	private Query generarQueryExpedientesPorUnidadesOrganizacionales() {
		
		final StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT DISTINCT  e.id, ");
		sql.append("	e.numeroExpediente, ");
		sql.append("	e.fechaIngreso, ");
		sql.append("	u.descripcion, ");
		sql.append("    doc.numeroDocumento, ");
		sql.append("    td.descripcion, ");
		sql.append("    doc.fechaCreacion, ");
		sql.append("    doc.id ");
		sql.append("from Expediente e, Expediente e1 ");
		sql.append("	inner join e.emisor p ");
		sql.append("	inner join p.cargo c ");
		sql.append("	inner join c.unidadOrganizacional u ");
		sql.append("	inner join u.departamento d ");
		sql.append("	inner join d.division div ");
		sql.append("	inner join div.organizacion org ");
		sql.append("    inner join e1.documentos de ");
		sql.append("    inner join de.documento doc ");
		sql.append("    inner join doc.tipoDocumento td ");
		sql.append("	where e.fechaDespacho is not null ");
		sql.append("	and e.versionPadre = null ");
		sql.append("    and e.numeroExpediente = e1.numeroExpediente ");
		if (!unidad.equals(INICIO)) {
			sql.append("	and u.id = :unidad ");
		}
		if (!departamento.equals(INICIO)) {
			sql.append("	and d.id = :departamento ");
		}
		if (!division.equals(INICIO)) {
			sql.append("	and div.id = :division ");
		}
		if (!organizacion.equals(INICIO)) {
			sql.append("	and org.id = :organizacion ");
		}
		if (!cargo.equals(INICIO)) {
			sql.append("	and c.id = :cargo ");
		}
		final Date inicio = FechaUtil.inicio(fechaInicio);
		final Date termino = FechaUtil.fin(fechaTermino);
		log.info("inicio {0}", FechaUtil.BDSDF.format(inicio));
		log.info("termino {0}", FechaUtil.BDSDF.format(termino));

		sql.append("	and e.fechaIngreso between :inicio and :termino ");
		sql.append("    order by e.fechaIngreso, e.id,  doc.fechaCreacion");

		log.info("Query: {0}", sql.toString());

		final Query query = em.createQuery(sql.toString());
		query.setParameter(STRING_INICIO, inicio, TemporalType.TIMESTAMP);
		query.setParameter(STRING_TERMINO, termino, TemporalType.TIMESTAMP);
		if (!organizacion.equals(INICIO)) {
			query.setParameter("organizacion", organizacion);
		}
		if (!division.equals(INICIO)) {
			query.setParameter("division", division);
		}
		if (!departamento.equals(INICIO)) {
			query.setParameter("departamento", departamento);
		}
		if (!unidad.equals(INICIO)) {
			query.setParameter("unidad", unidad);
		}
		if (!cargo.equals(INICIO)) {
			query.setParameter("cargo", cargo);
		}
		return query;
	}
	
	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		return jerarquias.getOrganizaciones(TEXTO_INICIAL);
	}
	
	@Override
	public List<SelectItem> buscarDivisiones() {
		return jerarquias.getDivisiones(TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDepartamentos() {
		this.listDepartamento = jerarquias.getAllDepartamentos(TEXTO_INICIAL);
		listUnidad =  this.iniciarLista();
		listCargo = this.iniciarLista();
		listPersonas = this.iniciarLista();
	}

	@Override
	public void buscarUnidades() {
		this.listUnidad = jerarquias.getUnidadesOrganzacionales(TEXTO_INICIAL, departamento);
		listCargo = this.iniciarLista();
		listPersonas = this.iniciarLista();
	}

	@Override
	public void buscarCargos() {
		listCargo = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				unidad);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
				JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL,
				cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
		if(listPersonas.size() == 0){
			listPersonas.clear();
			listPersonas.add(new SelectItem(JerarquiasLocal.INICIO,
					JerarquiasLocal.TEXTO_INICIAL));
		}
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(final Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<ExpedienteBandejaSalida> getBandejaLista() {
		return bandejaLista;
	}

	@Override
	public void setBandejaLista(final List<ExpedienteBandejaSalida> bandeja) {
		this.bandejaLista = bandeja;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidad() {
		return listUnidad;
	}

	@Override
	public void setListUnidad(final List<SelectItem> listUnidad) {
		this.listUnidad = listUnidad;
	}

	@Override
	public List<SelectItem> getListCargo() {
		return listCargo;
	}

	@Override
	public void setListCargo(final List<SelectItem> listCargo) {
		this.listCargo = listCargo;
	}

	@Override
	public Locale getLocale() {
		return LOCALE;
	}

	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

	@Override
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Override
	public Date getFechaTermino() {
		return fechaTermino;
	}

	@Override
	public void setFechaTermino(final Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

}
