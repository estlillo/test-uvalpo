package cl.exe.exedoc.reportes;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;

@Local
public interface ReporteExpedientesReenviadosInterface {

	// Bean
	public void destroy();
	
	// Logica
	public String initBusquedaExpReenviados();
	public void buscar();
	public Integer obtenerVecesEnBE(String numeroExpediente);
//	public boolean esFactura();
//	public void buscarClasificaciones();
	public boolean renderedVerExpediente(Long idDocumento);
	
	// Getters && Setters
	public String getBNumExp();
	public void setBNumExp(String numExp);
	public Integer getBIdTipoDoc();
	public void setBIdTipoDoc(Integer idTipoDoc);
	public Date getBIFechaDoc();
	public void setBIFechaDoc(Date fechaDoc);
	public Date getBTFechaDoc();
	public void setBTFechaDoc(Date fechaDoc);
	public String getBEmisor();
	public void setBEmisor(String emisor);
	public String getBFRutProveedor();
	public void setBFRutProveedor(String rutProveedor);
	public String getBFNumOrdenCompra();
	public void setBFNumOrdenCompra(String numOrdenCompra);
	public List<ExpedienteBandejaEntrada> getExpReenviados();
	public void setExpReenviados(List<ExpedienteBandejaEntrada> expReenviados);
	public Locale getLocale();
	public String getTIMEZONE();
	public String getClasificacionTipoBusqueda();
	public void setClasificacionTipoBusqueda(String clasificacionTipoBusqueda);
	public String getClasificacionSubtipoBusqueda();
	public void setClasificacionSubtipoBusqueda(String clasificacionSubtipoBusqueda);
	public String getClasificacionMateriaBusqueda();
	public void setClasificacionMateriaBusqueda(String clasificacionMateriaBusqueda);
	public List<SelectItem> getListClasificacionTipo();
	public void setListClasificacionTipo(List<SelectItem> listClasificacionTipo);
	public List<SelectItem> getListClasificacionSubtipo();
	public void setListClasificacionSubtipo(List<SelectItem> listClasificacionSubtipo);
	public List<SelectItem> getListClasificacionMateria();
	public void setListClasificacionMateria(List<SelectItem> listClasificacionMateria);
	public int getFlagMostrarClasificaciones();
	public void setFlagMostrarClasificaciones(int flagMostrarClasificaciones);
}
