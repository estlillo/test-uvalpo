package cl.exe.exedoc.reportes;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.xpath.XPathExpressionException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.Messages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.ArchivoDocumentoBinario;
import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Carta;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoBinario;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Memorandum;
import cl.exe.exedoc.entity.Oficio;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.Resolucion;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.TipoDocumentoExpediente;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.DatosUsuarioBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.repositorio.RepositorioLocal;
import cl.exe.exedoc.util.DocumentoExpedienteComparador;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.FormatoDocumentosList;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * @author
 */
@Stateful
@Name("reporte")
@Scope(ScopeType.SESSION)
public class Reporte implements ReporteInterface {
 
	private static final String NUMERO_DOCUMENTO = "numeroDocumento";
	private static final String FECHA_TERMINO_DOCUMENTO_BUSQUEDA = "fechaTerminoDocumentoBusqueda";
	private static final String FECHA_INICIO_DOCUMENTO_BUSQUEDA = "fechaInicioDocumentoBusqueda";
	private static final String INICIO = "inicio";
	private static final String TERMINO = "termino";
	private static final String INCLUIR_FECHA = "Debe incluir la fecha de {0}";
	private static final String MINIMO_RANGO_FECHAS = "Se debe incluir mínimo el rango de fechas";
	private static final String SELECCIONAR = "<<Seleccionar>>";
	private static final String _1 = "-1";
	private static final String ID = "id";
	private static final String MATERIA = "Materia";
	private static final String FECHA_DOCUMENTO = "Fecha del Documento";
	private static final String DE = "De";
	private static final String COL_NUMERO_DOCUMENTO = "Número de Documento";
	private static final String FECHA_INGRESO = "Fecha Ingreso";
	private static final String FWD_REPORTE_DOCUMENTOS_BANDEJA_ENTRADA = "reporteDocumentosBandejaEntrada";
	private static final String FWD_REPORTE_RUTA_SEGUIDA_DOCUMENTO = "reporteRutaSeguidaDocumento";
	private static final String FWD_REPORTE_HISTORIAL_DOCUMENTO = "reporteHistorialDocumento";
	private static final String FWD_REPORTE_DOCUMENTOS_RELACIONADOS = "reporteDocumentosRelacionados";
	private static final String FWD_REPORTE_HISTORIAL_BANDEJA_ENTRADA = "reporteHistorialBandejaEntrada";
	private static final String FWD_REPORTE_ESTADO_BANDEJA_SALIDA = "reporteEstadoBandejaSalida";
	private static final String FWD_REPORTE_DOCUMENTOS = "reporteDocumentos";
	private static final String FWD_REPORTE_DOCUMENTOS_OPARTES = "reporteDocumentosOficinaPartes";
	private static final String FWD_REPORTE_DOCUMENTOS_UO = "reporteDocumentosUnidadOrganizacional";
	private static final String FWD_REPORTE_BUSCAR_DOCUMENTOS = "buscarDocumentos";
	private static final String FWD_REPORTE_CIERRE_DOCUMENTOS = "cerrarDocumentos";
	private static final Locale locale = new Locale("es", "CL");
	private static final String STRING = "/";
	private static final String[] CAMPOS = { FECHA_INGRESO, "Numero de Expediente", "Tipo de Documento",
			COL_NUMERO_DOCUMENTO, DE, "A", FECHA_DOCUMENTO, MATERIA, };

	private static final String[] CAMPOS_OPARTES = { FECHA_INGRESO, DE, COL_NUMERO_DOCUMENTO, FECHA_DOCUMENTO, MATERIA, };

	private static final String TIMEZONE = "America/Cayenne";

	// private static final String _TIPO_DOCUMENTO_RESOLUCION = "6";

	//private static final String MATERIA_RESERVADA = "Materia Confidencial";
	private static final int LARGO_MAXIMO = 255;
	
	@Logger
	private Log logger;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> estadoBandejaSalida;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaEntrada> historialBandejaEntrada;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaEntrada> documentoEnBandejaEntrada;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> documentosRelacionados;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> historialDocumento;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> documentosEncontrados;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> rutaSeguidaDocumento;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> reporteDocumentos;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> documentosCierre;

	@In(required = false, scope = ScopeType.SESSION)
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;

	private boolean factura;

	/**
	 * Entity Manager.
	 */
	@PersistenceContext
	protected EntityManager em;

	@EJB
	private RepositorioLocal repositorio;

	@In(required = false)
	private Persona usuario;

	private String tipoDocumentoBusqueda;
	private String numeroDocumentoBusqueda;
	private Date fechaInicioDocumentoBusqueda;
	private Date fechaTerminoDocumentoBusqueda;
	private String emisorDocumentoBusqueda;
	private String materiaDocumentoBusqueda;
	private String nombreArchivoBusqueda;
	private String numeroExpedienteBusqueda;
	private String destinatarioDocumentoBusqueda;
	private Integer numeroOrdenDeCompraBusqueda;
	private String rutProveedorBusqueda;
	private Long organizacion = JerarquiasLocal.INICIO;
	private Long division = JerarquiasLocal.INICIO;
	private Long departamento = JerarquiasLocal.INICIO;
	private Long unidadOrganizacional = JerarquiasLocal.INICIO;
	private Long cargo = JerarquiasLocal.INICIO;

	private Long persona;

	private List<SelectItem> listCargos = new ArrayList<SelectItem>();
	private List<SelectItem> listUnidadesOrganizacionales = new ArrayList<SelectItem>();
	private List<SelectItem> listDepartamento = new ArrayList<SelectItem>();
	private List<SelectItem> listDivision = new ArrayList<SelectItem>();
	private List<SelectItem> listOrganizacion = new ArrayList<SelectItem>();
	private List<SelectItem> listPersonas = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionTipo = new ArrayList<SelectItem>();
	private List<SelectItem> listClasificacionSubtipo = new ArrayList<SelectItem>();

	private String clasificacionTipoBusqueda;

	private String clasificacionSubtipoBusqueda;

	private int flagMostrarClasificaciones;

	@Out(required = false, scope = ScopeType.SESSION)
	private List<SelectItem> unidadesOrganizacionales;

	private Long idEmisor = -1L;
	private Long idDestinatario = -1L;

	/**
	 * Constructor.
	 */
	public Reporte() {
		estadoBandejaSalida = new ArrayList<ExpedienteBandejaSalida>();
		historialBandejaEntrada = new ArrayList<ExpedienteBandejaEntrada>();
		documentoEnBandejaEntrada = new ArrayList<ExpedienteBandejaEntrada>();
		documentosRelacionados = new ArrayList<ExpedienteBandejaSalida>();
		historialDocumento = new ArrayList<ExpedienteBandejaSalida>();
		rutaSeguidaDocumento = new ArrayList<ExpedienteBandejaSalida>();
		reporteDocumentos = new ArrayList<ExpedienteBandejaSalida>();
		documentosEncontrados = new ArrayList<ExpedienteBandejaSalida>();
		listRegionesEmisor = new ArrayList<SelectItem>();
		listUnidadesOrganizacionalesEmisor = new ArrayList<SelectItem>();
		listRegionesDestinatario = new ArrayList<SelectItem>();
		listUnidadesOrganizacionalesDestinatario = new ArrayList<SelectItem>();
		listCargosEmisor = new ArrayList<SelectItem>();
		listPersonasEmisor = new ArrayList<SelectItem>();
		listCargosDestinatario = new ArrayList<SelectItem>();
		listPersonasDestinatario = new ArrayList<SelectItem>();
		documentosCierre = new ArrayList<ExpedienteBandejaSalida>();
	}

	@Override
	public void buscarPersonas() {
		listPersonas = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargo);
		if (listPersonas.size() == 1) {
			persona = (Long) listPersonas.get(0).getValue();
		}
	}

	@Override
	public boolean renderedVisualizarExpediente(final Long idDocumento) {
		boolean valido = false;
		final Documento doc = em.find(Documento.class, idDocumento);
		if (doc != null && doc.getFormatoDocumento() != null) {
			if ((doc.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL) && 
						doc.getCmsId() != null)|| 
					(doc.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO) &&
						(doc instanceof Carta ||
								doc instanceof Memorandum ||
								doc instanceof Oficio ||
								doc instanceof Resolucion))) {
				if (!doc.getReservado()) {
					valido = true;
				} else {
					for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
						if (destinatario.getDestinatarioPersona() != null) {
							if (destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
								valido = true;
								break;
							}
						}
					}
				}
			}
		}
		return valido;
	}

	@Override
	public boolean renderedVerExpediente(final Long idDocumento) {
		boolean valido = true;
		final Documento doc = em.find(Documento.class, idDocumento);

		if (doc.getReservado()) {
			valido = false;
			for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
				if (destinatario.getDestinatarioPersona() != null) {
					if (destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
						valido = true;
						break;
					}
				}
			}
		}
		return valido;
	}

	// public boolean renderedVisualizarSolicitud(Long idDocumento) {
	// boolean valido = true;
	// Documento doc = em.find(Documento.class, idDocumento);
	// if (doc != null && doc.getFormatoDocumento() != null) {
	// /*
	// * if
	// * (doc.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL
	// * )) { valido = false; }
	// */
	// if (!(doc instanceof Vacaciones)) {
	// valido = false;
	// }
	// }
	// return valido;
	// }

	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdExpediente(final String numeroExpediente) {
		Long idExpediente = null;
		final StringBuffer hql = new StringBuffer(10);
		hql.append("SELECT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = :numeroExpediente ");
		hql.append("order by e.id desc limit 1");
		final Query query = em.createQuery(hql.toString());
		query.setParameter("numeroExpediente", numeroExpediente);
		final List<Long> idExpedientes = query.getResultList();
		if (idExpedientes.size() >= 1) {
			idExpediente = idExpedientes.get(0);
		}
		return idExpediente;
	}

	@Override
	public void completaJerarquia() {
		this.buscarPersonas();
		cargo = JerarquiasLocal.INICIO;
		persona = JerarquiasLocal.INICIO;

		if (listOrganizacion == null) {
			listDivision = new ArrayList<SelectItem>();
			listDepartamento = new ArrayList<SelectItem>();
			listUnidadesOrganizacionales = new ArrayList<SelectItem>();
			listCargos = new ArrayList<SelectItem>();
			listPersonas = new ArrayList<SelectItem>();
		}
		this.cargarListas();
	}

	/**
	 * Matodo que carga las listas default.
	 */
	private void cargarListas() {
//		listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento = jerarquias.getDepartamentos(SELECCIONAR);
	}

	/**
	 * limpiar Destinatario.
	 */
	private void limpiarDestinatario() {
		organizacion = JerarquiasLocal.INICIO;
		division = JerarquiasLocal.INICIO;
		departamento = JerarquiasLocal.INICIO;
		unidadOrganizacional = JerarquiasLocal.INICIO;
		cargo = JerarquiasLocal.INICIO;
		persona = JerarquiasLocal.INICIO;
		listDivision.clear();
		listDepartamento.clear();
		listUnidadesOrganizacionales.clear();
		listCargos.clear();
		listPersonas.clear();
		listDivision.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listDepartamento.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * @return {@link List}
	 */
	@SuppressWarnings("unused")
	private List<SelectItem> buscarOrganizaciones() {
		this.limpiarDestinatario();
		return jerarquias.getOrganizaciones(JerarquiasLocal.TEXTO_INICIAL);
	}

	// @SuppressWarnings("unchecked")
	@Override
	public void buscarDivisiones() {
		final long idOrganizacion = organizacion;
		this.limpiarDestinatario();
		organizacion = idOrganizacion;
		/*
		 * Query query = em.createNamedQuery("Organizacion.findById"); query.setParameter("id", organizacion);
		 * List<Organizacion> organizaciones = query.getResultList(); if (organizaciones != null &&
		 * organizaciones.size() == 1) { Organizacion organizacion = organizaciones.get(0); }
		 */
		listDivision = jerarquias.getDivisiones(JerarquiasLocal.TEXTO_INICIAL, organizacion);
		listPersonas.clear();
		this.buscarDepartamentos();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarDepartamentos() {
		final Query query = em.createNamedQuery("Division.findById");
		query.setParameter(ID, division);
		final List<Division> divisiones = query.getResultList();
		boolean conCargo = false;
		if (divisiones != null && divisiones.size() == 1) {
			final Division div = divisiones.get(0);
			if (div.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDivision(this.division);
				this.buscarCargos();
				this.buscarUnidadesOrganizacionalesDepartamento();

			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listDepartamento = jerarquias.getDepartamentos(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		if (listUnidadesOrganizacionales.size() == 0) {
			listUnidadesOrganizacionales.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	/**
	 * buscar Unidades Organizacionales Departamento.
	 */
	public void buscarUnidadesOrganizacionalesDepartamento() {
		listUnidadesOrganizacionales.clear();
		listUnidadesOrganizacionales = jerarquias.getIdUnidadesDepartamento(JerarquiasLocal.TEXTO_INICIAL, division);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	// public void buscarClasificaciones() {
	// String tipoDocumento = this.tipoDocumentoBusqueda;
	// if (tipoDocumento != null && tipoDocumento.equals(_TIPO_DOCUMENTO_RESOLUCION)) {
	// listClasificacionTipo = jerarquias.getClasificacionesTipo(JerarquiasLocal.TEXTO_INICIAL);
	// listClasificacionSubtipo = jerarquias.getClasificacionesSubtipo(JerarquiasLocal.TEXTO_INICIAL);
	// flagMostrarClasificaciones = 1;
	// } else {
	// flagMostrarClasificaciones = 0;
	// }
	// }

	@SuppressWarnings("unchecked")
	@Override
	public void buscarUnidadesOrganizacionales() {
		final Query query = em.createNamedQuery("Departamento.findById");
		query.setParameter(ID, departamento);
		final List<Departamento> departamentos = query.getResultList();
		boolean conCargo = false;
		if (departamentos != null && departamentos.size() == 1) {
			final Departamento dpto = departamentos.get(0);
			if (dpto.getConCargo()) {
				conCargo = true;
				this.unidadOrganizacional = jerarquias.getIdUnidadVirtualDepartamento(this.division, this.departamento);
				this.buscarCargos();
			}
		}
		if (!conCargo) {
			listCargos.clear();
			listCargos.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
		}
		listUnidadesOrganizacionales = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				departamento);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@Override
	public void buscarCargos() {
		listCargos = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, unidadOrganizacional);
		listPersonas.clear();
		listPersonas.add(new SelectItem(JerarquiasLocal.INICIO, JerarquiasLocal.TEXTO_INICIAL));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setUnidadesOrganizacionales() {
		if (unidadesOrganizacionales == null) {
			final List<UnidadOrganizacional> listaPersonas = em.createQuery("SELECT uo FROM UnidadOrganizacional uo")
					.getResultList();
			unidadesOrganizacionales = new ArrayList<SelectItem>();
			unidadesOrganizacionales.add(new SelectItem(_1, SELECCIONAR));
			for (UnidadOrganizacional uo : listaPersonas) {
				unidadesOrganizacionales.add(new SelectItem(uo.getId(), uo.getDescripcion()));
			}
		}
	}

	@Factory(value = "documentoEnBandejaEntrada", scope = ScopeType.SESSION)
	@Override
	public String obtenerInformacionDeDocumentosEnBandejaDeEntrada() {
		logger.debug("[obtenerInformacionDeDocumentosEnBandejaDeEntrada] Iniciando método.");
		this.emisorDocumentoBusqueda = "";
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		documentoEnBandejaEntrada.clear();
		//this.completaJerarquia();
		//this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		//this.buscarUnidadesOrganizacionales();
		homeCrear = FWD_REPORTE_DOCUMENTOS_BANDEJA_ENTRADA;
		return FWD_REPORTE_DOCUMENTOS_BANDEJA_ENTRADA;		
	}

	@SuppressWarnings("unchecked")
	private List<Expediente> obtenerExpedientesAsociadosADocumento(final Documento documento) {
		logger.debug("[obtenerExpedientesAsociadosADocumento] Iniciando método con documento: " + documento);
		List<Expediente> expedientes = null;
		final StringBuilder jpQL = new StringBuilder();
		jpQL.append("SELECT e ");
		jpQL.append("FROM Documento d, ");
		jpQL.append("	DocumentoExpediente de, ");
		jpQL.append("	Expediente e ");
		jpQL.append("WHERE d = ? ");
		jpQL.append("	and d.id = de.documento.id ");
		jpQL.append("	and de.expediente.id = e.id ");
		jpQL.append("	and e.destinatario is null ");
		logger.debug("[obtenerExpedientesAsociadosADocumento] La consulta a ejecutar es: {0}", jpQL);
		final Query query = em.createQuery(jpQL.toString());
		query.setParameter(1, documento);
		expedientes = query.getResultList();
		logger.debug("[obtenerExpedientesAsociadosADocumento] El resultado de la consulta es: {0}", expedientes);
		return expedientes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void filtrarDocumentosEnBandejaEntrada() {
		//TODO sacar este parche
		nombreArchivoBusqueda = new String();
		tipoDocumentoBusqueda = JerarquiasLocal.INICIO.toString();
		persona = JerarquiasLocal.INICIO;
		//hasta aqui
		
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		if (numeroExpedienteBusqueda.isEmpty() && numeroDocumentoBusqueda.isEmpty() && persona.equals(-1)) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MINIMO_RANGO_FECHAS);
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				return;
			}
		}
		logger.info("[filtrarDocumentosEnBandejaEntrada] Iniciando metodo.");
		final long init = System.currentTimeMillis();
		documentoEnBandejaEntrada.clear();
		final StringBuilder sql = new StringBuilder();
		sql.append("select d.id as idDoc, td.descripcion as tipo, d.numeroDocumento, d.reservado, d.materia, ");
		sql.append("d.fechaDocumentoOrigen, d.fechaCreacion, d.emisor, fd.descripcion as formato, "); 
		sql.append("e.id as idExp, e.fechaIngreso, e.fechaAcuseRecibo, e.numeroExpediente, ");
		sql.append("(case when p is not null then p.nombres else p_dest.nombres end) as nombres, "); 
		sql.append("(case when p is not null then p.apellidoPaterno else p_dest.apellidoPaterno end) as apellido, "); 
		sql.append("(case when p is not null then c_e.descripcion else c.descripcion end) as cargo, "); 
		sql.append("(case when p is not null then u_e.descripcion else u.descripcion end) as unidad ");
		sql.append("from Expediente e "); 
		sql.append("inner join e.documentos de "); 
		sql.append("inner join de.documento d ");
		sql.append("inner join d.tipoDocumento td ");
		sql.append("inner join d.formatoDocumento fd ");
		sql.append("left join e.destinatarioHistorico p ");
		sql.append("left join p.cargo c_e ");
		sql.append("left join c_e.unidadOrganizacional u_e "); 
		sql.append("left join u_e.departamento dep_e ");
		sql.append("left join dep_e.division div_e ");
		sql.append("left join div_e.organizacion o_e ");
		sql.append("left join e.destinatario p_dest ");
		sql.append("left join p_dest.cargo c ");
		sql.append("left join c.unidadOrganizacional u ");  
		sql.append("left join u.departamento dep ");
		sql.append("left join dep.division div ");
		sql.append("left join div.organizacion o ");
		sql.append("where e.archivado is null ");
		sql.append("and e.reasignado is null ");
		sql.append("and e.cancelado is null ");
		// sql.append(" and ( ");
		// sql.append(tipoDocumentoBusqueda);
		// sql.append(" = -1 or td.id = ");
		// sql.append(tipoDocumentoBusqueda).append(") ");
		sql.append(MessageFormat.format(" and ( {0} = -1 or td.id = {1} ) ", tipoDocumentoBusqueda,
				tipoDocumentoBusqueda));
		if (!numeroDocumentoBusqueda.isEmpty()) {
			sql.append(" and d.numeroDocumento like :numeroDocumento ");
		}
		if (!"".equals(emisorDocumentoBusqueda)) {
			// sql.append(" and upper(d.emisor) like upper('%");
			// sql.append(emisorDocumentoBusqueda);
			// sql.append("%')");
			sql.append(MessageFormat.format(" and upper(d.emisor) like upper(''%{0}%'') ", emisorDocumentoBusqueda));
		}
		if (materiaDocumentoBusqueda.length() > 0) {
			// sql.append(" and upper(d.materia) like upper('%");
			// sql.append(materiaDocumentoBusqueda);
			// sql.append("%')");
			sql.append(MessageFormat.format(" and upper(d.materia) like upper(''%{0}%'') ", materiaDocumentoBusqueda));
		}
		if (this.getNumeroExpedienteBusqueda().length() > 0) {
			// sql.append(" and e.numeroExpediente = '").append(numeroExpedienteBusqueda).append("' ");
			sql.append(MessageFormat.format(" and e.numeroExpediente = ''{0}'' ", numeroExpedienteBusqueda));
		}
//		if (nombreArchivoBusqueda != null && nombreArchivoBusqueda.length() > 0) {
//			// sql.append(" and upper(a.nombreArchivo) like upper('%");
//			// sql.append(nombreArchivoBusqueda);
//			// sql.append("%')");
//			sql.append(MessageFormat
//					.format(" and upper(a.nombreArchivo) like upper(''%{0}%'') ", nombreArchivoBusqueda));
//		}

//		if (flagMostrarClasificaciones == 1) {
//			if (clasificacionTipoBusqueda != null && !clasificacionTipoBusqueda.trim().equals(_1)) {
//				// sql.append(" and d.clasificacionTipo.id = ");
//				// sql.append(Integer.parseInt(clasificacionTipoBusqueda));
//				// sql.append(" ");
//				sql.append(MessageFormat.format(" and d.clasificacionTipo.id = {0} ",
//						Integer.parseInt(clasificacionTipoBusqueda)));
//			}
//			if (clasificacionSubtipoBusqueda != null && !clasificacionSubtipoBusqueda.trim().equals(_1)) {
//				// sql.append(" and d.clasificacionSubtipo.id = ");
//				// sql.append(Integer.parseInt(clasificacionSubtipoBusqueda));
//				// sql.append(" ");
//				sql.append(MessageFormat.format(" and d.clasificacionSubtipo.id = {0} ",
//						Integer.parseInt(clasificacionSubtipoBusqueda)));
//			}
//		}

		
		sql.append(" and ((e.destinatario is not null ");
		sql.append(" and e.fechaDespacho is not null ");
		sql.append(" and e.fechaAcuseRecibo is null ");

//		if (persona != null && persona != -1) {
//			sql.append(" and e.destinatario.id = " + persona);
//		} else {
//			if (cargo != null && cargo != -1) {
//				sql.append(" and c.id= " + cargo);
//			} else {
//				if (unidadOrganizacional != null && unidadOrganizacional != -1) {
//					sql.append(" and u.id= " + unidadOrganizacional);
//				} else {
//					if (departamento != null && departamento != -1) {
//						sql.append(" and dep.id= " + departamento);
//					} else {
//						if (division != null && division != -1) {
//							sql.append(" and div.id = " + division);
//						}
//					}
//				}
//			}
//		}
		// sql.append(")");
		sql.append(") OR (e.destinatarioHistorico is not null ");
		sql.append(" and e.fechaDespacho is null ");
		sql.append(" and e.fechaAcuseRecibo is not null ");

//		if (persona != null && persona != -1) {
//			sql.append(" and e.emisor.id =" + persona);
//		} else {
//			if (cargo != null && cargo != -1) {
//				sql.append(" and c_e.id= " + cargo);
//			} else {
//				if (unidadOrganizacional != null && unidadOrganizacional != -1) {
//					sql.append(" and u_e.id= " + unidadOrganizacional);
//				} else {
//					if (departamento != null && departamento != -1) {
//						sql.append(" and dep_e.id= " + departamento);
//					} else {
//						if (division != null && division != -1) {
//							sql.append(" and div_e.id= " + division);
//						}
//					}
//				}
//			}
//		}

		sql.append("))");
		// if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
		// sql.append(" and TO_DATE(TO_CHAR(d.fechaCreacion,'dd-MM-yyyy'),'dd-MM-yyyy') between TO_DATE('");
		// sql.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda));
		// sql.append("','dd-MM-yyyy') and TO_DATE('");
		// sql.append(simpleDateFormat.format(fechaTerminoDocumentoBusqueda));
		// sql.append("','dd-MM-yyyy')");
		// }

		// Condiciones para la fecha de busqueda, opcionalidad de las dos fechas
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {

			sql.append(" and d.fechaCreacion between :fechaInicioDocumentoBusqueda ");
			sql.append(" and :fechaTerminoDocumentoBusqueda");

		}

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {

			sql.append(" and d.fechaCreacion >= :fechaInicioDocumentoBusqueda ");

		}

		if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {

			sql.append(" and d.fechaCreacion <= :fechaTerminoDocumentoBusqueda ");

		}

		sql.append(" order by d.fechaCreacion, d.id, e.fechaIngreso ");

		logger.info("[filtrarDocumentosEnBandejaEntrada] SQL a ejecutar: " + sql);

		final Query query = em.createQuery(sql.toString());

		// Llenando las fechas desde y hasta para buscar por fecha de creacion.
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {

			// Se pasa la fecha de inicio como el inicio del dia
			// y la fecha fin como el fin del dia (23:59:60)
			fechaInicioDocumentoBusqueda = FechaUtil.inicio(fechaInicioDocumentoBusqueda);
			fechaTerminoDocumentoBusqueda = FechaUtil.fin(fechaTerminoDocumentoBusqueda);

			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda, TemporalType.TIMESTAMP);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda, TemporalType.TIMESTAMP);

		}

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {

			// fecha fin como el fin del dia (23:59:60)
			fechaInicioDocumentoBusqueda = FechaUtil.inicio(fechaInicioDocumentoBusqueda);
			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda, TemporalType.TIMESTAMP);

		}

		if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {

			// fecha fin como el fin del dia (23:59:60)
			fechaTerminoDocumentoBusqueda = FechaUtil.fin(fechaTerminoDocumentoBusqueda);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda, TemporalType.TIMESTAMP);

		}

		// Fin filtro de busqueda por fecha

		if (!numeroDocumentoBusqueda.isEmpty()) {
			query.setParameter(NUMERO_DOCUMENTO, numeroDocumentoBusqueda + "%");
		}
		final List<Object[]> arrayObject = query.getResultList();

		logger.info("[Cantidad resgitros obtenidos BD]: {0} ", arrayObject.size());
		logger.info("[Tiempo en ejecutar consulta]: {0} ", System.currentTimeMillis() - init);

		documentoEnBandejaEntrada = new ArrayList<ExpedienteBandejaEntrada>();
		for (Object[] o : arrayObject) {
			Expediente exp = new Expediente();
			Persona destinatario = new Persona();
			Cargo cargoDestinatario = new Cargo();
			UnidadOrganizacional unidadDestinatario = new UnidadOrganizacional();
			//d.id as idDoc
			Long idDoc = (Long)o[0];
			//td.descripcion as tipo
			String tipoDocumento = o[1] != null ? (String) o[1] : "";
			//d.numeroDocumento
			String numeroDocumento = o[2] != null ? (String) o[2] : "";
			//d.reservado
			Boolean reservado = o[3] != null ? (Boolean) o[3] : false;
			//d.materia
			String materia = reservado ? Messages.instance().get("materia_reservada") : (o[4] != null ? (String) o[4] : "");
			//d.fechaDocumentoOrigen
			Date fechaDocOrigen = (Date) o[5];
			//d.fechaCreacion
			Date fechaCreacion = (Date) o[6];
			//d.emisor
			String emisor = o[7] != null ? (String) o[7] : "";
			//fd.descripcion as formato
			String formato = o[8] != null ? (String) o[8] : "";
			//e.id as idExp
			Long idExp = (Long)o[9];
			exp.setId(idExp);
			//e.fechaIngreso
			Date fechaIngreso = (Date)o[10];
			exp.setFechaIngreso(fechaIngreso);
			//e.fechaAcuseRecibo
			Date fechaAcuseRecibo = (Date)o[11];
			exp.setFechaAcuseRecibo(fechaAcuseRecibo);
			//e.numeroExpediente
			String numeroExpediente = o[12] != null ? (String)o[12] : "";
			exp.setNumeroExpediente(numeroExpediente);
			//(case when p is not null then p.nombres else p_dest.nombres end) as nombres
			String nombre = o[13] != null ? (String)o[13] : "";
			destinatario.setNombres(nombre);
			//(case when p is not null then p.apellidoPaterno else p_dest.apellidoPaterno end) as apellido
			String apellido = o[14] != null ? (String)o[14] : "";
			destinatario.setApellidoPaterno(apellido);
			//(case when p is not null then c_e.descripcion else c.descripcion end) as cargo
			String cargo = o[15] != null ? (String)o[15] : "";
			cargoDestinatario.setDescripcion(cargo);
			//(case when p is not null then u_e.descripcion else u.descripcion end) as unidad  
			String unidad = o[16] != null ? (String)o[16] : "";
			unidadDestinatario.setDescripcion(unidad);
			cargoDestinatario.setUnidadOrganizacional(unidadDestinatario);
			destinatario.setCargo(cargoDestinatario);
			exp.setDestinatario(destinatario);
			
			int indice = indiceDocEnBandejaDeEntrada(idDoc, numeroExpediente, documentoEnBandejaEntrada);
			if (indice != -1) {
				documentoEnBandejaEntrada.get(indice).getExpedientesAsociadosADocumento().add(exp);
			} else {
				ExpedienteBandejaEntrada bandejaEntrada = new ExpedienteBandejaEntrada();
				bandejaEntrada.setId(idDoc);
				bandejaEntrada.setNumeroExpediente(numeroExpediente);
				bandejaEntrada.setIdDocumento(idDoc);
				bandejaEntrada.setTipoDocumento(tipoDocumento);
				bandejaEntrada.setNumeroDocumento(numeroDocumento);
				bandejaEntrada.setMateria(materia);
				bandejaEntrada.setFechaDocumentoOrigen(fechaDocOrigen);
				bandejaEntrada.setFechaIngreso(fechaCreacion);
				bandejaEntrada.setEmisor(emisor);
				bandejaEntrada.setFormatoDocumento(formato);
				bandejaEntrada.setReservado(reservado);
				bandejaEntrada.setExpedientesAsociadosADocumento(new ArrayList<Expediente>());
				bandejaEntrada.getExpedientesAsociadosADocumento().add(exp);
				documentoEnBandejaEntrada.add(bandejaEntrada);
			}
		}
		
		logger.info("[Cantidad resgitros a desplegar]: {0} ", documentoEnBandejaEntrada.size());
		logger.info("[Tiempo total]: {0}", System.currentTimeMillis() - init);
		this.cargarListas();
	}

	/**
	 * Metodo que obtiene un Expediente.
	 * 
	 * @param exp {@link Expediente}
	 * @return {@link Expediente}
	 */
	private Expediente obtieneExpediente(final Expediente exp) {
		Expediente nuevoExpediente = null;
		if (exp.getFechaAcuseRecibo() != null) {
			nuevoExpediente = new Expediente();
			nuevoExpediente.setDestinatario(exp.getEmisor());
			nuevoExpediente.setFechaDespacho(exp.getFechaDespachoHistorico());
			nuevoExpediente.setFechaAcuseRecibo(exp.getFechaAcuseRecibo());
		} else {
			nuevoExpediente = exp;
			nuevoExpediente.setDestinatario(exp.getDestinatario());
		}
		return nuevoExpediente;
	}

	/**
	 * indice de Documento En Bandeja De Entrada.
	 * 
	 * @param idDoc {@link Long}
	 * @param documentoEnBandeja List<? extends ExpedienteBandeja>
	 * @return {@link Integer}
	 */
	private int indiceDocEnBandejaDeEntrada(final Long idDoc, final List<? extends ExpedienteBandeja> documentoEnBandeja) {
		for (int i = 0; i < documentoEnBandeja.size(); i++) {
			if (documentoEnBandeja.get(i).getId().equals(idDoc)) { return i; }
		}
		return -1;
	}
	
	//Usar para cuando haya fusion de expedientes, el doc cambia de n expeddiente
	private int indiceDocEnBandejaDeEntrada(final Long idDoc, final String numeroExpediente, final List<? extends ExpedienteBandeja> documentoEnBandeja) {
		for (int i = 0; i < documentoEnBandeja.size(); i++) {
			if (documentoEnBandeja.get(i).getId().equals(idDoc) &&
					documentoEnBandeja.get(i).getNumeroExpediente().equals(numeroExpediente)) {
				return i;
			}
		}
		return -1;
	}

	@Factory(value = "rutaSeguidaDocumento", scope = ScopeType.SESSION)
	@Override
	public String obtenerInformacionDeRutaSeguidaPorUnDocumento() {
		logger.debug("[obtenerInformacionDeRutaSeguidaPorUnDocumento] Iniciando método");
		this.emisorDocumentoBusqueda = "";
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		rutaSeguidaDocumento.clear();
		tipoDocumentoBusqueda = null;
		//this.completaJerarquia();
		//this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		//this.buscarUnidadesOrganizacionales();
		homeCrear = FWD_REPORTE_RUTA_SEGUIDA_DOCUMENTO;
		return FWD_REPORTE_RUTA_SEGUIDA_DOCUMENTO;
	}

	@Override
	public void filtrarRutaDeDocumentos() {
		logger.info("[filtrarRutaDeDocumentos] Iniciando metodo. ");
		
		if (numeroExpedienteBusqueda.isEmpty() && numeroDocumentoBusqueda.isEmpty()) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add("Se debe incluir N° Expediente o Fechas de Inicio-Término al menos.");
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				return;
			}
		}
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
			return;
		} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
			return;
		}
		
		final long init = System.currentTimeMillis();
		rutaSeguidaDocumento.clear();
		
		final List<Object[]> arrayObject = this.filtrarRutaDeDocumentosDestinatario(this.sqlRutaSeguidaPorDocumentoDestinatario());
		arrayObject.addAll(this.filtrarRutaDeDocumentosDestinatario(this.sqlRutaSeguidaPorDocumentoDestinatarioHistorico()));
		
		logger.info("[Cantidad resgitros obtenidos BD]: {0} ", arrayObject.size());
		logger.info("[Tiempo en ejecutar consulta]: {0}", System.currentTimeMillis() - init);
		this.procesaRutaSeguidaPorDocumento(arrayObject);
		if(arrayObject.size()<=0){
			FacesMessages.instance().add("No se encontraron resultados para los parámetros de búsqueda");
		}

	}

	/**
	 * @param sqlBaseRutaSeguidaPorDocumento
	 * @return
	 */
	public String sqlBasefiltrarRutaDeDocumentosII(final String sqlBaseRutaSeguidaPorDocumento) {
		logger.info("[filtrarRutaDeDocumentos] Iniciando metodo");
		final StringBuilder sql = new StringBuilder(sqlBaseRutaSeguidaPorDocumento);
		sql.append("WHERE dep.documento = de.documento ");
		// TODO NEW LINE
		sql.append("and not (e.fechaDespacho is null and e.fechaDespachoHistorico is null) ");
		sql.append("and (e.cancelado is null) ");
		sql.append("and ((");
		sql.append(tipoDocumentoBusqueda);
		sql.append(" = -1 or d.tipoDocumento.id = ");
		sql.append(tipoDocumentoBusqueda);
		sql.append(") ");
		if (!numeroDocumentoBusqueda.isEmpty()) {
			sql.append(" and  ");
			sql.append("d.numeroDocumento like :numeroDocumento");
			// '" + numeroDocumentoBusqueda + "%'");
		}
		sql.append(" and ('");
		sql.append(emisorDocumentoBusqueda.equals("") ? _1 : emisorDocumentoBusqueda);
		sql.append("' = '-1' or upper(d.emisor) like upper('%");
		sql.append(emisorDocumentoBusqueda.equals("") ? _1 : emisorDocumentoBusqueda);
		sql.append("%')) and e.versionPadre.id is not null ");

		if (materiaDocumentoBusqueda.length() > 0) {
			sql.append(" and ( upper(d.materia) like upper('%");
			sql.append(materiaDocumentoBusqueda.equals("") ? _1 : materiaDocumentoBusqueda);
			sql.append("%'))");
		}
		if (numeroExpedienteBusqueda.length() > 0) {
			sql.append(" and upper(e.numeroExpediente) = upper('");
			sql.append(numeroExpedienteBusqueda.equals("") ? _1 : numeroExpedienteBusqueda);
			sql.append("')");
		}
//		if (nombreArchivoBusqueda != null && nombreArchivoBusqueda.length() > 0) {
//			sql.append(" and upper(a.nombreArchivo) like upper('%");
//			sql.append(nombreArchivoBusqueda.equals("") ? _1 : nombreArchivoBusqueda);
//			sql.append("%')");
//		}
//		if (flagMostrarClasificaciones == 1) {
//			if (clasificacionTipoBusqueda != null && !clasificacionTipoBusqueda.trim().equals(_1)) {
//				sql.append(" and d.clasificacionTipo.id = ");
//				sql.append(Integer.parseInt(clasificacionTipoBusqueda));
//				sql.append(" ");
//			}
//			if (clasificacionSubtipoBusqueda != null && !clasificacionSubtipoBusqueda.trim().equals(_1)) {
//				sql.append(" and d.clasificacionSubtipo.id = ");
//				sql.append(Integer.parseInt(clasificacionSubtipoBusqueda));
//				sql.append(" ");
//			}
//		}
//		if (this.factura) {
//			if (this.rutProveedorBusqueda != null && !this.rutProveedorBusqueda.trim().equals("")) {
//				sql.append(" and upper(d.rutProveedorFactura) like upper('");
//				sql.append(this.rutProveedorBusqueda);
//				sql.append("%')");
//			}
//			if (this.numeroOrdenDeCompraBusqueda != null) {
//				sql.append(" and d.numeroOrdenCompraFactura = '");
//				sql.append(this.numeroOrdenDeCompraBusqueda);
//				sql.append("'");
//			}
//		}

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			sql.append(" and d.fechaCreacion between :fechaInicioDocumentoBusqueda ");
			sql.append(" and :fechaTerminoDocumentoBusqueda");

		}
		
//		if (departamento != null && departamento > 0) {
//			sql.append(" and p.cargo.unidadOrganizacional.departamento.id = " + departamento + " ");
//		}

		sql.append(")");
		sql.append(" order by d.fechaCreacion, d.id, e.fechaIngreso ");
		logger.info("[filtrarRutaDeDocumentos] SQL a ejecutar: {0} ", sql);

		return sql.toString();

	}
	
	// TODO IMPLEMENT!!!
	private String sqlBaseFiltrarRutaDeDocumentos(final String sqlBaseRutaSeguidaPorDocumento) {
		final StringBuilder sql = new StringBuilder(sqlBaseRutaSeguidaPorDocumento);
		sql.append(" where dep.documento = de.documento ");
		sql.append(" and not (e.fechaDespacho is null and e.fechaDespachoHistorico is null) ");
		sql.append(" and e.cancelado is null ");
		//sql.append(" and e.versionPadre.id is not null "); //no necesario
		if (!((Long)Long.parseLong(tipoDocumentoBusqueda)).equals(JerarquiasLocal.INICIO)) {
			sql.append(" and d.tipoDocumento.id = :tipoDocumento ");
		}
		if (!numeroDocumentoBusqueda.trim().isEmpty()) {
			sql.append(" and upper(d.numeroDocumento) like upper(:numeroDocumento) ");
		}
		if (!emisorDocumentoBusqueda.trim().isEmpty()) {
			sql.append(" and upper(d.emisor) like upper(:emisor) ");
		}
		if (!materiaDocumentoBusqueda.trim().isEmpty()) {
			sql.append(" and upper(d.materia) like upper(:materia) ");
		}
		if (!numeroExpedienteBusqueda.trim().isEmpty()) {
			sql.append(" and upper(e.numeroExpediente) like upper(:numeroExpediente) ");
		}
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			sql.append(" and d.fechaCreacion between :fechaInicioDocumentoBusqueda ");
			sql.append(" and :fechaTerminoDocumentoBusqueda ");
		}
		return sql.toString();
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> filtrarRutaDeDocumentosDestinatario(String sqlBaseRutaSeguidaPorDocumento) {
		logger.info("[filtrarRutaDeDocumentos] Iniciando metodo");
		long init = System.currentTimeMillis();
		rutaSeguidaDocumento.clear();
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);

		String sql = sqlBasefiltrarRutaDeDocumentosII(sqlBaseRutaSeguidaPorDocumento);

		logger.info("[filtrarRutaDeDocumentos] SQL a ejecutar: " + sql);
		final Query query = em.createQuery(sql);
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, FechaUtil.inicio(fechaInicioDocumentoBusqueda), TemporalType.TIMESTAMP);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, FechaUtil.fin(fechaTerminoDocumentoBusqueda), TemporalType.TIMESTAMP);

		}
		if (!numeroDocumentoBusqueda.isEmpty()) {
			query.setParameter(NUMERO_DOCUMENTO, numeroDocumentoBusqueda + "%");
		}
		List<Object[]> arrayObject = query.getResultList();
		logger.info("[Cantidad resgitros obtenidos BD]: " + arrayObject.size());
		logger.info("[Tiempo en ejecutar consulta]: " + (System.currentTimeMillis() - init));
		return arrayObject;
	}

	private void procesaRutaSeguidaPorDocumento(List<Object[]> arrayObject) {
		long init = System.currentTimeMillis();
		ExpedienteBandejaSalida bandejaSalida = null;
		List<DatosUsuarioBandejaSalida> usuarioAsociadosARuta = null;

		Expediente exp = null;
		for (Object[] o : arrayObject) {

			Long idDoc = (((Long) o[0]) != null) ? ((Long) o[0]).longValue() : null;
			// Long idDoc = (((BigInteger) o[0]) != null) ? ((BigInteger) o[0]).longValue() : null;
			Date fechaAcuseRecibo = (Date) o[2];
			//Boolean acuseRecibo = (Boolean) o[2];
			Boolean reservado = false;
			if (o[1] != null) {
				reservado = (Boolean)o[1];
			}
			Date fechaIngreso = (Date) o[3];
			Date fechaDespacho = (Date) o[4];
			Date fechaDespachoHistorico = (Date) o[5];
			Long idDest = (((Long) o[6]) != null) ? ((Long) o[6]).longValue() : null;
			Long idDestiHist = (((Long) o[7]) != null) ? ((Long) o[7]).longValue() : null;
			// Long idDest = (((BigInteger) o[6]) != null) ? ((BigInteger) o[6]).longValue() : null;
			// Long idDestiHist = (((BigInteger) o[7]) != null) ? ((BigInteger) o[7]).longValue() : null;
			String descTipoDocumento = (String) o[8];
			String numDoc = (String) o[9];
			String emisor = (String) o[10];
			Date fechaDocOrigen = (Date) o[11];
			String materia = reservado ? Messages.instance().get("materia_reservada") : (o[12] != null ? (String) o[12] :"");
			String descFd = (String) o[13];
			Object cmsId = o[14];
			String numeroExpediente = (String) o[15];
			String nombresAutor = ((String) o[16] != null) ? (String) o[16] : "";
			String apellidoPaternoAutor = ((String) o[17] != null) ? (String) o[17] : "";
			String apellidoMaternoAutor = ((String) o[18] != null) ? (String) o[18] : "";

			String nombreDestinatario = (String) o[19];
			String apellidoPaternoDestinatario = (String) o[20];
			String apellidoMaternoDestinatario = (String) o[21];
			String descCargo = (String) o[22];
			String descUnidad = (String) o[23];

			Date archivado = (Date) o[24];
			String nombreArchivo = (String) o[25];
			String rutProveedorFactura = (String) o[26];
			String numeroOrdenCompra = (((String) o[27]) != null) ? (((String) o[27])) : "";
			Date fechaCreacionDocumento = (Date) o[28];
			Long idPadre = (((Long) o[29]) != null) ? ((Long) o[29]).longValue() : null;
			// Long idPadre = (((BigInteger) o[29]) != null) ? ((BigInteger) o[29]).longValue() : null;

			String nombreEmisor = ((String) o[30] == null ? "" : (String) o[30] + " ")
					+ ((String) o[31] == null ? "" : " " + (String) o[31]);
			String nombreEmisorHistorico = ((String) o[32] == null ? "" : (String) o[32] + " ")
					+ ((String) o[33] == null ? "" : " " + (String) o[33]);

			Long idExp = (((Long) o[34]) != null) ? ((Long) o[34]).longValue() : null;
			// Long idExp = (((BigInteger) o[34]) != null) ? ((BigInteger) o[34]).longValue() : null;

			Date fechaReasignado = o[35] != null ? (Date)o[35] : null;
			Date fechaArchivadoHistorico = o[36] != null ? (Date)o[36] : null;
			Expediente expPadre = new Expediente();
			expPadre.setId(idPadre);
			exp = new Expediente();
			exp.setId(idExp);
			exp.setFechaAcuseRecibo(fechaAcuseRecibo);
			exp.setFechaIngreso(fechaIngreso);
			exp.setFechaDespacho(fechaDespacho);
			exp.setFechaDespachoHistorico(fechaDespachoHistorico);
			exp.setArchivado(archivado);
			exp.setVersionPadre(expPadre);
			exp.setReasignado(fechaReasignado);
			exp.setArchivadoHistorico(fechaArchivadoHistorico);

			Persona dest = new Persona();
			dest.setId((idDest != null) ? idDest : idDestiHist);
			dest.setNombres(nombreDestinatario);
			dest.setApellidoPaterno(apellidoPaternoDestinatario);
			dest.setApellidoMaterno(apellidoMaternoDestinatario);

			Cargo cargo = new Cargo();
			UnidadOrganizacional unidad = new UnidadOrganizacional();
			unidad.setDescripcion(descUnidad);
			cargo.setDescripcion(descCargo);
			dest.setCargo(cargo);
			cargo.setUnidadOrganizacional(unidad);
			exp.setDestinatario(dest);

			int indice = indiceDocEnBandejaDeEntrada(idDoc, numeroExpediente, rutaSeguidaDocumento);
			if (indice != -1) {
				List<DatosUsuarioBandejaSalida> listDatosBandejaSalida = rutaSeguidaDocumento.get(indice)
						.getDatosUsuariosAsociados();
				DatosUsuarioBandejaSalida datosUsuarioBandejaSalida = buscarDatosUsuario(exp);
				//TODO NEW LINE
				datosUsuarioBandejaSalida.setIdExpediente(exp.getVersionPadre().getId());
				
				if (!nombreEmisorHistorico.equals("")) {
					datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisorHistorico);
				} else {
					datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisor);
				}

				/*
				 * if (datosUsuarioBandejaSalida.getFechaDespacho() == null) {
				 * datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisor); } else {
				 * datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisorHistorico ); }
				 */
				listDatosBandejaSalida.add(datosUsuarioBandejaSalida);

				rutaSeguidaDocumento.get(indice).setDatosUsuariosAsociados(listDatosBandejaSalida);

			} else {
				bandejaSalida = new ExpedienteBandejaSalida();
				usuarioAsociadosARuta = new ArrayList<DatosUsuarioBandejaSalida>();
				bandejaSalida.setTipoDocumento(descTipoDocumento);
				bandejaSalida.setNumeroDocumento(numDoc);
				bandejaSalida.setId(idDoc);
				bandejaSalida.setEmisor(emisor);
				bandejaSalida.setFechaDocumentoOrigen(fechaDocOrigen);
				bandejaSalida.setAutor(nombresAutor + " " + apellidoPaternoAutor + " " + apellidoMaternoAutor);
				bandejaSalida.setMateria(materia);
				bandejaSalida.setNumeroOrdenCompra(numeroOrdenCompra);
				bandejaSalida.setRutProveedorFactura(rutProveedorFactura);
				bandejaSalida.setNombreArchivo(nombreArchivo);
				bandejaSalida.setFechaCreacionDocumento(fechaCreacionDocumento);

				DatosUsuarioBandejaSalida datosUsuarioBandejaSalida = buscarDatosUsuario(exp);
				// TODO NEW LINE
				datosUsuarioBandejaSalida.setIdExpediente(exp.getVersionPadre().getId());

				if (!nombreEmisorHistorico.equals("")) {
					datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisorHistorico);
				} else {
					datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisor);
				}
				/*
				 * if (datosUsuarioBandejaSalida.getFechaDespacho() == null) {
				 * datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisor); } else {
				 * datosUsuarioBandejaSalida.setNombreEmisor(nombreEmisorHistorico ); }
				 */
				usuarioAsociadosARuta.add(datosUsuarioBandejaSalida);
				bandejaSalida.setDatosUsuariosAsociados(usuarioAsociadosARuta);
				bandejaSalida.setFormatoDocumento(descFd);
				bandejaSalida.setIdDocumento(idDoc);
				bandejaSalida.setTieneCmsId(cmsId != null);
				bandejaSalida.setNumeroExpediente(numeroExpediente);
				rutaSeguidaDocumento.add(bandejaSalida);
			}
		}
		// THIS IS AWFUL!!!!
		Collections.sort(rutaSeguidaDocumento, new Comparator<ExpedienteBandejaSalida>(){
			public int compare(ExpedienteBandejaSalida e1, ExpedienteBandejaSalida e2) {
				return e1.getFechaCreacionDocumento().compareTo(e2.getFechaCreacionDocumento());
			}
		});
		for (ExpedienteBandejaSalida e : rutaSeguidaDocumento) {
			Collections.sort(e.getDatosUsuariosAsociados(), new Comparator<DatosUsuarioBandejaSalida>() {
				public int compare(final DatosUsuarioBandejaSalida o1, final DatosUsuarioBandejaSalida o2) {
					return o1.getFechaIngresoBandeja().compareTo(o2.getFechaIngresoBandeja());
				}
			});
		}
		logger.info("[Cantidad resgitros a desplegar]: " + rutaSeguidaDocumento.size());
		logger.info("[Tiempo total procesar consulta]: " + (System.currentTimeMillis() - init));
	}

	private DatosUsuarioBandejaSalida buscarDatosUsuario(Expediente expediente) {
		DatosUsuarioBandejaSalida datosUsuario = new DatosUsuarioBandejaSalida();

		datosUsuario.setIdExpediente(expediente.getId());
		datosUsuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
		datosUsuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
		if (expediente.getFechaAcuseRecibo() != null) {
			datosUsuario.setFechaDespacho(expediente.getFechaDespacho());
		}
		
//		if (datosUsuario.getFechaAcuseRecibo() != null && !expediente.getAcuseRecibo()) {
//			if (expediente.getFechaDespacho() != null) {
//				datosUsuario.setFechaDespacho(expediente.getFechaDespacho());
//			} else {
//				datosUsuario.setFechaDespacho(expediente.getFechaIngreso());
//			}
//		} else {
//			datosUsuario.setFechaDespacho(null);
//		}
//		if (expediente.getFechaDespacho() != null) {
//			datosUsuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
//		} else {
//			datosUsuario.setFechaIngresoBandeja(expediente.getFechaDespachoHistorico());
//		}

		Persona destinatario = expediente.getDestinatario();
		if (destinatario != null) {
			if (destinatario.getCargo() != null) datosUsuario.setDestinatarioCargo(destinatario.getCargo()
					.getDescripcion());
			if (destinatario.getCargo().getUnidadOrganizacional() != null) datosUsuario
					.setDestinatarioUnidadOrganizacional(destinatario.getCargo().getUnidadOrganizacional()
							.getDescripcion());
			datosUsuario.setDestinatarioUsuario(destinatario.getNombreApellido());
		}

		datosUsuario.setFechaArchivado(expediente.getArchivado());
		
		if (expediente.getReasignado() != null) {
			datosUsuario.setFechaReasignado(expediente.getReasignado());
			if (expediente.getArchivadoHistorico() != null) {
				datosUsuario.setFechaArchivado(expediente.getArchivadoHistorico());
			}
		}
		
		return datosUsuario;
	}

	@Factory(value = "historialDocumento", scope = ScopeType.SESSION)
	public String obtenerHistorialDeUnDocumento() {
		logger.info("[obtenerHistorialDeUnDocumento] Iniciando método");
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		historialDocumento.clear();

		return FWD_REPORTE_HISTORIAL_DOCUMENTO;
	}

	@SuppressWarnings("unchecked")
	public void filtrarHistorialDocumento() {
		logger.info("[filtrarHistorialDocumento] Iniciando método.");
		historialDocumento.clear();
		long init = System.currentTimeMillis();
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);

		if (numeroDocumentoBusqueda.isEmpty()) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MINIMO_RANGO_FECHAS);
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				return;
			}
		}

		StringBuilder sql = new StringBuilder(
				"SELECT d.id, d.numeroDocumento, td.descripcion as desc_tipo_doc, d.fechaDocumentoOrigen, ");
		sql.append("d.emisor, p.nombres, p.apellidoPaterno, p.apellidoMaterno, ");
		sql.append("d.materia, d.cmsId, fd.descripcion as desc_fd "); // ,
																		// de.id_expediente
																		// as
																		// id_expediente,
																		// e.numero_expediente
																		// ");
		sql.append("FROM tipoDocumento td, formatoDocumento fd, "); // documentos_expedientes
																	// de
																	// left
																	// join
																	// expedientes
																	// e
																	// on
																	// (de.id_expediente
																	// =
																	// e.id),
																	// ");
		sql.append("Documento d left join d.autor p) ");
		sql.append("WHERE d.tipoDocumento.id= td.id and d.formato.id= fd.id and ");
		sql.append("((");
		sql.append(tipoDocumentoBusqueda);
		sql.append(" = -1 or d.tipoDocumento.id = ");
		sql.append(tipoDocumentoBusqueda);
		sql.append(") and ('");
		sql.append(numeroDocumentoBusqueda.equals("") ? _1 : numeroDocumentoBusqueda);
		sql.append("' = '-1' or d.numeroDocumento = '");
		sql.append(numeroDocumentoBusqueda.equals("") ? _1 : numeroDocumentoBusqueda);
		sql.append("') and ("); // and de.id_documento = d.id
		sql.append("d.formato.id = " + FormatoDocumento.ELECTRONICO);
		sql.append(") and ('");
		sql.append(emisorDocumentoBusqueda.equals("") ? _1 : emisorDocumentoBusqueda);
		sql.append("' = '-1' or upper(d.emisor.nombres) like upper('%");
		sql.append(emisorDocumentoBusqueda.equals("") ? _1 : emisorDocumentoBusqueda);
		sql.append("%')) and ('");
		sql.append(materiaDocumentoBusqueda.equals("") ? _1 : materiaDocumentoBusqueda);
		sql.append("' = '-1' or upper(d.materia) like upper('%");
		sql.append(materiaDocumentoBusqueda.equals("") ? _1 : materiaDocumentoBusqueda);
		sql.append("%'))");
		// if (flagMostrarClasificaciones == 1) {
		// if (clasificacionTipoBusqueda != null && !clasificacionTipoBusqueda.trim().equals("-1")) {
		// sql.append(" and d.id_clasificacion_tipo = ");
		// sql.append(Integer.parseInt(clasificacionTipoBusqueda));
		// sql.append(" ");
		// }
		// if (clasificacionSubtipoBusqueda != null && !clasificacionSubtipoBusqueda.trim().equals("-1")) {
		// sql.append(" and d.id_clasificacion_subtipo = ");
		// sql.append(Integer.parseInt(clasificacionSubtipoBusqueda));
		// sql.append(" ");
		// }
		// }

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			sql.append(" and d.fechaDocumentoOrigen between :fechaInicioDocumentoBusqueda ");
			sql.append(" and :fechaTerminoDocumentoBusqueda");

		}

		sql.append(")");
		logger.info("[filtrarHistorialDocumento] SQL a ejecutar: " + sql);

		Query qry = em.createQuery(sql.toString());

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			qry.setParameter(1, fechaInicioDocumentoBusqueda);
			qry.setParameter(2, fechaTerminoDocumentoBusqueda);

		}

		List<Object[]> arrayObject = qry.getResultList();
		logger.info("[filtrarHistorialDocumento] Tiempo en ejecutar consulta: " + (System.currentTimeMillis() - init));
		this.procesarHistorialDeDocumento(arrayObject);
		logger.info("[filtrarHistorialDocumento] Tiempo total: " + (System.currentTimeMillis() - init));
	}

	@SuppressWarnings("unchecked")
	private void procesarHistorialDeDocumento(List<Object[]> arrayObject) {
		List<DatosUsuarioBandejaSalida> datosUsuarios = null;
		ExpedienteBandejaSalida expedienteSalida = null;
		DatosUsuarioBandejaSalida datosUsuario = null;
		for (Object[] o : arrayObject) {
			datosUsuarios = new ArrayList<DatosUsuarioBandejaSalida>();
			expedienteSalida = new ExpedienteBandejaSalida();

			Long idDoc = (((Long) o[0]) != null) ? ((Long) o[0]).longValue() : null;
			// Long idDoc = (((BigInteger) o[0]) != null) ? ((BigInteger) o[0]).longValue() : null;
			String numeroDoc = (String) o[1];
			String descTipoDoc = (String) o[2];
			Date fechaDocOrigen = (Date) o[3];
			String emisor = (String) o[4];
			String nombresAutor = (String) o[5];
			String apellidoPatAutor = (String) o[6];
			String apellidoMatAutor = (String) o[7];
			String materia = (String) o[8];
			Object cmsId = o[9];
			String descFd = (String) o[10];

			// String numeroExpediente = (String) o[12];

			expedienteSalida.setTipoDocumento(descTipoDoc);
			expedienteSalida.setNumeroDocumento(numeroDoc);
			expedienteSalida.setFechaDocumentoOrigen(fechaDocOrigen);
			expedienteSalida.setEmisor(emisor);
			expedienteSalida.setAutor(nombresAutor + " " + apellidoPatAutor + " " + apellidoMatAutor);
			expedienteSalida.setMateria(materia);
			expedienteSalida.setIdDocumento(idDoc);
			expedienteSalida.setTieneCmsId(cmsId != null);
			expedienteSalida.setFormatoDocumento(descFd);
			// expedienteSalida.setNumeroExpediente(numeroExpediente);

			StringBuilder sql = new StringBuilder(
					"select b.fecha, ed.descripcion as estadoDescripcion, p.nombres, p.apellido_paterno, ");
			sql.append("p.apellido_materno, c.descripcion as cargoDescripcion, u.descripcion as unidadDescripcion ");
			sql.append("from estadoDocumentos as ed ,Bitacora as b left join b.personas as p  ");
			sql.append("left join p.cargo as c  left join c.unidadOrganizacional as u  ");
			sql.append("where b.estadoDocumento.id = ed.id and b.documento.id = " + idDoc);
			sql.append("group by b.fecha, estadoDescripcion, p.nombres, p.apellidoPaterno, ");
			sql.append("p.apellidoMaterno,cargoDescripcion,unidadDescripcion ");
			sql.append("order by b.fecha asc ");
			List<Object[]> arrayObjectBita = em.createQuery(sql.toString()).getResultList();

			for (Object[] ob : arrayObjectBita) {
				datosUsuario = new DatosUsuarioBandejaSalida();

				Date fecha = (Date) ob[0];
				String descEstadoDoc = (String) ob[1];
				String nombresPersona = (String) ob[2];
				String apellidoPatPersona = (String) ob[3];
				String apellidoMatPersona = (String) ob[4];
				String descCargo = (String) ob[5];
				String descUnidad = (String) ob[6];

				datosUsuario.setDestinatarioUsuario(nombresPersona + " " + apellidoPatPersona + " "
						+ apellidoMatPersona);
				datosUsuario.setDestinatarioCargo(descCargo);
				datosUsuario.setDestinatarioUnidadOrganizacional(descUnidad);
				datosUsuario.setFechaModificacionDocumento(fecha);
				datosUsuario.setEstadoDocumentoModificado(descEstadoDoc);
				datosUsuarios.add(datosUsuario);
			}
			expedienteSalida.setDatosUsuariosAsociados(datosUsuarios);
			historialDocumento.add(expedienteSalida);
		}
	}

	@Factory(value = "documentosRelacionados", scope = ScopeType.SESSION)
	public String obtenerReporteDeDocumentosRelacionados() {
		logger.debug("[obtenerReporteDeDocumentosRelacionados] Iniciando método");
		this.emisorDocumentoBusqueda = "";
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		tipoDocumentoBusqueda = null;
		documentosRelacionados.clear();
		//this.completaJerarquia();
		//this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		//this.buscarUnidadesOrganizacionales();
		homeCrear = FWD_REPORTE_DOCUMENTOS_RELACIONADOS;
		return FWD_REPORTE_DOCUMENTOS_RELACIONADOS;
	}

	@SuppressWarnings("unchecked")
	public void buscarDocumentos() {
		StringBuilder sql = new StringBuilder("SELECT d FROM Documento d WHERE  ");
		numeroDocumentoBusqueda = numeroDocumentoBusqueda == null ? "" : numeroDocumentoBusqueda.trim();

		numeroInternoDocumentoBusqueda = numeroInternoDocumentoBusqueda == null ? "" : numeroInternoDocumentoBusqueda
				.trim();

		materiaDocumentoBusqueda = materiaDocumentoBusqueda == null ? "" : materiaDocumentoBusqueda.trim();
		if (numeroDocumentoBusqueda.length() > 0) sql.append("d.numeroDocumento = '").append(numeroDocumentoBusqueda)
				.append("' and ");
		if (numeroInternoDocumentoBusqueda.length() > 0) sql.append("d.numeroDocumentoInterno = '")
				.append(numeroInternoDocumentoBusqueda).append("' and ");
		if (materiaDocumentoBusqueda.length() > 0) sql.append("upper(d.materia) like upper('%")
				.append(materiaDocumentoBusqueda).append("%') and ");
		sql.append(" 1 = 1 ");

		List<Documento> documentos;
		documentos = em.createQuery(sql.toString()).getResultList();

		documentosEncontrados.clear();
		for (Documento doc : documentos) {
			ExpedienteBandejaSalida exp = new ExpedienteBandejaSalida();
			exp.setIdDocumento(doc.getId());
			exp.setMateria(doc.getMateria());
			exp.setNumeroDocumento(doc.getNumeroDocumento());
			exp.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
			exp.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
			exp.setEmisor(doc.getEmisor());
			exp.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			exp.setTieneCmsId(doc.getCmsId() != null);
			documentosEncontrados.add(exp);
		}
	}

	@SuppressWarnings("unchecked")
	public void filtrarDocumentosRelacionados() {
		logger.info("[filtrarDocumentosRelacionados] Iniciando método.");
		documentosRelacionados.clear();
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		if (numeroDocumentoBusqueda.isEmpty()) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MINIMO_RANGO_FECHAS);
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				return;
			}
		}
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct e.numeroExpediente, eo.id, eo.fechaIngreso, ");
		sql.append("do.id, to.descripcion, do.numeroDocumento, do.fechaDocumentoOrigen, ");
		sql.append("do.emisor, do.materia, fo.descripcion, edo.tipoDocumentoExpediente.id, ");
		sql.append("d.id, t.descripcion, d.numeroDocumento, d.fechaDocumentoOrigen, ");
		sql.append("d.emisor, d.materia, f.descripcion, ed.tipoDocumentoExpediente.id ");
		sql.append("from Expediente e, Expediente eo ");
		sql.append("left join e.documentos edo ");
		sql.append("left join edo.documento do ");
		sql.append("left join do.tipoDocumento to "); 
		sql.append("left join do.formatoDocumento fo ");
		sql.append("left join e.documentos ed ");
		sql.append("left join ed.documento d ");
		sql.append("left join d.tipoDocumento t ");
		sql.append("left join d.formatoDocumento f ");
		sql.append("where edo.tipoDocumentoExpediente.id = 1 ");
		sql.append("and e.numeroExpediente = eo.numeroExpediente ");
		sql.append("and eo.versionPadre is null ");
		sql.append("and (do.eliminado is null or do.eliminado = false) ");
		sql.append("and (d.eliminado is null or d.eliminado = false) ");
		sql.append("and e.numeroExpediente in (");
		sql.append("		select e1.numeroExpediente ");
		sql.append("		from Expediente e1 ");
		sql.append("		left join e1.documentos ed1 ");
		sql.append("		left join ed1.documento d1 ");
		sql.append("		left join d1.tipoDocumento t1 ");
		sql.append("		where e1.numeroExpediente = e.numeroExpediente ");
		
		if (!tipoDocumentoBusqueda.equals(_1)) {
			sql.append("and t1.id = :tipoDocumento ");
		}
		if (!numeroDocumentoBusqueda.trim().isEmpty()) {
			sql.append("and upper(d1.numeroDocumento) like upper(:numeroDocumento) ");
		}
		if (!emisorDocumentoBusqueda.trim().isEmpty()) {
			sql.append("and upper(d1.emisor) like upper(:emisor) ");
		}
		if (!materiaDocumentoBusqueda.trim().isEmpty()) {
			sql.append("and upper(d1.materia) like upper(:materia) ");
		}
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			sql.append("and (d1.fechaDocumentoOrigen between :fechaInicioDocumentoBusqueda and :fechaTerminoDocumentoBusqueda) ");
		}
		sql.append(") ");
		sql.append("order by eo.fechaIngreso, do.fechaDocumentoOrigen, do.id, ed.tipoDocumentoExpediente.id, d.fechaDocumentoOrigen, d.id ");

		logger.info("[filtrarDocumentosRelacionados] SQL a ejecutar: " + sql);

		Query query = em.createQuery(sql.toString());

		if (!tipoDocumentoBusqueda.equals(_1)) {
			query.setParameter("tipoDocumento", Integer.parseInt(tipoDocumentoBusqueda));
		}
		
		if (!numeroDocumentoBusqueda.trim().isEmpty()) {
			query.setParameter("numeroDocumento", "%" + numeroDocumentoBusqueda.trim() + "%");
		}
		
		if (!emisorDocumentoBusqueda.trim().isEmpty()) {
			query.setParameter("emisor", "%" + emisorDocumentoBusqueda.trim() + "%");
		}
		
		if (!materiaDocumentoBusqueda.trim().isEmpty()) {
			query.setParameter("materia", "%" + materiaDocumentoBusqueda.trim() + "%");
		}
		
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			fechaInicioDocumentoBusqueda = FechaUtil.inicio(fechaInicioDocumentoBusqueda);
			fechaTerminoDocumentoBusqueda = FechaUtil.fin(fechaTerminoDocumentoBusqueda);
			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda);
		}

		List<Object[]> arrayObject = query.getResultList();
		this.busquedaDocumentosRelacionados(arrayObject);
	}

	/**
	 * Crea una lista de documentos, con todos sus documentos asociados y los deja en la variable
	 * documentosRelacionados.
	 * 
	 * @param docExpList List<DocumentoExpediente> que contiene la lista de los DocumentoExpediente que contiene todos
	 *        los documentos y Expedientes que se quieren procesar.
	 */
	private void busquedaDocumentosRelacionados(List<Object[]> arrayObject) {
		String prevExp = new String();
		documentosRelacionados = new ArrayList<ExpedienteBandejaSalida>();
		ExpedienteBandejaSalida expedienteBandejaSalida = null;
		for (Object[] o : arrayObject) {
			if (!prevExp.equals((String)o[0])) {
				prevExp= (String)o[0];
				expedienteBandejaSalida = new ExpedienteBandejaSalida();
				//e.numeroExpediente
				expedienteBandejaSalida.setNumeroExpediente((String)o[0]);
				//eo.id
				//eo.fechaIngreso
				//do.id
				expedienteBandejaSalida.setIdDocumento((Long)o[3]);
				//to.descripcion
				expedienteBandejaSalida.setTipoDocumento(o[4] != null ? (String)o[4] : "");
				//do.numeroDocumento
				expedienteBandejaSalida.setNumeroDocumento(o[5] != null ? (String)o[5] : "");
				//do.fechaDocumentoOrigen
				expedienteBandejaSalida.setFechaDocumentoOrigen((Date)o[6]);
				//do.emisor
				expedienteBandejaSalida.setEmisor(o[7] != null ? (String)o[7] : "");
				//do.materia
				expedienteBandejaSalida.setMateria(o[8] != null ? (String)o[8] : "");
				//fo.descripcion
				expedienteBandejaSalida.setFormatoDocumento(o[9] != null ? (String)o[9] : "");
				//edo.tipoDocumentoExpediente.id
				
				expedienteBandejaSalida.setDocumentosRelacionados(new ArrayList<Documento>());
				documentosRelacionados.add(expedienteBandejaSalida);
			
			}
			if (o[18] != null && ((Integer)o[18]).equals(new Integer(2))) {
				Documento documento = new Documento();
				//d.id
				documento.setId((Long)o[11]);
				//t.descripcion
				TipoDocumento tipoDocumento = new TipoDocumento();
				tipoDocumento.setDescripcion(o[12] != null ? (String)o[12] : "");
				documento.setTipoDocumento(tipoDocumento);
				//d.numeroDocumento
				documento.setNumeroDocumento(o[13] != null ? (String)o[13] : "");
				//d.fechaDocumentoOrigen
				documento.setFechaDocumentoOrigen((Date)o[14]);
				//d.emisor
				documento.setEmisor(o[15] != null ? (String)o[15] : "");
				//d.materia
				documento.setMateria(o[16] != null ? (String)o[16] : "");
				//f.descripcion
				FormatoDocumento formatoDocumento = new FormatoDocumento();
				formatoDocumento.setDescripcion(o[17] != null ? (String)o[17] : "");
				documento.setFormatoDocumento(formatoDocumento);
				expedienteBandejaSalida.getDocumentosRelacionados().add(documento);
				//ed.tipoDocumentoExpediente.id
			}
			

//			String descTipoDoc = (String) o[0];
//			String numeroDoc = (String) o[1];
//			Long idDoc = (((Long) o[2] != null) ? ((Long) o[2]).longValue() : null);
//			// Long idDoc = (((BigInteger) o[2] != null) ? ((BigInteger) o[2]).longValue() : null);
//			Date fechaDocOrigen = (Date) o[3];
//			String emisor = (String) o[4];
//			String materia = (String) o[5];
//			String descFormatoDoc = (String) o[6];
//			Object cmsId = (Object) o[7];
//			String nombreAutor = (String) o[8];
//			String apellidoPatAutor = (String) o[9];
//			String apellidoMatAutor = (String) o[10];
//			Long idExp = (((Long) o[11] != null) ? ((Long) o[11]).longValue() : null);
//			// Long idExp = (((BigInteger) o[11] != null) ? ((BigInteger) o[11]).longValue() : null);
//			String numeroExpediente = (String) o[12];
//
//			Expediente exp = new Expediente();
//			exp.setId(idExp);
//			Documento doc = new Documento();
//			doc.setId(idDoc);
//
//			int indice = indiceDocEnBandejaDeEntrada(idDoc, "", documentosRelacionados);
//			if (indice != -1) {
//				List<Documento> docListActuales = documentosRelacionados.get(indice).getDocumentosRelacionados();
//				List<Documento> docListNuevos = buscarDocumentosAsociadosAExpediente(exp);
//				documentosRelacionados.get(indice).setDocumentosRelacionados(
//						eliminarDocumentoDeLista(filtrarDocumentosRepetidos(docListActuales, docListNuevos), doc));
//			} else {
//				expedienteBandejaSalida = new ExpedienteBandejaSalida();
//				expedienteBandejaSalida.setTipoDocumento(descTipoDoc);
//				expedienteBandejaSalida.setNumeroDocumento(numeroDoc);
//				expedienteBandejaSalida.setId(idDoc);
//				expedienteBandejaSalida.setFechaDocumentoOrigen(fechaDocOrigen);
//				expedienteBandejaSalida.setAutor(nombreAutor + " " + apellidoPatAutor + " " + apellidoMatAutor);
//				expedienteBandejaSalida.setEmisor(emisor);
//				expedienteBandejaSalida.setMateria(materia);
//				expedienteBandejaSalida.setFormatoDocumento(descFormatoDoc);
//				expedienteBandejaSalida.setTieneCmsId(cmsId != null);
//				if (doc != null) {
//					expedienteBandejaSalida.setIdDocumento(idDoc);
//				}
//				expedienteBandejaSalida.setDocumentosRelacionados(eliminarDocumentoDeLista(
//						buscarDocumentosAsociadosAExpediente(exp), doc));
//				expedienteBandejaSalida.setNumeroExpediente(numeroExpediente);
//				documentosRelacionados.add(expedienteBandejaSalida);
//			}
		}
	}

	private List<Documento> eliminarDocumentoDeLista(List<Documento> docList, Documento docBuscado) {
		List<Documento> docListRetorno = new ArrayList<Documento>();
		if (docList.contains(docBuscado)) {
			for (Documento documento : docList) {
				if (!documento.getId().equals(docBuscado.getId())) {
					docListRetorno.add(documento);
				}
			}
			return docListRetorno;
		}
		return docList;
	}

	private List<Documento> filtrarDocumentosRepetidos(List<Documento> docList1, List<Documento> docList2) {
		List<Documento> docListRetorno = new ArrayList<Documento>();
		if (docList1 != null) {
			for (Documento doc : docList1) {
				if (!docListRetorno.contains(doc)) {
					docListRetorno.add(doc);
				}
			}
		}
		if (docList2 != null) {
			for (Documento doc : docList2) {
				if (!docListRetorno.contains(doc)) {
					docListRetorno.add(doc);
				}
			}
		}
		return docListRetorno;
	}

	/**
	 * Busca los documentos asociados a un expediente en particular.
	 * 
	 * @param expediente Expediente del que se quieren obtener sus documentos asociados.
	 * @return List<Documento> que contiene la lista de documentos asociados al expediente de entrada
	 */
	@SuppressWarnings("unchecked")
	private List<Documento> buscarDocumentosAsociadosAExpediente(Expediente expediente) {
		List<Documento> docAsExp = new ArrayList<Documento>();
		StringBuilder sql = new StringBuilder("");
		sql.append("select e.id, de.documento.id, td.descripcion, d.numeroDocumento, ");
		sql.append("d.fechaDocumentoOrigen, d.emisor, d.materia ");
		sql.append("from Expediente e, DocumentoExpediente de, ");
		sql.append("Documento d, TipoDocumento td ");
		sql.append("where e.id= de.expediente.id and de.documento.id= d.id ");
		sql.append("and d.tipoDocumento.id = td.id and e.id= " + expediente.getId());
		List<Object[]> arrayObject = em.createQuery(sql.toString()).getResultList();

		for (Object[] o : arrayObject) {
			// Long idExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			Long idDoc = (((Long) o[1] != null) ? ((Long) o[1]).longValue() : null);
			String descTipoDoc = (String) o[2];
			String numeroDoc = (String) o[3];
			Date fechaDocOrigen = (Date) o[4];
			String emisor = (String) o[5];
			String materia = (String) o[6];
			Documento doc = new Documento();
			TipoDocumento tipoDoc = new TipoDocumento();
			doc.setId(idDoc);
			doc.setEmisor(emisor);
			doc.setNumeroDocumento(numeroDoc);
			doc.setMateria(materia);
			doc.setFechaDocumentoOrigen(fechaDocOrigen);
			tipoDoc.setDescripcion(descTipoDoc);
			doc.setTipoDocumento(tipoDoc);
			docAsExp.add(doc);
		}
		return filtrarDocumentosRepetidos(docAsExp, null);
	}

	@Factory(value = "historialBandejaEntrada", scope = ScopeType.SESSION)
	public String obtenerHistorialBandejaEntrada() {
		logger.info("[obtenerHistorialBandejaEntrada] Iniciando método.");
		this.emisorDocumentoBusqueda = "";
		numeroExpedienteBusqueda = "";
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		tipoDocumentoBusqueda = null;
		historialBandejaEntrada.clear();
		//this.completaJerarquia();
		//this.setDepartamento(jerarquias.INICIO);
		//this.buscarUnidadesOrganizacionales();
		homeCrear = FWD_REPORTE_HISTORIAL_BANDEJA_ENTRADA;
		return FWD_REPORTE_HISTORIAL_BANDEJA_ENTRADA;
	}

	@SuppressWarnings("unchecked")
	public void filtrarHistorialBandejaEntrada() {
		
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		if (numeroExpedienteBusqueda.isEmpty()) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MINIMO_RANGO_FECHAS);
				return;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				return;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				return;
			}
		}

		logger.info("[filtrarHistorialBandejaEntrada] Iniciando método.");
		historialBandejaEntrada.clear();
		StringBuilder jpQL = new StringBuilder();
		jpQL.append("select e.id, e.numeroExpediente, e.fechaIngreso, e.fechaAcuseRecibo, "); 
		jpQL.append("(case when e.fechaAcuseRecibo is not null then e.fechaDespacho else null end) as fechaDespacho, e.copia, e.reasignado, ");
		jpQL.append("(case when e.fechaAcuseRecibo is null then emi.nombres else emi_h.nombres end) as nombre, ");
		jpQL.append("(case when e.fechaAcuseRecibo is null then emi.apellidoPaterno else emi_h.apellidoPaterno end) as apellido, ");
		jpQL.append("d.id, d.numeroDocumento, td.descripcion, fd.descripcion, ");
		jpQL.append("d.reservado, d.emisor, d.fechaDocumentoOrigen, d.materia "); 
		jpQL.append("from Expediente e "); 
		jpQL.append("inner join e.documentos de ");
		jpQL.append("inner join de.documento d ");
		jpQL.append("inner join d.tipoDocumento td "); 
		jpQL.append("inner join d.formatoDocumento fd ");
		jpQL.append("left join e.emisor emi ");
		jpQL.append("left join e.emisorHistorico emi_h ");
		jpQL.append("where e.archivado is null ");
		jpQL.append("and e.cancelado is null ");
		jpQL.append("and not (e.fechaDespacho is null and e.fechaDespachoHistorico is null) ");
		jpQL.append("and d.fechaCreacion = (");
		jpQL.append("   select max(d2.fechaCreacion) "); 
		jpQL.append("   from Expediente e2 ");
		jpQL.append("   inner join e2.documentos ed2 ");
		jpQL.append("   inner join ed2.documento d2 ");
		jpQL.append("   where e2.id = e.id ");
		jpQL.append("   and (d2.eliminado is null or d2.eliminado = false)) ");
		jpQL.append("and ((e.destinatario.id = ").append(usuario.getId()).append(" and e.fechaAcuseRecibo is null) ");
		jpQL.append("	or (e.destinatarioHistorico.id = ").append(usuario.getId()).append(" and e.fechaAcuseRecibo is not null)) ");
		jpQL.append("and e.versionPadre is not null ");
		jpQL.append(" and (");
		jpQL.append(tipoDocumentoBusqueda);
		jpQL.append(" = -1 or d.tipoDocumento.id = ");
		jpQL.append(tipoDocumentoBusqueda);
		jpQL.append(") and ('");
		jpQL.append(emisorDocumentoBusqueda.equals("") ? _1 : emisorDocumentoBusqueda);
		jpQL.append("' = '-1' or d.autor.usuario = '");
		jpQL.append(emisorDocumentoBusqueda);
		jpQL.append("') and ('");
		jpQL.append(numeroExpedienteBusqueda.equals("") ? _1 : numeroExpedienteBusqueda);
		jpQL.append("' = '-1' or e.numeroExpediente = '");
		jpQL.append(numeroExpedienteBusqueda);
		jpQL.append("') ");
		// if (flagMostrarClasificaciones == 1) {
		// if (clasificacionTipoBusqueda != null && !clasificacionTipoBusqueda.trim().equals("-1")) {
		// jpQL.append(" and d.id_clasificacion_tipo = ");
		// jpQL.append(Integer.parseInt(clasificacionTipoBusqueda));
		// jpQL.append(" ");
		// }
		// if (clasificacionSubtipoBusqueda != null && !clasificacionSubtipoBusqueda.trim().equals("-1")) {
		// jpQL.append(" and d.id_clasificacion_subtipo = ");
		// jpQL.append(Integer.parseInt(clasificacionSubtipoBusqueda));
		// jpQL.append(" ");
		// }
		// }
		// if (this.factura) {
		// if (this.rutProveedorBusqueda != null && !this.rutProveedorBusqueda.trim().equals("")) {
		// jpQL.append(" and upper(d.rut_proveedor_factura) like upper('");
		// jpQL.append(this.rutProveedorBusqueda.trim());
		// jpQL.append("%')");
		// }
		// if (this.numeroOrdenDeCompraBusqueda != null) {
		// jpQL.append(" and d.numero_orden_compra_factura = '");
		// jpQL.append(this.numeroOrdenDeCompraBusqueda);
		// jpQL.append("'");
		// }
		// }
//		if( departamento != null && departamento > 0) {
//			jpQL.append(" and ('");
//			jpQL.append(departamento.equals("") ? _1 : departamento);
//			jpQL.append("' = '-1' or ( e.emisor.cargo.unidadOrganizacional.departamento.id = '");
//			jpQL.append(departamento);
//			jpQL.append("' or e.emisorHistorico.cargo.unidadOrganizacional.departamento.id = '");
//			jpQL.append(departamento);
//			jpQL.append("') ) ");
//		}
//		
		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			jpQL.append(" and e.fechaIngreso between :fechaInicioDocumentoBusqueda ");
			jpQL.append(" and :fechaTerminoDocumentoBusqueda");
		}
		jpQL.append(" order by e.fechaIngreso");

		StringBuilder jpQL2 = new StringBuilder(jpQL);
		//jpQL carga consulta con expedientes con destinatarios
		//jpQL.replace(130, 130, "e.destinatario.nombres, e.destinatario.apellidoPaterno,");
		//jpQL2 carga consulta con expediente con destinatarios historicos
		//jpQL2.replace(130, 130, "e.destinatarioHistorico.nombres, e.destinatarioHistorico.apellidoPaterno,");
		logger.info("[filtrarHistorialBandejaEntrada] SQL a ejecutar : " + jpQL);
		//logger.info("[filtrarHistorialBandejaEntrada] SQL2 a ejecutar (destinatarios histopricos) : " + jpQL2);
		Query query = em.createQuery(jpQL.toString());
		//Query query2 = em.createQuery(jpQL2.toString());

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {

			fechaInicioDocumentoBusqueda = FechaUtil.inicio(fechaInicioDocumentoBusqueda);
			fechaTerminoDocumentoBusqueda = FechaUtil.fin(fechaTerminoDocumentoBusqueda);

			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda);
			//query2.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda);
			//query2.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda);

		}

		//logger.info("[filtrarHistorialBandejaEntrada] -SQL2 a ejecutar: " + jpQL2);
		List<Object[]> arrayObject = query.getResultList();
		//arrayObject.addAll(query2.getResultList());

		//query = em.createQuery(jpQL2.toString());
		//List<Object[]> arrayObject2 = query.getResultList();
		//arrayObject.addAll(query2.getResultList());

		//logger.info("[filtrarHistorialBandejaEntrada] La consulta a ejecutar es: (" + arrayObject.size() + ") " + jpQL);
		//logger.info("[filtrarHistorialBandejaEntrada] La consulta2 a ejecutar es: (" + arrayObject2.size() + ") " + jpQL2);
		this.procesarHistorialBandejaEntrada(arrayObject);
	}

	private void procesarHistorialBandejaEntrada(List<Object[]> arrayObject) {
		//Map<Long, ExpedienteBandejaEntrada> map = new HashMap<Long, ExpedienteBandejaEntrada>();
		//Long idExpPrev = 0L;
		historialBandejaEntrada = new ArrayList<ExpedienteBandejaEntrada>();
		for (Object[] o : arrayObject) {
			ExpedienteBandejaEntrada bandejaEntrada = new ExpedienteBandejaEntrada();
			//e.id
			bandejaEntrada.setId((Long)o[0]);
			//e.numeroExpediente
			bandejaEntrada.setNumeroExpediente((String)o[1]);
			//e.fechaIngreso
			bandejaEntrada.setFechaIngresoBandeja((Date)o[2]);
			//e.fechaAcuseRecibo
			bandejaEntrada.setFechaAcuseRecibo((Date)o[3]);
			//e.fechaDespacho
			bandejaEntrada.setFechaDespacho((Date)o[4]);
			//e.copia
			bandejaEntrada.setCopia(o[5] != null ? (Boolean)o[5] : false);
			//e.reasignado
			bandejaEntrada.setFechaReasignado((Date)o[6]);
			//nombre
			//apellido
			bandejaEntrada.setRemitente((o[7] != null ? ((String)o[7]).concat(" ") : "") + (o[8] != null ? (String)o[8] : ""));
			//d.id
			bandejaEntrada.setIdDocumento((Long)o[9]);
			//d.numeroDocumento
			bandejaEntrada.setNumeroDocumento(o[10] != null ? (String)o[10] : "");
			//tipoDocumento
			bandejaEntrada.setTipoDocumento(o[11] != null ? (String)o[11] : "");
			//formato
			bandejaEntrada.setFormatoDocumento(o[12] != null ? (String)o[12] : "");
			//d.reservado
			bandejaEntrada.setReservado(o[13] != null ? (Boolean)o[13] : false);
			//d.emisor
			bandejaEntrada.setEmisor(o[14] != null ? (String)o[14] : "");
			//d.fechaDocumentoOrigen
			bandejaEntrada.setFechaDocumentoOrigen((Date)o[15]);
			//d.materia
			bandejaEntrada.setMateria(bandejaEntrada.getReservado() ? Messages.instance().get("materia_reservada") : (o[16] != null ? (String)o[16]: ""));
			historialBandejaEntrada.add(bandejaEntrada);
		}
			
//		Map<Long, ExpedienteBandejaEntrada> map = new HashMap<Long, ExpedienteBandejaEntrada>();
//		Long idExpPrev = 0L;
//		historialBandejaEntrada = new ArrayList<ExpedienteBandejaEntrada>();
//		for (Object[] o : arrayObject) {		
//			Long idExp = (Long)o[0];
//			if (!idExp.equals(idExpPrev)) {
//				ExpedienteBandejaEntrada bandejaEntrada = new ExpedienteBandejaEntrada();
//				bandejaEntrada.setId(idExp);
//				bandejaEntrada.setNumeroExpediente((String)o[1]);
//				bandejaEntrada.setFechaIngreso(o[2] != null ? (Date)o[2] : null);
//				
//				//no acuse recibo
//				if (o[3] == null) {
//					bandejaEntrada.setRemitente((o[6] != null ? (String)o[6] : "" ) + (o[6] != null ? " " : "") + (o[7] != null ? (String)o[7] : "" ));
//				} else {
//					bandejaEntrada.setFechaAcuseRecibo((Date)o[3]);
//					if (o[5] != null) {
//						bandejaEntrada.setFechaDespacho((Date)o[4]);
//					}
//					bandejaEntrada.setRemitente((o[8] != null ? (String)o[8] : "" ) + (o[8] != null ? " " : "") + (o[9] != null ? (String)o[9] : "" ));
//				}
//				bandejaEntrada.setDocumentosRelacionados(new ArrayList<Documento>());
//				
//				Documento doc = new Documento();
//				doc.setId((Long)o[10]);
//				doc.setNumeroDocumento((String)o[11]);
//				TipoDocumento td = new TipoDocumento();
//				td.setDescripcion((String)o[12]);
//				doc.setTipoDocumento(td);
//				FormatoDocumento fd = new FormatoDocumento();
//				fd.setDescripcion((String)o[13]);
//				doc.setFormatoDocumento(fd);
//				doc.setReservado((Boolean)o[14]);
//				doc.setEmisor((String)o[15]);
//				if (o[16] != null) {
//					doc.setFechaDocumentoOrigen((Date)o[16]);
//				}
//				bandejaEntrada.getDocumentosRelacionados().add(doc);
//				map.put(idExp, bandejaEntrada);
//			} else {
//				
//				Documento doc = new Documento();
//				doc.setId((Long)o[10]);
//				doc.setNumeroDocumento((String)o[11]);
//				TipoDocumento td = new TipoDocumento();
//				td.setDescripcion((String)o[12]);
//				doc.setTipoDocumento(td);
//				FormatoDocumento fd = new FormatoDocumento();
//				fd.setDescripcion((String)o[13]);
//				doc.setFormatoDocumento(fd);
//				doc.setReservado((Boolean)o[14]);
//				doc.setEmisor((String)o[15]);
//				if (o[16] != null) {
//					doc.setFechaDocumentoOrigen((Date)o[16]);
//				}
//				map.get(idExp).getDocumentosRelacionados().add(doc);
//			}
//			idExpPrev = idExp;
//		}
//		// TODO SORT DATES
//		historialBandejaEntrada.addAll(map.values());
//		Collections.sort(historialBandejaEntrada, new Comparator<ExpedienteBandejaEntrada>() {
//			public int compare(final ExpedienteBandejaEntrada o1, final ExpedienteBandejaEntrada o2) {
//				return o1.getFechaIngreso().compareTo(o2.getFechaIngreso());
//			}
//		});
	}

	@Factory(value = "cerrarDocumentos", scope = ScopeType.SESSION)
	public String obtenerCierreDocumentos() {
		logger.debug("[cerrarDocumentos] Iniciando método");
		documentosCierre.clear();
		return FWD_REPORTE_CIERRE_DOCUMENTOS;
	}

	public List<ExpedienteBandejaSalida> buscarDocumentosCierre(Date fechaInicio, Date fechaTermino) {
		documentosCierre.clear();
		fechaInicioDocumentoBusqueda = fechaInicio;
		fechaTerminoDocumentoBusqueda = fechaTermino;
		buscarDocumentosCierre();
		return documentosCierre;
	}

	public Long getFechaInicio() {
		if (fechaInicioDocumentoBusqueda != null) {
			return fechaInicioDocumentoBusqueda.getTime();
		} else {
			return new Date().getTime();
		}
		// return fechaInicioDocumentoBusqueda.getTime();
	}

	public Long getFechaTermino() {
		if (fechaTerminoDocumentoBusqueda != null) {
			return fechaTerminoDocumentoBusqueda.getTime();
		} else {
			return new Date().getTime();
		}
		// return fechaTerminoDocumentoBusqueda.getTime();
	}

	private Date fechaCierre;

	public Long getFechaCierre() {
		if (fechaCierre == null) {
			fechaCierre = new Date();
		}
		return fechaCierre.getTime();
	}

	@SuppressWarnings("unchecked")
	public void buscarDocumentosCierre() {
		fechaCierre = new Date();
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);

		StringBuilder jpQL = new StringBuilder("SELECT d ");
		jpQL.append("FROM Documento d ");
		jpQL.append("WHERE ");

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			jpQL.append(" and d.fechaDocumentoOrigen between :fechaInicioDocumentoBusqueda ");
			jpQL.append(" and :fechaTerminoDocumentoBusqueda");

		}

		jpQL.append(" ORDER BY d.fechaCreacion ");

		logger.info("[buscarDocumentosCierre] SQL a ejecutar: " + jpQL);

		Query query = em.createQuery(jpQL.toString());

		if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {

			query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda);
			query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda);

		}

		logger.info("buscarDocumentosCierre: " + jpQL.toString());
		List<Documento> documentos = query.getResultList();
		for (Documento doc : documentos) {
			ExpedienteBandejaSalida ebs = new ExpedienteBandejaSalida();
			ebs.setNumeroDocumento(doc.getNumeroDocumento() == null ? "s/n" : doc.getNumeroDocumento());
			ebs.setFechaIngreso(doc.getFechaCreacion());
			ebs.setEmisor(doc.getEmisor());
			if (doc.getDestinatarios().size() != 0) {
				ebs.setDestinatario(doc.getDestinatarios().toString());
			} else {
				ebs.setDestinatario("");
			}
			ebs.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
			ebs.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			documentosCierre.add(ebs);
		}
	}

	@Factory(value = "estadoBandejaSalida", scope = ScopeType.SESSION)
	public String obtenerEstadoBandejaSalida() {
		logger.info("[obtenerEstadoBandejaSalida] Iniciando método.");
		this.emisorDocumentoBusqueda = "";
		fechaInicioDocumentoBusqueda = null;
		fechaTerminoDocumentoBusqueda = null;
		tipoDocumentoBusqueda = null;
		estadoBandejaSalida.clear();
		//this.completaJerarquia();
		//this.setDepartamento(jerarquias.INICIO);
		homeCrear = FWD_REPORTE_ESTADO_BANDEJA_SALIDA;
		return FWD_REPORTE_ESTADO_BANDEJA_SALIDA;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void filtrarEstadoBandejaSalida() {
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		Boolean estado = Boolean.TRUE;
		int largoExp = numeroExpedienteBusqueda.trim().length();
		if (numeroExpedienteBusqueda != null && !numeroExpedienteBusqueda.trim().isEmpty() && largoExp >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres. en el campo \"N° Expediente\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		int largoEmi = emisorDocumentoBusqueda.trim().length();
		if (emisorDocumentoBusqueda != null && !emisorDocumentoBusqueda.trim().isEmpty() && largoEmi >= LARGO_MAXIMO) {
			String detalle = String.format("No puede ingresar más de %s carácteres. en el campo \"De\"", LARGO_MAXIMO);
			FacesMessages.instance().add(detalle);
			estado = Boolean.FALSE;
		}
		if (numeroExpedienteBusqueda.isEmpty()) {
			if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MINIMO_RANGO_FECHAS);
				estado = Boolean.FALSE;
			} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, TERMINO));
				estado = Boolean.FALSE;
			} else if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda != null) {
				FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, INICIO));
				estado = Boolean.FALSE;
			}
		}

		if (estado) {
			final long init = System.currentTimeMillis();
			logger.info("[filtrarEstadoBandejaSalida] Iniciando método.");
			estadoBandejaSalida.clear();
			
			final StringBuilder jpQL = new StringBuilder();
			jpQL.append("select e.id, e.numeroExpediente, e.fechaIngreso, e.fechaAcuseRecibo, ");
			jpQL.append("(case when e.fechaAcuseRecibo is not null then e.fechaDespacho else null end) as fechaDespacho, e.copia, e.reasignado, ");
			jpQL.append("(case when e.fechaAcuseRecibo is null then emi.nombres else emi_h.nombres end) as nombre, ");
			jpQL.append("(case when e.fechaAcuseRecibo is null then emi.apellidoPaterno else emi_h.apellidoPaterno end) as apellido, ");
			jpQL.append("d.id, d.numeroDocumento, td.descripcion, fd.descripcion, ");
			jpQL.append("d.reservado, d.emisor, d.fechaDocumentoOrigen, d.materia, e.versionPadre.id, g.nombre ");
			jpQL.append("from Expediente e ");
			jpQL.append("inner join e.documentos de ");
			jpQL.append("inner join de.documento d ");
			jpQL.append("inner join d.tipoDocumento td ");
			jpQL.append("inner join d.formatoDocumento fd ");
			jpQL.append("left join e.destinatario emi ");
			jpQL.append("left join e.destinatarioHistorico emi_h "); 
			jpQL.append("left join e.grupo g ");
			jpQL.append("where e.cancelado is null ");
			jpQL.append("and e.versionPadre is not null ");
			jpQL.append("and e.fechaIngreso is not null ");
			jpQL.append("and d.fechaCreacion = (");
			jpQL.append("   select max(d2.fechaCreacion) ");
			jpQL.append("   from Expediente e2 ");
			jpQL.append("   inner join e2.documentos ed2 ");
			jpQL.append("   inner join ed2.documento d2 ");
			jpQL.append("   where e2.id = e.id ");
			jpQL.append("   and (d2.eliminado is null or d2.eliminado = false)) ");
			jpQL.append("and ((e.emisor.id = ").append(usuario.getId()).append(" and e.fechaAcuseRecibo is null and e.fechaDespacho is not null and e.fechaDespachoHistorico is null) ");
			jpQL.append("	or (e.emisorHistorico.id = ").append(usuario.getId()).append(" and e.fechaAcuseRecibo is not null and e.fechaDespachoHistorico is not null)) ");
			jpQL.append("and (" + tipoDocumentoBusqueda);
			jpQL.append(" = -1 or d.tipoDocumento.id = ");
			jpQL.append(tipoDocumentoBusqueda);
			jpQL.append(") and ('");
			jpQL.append(emisorDocumentoBusqueda.trim().equals("") ? _1 : emisorDocumentoBusqueda);
			jpQL.append("' = '-1' or d.emisor = '%");
			jpQL.append(emisorDocumentoBusqueda.trim());
			jpQL.append("%') and ('");
			jpQL.append(numeroExpedienteBusqueda.trim().equals("") ? _1 : numeroExpedienteBusqueda);
			jpQL.append("' = '-1' or e.numeroExpediente = '");
			jpQL.append(numeroExpedienteBusqueda.trim());
			jpQL.append("') ");
	
			// if (flagMostrarClasificaciones == 1) {
			// if (clasificacionTipoBusqueda != null && !clasificacionTipoBusqueda.trim().equals("-1")) {
			// jpQL.append(" and d.id_clasificacion_tipo = ");
			// jpQL.append(Integer.parseInt(clasificacionTipoBusqueda));
			// jpQL.append(" ");
			// }
			// if (clasificacionSubtipoBusqueda != null && !clasificacionSubtipoBusqueda.trim().equals("-1")) {
			// jpQL.append(" and d.id_clasificacion_subtipo = ");
			// jpQL.append(Integer.parseInt(clasificacionSubtipoBusqueda));
			// jpQL.append(" ");
			// }
			// }
	
			if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
				jpQL.append(" and d.fechaDocumentoOrigen between :fechaInicioDocumentoBusqueda ");
				jpQL.append(" and :fechaTerminoDocumentoBusqueda");
	
			}
	
			logger.info("[filtrarEstadoBandejaSalida] SQL a ejecutar: " + jpQL);
	
			final Query query = em.createQuery(jpQL.toString());
	
			if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
	
				fechaInicioDocumentoBusqueda = FechaUtil.inicio(fechaInicioDocumentoBusqueda);
				fechaTerminoDocumentoBusqueda = FechaUtil.fin(fechaTerminoDocumentoBusqueda);
	
				query.setParameter(FECHA_INICIO_DOCUMENTO_BUSQUEDA, fechaInicioDocumentoBusqueda, TemporalType.TIMESTAMP);
				query.setParameter(FECHA_TERMINO_DOCUMENTO_BUSQUEDA, fechaTerminoDocumentoBusqueda, TemporalType.TIMESTAMP);
	
			}
			logger.info("[obtenerEstadoBandejaSalida] La consulta a ejecutar es: " + jpQL.toString());
			final List<Object[]> arrayObject = query.getResultList();
			this.procesarEstadoBandejaSalida(arrayObject);
			logger.info("[obtenerEstadoBandejaSalida] Tiempo total: " + (System.currentTimeMillis() - init));
		}
	}

	private void procesarEstadoBandejaSalida(List<Object[]> arrayObject) {
//		List<Expediente> expedientesHijos = null;
//		ExpedienteBandejaSalida bandejaSalida = null;
//		ArrayList<DocumentoExpediente> respuestas = null;
//		ArrayList<DocumentoExpediente> originales = null;
		estadoBandejaSalida = new ArrayList<ExpedienteBandejaSalida>();
		for (Object[] o : arrayObject) {
			ExpedienteBandejaSalida bandejaSalida = new ExpedienteBandejaSalida();
			//e.id
			//bandejaSalida.setId((Long)o[0]);
			//e.versionPadre.id 
			bandejaSalida.setId((Long)o[17]);
			//e.numeroExpediente
			bandejaSalida.setNumeroExpediente(o[1] != null ? (String)o[1] : "");
			//e.fechaIngreso
			bandejaSalida.setFechaIngresoBandeja((Date)o[2]);
			//e.fechaAcuseRecibo
			bandejaSalida.setFechaAcuseRecibo((Date)o[3]);
			//(case when e.fechaAcuseRecibo is not null then e.fechaDespacho else null end) as fechaDespacho
			bandejaSalida.setFechaDespacho((Date)o[4]);
			//e.copia
			bandejaSalida.setCopia((Boolean)o[5]);
			//e.reasignado
			bandejaSalida.setFechaReasignado((Date)o[6]);
			//(case when e.fechaAcuseRecibo is null then emi.nombres else emi_h.nombres end) as nombre
			//(case when e.fechaAcuseRecibo is null then emi.apellidoPaterno else emi_h.apellidoPaterno end) as apellido
			bandejaSalida.setDestinatario((o[7] != null ? ((String)o[7] + " "): "") + (o[8] != null ? (String)o[8] : ""));
			//d.id
			//d.numeroDocumento
			bandejaSalida.setNumeroDocumento(o[10] != null ? (String)o[10] : "");
			//td.descripcion
			bandejaSalida.setTipoDocumento(o[11] != null ? (String)o[11] : "");
			//fd.descripcion
			bandejaSalida.setFormatoDocumento(o[12] != null ? (String)o[12] : "");
			//d.reservado
			boolean reservado = false;
			if (o[13] != null) {
				reservado = (Boolean)o[13];
			}
			//d.emisor
			bandejaSalida.setEmisor(o[14] != null ? (String)o[14] : "");
			//d.fechaDocumentoOrigen
			bandejaSalida.setFechaDocumentoOrigen((Date)o[15]);
			//d.materia
			bandejaSalida.setMateria(reservado ? Messages.instance().get("materia_reservada") :(o[16] != null ? (String)o[16] : ""));
			//g.nombre
			if (o[18] != null) {
				bandejaSalida.setDestinatario((String)o[18]);
			}

//			// Long idExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
//			Long idExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
//			String numeroExp = (String) o[1];
//			Date fechaDespacho = (Date) o[2];
//			Date fechaIngreso = (Date) o[3];
//			String nombreEmisor = (String) o[4];
////			String apellidoPatEmisor = (String) o[5];
////			String apellidoMatEmisor = (String) o[6];
//
//			Expediente exp = new Expediente();
//			exp.setId((long) Math.ceil(idExp));
//			exp.setNumeroExpediente(numeroExp);
//
//			bandejaSalida = new ExpedienteBandejaSalida();
//			bandejaSalida.setNumeroExpediente(numeroExp);
//			bandejaSalida.setFechaIngreso(fechaIngreso);
//			bandejaSalida.setFechaDespacho(fechaDespacho);
//			bandejaSalida.setFechaIngresoBandeja(fechaDespacho);
//			bandejaSalida.setEmisor(nombreEmisor);
//			//bandejaSalida.setEmisor(nombreEmisor + " " + apellidoPatEmisor + " " + apellidoMatEmisor);
//
//			respuestas = new ArrayList<DocumentoExpediente>();
//			originales = new ArrayList<DocumentoExpediente>();
//			this.obtenerDatosUltimoDocumento(exp, bandejaSalida, respuestas, originales);
//			expedientesHijos = this.obtenerExpedientesHijos(exp.getId());
//			if (expedientesHijos.size() != 0) {
//				fechaDespacho = expedientesHijos.get(0).getFechaDespacho() != null ? expedientesHijos.get(0)
//						.getFechaDespacho() : expedientesHijos.get(0).getFechaDespachoHistorico();
//				bandejaSalida.setFechaDespacho(fechaDespacho);
//			}
//			bandejaSalida.setDatosUsuariosAsociados(this.obtenerDatosUsuariosAsociados(expedientesHijos, numeroExp));
			estadoBandejaSalida.add(bandejaSalida);
		}
//		Collections.sort(estadoBandejaSalida, new Comparator<ExpedienteBandejaSalida>() {
//			public int compare(ExpedienteBandejaSalida o1, ExpedienteBandejaSalida o2) {
//				return o2.getFechaIngreso().compareTo(o1.getFechaIngreso());
//			}
//		});
	}

	/*
	 * @SuppressWarnings("unchecked") private List<Expediente> obtenerExpedientesHijos(Expediente padre) { return
	 * padre.getExpedientesHijos(); }
	 */

	@SuppressWarnings("unchecked")
	private List<Expediente> obtenerExpedientesHijos(long idPadre) {
		List<Expediente> listExpHijos = new ArrayList<Expediente>();
		Expediente exp = null;
		StringBuilder sql = new StringBuilder("select e.id, e.numeroExpediente, e.destinatario.id, ");
		sql.append("p.nombres as nombres_dest, p.apellidoPaterno as ap_pat_des, p.apellidoMaterno as ap_mat_dest, ");
		sql.append("c.descripcion as desc_cargo_dest, u.descripcion as desc_unidad_dest, ");
		sql.append("e.destinatarioHistorico.id, ");
		sql.append("p2.nombres as nombres_hist, p2.apellidoPaterno as ap_pat_hist, p2.apellidoMaterno as ap_mat_hist, ");
		sql.append("c2.descripcion as desc_cargo_hist, u2.descripcion as desc_unidad_hist, ");
		sql.append("e.fechaAcuseRecibo, e.fechaDespachoHistorico, e.fechaDespacho, e.fechaIngreso, ");
		sql.append("d.descripcion, d2.descripcion ");
		sql.append("from Expediente e ");
		sql.append("left join e.destinatario p ");
		sql.append("left join p.cargo c ");
		sql.append("left join c.unidadOrganizacional u ");
		sql.append("left join u.departamento d ");
		sql.append("left join e.destinatarioHistorico p2  ");
		sql.append("left join p2.cargo c2 ");
		sql.append("left join c2.unidadOrganizacional u2  ");
		sql.append("left join u2.departamento d2  ");
		sql.append("where e.versionPadre.id= " + idPadre);

//		 logger.info("[obtenerExpedientesHijos] La consulta a ejecutar es: " +
//		 sql.toString());
		List<Object[]> arrayObject = em.createQuery(sql.toString()).getResultList();

		for (Object[] o : arrayObject) {
			exp = new Expediente();
			// Long idExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			Long idExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			String numeroExp = (String) o[1];
			// Long idDest = (((BigInteger) o[2] != null) ? ((BigInteger) o[2]).longValue() : null);
			Long idDest = (((Long) o[2] != null) ? ((Long) o[2]).longValue() : null);
			String nombresDest = (String) o[3];
			String apellidoPatDest = (String) o[4];
			String apellidoMatDest = (String) o[5];
			String descCargo = (String) o[6];
			String descUnidad = (String) o[7];
			// Long idDestHist = (((BigInteger) o[8] != null) ? ((BigInteger) o[8]).longValue() : null);
			Long idDestHist = (((Long) o[8] != null) ? ((Long) o[8]).longValue() : null);
			String nombresDestHist = (String) o[9];
			String apellidoPatDestHist = (String) o[10];
			String apellidoMatDestHist = (String) o[11];
			String descCargoHist = (String) o[12];
			String descUnidadHist = (String) o[13];
			Date fechaAcuseRecibo = (Date) o[14];
			Date fechaDespachoHist = (Date) o[15];
			Date fechaDespacho = (Date) o[16];
			Date fechaIngreso = (Date) o[17];
			String fiscaliaDest = (String) o[18];
			String fiscaliaDesHist = (String) o[19];

			exp.setId(idExp);
			exp.setNumeroExpediente(numeroExp);

			if (idDest != null) {
				Persona destinatario = new Persona();
				destinatario.setId(idDest);
				destinatario.setNombres(nombresDest);
				destinatario.setApellidoPaterno(apellidoPatDest);
				destinatario.setApellidoMaterno(apellidoMatDest);
				Cargo cargoDest = new Cargo();
				UnidadOrganizacional unidadDest = new UnidadOrganizacional();
				Departamento departamentoDest = new Departamento();
				departamentoDest.setDescripcion(fiscaliaDest);
				unidadDest.setDescripcion(descUnidad);
				cargoDest.setDescripcion(descCargo);
				unidadDest.setDepartamento(departamentoDest);
				cargoDest.setUnidadOrganizacional(unidadDest);
				destinatario.setCargo(cargoDest);
				
				exp.setDestinatario(destinatario);
			} else {
				Persona destinatarioHist = new Persona();
				destinatarioHist.setId(idDestHist);
				destinatarioHist.setNombres(nombresDestHist);
				destinatarioHist.setApellidoPaterno(apellidoPatDestHist);
				destinatarioHist.setApellidoMaterno(apellidoMatDestHist);
				Cargo cargoDestHist = new Cargo();
				UnidadOrganizacional unidadDestHist = new UnidadOrganizacional();
				Departamento departamentoHist = new Departamento();
				departamentoHist.setDescripcion(fiscaliaDesHist);
				unidadDestHist.setDescripcion(descUnidadHist);
				cargoDestHist.setDescripcion(descCargoHist);
				unidadDestHist.setDepartamento(departamentoHist);
				cargoDestHist.setUnidadOrganizacional(unidadDestHist);
				destinatarioHist.setCargo(cargoDestHist);
				
				exp.setDestinatarioHistorico(destinatarioHist);
			}

			exp.setFechaAcuseRecibo(fechaAcuseRecibo);
			exp.setFechaDespachoHistorico(fechaDespachoHist);
			exp.setFechaDespacho(fechaDespacho);
			exp.setFechaIngreso(fechaIngreso);

			listExpHijos.add(exp);
		}
		return listExpHijos;
	}

	private List<DatosUsuarioBandejaSalida> obtenerDatosUsuariosAsociados(List<Expediente> expedientes,
			String codigoExpedienteActual) {
		List<DatosUsuarioBandejaSalida> datosUsuarios = new ArrayList<DatosUsuarioBandejaSalida>();
		DatosUsuarioBandejaSalida usuario = null;
		for (Expediente expediente : expedientes) {
			if (expediente.getNumeroExpediente().equalsIgnoreCase(codigoExpedienteActual)) {
				usuario = new DatosUsuarioBandejaSalida();
				// if (!expediente.getFechaAcuseRecibo() == null) {
				if (expediente.getDestinatario() != null) {
					usuario.setDestinatarioCargo(expediente.getDestinatario().getCargo().getDescripcion());
					usuario.setDestinatarioUnidadOrganizacional(expediente.getDestinatario().getCargo()
							.getUnidadOrganizacional().getDescripcion());
					usuario.setDestinatarioUsuario(expediente.getDestinatario().getNombreApellido());
					usuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
					usuario.setFechaDespacho(expediente.getFechaDespachoHistorico());
					usuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
					usuario.setDestinatarioDepartamento(expediente.getDestinatario().getCargo().
								getUnidadOrganizacional().getDepartamento().getDescripcion());
				} else {
					if (expediente.getDestinatarioHistorico() != null) {
						usuario.setDestinatarioCargo(expediente.getDestinatarioHistorico().getCargo().getDescripcion());
						usuario.setDestinatarioUnidadOrganizacional(expediente.getDestinatarioHistorico().getCargo()
								.getUnidadOrganizacional().getDescripcion());
						usuario.setDestinatarioUsuario(expediente.getDestinatarioHistorico().getNombreApellido());
						usuario.setDestinatarioDepartamento(expediente.getDestinatarioHistorico().getCargo().
								getUnidadOrganizacional().getDepartamento().getDescripcion());
					}
					usuario.setFechaAcuseRecibo(expediente.getFechaAcuseRecibo());
					usuario.setFechaDespacho(expediente.getFechaDespachoHistorico());
					usuario.setFechaIngresoBandeja(expediente.getFechaIngreso());
				}
				if (expediente.getCopia() == null || !expediente.getCopia()) {
					usuario.setCopia("DIRECTO");
				} else {
					usuario.setCopia("COPIA");
				}
				datosUsuarios.add(usuario);
			}
		}
		logger.debug("[obtenerDatosUsuariosAsociados] El resultado de la consulta es: " + datosUsuarios);
		return datosUsuarios;
	}

	@SuppressWarnings("unchecked")
	private void obtenerDatosUltimoDocumento(Expediente expediente, ExpedienteBandeja bandejaSalida,
			ArrayList<DocumentoExpediente> respuestas, ArrayList<DocumentoExpediente> originales) {
		DocumentoExpediente documentoAuxiliar = null;

		StringBuilder jpQL = new StringBuilder(
				"select de.id as id_doc_exp, de.tipoDocumentoExpediente.id, td.descripcion as desc_td, ");
		jpQL.append("d.numeroDocumento, d.fechaDocumentoOrigen, d.materia, d.id as id_doc, ");
		jpQL.append("fd.descripcion as desc_td, d.cmsId ");
		jpQL.append("from DocumentoExpediente de, Expediente e, Documento d, ");
		jpQL.append("TipoDocumento td, FormatoDocumento fd ");
		jpQL.append("where e.id= de.expediente.id and de.documento.id= d.id ");
		jpQL.append("and d.tipoDocumento.id= td.id and d.formatoDocumento.id= fd.id ");
		jpQL.append("and e.numeroExpediente= '" + expediente.getNumeroExpediente() + "' ");

		// logger.info("[obtenerDatosUltimoDocumento] La consulta a ejecutar es:
		// " + jpQL.toString());
		List<Object[]> arrayObject = em.createQuery(jpQL.toString()).getResultList();
		Long idDocExp;
		Integer idTipoDocExp = null;
		String descTd = null;
		String numeroDoc = null;
		Date fechaDocOrigen = null;
		String materia = null;
		Long idDoc = null;
		String descFd = null;
		String cmsId = null;
		DocumentoExpediente docExp = null;
		Documento doc = null;

		for (Object[] o : arrayObject) {
			docExp = new DocumentoExpediente();
			doc = new Documento();
			// idDocExp = (((Long) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			idDocExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
			idTipoDocExp = (Integer) o[1];
			docExp.setId((long) idDocExp);
			descTd = (String) o[2];
			numeroDoc = (String) o[3];
			fechaDocOrigen = (Date) o[4];
			materia = (String) o[5];
			// idDoc = (((BigInteger) o[6] != null) ? ((BigInteger) o[6]).longValue() : null);
			idDoc = (((Long) o[6] != null) ? ((Long) o[6]).longValue() : null);
			descFd = (String) o[7];
			cmsId = (String) o[8];

			TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
			tipoDocExp.setId(idTipoDocExp);
			docExp.setTipoDocumentoExpediente(tipoDocExp);
			TipoDocumento tipoDoc = new TipoDocumento();
			tipoDoc.setDescripcion(descTd);
			doc.setTipoDocumento(tipoDoc);
			doc.setNumeroDocumento(numeroDoc);
			doc.setFechaDocumentoOrigen(fechaDocOrigen);
			doc.setMateria(materia);
			doc.setId(idDoc);
			FormatoDocumento formatoDoc = new FormatoDocumento();
			formatoDoc.setDescripcion(descFd);
			doc.setFormatoDocumento(formatoDoc);
			doc.setCmsId(cmsId);
			// (cmsId != null) ? new byte[1] : null);
			docExp.setDocumento(doc);

			if (idTipoDocExp.equals(TipoDocumentoExpediente.RESPUESTA)) {
				respuestas.add(docExp);
			}
		}
		if (respuestas.size() == 0) {
			for (Object[] o : arrayObject) {
				docExp = new DocumentoExpediente();
				doc = new Documento();
				// idDocExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
				idDocExp = (((Long) o[0] != null) ? ((Long) o[0]).longValue() : null);
				idTipoDocExp = (Integer) o[1];
				docExp.setId((long) idDocExp);
				descTd = (String) o[2];
				numeroDoc = (String) o[3];
				fechaDocOrigen = (Date) o[4];
				materia = (String) o[5];
				// idDoc = (((BigInteger) o[6] != null) ? ((BigInteger) o[6]).longValue() : null);
				idDoc = (((Long) o[6] != null) ? ((Long) o[6]).longValue() : null);
				descFd = (String) o[7];
				cmsId = (String) o[8];

				TipoDocumentoExpediente tipoDocExp = new TipoDocumentoExpediente();
				tipoDocExp.setId(idTipoDocExp);
				docExp.setTipoDocumentoExpediente(tipoDocExp);
				TipoDocumento tipoDoc = new TipoDocumento();
				tipoDoc.setDescripcion(descTd);
				doc.setTipoDocumento(tipoDoc);
				doc.setNumeroDocumento(numeroDoc);
				doc.setFechaDocumentoOrigen(fechaDocOrigen);
				doc.setMateria(materia);
				doc.setId(idDoc);
				FormatoDocumento formatoDoc = new FormatoDocumento();
				formatoDoc.setDescripcion(descFd);
				doc.setFormatoDocumento(formatoDoc);
				doc.setCmsId(cmsId);
				// (cmsId != null) ? new byte[1] : null);
				docExp.setDocumento(doc);

				if (idTipoDocExp.equals(TipoDocumentoExpediente.ORIGINAL)) {
					originales.add(docExp);
				}
			}
			if (originales.size() != 0) {
				Collections.sort(originales, new DocumentoExpedienteComparador());
				documentoAuxiliar = originales.get(0);
				bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
				bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
				bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
				bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
				bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
				bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento()
						.getDescripcion());
				bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
			}
		} else {
			Collections.sort(respuestas, new DocumentoExpedienteComparador());
			documentoAuxiliar = respuestas.get(0);
			bandejaSalida.setTipoDocumento(documentoAuxiliar.getDocumento().getTipoDocumento().getDescripcion());
			bandejaSalida.setNumeroDocumento(documentoAuxiliar.getDocumento().getNumeroDocumento());
			bandejaSalida.setFechaDocumentoOrigen(documentoAuxiliar.getDocumento().getFechaDocumentoOrigen());
			bandejaSalida.setMateria(documentoAuxiliar.getDocumento().getMateria());
			bandejaSalida.setIdDocumento(documentoAuxiliar.getDocumento().getId());
			bandejaSalida.setFormatoDocumento(documentoAuxiliar.getDocumento().getFormatoDocumento().getDescripcion());
			bandejaSalida.setTieneCmsId(documentoAuxiliar.getDocumento().getCmsId() != null);
		}
		respuestas = null;
		originales = null;
	}

	public String obtenerDocumentos() {
		reporteDocumentos.clear();
		return FWD_REPORTE_DOCUMENTOS;
	}

	public String obtenerDocumentosUnidadOrganizacional() {

		reporteDocumentos.clear();
		return FWD_REPORTE_DOCUMENTOS_UO;
	}

	public String obtenerDocumentosOficinaDePartes() {
		reporteDocumentos.clear();
		return FWD_REPORTE_DOCUMENTOS_OPARTES;
	}

	@SuppressWarnings("unchecked")
	public void filtrarDocumentos() {
		reporteDocumentos.clear();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
		StringBuilder jpQL = new StringBuilder("select doc.* from (SELECT d.idInterno ");
		jpQL.append(" FROM Documento d, DocumentoExpediente de, Expediente ex ");

		if (idDestinatario == null) {
			idDestinatario = -1L;
		}
		if (idEmisor == null) {
			idEmisor = -1L;
		}

		if (idDestinatario != -1) {
			jpQL.append(" , (SELECT e.id id_exp FROM Expediente e, Persona p, Cargo c ");
			jpQL.append(" WHERE ((e.destinatario.id = p.id or e.destinatarioHistorico.id = p.id) ");
			jpQL.append("  and p.cargo.id = c.id ");
			jpQL.append("  and c.unidadOrganizacional.id = ");
			jpQL.append(idDestinatario);
			jpQL.append(" )) tin");
		}

		if (idEmisor != -1) {
			jpQL.append(" , Persona pe, Cargo ca ");
		}

		jpQL.append(" WHERE ");

		jpQL.append(" d.id = de.documento.id ");
		jpQL.append(" and de.expediente.id = ex.id ");

		if (busquedaTipoDocumento > 0) {
			jpQL.append(" and (d.tipoDocumento.id = ");
			jpQL.append(busquedaTipoDocumento);
			jpQL.append(")   ");
		}
		if (busquedaFormatoDocumento > 0) {
			jpQL.append(" and (d.formatoDocumento.id = ");
			jpQL.append(busquedaFormatoDocumento);
			jpQL.append(")   ");
		}

		if (idEmisor != -1) {
			jpQL.append(" and ((ex.emisor.id = pe.id or ex.emisorHistorico.id = pe.id) ");
			jpQL.append(" and pe.cargo.id = ca.id ");
			jpQL.append(" and ca.unidadOrganizacional.id = ");
			jpQL.append(idEmisor);
			jpQL.append(" ) ");
			if (idDestinatario != -1) {
				jpQL.append(" and tin.id_exp = ex.id ");
			}
		}

		if (fechaInicioDocumentoBusqueda == null && fechaTerminoDocumentoBusqueda == null) {
			fechaInicioDocumentoBusqueda = new Date();
			fechaTerminoDocumentoBusqueda = fechaInicioDocumentoBusqueda;
			jpQL.append(" and (to_char(d.fecha_Creacion, 'yyyy-MM-dd') = '");
			jpQL.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda));
			jpQL.append("')");

		} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda == null) {
			fechaTerminoDocumentoBusqueda = fechaInicioDocumentoBusqueda;
			jpQL.append(" and (to_char(d.fecha_Creacion, 'yyyy-MM-dd') = '");
			jpQL.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda));
			jpQL.append("')");
		} else if (fechaInicioDocumentoBusqueda != null && fechaTerminoDocumentoBusqueda != null) {
			jpQL.append(" and (to_char(d.fecha_Creacion, 'yyyy-MM-dd') between '");
			jpQL.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda));
			jpQL.append("' and '");
			jpQL.append(simpleDateFormat.format(fechaTerminoDocumentoBusqueda));
			jpQL.append("')");
		}

		jpQL.append("group by d.id), Documento doc where idInterno = doc.id");
		logger.info("[filtrarDocumentos] La consulta a ejecutar es: " + jpQL);
		Query query = em.createQuery(jpQL.toString());
		List<Documento> documentos = query.getResultList();
		this.procesarDocumentos(documentos);
	}

	@SuppressWarnings("unchecked")
	public void filtrarExpedientesUnidadOrganizacional() {
		reporteDocumentos.clear();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		List<Object[]> expedientes = null;
		StringBuilder jpQL = new StringBuilder("SELECT e, de FROM Expediente as e join e.documentos as de  ");

		/**
		 * jpQL.append(" where ( ( TO_DATE(TO_CHAR(e.fechaIngreso, 'dd-MM-yyyy'),'dd-MM-yyyy') between TO_DATE('");
		 * jpQL.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda)); jpQL.append("','dd-MM-yyyy') ");
		 * jpQL.append(" and TO_DATE('"); jpQL.append(simpleDateFormat.format(fechaTerminoDocumentoBusqueda));
		 * jpQL.append("','dd-MM-yyyy') ) "); jpQL.append(" or ");
		 */
		jpQL.append("where ( TO_DATE(TO_CHAR(e.fechaDespacho, 'dd-MM-yyyy'),'dd-MM-yyyy') between TO_DATE('");
		jpQL.append(simpleDateFormat.format(fechaInicioDocumentoBusqueda));
		jpQL.append("','dd-MM-yyyy') ");
		jpQL.append(" and  TO_DATE('");
		jpQL.append(simpleDateFormat.format(fechaTerminoDocumentoBusqueda));
		jpQL.append("','dd-MM-yyyy') ) ");

		if (busquedaTipoDocumento != null && busquedaTipoDocumento > 0) {
			jpQL.append(" and (de.documento.tipoDocumento.id = ").append(busquedaTipoDocumento).append(") ");
		}

		if (busquedaFormatoDocumento != null && busquedaFormatoDocumento > 0) {
			jpQL.append(" and (de.documento.formatoDocumento.id = ").append(busquedaFormatoDocumento).append(") ");
		}

		if (personaEmisor != null && personaEmisor > 0) {
			jpQL.append(" and (e.emisor.id = ").append(personaEmisor).append(") ");
		} else if (cargoEmisor != null && cargoEmisor > 0) {
			jpQL.append(" and (e.emisor.cargo.id = ").append(cargoEmisor).append(") ");
		} else if (idUnidadOrganizacionalEmisor != null && idUnidadOrganizacionalEmisor > 0) {
			jpQL.append(" and (e.emisor.cargo.unidadOrganizacional.id = ").append(idUnidadOrganizacionalEmisor)
					.append(") ");
		} else if (regionEmisor != null && regionEmisor > 0) {
			jpQL.append(" and (e.emisor.cargo.unidadOrganizacional.region.id = ").append(regionEmisor).append(") ");
		}

		if (personaDestinatario != null && personaDestinatario > 0) {
			jpQL.append(" and (e.destinatario.id = ").append(personaDestinatario).append(") ");
		} else if (cargoDestinatario != null && cargoDestinatario > 0) {
			jpQL.append(" and (e.destinatario.cargo.id = ").append(cargoDestinatario).append(") ");
		} else if (idUnidadOrganizacionalDestinatario != null && idUnidadOrganizacionalDestinatario > 0) {
			jpQL.append(" and (e.destinatario.cargo.unidadOrganizacional.id = ")
					.append(idUnidadOrganizacionalDestinatario).append(") ");
		} else if (regionDestinatario != null && regionDestinatario > 0) {
			jpQL.append(" and (e.destinatario.cargo.unidadOrganizacional.region.id = ").append(regionDestinatario)
					.append(") ");
		}

		logger.info("busqueda expedientes u.o query = #0", jpQL.toString());

		Query query = em.createQuery(jpQL.toString());
		expedientes = query.getResultList();
		logger.info("query ejecutado");
		this.procesarDocumentosUnidadOrganizacional(expedientes);

	}

	private void procesarDocumentosUnidadOrganizacional(List<Object[]> rows) {
		for (Object[] r : rows) {
			ExpedienteBandejaSalida exp;
			Expediente e = (Expediente) r[0];
			DocumentoExpediente d = (DocumentoExpediente) r[1];
			Documento doc = d.getDocumento();

			exp = new ExpedienteBandejaSalida();
			exp.setId(e.getId());
			exp.setNumeroExpediente(e.getNumeroExpediente());
			exp.setFechaIngreso(e.getFechaIngreso());
			exp.setFechaDespacho(e.getFechaDespacho());
			exp.setTipoDocumento(doc.getTipoDocumento().getDescripcion());
			exp.setFormatoDocumento(doc.getFormatoDocumento().getDescripcion());
			exp.setFechaDocumentoOrigen(doc.getFechaDocumentoOrigen());
			exp.setNumeroDocumento(doc.getNumeroDocumento());
			// exp.setNumeroDocumentoInterno(doc.getNumeroDocumentoInterno());
			exp.setEmisor(doc.getEmisor());
			exp.setEmisorExpediente(e.getEmisor());
			if (e.getDestinatario() != null) exp.setDestinatarioExpediente(e.getDestinatario());
			else exp.setDestinatarioExpediente(e.getDestinatarioHistorico());
			exp.setMateria(doc.getReservado() ? "MATERIA RESERVADA" : doc.getMateria());

			reporteDocumentos.add(exp);
		}
	}

	@SuppressWarnings("unchecked")
	private void procesarDocumentos(List<Documento> documentos) {
		ExpedienteBandejaSalida bandejaSalida = null;
		for (Documento documento : documentos) {
			bandejaSalida = new ExpedienteBandejaSalida();
			bandejaSalida.setFechaIngreso(documento.getFechaCreacion());
			bandejaSalida.setNumeroDocumento(documento.getNumeroDocumento());
			bandejaSalida.setMateria(documento.getMateria());
			bandejaSalida.setFechaDocumentoOrigen(documento.getFechaDocumentoOrigen());
			bandejaSalida.setAutor(documento.getAutor().getNombres() + " " + documento.getAutor().getApellidoPaterno());
			bandejaSalida.setTipoDocumento(documento.getTipoDocumento().getDescripcion());
			bandejaSalida.setTieneCmsId(documento.getCmsId() != null);
			bandejaSalida.setIdDocumento(documento.getId());
			bandejaSalida.setEmisor(documento.getEmisor());
			bandejaSalida.setFormatoDocumento(documento.getFormatoDocumento().getDescripcion());
			StringBuilder sbDest = new StringBuilder();
			int destinatarios = documento.getDestinatarios().size();
			for (int i = 0; i < destinatarios; i++) {
				ListaPersonasDocumento p = documento.getDestinatarios().get(i);
				if (i + 1 == destinatarios) {
					sbDest.append(p.getDestinatario());
				} else {
					sbDest.append(p.getDestinatario() + ", ");
				}
			}
			bandejaSalida.setDestinatario(sbDest.toString());

			StringBuilder sb = new StringBuilder("SELECT de.expediente FROM DocumentoExpediente de ");
			sb.append("WHERE de.documento.id = '" + documento.getId() + "' ");
			sb.append("and de.expediente.versionPadre = null");
			Query q = em.createQuery(sb.toString());
			String numeroExpediente = null;
			// String numeroExpedienteOPartes = null;
			List<Expediente> numeros = q.getResultList();
			if (numeros != null) {
				if (numeros.size() == 1) {
					numeroExpediente = ((Expediente) numeros.get(0)).getNumeroExpediente();
					// numeroExpedienteOPartes = ((Expediente) numeros.get(0))
					// .getNumeroExpedienteOPartes();
				}
			}
			if (numeroExpediente == null) {
				numeroExpediente = this.obtenerNumeroExpedientePadre(documento);
			}
			bandejaSalida.setNumeroExpediente(numeroExpediente);
			// bandejaSalida.setNumeroExpedienteOPartes(numeroExpedienteOPartes);
			reporteDocumentos.add(bandejaSalida);
		}
	}

	private String obtenerNumeroExpedientePadre(Documento documento) {
		List<Expediente> expedientes = this.obtenerExpedientesAsociadosADocumento(documento);
		String numeroExpedientePadre = null;
		for (Expediente expediente : expedientes) {
			numeroExpedientePadre = expediente.getNumeroExpediente();
			break;
		}
		return numeroExpedientePadre;
	}

	public void exportarReporteDeDocumentosOficinaPartesAExcel() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", locale);

		HSSFWorkbook libroTrabajoReporte = new HSSFWorkbook();
		HSSFSheet primera_hoja = libroTrabajoReporte.createSheet("Reporte Documentos");

		HSSFFont negritas = libroTrabajoReporte.createFont();
		negritas.setFontHeightInPoints((short) 8);
		negritas.setFontName("Times New Roman");
		negritas.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		HSSFCellStyle estiloCeldaCabecera = libroTrabajoReporte.createCellStyle();
		estiloCeldaCabecera.setFont(negritas);
		estiloCeldaCabecera.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		estiloCeldaCabecera.setWrapText(true);
		estiloCeldaCabecera.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		estiloCeldaCabecera.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		estiloCeldaCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		HSSFFont normal = libroTrabajoReporte.createFont();
		normal.setFontHeightInPoints((short) 8);
		normal.setFontName("Times New Roman");
		normal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

		HSSFCellStyle estiloCeldaCuerpo = libroTrabajoReporte.createCellStyle();
		estiloCeldaCuerpo.setFont(normal);
		estiloCeldaCuerpo.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		estiloCeldaCuerpo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setWrapText(true);
		estiloCeldaCuerpo.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

		HSSFCell celda = null;
		HSSFRow row = primera_hoja.createRow((short) 0);
		for (int i = 0; i < CAMPOS_OPARTES.length; i++) {
			primera_hoja.setColumnWidth((short) i, (short) (256 * 10));
			celda = row.createCell((short) i);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellStyle(estiloCeldaCabecera);
			celda.setCellValue(CAMPOS_OPARTES[i]);
		}
		ExpedienteBandejaSalida bandejaSalida = null;
		for (int j = 0; j < reporteDocumentos.size(); j++) {
			row = primera_hoja.createRow((short) j + 1);
			bandejaSalida = reporteDocumentos.get(j);

			celda = row.createCell((short) 0);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(simpleDateTimeFormat.format(bandejaSalida.getFechaIngreso()));

			celda = row.createCell((short) 1);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getEmisor());

			celda = row.createCell((short) 2);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getNumeroDocumento());

			celda = row.createCell((short) 3);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(simpleDateFormat.format(bandejaSalida.getFechaDocumentoOrigen()));

			celda = row.createCell((short) 4);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getMateria());
		}
		try {

			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename=" + "Reporte_de_documentos_"
					+ simpleDateFormat.format(new Date()) + ".xls");
			ServletOutputStream out = response.getOutputStream();
			libroTrabajoReporte.write(out);

			out.write(libroTrabajoReporte.getBytes());

			out.flush();
			facesContext.responseComplete();

		} catch (IOException e) {

		}
	}

	public void exportarReporteDocumentosExcel() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", locale);
		SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", locale);

		HSSFWorkbook libroTrabajoReporte = new HSSFWorkbook();
		HSSFSheet primera_hoja = libroTrabajoReporte.createSheet("Reporte Documentos");

		HSSFFont negritas = libroTrabajoReporte.createFont();
		negritas.setFontHeightInPoints((short) 8);
		negritas.setFontName("Times New Roman");
		negritas.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		HSSFCellStyle estiloCeldaCabecera = libroTrabajoReporte.createCellStyle();
		estiloCeldaCabecera.setFont(negritas);
		estiloCeldaCabecera.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		estiloCeldaCabecera.setWrapText(true);
		estiloCeldaCabecera.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		estiloCeldaCabecera.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		estiloCeldaCabecera.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
		estiloCeldaCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		HSSFFont normal = libroTrabajoReporte.createFont();
		normal.setFontHeightInPoints((short) 8);
		normal.setFontName("Times New Roman");
		normal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

		HSSFCellStyle estiloCeldaCuerpo = libroTrabajoReporte.createCellStyle();
		estiloCeldaCuerpo.setFont(normal);
		estiloCeldaCuerpo.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		estiloCeldaCuerpo.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estiloCeldaCuerpo.setWrapText(true);
		estiloCeldaCuerpo.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);

		HSSFCell celda = null;
		HSSFRow row = primera_hoja.createRow((short) 0);
		for (int i = 0; i < CAMPOS.length; i++) {
			primera_hoja.setColumnWidth((short) i, (short) (256 * 10));
			celda = row.createCell((short) i);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellStyle(estiloCeldaCabecera);
			celda.setCellValue(CAMPOS[i]);
		}
		ExpedienteBandejaSalida bandejaSalida = null;
		for (int j = 0; j < reporteDocumentos.size(); j++) {
			row = primera_hoja.createRow((short) j + 1);
			bandejaSalida = reporteDocumentos.get(j);

			celda = row.createCell((short) 0);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(simpleDateTimeFormat.format(bandejaSalida.getFechaIngreso()));

			celda = row.createCell((short) 1);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getNumeroExpediente());

			celda = row.createCell((short) 2);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getTipoDocumento());

			celda = row.createCell((short) 3);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getNumeroDocumento());

			celda = row.createCell((short) 4);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getEmisor());

			celda = row.createCell((short) 5);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getDestinatario());

			celda = row.createCell((short) 6);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(simpleDateFormat.format(bandejaSalida.getFechaDocumentoOrigen()));

			celda = row.createCell((short) 7);
			celda.setCellType(HSSFCell.CELL_TYPE_STRING);
			celda.setCellStyle(estiloCeldaCuerpo);
			celda.setEncoding(HSSFCell.ENCODING_UTF_16);
			celda.setCellValue(bandejaSalida.getMateria());
		}
		try {

			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment; filename=" + "Reporte_de_documentos_"
					+ simpleDateFormat.format(new Date()) + ".xls");
			ServletOutputStream out = response.getOutputStream();
			libroTrabajoReporte.write(out);

			out.write(libroTrabajoReporte.getBytes());

			out.flush();
			facesContext.responseComplete();

		} catch (IOException e) {

		}
	}

	public Locale getLocale() {
		return locale;
	}

	public List<ExpedienteBandejaSalida> getEstadoBandejaSalida() {
		return estadoBandejaSalida;
	}

	public void setEstadoBandejaSalida(List<ExpedienteBandejaSalida> estadoBandejaSalida) {
		this.estadoBandejaSalida = estadoBandejaSalida;
	}

	public List<ExpedienteBandejaEntrada> getHistorialBandejaEntrada() {
		return historialBandejaEntrada;
	}

	public void setHistorialBandejaEntrada(List<ExpedienteBandejaEntrada> historialBandejaEntrada) {
		this.historialBandejaEntrada = historialBandejaEntrada;
	}

	public List<ExpedienteBandejaEntrada> getDocumentoEnBandejaEntrada() {
		return documentoEnBandejaEntrada;
	}

	public void setDocumentoEnBandejaEntrada(List<ExpedienteBandejaEntrada> documentoEnBandejaEntrada) {
		this.documentoEnBandejaEntrada = documentoEnBandejaEntrada;
	}

	public Persona getUsuario() {
		return usuario;
	}

	public void setUsuario(Persona usuario) {
		this.usuario = usuario;
	}

	public String getTipoDocumentoBusqueda() {
		return tipoDocumentoBusqueda;
	}

	public void setTipoDocumentoBusqueda(String tipoDocumentoBusqueda) {
		this.tipoDocumentoBusqueda = tipoDocumentoBusqueda;
	}

	public String getNumeroDocumentoBusqueda() {
		return numeroDocumentoBusqueda;
	}

	public void setNumeroDocumentoBusqueda(String numeroDocumentoBusqueda) {
		this.numeroDocumentoBusqueda = numeroDocumentoBusqueda.trim();
	}

	public Date getFechaInicioDocumentoBusqueda() {
		return fechaInicioDocumentoBusqueda;
	}

	public void setFechaInicioDocumentoBusqueda(Date fechaInicioDocumentoBusqueda) {
		this.fechaInicioDocumentoBusqueda = fechaInicioDocumentoBusqueda;
	}

	public Date getFechaTerminoDocumentoBusqueda() {
		return fechaTerminoDocumentoBusqueda;
	}

	public void setFechaTerminoDocumentoBusqueda(Date fechaTerminoDocumentoBusqueda) {
		this.fechaTerminoDocumentoBusqueda = fechaTerminoDocumentoBusqueda;
	}

	public String getEmisorDocumentoBusqueda() {
		return emisorDocumentoBusqueda;
	}

	public void setEmisorDocumentoBusqueda(String emisorDocumentoBusqueda) {
		this.emisorDocumentoBusqueda = emisorDocumentoBusqueda;
	}

	public String getMateriaDocumentoBusqueda() {
		return materiaDocumentoBusqueda;
	}

	public void setMateriaDocumentoBusqueda(String materiaDocumentoBusqueda) {
		this.materiaDocumentoBusqueda = materiaDocumentoBusqueda;
	}

	public String getNumeroExpedienteBusqueda() {
		return numeroExpedienteBusqueda;
	}

	public void setNumeroExpedienteBusqueda(String numeroExpedienteBusqueda) {
		this.numeroExpedienteBusqueda = numeroExpedienteBusqueda.trim();
	}

	public List<ExpedienteBandejaSalida> getDocumentosRelacionados() {
		return documentosRelacionados;
	}

	public void setDocumentosRelacionados(List<ExpedienteBandejaSalida> documentosRelacionados) {
		this.documentosRelacionados = documentosRelacionados;
	}

	public List<ExpedienteBandejaSalida> getHistorialDocumento() {
		return historialDocumento;
	}

	public void setHistorialDocumento(List<ExpedienteBandejaSalida> historialDocumento) {
		this.historialDocumento = historialDocumento;
	}

	public List<ExpedienteBandejaSalida> getRutaSeguidaDocumento() {
		return rutaSeguidaDocumento;
	}

	public void setRutaSeguidaDocumento(List<ExpedienteBandejaSalida> rutaSeguidaDocumento) {
		this.rutaSeguidaDocumento = rutaSeguidaDocumento;
	}

	public List<ExpedienteBandejaSalida> getReporteDocumentos() {
		return reporteDocumentos;
	}

	public void setReporteDocumentos(List<ExpedienteBandejaSalida> reporteDocumentos) {
		this.reporteDocumentos = reporteDocumentos;
	}

	public String getDestinatarioDocumentoBusqueda() {
		return destinatarioDocumentoBusqueda;
	}

	public void setDestinatarioDocumentoBusqueda(String destinatarioDocumentoBusqueda) {
		this.destinatarioDocumentoBusqueda = destinatarioDocumentoBusqueda;
	}

	public List<ExpedienteBandejaSalida> getDocumentosCierre() {
		return documentosCierre;
	}

	@Destroy
	@Remove
	public void destroy() {

	}

	public Long getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(Long idEmisor) {
		this.idEmisor = idEmisor;
	}

	public Long getIdDestinatario() {
		return idDestinatario;
	}

	public void setIdDestinatario(Long idDestinatario) {
		this.idDestinatario = idDestinatario;
	}

	public List<SelectItem> getUnidadesOrganizacionales() {
		if (unidadesOrganizacionales == null) {
			setUnidadesOrganizacionales();
		}
		return unidadesOrganizacionales;
	}

	public void verDocumento(Long idDocumento) {
		Documento doc = em.find(Documento.class, idDocumento);
		logger.info("busca doc=#0", idDocumento);
		if (doc == null) return;
		logger.info("doc encontrado = #0", doc.getId());
		String contentType;
		String nombreArchivo;
		String cmsId = null;
		if (doc instanceof DocumentoBinario) {
			ArchivoDocumentoBinario archivo = ((DocumentoBinario) doc).getArchivo();
			if (archivo == null) return;
			contentType = archivo.getContentType();
			nombreArchivo = archivo.getNombreArchivo().replace(' ', '_');
			cmsId = archivo.getCmsId();
		} else if (doc instanceof DocumentoElectronico) {
			contentType = "text/xml";
			nombreArchivo = "documento electronico";
			cmsId = doc.getCmsId();
		} else return;

		logger.info("busca cmsId = #0", cmsId);
		if (cmsId == null) return;

		// byte[] data = repositorio.recuperarArchivo(cmsId);
		byte[] data = null;
		try {
			data = repositorio.getFile(cmsId);
		} catch (XPathExpressionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (data != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			try {
				HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
				response.setContentType(contentType);
				response.setHeader("Content-disposition", "attachment; filename=\"" + nombreArchivo + "\"");
				response.getOutputStream().write(data);
				response.getOutputStream().flush();
				facesContext.responseComplete();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		FacesMessages.instance().add(
				"No existe el archivo, consulte con el administrador. (Codigo Archivo: " + doc.getId() + ")");
	}

	@EJB
	private JerarquiasLocal jerarquias;

	private Long regionEmisor = JerarquiasLocal.INICIO;
	private Long regionDestinatario = JerarquiasLocal.INICIO;

	private List<SelectItem> listRegionesEmisor;
	private List<SelectItem> listRegionesDestinatario;
	private List<SelectItem> listUnidadesOrganizacionalesEmisor;
	private List<SelectItem> listCargosEmisor;
	private List<SelectItem> listPersonasEmisor;
	private List<SelectItem> listCargosDestinatario;
	private List<SelectItem> listPersonasDestinatario;
	private List<SelectItem> listUnidadesOrganizacionalesDestinatario;

	private Integer busquedaFormatoDocumento;

	private Integer busquedaTipoDocumento;

	public void buscarUnidadesOrganizacionalesEmisor() {
		listUnidadesOrganizacionalesEmisor = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				regionEmisor);
		idUnidadOrganizacionalEmisor = null;
		personaEmisor = null;
		cargoEmisor = null;
	}

	public void buscarCargosEmisor() {
		listCargosEmisor = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL, this.idUnidadOrganizacionalEmisor);
		personaEmisor = null;
		cargoEmisor = null;
	}

	public void buscarCargosDestinatario() {
		listCargosDestinatario = jerarquias.getCargos(JerarquiasLocal.TEXTO_INICIAL,
				this.idUnidadOrganizacionalDestinatario);
		personaDestinatario = null;
		cargoDestinatario = null;
	}

	public Persona buscarDestinatario(Long id) {
		return jerarquias.getPersona(id);
	}

	public Persona buscarEmisor(Long id) {
		return jerarquias.getPersona(id);
	}

	public void buscarPersonasEmisor() {
		listPersonasEmisor = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargoEmisor);
		personaEmisor = null;
		if (listPersonasEmisor.size() == 1) {
			personaEmisor = (Long) listPersonasEmisor.get(0).getValue();
		}
	}

	public void buscarPersonasDestinatario() {
		listPersonasDestinatario = jerarquias.getPersonas(JerarquiasLocal.TEXTO_INICIAL, cargoDestinatario);
		personaDestinatario = null;
		if (listPersonasDestinatario.size() == 1) {
			personaDestinatario = (Long) listPersonasDestinatario.get(0).getValue();
		}
	}

	public void buscarUnidadesOrganizacionalesDestinatario() {
		listUnidadesOrganizacionalesDestinatario = jerarquias.getUnidadesOrganzacionales(JerarquiasLocal.TEXTO_INICIAL,
				regionDestinatario);
		idUnidadOrganizacionalDestinatario = null;
		cargoDestinatario = null;
		personaDestinatario = null;

	}

	public Long getRegionEmisor() {
		return regionEmisor;
	}

	public Long getRegionDestinatario() {
		return regionDestinatario;
	}

	public List<SelectItem> getRegionesEmisor() {
		if (listRegionesEmisor.size() == 0) listRegionesEmisor = buscarRegiones();
		return listRegionesEmisor;
	}

	public List<SelectItem> getRegionesDestinatario() {
		if (listRegionesDestinatario.size() == 0) listRegionesDestinatario = buscarRegiones();
		return listRegionesDestinatario;
	}

	public List<SelectItem> getUnidadesOrganizacionalesEmisor() {
		return listUnidadesOrganizacionalesEmisor;
	}

	public List<SelectItem> getUnidadesOrganizacionalesDestinatario() {
		return listUnidadesOrganizacionalesDestinatario;
	}

	public void setRegionEmisor(Long idRegion) {
		regionEmisor = idRegion;
	}

	public void setRegionDestinatario(Long idRegion) {
		regionDestinatario = idRegion;
	}

	private List<SelectItem> buscarRegiones() {
		return null; // jerarquias.getRegiones(JerarquiasLocal.TEXTO_INICIAL);
	}

	public List<ExpedienteBandejaSalida> getDocumentosEncontrados() {
		return documentosEncontrados;
	}

	@Factory(value = "documentosEncontrados", scope = ScopeType.SESSION)
	public String menuBuscarDocumentos() {
		documentosEncontrados.clear();
		return FWD_REPORTE_BUSCAR_DOCUMENTOS;
	}

	public String getTimeZone() {
		return TIMEZONE;
	}

	public List<SelectItem> getListaFormatoDocumento() {
		return FormatoDocumentosList.getInstance(em).getFormatosDocumentoList("-- Seleccion Formato Documento --");
	}

	public List<SelectItem> getListaTipoDocumento() {
		return TipoDocumentosList.getInstanceTipoDocumentoList(em).getTiposDocumentoList(
				"-- Seleccione Tipo Documento --");

	}

	public Integer getBusquedaFormatoDocumento() {
		return busquedaFormatoDocumento;
	}

	public Integer getBusquedaTipoDocumento() {
		return busquedaTipoDocumento;
	}

	public void setBusquedaFormatoDocumento(Integer formatoDoc) {
		logger.info("busqueda formato documento por #0", formatoDoc);
		busquedaFormatoDocumento = formatoDoc;

	}

	public void setBusquedaTipoDocumento(Integer tipoDoc) {
		logger.info("busqueda tipo documento por #0", tipoDoc);
		busquedaTipoDocumento = tipoDoc;
	}

	private Long cargoEmisor = JerarquiasLocal.INICIO;
	private Long personaEmisor = JerarquiasLocal.INICIO;
	private Long cargoDestinatario = JerarquiasLocal.INICIO;
	private Long personaDestinatario = JerarquiasLocal.INICIO;

	public Long getCargoEmisor() {
		return cargoEmisor;
	}

	public void setPersonaEmisor(Long persona) {
		this.personaEmisor = persona;
	}

	public Long getCargoDestinatario() {
		return cargoDestinatario;
	}

	public void setCargoDestinatario(Long cargo) {
		cargoDestinatario = cargo;
	}

	public Long getPersonaEmisor() {
		return personaEmisor;
	}

	public void setCargoEmisor(Long cargo) {
		this.cargoEmisor = cargo;
	}

	public Long getPersonaDestinatario() {
		return personaDestinatario;
	}

	public void setPersonaDestinatario(Long persona) {
		personaDestinatario = persona;
	}

	private Long idUnidadOrganizacionalEmisor = JerarquiasLocal.INICIO;
	private Long idUnidadOrganizacionalDestinatario = JerarquiasLocal.INICIO;

	public Long getIdUnidadOrganizacionalEmisor() {
		return idUnidadOrganizacionalEmisor;
	}

	public void setIdUnidadOrganizacionalEmisor(Long id) {
		this.idUnidadOrganizacionalEmisor = id;
	}

	public Long getIdUnidadOrganizacionalDestinatario() {
		return idUnidadOrganizacionalDestinatario;
	}

	public void setIdUnidadOrganizacionalDestinatario(Long id) {
		this.idUnidadOrganizacionalDestinatario = id;
	}

	public List<SelectItem> getCargosEmisor() {
		return listCargosEmisor;
	}

	public List<SelectItem> getPersonasEmisor() {
		return listPersonasEmisor;
	}

	public List<SelectItem> getCargosDestinatario() {
		return listCargosDestinatario;
	}

	public List<SelectItem> getPersonasDestinatario() {
		return listPersonasDestinatario;
	}

	private String numeroInternoDocumentoBusqueda;

	public String getNumeroInternoDocumentoBusqueda() {
		return numeroInternoDocumentoBusqueda;
	}

	public void setNumeroInternoDocumentoBusqueda(String numeroDocumentoBusqueda) {
		numeroInternoDocumentoBusqueda = numeroDocumentoBusqueda;
	}

	public String getNombreArchivoBusqueda() {
		return nombreArchivoBusqueda;
	}

	public void setNombreArchivoBusqueda(String nombreArchivoBusqueda) {
		this.nombreArchivoBusqueda = nombreArchivoBusqueda.trim();
	}

	public Long getCargo() {
		return cargo;
	}

	public void setCargo(Long cargo) {
		this.cargo = cargo;
	}

	public Long getDivision() {
		return division;
	}

	public void setDivision(Long division) {
		this.division = division;
	}

	public Long getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Long departamento) {
		this.departamento = departamento;
	}

	public Long getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(Long unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(Long persona) {
		this.persona = persona;
	}

	public void setUnidadesOrganizacionales(List<SelectItem> unidadesOrganizacionales) {
		this.unidadesOrganizacionales = unidadesOrganizacionales;
	}

	public List<SelectItem> getListCargos() {
		return listCargos;
	}

	public void setListCargos(List<SelectItem> listCargos) {
		this.listCargos = listCargos;
	}

	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	public void setListDepartamento(List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	public void setListDivision(List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	public void setListPersonas(List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	public List<SelectItem> getListUnidadesOrganizacionales() {
		return listUnidadesOrganizacionales;
	}

	public void setListUnidadesOrganizacionales(List<SelectItem> listUnidadesOrganizacionales) {
		this.listUnidadesOrganizacionales = listUnidadesOrganizacionales;
	}

	// public void esFactura() {
	// factura = false;
	// if (this.tipoDocumentoBusqueda != null) {
	// int idTipoDoc = Integer.parseInt(this.tipoDocumentoBusqueda);
	// if (idTipoDoc == TipoDocumento.FACTURA) {
	// factura = true;
	// }
	// }
	// }

	// public void buscarClasificacionesVerificarFactura() {
	// esFactura();
	// buscarClasificaciones();
	// }

	public boolean isFactura() {
		return factura;
	}

	public void setFactura(boolean factura) {
		this.factura = factura;
	}

	public Integer getNumeroOrdenDeCompraBusqueda() {
		return numeroOrdenDeCompraBusqueda;
	}

	public void setNumeroOrdenDeCompraBusqueda(Integer numeroOrdenDeCompraBusqueda) {
		this.numeroOrdenDeCompraBusqueda = numeroOrdenDeCompraBusqueda;
	}

	public String getRutProveedorBusqueda() {
		return rutProveedorBusqueda;
	}

	public void setRutProveedorBusqueda(String rutProveedorBusqueda) {
		this.rutProveedorBusqueda = rutProveedorBusqueda;
	}

	public String getClasificacionTipoBusqueda() {
		return clasificacionTipoBusqueda;
	}

	public void setClasificacionTipoBusqueda(String clasificacionTipoBusqueda) {
		this.clasificacionTipoBusqueda = clasificacionTipoBusqueda;
	}

	public String getClasificacionSubtipoBusqueda() {
		return clasificacionSubtipoBusqueda;
	}

	public void setClasificacionSubtipoBusqueda(String clasificacionSubtipoBusqueda) {
		this.clasificacionSubtipoBusqueda = clasificacionSubtipoBusqueda;
	}

	public int getFlagMostrarClasificaciones() {
		return flagMostrarClasificaciones;
	}

	public void setFlagMostrarClasificaciones(int flagMostrarClasificaciones) {
		this.flagMostrarClasificaciones = flagMostrarClasificaciones;
	}

	public List<SelectItem> getListClasificacionTipo() {
		return listClasificacionTipo;
	}

	public void setListClasificacionTipo(List<SelectItem> listClasificacionTipo) {
		this.listClasificacionTipo = listClasificacionTipo;
	}

	public List<SelectItem> getListClasificacionSubtipo() {
		return listClasificacionSubtipo;
	}

	public void setListClasificacionSubtipo(List<SelectItem> listClasificacionSubtipo) {
		this.listClasificacionSubtipo = listClasificacionSubtipo;
	}

	public String getUrlFirma() {
		
		  Properties defaultProps = new Properties();
	        try {
	            defaultProps.load(getArchivoStream("customizacion.properties"));
	        }
	        catch (Exception e) {
	            logger.error("No se pudo cargar archivo de configuracion customizacion.properties");
	        }

			final String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + STRING;
		//String urlDocumento = "http://" + System.getProperty("jboss.bind.address") + ":" + "8080" + "/";
		return urlDocumento;
	}
	
	private InputStream getArchivoStream(String nombre) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        return loader.getResourceAsStream(nombre);
	}

	public Long getOrganizacion() {
		return organizacion;
	}

	public void setOrganizacion(Long organizacion) {
		this.organizacion = organizacion;
	}

	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	public void setListOrganizacion(List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	public String sqlRutaSeguidaPorDocumentoDestinatario() {

		StringBuilder sql = new StringBuilder("SELECT "); 
		sql.append("d.id, d.reservado, e.fechaAcuseRecibo, e.fechaIngreso, ");
		sql.append("e.fechaDespacho, e.fechaDespachoHistorico, "); 
		sql.append("e.destinatario.id, e.destinatarioHistorico.id, ");
		sql.append("td.descripcion as desc_tipodoc, d.numeroDocumento, d.emisor, d.fechaDocumentoOrigen, ");
		sql.append("d.materia, fd.descripcion as desc_fd, d.cmsId, e.numeroExpediente, p.nombres as nombre_autor, "); 
		sql.append("p.apellidoPaterno as apellido_paterno_autor, p.apellidoMaterno as apellido_materno_autor, ");
		sql.append("p_dest.nombres as nombre_destinatario, p_dest.apellidoPaterno as apellido_paterno_destinatario, ");
		sql.append("p_dest.apellidoMaterno as apellido_materno_destinatario, "); 
		sql.append("c.descripcion as desc_cargo, u.descripcion as desc_unidad, ");
		sql.append("e.archivado, a.nombreArchivo, '','',d.fechaCreacion, e.versionPadre.id, "); 
		sql.append("p_origen.nombres as nombreOrigen, p_origen.apellidoPaterno as apellidoOrigen, ");
		sql.append("p_origen_historico.nombres as nombreDestino, ");
		sql.append("p_origen_historico.apellidoPaterno as apellidoDestino, ");
		sql.append("e.id as id_expediente, e.reasignado, e.archivadoHistorico ");
		sql.append("from Expediente e ");
		sql.append("inner join e.documentos de ");
		sql.append("inner join de.documento d ");
		sql.append("inner join d.tipoDocumento td ");
		sql.append("inner join d.formatoDocumento fd ");
		sql.append("inner join e.destinatario p_dest ");
		sql.append("inner join e.versionPadre ep ");
		sql.append("inner join ep.documentos dep ");
		sql.append("left join d.autor p ");
		sql.append("left join d.archivo a ");
		sql.append("left join p_dest.cargo c ");
		sql.append("left join c.unidadOrganizacional u ");
		sql.append("left join e.emisor p_origen ");
		sql.append("left join e.emisorHistorico p_origen_historico ");
		return sql.toString();
	}

	public String sqlRutaSeguidaPorDocumentoDestinatarioHistorico() {
		
		StringBuilder sql = new StringBuilder("SELECT ");
		sql.append("d.id, d.reservado, e.fechaAcuseRecibo, e.fechaIngreso, e.fechaDespacho, ");
		sql.append("e.fechaDespachoHistorico, e.destinatario.id, e.destinatarioHistorico.id, ");
		sql.append("td.descripcion as desc_tipodoc, d.numeroDocumento, d.emisor, d.fechaDocumentoOrigen, ");
		sql.append("d.materia, fd.descripcion as desc_fd, d.cmsId, e.numeroExpediente, p.nombres as nombre_autor, ");
		sql.append("p.apellidoPaterno as apellido_paterno_autor, p.apellidoMaterno as apellido_materno_autor, ");
		sql.append("p_dest.nombres as nombre_destinatario, p_dest.apellidoPaterno as apellido_paterno_destinatario, ");
		sql.append("p_dest.apellidoMaterno as apellido_materno_destinatario, c.descripcion as desc_cargo, ");
		sql.append("u.descripcion as desc_unidad, e.archivado, a.nombreArchivo, '','', d.fechaCreacion, ");
		sql.append("e.versionPadre.id, p_origen.nombres as nombreOrigen, p_origen.apellidoPaterno as apellidoOrigen, ");
		sql.append("p_origen_historico.nombres as nombreDestino, ");
		sql.append("p_origen_historico.apellidoPaterno as apellidoDestino, ");
		sql.append("e.id as id_expediente, e.reasignado, e.archivadoHistorico ");
		sql.append("from Expediente e ");
		sql.append("inner join e.documentos de ");
		sql.append("inner join de.documento d "); 
		sql.append("inner join d.tipoDocumento td "); 
		sql.append("inner join d.formatoDocumento fd ");
		sql.append("inner join e.destinatarioHistorico p_dest ");
		sql.append("inner join e.versionPadre ep ");
		sql.append("inner join ep.documentos dep "); 
		sql.append("left join d.autor p "); 
		sql.append("left join d.archivo a ");  
		sql.append("left join p_dest.cargo c "); 
		sql.append("left join c.unidadOrganizacional u "); 
		sql.append("left join e.emisor p_origen "); 
		sql.append("left join e.emisorHistorico p_origen_historico "); 
		return sql.toString();
	}

}
