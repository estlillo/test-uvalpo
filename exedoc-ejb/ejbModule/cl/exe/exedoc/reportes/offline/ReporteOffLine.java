package cl.exe.exedoc.reportes.offline;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.reportes.offline.estructura.OffLine;

/**
 * @author Ricardo Fuentes
 */
@Local
public interface ReporteOffLine {

	/**
	 * Metodo que inicia la converzacion.
	 * 
	 * @param ingresado {@link Boolean}
	 * @param cancelado {@link Boolean}
	 * @param archivado {@link Boolean}
	 * @param despachado {@link Boolean}
	 * @return {@link String}
	 */
	String begin(final Boolean archivado, final Boolean cancelado, final Boolean ingresado, final Boolean despachado);

	/**
	 * Metodo que finaliza la converzacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	/**
	 * Metodo que destruye la converzacion.
	 */
	void destroy();

	/**
	 * Metodo que retorna el titulo para mostrar en el panel del filtro.
	 * 
	 * @return {@link String}
	 */
	String getTituloFiltro();

	/**
	 * Metodo que setea el titulo para mostrar en el panel del filtro.
	 * 
	 * @param tituloFiltro {@link String}
	 */
	void setTituloFiltro(final String tituloFiltro);

	/**
	 * @return {@link Locale}
	 */
	Locale getLocale();

	/**
	 * @return {@link Date}
	 */
	Date getFechaInicio();

	/**
	 * @param fecha {@link Date}
	 */
	void setFechaInicio(Date fecha);

	/**
	 * @return {@link Date}
	 */
	Date getFechaTermino();

	/**
	 * @param fechaTermino {@link Date}
	 */
	void setFechaTermino(Date fechaTermino);

	/**
	 * @return {@link String}
	 */
	String getTituloFiltroDos();

	/**
	 * @param tituloFiltroDos {@link String}
	 */
	void setTituloFiltroDos(String tituloFiltroDos);

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion {@link Long}
	 */
	void setOrganizacion(Long organizacion);

	/**
	 * Metodo que busca las divisiones segun la organizacion.
	 */
	void buscarDivisiones();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivision();

	/**
	 * @param listDivision {@link List} of {@link SelectItem}
	 */
	void setListDivision(List<SelectItem> listDivision);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion {@link List} of {@link SelectItem}
	 */
	void setListOrganizacion(List<SelectItem> listOrganizacion);

	/**
	 * @return {@link Long}
	 */
	Long getDivision();

	/**
	 * @param division {@link Long}
	 */
	void setDivision(Long division);

	/**
	 * Metodo que busca los departamentos segun la division.
	 */
	void buscarDepartamentos();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @param listDepartamento {@link List} of {@link SelectItem}
	 */
	void setListDepartamento(List<SelectItem> listDepartamento);

	/**
	 * @return {@link Long}
	 */
	Long getDepartamento();

	/**
	 * @param departamento {@link Long}
	 */
	void setDepartamento(Long departamento);

	/**
	 * Metodo que busca las unidades segun la departamento.
	 */
	void buscarUnidades();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListUnidad();

	/**
	 * @param listUnidad {@link List} of {@link SelectItem}
	 */
	void setListUnidad(List<SelectItem> listUnidad);

	/**
	 * Metodo que busca los cargos segun la unidad.
	 */
	void buscarCargos();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListCargo();

	/**
	 * @param listCargo {@link List} of {@link SelectItem}
	 */
	void setListCargo(List<SelectItem> listCargo);

	/**
	 * @return {@link Long}
	 */
	Long getCargo();

	/**
	 * @param cargo {@link Long}
	 */
	void setCargo(Long cargo);

	/**
	 * @return {@link Long}
	 */
	Long getUnidad();

	/**
	 * @param unidad {@link Long}
	 */
	void setUnidad(Long unidad);

	/**
	 * Metodo que busca las personas segun el cargo.
	 */
	void buscarPersonas();

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListPersonas();

	/**
	 * @param listPersonas {@link List} of {@link SelectItem}
	 */
	void setListPersonas(List<SelectItem> listPersonas);

	/**
	 * @return {@link Long}
	 */
	Long getPersona();

	/**
	 * @param persona {@link Long}
	 */
	void setPersona(Long persona);

	/**
	 * Metodo que busca los Expedientes Archivado.
	 */
	void buscarExpedienteArchivado();

	/**
	 * Metodo que busca los Expedientes cancelados.
	 */
	void buscarExpedienteCancelado();

	/**
	 * Metodo que busca los Expedientes ingresados.
	 */
	void buscarExpedienteIngresados();

	/**
	 * @return {@link List} of {@link OffLine}
	 */
	List<OffLine> getBandejaListaArchivado();

	/**
	 * @param bandejaListaArchivado {@link List} of {@link OffLine}
	 */
	void setBandejaListaArchivado(List<OffLine> bandejaListaArchivado);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<OffLine> getBandejaListaCancelado();

	/**
	 * @param bandejaListaCancelado {@link List} of {@link OffLine}
	 */
	void setBandejaListaCancelado(List<OffLine> bandejaListaCancelado);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<OffLine> getBandejaListaIngresado();

	/**
	 * @param bandejaListaIngresado {@link List} of {@link OffLine}
	 */
	void setBandejaListaIngresado(List<OffLine> bandejaListaIngresado);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getArchivado();

	/**
	 * @param archivado {@link Boolean}
	 */
	void setArchivado(Boolean archivado);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getCancelado();

	/**
	 * @param cancelado {@link Boolean}
	 */
	void setCancelado(Boolean cancelado);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getIngresado();

	/**
	 * @param ingresado {@link Boolean}
	 */
	void setIngresado(Boolean ingresado);

	/**
	 * @return {@link Boolean}
	 */
	Boolean getDespachado();

	/**
	 * @param despachado {@link Boolean}
	 */
	void setDespachado(Boolean despachado);

	/**
	 * @return {@link List} of {@link OffLine}
	 */
	List<OffLine> getBandejaListaDespachado();

	/**
	 * @param bandejaListaDespachado {@link List} of {@link OffLine}
	 */
	void setBandejaListaDespachado(List<OffLine> bandejaListaDespachado);

	/**
	 * Metodo que busca los Expedientes despachados.
	 */
	void buscarExpedienteDespachados();

}
