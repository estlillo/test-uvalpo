package cl.exe.exedoc.reportes.offline.estructura;

/**
 * Class utilizada para mostrar detalle de los expedientes offline.
 * 
 * @author Ricardo Fuentes
 */
public class OffLine {

	private String fechas;
	private String unidad;
	private String usuario;
	private Long cantidad;

	/**
	 * Constructor con parametros.
	 * 
	 * @param fechas {@link String}
	 * @param unidad {@link String}
	 * @param usuario {@link String}
	 * @param cantidad {@link Long}
	 */
	public OffLine(final String fechas, final String unidad, final String usuario, final Long cantidad) {
		super();
		this.fechas = fechas;
		this.unidad = unidad;
		this.usuario = usuario;
		this.cantidad = cantidad;
	}

	/**
	 * Constructor.
	 */
	public OffLine() {
		super();
	}

	/**
	 * @return {@link String}
	 */
	public String getFechas() {
		return fechas;
	}

	/**
	 * @param fechas {@link String}
	 */
	public void setFechas(final String fechas) {
		this.fechas = fechas;
	}

	/**
	 * @return {@link String}
	 */
	public String getUnidad() {
		return unidad;
	}

	/**
	 * @param unidad {@link String}
	 */
	public void setUnidad(final String unidad) {
		this.unidad = unidad;
	}

	/**
	 * @return {@link String}
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario {@link String}
	 */
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return {@link String}
	 */
	public Long getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad {@link String}
	 */
	public void setCantidad(final Long cantidad) {
		this.cantidad = cantidad;
	}

}
