package cl.exe.exedoc.reportes.offline;

import static cl.exe.exedoc.util.JerarquiasLocal.INICIO;
import static cl.exe.exedoc.util.JerarquiasLocal.TEXTO_INICIAL;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.reportes.offline.estructura.OffLine;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import org.jboss.seam.annotations.Destroy;

/**
 * @author Ricardo Fuentes
 */
@Stateful
@Name(ReporteOffLineBean.NAME)
public class ReporteOffLineBean implements ReporteOffLine, Serializable {

	/**
	 * NAME.
	 */
	public static final String NAME = "reporteOffLine";

	private static final long serialVersionUID = 1007181304069434883L;

	private static final String TITULO_NUM_EXP_ARCH = "Número de Expedientes Archivados";
	private static final String TITULO_NUM_EXP_CAN = "Número de Expedientes Cancelados";
	private static final String TITULO_NUM_EXP_ING = "Número de Expedientes Ingresados";
	private static final String TITULO_NUM_EXP_DES = "Número de Expedientes Despachados";
	
	private static final String TITULO_ENTIDAD_CANCELA = "Entidad que Cancela:";
	private static final String TIUTLO_ENTIDAD_INGRESA = "Entidad que Ingresa:";
	private static final String TITULO_ENTIDAD_DESPACHA = "Entidad que Despacha:";

	private static final String STRING_INICIO = "Inicio";
	private static final String STRING_TERMINO = "Término";
	private static final String INCLUIR_FECHA = "Debe incluir la fecha de {0}";

	private static final Locale LOCALE = FechaUtil.LOCALE;

	private Long organizacion = INICIO;
	private Long division = INICIO;
	private Long departamento = INICIO;
	private Long unidad = INICIO;
	private Long cargo = INICIO;
	private Long persona = INICIO;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidad;
	private List<SelectItem> listCargo;
	private List<SelectItem> listPersonas;

	private String tituloFiltro;
	private String tituloFiltroDos;

	private Date fechaInicio;
	private Date fechaTermino;

	private List<OffLine> bandejaListaArchivado;
	private List<OffLine> bandejaListaCancelado;
	private List<OffLine> bandejaListaIngresado;
	private List<OffLine> bandejaListaDespachado;

	private Boolean archivado;
	private Boolean cancelado;
	private Boolean ingresado;
	private Boolean despachado;

	@EJB
	private JerarquiasLocal jerarquias;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;
	
//	@In(required = false)
//	private Persona usuario;

	/**
	 * Constructor.
	 */
	public ReporteOffLineBean() {

	}

	@Begin(join = true)
	@Override
	public String begin(final Boolean archi, final Boolean canc, final Boolean ing, final Boolean desp) {
		log.info("beginning conversation");

		if (archi) {
			this.setTituloFiltro(TITULO_NUM_EXP_ARCH);
			this.setTituloFiltroDos(TITULO_ENTIDAD_CANCELA);
			this.bandejaListaArchivado = new ArrayList<OffLine>();
		}
		if (canc) {
			this.setTituloFiltro(TITULO_NUM_EXP_CAN);
			this.setTituloFiltroDos(TITULO_ENTIDAD_CANCELA);
			this.bandejaListaCancelado = new ArrayList<OffLine>();
		}
		if (ing) {
			this.setTituloFiltro(TITULO_NUM_EXP_ING);
			this.setTituloFiltroDos(TIUTLO_ENTIDAD_INGRESA);
			this.bandejaListaIngresado = new ArrayList<OffLine>();
		}
		if (desp) {
			this.setTituloFiltro(TITULO_NUM_EXP_DES);
			this.setTituloFiltroDos(TITULO_ENTIDAD_DESPACHA);
			this.bandejaListaDespachado = new ArrayList<OffLine>();
		}
		archivado = archi;
		cancelado = canc;
		ingresado = ing;
		despachado = desp;

		this.listOrganizacion = this.iniciarLista();
		this.listDivision = this.iniciarLista();
		this.listDepartamento = this.iniciarLista();
		this.listUnidad = this.iniciarLista();
		this.listCargo = this.iniciarLista();
		this.listPersonas = this.iniciarLista();
		
		organizacion = JerarquiasLocal.INICIO; 
		this.listOrganizacion = this.buscarOrganizaciones();
//		organizacion = (Long) listOrganizacion.get(1).getValue();
//		this.buscarDivisiones();
//		division = (Long) listDivision.get(1).getValue();
//		this.buscarDepartamentos();
//		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
//        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
//        this.buscarUnidades();

		return NAME;
	}

	@End
	@Override
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Destroy
	@Remove
	@Override
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedienteArchivado() {
		log.info("Iniciando busqueda de expedientes archivados.");
		this.bandejaListaArchivado = new ArrayList<OffLine>();
		if (this.validarFechas()) {
			final StringBuilder sql = new StringBuilder();
			sql.append("select count(id_emisor), nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("from ");
			sql.append("	(select distinct e.numero_expediente, e.nombres, e.id_emisor, e.archivado, ");
			sql.append("		e.id_unidad, e.unidad, e.id_cargo, e.cargo, e.id_departamento, e.departamento, ");
			sql.append("		e.id_division, e.division, e.id_organizacion, e.organizacion ");
			sql.append("	from expedientes_offline e ");
			sql.append("	where e.archivado is not NULL ");
			sql.append("	order by e.id_emisor ) ");
			sql.append("where archivado between :inicio and :fin ");
			this.agregarWhere(sql);
			sql.append("group by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("order by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");

			final Query query = em.createNativeQuery(sql.toString());
			query.setParameter("inicio", FechaUtil.inicio(fechaInicio), TemporalType.TIMESTAMP);
			query.setParameter("fin", FechaUtil.fin(fechaTermino), TemporalType.TIMESTAMP);

			final List<Object[]> lista = (List<Object[]>) query.getResultList();

			this.cargarLista(lista);
		}

		log.info("fin de la busqueda de expedientes archivados.");
	}

	/**
	 * Metodo que agrega el Where a la Query.
	 * 
	 * @param sql {@link StringBuilder}
	 */
	private void agregarWhere(StringBuilder sql) {
		if (!organizacion.equals(INICIO) || !division.equals(INICIO) || !departamento.equals(INICIO)
				|| !unidad.equals(INICIO)) {
			sql.append(" and ");
		}
		if (!organizacion.equals(INICIO)) {
			sql.append(" id_organizacion = " + organizacion);
			if (!division.equals(INICIO) || !departamento.equals(INICIO) || !unidad.equals(INICIO)) {
				sql.append(" and ");
			}
		}
		if (!division.equals(INICIO)) {
			sql.append(" id_division = " + division);
			if (!departamento.equals(INICIO) || !unidad.equals(INICIO)) {
				sql.append(" and ");
			}
		}
		if (!departamento.equals(INICIO)) {
			sql.append(" id_departamento = " + departamento);
			if (!unidad.equals(INICIO)) {
				sql.append(" and ");
			}
		}
		if (!unidad.equals(INICIO)) {
			sql.append(" id_unidad = " + unidad);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedienteCancelado() {
		log.info("Iniciando busqueda de expedientes cancelados.");
		if (this.validarFechas()) {
			this.bandejaListaCancelado = new ArrayList<OffLine>();
			
			final StringBuilder sql = new StringBuilder();
			sql.append("select count(id_emisor), nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("from ");
			sql.append("	(select distinct e.numero_expediente, e.nombres, e.id_emisor, e.fecha_cancelado, ");
			sql.append("		e.id_unidad, e.unidad, e.id_cargo, e.cargo, e.id_departamento, e.departamento, ");
			sql.append("		e.id_division, e.division, e.id_organizacion, e.organizacion ");
			sql.append("	from expedientes_offline e ");
			sql.append("	where e.fecha_cancelado is not NULL ");
			sql.append("order by e.id_emisor ) ");
			sql.append("where fecha_cancelado between :inicio and :fin ");
			this.agregarWhere(sql);
			sql.append("group by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("order by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("departamento, id_division, division, id_organizacion, organizacion ");
			
			final Query query = em.createNativeQuery(sql.toString());
			query.setParameter("inicio", FechaUtil.inicio(fechaInicio), TemporalType.TIMESTAMP);
			query.setParameter("fin", FechaUtil.fin(fechaTermino), TemporalType.TIMESTAMP);

			final List<Object[]> lista = (List<Object[]>) query.getResultList();

			this.cargarLista(lista);
			
		}
		log.info("fin de la busqueda de expedientes cancelados.");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedienteIngresados() {
		log.info("Iniciando busqueda de expedientes ingresados.");
		this.bandejaListaIngresado = new ArrayList<OffLine>();
		if (this.validarFechas()) {
			final StringBuilder sql = new StringBuilder();
			sql.append("select count(id_emisor), nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("from ");
			sql.append("	(select distinct e.numero_expediente, e.nombres, e.id_emisor, e.fecha_ingreso, ");
			sql.append("		e.id_unidad, e.unidad, e.id_cargo, e.cargo, e.id_departamento, e.departamento, ");
			sql.append("		e.id_division, e.division, e.id_organizacion, e.organizacion ");
			sql.append("	from expedientes_offline e ");
			sql.append("	order by e.id_emisor ) ");
			sql.append("where fecha_ingreso between :inicio and :fin ");
			this.agregarWhere(sql);
			sql.append("group by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("order by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");

			final Query query = em.createNativeQuery(sql.toString());
			query.setParameter("inicio", FechaUtil.inicio(fechaInicio), TemporalType.TIMESTAMP);
			query.setParameter("fin", FechaUtil.fin(fechaTermino), TemporalType.TIMESTAMP);

			final List<Object[]> lista = (List<Object[]>) query.getResultList();

			this.cargarLista(lista);
		}

		log.info("fin de la busqueda de expedientes ingresados.");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void buscarExpedienteDespachados() {
		log.info("Iniciando busqueda de expedientes despachados.");
		this.bandejaListaDespachado = new ArrayList<OffLine>();
		if (this.validarFechas()) {
			final StringBuilder sql = new StringBuilder();
			sql.append("select count(id_emisor), nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("from ");
			sql.append("	(select distinct e.numero_expediente, e.nombres, e.id_emisor, e.fecha_despacho, ");
			sql.append("		e.id_unidad, e.unidad, e.id_cargo, e.cargo, e.id_departamento, e.departamento,");
			sql.append("		 e.id_division, e.division, e.id_organizacion, e.organizacion ");
			sql.append("	from expedientes_offline e ");
			sql.append("	where e.fecha_despacho is not NULL ");
			sql.append("	order by e.id_emisor ) ");
			sql.append("where fecha_despacho between :inicio and :fin ");
			this.agregarWhere(sql);
			sql.append("group by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			sql.append("order by id_emisor, nombres, id_unidad, unidad, id_cargo, cargo, id_departamento, ");
			sql.append("	departamento, id_division, division, id_organizacion, organizacion ");
			
			final Query query = em.createNativeQuery(sql.toString());
			query.setParameter("inicio", FechaUtil.inicio(fechaInicio), TemporalType.TIMESTAMP);
			query.setParameter("fin", FechaUtil.fin(fechaTermino), TemporalType.TIMESTAMP);

			final List<Object[]> lista = (List<Object[]>) query.getResultList();

			this.cargarLista(lista);
		}
		log.info("fin de la busqueda de expedientes despachados.");
	}

	/**
	 * Metodo que crea la lista a mostrar.
	 * 
	 * @param lista {@link List} of {@link Object}
	 */
	private void cargarLista(final List<Object[]> lista) {

		for (Object[] o : lista) {
			final String uni = (String) o[3];
			final String nombre = (String) o[1];
			final Long cantidad = ((BigDecimal) o[0]).longValue();
			final String rango = FechaUtil.EXTSDF.format(fechaInicio) + " a " + FechaUtil.EXTSDF.format(fechaTermino);
			final OffLine off = new OffLine(rango, uni, nombre, cantidad);
			this.identificarLista(off);
		}
	}

	/**
	 * @param off {@link OffLine}
	 */
	private void identificarLista(final OffLine off) {
		if (archivado) {
			this.bandejaListaArchivado.add(off);
		}
		if (cancelado) {
			this.bandejaListaCancelado.add(off);
		}
		if (ingresado) {
			this.bandejaListaIngresado.add(off);
		}
		if (despachado) {
			this.bandejaListaDespachado.add(off);
		}
	}

	/**
	 * Metodo encargodo de validar las fechas de inicio y termino correspondientes a la fecha de despacho de un
	 * expediente.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarFechas() {
		Boolean validar = Boolean.TRUE;

		if (fechaInicio == null && fechaTermino == null) {
			FacesMessages.instance().add("Se debe incluir mínimo, el rango de fechas");
			validar = Boolean.FALSE;
		} else if (fechaInicio != null && fechaTermino == null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_TERMINO));
			validar = Boolean.FALSE;
		} else if (fechaInicio == null && fechaTermino != null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_INICIO));
			validar = Boolean.FALSE;
		}
		return validar;
	}

	/**
	 * Metodo que iniciliza una lista de un formularios.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> iniciarLista() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(INICIO, TEXTO_INICIAL));
		return lista;
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		return jerarquias.getOrganizaciones(TEXTO_INICIAL);
	}

	@Override
	public void buscarDivisiones() {
		this.listDivision = this.iniciarLista();
		this.listDepartamento = this.iniciarLista();
		this.listUnidad = this.iniciarLista();
		this.listCargo = this.iniciarLista();
		this.listPersonas = this.iniciarLista();
		
		this.listDivision = jerarquias.getDivisiones(TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDepartamentos() {
		this.listDepartamento = jerarquias.getDepartamentos(TEXTO_INICIAL, division);
	}

	@Override
	public void buscarUnidades() {
		this.listUnidad = jerarquias.getUnidadesOrganzacionales(TEXTO_INICIAL, departamento);
	}

	@Override
	public void buscarCargos() {
		this.listCargo = jerarquias.getCargos(TEXTO_INICIAL, unidad);
	}

	@Override
	public void buscarPersonas() {
		this.listPersonas = jerarquias.getPersonas(TEXTO_INICIAL, cargo);
		log.info("lista de listPersonas: {0}", listPersonas.size());
	}

	@Override
	public String getTituloFiltro() {
		return tituloFiltro;
	}

	@Override
	public void setTituloFiltro(final String tituloFiltro) {
		this.tituloFiltro = tituloFiltro;
	}

	@Override
	public Locale getLocale() {
		return LOCALE;
	}

	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

	@Override
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Override
	public Date getFechaTermino() {
		return fechaTermino;
	}

	@Override
	public void setFechaTermino(final Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	@Override
	public String getTituloFiltroDos() {
		return tituloFiltroDos;
	}

	@Override
	public void setTituloFiltroDos(final String tituloFiltroDos) {
		this.tituloFiltroDos = tituloFiltroDos;
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public List<SelectItem> getListUnidad() {
		return listUnidad;
	}

	@Override
	public void setListUnidad(final List<SelectItem> listUnidad) {
		this.listUnidad = listUnidad;
	}

	@Override
	public List<SelectItem> getListCargo() {
		return listCargo;
	}

	@Override
	public void setListCargo(final List<SelectItem> listCargo) {
		this.listCargo = listCargo;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(final Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<OffLine> getBandejaListaArchivado() {
		return bandejaListaArchivado;
	}

	@Override
	public void setBandejaListaArchivado(final List<OffLine> bandejaListaArchivado) {
		this.bandejaListaArchivado = bandejaListaArchivado;
	}

	@Override
	public List<OffLine> getBandejaListaCancelado() {
		return bandejaListaCancelado;
	}

	@Override
	public void setBandejaListaCancelado(final List<OffLine> bandejaListaCancelado) {
		this.bandejaListaCancelado = bandejaListaCancelado;
	}

	@Override
	public List<OffLine> getBandejaListaIngresado() {
		return bandejaListaIngresado;
	}

	@Override
	public void setBandejaListaIngresado(final List<OffLine> bandejaListaIngresado) {
		this.bandejaListaIngresado = bandejaListaIngresado;
	}

	@Override
	public Boolean getArchivado() {
		return archivado;
	}

	@Override
	public void setArchivado(final Boolean archivado) {
		this.archivado = archivado;
	}

	@Override
	public Boolean getCancelado() {
		return cancelado;
	}

	@Override
	public void setCancelado(final Boolean cancelado) {
		this.cancelado = cancelado;
	}

	@Override
	public Boolean getIngresado() {
		return ingresado;
	}

	@Override
	public void setIngresado(final Boolean ingresado) {
		this.ingresado = ingresado;
	}

	@Override
	public Boolean getDespachado() {
		return despachado;
	}

	@Override
	public void setDespachado(final Boolean despachado) {
		this.despachado = despachado;
	}

	@Override
	public List<OffLine> getBandejaListaDespachado() {
		return bandejaListaDespachado;
	}

	@Override
	public void setBandejaListaDespachado(final List<OffLine> bandejaListaDespachado) {
		this.bandejaListaDespachado = bandejaListaDespachado;
	}
	
	

}
