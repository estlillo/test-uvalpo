package cl.exe.exedoc.reportes.firmados;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.FirmaEstructuradaDocumento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.util.JerarquiasLocal;

import javax.ejb.Remove;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.faces.FacesMessages;

/**
 * Implementacion de {@link ReporteDocumentoElectronicoEsperaFirma}.
 * 
 */
@Stateful
@Name(ReporteDocumentoElectronicoEsperaFirmaBean.NAME)
@Scope(ScopeType.APPLICATION)
public class ReporteDocumentoElectronicoEsperaFirmaBean implements
		ReporteDocumentoElectronicoEsperaFirma, Serializable {

	private static final long serialVersionUID = 6059125629648891914L;

	public static final String NAME = "reporteDocumentoElectronicoEsperaFirma";
	private String filterValue = "";
	private String visualizar = JerarquiasLocal.VISUALIZAR;

	@PersistenceContext
	private EntityManager em;

	@Logger
	private Log log;

	@In(required = true)
	private Persona usuario;

	private List<DocumentoElectronico> documentosElectronicosNoFirmados;

	@Override
	public String begin(boolean privilegioParaFirmar) {
		log.info("Privilegio de firmar: " + privilegioParaFirmar);
		log.info("Usuario: " + usuario);
		if (privilegioParaFirmar)
			buscarDocumentosConFirmaPendiente(usuario);
		else
			buscarDocumentosConFirmaPendiente();
		if(documentosElectronicosNoFirmados.size()==0)
			FacesMessages.instance().add("No hay documentos electronicos para mostrar que tengan firmas pendientes.");
			
		log.info("Cantidad de Documentos con firma pendiente: " + documentosElectronicosNoFirmados.size());
		return "reportesDocumentosEletronicosNoFirmado";
	}

	@SuppressWarnings("unchecked")
	private void buscarDocumentosConFirmaPendiente(Persona usuario) {
		Query query = em
				.createQuery("Select firmasEspera.documento from FirmaEstructuradaDocumento " +
						"firmasEspera where " +
						"firmasEspera.documento.formatoDocumento = :formato " +
						"and firmasEspera.persona = :persona");
		query.setParameter("formato", new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		query.setParameter("persona", usuario);
		documentosElectronicosNoFirmados = query.getResultList();
		for(DocumentoElectronico doc:documentosElectronicosNoFirmados)
			doc.getFirmasEstructuradas().size();
	}

	@SuppressWarnings("unchecked")
	private void buscarDocumentosConFirmaPendiente() {
		Query query = em
				.createQuery("Select documento " +
						"from DocumentoElectronico documento " +
						"where documento.id in (Select distinct firmasEspera.documento.id " +
						"from FirmaEstructuradaDocumento firmasEspera " +
						"where firmasEspera.documento.formatoDocumento = :formato " +
						"and firmasEspera.persona.cargo.unidadOrganizacional = :fiscalia)");
		query.setParameter("formato", new FormatoDocumento(FormatoDocumento.ELECTRONICO));
		query.setParameter("fiscalia", usuario.getCargo().getUnidadOrganizacional());
		documentosElectronicosNoFirmados = query.getResultList();
		for(DocumentoElectronico doc:documentosElectronicosNoFirmados)
			doc.getFirmasEstructuradas().size();
	}

	@Override
	public List<DocumentoElectronico> getDocumentosElectronicosNoFirmados() {
		return this.documentosElectronicosNoFirmados;
	}

	@Remove
	public void remove() {
	}

	@Override
	public void setDocumentosElectronicosNoFirmados(
			List<DocumentoElectronico> listaDocs) {
		this.documentosElectronicosNoFirmados = listaDocs;

	}

	@Override
	public String getVisualizar() {
		return visualizar;
	}

	@Override
	public void setVisualizar(String visualizar) {
		this.visualizar = visualizar;
	}

	@Destroy
	public void destroy() {
	}
	@Override
	public boolean filtrarFirmantes(Object d){
		if(d instanceof DocumentoElectronico){
			DocumentoElectronico docu = (DocumentoElectronico)d;
			return filtrarDocumentoElectronicoFirmante(docu,filterValue);
		}
		else
			log.info("NO ERA DOC ELECTRONICO: "+d);
		return true;
	}
	public boolean filtrarDocumentoElectronicoFirmante(DocumentoElectronico doc, String filtro){
		for(FirmaEstructuradaDocumento firma : doc.getFirmasEstructuradas())
			if(filtrarFirma(firma,filtro))
				return true;
		return false;
	}
	public boolean filtrarFirma(FirmaEstructuradaDocumento firma, String filtro){
		String nombreApellido = firma.getPersona().getNombreApellido();
		return nombreApellido.toLowerCase().startsWith(filtro.toLowerCase());
	}
	@Override
	public String getFilterValue() {
		return filterValue;
	}

	@Override
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

}
