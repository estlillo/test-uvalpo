package cl.exe.exedoc.reportes.firmados;

import java.util.List;

import cl.exe.exedoc.entity.DocumentoElectronico;

public interface ReporteDocumentoElectronicoEsperaFirma {

	String begin(boolean privilegioParaFirmar);

	List<DocumentoElectronico> getDocumentosElectronicosNoFirmados();

	void setDocumentosElectronicosNoFirmados(
			List<DocumentoElectronico> listaDocs);

	void destroy();

	void remove();

	String getVisualizar();

	void setVisualizar(String visualizar);

	boolean filtrarFirmantes(Object d);

	String getFilterValue();

	void setFilterValue(String filterValue);
}
