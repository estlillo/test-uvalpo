package cl.exe.exedoc.reportes.despacho;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.Local;
import javax.faces.model.SelectItem;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;

/**
 * @author Ricardo Fuentes
 */
@Local
public interface ReporteHojaDespachoPersonal {

	/**
	 * Inicia la converzacion.
	 * 
	 * @return {@link String}
	 */
	String begin();

	/**
	 * implementa el termino de la conversacion.
	 * 
	 * @return {@link String}
	 */
	String end();

	void destroy();

	/**
	 * Metodo que obtiene el id del expediente.
	 * 
	 * @param numeroExpediente
	 * @return {@link Long}
	 */
	Long obtenerIdExpediente(final String numeroExpediente);

	/**
	 * Metodo que valida permisos sobre el doc.
	 * 
	 * @param idDocumento {@link Long}
	 * @return {@link Boolean}
	 */
	Boolean renderedVisualizarExpediente(final Long idDocumento);

	/**
	 * Metodo que realiza la busqueda de expedientes despachados.
	 */
	void buscar();

	/**
	 * Metodo encargado de filtrar las division, dependiendo de la organizacion seleccionada.
	 */
	void buscarDivisiones();

	/**
	 * Metodo encargado de filtrar los departamentos, dependiendo de la division seleccionada.
	 */
	void buscarDepartamentos();

	/**
	 * Metodo encargado de filtrar las unidades, dependiendo del departamento seleccionado.
	 */
	void buscarUnidades();

	/**
	 * Metodo encargado de filtrar los cargos, dependiendo de la unidad seleccionada.
	 */
	void buscarCargos();

	/**
	 * Metdo encargado de filtrar las personasm dependiendo del cargo seleccionado.
	 */
	void buscarPersonas();

	/**
	 * @return {@link Long}
	 */
	Long getOrganizacion();

	/**
	 * @param organizacion {@link Long}
	 */
	void setOrganizacion(final Long organizacion);

	/**
	 * @return {@link Long}
	 */
	Long getDivision();

	/**
	 * @param division {@link Long}
	 */
	void setDivision(final Long division);

	/**
	 * @return {@link Long}
	 */
	Long getDepartamento();

	/**
	 * @param departamento {@link Long}
	 */
	void setDepartamento(final Long departamento);

	/**
	 * @return {@link Long}
	 */
	Long getUnidad();

	/**
	 * @param unidad {@link Long}
	 */
	void setUnidad(final Long unidad);

	/**
	 * @return {@link Long}
	 */
	Long getCargo();

	/**
	 * @param cargo {@link Long}
	 */
	void setCargo(final Long cargo);

	/**
	 * @return {@link Long}
	 */
	Long getPersona();

	/**
	 * @param persona {@link Long}
	 */
	void setPersona(final Long persona);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListOrganizacion();

	/**
	 * @param listOrganizacion {@link List} of {@link SelectItem}
	 */
	void setListOrganizacion(final List<SelectItem> listOrganizacion);

	/**
	 * @return {@link List} of {@link ExpedienteBandejaSalida}
	 */
	List<ExpedienteBandejaSalida> getBandejaLista();

	/**
	 * @param bandeja {@link List} of {@link ExpedienteBandejaSalida}
	 */
	void setBandejaLista(final List<ExpedienteBandejaSalida> bandeja);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDivision();

	/**
	 * @param listDivision {@link List} of {@link SelectItem}
	 */
	void setListDivision(final List<SelectItem> listDivision);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListPersonas();

	/**
	 * @param listPersonas {@link List} of {@link SelectItem}
	 */
	void setListPersonas(final List<SelectItem> listPersonas);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListDepartamento();

	/**
	 * @param listDepartamento {@link List} of {@link SelectItem}
	 */
	void setListDepartamento(final List<SelectItem> listDepartamento);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListUnidad();

	/**
	 * @param listUnidad {@link List} of {@link SelectItem}
	 */
	void setListUnidad(final List<SelectItem> listUnidad);

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	List<SelectItem> getListCargo();

	/**
	 * @param listCargo {@link List} of {@link SelectItem}
	 */
	void setListCargo(final List<SelectItem> listCargo);
	
	/**
	 * @return {@link Locale}
	 */
	Locale getLocale();
	
	/**
	 * @return {@link Date}
	 */
	Date getFechaInicio();
	
	/**
	 * @param fechaInicio {@link Date}
	 */
	void setFechaInicio(final Date fechaInicio);
	
	/**
	 * @return {@link Date}
	 */
	Date getFechaTermino();
	
	/**
	 * @param fechaTermino {@link Date}
	 */
	void setFechaTermino(final Date fechaTermino);
}
