package cl.exe.exedoc.reportes.despacho;

import static cl.exe.exedoc.util.JerarquiasLocal.INICIO;
import static cl.exe.exedoc.util.JerarquiasLocal.TEXTO_INICIAL;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoExpediente;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.ListaPersonasDocumento;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.util.ComparatorUtils;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.bandeja.Bandeja;

/**
 * Class utilizada para crear reporte de expedientes despachados.
 * 
 * @author Ricardo Fuentes
 */
@Stateful
@Name(ReporteHojaDespachoBean.NAME)
public class ReporteHojaDespachoBean implements ReporteHojaDespacho, Serializable {

	/**
	 * NAME.
	 */
	public static final String NAME = "reporteHojaDespacho";

	private static final String DESTINATARIO_HISTORICO = "destinatarioHistorico";
	private static final String DESTINATARIO = "destinatario";

	private static final long serialVersionUID = -4056048351930327337L;

	private static final String STRING_INICIO = "inicio";
	private static final String STRING_TERMINO = "termino";
	private static final String INCLUIR_FECHA = "Debe incluir la fecha de {0}";

	private static final Locale LOCALE = FechaUtil.LOCALE;
	
	@Out(required = false, scope = ScopeType.SESSION)
	private String homeCrear;

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@In(required = false)
	private Persona usuario;

	@EJB
	private JerarquiasLocal jerarquias;
	@EJB
	private Bandeja bandeja;
	
	@EJB
	private ManejarExpedienteInterface me;

	private Long organizacion = INICIO;
	private Long division = INICIO;
	private Long departamento = INICIO;
	private Long unidad = INICIO;
	private Long cargo = INICIO;
	private Long persona = INICIO;

	private Date fechaInicio;
	private Date fechaTermino;

	@Out(required = false, scope = ScopeType.SESSION)
	@In(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> bandejaLista;
	
	private List<ExpedienteBandejaSalida> expedientesTotal;

	private List<SelectItem> listOrganizacion;
	private List<SelectItem> listDivision;
	private List<SelectItem> listPersonas;
	private List<SelectItem> listDepartamento;
	private List<SelectItem> listUnidad;
	private List<SelectItem> listCargo;

	/**
	 * Constructor.
	 */
	public ReporteHojaDespachoBean() {
		super();
	}

	@Begin(join = true)
	@Override
	public String begin() {
		log.info("beginning conversation");
		
		this.listDivision = this.iniciarLista();
		this.listDepartamento = this.iniciarLista();
		this.listUnidad = this.iniciarLista();
		this.listPersonas = this.iniciarLista();
		this.listCargo = this.iniciarLista();
		
		this.cargarLista();
		
		this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();
		this.expedientesTotal = new ArrayList<ExpedienteBandejaSalida>();
		homeCrear = ReporteHojaDespachoBean.NAME;
		return ReporteHojaDespachoBean.NAME;
	}

	/**
	 * Metodo que carga la listas por default.
	 */
	private void cargarLista() {
		this.listOrganizacion = this.buscarOrganizaciones();
		organizacion = (Long) listOrganizacion.get(1).getValue();
		this.buscarDivisiones();
		division = (Long) listDivision.get(1).getValue();
		this.buscarDepartamentos();
		listDepartamento = jerarquias.getDepartamentos("<<Seleccionar>>");
        this.setDepartamento(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
        this.buscarUnidades();
	}

	/**
	 * Metodo que iniciliza una lista de un formularios.
	 * 
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> iniciarLista() {
		final List<SelectItem> lista = new ArrayList<SelectItem>();
		lista.add(new SelectItem(INICIO, TEXTO_INICIAL));
		return lista;
	}

	/**
	 * @return {@link List} of {@link SelectItem}
	 */
	private List<SelectItem> buscarOrganizaciones() {
		return jerarquias.getOrganizaciones(TEXTO_INICIAL);
	}

	@End
	@Override
	public String end() {
		log.info("ending conversation");
		return "home";
	}

	@Remove
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdExpediente(final String numeroExpediente) {
		Long idExpediente = null;
		final StringBuffer hql = new StringBuffer(10);
		hql.append("SELECT DISTINCT e.id FROM Expediente e ");
		hql.append("WHERE e.numeroExpediente = :numeroExpediente ");
		hql.append("order by e.id desc ");
		final Query query = em.createQuery(hql.toString());
		query.setParameter("numeroExpediente", numeroExpediente);
		final List<Long> idExpedientes = (List<Long>) query.getResultList();
		if (idExpedientes.size() >= 1) {
			idExpediente = idExpedientes.get(0);
		}
		return idExpediente;
	}

	@Override
	public Boolean renderedVisualizarExpediente(final Long idDocumento) {
		Boolean valido = true;
		final Documento doc = em.find(Documento.class, idDocumento);
		if (doc != null && doc.getFormatoDocumento() != null) {
			if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.PAPEL)) {
				valido = false;
			}
			if (doc.getReservado()) {
				for (ListaPersonasDocumento destinatario : doc.getListaPersonas()) {
					final Persona p = destinatario.getDestinatarioPersona();
					if (p == null) { return false; }
					if (!destinatario.getDestinatarioPersona().getId().equals(this.usuario.getId())) {
						valido = false;
					} else {
						valido = true;
						break;
					}
				}
			}
		}
		return valido;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buscar() {
		log.info("[ReporteHojaDespacho] iniciando busqueda...");
		log.info("homeCrear [{0}]", homeCrear);
		if (this.validarFechas()) {
			this.bandejaLista = new ArrayList<ExpedienteBandejaSalida>();
			final Query query = this.generarQuery();
			final List<Object[]> lista = (List<Object[]>) query.getResultList();
			this.procesarResultadoBusqueda(lista);
		}
		this.cargarLista();
		log.info("[ReporteHojaDespacho] fin busqueda...");
	}

	/**
	 * Metodo encargodo de validar las fechas de inicio y termino correspondientes a la fecha de despacho de un
	 * expediente.
	 * 
	 * @return {@link Boolean}
	 */
	private Boolean validarFechas() {
		Boolean validar = Boolean.TRUE;

		if (fechaInicio == null && fechaTermino == null) {
			FacesMessages.instance().add("Se debe incluir mínimo, el rango de fechas");
			validar = Boolean.FALSE;
		} else if (fechaInicio != null && fechaTermino == null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_TERMINO));
			validar = Boolean.FALSE;
		} else if (fechaInicio == null && fechaTermino != null) {
			FacesMessages.instance().add(MessageFormat.format(INCLUIR_FECHA, STRING_INICIO));
			validar = Boolean.FALSE;
		}
		return validar;
	}

	/**
	 * Metodo encargado de procesar los resultado de la busqueda.
	 * 
	 * @param lista {@link List} of {@link Object[]}
	 */
	private void procesarResultadoBusqueda(final List<Object[]> lista) {
		List<Expediente> expedientesHijos = null;
		ExpedienteBandejaSalida bs = null;
		List<DocumentoExpediente> respuestas = null;
		List<DocumentoExpediente> originales = null;

		for (Object[] obj : lista) {
			final Long idExp = ((Long) obj[0] != null) ? ((Long) obj[0]).longValue() : null;
			final String numeroExp = (String) obj[1];
			Date fechaDespacho = (Date) obj[2];
			final Date fechaIngreso = (Date) obj[3];
			final String nombreEmisor = ( (String) obj[4] != null ) ? (String) obj[4] : " ";
			final String apellidoPatEmisor = ( (String) obj[5] != null ) ? (String) obj[5] : " ";
			final String apellidoMatEmisor = ( (String) obj[6] != null ) ? (String) obj[6] : " ";

			final Expediente exp = new Expediente(idExp);
			exp.setNumeroExpediente(numeroExp);

			bs = new ExpedienteBandejaSalida();
			bs.setNumeroExpediente(numeroExp);
			bs.setFechaIngreso(fechaIngreso);
			bs.setFechaDespacho(fechaDespacho);
			bs.setFechaIngresoBandeja(fechaDespacho);
			bs.setEmisor(MessageFormat.format("{0} {1} {2}", nombreEmisor, apellidoPatEmisor, apellidoMatEmisor));

			respuestas = new ArrayList<DocumentoExpediente>();
			originales = new ArrayList<DocumentoExpediente>();
			bandeja.obtenerDatosUltimoDocumento(exp, bs, respuestas, originales);
			expedientesHijos = this.obtenerExpedientesHijos(exp.getId());
			if (expedientesHijos.size() != 0) {
				fechaDespacho = expedientesHijos.get(0).getFechaDespacho() != null ? expedientesHijos.get(0)
						.getFechaDespacho() : expedientesHijos.get(0).getFechaDespachoHistorico();
				bs.setFechaDespacho(fechaDespacho);
				bs.setDatosUsuariosAsociados(bandeja.obtenerDatosUsuariosAsociados(expedientesHijos, numeroExp));
			}
			bandejaLista.add(bs);
		}
		List<ExpedienteBandejaSalida> bandejaListaTmp = new ArrayList<ExpedienteBandejaSalida>();
		for (ExpedienteBandejaSalida bList : bandejaLista ) {
			if (bList.getDatosUsuariosAsociados() != null && !bList.getDatosUsuariosAsociados().isEmpty() ) {
				bandejaListaTmp.add(bList);
			}
		}
		bandejaLista = bandejaListaTmp;
		
		Collections.sort(bandejaLista, ComparatorUtils.compararListaBandejaSalida);
	}

	/**
	 * Metodo que obtienes la lista de expedientes que se han despachados por expediente.
	 * 
	 * @param idPadre {@link Long} identificador del expediente padre.
	 * @return {@link List} of {@link Expediente}
	 */
	@SuppressWarnings("unchecked")
	private List<Expediente> obtenerExpedientesHijos(final Long idPadre) {
		final List<Expediente> listExpHijos = new ArrayList<Expediente>();
		Expediente exp = null;
		final List<Object[]> lista = new ArrayList<Object[]>();

		final Query queryD = this.construirQuery(idPadre, DESTINATARIO);
		final List<Object[]> listDest = queryD.getResultList();
		final Query queryDH = this.construirQuery(idPadre, DESTINATARIO_HISTORICO);
		final List<Object[]> listDestHist = queryDH.getResultList();

		lista.addAll(listDest);
		lista.addAll(listDestHist);

		log.info("lista de expedientes relacionados: {0}", listDest.size());

		for (Object[] o : lista) {
			exp = this.construirExpediente(o);
			listExpHijos.add(exp);
		}
		return listExpHijos;
	}

	/**
	 * Metodo que contruye query para obtener expedientes despachados por unidad.
	 * 
	 * @param idPadre {@link Long}
	 * @param clazz {@link String}
	 * @return {@link Query}
	 */
	private Query construirQuery(final Long idPadre, final String clazz) {

		final StringBuilder sql = new StringBuilder();

		sql.append("select ex.id, ");
		sql.append("	ex.numeroExpediente, ");
		sql.append("	p.id, ");
		sql.append("	p.nombres, ");
		sql.append("	p.apellidoPaterno, ");
		sql.append("	p.apellidoMaterno, ");
		sql.append("	c.descripcion, ");
		sql.append("	u.descripcion, ");
		sql.append("	ex.fechaAcuseRecibo, ");
		sql.append("	ex.fechaDespachoHistorico, ");
		sql.append("	ex.fechaDespacho, ");
		sql.append("	ex.fechaIngreso, ");
		sql.append("	'" + clazz + "' ");
		sql.append("from Expediente ex ");
		sql.append("	left join ex." + clazz + " p ");
		sql.append("	left join p.cargo c ");
		sql.append("	left join c.unidadOrganizacional u ");
		sql.append("	left join u.departamento d ");
		sql.append("	left join d.division div ");
		sql.append("	left join div.organizacion org ");
		sql.append("where ex.versionPadre.id = :idPadre ");
		sql.append("	and p.id != null ");
		if (!unidad.equals(INICIO)) {
			sql.append("	and u.id = :unidad ");
		}
		if (!departamento.equals(INICIO)) {
			sql.append("	and d.id = :departamento ");
		}
//		if (!division.equals(INICIO)) {
//			sql.append("	and div.id = :division ");
//		}
		if (!cargo.equals(INICIO)) {
			System.out.println("cargo");
			sql.append("	and c.id = :cargo ");
		}
//		if (!organizacion.equals(INICIO)) {
//			sql.append("	and org.id = :organizacion ");
//		}
		
		
		//log.info("obtener expedientes relacionados query {0}", sql.toString());
		final Query query = em.createQuery(sql.toString());

		query.setParameter("idPadre", idPadre);
//		if (!organizacion.equals(INICIO)) {
//			query.setParameter("organizacion", organizacion);
//		}
//		if (!division.equals(INICIO)) {
//			query.setParameter("division", division);
//		}
		if (!departamento.equals(INICIO)) {
			query.setParameter("departamento", departamento);
		}
		if (!unidad.equals(INICIO)) {
			query.setParameter("unidad", unidad);
		}

		if (!cargo.equals(INICIO)) {
			query.setParameter("cargo", cargo);
		}
		return query;
	}

	/**
	 * Metodo que construye un expediente a partir de un arreglo de object.
	 * 
	 * @param o {@link Object}
	 * @return {@link Expediente}
	 */
	private Expediente construirExpediente(final Object[] o) {
		final Expediente exp = new Expediente();

		final Long idExp = ((Long) o[0] != null) ? ((Long) o[0]).longValue() : null;
		final String numeroExp = (String) o[1];
		final Long idDest = ((Long) o[2] != null) ? ((Long) o[2]).longValue() : null;
		final String nombresDest = (String) o[3];
		final String apellidoPatDest = (String) o[4];
		final String apellidoMatDest = (String) o[5];
		final String descCargo = (String) o[6];
		final String descUnidad = (String) o[7];
		final Date fechaAcuseRecibo = (Date) o[8];
		final Date fechaDespachoHist = (Date) o[9];
		final Date fechaDespacho = (Date) o[10];
		final Date fechaIngreso = (Date) o[11];
		final String tipoDest = (String) o[12];

		exp.setId(idExp);
		exp.setNumeroExpediente(numeroExp);
		final Persona destinatario = new Persona();
		destinatario.setId(idDest);
		destinatario.setNombres(nombresDest);
		destinatario.setApellidoPaterno(apellidoPatDest);
		destinatario.setApellidoMaterno(apellidoMatDest);

		final Cargo cargoDest = new Cargo();
		final UnidadOrganizacional unidadDest = new UnidadOrganizacional();
		unidadDest.setDescripcion(descUnidad);
		cargoDest.setDescripcion(descCargo);
		cargoDest.setUnidadOrganizacional(unidadDest);
		destinatario.setCargo(cargoDest);

		if (DESTINATARIO.equals(tipoDest)) {
			exp.setDestinatario(destinatario);
		} else if (DESTINATARIO_HISTORICO.equals(tipoDest)) {
			exp.setDestinatarioHistorico(destinatario);
		}

		exp.setFechaAcuseRecibo(fechaAcuseRecibo);
		exp.setFechaDespachoHistorico(fechaDespachoHist);
		exp.setFechaDespacho(fechaDespacho);
		exp.setFechaIngreso(fechaIngreso);

		return exp;
	}

	/**
	 * Metodo que crea query, para rescatar de la base de datos los expedientes despachados.
	 * 
	 * @return {@link Query}
	 */
	private Query generarQuery() {
		final StringBuilder sql = new StringBuilder();
		sql.append("select DISTINCT e.id, ");
		sql.append("	e.numeroExpediente, ");
		sql.append("	e.fechaDespacho, ");
		sql.append("	e.fechaIngreso, ");
		sql.append("	e.emisor.nombres, ");
		sql.append("	e.emisor.apellidoPaterno, ");
		sql.append("	e.emisor.apellidoMaterno, ");
		sql.append("	e.emisorHistorico.nombres, ");
		sql.append("	e.emisorHistorico.apellidoPaterno, ");
		sql.append("	e.emisorHistorico.apellidoMaterno ");
		sql.append("from Expediente e ");
		sql.append("	join e.emisor.cargo.unidadOrganizacional p2 ");
		sql.append(" 	left join e.emisorHistorico.cargo.unidadOrganizacional p3 ");

		sql.append("where (e.emisor.cargo.unidadOrganizacional.id = " + usuario.getCargo().getUnidadOrganizacional().getId() );
		//sql.append(" or e.emisorHistorico.cargo.unidadOrganizacional.id = " + usuario.getCargo().getUnidadOrganizacional().getId() );
		sql.append(")	");
		sql.append("	and e.cancelado is null ");
		//sql.append("	and e.destinatario.id is null ");
		
		final Date inicio = FechaUtil.inicio(fechaInicio);
		final Date termino = FechaUtil.fin(fechaTermino);
		log.info("inicio {0}", FechaUtil.BDSDF.format(inicio));
		log.info("termino {0}", FechaUtil.BDSDF.format(termino));

		sql.append("	and( e.fechaDespacho between :inicio and :termino ");
		sql.append("	or e.fechaDespachoHistorico between :inicio and :termino ) ");

		final Query query = em.createQuery(sql.toString());
		//query.setParameter("idUsuario", usuario.getId());
		query.setParameter(STRING_INICIO, inicio);
		query.setParameter(STRING_TERMINO, termino);
		log.info("sql.ToString:  " + sql.toString());
		return query;
	}

	@Override
	public void buscarDivisiones() {
		this.listDivision = jerarquias.getDivisiones(TEXTO_INICIAL, organizacion);
	}

	@Override
	public void buscarDepartamentos() {
		this.listDepartamento = jerarquias.getDepartamentos(TEXTO_INICIAL, division);
	}

	@Override
	public void buscarUnidades() {
		this.listUnidad = jerarquias.getUnidadesOrganzacionales(TEXTO_INICIAL, departamento);
	}

	@Override
	public void buscarCargos() {
		this.listCargo = jerarquias.getCargos(TEXTO_INICIAL, unidad);
	}

	@Override
	public void buscarPersonas() {
		this.listPersonas = jerarquias.getPersonas(TEXTO_INICIAL, cargo);
		log.info("lista de listPersonas: {0}", listPersonas.size());
	}

	@Override
	public Long getOrganizacion() {
		return organizacion;
	}

	@Override
	public void setOrganizacion(final Long organizacion) {
		this.organizacion = organizacion;
	}

	@Override
	public Long getDivision() {
		return division;
	}

	@Override
	public void setDivision(final Long division) {
		this.division = division;
	}

	@Override
	public Long getDepartamento() {
		return departamento;
	}

	@Override
	public void setDepartamento(final Long departamento) {
		this.departamento = departamento;
	}

	@Override
	public Long getUnidad() {
		return unidad;
	}

	@Override
	public void setUnidad(final Long unidad) {
		this.unidad = unidad;
	}

	@Override
	public Long getCargo() {
		return cargo;
	}

	@Override
	public void setCargo(final Long cargo) {
		this.cargo = cargo;
	}

	@Override
	public Long getPersona() {
		return persona;
	}

	@Override
	public void setPersona(final Long persona) {
		this.persona = persona;
	}

	@Override
	public List<SelectItem> getListOrganizacion() {
		return listOrganizacion;
	}

	@Override
	public void setListOrganizacion(final List<SelectItem> listOrganizacion) {
		this.listOrganizacion = listOrganizacion;
	}

	@Override
	public List<ExpedienteBandejaSalida> getBandejaLista() {
		return bandejaLista;
	}

	@Override
	public void setBandejaLista(final List<ExpedienteBandejaSalida> bandeja) {
		this.bandejaLista = bandeja;
	}

	@Override
	public List<SelectItem> getListDivision() {
		return listDivision;
	}

	@Override
	public void setListDivision(final List<SelectItem> listDivision) {
		this.listDivision = listDivision;
	}

	@Override
	public List<SelectItem> getListPersonas() {
		return listPersonas;
	}

	@Override
	public void setListPersonas(final List<SelectItem> listPersonas) {
		this.listPersonas = listPersonas;
	}

	@Override
	public List<SelectItem> getListDepartamento() {
		return listDepartamento;
	}

	@Override
	public void setListDepartamento(final List<SelectItem> listDepartamento) {
		this.listDepartamento = listDepartamento;
	}

	@Override
	public List<SelectItem> getListUnidad() {
		return listUnidad;
	}

	@Override
	public void setListUnidad(final List<SelectItem> listUnidad) {
		this.listUnidad = listUnidad;
	}

	@Override
	public List<SelectItem> getListCargo() {
		return listCargo;
	}

	@Override
	public void setListCargo(final List<SelectItem> listCargo) {
		this.listCargo = listCargo;
	}

	@Override
	public Locale getLocale() {
		return LOCALE;
	}

	@Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

	@Override
	public void setFechaInicio(final Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	@Override
	public Date getFechaTermino() {
		return fechaTermino;
	}

	@Override
	public void setFechaTermino(final Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	@Override
	public String begin(int tipo) {
		// TODO Auto-generated method stub
		return null;
	}
	public void borraLista(){}

	@Override
	public String nombreBean() {
		return NAME;
	}

}
