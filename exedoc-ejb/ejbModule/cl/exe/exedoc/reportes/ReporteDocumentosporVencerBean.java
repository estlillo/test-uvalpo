package cl.exe.exedoc.reportes;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.End;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.Expediente;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.mantenedores.dao.AdministradorReporteFolio;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandeja;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.JerarquiasLocal;
import cl.exe.exedoc.util.TipoDocumentosList;

/**
 * @author Sergio
 */
@Stateful
@Name("reporteDocumentosporVencer")
@Scope(ScopeType.CONVERSATION)
public class ReporteDocumentosporVencerBean implements ReporteDocumentosporVencer, Serializable
{
  
	private static final long serialVersionUID = -1796233940289568935L;
    
    public static final String REPORTE_DOCUMENTOSXVENCER = "reporteDocumentosporVencer";

	@Logger
	private Log log;

	@PersistenceContext
	private EntityManager em;

	@EJB
	private JerarquiasLocal jerarquias;
    
    @In(required = true)
	private Persona usuario;
    
    private List<SelectItem> listTipoDocumentos;
    private Long tipoDocumento = jerarquias.INICIO;
    
    private Date fechaInicio;
	private Date fechaTermino;
	private Date fInicio;
	private Date fTermino;
	
	private String numeroDocumento;
	private String materia;
	private String plazoDoc;
	
	@Out(required = false, scope = ScopeType.SESSION)
	private List<ExpedienteBandejaSalida> documentosRelacionados;

	@Override
	@Begin(join = true)
    public String begin() {
        // implement your begin conversation business logic
        log.info("beginning conversation");
        documentosRelacionados = new ArrayList<ExpedienteBandejaSalida>();

        final Calendar toDay = Calendar.getInstance();
        this.setFechaInicio(toDay.getTime());
        toDay.add(Calendar.DAY_OF_MONTH, 5);
        this.setFechaTermino(toDay.getTime());
        this.numeroDocumento = "";
        this.materia = "";
        return REPORTE_DOCUMENTOSXVENCER;
    }
  
    // add additional action methods that participate in this conversation
  
    @End
    public String end() {
        // implement your end conversation business logic
        log.info("ending conversation");
        return "home";
    }
  
    @Remove
    public void destroy() {
    	
    }

    @Override
	public void setListTipoDocumentos(List<SelectItem> listTipoDocumentos) {
		this.listTipoDocumentos = listTipoDocumentos;
	}

    @Override
	public List<SelectItem> getListTipoDocumentos() {
		return listTipoDocumentos;
	}

    @Override
	public void setTipoDocumento(Long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

    @Override
	public Long getTipoDocumento() {
		return tipoDocumento;
	}

    @Override
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

    @Override
	public Date getFechaInicio() {
		return fechaInicio;
	}

    @Override
	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

    @Override
	public Date getFechaTermino() {
		return fechaTermino;
	}
    
    @Override
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento.trim();
	}

    @Override
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

    @Override
	public void setMateria(String materia) {
		this.materia = materia.trim();
	}

    @Override
	public String getMateria() {
		return materia;
	}
    
    @Override
    public List<ExpedienteBandejaSalida> getDocumentosRelacionados() {
		return documentosRelacionados;
	}

    @Override
	public void setDocumentosRelacionados(
			List<ExpedienteBandejaSalida> documentosRelacionados) {
		this.documentosRelacionados = documentosRelacionados;
	}
	
    @Override
    public void setPlazoDoc(String plazoDoc) {
		this.plazoDoc = plazoDoc;
	}

    @Override
	public String getPlazoDoc() {
		return plazoDoc;
	}

	@Override
	public void filtrarDocumentosporVencer() {
		log.info("[filtrarDocumentosporVencer] Iniciando método.");
		
		if (this.fechaInicio != null && this.fechaTermino != null) {
			if (this.getFechaInicio().after(this.getFechaTermino())) {
				FacesMessages.instance().add("La fecha de inicio no puede ser mayor a la fecha de término");
				return;
			}
		}
		
		final StringBuilder sql = new StringBuilder(
				"SELECT d.documento.tipoDocumento.descripcion as descTipoDoc, d.documento.numeroDocumento, ");
		sql.append("d.documento.id as id_doc, d.documento.fechaDocumentoOrigen, d.documento.emisor, d.documento.materia, ");
		sql.append("d.documento.formatoDocumento.descripcion as descFd, d.documento.cmsId,d.documento.autor.nombres, ");
		sql.append("d.documento.autor.apellidoPaterno, d.documento.autor.apellidoMaterno, e.id as id_exp, e.numeroExpediente, d.documento.plazo, ");
		sql.append("lp.destinatario ");
		sql.append("FROM Expediente e inner join e.documentos d  ");
		sql.append("inner join d.documento de inner join de.listaPersonas lp ");
		sql.append("WHERE (d.documento.autor.cargo.unidadOrganizacional.departamento.id = ");
		sql.append(this.usuario.getCargo().getUnidadOrganizacional().getDepartamento().getId());
		sql.append(" ) and (" + tipoDocumento + " = -1 or d.documento.tipoDocumento.id = ");
		sql.append(tipoDocumento + ") ");

		if (!this.numeroDocumento.isEmpty()) {
			sql.append(" and  ");
			sql.append("d.documento.numeroDocumento like '" + this.numeroDocumento + "%'");
		}

		if (!this.materia.isEmpty()) {
			sql.append(" and ");
			sql.append("d.documento.materia like '" + this.materia + "%'");
		}

		if (this.fechaInicio != null && this.fechaTermino != null) {
			sql.append(" and d.documento.plazo between ? and ? ");
		}

		log.info("[filtrarDocumentosporVencer] SQL a ejecutar: " + sql);
		final Query query = em.createQuery(sql.toString());
		
		if (this.fechaInicio != null && this.fechaTermino != null) {
			this.fInicio = FechaUtil.inicio(this.fechaInicio);
			this.fTermino = FechaUtil.fin(this.fechaTermino);
			query.setParameter(1, this.fInicio);
			query.setParameter(2, this.fTermino);
		}
		
		List<Object[]> arrayObject = query.getResultList();
		this.busquedaDocumentosRelacionados(arrayObject);
	}

    /**
	 * Crea una lista de documentos, con todos sus documentos asociados y los deja en la variable
	 * documentosRelacionados.
	 * 
	 * @param docExpList List<DocumentoExpediente> que contiene la lista de los DocumentoExpediente que contiene todos
	 *        los documentos y Expedientes que se quieren procesar.
	 */
	private void busquedaDocumentosRelacionados(List<Object[]> arrayObject) {
		ExpedienteBandejaSalida expedienteBandejaSalida = null;
		documentosRelacionados = new ArrayList<ExpedienteBandejaSalida>();
		for (Object[] o : arrayObject) {

			String descTipoDoc = (String) o[0];
			String numeroDoc = (String) o[1];
			Long idDoc = (((Long) o[2] != null) ? ((Long) o[2]).longValue() : null);
			// Long idDoc = (((BigInteger) o[2] != null) ? ((BigInteger) o[2]).longValue() : null);
			Date fechaDocOrigen = (Date) o[3];
			String emisor = (String) o[4];
			String materia = (String) o[5];
			String descFormatoDoc = (String) o[6];
			Object cmsId = (Object) o[7];
			String nombreAutor = (String) o[8];
			String apellidoPatAutor = (String) o[9];
			String apellidoMatAutor = (String) o[10];
			Long idExp = (((Long) o[11] != null) ? ((Long) o[11]).longValue() : null);
			// Long idExp = (((BigInteger) o[11] != null) ? ((BigInteger) o[11]).longValue() : null);
			String numeroExpediente = (String) o[12];
			Date plazo = (Date) o[13];
			String destinatario = (String) o[14];
			
			Expediente exp = new Expediente();
			exp.setId(idExp);
			Documento doc = new Documento();
			doc.setId(idDoc);

			int indice = indiceDocEnBandejaDeEntrada(idDoc, documentosRelacionados);
			if (indice != -1) {
				List<Documento> docListActuales = documentosRelacionados.get(indice).getDocumentosRelacionados();
				List<Documento> docListNuevos = buscarDocumentosAsociadosAExpediente(exp);
				documentosRelacionados.get(indice).setDocumentosRelacionados(
						eliminarDocumentoDeLista(filtrarDocumentosRepetidos(docListActuales, docListNuevos), doc));
			} else {
				expedienteBandejaSalida = new ExpedienteBandejaSalida();
				expedienteBandejaSalida.setTipoDocumento(descTipoDoc);
				expedienteBandejaSalida.setNumeroDocumento(numeroDoc);
				expedienteBandejaSalida.setId(idDoc);
				expedienteBandejaSalida.setFechaDocumentoOrigen(fechaDocOrigen);
				expedienteBandejaSalida.setAutor(nombreAutor + " " + apellidoPatAutor + " " + apellidoMatAutor);
				expedienteBandejaSalida.setEmisor(emisor);
				expedienteBandejaSalida.setMateria(materia);
				expedienteBandejaSalida.setFormatoDocumento(descFormatoDoc);
				expedienteBandejaSalida.setTieneCmsId(cmsId != null);
				expedienteBandejaSalida.setPlazo(plazo);
				expedienteBandejaSalida.setDestinatario(destinatario);
				if (doc != null) {
					expedienteBandejaSalida.setIdDocumento(idDoc);
				}
				expedienteBandejaSalida.setDocumentosRelacionados(eliminarDocumentoDeLista(
						buscarDocumentosAsociadosAExpediente(exp), doc));
				expedienteBandejaSalida.setNumeroExpediente(numeroExpediente);
				documentosRelacionados.add(expedienteBandejaSalida);
			}
		}
	}
	
	/**
	 * indice de Documento En Bandeja De Entrada.
	 * 
	 * @param idDoc {@link Long}
	 * @param documentoEnBandeja List<? extends ExpedienteBandeja>
	 * @return {@link Integer}
	 */
	private int indiceDocEnBandejaDeEntrada(final Long idDoc, final List<? extends ExpedienteBandeja> documentoEnBandeja) {
		for (int i = 0; i < documentoEnBandeja.size(); i++) {
			if (documentoEnBandeja.get(i).getId().equals(idDoc)) { return i; }
		}
		return -1;
	}
	
	/**
	 * Busca los documentos asociados a un expediente en particular.
	 * 
	 * @param expediente Expediente del que se quieren obtener sus documentos asociados.
	 * @return List<Documento> que contiene la lista de documentos asociados al expediente de entrada
	 */
	@SuppressWarnings("unchecked")
	private List<Documento> buscarDocumentosAsociadosAExpediente(Expediente expediente) {
		List<Documento> docAsExp = new ArrayList<Documento>();
		StringBuilder sql = new StringBuilder("");
		
		sql.append("select e.id, de.documento.id, d.tipoDocumento.descripcion, d.numeroDocumento, ");
		sql.append("d.fechaDocumentoOrigen, d.emisor, d.materia, lp.destinatarioPersona.nombres, lp.destinatarioPersona.apellidoPaterno ");
		sql.append("from Expediente e inner join e.documentos de ");
		sql.append("inner join de.documento d inner join d.listaPersonas lp ");
		sql.append("where e.id = " + expediente.getId());
		
//		sql.append("select e.id, de.documento.id, td.descripcion, d.numeroDocumento, ");
//		sql.append("d.fechaDocumentoOrigen, d.emisor, d.materia ");
//		sql.append("from Expediente e, DocumentoExpediente de, ");
//		sql.append("Documento d, TipoDocumento td ");
//		sql.append("where e.id= de.expediente.id and de.documento.id= d.id ");
//		sql.append("and d.tipoDocumento.id = td.id and e.id= " + expediente.getId());
		List<Object[]> arrayObject = em.createQuery(sql.toString()).getResultList();

		for (Object[] o : arrayObject) {
			// Long idExp = (((BigInteger) o[0] != null) ? ((BigInteger) o[0]).longValue() : null);
			Long idDoc = (((Long) o[1] != null) ? ((Long) o[1]).longValue() : null);
			String descTipoDoc = (String) o[2];
			String numeroDoc = (String) o[3];
			Date fechaDocOrigen = (Date) o[4];
			String emisor = (String) o[5];
			String materia = (String) o[6];
			String destinatario = (String) o[7] + " " + (String) o[8];
			Documento doc = new Documento();
			TipoDocumento tipoDoc = new TipoDocumento();
			doc.setId(idDoc);
			doc.setEmisor(emisor);
			doc.setNumeroDocumento(numeroDoc);
			doc.setMateria(materia);
			doc.setFechaDocumentoOrigen(fechaDocOrigen);
			tipoDoc.setDescripcion(descTipoDoc);
			doc.setTipoDocumento(tipoDoc);
		
			docAsExp.add(doc);
		}
		return filtrarDocumentosRepetidos(docAsExp, null);
	}
	
	private List<Documento> filtrarDocumentosRepetidos(List<Documento> docList1, List<Documento> docList2) {
		List<Documento> docListRetorno = new ArrayList<Documento>();
		if (docList1 != null) {
			for (Documento doc : docList1) {
				if (!docListRetorno.contains(doc)) {
					docListRetorno.add(doc);
				}
			}
		}
		if (docList2 != null) {
			for (Documento doc : docList2) {
				if (!docListRetorno.contains(doc)) {
					docListRetorno.add(doc);
				}
			}
		}
		return docListRetorno;
	}
	
	private List<Documento> eliminarDocumentoDeLista(List<Documento> docList, Documento docBuscado) {
		List<Documento> docListRetorno = new ArrayList<Documento>();
		if (docList.contains(docBuscado)) {
			for (Documento documento : docList) {
				if (!documento.getId().equals(docBuscado.getId())) {
					docListRetorno.add(documento);
				}
			}
			return docListRetorno;
		}
		return docList;
	}
}
