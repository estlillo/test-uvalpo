package cl.exe.exedoc.resumen.remote;

import java.util.List;

import javax.ejb.Remote;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.resumen.DatosResumenUsuario;

@Remote
public interface ResumenRemote {

	public List<Persona> obtenerJefes();
	public List<DatosResumenUsuario> cargarResumenSubalternos(Persona jefe);
	public void setSubalternosDesdeCargo(Cargo c, List<Persona> personas, Persona jefe);
	public void setSubalternosDesdeUO(UnidadOrganizacional uo, List<Persona> personas, Persona jefe);
	public void setSubalternosDesdeDepto(Departamento depto, List<Persona> personas, Persona jefe);
	public void setSubalternosDesdeDivision(Division div, List<Persona> personas, Persona jefe);
	public boolean esJefeUnidad(Persona p);
	public boolean esJefeDepto(Persona p);
	public Integer buscarExpedientesBandejaDirectoUsuario(Persona p);
	public Integer buscarExpedientesBandejaCopiaUsuario(Persona p);
	public Integer buscarExpedientesDocumentosVencidos(Persona p);
	public Integer buscarExpedientesSinAcuseRecibo(Persona p);
	public byte[] crearResumenPDF(List<DatosResumenUsuario> resumenSubalternos);
}
