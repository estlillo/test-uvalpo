package cl.exe.exedoc.resumen.remote;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;
import cl.exe.exedoc.resumen.DatosResumenUsuario;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@Name("resumenRemote")
@Stateless
@Scope(ScopeType.STATELESS)
@Remote(ResumenRemote.class)
public class ResumenBean implements ResumenRemote {

	private List<ExpedienteBandejaEntrada> bandejaEntradaUsuario;
	private List<ExpedienteBandejaEntrada> bandejaEntradaCopiaUsuario;
	private List<DatosResumenUsuario> resumenSubalternos;

	@EJB
	private ManejarExpedienteInterface me;

	@PersistenceContext
	protected EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Persona> obtenerJefes() {
		List<Persona> jefes = em.createQuery("SELECT p FROM Persona p WHERE p.cargo.descripcion LIKE 'Jefe%'").getResultList();
		return jefes;
	}

	public List<DatosResumenUsuario> cargarResumenSubalternos(Persona jefe) {
		List<DatosResumenUsuario> lista = new ArrayList<DatosResumenUsuario>();
		List<Persona> subalternos = new ArrayList<Persona>();

		if (esJefeUnidad(jefe)) {
			UnidadOrganizacional uo = jefe.getCargo().getUnidadOrganizacional();
			setSubalternosDesdeUO(uo, subalternos, jefe);
		} else if (esJefeDepto(jefe)) {
			Departamento dep = jefe.getCargo().getUnidadOrganizacional().getDepartamento();
			setSubalternosDesdeDepto(dep, subalternos, jefe);
		} else {
			Division div = jefe.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
			setSubalternosDesdeDivision(div, subalternos, jefe);
		}

		for (Persona p : subalternos) {
			DatosResumenUsuario subalterno = new DatosResumenUsuario();
			subalterno.setCargo(p.getCargo().getDescripcion());
			subalterno.setUnidadOrganizacional(!p.getCargo().getUnidadOrganizacional().getVirtual() ? p.getCargo().getUnidadOrganizacional().getDescripcion() : "");
			subalterno.setDepartamento(!p.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual() ? p.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion() : "");
			subalterno.setDivision(p.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getDescripcion());
			subalterno.setIdPersona(p.getId());
			subalterno.setNombrePersona(p.getNombreApellido());
			subalterno.setNumExpBandejaCopia(buscarExpedientesBandejaCopiaUsuario(p));
			subalterno.setNumExpBandejaDirecto(buscarExpedientesBandejaDirectoUsuario(p));
			subalterno.setNumExpDocsVencidos(buscarExpedientesDocumentosVencidos(p));
			subalterno.setNumExpSinAcuseRecibo(buscarExpedientesSinAcuseRecibo(p));
			lista.add(subalterno);

			bandejaEntradaUsuario = null;
			bandejaEntradaCopiaUsuario = null;
		}

		return lista;
	}

	public void setSubalternosDesdeCargo(Cargo c, List<Persona> personas, Persona jefe) {
		List<Persona> ps = c.getPersonas();
		ps.remove(jefe);
		for (Persona p : ps) {
			if (p.getVigente()) {
				personas.add(p);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void setSubalternosDesdeUO(UnidadOrganizacional uo, List<Persona> personas, Persona jefe) {
		List<Cargo> cargos = em.createQuery("SELECT c FROM Cargo c WHERE c.unidadOrganizacional.id = :idUnidad").setParameter("idUnidad", uo.getId()).getResultList();
		for (Cargo c : cargos) {
			setSubalternosDesdeCargo(c, personas, jefe);
		}
	}

	@SuppressWarnings("unchecked")
	public void setSubalternosDesdeDepto(Departamento depto, List<Persona> personas, Persona jefe) {
		List<UnidadOrganizacional> unidades = em.createQuery("SELECT uo FROM UnidadOrganizacional uo WHERE uo.departamento.id = :idDepto").setParameter("idDepto", depto.getId()).getResultList();
		for (UnidadOrganizacional uo : unidades) {
			setSubalternosDesdeUO(uo, personas, jefe);
		}
	}

	@SuppressWarnings("unchecked")
	public void setSubalternosDesdeDivision(Division div, List<Persona> personas, Persona jefe) {
		List<Departamento> deptos = em.createQuery("SELECT d FROM Departamento d WHERE d.division.id = :idDivision").setParameter("idDivision", div.getId()).getResultList();
		for (Departamento depto : deptos) {
			setSubalternosDesdeDepto(depto, personas, jefe);
		}
	}

	public boolean esJefeUnidad(Persona p) {
		return p.getCargo().getDescripcion() != null && p.getCargo().getDescripcion().toUpperCase().startsWith("JEFE") && p.getCargo().getDescripcion().toUpperCase().contains("UNIDAD");
	}

	public boolean esJefeDepto(Persona p) {
		return p.getCargo().getDescripcion() != null && p.getCargo().getDescripcion().toUpperCase().startsWith("JEFE") && p.getCargo().getDescripcion().toUpperCase().contains("DEPARTAMENTO");
	}

	public Integer buscarExpedientesBandejaDirectoUsuario(Persona p) {
		bandejaEntradaUsuario = me.listarExpedienteBandejaEntradaUsuario(p);
		return bandejaEntradaUsuario.size();
	}

	public Integer buscarExpedientesBandejaCopiaUsuario(Persona p) {
		bandejaEntradaCopiaUsuario = me.listarExpedienteBandejaEntradaCopiaUsuario(p);
		return bandejaEntradaCopiaUsuario.size();
	}

	public Integer buscarExpedientesDocumentosVencidos(Persona p) {
		Integer num = 0;
		if (bandejaEntradaUsuario != null && !bandejaEntradaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada e : bandejaEntradaUsuario) {
				if (!e.getCompletado() && e.getPlazo() != null) {
					num++;
				}
			}
		}

		if (bandejaEntradaCopiaUsuario != null && !bandejaEntradaCopiaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada ec : bandejaEntradaCopiaUsuario) {
				if (!ec.getCompletado() && ec.getPlazo() != null) {
					num++;
				}
			}
		}

		return num;
	}

	public Integer buscarExpedientesSinAcuseRecibo(Persona p) {
		Integer num = 0;
		if (bandejaEntradaUsuario != null && !bandejaEntradaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada e : bandejaEntradaUsuario) {
				if (!e.getRecibido()) {
					num++;
				}
			}
		}

		if (bandejaEntradaCopiaUsuario != null && !bandejaEntradaCopiaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada ec : bandejaEntradaCopiaUsuario) {
				if (!ec.getRecibido()) {
					num++;
				}
			}
		}

		return num;
	}

	public byte[] crearResumenPDF(List<DatosResumenUsuario> resumenSubalternos) {
		Document resumenPDF = new Document(PageSize.LETTER, 18, 18, 18, 18);
		ByteArrayOutputStream resumenPDFByte = new ByteArrayOutputStream();

		try {
			PdfWriter.getInstance(resumenPDF, resumenPDFByte);
			resumenPDF.open();

			Date now = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String tituloStr = "Resumen de Actividades de subalternos al " + simpleDateFormat.format(now);
			Paragraph titulo = new Paragraph(new Phrase(tituloStr, FontFactory.getFont(FontFactory.TIMES_ROMAN, 14, Font.BOLDITALIC)));
			titulo.setSpacingAfter(15f);
			titulo.setAlignment(Element.ALIGN_CENTER);
			resumenPDF.add(titulo);

			PdfPTable tablaReporte = new PdfPTable(9);
			int headerwidths[] = { 13, 10, 10, 13, 13, 10, 10, 10, 10 };
			tablaReporte.setWidths(headerwidths);
			tablaReporte.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			tablaReporte.getDefaultCell().setGrayFill(0.8f);

			// Setear encabezado
			tablaReporte.addCell(new Phrase("Nombre", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Cargo", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Unidad Organizacional", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Departamento", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Division", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Expedientes en Bandeja de Entrada Directo", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Expedientes en Bandeja de Entrada Copia", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Expedientes con Documentos vencidos", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.addCell(new Phrase("Expedientes sin Acuse de Recibo", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD)));
			tablaReporte.setHeaderRows(1);

			tablaReporte.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
			tablaReporte.getDefaultCell().setGrayFill(1.0f);

			// Setear cuerpo
			for (DatosResumenUsuario subalterno : resumenSubalternos) {
				tablaReporte.addCell(new Phrase(subalterno.getNombrePersona(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getCargo(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getUnidadOrganizacional(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getDepartamento(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getDivision(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getNumExpBandejaDirecto().toString(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getNumExpBandejaCopia().toString(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getNumExpDocsVencidos().toString(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
				tablaReporte.addCell(new Phrase(subalterno.getNumExpSinAcuseRecibo().toString(), FontFactory.getFont(FontFactory.TIMES_ROMAN, 10)));
			}

			tablaReporte.setWidthPercentage(100);

			resumenPDF.add(tablaReporte);

		} catch (com.lowagie.text.DocumentException e) {
			e.printStackTrace();
		}

		resumenPDF.close();
		return resumenPDFByte.toByteArray();
	}

	public List<ExpedienteBandejaEntrada> getBandejaEntradaUsuario() {
		return bandejaEntradaUsuario;
	}

	public void setBandejaEntradaUsuario(List<ExpedienteBandejaEntrada> bandejaEntradaUsuario) {
		this.bandejaEntradaUsuario = bandejaEntradaUsuario;
	}

	public List<ExpedienteBandejaEntrada> getBandejaEntradaCopiaUsuario() {
		return bandejaEntradaCopiaUsuario;
	}

	public void setBandejaEntradaCopiaUsuario(List<ExpedienteBandejaEntrada> bandejaEntradaCopiaUsuario) {
		this.bandejaEntradaCopiaUsuario = bandejaEntradaCopiaUsuario;
	}

	public List<DatosResumenUsuario> getResumenSubalternos() {
		return resumenSubalternos;
	}

	public void setResumenSubalternos(List<DatosResumenUsuario> resumenSubalternos) {
		this.resumenSubalternos = resumenSubalternos;
	}

}
