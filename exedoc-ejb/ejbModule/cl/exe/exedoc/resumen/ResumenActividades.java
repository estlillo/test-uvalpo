package cl.exe.exedoc.resumen;

import java.util.List;

import javax.ejb.Local;

@Local
public interface ResumenActividades {

	public void destroy();
	
	public String initResumen();
	
	public List<DatosResumenUsuario> getResumenSubalternos();
	public void setResumenSubalternos(List<DatosResumenUsuario> resumenSubalternos);
}
