package cl.exe.exedoc.resumen;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.entity.Cargo;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaEntrada;
import cl.exe.exedoc.pojo.expediente.ManejarExpedienteInterface;

@Stateful
@Name("resumen")
@Scope(ScopeType.SESSION)
public class ResumenActividadesBean implements ResumenActividades {

	private List<ExpedienteBandejaEntrada> bandejaEntradaUsuario;
	private List<ExpedienteBandejaEntrada> bandejaEntradaCopiaUsuario;
	private List<DatosResumenUsuario> resumenSubalternos;

	@EJB
	private ManejarExpedienteInterface me;

	@Logger
	private Log log;

	@PersistenceContext
	protected EntityManager em;

	@In(required = true)
	private Persona usuario;

	public String initResumen() {
		log.info("Iniciando bean...");

		bandejaEntradaUsuario = null;
		bandejaEntradaCopiaUsuario = null;
		resumenSubalternos = null;

		resumenSubalternos = cargarResumenSubalternos();
		log.info("Datos de subalternos extraidos");

		return "resumen";
	}

	private List<DatosResumenUsuario> cargarResumenSubalternos() {
		List<DatosResumenUsuario> lista = new ArrayList<DatosResumenUsuario>();
		List<Persona> subalternos = new ArrayList<Persona>();

		if (esJefeUnidad(usuario)) {
			UnidadOrganizacional uo = usuario.getCargo().getUnidadOrganizacional();
			setSubalternosDesdeUO(uo, subalternos);
		} else if (esJefeDepto(usuario)) {
			Departamento dep = usuario.getCargo().getUnidadOrganizacional().getDepartamento();
			setSubalternosDesdeDepto(dep, subalternos);
		} else {
			Division div = usuario.getCargo().getUnidadOrganizacional().getDepartamento().getDivision();
			setSubalternosDesdeDivision(div, subalternos);
		}

		for (Persona p : subalternos) {
			DatosResumenUsuario subalterno = new DatosResumenUsuario();
			subalterno.setCargo(p.getCargo().getDescripcion());
			subalterno.setUnidadOrganizacional(!p.getCargo().getUnidadOrganizacional().getVirtual() ? p.getCargo().getUnidadOrganizacional().getDescripcion() : "");
			subalterno.setDepartamento(!p.getCargo().getUnidadOrganizacional().getDepartamento().getVirtual() ? p.getCargo().getUnidadOrganizacional().getDepartamento().getDescripcion() : "");
			subalterno.setDivision(p.getCargo().getUnidadOrganizacional().getDepartamento().getDivision().getDescripcion());
			subalterno.setIdPersona(p.getId());
			subalterno.setNombrePersona(p.getNombreApellido());
			subalterno.setNumExpBandejaCopia(buscarExpedientesBandejaCopiaUsuario(p));
			subalterno.setNumExpBandejaDirecto(buscarExpedientesBandejaDirectoUsuario(p));
			subalterno.setNumExpDocsVencidos(buscarExpedientesDocumentosVencidos(p));
			subalterno.setNumExpSinAcuseRecibo(buscarExpedientesSinAcuseRecibo(p));
			lista.add(subalterno);

			bandejaEntradaUsuario = null;
			bandejaEntradaCopiaUsuario = null;
		}

		return lista;
	}

	private void setSubalternosDesdeCargo(Cargo c, List<Persona> personas) {
		List<Persona> ps = c.getPersonas();
		ps.remove(usuario);
		for (Persona p : ps) {
			if (p.getVigente()) {
				personas.add(p);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void setSubalternosDesdeUO(UnidadOrganizacional uo, List<Persona> personas) {
		List<Cargo> cargos = em.createQuery("SELECT c FROM Cargo c WHERE c.unidadOrganizacional.id = :idUnidad").setParameter("idUnidad", uo.getId()).getResultList();
		for (Cargo c : cargos) {
			setSubalternosDesdeCargo(c, personas);
		}
	}

	@SuppressWarnings("unchecked")
	private void setSubalternosDesdeDepto(Departamento depto, List<Persona> personas) {
		List<UnidadOrganizacional> unidades = em.createQuery("SELECT uo FROM UnidadOrganizacional uo WHERE uo.departamento.id = :idDepto").setParameter("idDepto", depto.getId()).getResultList();
		for (UnidadOrganizacional uo : unidades) {
			setSubalternosDesdeUO(uo, personas);
		}
	}

	@SuppressWarnings("unchecked")
	private void setSubalternosDesdeDivision(Division div, List<Persona> personas) {
		List<Departamento> deptos = em.createQuery("SELECT d FROM Departamento d WHERE d.division.id = :idDivision").setParameter("idDivision", div.getId()).getResultList();
		for (Departamento depto : deptos) {
			setSubalternosDesdeDepto(depto, personas);
		}
	}

	private boolean esJefeUnidad(Persona p) {
		return p.getCargo().getDescripcion() != null && p.getCargo().getDescripcion().toUpperCase().startsWith("JEFE") && p.getCargo().getDescripcion().toUpperCase().contains("UNIDAD");
	}

	private boolean esJefeDepto(Persona p) {
		return p.getCargo().getDescripcion() != null && p.getCargo().getDescripcion().toUpperCase().startsWith("JEFE") && p.getCargo().getDescripcion().toUpperCase().contains("DEPARTAMENTO");
	}

	private Integer buscarExpedientesBandejaDirectoUsuario(Persona p) {
		bandejaEntradaUsuario = me.listarExpedienteBandejaEntradaUsuario(p);
		return bandejaEntradaUsuario.size();
	}

	private Integer buscarExpedientesBandejaCopiaUsuario(Persona p) {
		bandejaEntradaCopiaUsuario = me.listarExpedienteBandejaEntradaCopiaUsuario(p);
		return bandejaEntradaCopiaUsuario.size();
	}

	private Integer buscarExpedientesDocumentosVencidos(Persona p) {
		Integer num = 0;
		if (bandejaEntradaUsuario != null && !bandejaEntradaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada e : bandejaEntradaUsuario) {
				if (!e.getCompletado() && e.getPlazo() != null) {
					num++;
				}
			}
		}

		if (bandejaEntradaCopiaUsuario != null && !bandejaEntradaCopiaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada ec : bandejaEntradaCopiaUsuario) {
				if (!ec.getCompletado() && ec.getPlazo() != null) {
					num++;
				}
			}
		}

		return num;
	}

	private Integer buscarExpedientesSinAcuseRecibo(Persona p) {
		Integer num = 0;
		if (bandejaEntradaUsuario != null && !bandejaEntradaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada e : bandejaEntradaUsuario) {
				if (!e.getRecibido()) {
					num++;
				}
			}
		}

		if (bandejaEntradaCopiaUsuario != null && !bandejaEntradaCopiaUsuario.isEmpty()) {
			for (ExpedienteBandejaEntrada ec : bandejaEntradaCopiaUsuario) {
				if (!ec.getRecibido()) {
					num++;
				}
			}
		}

		return num;
	}

	@Destroy
	@Remove
	public void destroy() {
	}

	public List<DatosResumenUsuario> getResumenSubalternos() {
		return resumenSubalternos;
	}

	public void setResumenSubalternos(List<DatosResumenUsuario> resumenSubalternos) {
		this.resumenSubalternos = resumenSubalternos;
	}

}
