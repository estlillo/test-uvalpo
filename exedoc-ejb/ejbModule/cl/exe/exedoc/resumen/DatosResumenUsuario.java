package cl.exe.exedoc.resumen;

import java.io.Serializable;

public class DatosResumenUsuario implements Serializable {

	private static final long serialVersionUID = 4146913568697132405L;

	private Long idPersona;
	private String nombrePersona;
	private String cargo;
	private String unidadOrganizacional;
	private String departamento;
	private String division;
	private Integer numExpBandejaDirecto;
	private Integer numExpBandejaCopia;
	private Integer numExpDocsVencidos;
	private Integer numExpSinAcuseRecibo;

	public DatosResumenUsuario() {
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombrePersona() {
		return nombrePersona;
	}

	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getUnidadOrganizacional() {
		return unidadOrganizacional;
	}

	public void setUnidadOrganizacional(String unidadOrganizacional) {
		this.unidadOrganizacional = unidadOrganizacional;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public Integer getNumExpBandejaDirecto() {
		return numExpBandejaDirecto;
	}

	public void setNumExpBandejaDirecto(Integer numExpBandejaDirecto) {
		this.numExpBandejaDirecto = numExpBandejaDirecto;
	}

	public Integer getNumExpBandejaCopia() {
		return numExpBandejaCopia;
	}

	public void setNumExpBandejaCopia(Integer numExpBandejaCopia) {
		this.numExpBandejaCopia = numExpBandejaCopia;
	}

	public Integer getNumExpDocsVencidos() {
		return numExpDocsVencidos;
	}

	public void setNumExpDocsVencidos(Integer numExpDocsVencidos) {
		this.numExpDocsVencidos = numExpDocsVencidos;
	}

	public Integer getNumExpSinAcuseRecibo() {
		return numExpSinAcuseRecibo;
	}

	public void setNumExpSinAcuseRecibo(Integer numExpSinAcuseRecibo) {
		this.numExpSinAcuseRecibo = numExpSinAcuseRecibo;
	}
	
	@Override
	public String toString() {
		StringBuilder resumen = new StringBuilder();
		resumen.append("[usuario: ").append(this.nombrePersona + "], ");
		resumen.append("[cargo: ").append(this.cargo + "], ");
		resumen.append("[unidadOrganizacional: ").append(this.unidadOrganizacional + "], ");
		resumen.append("[departamento: ").append(this.departamento + "], ");
		resumen.append("[division: ").append(this.division + "], ");
		resumen.append("[expBandejaDirecto: ").append(this.numExpBandejaDirecto + "], ");
		resumen.append("[expBandejaCopia: ").append(this.numExpBandejaCopia + "], ");
		resumen.append("[expDocsVencidos: ").append(this.numExpDocsVencidos + "], ");
		resumen.append("[expSinAcuseRecibo: ").append(this.numExpSinAcuseRecibo + "]");
		
		return resumen.toString();
	}

}
