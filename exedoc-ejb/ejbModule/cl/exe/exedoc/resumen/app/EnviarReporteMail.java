package cl.exe.exedoc.resumen.app;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.naming.InitialContext;

import cl.exe.exedoc.entity.Persona;
import cl.exe.exedoc.pojo.expediente.SendMailUsingAuthentication;
import cl.exe.exedoc.resumen.DatosResumenUsuario;
import cl.exe.exedoc.resumen.remote.ResumenRemote;

public class EnviarReporteMail {

	public static final String EJB_RESUMEN_BEAN = "cl.exe.exedoc.resumen.ejb";
	public static final String EMAIL_SENDER_SUBDERE = "admexedoc@exe.cl";

	public static void main(String[] args) throws Exception {
		InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("resumen.properties");
		System.getProperties().load(inputStream);
		inputStream.close();
		
		ResumenRemote resumenInst = null;

		try {
			InitialContext ic = new InitialContext();
			String ejbJNDI = System.getProperty(EJB_RESUMEN_BEAN);
			resumenInst = (ResumenRemote) ic.lookup(ejbJNDI);
			
			List<Persona> jefes = resumenInst.obtenerJefes();
			List<DatosResumenUsuario> resumenSubalternos = null;
			Date now = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "CL"));
			String nowStr = simpleDateFormat.format(now);
			byte[] resumenPDFByte = null;
			String mailSubject = null;
			String[] mailTo = null;
			StringBuilder mailBody = null;
			for (Persona jefe : jefes) {
				resumenSubalternos = resumenInst.cargarResumenSubalternos(jefe);
				resumenPDFByte = resumenInst.crearResumenPDF(resumenSubalternos);
				resumenSubalternos = null;
				mailSubject = "Resumen Actividades Subalternos - " + nowStr;
				mailTo = new String[] {jefe.getEmail()};
				mailBody = new StringBuilder("Estimado(a) " + jefe.getNombreApellido() + ", con fecha " + nowStr + " \n");
				mailBody.append("se envia a usted el resumen de actividades de sus subalternos. Dicho resumen se \n");
				mailBody.append("encuentra como archivo adjunto en el presente mail. \n\n");
				
				mailBody.append("Enviado por EXEDOC exedoc\n\n" + "Para ingresar al Sistema, siga este <a href=\"http://10.80.11.5:8080/exedoc\">link</a>.");
				
				System.out.println("Enviando mail a " + jefe.getNombreApellido());
				enviarResumenMail(EMAIL_SENDER_SUBDERE, mailTo, mailSubject, mailBody.toString(), resumenPDFByte);
				
				resumenPDFByte = null;
				mailSubject = null;
				mailTo = null;
				mailBody = null;
			}
			System.out.println("Mails exitosamente enviados");
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private static void enviarResumenMail(String from, String[] to, String subject, String body, byte[] attachment) {
		try {
			SendMailUsingAuthentication mailSender = SendMailUsingAuthentication.getSendMail();
			mailSender.setEmailFromAddress(from);
			mailSender.setEmailList(to);
			mailSender.setEmailSubjectTxt(subject);
			mailSender.setEmailMsgTxt(body);
			mailSender.setFile(attachment);
			mailSender.setFilename("resumen.pdf");
			mailSender.sendMail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
