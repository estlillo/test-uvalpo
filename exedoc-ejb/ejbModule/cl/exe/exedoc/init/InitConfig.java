package cl.exe.exedoc.init;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.async.QuartzTriggerHandle;

import cl.exe.exedoc.util.FechaUtil;

/**
 * @author Pablo
 *
 */
@Name("initConfigStarter")
@Scope(ScopeType.APPLICATION)
@Startup(depends = "quartzDispatcher")
@AutoCreate
public class InitConfig {
	
	/**
	 * 
	 */
	//@SuppressWarnings("unused")
	private QuartzTriggerHandle handler;
	
	private Date midnite;
	private Long intervalo;

	/**
	 * Constructor.
	 */
	public InitConfig() {
		super();
	}
	
	/**
	 * @throws Exception 
	 */
	@Create
	public void start() throws Exception {
		
		final CustomizacionTarea customizacion = (CustomizacionTarea) Component.getInstance(CustomizacionTarea.NAME);
		
		midnite = customizacion.getFecha();
		intervalo = customizacion.getIntervalo();
		//intervalo = 5*FechaUtil.MINUTO;
		
		//handler = InitQuartzer.instance().iniciarTarea(midnite, intervalo);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		System.out.println("Cargando configuracion de Alertas: " + sdf.format(midnite) + ", " + intervalo);
		InitQuartzer.instance().iniciarTarea(midnite, intervalo);
	}
	
}
