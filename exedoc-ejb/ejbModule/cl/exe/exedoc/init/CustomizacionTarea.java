package cl.exe.exedoc.init;

import static cl.exe.exedoc.util.FechaUtil.DIA;

import java.util.Calendar;
import java.util.Date;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.util.FechaUtil;
import cl.exe.exedoc.util.singleton.CustomizacionSingleton;

/**
 * @author Pablo
 * 
 */
@Name(CustomizacionTarea.NAME)
public class CustomizacionTarea {

	/**
	 * NAME = "customizacionConfig".
	 */
	public static final String NAME = "customizacionConfig";

	/**
	 * 
	 */
	@Logger
	private Log log;

	private Date fecha;
	private Long intervalo;

	/**
	 * Constructor.
	 */
	public CustomizacionTarea() {
		super();
	}

	/**
	 * @return {@link Date}
	 */
	public Date getFecha() {

		final CustomizacionSingleton customizacion = CustomizacionSingleton
				.getInstance();

		final Calendar fechaCalendar = Calendar.getInstance();

		final int hora = customizacion.getHorainicio();
		
		fechaCalendar.set(Calendar.HOUR_OF_DAY, hora);
		fechaCalendar.set(Calendar.MINUTE, 0);
		fechaCalendar.set(Calendar.SECOND, 0);
		fechaCalendar.set(Calendar.MILLISECOND, 0);

		this.fecha = fechaCalendar.getTime();
		if (fecha.before(new Date())) {
			this.fecha = FechaUtil.obtenerDia(fechaCalendar.getTime(), 1);
		}

		return this.fecha;
	}

	/**
	 * @param fecha
	 *            {@link Date}
	 */
	public void setFecha(final Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return {@link Long}
	 */
	public Long getIntervalo() {
		intervalo = DIA;
		final CustomizacionSingleton customizacion = CustomizacionSingleton
				.getInstance();
		if (customizacion.getIntervalo() != null) {
			intervalo = customizacion.getIntervalo().longValue() * FechaUtil.HORA;
		}
		return intervalo;
	}

	/**
	 * @param intervalo
	 *            {@link Long}
	 */
	public void setIntervalo(final Long intervalo) {
		this.intervalo = intervalo;
	}
}
