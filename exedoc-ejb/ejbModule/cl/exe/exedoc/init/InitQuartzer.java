package cl.exe.exedoc.init;

import java.util.Date;

import javax.persistence.PersistenceException;

import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.async.Asynchronous;
import org.jboss.seam.annotations.async.Expiration;
import org.jboss.seam.annotations.async.IntervalDuration;
import org.jboss.seam.async.QuartzTriggerHandle;
import org.jboss.seam.log.Log;

import cl.exe.exedoc.session.ProcesoAutomatico;
import cl.exe.exedoc.session.ProcesoAutomaticoBean;

/**
 * @author Pablo
 * 
 */
@Name(InitQuartzer.NAME)
@AutoCreate
public class InitQuartzer {

	/**
	 * NAME = "initQuartzer".
	 */
	public static final String NAME = "initQuartzer";

	@Logger
	private Log log;

	/**
	 * Constructor.
	 */
	public InitQuartzer() {
		super();
	}

	/**
	 * @param when
	 *            {@link Date}
	 * @param interval
	 *            {@link Long}
	 * @return {@link QuartzTriggerHandle}
	 */
	@Asynchronous
	@Transactional
	public QuartzTriggerHandle iniciarTarea(@Expiration final Date when,
			@IntervalDuration final Long interval) {

		log.info("Inicio Tarea Automatica");

		try {
			final ProcesoAutomatico proceso = (ProcesoAutomatico) Component
					.getInstance(ProcesoAutomaticoBean.NAME);

			proceso.iniciarProceso(new Date());
		} catch (PersistenceException e) {
			log.error("PersistenceException: ", e);
		} catch (NullPointerException e) {
			log.error("NullPointerException: ", e);
		}

		log.info("Fin Tarea Automatica");

		return null;
	}

	/**
	 * @return {@link InitQuartzer}
	 */
	public static InitQuartzer instance() {
		return (InitQuartzer) Component.getInstance(InitQuartzer.NAME);
	}
}
