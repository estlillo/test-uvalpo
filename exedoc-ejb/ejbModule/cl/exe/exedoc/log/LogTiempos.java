package cl.exe.exedoc.log;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

public class LogTiempos {

	public static String NAME = "LogTiempos";

	private static Log log = Logging.getLog(LogTiempos.class);
	private static SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss.SS");

	public static void mostrarLog(final String usuario, final String operacion,
			final String marca, final Date fecha) {

		log.info(usuario + "," + operacion + "," + marca + ","
				+ formatter.format(fecha));
	}


	public static void mostrarLog(final String usuario, final String operacion,
			final long total) {
		log.info(usuario + "," + operacion + "," + total + " milisegundos");
	}

}
