package cl.exe.externos;

import java.util.List;

import cl.exe.exedoc.dao.DatosDiasAdministrativosDAO;
import cl.exe.exedoc.dao.DatosSolicitudTransparenciaDAO;
import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.entity.HistorialActualizacionTransparencia;
import cl.exe.exedoc.util.DatosSolicitud;
import cl.exe.exedoc.util.DatosSolicitudTransparencia;

public class DatosInterfacesExternas {

	public static DatosSolicitud getDiasAdministrativosManager(long idPersona) {
		return new DatosDiasAdministrativosDAO().getDiasAdministrativosPendientes(idPersona);
	}
	
//	public static boolean informarLicenciasMedicas(Documento doc){
//		return new DatosLicenciasMedicasDAO().informarLicencias(doc);
//	}

	public static DatosPersonasManager getDatosPersonasManager() {
		throw new UnsupportedOperationException("UnsupportedOperationException");
	}

	public static DatosSolicitud getDatosVacacionesManager(long idPersona) {
		return new DatosDiasAdministrativosDAO().getDiasVacacionesPendientes(idPersona);
	}
	
	public static boolean setDatosVacacionesManager(long idPersona, DetalleDiasVacaciones detalleDiasVacaciones, String numeroResolucion) {
		return new DatosDiasAdministrativosDAO().setDiasVacacionesPendientes(idPersona, detalleDiasVacaciones, numeroResolucion);
	}
	
	public static boolean setDatosDiasAdministrativosManager(long idPersona, List<DetalleDias> listDetalleDias, String numeroResolucion) {
		return new DatosDiasAdministrativosDAO().setDiasAdministrativosPendientes(idPersona, listDetalleDias, numeroResolucion);
	}
	
	public static List<DatosSolicitudTransparencia> getDatosSolicitudesTransparenciaManager(HistorialActualizacionTransparencia hat) {
		return new DatosSolicitudTransparenciaDAO().getSolicitudesTransparenciaPendientes(hat);
	}
	
	public static Boolean setDatosSolicitudesTransparenciaManager(DatosRespuestaSolicitudExterno drse) {
		return new DatosSolicitudTransparenciaDAO().setSolicitudUsuarioExterno(drse);
	}
}
