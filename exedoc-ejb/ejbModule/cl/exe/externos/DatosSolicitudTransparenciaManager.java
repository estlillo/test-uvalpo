package cl.exe.externos;

import java.util.List;

import cl.exe.exedoc.entity.HistorialActualizacionTransparencia;
import cl.exe.exedoc.util.DatosSolicitudTransparencia;

public interface DatosSolicitudTransparenciaManager {

	public List<DatosSolicitudTransparencia> getSolicitudesTransparenciaPendientes(HistorialActualizacionTransparencia hat);
	public Boolean setSolicitudUsuarioExterno(DatosRespuestaSolicitudExterno drse);
}