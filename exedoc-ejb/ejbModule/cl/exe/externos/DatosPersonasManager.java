package cl.exe.externos;

public interface DatosPersonasManager {

	public DatosPersona getDatosPersona(String rut);

	public DatosPersona getDatosPersona(Long idPersona);
}