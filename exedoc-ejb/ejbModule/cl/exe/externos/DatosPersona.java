package cl.exe.externos;

public class DatosPersona {

	public String calidadJuridica;
	public Integer grado;

	public String getCalidadJuridica() {
		return calidadJuridica;
	}

	public void setCalidadJuridica(String calidadJuridica) {
		this.calidadJuridica = calidadJuridica;
	}

	public Integer getGrado() {
		return grado;
	}

	public void setGrado(Integer grado) {
		this.grado = grado;
	}
}