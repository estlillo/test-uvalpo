package cl.exe.externos;

import java.util.Date;

public class DatosRespuestaSolicitudExterno {

	private String folio;
	private Long idEstadoSolicitud;
	private Long idEstadoRespuesta;
	private Date fechaRespuesta;
	private Long idUsuario;
	private String observacion;
	private Long orden;
	private Long idFlujoEstadoSolicitud;
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public Long getIdEstadoSolicitud() {
		return idEstadoSolicitud;
	}
	public void setIdEstadoSolicitud(Long idEstadoSolicitud) {
		this.idEstadoSolicitud = idEstadoSolicitud;
	}
	public Long getIdEstadoRespuesta() {
		return idEstadoRespuesta;
	}
	public void setIdEstadoRespuesta(Long idEstadoRespuesta) {
		this.idEstadoRespuesta = idEstadoRespuesta;
	}
	public Date getFechaRespuesta() {
		return fechaRespuesta;
	}
	public void setFechaRespuesta(Date fechaRespuesta) {
		this.fechaRespuesta = fechaRespuesta;
	}
	public Long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public Long getOrden() {
		return orden;
	}
	public void setOrden(Long orden) {
		this.orden = orden;
	}
	public Long getIdFlujoEstadoSolicitud() {
		return idFlujoEstadoSolicitud;
	}
	public void setIdFlujoEstadoSolicitud(Long idFlujoEstadoSolicitud) {
		this.idFlujoEstadoSolicitud = idFlujoEstadoSolicitud;
	}
}