package cl.exe.externos;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import cl.exe.exedoc.entity.DetalleDias;
import cl.exe.exedoc.entity.DetalleDiasVacaciones;
import cl.exe.exedoc.util.DatosSolicitud;

@Local
public interface DatosDiasAdministrativosManager {
	
	public Integer getDiasFeriadosLegalesPendientes(String rut);

	public Integer getDiasFeriadosLegalesPendientes(Long idPersona);

	public DatosSolicitud getDiasAdministrativosPendientes(String rut);

	public DatosSolicitud getDiasAdministrativosPendientes(Long idPersona);
	
	public DatosSolicitud getDiasVacacionesPendientes(Long idPersona);
	
	public Integer getMediosDiasFeriadosLegalesPendientes(String rut);

	public Integer getMediosDiasFeriadosLegalesPendientes(Long idPersona);

	public Integer getMediosDiasAdministrativosPendientes(String rut);

	public Integer getMediosDiasAdministrativosPendientes(Long idPersona);
        
	public Boolean setDiasFeriadosLegalesPendientes(String rut, Double diasFeriadosTomados);

	public Boolean setDiasFeriadosLegalesPendientes(Long idPersona, Double diasFeriadosTomados);

	public Boolean setDiasAdministrativosPendientes(String rut, Double diasAdministrativosTomados);

	public Boolean setDiasAdministrativosPendientes(Integer idPersona, Double diasAdministrativosTomados);
	
	public Boolean setMediosDiasFeriadosLegalesPendientes(String rut, Double diasFeriadosTomados);

	public Boolean setMediosDiasFeriadosLegalesPendientes(Long idPersona, Double diasFeriadosTomados);

	public Boolean setMediosDiasAdministrativosPendientes(String rut, Double diasAdministrativosTomados);

	public Boolean setMediosDiasAdministrativosPendientes(Long idPersona, Double diasAdministrativosTomados);

	public boolean setDiasVacacionesPendientes(long idPersona, DetalleDiasVacaciones detalleDiasVacaciones, String numeroResolucion);

	public boolean setDiasAdministrativosPendientes(long idPersona, List<DetalleDias> listDetalleDias, String numeroResolucion);

	boolean esDiaHabil(Date fecha);

	boolean esFeriado(Date fecha);
}