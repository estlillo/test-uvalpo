/*
 * EXE Ingenieria y Software Ltda.
 * 
 */
package cl.exe.alfresco.client.nuevo;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.alfresco.webservice.authentication.AuthenticationFault;
import org.alfresco.webservice.content.Content;
import org.alfresco.webservice.content.ContentServiceSoapBindingStub;
import org.alfresco.webservice.repository.RepositoryServiceSoapBindingStub;
import org.alfresco.webservice.repository.UpdateResult;
import org.alfresco.webservice.types.CML;
import org.alfresco.webservice.types.CMLAddAspect;
import org.alfresco.webservice.types.CMLCreate;
import org.alfresco.webservice.types.ContentFormat;
import org.alfresco.webservice.types.NamedValue;
import org.alfresco.webservice.types.Node;
import org.alfresco.webservice.types.ParentReference;
import org.alfresco.webservice.types.Predicate;
import org.alfresco.webservice.types.Reference;
import org.alfresco.webservice.types.Store;
import org.alfresco.webservice.util.AuthenticationUtils;
import org.alfresco.webservice.util.Constants;
import org.alfresco.webservice.util.Utils;
import org.alfresco.webservice.util.WebServiceException;
import org.alfresco.webservice.util.WebServiceFactory;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.xml.sax.InputSource;

import cl.exe.alfresco.client.support.AlfrescoAdapterException;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.ArchivoAdjuntoDocumentoElectronico;
import cl.exe.exedoc.entity.Departamento;
import cl.exe.exedoc.entity.Division;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.Organizacion;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.UnidadOrganizacional;
import cl.exe.exedoc.util.TextUtil;
import cl.exe.exedoc.util.singleton.AlfrescoSingleton;

/**
 * Nueva version del AlfrescoAdapter, para la subida y descarga de archivos al
 * repositorio documental Alfresco. Usa nueva funcionalidad de comunicacion con
 * el Web Service de Alfresco.
 * 
 * @author Jose Nova
 * @version 1.0 19 May 2011
 */
public class NewAlfrescoAdapter extends AbstractNewGenericAlfrescoAdapter {

	private static final String NAMESPACE_CONTENT_MODEL = "http://www.exe.cl/model/content/1.0";

	private static final String DOCUMENTOS_RESERVADOS = "Documentos_Reservados";
	private static final String DOCUMENTOS_PUBLICOS = "Documentos_Publicos";

	private final Store store = new Store(Constants.WORKSPACE_STORE,
			"SpacesStore");
	private static final String SPACES_STORE = "SpacesStore";

	private final Log log = Logging.getLog(NewAlfrescoAdapter.class);

	private String username;
	private String password;
	private String serviceURL;
	private String serviceURLService;
	private String directorioBaseBinarios;
	private String directorioBaseElectronicos;
	private String directorioBaseAnexos;

	/**
	 * Constructor vacío.
	 */
	public NewAlfrescoAdapter() {

	}

	/**
	 * Constructor.
	 * 
	 * @param username
	 *            el nombre de usuario de Alfresco.
	 * @param password
	 *            el password del usuario de Alfresco.
	 * @param serviceURL
	 *            la URL del servicio de Alfresco.
	 * @param directorioBaseBinarios
	 *            el nombre del directorio para los documentos binarios.
	 * @param directorioBaseElectronicos
	 *            el nombre del directorio para los documentos electronicos.
	 * @param directorioBaseAnexos
	 *            el nombre del directorio para los documentos anexos.
	 */
	public NewAlfrescoAdapter(final String username, final String password,
			final String serviceURL, final String directorioBaseBinarios,
			final String directorioBaseElectronicos,
			final String directorioBaseAnexos) {
		this.username = username;
		this.password = password;
		this.serviceURL = serviceURL + "/api";
		this.serviceURLService = serviceURL + "/service";
		this.directorioBaseBinarios = directorioBaseBinarios;
		this.directorioBaseElectronicos = directorioBaseElectronicos;
		this.directorioBaseAnexos = directorioBaseAnexos;
		WebServiceFactory.setEndpointAddress(this.serviceURL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cl.exe.alfresco.client.nuevo.AbstractNewGenericAlfrescoAdapter#upload
	 * (java.lang.String, cl.exe.exedoc.entity.Archivo,
	 * cl.exe.exedoc.entity.Documento)
	 */
	@Override
	public String upload(final String filename, final Archivo file,
			final Documento doc) throws RemoteException {
		String uploadURL = null;

		try {
			AuthenticationUtils.startSession(username, password);
		} catch (WebServiceException e) {
			throw new RemoteException("Error en la conexión: " + e.getMessage());
		}

		try {
			final Date fecha = file.getFecha();
			final String agno = this.getAgno(fecha);

			// Generar la estructura de directorios dentro de Alfresco.
			final Reference ref = this.createDirStructure(doc, agno);
			final ParentReference parentRef = this.getParentReference(ref,
					filename);

			final byte[] data = file.getArchivo();
			final String contentType = file.getContentType();

			// Subir el documento a Alfresco.
			uploadURL = this.commonUpload(filename, contentType, data, doc,
					parentRef, file);
		} finally {
			AuthenticationUtils.endSession();
		}

		return uploadURL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cl.exe.alfresco.client.nuevo.AbstractNewGenericAlfrescoAdapter#upload
	 * (java.lang.String, byte[], cl.exe.exedoc.entity.Documento)
	 */
	@Override
	public String upload(final String filename, final byte[] file,
			final Documento doc) throws RemoteException {
		String uploadURL = null;

		AuthenticationUtils.startSession(username, password);

		try {
			final Date fecha = doc.getFechaDocumentoOrigen();
			final String agno = this.getAgno(fecha);

			// Generar la estructura de directorios dentro de Alfresco.
			final Reference ref = this.createDirStructure(doc, agno);
			final ParentReference parentRef = this.getParentReference(ref,
					filename);

			// Para los documentos electronicos, el archivo siempre será subido
			// en formato XML.
			final String contentType = "text/xml";

			// Subir el documento a Alfresco.
			uploadURL = this.commonUpload(filename, contentType, file, doc,
					parentRef, null);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			AuthenticationUtils.endSession();
		}

		return uploadURL;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cl.exe.alfresco.client.nuevo.AbstractNewGenericAlfrescoAdapter#download
	 * (java.lang.String)
	 */
	@Override
	public byte[] download(final String url) throws IOException,
			XPathExpressionException {
		byte[] file = null;

		final HttpClient client = new HttpClient();
		final StringBuilder urlBuilder = new StringBuilder(this.getPreURL())
				.append(url).append("?ticket=").append(this.getTicket());
		final String urlAlfresco = urlBuilder.toString();
		final GetMethod method = new GetMethod(urlAlfresco);

		try {
			client.executeMethod(method);
			file = method.getResponseBody();
		} finally {
			method.releaseConnection();
		}

		return file;
	}

	/**
	 * Metodo para obtener el ticket de Alfresco.
	 * 
	 * @return un string con el ticket de Alfresco.
	 * 
	 * @throws IOException
	 *             si ocurre un problema ejecutando el HTTP GET.
	 * @throws XPathExpressionException
	 *             si ocurre un problema obteniendo el ticket.
	 */
	private String getTicket() throws IOException, XPathExpressionException {
		String ticket = null;
		final StringBuilder urlBuilder = new StringBuilder(serviceURLService)
				.append("/api/login?u=").append(username).append("&pw=")
				.append(password);
		final String url = urlBuilder.toString();

		final HttpClient client = new HttpClient();
		final GetMethod method = new GetMethod(url);

		try {
			client.executeMethod(method);
			final InputStream ticketStream = method.getResponseBodyAsStream();
			final XPath xpath = XPathFactory.newInstance().newXPath();
			final String expression = "/ticket";
			final InputSource inputSource = new InputSource(ticketStream);

			ticket = (String) xpath.evaluate(expression, inputSource,
					XPathConstants.STRING);
		} finally {
			method.releaseConnection();
		}

		return ticket;
	}

	/**
	 * Metodo para obtener la URL previa a la URL de descarga.
	 * 
	 * @return la URL con el formato
	 *         http://ip:8080/alfresco/download/direct/cms_id.
	 */
	private String getPreURL() {

		return AlfrescoSingleton.SERVICE_URL + "/download/direct/";
	}

	/**
	 * Metodo para obtener el año de una fecha dada.
	 * 
	 * @param fecha
	 *            la fecha inicial.
	 * @return el año, como string.
	 */
	private String getAgno(final Date fecha) {
		final SimpleDateFormat dfAgno = new SimpleDateFormat("yyyy");
		return dfAgno.format(fecha);
	}

	/**
	 * Metodo que crea la estructura de directorios para albergar los archivos
	 * subidos a Alfresco.
	 * 
	 * @param doc
	 *            el documento tomado como base para generar la estructura de
	 *            documentos.
	 * @param agno
	 *            el año, como string.
	 * @return la referencia al directorio creado.
	 * @throws RemoteException
	 *             en caso de que no se haya podido crear el directorio.
	 */
	private Reference createDirStructure(final Documento doc, final String agno)
			throws RemoteException {
		final Integer idFormatoDoc = doc.getFormatoDocumento().getId();
		Reference ref = null;

		if (FormatoDocumento.ELECTRONICO.equals(idFormatoDoc)) {
			final List<ArchivoAdjuntoDocumentoElectronico> archivos = ((DocumentoElectronico) doc)
					.getArchivosAdjuntos();
			if (archivos != null && !archivos.isEmpty()) {
				for (ArchivoAdjuntoDocumentoElectronico archivo : archivos) {
					if (archivo.getCmsId() == null) {
						return this.generateSpaces(directorioBaseAnexos, doc,
								agno);
					}
				}
			}
			ref = this.generateSpaces(directorioBaseElectronicos, doc, agno);
		} else if (FormatoDocumento.DIGITAL.equals(idFormatoDoc)) {
			// Si el documento ya ha sido guardado en Alfresco, significa que
			// se desea guardar un documento anexo.
			if (doc.getCmsId() == null) {
				ref = this.generateSpaces(directorioBaseBinarios, doc, agno);
			} else {
				ref = this.generateSpaces(directorioBaseAnexos, doc, agno);
			}
		} else {
			ref = this.generateSpaces(directorioBaseAnexos, doc, agno);
		}

		return ref;
	}

	/**
	 * Metodo para obtener la referencia padre de una referencia dada.
	 * 
	 * @param ref
	 *            la referencia de la cual se desea obtener su referencia padre.
	 * @param filename
	 *            el archivo asociado a la referencia.
	 * @return la referencia padre.
	 */
	private ParentReference getParentReference(final Reference ref,
			final String filename) {
		final ParentReference parentRef = this.referenceToParent(ref);
		parentRef.setChildName(Constants.createQNameString(
				Constants.NAMESPACE_CONTENT_MODEL,
				this.normalizeNodeName(filename)));

		return parentRef;
	}

	/**
	 * Metodo con la funcionalidad comun para la subida de archivos a Alfresco.
	 * 
	 * @param filename
	 *            el nombre del archivo.
	 * @param contentType
	 *            el tipo de contenido del archivo.
	 * @param data
	 *            el archivo como arreglo de bytes.
	 * @param doc
	 *            el documento que referencia el archivo.
	 * @param parentRef
	 *            el nodo padre del directorio donde se desea subir el archivo.
	 * @return la URL del archivo subido a Alfresco.
	 * @throws RemoteException
	 *             si el archivo no se puede subir.
	 */
	private String commonUpload(final String filename,
			final String contentType, final byte[] data, final Documento doc,
			final ParentReference parentRef, final Archivo file)
			throws RemoteException {
		final String encoding = "UTF8";

		// Generar la metadata para el documento a subir a Alfresco.
		final NamedValue[] metadata = MetaDataUtils.createMetadata(filename,
				doc, file);
		final CMLCreate create = new CMLCreate("0", parentRef, null, null,
				null, Constants.createQNameString(NAMESPACE_CONTENT_MODEL,
						this.getSpacenameId(doc)), metadata);

		// Subir el documento a Alfresco.
		final CML cml = new CML();
		cml.setCreate(new CMLCreate[] { create });
		final UpdateResult[] results = WebServiceFactory.getRepositoryService()
				.update(cml);
		final Reference docRef = results[0].getDestination();
		final ContentServiceSoapBindingStub contentService = WebServiceFactory
				.getContentService();
		final ContentFormat contentFormat = new ContentFormat(contentType,
				encoding);
		final Content docContentRef = contentService.write(docRef,
				Constants.PROP_CONTENT, data, contentFormat);

		if (docContentRef != null && docContentRef.getUrl() != null) {
			final Reference node = docContentRef.getNode();
			return node.getStore().getScheme() + "/"
					+ node.getStore().getAddress() + "/" + node.getUuid() + "/"
					+ filename;
		}

		return null;
	}

	/**
	 * Metodo que crea la estructura de directorios recursivamente, conforme se
	 * vayan subiendo los archivos a Alfresco.
	 * 
	 * @param directorioBase
	 *            el nombre del directorio base (binario, electronico o anexo).
	 * @param doc
	 *            el documento que se desea guardar.
	 * @param directorioAgno
	 *            el año.
	 * @return la referencia al directorio creado, como parte de la estructura.
	 * @throws RemoteException
	 *             si el directorio no se pudo crear.
	 */
	private Reference generateSpaces(final String directorioBase,
			final Documento doc, final String directorioAgno)
			throws RemoteException {
		Reference ref = null;
		// Obtener la estructura de directorios desde el autor del documento.
//		final UnidadOrganizacional unidadUploader = doc.getAutor().getCargo()
//				.getUnidadOrganizacional();
//		final Departamento deptoUploader = unidadUploader.getDepartamento();
//		final Division divisionUploader = deptoUploader.getDivision();
//		final Organizacion organizacionUploader = divisionUploader
//				.getOrganizacion();

		final UnidadOrganizacional unidadUploader = doc.getAutor().getCargo()
		.getUnidadOrganizacional();
		final Departamento deptoUploader = unidadUploader.getDepartamento();
		final Division divisionUploader = deptoUploader.getDivision();
		final Organizacion organizacionUploader = divisionUploader
		.getOrganizacion();
		
		Reference ref1,ref2,ref3,ref4,ref5,ref6,ref7,ref8;
		
		if (directorioBase.equals(directorioBaseBinarios)) {
//			ref = this.createSpace(this.createSpace(
//					this.createSpace(this.createSpace(this.createSpace(this
//							.createSpace(this.createSpace(
//									this.createSpace(null, directorioBase),
//									organizacionUploader.getDescripcion()),
//									divisionUploader.getDescripcion()),
//							deptoUploader.getDescripcion()), unidadUploader
//							.getDescripcion()), this.setTipoDocDirName(doc)),
//					"_" + directorioAgno), this.setFinalLeaveDir(doc,
//					directorioBase, directorioAgno));
			ref8 = this.createSpace(null, directorioBase);
			ref7 = this.createSpace(ref8, "_" + organizacionUploader.getId().toString());
			ref6 = this.createSpace(ref7, "_" + divisionUploader.getId().toString());
			ref5 = this.createSpace(ref6, "_" + deptoUploader.getId().toString());
			ref4 = this.createSpace(ref5, "_" + unidadUploader.getId().toString());
			ref3 = this.createSpace(ref4, "_" + this.setTipoDocDirName(doc));
			ref2 = this.createSpace(ref3, "_" + directorioAgno);
			ref1 = this.createSpace(ref2,setFinalLeaveDir(doc,directorioBase, directorioAgno));
 
			ref = ref1;
//			ref = this.createSpace(
//					this.createSpace(
//						this.createSpace(
//							this.createSpace(
//								this.createSpace(
//									this.createSpace(
//										this.createSpace(
//											this.createSpace(null, directorioBase),
//											organizacionUploader.getId().toString()
//										),
//										divisionUploader.getId().toString()
//									),
//									deptoUploader.getId().toString()
//								), 
//								unidadUploader.getId().toString()
//							), 
//						this.setTipoDocDirName(doc)
//						),
//						"_" + directorioAgno
//					), 
//				    this.setFinalLeaveDir(doc,directorioBase, directorioAgno)
//				 );
		}

		if (directorioBase.equals(directorioBaseElectronicos)) {

			ref8 = this.createSpace(null, directorioBase);
			ref7 = this.createSpace(ref8, "_" + organizacionUploader.getId().toString());
			ref6 = this.createSpace(ref7, "_" + divisionUploader.getId().toString());
			ref5 = this.createSpace(ref6, "_" + deptoUploader.getId().toString());
			ref4 = this.createSpace(ref5, "_" + unidadUploader.getId().toString());
			ref3 = this.createSpace(ref4, "_" + this.setTipoDocDirName(doc));
			ref2 = this.createSpace(ref3,setFinalLeaveDir(doc,directorioBase, directorioAgno));
			ref = ref2;
			
//			ref = this.createSpace(
//					this.createSpace(this.createSpace(this.createSpace(this
//							.createSpace(this.createSpace(
//									this.createSpace(null, directorioBase),
//									organizacionUploader.getDescripcion()),
//									divisionUploader.getDescripcion()),
//							deptoUploader.getDescripcion()), unidadUploader
//							.getDescripcion()), this.setTipoDocDirName(doc)),
//					this.setFinalLeaveDir(doc, directorioBase, directorioAgno));
		}

		if (directorioBase.equals(directorioBaseAnexos)) {
//			ref = this.createSpace(this.createSpace(this.createSpace(this
//					.createSpace(this.createSpace(
//							this.createSpace(null, directorioBase),
//							organizacionUploader.getDescripcion()),
//							divisionUploader.getDescripcion()), deptoUploader
//					.getDescripcion()), unidadUploader.getDescripcion()), this
//					.setFinalLeaveDir(doc, directorioBase, directorioAgno));
			ref8 = this.createSpace(null, directorioBase);
			ref7 = this.createSpace(ref8, "_" + organizacionUploader.getId().toString());
			ref6 = this.createSpace(ref7, "_" + divisionUploader.getId().toString());
			ref5 = this.createSpace(ref6, "_" + deptoUploader.getId().toString());
			ref4 = this.createSpace(ref5, "_" + unidadUploader.getId().toString());
			ref3 = this.createSpace(ref4, "_" + this.setTipoDocDirName(doc));
			ref2 = this.createSpace(ref3,setFinalLeaveDir(doc,directorioBase, directorioAgno));
			ref = ref2;
		}

		return ref;
	}

	/**
	 * Metodo para obtener el directorio padre de un determinado directorio.
	 * 
	 * @param spaceRef
	 *            la referencia al directorio del que se quiere obtener el
	 *            padre.
	 * @return la referencia al directorio padre.
	 */
	private ParentReference referenceToParent(final Reference spaceRef) {
		final ParentReference parent = new ParentReference();

		parent.setStore(store);
		parent.setPath(spaceRef.getPath());
		parent.setUuid(spaceRef.getUuid());
		parent.setAssociationType(Constants.ASSOC_CONTAINS);

		return parent;
	}

	/**
	 * Los nombres de las rutas no pueden llevar espacios. Esta funcion
	 * convierte los posibles espacios en underscores.
	 * 
	 * @param name
	 *            el nombre del directorio.
	 * @return el nombre del directorio, sin espacios.
	 */
	private String normalizeNodeName(String name) {

		log.info(
				"{0}",
				TextUtil.eliminaTildes(name)
						.replaceAll("[^a-zA-Z0-9\\s\\_\\á\\é\\í\\ó\\ú\\Á\\É\\Í\\Ó\\Ú]", "").replace(" ", "_"));

		return name.replaceAll("[^a-zA-Z0-9\\s\\_\\á\\é\\í\\ó\\ú\\Á\\É\\Í\\Ó\\Ú]", "").replace(" ", "_");
	}

	/**
	 * Metodo para obtener el nombre del id del spacename (directorio) en
	 * Alfresco.
	 * 
	 * @param doc
	 *            el documento.
	 * @return el nombre del id del spacename.
	 */
	private String getSpacenameId(final Documento doc) {
		String spacenameId = null;
		if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL)) {
			if (doc.getCmsId() == null) {
				spacenameId = "documentoBinario";
			} else {
				spacenameId = "documentoAnexo";
			}
		} else if (doc.getFormatoDocumento().getId()
				.equals(FormatoDocumento.ELECTRONICO)) {
			final List<ArchivoAdjuntoDocumentoElectronico> archivos = ((DocumentoElectronico) doc)
					.getArchivosAdjuntos();
			if (archivos != null) {
				for (ArchivoAdjuntoDocumentoElectronico archivo : archivos) {
					if (archivo.getCmsId() == null) {
						return "documentoAnexo";
					}
				}
			}
			spacenameId = this.getSpacenameIdElectronico(doc);
		} else {
			spacenameId = "documentoAnexo";
		}

		return spacenameId;
	}

	/**
	 * Metodo para obtener el nombre del id del spacename (directorio) en
	 * Alfresco, para un documento electronico.
	 * 
	 * @param doc
	 *            el documento.
	 * @return el nombre del id del spacename.
	 */
	private String getSpacenameIdElectronico(final Documento doc) {
		String spacenameIdElectronico = null;
		final Integer idTipoDoc = doc.getTipoDocumento().getId();

		if (TipoDocumento.DECRETO.equals(idTipoDoc)) {
			spacenameIdElectronico = "decreto";
		} else if (TipoDocumento.MEMORANDUM.equals(idTipoDoc)) {
			spacenameIdElectronico = "memorandum";
		} else if (TipoDocumento.RESOLUCION.equals(idTipoDoc)) {
			spacenameIdElectronico = "resolucion";
		}
		else if (TipoDocumento.OFICIO_ORDINARIO.equals(idTipoDoc) || TipoDocumento.OFICIO_CIRCULAR.equals(idTipoDoc) || TipoDocumento.OFICIO_RESERVADO.equals(idTipoDoc)) {
			spacenameIdElectronico = "oficio";
		}
		else if (TipoDocumento.CARTA.equals(idTipoDoc)) {
			spacenameIdElectronico = "carta";
		}
		return spacenameIdElectronico;
	}

	/**
	 * Metodo que setea el nombre del directorio que corresponde al tipo del
	 * documento.
	 * 
	 * @param doc
	 *            el documento que se desea guardar.
	 * @return el tipo del documento. Si el documento es binario o electronico,
	 *         retorna null.
	 */
	private String setTipoDocDirName(final Documento doc) {
		final Integer idFormatoDoc = doc.getFormatoDocumento().getId();
//		return FormatoDocumento.DIGITAL.equals(idFormatoDoc)
//		|| FormatoDocumento.ELECTRONICO.equals(idFormatoDoc) ? doc
//		.getTipoDocumento().getDescripcion() : null;
		return FormatoDocumento.DIGITAL.equals(idFormatoDoc)
		|| FormatoDocumento.ELECTRONICO.equals(idFormatoDoc) ? doc
		.getTipoDocumento().getId().toString() : null;
	}

	/**
	 * Metodo que crea un espacio (directorio) en Alfresco, dado su nombre y su
	 * directorio padre.
	 * 
	 * @param parentRef
	 *            la referencia al directorio padre.
	 * @param spacename
	 *            el nombre que se le dara al nuevo directorio.
	 * @return la referencia al nuevo directorio creado.
	 * @throws RemoteException
	 *             si el directorio no se puede crear.
	 */
	private Reference createSpace(final Reference parentRef,
			final String spacename) throws RemoteException {
		Reference space = null;
		ParentReference parent = null;

		if (parentRef != null) {
			parent = this.referenceToParent(parentRef);
		} else {
			// TODO: company home
			parent = this.getCompanyHome();
		}

		try {
			log.info("Entrando a directorio #0", spacename);
			space = new Reference(store, null, parent.getPath() + "/cm:"
					+ this.normalizeNodeName(spacename));
			WebServiceFactory.getRepositoryService().get(
					new Predicate(new Reference[] { space }, store, null));
		} catch (RemoteException e1) {
			log.info("Espacio #0 no existe. Creándolo...", spacename);

			// Seteando el directorio padre.
			parent.setChildName(Constants.createQNameString(
					Constants.NAMESPACE_CONTENT_MODEL,
					this.normalizeNodeName(spacename)));

			// Asigna el nombre al directorio.
			final NamedValue[] properties = new NamedValue[] { Utils
					.createNamedValue(Constants.PROP_NAME, spacename), };

			// Crea el espacio usando CML (Content Manipulation Language).
			final CMLCreate create = new CMLCreate("1", parent, null, null,
					null, Constants.TYPE_FOLDER, properties);
			final CML cml = new CML();
			cml.setCreate(new CMLCreate[] { create });

			// Ejecuta la sentencia CML de creacion.
			WebServiceFactory.getRepositoryService().update(cml);
		}

		return space;
	}

	/**
	 * Metodo que setea el nombre del directorio que debe ir al final de la
	 * estructura de directorios en Alfresco.
	 * 
	 * @param doc
	 *            el documento que se desea guardar.
	 * @param agno
	 *            string con el agno.
	 * @return el nombre del directorio, dependiendo de si el documento es o no
	 *         reservado. Si es un documento electronico o tiene su valor
	 *         reservado nulo, se retorna null.
	 */
	private String setFinalLeaveDir(final Documento doc,
			final String directorioBase, final String agno) {
		String finalLeaveDir;
		if (directorioBase.equals(directorioBaseAnexos)
				|| directorioBase.equals(directorioBaseElectronicos)) {
			finalLeaveDir = "_" + agno;
		} else if (!doc.getReservado()) {
			finalLeaveDir = DOCUMENTOS_PUBLICOS;
		} else {
			finalLeaveDir = DOCUMENTOS_RESERVADOS;
		}
		return finalLeaveDir;
	}

	/**
	 * Metodo para obtener el directorio raiz de Alfresco.
	 * 
	 * @return la referencia al directorio raiz de Alfresco.
	 */
	private ParentReference getCompanyHome() {
		return new ParentReference(store, null,
				"/app:company_home/app:user_homes/sys:" + username,
				Constants.ASSOC_CONTAINS, null);
	}
	
	//this does not work correctly, use at your own risk
	@Override
	public void makeVersionableContent(final String destinationNodeUuid) throws AlfrescoAdapterException {
		try {
			// Iniciar sesion
			AuthenticationUtils.startSession(username, password);

			// Obtener repositorio
			RepositoryServiceSoapBindingStub repositoryService;
			repositoryService = WebServiceFactory.getRepositoryService();

			// Obtener referencia al nodo
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference referenceVersion = new Reference(storeRef, destinationNodeUuid, null);
			final Predicate predicateVersion = new Predicate(new Reference[] { referenceVersion, }, null, null);

			// Crear aspecto versionable
			final CMLAddAspect addVersionableAspect = new CMLAddAspect();
			addVersionableAspect.setAspect(Constants.ASPECT_VERSIONABLE);
			addVersionableAspect.setWhere(predicateVersion);

			// Crear bloque CML
			final CML cml = new CML();
			cml.setAddAspect(new CMLAddAspect[] { addVersionableAspect, });

			repositoryService.update(cml);

		} catch (Exception e) {
			log.error(e.getMessage());
			log.error(e.getClass().getName());
			e.printStackTrace();
			throw new AlfrescoAdapterException(true, "Exception en: \"makeVersionableContent\"",
					"Exception: \"makeVersionableContent\"", e);
		} finally {
			// Terminar sesion
			AuthenticationUtils.endSession();
		}
	}
	    
	    
    @Override
    public void update(final Archivo file, final Documento doc) throws RemoteException {
        String uploadURL = null;
        
        AuthenticationUtils.startSession(username, password);
        
        try {
            final String contentType = "text/xml";
            final byte[] data = file.getArchivo();
            String[] arr = doc.getCmsId().split("[/]");
            //System.out.println("uuid:" + arr[arr.length - 2]);
            updateContent(contentType, data, arr[arr.length - 2]);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            AuthenticationUtils.endSession();
        }
    }
	    
    @Override
    public void update(final byte[] file, final Documento doc) throws RemoteException {
        String uploadURL = null;
        
        AuthenticationUtils.startSession(username, password);
        
        try {
            final String contentType = "text/xml";
            String[] arr = doc.getCmsId().split("[/]");
            updateContent(contentType, file, arr[arr.length - 2]);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            AuthenticationUtils.endSession();
        }
    }
	    
    @Override
    public void updateContent(final String mimeType, final byte[] content,
			final String destinationNodeUUID) throws AlfrescoAdapterException {
    	final String encoding = "UTF-8";
		try {
			AuthenticationUtils.startSession(username, password);
		} catch (AuthenticationFault fault) {
			log.error("fallo la autenticacion al fresco");
		}

		try {
			// Start the session
			ContentServiceSoapBindingStub contentService = WebServiceFactory.getContentService();

			// Create a reference to the parent where we want to insert content
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference reference = new Reference(storeRef, destinationNodeUUID, null);

			final Content[] readResult = contentService.read(new Predicate(new Reference[] { reference }, storeRef,
					null), Constants.PROP_CONTENT);
			final Content existingContent = readResult[0];
			final Reference existingReference = existingContent.getNode();

			final ContentFormat format = new ContentFormat(mimeType, encoding);
			contentService.write(existingReference, Constants.PROP_CONTENT, content, format);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			throw new AlfrescoAdapterException(true, "Exception occurred during \"updateContent\"",
					"Exception occurred during \"updateContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

}
