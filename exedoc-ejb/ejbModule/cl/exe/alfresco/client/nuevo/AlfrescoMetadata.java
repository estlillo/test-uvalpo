/*
 * EXE Ingenieria y Software Ltda.
 * 
 */
package cl.exe.alfresco.client.nuevo;

import java.util.Date;

import org.alfresco.webservice.types.NamedValue;
import org.alfresco.webservice.util.Constants;
import org.alfresco.webservice.util.Utils;

/**
 * Clase que maneja la estructura de la metadata del archivo a subir a Alfresco,
 * sin importar si el documento es binario, electronico o anexo.
 * 
 * @author Jose Nova
 * @version 1.0 29 Mar 2011
 */
public class AlfrescoMetadata {

    private static final String NAMESPACE_CONTENT_MODEL = "http://www.exe.cl/model/content/1.0";
    
    private String tipoDoc;
    private String numDoc;
    private String numDocRaiz;
    private String reservadoDoc;
    private Date fechaDoc;
    private String materiaDoc;
    private String emisorDoc;
    private String destinatariosDoc;
    private Date plazoDoc;
    private String tipoDecreto;
    private String adjuntadoPorDoc;
    
    /**
     * Constructor.
     */
    public AlfrescoMetadata() {
        
    }
    
    /**
     * Metodo que transforma un valor cualquiera en un NamedValue para Alfresco.
     * 
     * @param name
     *            el nombre de la variable dentro del modelo de metadatos de
     *            Alfresco.
     * @param value
     *            el valor que se desea transformar.
     * @return el valor transformado a NamedValue.
     */
    public NamedValue transform(final String name, final String value) {
        return Utils.createNamedValue(Constants.createQNameString(NAMESPACE_CONTENT_MODEL, name), value != null ? value
                : "");
    }

    /**
     * @return the tipoDoc
     */
    public String getTipoDoc() {
        return tipoDoc;
    }

    /**
     * @param tipoDoc the tipoDoc to set
     */
    public void setTipoDoc(final String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    /**
     * @return the numDoc
     */
    public String getNumDoc() {
        return numDoc;
    }

    /**
     * @param numDoc the numDoc to set
     */
    public void setNumDoc(final String numDoc) {
        this.numDoc = numDoc;
    }

    /**
     * @return the numDocRaiz
     */
    public String getNumDocRaiz() {
        return numDocRaiz;
    }

    /**
     * @param numDocRaiz the numDocRaiz to set
     */
    public void setNumDocRaiz(final String numDocRaiz) {
        this.numDocRaiz = numDocRaiz;
    }

    /**
     * @return the reservadoDoc
     */
    public String getReservadoDoc() {
        return reservadoDoc;
    }

    /**
     * @param reservadoDoc the reservadoDoc to set
     */
    public void setReservadoDoc(final String reservadoDoc) {
        this.reservadoDoc = reservadoDoc;
    }

    /**
     * @return the fechaDoc
     */
    public Date getFechaDoc() {
        return fechaDoc;
    }

    /**
     * @param fechaDoc the fechaDoc to set
     */
    public void setFechaDoc(final Date fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    /**
     * @return the materiaDoc
     */
    public String getMateriaDoc() {
        return materiaDoc;
    }

    /**
     * @param materiaDoc the materiaDoc to set
     */
    public void setMateriaDoc(final String materiaDoc) {
        this.materiaDoc = materiaDoc;
    }

    /**
     * @return the emisorDoc
     */
    public String getEmisorDoc() {
        return emisorDoc;
    }

    /**
     * @param emisorDoc the emisorDoc to set
     */
    public void setEmisorDoc(final String emisorDoc) {
        this.emisorDoc = emisorDoc;
    }

    /**
     * @return the destinatariosDoc
     */
    public String getDestinatariosDoc() {
        return destinatariosDoc;
    }

    /**
     * @param destinatariosDoc the destinatariosDoc to set
     */
    public void setDestinatariosDoc(final String destinatariosDoc) {
        this.destinatariosDoc = destinatariosDoc;
    }

    /**
     * @return the plazoDoc
     */
    public Date getPlazoDoc() {
        return plazoDoc;
    }

    /**
     * @param plazoDoc the plazoDoc to set
     */
    public void setPlazoDoc(final Date plazoDoc) {
        this.plazoDoc = plazoDoc;
    }

    /**
     * @return the tipoDecreto
     */
    public String getTipoDecreto() {
        return tipoDecreto;
    }

    /**
     * @param tipoDecreto the tipoDecreto to set
     */
    public void setTipoDecreto(final String tipoDecreto) {
        this.tipoDecreto = tipoDecreto;
    }

    /**
     * @return the adjuntadoPorDoc
     */
    public String getAdjuntadoPorDoc() {
        return adjuntadoPorDoc;
    }

    /**
     * @param adjuntadoPorDoc the adjuntadoPorDoc to set
     */
    public void setAdjuntadoPorDoc(final String adjuntadoPorDoc) {
        this.adjuntadoPorDoc = adjuntadoPorDoc;
    }

}
