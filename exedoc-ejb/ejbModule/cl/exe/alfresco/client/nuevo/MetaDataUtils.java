package cl.exe.alfresco.client.nuevo;

import java.text.SimpleDateFormat;

import org.alfresco.webservice.types.NamedValue;
import org.alfresco.webservice.util.Constants;
import org.alfresco.webservice.util.Utils;

import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Decreto;
import cl.exe.exedoc.entity.Documento;
import cl.exe.exedoc.entity.FormatoDocumento;
import cl.exe.exedoc.entity.TipoDocumento;
import cl.exe.exedoc.entity.descriptores.ArchivoAdjunto;

/**
 * Clase utilitaria para crear la metadata de un archivo adjunto.
 */
public final class MetaDataUtils {

	private static final String HORAS = "T00:00:00.000+00:00";

	/**
	 * Constructor.
	 */
	private MetaDataUtils() {
		super();
	}

	/**
	 * Metodo para crear la metadata del documento a subir a Alfresco.
	 * 
	 * @param filename el nombre del archivo a subir.
	 * @param doc el documento asociado al archivo a subir.
	 * @param file {@link Archivo} que sera adjuntado.
	 * @return un arreglo con la metadata asociada.
	 */
	public static NamedValue[] createMetadata(final String filename, final Documento doc, final Archivo file) {
		final SimpleDateFormat dfFecha = new SimpleDateFormat("yyyy-MM-dd");
		NamedValue[] metadata = null;

		if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.ELECTRONICO)) {
			// Preguntar si es un archivo adjunto.
		    if (file instanceof ArchivoAdjunto && file.getCmsId() == null) {
			    return MetaDataUtils.createMetadataAnexo(filename, doc, dfFecha, file);
			}
		    metadata = MetaDataUtils.createMetadataElectronico(filename, doc, dfFecha, file);
		} else if (doc.getFormatoDocumento().getId().equals(FormatoDocumento.DIGITAL)) {
		    // Si el documento ya ha sido guardado en Alfresco, significa que
            // se desea guardar la metadata de un documento anexo.
		    if (doc.getCmsId() == null) {
		        metadata = MetaDataUtils.createMetadataBinario(filename, doc, dfFecha, file);
            } else {
                metadata = MetaDataUtils.createMetadataAnexo(filename, doc, dfFecha, file);
            }
		} else{
			metadata = MetaDataUtils.createMetadataAnexo(filename, doc, dfFecha, file);
		}
		
		return metadata;
	}

	/**
	 * Metodo para crear la metadata de un documento electronico a subir a Alfresco.
	 * 
	 * @param filename el nombre del archivo XML a subir.
	 * @param doc el documento asociado al archivo a subir.
	 * @param dfFecha el formato de fecha para setear la fecha del documento.
	 * @param file {@link Archivo} que sera adjuntado.
	 * @return un arreglo con la metadata asociada al documento electronico.
	 */
	private static NamedValue[] createMetadataElectronico(final String filename, final Documento doc,
			final SimpleDateFormat dfFecha, final Archivo file) {
		final TipoDocumento tipoDoc = doc.getTipoDocumento();
		final Integer idTipoDoc = doc.getTipoDocumento().getId();

		// Llenar estructura con metadata.
		final AlfrescoMetadata electrMetadata = new AlfrescoMetadata();
		electrMetadata.setTipoDoc(tipoDoc.getDescripcion());
		electrMetadata.setNumDoc(doc.getNumeroDocumento());
		electrMetadata.setFechaDoc(doc.getFechaDocumentoOrigen());
		electrMetadata.setMateriaDoc(doc.getMateria());
		electrMetadata.setPlazoDoc(doc.getPlazo());

		// Los decretos no tienen ni emisor, ni destinatarios ni plazo.
		if (!TipoDocumento.DECRETO.equals(tipoDoc.getId())) {
			electrMetadata.setEmisorDoc(doc.getEmisor());
			electrMetadata.setDestinatariosDoc(doc.getDestinatarios().toString());
			electrMetadata.setPlazoDoc(doc.getPlazo());
		}

		// Campos especificos de decretos y resoluciones.
		if (TipoDocumento.DECRETO.equals(tipoDoc.getId())) {
			electrMetadata.setTipoDecreto(((Decreto) doc).getTipo().getDescripcion());
		}
		
		// Crear NamedValues para la metadata de Alfresco.
		final NamedValue nameValue = Utils.createNamedValue(Constants.PROP_NAME, filename);
		final NamedValue description = Utils.createNamedValue(Constants.PROP_DESCRIPTION, "");
		final NamedValue numeroDocumento = electrMetadata.transform(
				"numeroDocumento" + MetaDataUtils.getTipoDocSuffix(idTipoDoc), electrMetadata.getNumDoc());
		final NamedValue fecha = electrMetadata.transform("fecha" + MetaDataUtils.getTipoDocSuffix(idTipoDoc),
				dfFecha.format(electrMetadata.getFechaDoc()) + HORAS);
		final NamedValue materia = electrMetadata.transform("materia" + MetaDataUtils.getTipoDocSuffix(idTipoDoc),
				electrMetadata.getMateriaDoc());
		final NamedValue emisor = electrMetadata.transform("emisor" + MetaDataUtils.getTipoDocSuffix(idTipoDoc),
				electrMetadata.getEmisorDoc());
		final NamedValue destinatarios = electrMetadata.transform(
				"destinatarios" + MetaDataUtils.getTipoDocSuffix(idTipoDoc), electrMetadata.getDestinatariosDoc());
		NamedValue plazo = null;
		if (electrMetadata.getPlazoDoc() != null) {
			plazo = electrMetadata.transform("plazo" + MetaDataUtils.getTipoDocSuffix(idTipoDoc),
				dfFecha.format(electrMetadata.getPlazoDoc()) + HORAS);
		}
		final NamedValue tipoDecreto = electrMetadata.transform("tipoDecreto", electrMetadata.getTipoDecreto());
		if (electrMetadata.getPlazoDoc() != null) {
			return new NamedValue[] {nameValue, description, numeroDocumento, fecha, materia, emisor, destinatarios,
					plazo, tipoDecreto, };
		} else {
			return new NamedValue[] {nameValue, description, numeroDocumento, fecha, materia, emisor, destinatarios,
					tipoDecreto, };
		}
	}

	/**
	 * Metodo para crear la metadata de un documento binario a subir a Alfresco.
	 * 
	 * @param filename el nombre del archivo a subir.
	 * @param doc el documento asociado al archivo a subir.
	 * @param dfFecha el formato de fecha para setear la fecha del documento.
	 * @param file {@link Archivo} que sera adjuntado.
	 * @return un arreglo con la metadata asociada al documento binario.
	 */
	private static NamedValue[] createMetadataBinario(final String filename, final Documento doc,
			final SimpleDateFormat dfFecha, final Archivo file) {
		// Llenar estructura con metadata.
		final AlfrescoMetadata binMetadata = new AlfrescoMetadata();
		binMetadata.setTipoDoc(doc.getTipoDocumento().getDescripcion());
		binMetadata.setNumDoc(doc.getNumeroDocumento());
		binMetadata.setReservadoDoc(doc.getReservado().toString());
		binMetadata.setMateriaDoc(doc.getMateria());
		binMetadata.setFechaDoc(doc.getFechaDocumentoOrigen());
		binMetadata.setPlazoDoc(doc.getPlazo());
		binMetadata.setEmisorDoc(doc.getEmisor());
		binMetadata.setDestinatariosDoc(doc.getDestinatarios().toString());

		// Crear NamedValues para la metadata de Alfresco.
		final NamedValue nameValue = Utils.createNamedValue(Constants.PROP_NAME, filename);
		final NamedValue description = Utils.createNamedValue(Constants.PROP_DESCRIPTION, "");
		final NamedValue tipoDocumento = binMetadata.transform("tipoDocumentoBinario", binMetadata.getTipoDoc());
		final NamedValue numeroDocumento = binMetadata.transform("numeroDocumentoBinario", binMetadata.getNumDoc());
		final NamedValue reservado = binMetadata.transform("reservadoBinario", binMetadata.getReservadoDoc());
		final NamedValue materia = binMetadata.transform("materiaBinario", binMetadata.getMateriaDoc());
		final NamedValue fecha = binMetadata.transform("fechaBinario", dfFecha.format(binMetadata.getFechaDoc())
				+ HORAS);
		NamedValue plazo = null;
		if(binMetadata.getPlazoDoc() != null){
			 plazo = binMetadata.transform("plazoBinario", dfFecha.format(binMetadata.getPlazoDoc()) + HORAS);
		}
		final NamedValue emisor = binMetadata.transform("emisorBinario", binMetadata.getEmisorDoc());
		final NamedValue destinatarios = binMetadata.transform("destinatariosBinario",
				binMetadata.getDestinatariosDoc());

		if(binMetadata.getPlazoDoc() == null){
			return new NamedValue[] {nameValue, description, tipoDocumento, numeroDocumento, reservado, materia, fecha,
					 emisor, destinatarios, };
		}
		else{
			return new NamedValue[] {nameValue, description, tipoDocumento, numeroDocumento, reservado, materia, fecha,
					plazo, emisor, destinatarios, };
		}
		
	}

	/**
	 * Metodo para crear la metadata de un documento Anexo a subir a Alfresco.
	 * 
	 * @param filename el nombre del archivo a subir.
	 * @param doc el documento asociado al archivo a subir.
	 * @param dfFecha el formato de fecha para setear la fecha del documento.
	 * @param file {@link Archivo} que sera adjuntado.
	 * @return un arreglo con la metadata asociada al documento binario.
	 */
	private static NamedValue[] createMetadataAnexo(final String filename, final Documento doc,
			final SimpleDateFormat dfFecha, final Archivo file) {
		// Llenar estructura con metadata.
		final AlfrescoMetadata metaData = new AlfrescoMetadata();
		metaData.setNumDocRaiz(doc.getNumeroDocumento());

		// file nunca nulo
		if (file instanceof ArchivoAdjunto) {
			final ArchivoAdjunto archivoAdjunto = (ArchivoAdjunto) file;

			metaData.setAdjuntadoPorDoc(archivoAdjunto.getAdjuntadoPor().getNombreApellido());
			metaData.setMateriaDoc(archivoAdjunto.getMateria());
		}

		metaData.setFechaDoc(file.getFecha());

		// Crear NamedValues para la metadata de Alfresco.
		final NamedValue nameValue = Utils.createNamedValue(Constants.PROP_NAME, filename);
		final NamedValue description = Utils.createNamedValue(Constants.PROP_DESCRIPTION, "");

		final NamedValue numDocRaiz = metaData.transform("numeroDocumentoRaiz", metaData.getNumDocRaiz());
		final NamedValue materiaDoc = metaData.transform("materiaAnexo", metaData.getMateriaDoc());
		final NamedValue adjuntadoPor = metaData.transform("adjuntadoPor", metaData.getAdjuntadoPorDoc());
		final NamedValue fechaDoc = metaData.transform("fechaAnexo", dfFecha.format(metaData.getFechaDoc()) + HORAS);

		return new NamedValue[] {nameValue, description, numDocRaiz, materiaDoc, adjuntadoPor, fechaDoc, };
	}

	/**
	 * Metodo para obtener el sufijo que se le debe agregar al valor de la metadata del documento a subir a Alfresco, a
	 * partir de su tipo.
	 * 
	 * @param idTipoDoc el id del tipo del documento a subir a Alfresco.
	 * @return un string con el sufijo del documento.
	 */
	private static String getTipoDocSuffix(final Integer idTipoDoc) {
		String suffix = null;

		if (TipoDocumento.DECRETO.equals(idTipoDoc)) {
			suffix = "Dec";
		}
		if (TipoDocumento.MEMORANDUM.equals(idTipoDoc)) {
			suffix = "Memo";
		}
		if (TipoDocumento.RESOLUCION.equals(idTipoDoc)) {
			suffix = "Res";
		}
		if (TipoDocumento.OFICIO_ORDINARIO.equals(idTipoDoc) || TipoDocumento.OFICIO_CIRCULAR.equals(idTipoDoc) || TipoDocumento.OFICIO_RESERVADO.equals(idTipoDoc)) {
			suffix = "Oficio";
		}
		if (TipoDocumento.CARTA.equals(idTipoDoc)) {
			suffix = "Carta";
		}

		return suffix;
	}

}
