/*
 * EXE Ingeniería y Software Ltda.
 * 
 */
package cl.exe.alfresco.client.nuevo;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.xpath.XPathExpressionException;

import cl.exe.alfresco.client.support.AlfrescoAdapterException;
import cl.exe.exedoc.entity.Archivo;
import cl.exe.exedoc.entity.Documento;

/**
 * Nueva funcionalidad para subir documentos y archivos a Alfresco.
 * 
 * @author Jose Nova
 * @version 1.0 19 May 2011
 */
public abstract class AbstractNewGenericAlfrescoAdapter {

    /**
     * Metodo para obtener una instancia de NewAlfrescoAdapter.
     * 
     * @param username
     *            el nombre de usuario de Alfresco.
     * @param password
     *            la password del usuario de Alfresco.
     * @param serviceURL
     *            la URL del servicio web de alfresco.
     * @param directorioBaseBinarios
     *            el nombre del directorio para los documentos binarios.
     * @param directorioBaseElectronicos
     *            el nombre del directorio para los documentos electronicos.
     * @param directorioBaseAnexos
     *            el nombre del directorio para los documentos anexos.
     * @return una nueva instancia de NewAlfrescoAdapter.
     */
    public static NewAlfrescoAdapter getAlfrescoAdapter(final String username, final String password, 
            final String serviceURL, final String directorioBaseBinarios,
            final String directorioBaseElectronicos, final String directorioBaseAnexos) {
        return new NewAlfrescoAdapter(username, password, serviceURL, directorioBaseBinarios, 
                directorioBaseElectronicos, directorioBaseAnexos);
    }
    
    /**
     * Metodo para subir un archivo a Alfresco.
     * 
     * @param filename
     *            el nombre del archivo.
     * @param file
     *            el archivo como entidad.
     * @param doc
     *            la referencia al documento.
     * @return la URL de Alfresco con el archivo subido.
     * @throws RemoteException
     *             si el archivo no se pudo subir.
     */
    public abstract String upload(String filename, Archivo file, Documento doc) 
        throws RemoteException;
    
    /**
     * Sobrecarga del metodo de subida de archivos a Alfresco, para aceptar la
     * subida de documentos electronicos despues de la firma de los mismos.
     * 
     * @param filename
     *            el nombre del archivo. Corresponde al id del documento en la
     *            base de datos.
     * @param file
     *            el archivo XML como arreglo de bytes.
     * @param doc
     *            el documento asociado al XML a subir a Alfresco.
     * @return la URL del archivo subido a Alfresco.
     * @throws RemoteException
     *             si ocurre un problema subiendo el archivo a Alfresco.
     */
    public abstract String upload(String filename, byte[] file, Documento doc) throws RemoteException;
    
    /**
     * Metodo para descargar un archivo desde Alfresco.
     * 
     * @param url
     *            la URL del archivo en Alfresco.
     * @return el archivo deseado, como arreglo de bytes.
     * @throws IOException
     *             si ocurre un problema ejecutando el HTTP GET.
     * @throws XPathExpressionException
     *             si ocurre un problema obteniendo el ticket.
     */
    public abstract byte[] download(String url) throws IOException, XPathExpressionException;

	public void makeVersionableContent(String destinationNodeUuid)
			throws AlfrescoAdapterException {
		// TODO Auto-generated method stub
		
	}

	public void update(Archivo file, Documento doc) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void update(byte[] file, Documento doc) throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void updateContent(String mimeType, byte[] content,
			String destinationNodeUUID) throws AlfrescoAdapterException {
		// TODO Auto-generated method stub
		
	}
}
