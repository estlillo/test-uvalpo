package cl.exe.alfresco.client.support;

public class AlfrescoAdapterException extends Exception {
	public static final long serialVersionUID = 113333473222223L;
	private boolean critical;

	/**
	 * Detailed description, only if needed
	 */
	private String description;

	/**
	 * Message that is shown to the user
	 */
	private String errorMessage;

	/**
	 * Exception that lies below
	 */
	private Exception childException;

	public AlfrescoAdapterException() {
	}

	public AlfrescoAdapterException(boolean critical, String description, String errorMessage, Exception childException) {
		this.description = description;
		this.errorMessage = errorMessage;
		this.critical = critical;
		this.childException = childException;
	}

	public boolean isCritical() {
		return critical;
	}

	public void setCritical(boolean critical) {
		this.critical = critical;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Exception getChildException() {
		return childException;
	}

	public void setChildException(Exception childException) {
		this.childException = childException;
	}

}
