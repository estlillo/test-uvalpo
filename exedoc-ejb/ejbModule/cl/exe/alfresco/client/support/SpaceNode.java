package cl.exe.alfresco.client.support;

import java.util.HashMap;
import java.util.Map;

public class SpaceNode {

	/**
	 * Set containing all valueable information about a node.
	 * <key, value>
	 * 
	 * available keys are as String:
	 * {http://www.alfresco.org/model/content/1.0}name
	 * {http://www.alfresco.org/model/content/1.0}title
	 * {http://www.alfresco.org/model/system/1.0}node-uuid
	 * {http://www.alfresco.org/model/system/1.0}node-dbid
	 * {http://www.alfresco.org/model/system/1.0}store-protocol
	 * {http://www.alfresco.org/model/content/1.0}modifier
	 * {http://www.alfresco.org/model/content/1.0}modified
	 * {http://www.alfresco.org/model/content/1.0}owner
	 * {http://www.alfresco.org/model/content/1.0}created
	 * {http://www.alfresco.org/model/content/1.0}creator
	 * {http://www.alfresco.org/model/application/1.0}icon
	 * {http://www.alfresco.org/model/system/1.0}store-identifier
	 * {http://www.alfresco.org/model/content/1.0}path
	 */
	private Map<String, String> nodeInformation;
	
	public SpaceNode() {
		nodeInformation = new HashMap<String, String>();
	}

	/**
	 * @param nodeInformation
	 */
	public SpaceNode(Map<String, String> nodeInformation) {
		super();
		this.nodeInformation = nodeInformation;
	}

	public Map<String, String> getNodeInformation() {
		return nodeInformation;
	}

	public void setNodeInformation(Map<String, String> nodeInformation) {
		this.nodeInformation = nodeInformation;
	}
	
	public String getName() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}name"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}name");
		else return null;
	}
	
	public String getTitle() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}title"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}title");
		else return null;
	}
	
	public String getNodeUUID() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/system/1.0}node-uuid"))
			return nodeInformation.get("{http://www.alfresco.org/model/system/1.0}node-uuid");
		else return null;
	}
	
	public String getNodeDBID() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/system/1.0}node-dbid"))
			return nodeInformation.get("{http://www.alfresco.org/model/system/1.0}node-dbid");
		else return null;
	}
	
	public String getStoreProtocol() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/system/1.0}store-protocol"))
			return nodeInformation.get("{http://www.alfresco.org/model/system/1.0}store-protocol");
		else return null;
	}
	
	public String getModifier() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}modifier"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}modifier");
		else return null;
	}
	
	public String getModified() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}modified"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}modified");
		else return null;
	}
	
	public String getOwner() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}owner"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}owner");
		else return null;
	}
	
	public String getCreated() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}created"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}created");
		else return null;
	}
	
	public String getCreator() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}creator"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}creator");
		else return null;
	}
	
	public String getIcon() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/application/1.0}icon"))
			return nodeInformation.get("{http://www.alfresco.org/model/application/1.0}icon");
		else return null;
	}
	
	public String getStoreIdentifier() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/system/1.0}store-identifier"))
			return nodeInformation.get("{http://www.alfresco.org/model/system/1.0}store-identifier");
		else return null;
	}
	
	public String getPath() {
		if (nodeInformation.containsKey("{http://www.alfresco.org/model/content/1.0}path"))
			return nodeInformation.get("{http://www.alfresco.org/model/content/1.0}path");
		else return null;
	}
}
