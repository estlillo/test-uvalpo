package cl.exe.alfresco.client;

import org.alfresco.webservice.repository.QueryResult;

import cl.exe.alfresco.client.support.AlfrescoAdapterException;
import cl.exe.alfresco.client.support.ContentNode;
import cl.exe.alfresco.client.support.SpaceNode;

/**
 * Clase GenericAlfrescoAdapter.
 * @author Ricardo
 *
 */
public abstract class GenericAlfrescoAdapter {

	public static GenericAlfrescoAdapter getAlfrescoAdapter(String username, String password, String serviceURL) {
		return new AlfrescoAdapter(serviceURL);
	}

	/**
	 * This method will read a content specified by its nodeUUID.
	 * 
	 * @param contentNodeUUID {@link String} the exact UUID of the content to be read
	 * @return {@link Byte} the content as byte array
	 * @throws AlfrescoAdapterException alfresco Adapter.
	 */
	public abstract byte[] readContent(String contentNodeUUID) throws AlfrescoAdapterException;

	/**
	 * This method will write Content into the DMS/CMS system.
	 * 
	 * @param contentName {@link String} : The filename for the new content
	 * @param mimeType {@link String} : What is the content format written in, choose TEXT_PLAIN if you are not sure
	 * @param encoding {@link String} : UTF-8 ? UTF-16 ?
	 * @param content {@link Byte} : The content as byte[]
	 * @param destinationNodeUUID {@link String} : The exact UUID of the parent space
	 * @return {@link String} the UUID of the new content
	 * @throws AlfrescoAdapterException Alfresco Adapter.
	 */
	public abstract String writeContent(final String contentName, final String mimeType, final String encoding,
			final byte[] content, final String destinationNodeUUID) throws AlfrescoAdapterException;

	/**
	 * This method will write Content into a specified directory.
	 * 
	 * @param contentName {@link String} : The filename for the new content
	 * @param spaceName {@link String} : The exact name of the space to write in
	 * @param mimeType {@link String} : What is the content format written in, choose TEXT_PLAIN if you are not sure
	 * @param encoding {@link String} : UTF-8 ? UTF-16 ?
	 * @param content {@link Byte} : The content as byte[]
	 * @return {@link String} the UUID of the new content
	 * @throws AlfrescoAdapterException Alfresco Adapter.
	 */
	public abstract String writeContentInSpace(final String contentName, final String spaceName, final String mimeType,
			final String encoding, final byte[] content) throws AlfrescoAdapterException;

	/**
	 * @param destinationNodeUuid {@link String} 
	 * @throws AlfrescoAdapterException  Alfresco Adapter.
	 */
	public abstract void makeVersionableContent(final String destinationNodeUuid) throws AlfrescoAdapterException;

	public abstract void updateContent(String mimeType, String encoding, byte[] content, String destinationNodeUUID)
			throws AlfrescoAdapterException;

	/**
	 * This method will perform a checkin for a certain copy of a content that has been checked out before.
	 * 
	 * @param contentUUID node-UUID for the content to be checked in
	 * @param checkInDescription leave a message for your checkin
	 * @throws AlfrescoAdapterException
	 */
	public abstract void checkInContent(String contentUUID, String checkInDescription) throws AlfrescoAdapterException;

	/**
	 * This method will perform a checkout by providing a certain node-UUID.
	 * 
	 * @param contentUUID the node UUID
	 * @return the UUID of the working copy
	 * @throws AlfrescoAdapterException
	 */
	public abstract String checkOutContent(String contentUUID) throws AlfrescoAdapterException;

	/**
	 * This method will search inside a given space for all content that contain the searchText
	 * 
	 * @param spaceUUID where to search
	 * @param searchText for what to search
	 */
	public abstract ContentNode[] findContent(String spaceUUID, String searchText) throws AlfrescoAdapterException;

	/**
	 * This method will search for an existing space.
	 * 
	 * @param spaceName
	 * @return SpaceNode
	 * @throws AlfrescoAdapterException
	 */
	public abstract SpaceNode findSpace(String spaceName) throws AlfrescoAdapterException;

	/**
	 * This method will retrieve all User-Spaces under app:/company_home/app:user_homes
	 * 
	 * @return if spaces found: SpaceNode[], read the HashMap to get your information if NO spaces found: null
	 */
	public abstract SpaceNode[] listUserSpaces() throws AlfrescoAdapterException;

	/**
	 * This method will retrieve the content in a certain space
	 * 
	 * @param spaceName the EXACT space name as String NOTE: There maybe problems if a space name contains a
	 *        whitespace...
	 */
	public abstract ContentNode[] listContentInSpaceByName(String spaceName) throws AlfrescoAdapterException;

	/**
	 * This method will retrieve the content in a certain space (faster than search by SpaceName)
	 * 
	 * @param spaceName the EXACT space name as String NOTE: There maybe problems if a space name contains a
	 *        whitespace...
	 */
	public abstract ContentNode[] listContentInSpaceByUUID(String spaceUUID) throws AlfrescoAdapterException;

	/**
	 * This method will query the SpacesStore with a custom query string. NOTE: In final version this method will be
	 * removed in favor of a clean architecture.
	 * 
	 * @param customQuery your query string
	 * @return will return org.alfresco.webservice.repository.QueryResult
	 */
	public abstract QueryResult executeCustomQuery(String customQuery) throws AlfrescoAdapterException;
}
