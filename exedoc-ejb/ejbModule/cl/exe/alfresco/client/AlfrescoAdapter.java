package cl.exe.alfresco.client;

import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.PASSWORD;
import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.SERVICE_URL;
import static cl.exe.exedoc.util.singleton.AlfrescoSingleton.USERNAME;

import java.io.IOException;
import java.io.InputStream;

import org.alfresco.webservice.authentication.AuthenticationFault;
import org.alfresco.webservice.authoring.AuthoringServiceSoapBindingStub;
import org.alfresco.webservice.authoring.CheckoutResult;
import org.alfresco.webservice.content.Content;
import org.alfresco.webservice.content.ContentServiceSoapBindingStub;
import org.alfresco.webservice.repository.QueryResult;
import org.alfresco.webservice.repository.RepositoryServiceSoapBindingStub;
import org.alfresco.webservice.repository.UpdateResult;
import org.alfresco.webservice.types.CML;
import org.alfresco.webservice.types.CMLAddAspect;
import org.alfresco.webservice.types.CMLCreate;
import org.alfresco.webservice.types.ContentFormat;
import org.alfresco.webservice.types.NamedValue;
import org.alfresco.webservice.types.Node;
import org.alfresco.webservice.types.ParentReference;
import org.alfresco.webservice.types.Predicate;
import org.alfresco.webservice.types.Query;
import org.alfresco.webservice.types.Reference;
import org.alfresco.webservice.types.ResultSet;
import org.alfresco.webservice.types.ResultSetRow;
import org.alfresco.webservice.types.Store;
import org.alfresco.webservice.util.AuthenticationUtils;
import org.alfresco.webservice.util.Constants;
import org.alfresco.webservice.util.ContentUtils;
import org.alfresco.webservice.util.Utils;
import org.alfresco.webservice.util.WebServiceException;
import org.alfresco.webservice.util.WebServiceFactory;
import org.apache.log4j.Logger;

import cl.exe.alfresco.client.support.AlfrescoAdapterException;
import cl.exe.alfresco.client.support.ContentNode;
import cl.exe.alfresco.client.support.SpaceNode;

/**
 * @author Administrator
 */
public class AlfrescoAdapter extends GenericAlfrescoAdapter {

	private static final String SPACES_STORE = "SpacesStore";

	private static Logger logger = Logger.getLogger(AlfrescoAdapter.class.getName());

	// private String username;
	// private String password;
	// private String serviceURL;
	private boolean isLocalService;

	/**
	 * Constructor.
	 */
	public AlfrescoAdapter() {
		this.isLocalService = true;
	}

	/**
	 * Constructor.
	 * 
	 * @param serviceURL String
	 */
	public AlfrescoAdapter(final String serviceURL) {
		this.isLocalService = false;
		WebServiceFactory.setEndpointAddress(SERVICE_URL + "/api");
	}

	/**
	 * This method will read a content specified by its nodeUUID.
	 * 
	 * @param contentNodeUUID the exact UUID of the content to be read
	 * @return the content as byte array
	 * @throws AlfrescoAdapterException
	 */
	public byte[] readContent(final String contentNodeUUID) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Get the content service
			ContentServiceSoapBindingStub contentService;
			if (isLocalService) {
				contentService = WebServiceFactory.getContentService();
			} else {
				contentService = WebServiceFactory.getContentService(SERVICE_URL);
			}

			// Create a reference to the parent where we want to insert content
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference reference = new Reference(storeRef, contentNodeUUID, null);

			// Read the newly added content from the respository
			final Content[] readResult = contentService.read(new Predicate(new Reference[] { reference }, storeRef,
					null), Constants.PROP_CONTENT);
			final Content content = readResult[0];
			final InputStream is = ContentUtils.getContentAsInputStream(content);
			return this.readBytes(is);
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"readContent\"",
					"Exception occurred during \"readContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * @param inputStream InputStream
	 * @return byte[]
	 * @throws IOException
	 */
	private byte[] readBytes(final InputStream inputStream) throws IOException {
		byte[] bytes = null;
		final byte[] buffer = new byte[4096];
		int bytesRead = 0;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			if (bytes != null) {
				final byte[] oldBytes = bytes;
				bytes = new byte[oldBytes.length + bytesRead];
				System.arraycopy(oldBytes, 0, bytes, 0, oldBytes.length);
				System.arraycopy(buffer, 0, bytes, oldBytes.length, bytesRead);
			} else {
				bytes = new byte[bytesRead];
				System.arraycopy(buffer, 0, bytes, 0, bytesRead);
			}
		}
		return bytes;
	}

	/**
	 * This method will write Content into the DMS/CMS system.
	 * 
	 * @param contentName : The filename for the new content
	 * @param mimeType : What is the content format written in, choose TEXT_PLAIN if you are not sure
	 * @param encoding : UTF-8 ? UTF-16 ?
	 * @param content : The content as byte[]
	 * @param destinationNodeUUID : The exact UUID of the parent space
	 * @return the UUID of the new content
	 * @throws AlfrescoAdapterException
	 */
	public String writeContent(final String contentName, final String mimeType, final String encoding,
			final byte[] content, final String destinationNodeUUID) throws AlfrescoAdapterException {
		try {
			AuthenticationUtils.startSession(USERNAME, PASSWORD);
		} catch (AuthenticationFault fault) {
			logger.error("fallo la autenticacion alfresco");
			return null;
		}

		try {
			// Start the session

			// Create a reference to the parent where we want to insert content
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference reference = new Reference(storeRef, destinationNodeUUID, null);

			final ParentReference parentReference = new ParentReference(reference.getStore(), reference.getUuid(),
					null, Constants.ASSOC_CONTAINS, Constants.ASSOC_CONTAINS);

			final NamedValue[] properties = new NamedValue[] { Utils.createNamedValue(Constants.PROP_NAME, contentName) };
			final CMLCreate create = new CMLCreate("1", parentReference, null, null, null, Constants.TYPE_CONTENT,
					properties);
			final CML cml = new CML();
			cml.setCreate(new CMLCreate[] { create });

			UpdateResult[] results;
			if (isLocalService) {
				results = WebServiceFactory.getRepositoryService().update(cml);
			} else {
				results = WebServiceFactory.getRepositoryService(SERVICE_URL).update(cml);
			}

			// Set content
			final ContentFormat format = new ContentFormat(mimeType.toString(), encoding);
			Content newContent;
			if (isLocalService) {
				newContent = WebServiceFactory.getContentService().write(results[0].getDestination(),
						Constants.PROP_CONTENT, content, format);
			} else {
				newContent = WebServiceFactory.getContentService(SERVICE_URL).write(results[0].getDestination(),
						Constants.PROP_CONTENT, content, format);
			}

			return newContent.getNode().getUuid();
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"writeContent\"",
					"Exception occurred during \"writeContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will write Content into a specified directory.
	 * 
	 * @param contentName : The filename for the new content
	 * @param spaceName : The exact name of the space to write in
	 * @param mimeType : What is the content format written in, choose TEXT_PLAIN if you are not sure
	 * @param encoding : UTF-8 ? UTF-16 ?
	 * @param content : The content as byte[]
	 * @return the UUID of the new content
	 * @throws AlfrescoAdapterException
	 */
	public String writeContentInSpace(final String contentName, final String spaceName, final String mimeType,
			final String encoding, final byte[] content) throws AlfrescoAdapterException {

		try {
			AuthenticationUtils.startSession(USERNAME, PASSWORD);
		} catch (AuthenticationFault fault) {
			logger.error("fallo la autenticacion alfresco");
			return null;
		} catch (WebServiceException ws) {
			ws.printStackTrace();
		}

		try {

			// Searching for the space UUID
			// Get a reference to the respository web service
			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) {
				repositoryService = WebServiceFactory.getRepositoryService();
			} else {
				repositoryService = WebServiceFactory.getRepositoryService();
			}

			// Get a reference to the space we have named
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			Node[] nodes = null;
			try {
				final Reference reference = new Reference(storeRef, null, "/app:company_home/app:user_homes/cm:"
						+ spaceName);
				final Predicate predicate = new Predicate(new Reference[] { reference }, null, null);
				nodes = repositoryService.get(predicate);
			} catch (Exception ex) {
				logger.error("fallo en repositoryService.get()");
				logger.error("excepcion es = " + ex.getMessage());
				ex.printStackTrace();
				return null;
			}
			// Searching for the UUID out of the result
			String spaceUUID = "";
			if (nodes[0] != null) {
				final NamedValue[] properties = nodes[0].getProperties();
				final SpaceNode sn = new SpaceNode();
				for (int j = 0; j < properties.length; j++) {
					final NamedValue nv = properties[j];
					sn.getNodeInformation().put(nv.getName(), nv.getValue());
				}
				spaceUUID = sn.getNodeUUID();
			} else {
				throw new AlfrescoAdapterException(true, "No space '" + spaceName + "' found!", "No space '"
						+ spaceName + "' found!", null);
			}

			// Create a reference to the parent where we want to insert content
			final Reference referenceWrite = new Reference(storeRef, spaceUUID, null);

			final ParentReference parentReference = new ParentReference(referenceWrite.getStore(),
					referenceWrite.getUuid(), null, Constants.ASSOC_CONTAINS, Constants.ASSOC_CONTAINS);

			// Create content
			final NamedValue[] properties = new NamedValue[] { Utils.createNamedValue(Constants.PROP_NAME, contentName) };
			final CMLCreate create = new CMLCreate("1", parentReference, null, null, null, Constants.TYPE_CONTENT,
					properties);
			final CML cml = new CML();
			cml.setCreate(new CMLCreate[] { create });

			// update
			UpdateResult[] results;
			if (isLocalService) {
				results = WebServiceFactory.getRepositoryService().update(cml);
			} else {
				results = WebServiceFactory.getRepositoryService().update(cml);
			}

			// Set content
			final ContentFormat format = new ContentFormat(mimeType.toString(), encoding);
			Content newContent;
			if (isLocalService) {
				newContent = WebServiceFactory.getContentService().write(results[0].getDestination(),
						Constants.PROP_CONTENT, content, format);
			} else {
				newContent = WebServiceFactory.getContentService().write(results[0].getDestination(),
						Constants.PROP_CONTENT, content, format);
			}

			// return newContent.getNode().getUuid();
			System.out.println(newContent.getNode().getStore().getScheme() + "/"
					+ newContent.getNode().getStore().getAddress() + "/" + newContent.getNode().getUuid() + "/"
					+ contentName);
			return newContent.getNode().getStore().getScheme() + "/" + newContent.getNode().getStore().getAddress()
					+ "/" + newContent.getNode().getUuid() + "/" + contentName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getClass().getName());
			e.printStackTrace();
			throw new AlfrescoAdapterException(true, "Exception occurred during \"writeContentInSpace\"",
					"Exception occurred during \"writeContentInSpace\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	@Override
	public void makeVersionableContent(final String destinationNodeUuid) throws AlfrescoAdapterException {
		try {
			// Iniciar sesion
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Obtener repositorio
			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) {
				repositoryService = WebServiceFactory.getRepositoryService();
			} else {
				repositoryService = WebServiceFactory.getRepositoryService();
			}

			// Obtener referencia al nodo
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference referenceVersion = new Reference(storeRef, destinationNodeUuid, null);
			final Predicate predicateVersion = new Predicate(new Reference[] { referenceVersion, }, null, null);

			// Crear aspecto versionable
			final CMLAddAspect addVersionableAspect = new CMLAddAspect();
			addVersionableAspect.setAspect(Constants.ASPECT_VERSIONABLE);
			addVersionableAspect.setWhere(predicateVersion);

			// Crear bloque CML
			final CML cml = new CML();
			cml.setAddAspect(new CMLAddAspect[] { addVersionableAspect, });

			repositoryService.update(cml);
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error(e.getClass().getName());
			e.printStackTrace();
			throw new AlfrescoAdapterException(true, "Exception en: \"makeVersionableContent\"",
					"Exception: \"makeVersionableContent\"", e);
		} finally {
			// Terminar sesion
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * @param mimeType String
	 * @param encoding String
	 * @param contet byte[]
	 * @param destinationNodeUUID
	 * @throws AlfrescoAdapterException
	 */
	public void updateContent(final String mimeType, final String encoding, final byte[] content,
			final String destinationNodeUUID) throws AlfrescoAdapterException {
		try {
			AuthenticationUtils.startSession(USERNAME, PASSWORD);
		} catch (AuthenticationFault fault) {
			logger.error("fallo la autenticacion al fresco");
		}

		try {
			// Start the session
			ContentServiceSoapBindingStub contentService;
			if (isLocalService) {
				contentService = WebServiceFactory.getContentService();
			} else {
				contentService = WebServiceFactory.getContentService(SERVICE_URL);
			}

			// Create a reference to the parent where we want to insert content
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			final Reference reference = new Reference(storeRef, destinationNodeUUID, null);

			final Content[] readResult = contentService.read(new Predicate(new Reference[] { reference }, storeRef,
					null), Constants.PROP_CONTENT);
			final Content existingContent = readResult[0];
			final Reference existingReference = existingContent.getNode();

			final ContentFormat format = new ContentFormat(mimeType, encoding);
			contentService.write(existingReference, Constants.PROP_CONTENT, content, format);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.toString());
			throw new AlfrescoAdapterException(true, "Exception occurred during \"updateContent\"",
					"Exception occurred during \"updateContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will perform a checkin for a certain copy of a content that has been checked out before.
	 * 
	 * @param contentUUID node-UUID for the content to be checked in
	 * @param checkInDescription leave a message for your checkin
	 * @throws AlfrescoAdapterException
	 */
	public void checkInContent(final String contentUUID, final String checkInDescription)
			throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			AuthoringServiceSoapBindingStub authoringService;
			if (isLocalService) {
				authoringService = WebServiceFactory.getAuthoringService();
			} else {
				authoringService = WebServiceFactory.getAuthoringService(SERVICE_URL);
			}

			// Create a reference to the parent where we want to query
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			// Get a reference to the working copy
			final Reference workingCopyReference = new Reference(storeRef, contentUUID, null);

			// Now check the working copy in with a description of the change
			// made that will be recorded in the version history
			final Predicate predicate = new Predicate(new Reference[] { workingCopyReference }, null, null);
			final NamedValue[] comments = new NamedValue[] { Utils.createNamedValue("description", checkInDescription) };
			authoringService.checkin(predicate, comments, false);
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"checkInContent\"",
					"Exception occurred during \"checkInContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will perform a checkout by providing a certain node-UUID.
	 * 
	 * @param contentUUID the node UUID
	 * @return the UUID of the working copy
	 * @throws AlfrescoAdapterException
	 */
	public String checkOutContent(final String contentUUID) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Create a reference to the parent where we want to query
			final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			AuthoringServiceSoapBindingStub authoringService;
			if (isLocalService) {
				authoringService = WebServiceFactory.getAuthoringService();
			} else {
				authoringService = WebServiceFactory.getAuthoringService(SERVICE_URL);
			}

			// Get a reference to the content
			final Reference contentReference = new Reference(storeRef, contentUUID, null);

			// Checkout the content, placing the working document in the same
			// folder
			final Predicate itemsToCheckOut = new Predicate(new Reference[] { contentReference }, null, null);
			final CheckoutResult checkOutResult = authoringService.checkout(itemsToCheckOut, null);

			// Get a reference to the working copy
			final Reference workingCopyReference = checkOutResult.getWorkingCopies()[0];

			return workingCopyReference.getUuid();
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"checkOutContent\"",
					"Exception occurred during \"checkOutContent\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will search inside a given space for all content that contain the searchText.
	 * 
	 * @param spaceUUID where to search
	 * @param searchText for what to search
	 * @return ContentNode[]
	 * @throws AlfrescoAdapterException
	 */
	public ContentNode[] findContent(String spaceUUID, String searchText) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			try {
				RepositoryServiceSoapBindingStub repositoryService;
				if (isLocalService) {
					repositoryService = WebServiceFactory.getRepositoryService();
				} else {
					repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);
				}

				// Get a reference to the space we have named
				final Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
				final Reference reference = new Reference(storeRef, spaceUUID, null);
				final Predicate predicate = new Predicate(new Reference[] { reference }, null, null);
				final Node[] nodes = repositoryService.get(predicate);

				// Create a query object, looking for all items with alfresco in
				// the name of text
				final Query query = new Query(Constants.QUERY_LANG_LUCENE, "+PARENT:\"workspace://SpacesStore/"
						+ nodes[0].getReference().getUuid() + "\" +TEXT:\"" + searchText + "\"");

				// Execute the query
				QueryResult queryResult = repositoryService.query(storeRef, query, false);

				// Display the results
				ResultSet resultSet = queryResult.getResultSet();
				ResultSetRow[] rows = resultSet.getRows();

				// if no results are found, then return null
				if (rows == null) {
					return null;
				}
				// else fill the SpaceNode-Array and return it.
				else {
					return this.getContentNodes(rows);
				}
			} catch (Exception e) {
				throw new AlfrescoAdapterException(true, "Exception occurred during \"findContent\"",
						"Exception occurred during \"findContent\"", e);
			} finally {
				// End the session
				AuthenticationUtils.endSession();
			}
		} catch (Exception serviceException) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"findContent\"",
					"Exception occurred during \"findContent\"", serviceException);
		}
	}

	/**
	 * This method will search for an existing space.
	 * 
	 * @param spaceName
	 * @return SpaceNode
	 * @throws AlfrescoAdapterException
	 */
	public SpaceNode findSpace(String spaceName) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Get a reference to the respository web service
			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) repositoryService = WebServiceFactory.getRepositoryService();
			else repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);

			// Get a reference to the space we have named
			Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);
			Reference reference = new Reference(storeRef, null, "/app:company_home/app:user_homes/cm:" + spaceName);
			Predicate predicate = new Predicate(new Reference[] { reference }, null, null);
			Node[] nodes = repositoryService.get(predicate);

			return this.convertToSpaceNode(nodes[0]);
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"findSpace\"",
					"Exception occurred during \"findSpace\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will retrieve all User-Spaces under app:/company_home/app:user_homes
	 * 
	 * @return if spaces found: SpaceNode[], read the HashMap to get your information if NO spaces found: null
	 */
	public SpaceNode[] listUserSpaces() throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Create a reference to the parent where we want to query
			Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) repositoryService = WebServiceFactory.getRepositoryService();
			else repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);

			// Get a reference to the User_Homes Space
			Reference reference = new Reference(storeRef, null, "/app:company_home/app:user_homes");
			Predicate predicate = new Predicate(new Reference[] { reference }, null, null);

			Node[] nodes = repositoryService.get(predicate);
			String queryString = "+PARENT:\"workspace://SpacesStore/" + nodes[0].getReference().getUuid() + "\""
					+ " -TYPE:\"{http://www.alfresco.org/model/content/1.0}content\"";

			// Create a query object, looking for all items with alfresco in the
			// name of text
			Query query = new Query(Constants.QUERY_LANG_LUCENE, queryString);

			// Execute the query
			QueryResult queryResult = repositoryService.query(storeRef, query, false);

			// Display the results
			ResultSet resultSet = queryResult.getResultSet();
			ResultSetRow[] rows = resultSet.getRows();

			// if no results are found, then return null
			if (rows == null) {
				return null;
			}
			// else fill the SpaceNode-Array and return it.
			else {
				return this.getSpaceNodes(rows);
			}
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"listUserSpaces\"",
					"Exception occurred during \"listUserSpaces\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will retrieve the content in a certain space
	 * 
	 * @param spaceName the EXACT space name as String NOTE: There maybe problems if a space name contains a
	 *        whitespace...
	 */
	public ContentNode[] listContentInSpaceByName(String spaceName) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Create a reference to the parent where we want to query
			Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) repositoryService = WebServiceFactory.getRepositoryService();
			else repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);

			// Get a reference to the space we have named
			Reference reference = new Reference(storeRef, null, "//*[@cm:name=\"" + spaceName + "\"]");
			Predicate predicate = new Predicate(new Reference[] { reference }, null, null);

			Node[] nodes = repositoryService.get(predicate);
			String queryString = "+PARENT:\"workspace://SpacesStore/" + nodes[0].getReference().getUuid()
					+ "\" +TYPE:\"cm:content\"";

			// Create a query object, looking for all items with alfresco in the
			// name of text
			Query query = new Query(Constants.QUERY_LANG_LUCENE, queryString);

			// Execute the query
			QueryResult queryResult = repositoryService.query(storeRef, query, false);

			// Display the results
			ResultSet resultSet = queryResult.getResultSet();
			ResultSetRow[] rows = resultSet.getRows();

			// if no results are found, then return null
			if (rows == null) {
				return null;
			}
			// else fill the SpaceNode-Array and return it.
			else {
				return this.getContentNodes(rows);
			}
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"listContentInSpaceByName\"",
					"Exception occurred during \"listContentInSpaceByName\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will retrieve the content in a certain space (faster than search by SpaceName)
	 * 
	 * @param spaceUUID the UUID of the space in which to search for content NOTE: There maybe problems if a space name
	 *        contains a whitespace...
	 */
	public ContentNode[] listContentInSpaceByUUID(String spaceUUID) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Create a reference to the parent where we want to query
			Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) repositoryService = WebServiceFactory.getRepositoryService();
			else repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);

			String queryString = "+PARENT:\"workspace://SpacesStore/" + spaceUUID + "\" +TYPE:\"cm:content\"";

			// Create a query object, looking for all items with alfresco in the
			// name of text
			Query query = new Query(Constants.QUERY_LANG_LUCENE, queryString);

			// Execute the query
			QueryResult queryResult = repositoryService.query(storeRef, query, false);

			// Display the results
			ResultSet resultSet = queryResult.getResultSet();
			ResultSetRow[] rows = resultSet.getRows();

			// if no results are found, then return null
			if (rows == null) {
				return null;
			}
			// else fill the SpaceNode-Array and return it.
			else {
				return this.getContentNodes(rows);
			}
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"listContentInSpaceByUUID\"",
					"Exception occurred during \"listContentInSpaceByUUID\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	/**
	 * This method will query the SpacesStore with a custom query string.
	 * 
	 * @param customQuery your query string
	 * @return will return org.alfresco.webservice.repository.QueryResult
	 */
	public QueryResult executeCustomQuery(String customQuery) throws AlfrescoAdapterException {
		try {
			// Start the session
			AuthenticationUtils.startSession(USERNAME, PASSWORD);

			// Create a reference to the parent where we want to query
			Store storeRef = new Store(Constants.WORKSPACE_STORE, SPACES_STORE);

			RepositoryServiceSoapBindingStub repositoryService;
			if (isLocalService) repositoryService = WebServiceFactory.getRepositoryService();
			else repositoryService = WebServiceFactory.getRepositoryService(SERVICE_URL);

			// Create a query object, looking for all items with alfresco in the
			// name of text
			Query query = new Query(Constants.QUERY_LANG_LUCENE, customQuery);

			// Execute the query
			QueryResult queryResult = repositoryService.query(storeRef, query, false);

			return queryResult;
		} catch (Exception e) {
			throw new AlfrescoAdapterException(true, "Exception occurred during \"listContentInSpaceByUUID\"",
					"Exception occurred during \"listContentInSpaceByUUID\"", e);
		} finally {
			// End the session
			AuthenticationUtils.endSession();
		}
	}

	private SpaceNode convertToSpaceNode(Node node) {
		if (node != null) {
			SpaceNode sn = new SpaceNode();
			NamedValue[] properties = node.getProperties();

			for (int j = 0; j < properties.length; j++) {
				NamedValue nv = properties[j];
				sn.getNodeInformation().put(nv.getName(), nv.getValue());
			}

			return sn;
		} else {
			return null;
		}
	}

	private SpaceNode[] getSpaceNodes(ResultSetRow[] rows) {
		if (rows != null) {
			// get the result array ready
			SpaceNode[] result = new SpaceNode[rows.length];

			// for every row do...
			for (int i = 0; i < rows.length; i++) {
				ResultSetRow row = rows[i];

				// new SpaceNode to be filled with data...
				SpaceNode sn = new SpaceNode();

				// fill it with information
				NamedValue[] columns = row.getColumns();

				for (int j = 0; j < columns.length; j++) {
					NamedValue nv = columns[j];
					sn.getNodeInformation().put(nv.getName(), nv.getValue());
				}

				// put the SpaceNode into the array
				result[i] = sn;
			}

			return result;
		} else {
			return null;
		}
	}

	private ContentNode[] getContentNodes(ResultSetRow[] rows) {
		if (rows != null) {
			// get the result array ready
			ContentNode[] result = new ContentNode[rows.length];

			// for every row do...
			for (int i = 0; i < rows.length; i++) {
				ResultSetRow row = rows[i];

				// new ContentNode to be filled with data...
				ContentNode cn = new ContentNode();

				// fill it with information
				NamedValue[] columns = row.getColumns();

				for (int j = 0; j < columns.length; j++) {
					NamedValue nv = columns[j];
					cn.getNodeInformation().put(nv.getName(), nv.getValue());
				}

				// put the content node into the array
				result[i] = cn;
			}

			return result;
		} else {
			return null;
		}
	}
}
