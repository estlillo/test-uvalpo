package cl.exe.exedoc.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.Component;

import cl.exe.exedoc.util.VisualizarImagenFirmaLocal;

public class VisualizarImagenFirma extends HttpServlet implements Servlet {

	private static final long serialVersionUID = -1380822533737537075L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String rut = request.getParameter("rut");
		byte[] blanco = { 71, 73, 70, 56, 57, 97, 1, 0, 1, 0, -128, 0, 0, -1, -1, -1, 0, 0, 0, 33, -7, 4, 1, 20, 0, 0, 0, 44, 0, 0, 0, 0, 1, 0, 1, 0, 0, 2, 2, 68, 1, 0, 59 };
		byte[] imagenFirma = null;

		if (rut != null && !rut.trim().isEmpty()) {
			VisualizarImagenFirmaLocal visualizador = (VisualizarImagenFirmaLocal) Component.getInstance("visualizarImagenFirma");
			imagenFirma = visualizador.getImagenFirma(rut);
		}

		if (imagenFirma == null) {
			imagenFirma = blanco;
		}

		response.setContentLength(imagenFirma.length);
		ServletOutputStream ouputStream = response.getOutputStream();
		ouputStream.write(imagenFirma, 0, imagenFirma.length);
		ouputStream.flush();
		ouputStream.close();

	}

}
