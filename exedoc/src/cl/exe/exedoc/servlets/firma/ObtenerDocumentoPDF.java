package cl.exe.exedoc.servlets.firma;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.exe.exedoc.util.ServletUtils;

public class ObtenerDocumentoPDF extends HttpServlet implements Servlet {
	private static final long serialVersionUID = -6785586709714990431L;

	public ObtenerDocumentoPDF() {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cmsId = request.getParameter("cmsId");

		System.out.println("getDocumentoPDF: " + cmsId);
		if (cmsId != null) {
			byte[] data = ServletUtils.recuperarArchivo(cmsId);
			try {
				if (data != null) {
					response.setContentType("application/pdf");
					ServletOutputStream outputStream = response.getOutputStream();
					outputStream.write(data, 0, data.length);
					outputStream.flush();
					outputStream.close();
				}
			} catch (Exception e) {

			}
		}
	}
}
