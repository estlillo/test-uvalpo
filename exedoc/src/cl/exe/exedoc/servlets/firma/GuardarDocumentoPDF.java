package cl.exe.exedoc.servlets.firma;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.exe.exedoc.util.ServletUtils;

public class GuardarDocumentoPDF extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 464125767270315887L;

	public GuardarDocumentoPDF() {

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cmsIdStr = request.getParameter("cmsId");
		byte[] cmsId = cmsIdStr.getBytes();
		ServletInputStream iStream = request.getInputStream();
		ServletUtils.actualizarArchivo(cmsIdStr, "application/pdf", cmsId, iStream);
	}
}
