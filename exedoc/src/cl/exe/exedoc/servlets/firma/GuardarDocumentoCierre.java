package cl.exe.exedoc.servlets.firma;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.Component;

import cl.exe.exedoc.repositorio.Repositorio;
import cl.exe.exedoc.repositorio.RepositorioLocal;

public class GuardarDocumentoCierre extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 7239902802374557322L;

	private static final Locale locale = new Locale("es", "CL");

	public GuardarDocumentoCierre() {
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Long fc = new Long(request.getParameter("fechaCierre"));
		Date fechaCierre = new Date(fc);
		byte[] documento = new byte[4096];
		int cuantos;
		ByteArrayOutputStream bstream = new ByteArrayOutputStream();
		while ((cuantos = request.getInputStream().read(documento)) > 0) {
			bstream.write(documento, 0, cuantos);
		}
		guardaDocumentoFirmado(fechaCierre, bstream.toByteArray());

	}

	private void guardaDocumentoFirmado(Date fechaCierre, byte[] archivo) {
		RepositorioLocal repositorio = (RepositorioLocal) Component
				.getInstance(Repositorio.class);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"ddMMyyyyHHmmss", locale);
		String nombre = simpleDateFormat.format(fechaCierre).toString()
				+ ".xml";
		repositorio.almacenarDocumentosCerrados(archivo, nombre);
	}

}
