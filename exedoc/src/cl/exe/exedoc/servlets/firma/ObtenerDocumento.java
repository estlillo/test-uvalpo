package cl.exe.exedoc.servlets.firma;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.util.ServletUtils;

public class ObtenerDocumento extends HttpServlet implements Servlet {
    
    private static final long serialVersionUID = 8198306953096684781L;
    private static Log log = Logging.getLog(ObtenerDocumento.class);
    
    public ObtenerDocumento() {
    }
    
    @Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException,
			IOException {
        this.doGet(req, resp);
    }
    
    @Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException,
			IOException {
        final Long idDocumento = new Long(request.getParameter("documentoId"));
        final Long idFirmador = new Long(request.getParameter("firmadorId"));
        log.info("GetDocumento: " + idDocumento + " Firmador: " + idFirmador);
        final DocumentoElectronico documento = ServletUtils.getDocumento(idDocumento);
        
        if (ServletUtils.preparaDocumentoAFirma(documento, idFirmador)) {
            //final byte[] data = documento.getDocumentoXML().getBytes();
           final byte[] data = ServletUtils.getXmlData(documento);
            try {
                if (data != null) {
                    response.setContentType("text/xml");
                    this.transform(data, response.getOutputStream());
                    response.getOutputStream().flush();
                } else {
                    response.setContentType("text/xml");
                    response.getWriter().print("<?xml version=\"1.0\" ?>");
                    response.getWriter().print("<error>DOCUMENTO NO ENCONTRADO</error>");
                }
            } catch (Exception ex) {
            	 log.info("Exception: " +ex.getMessage());
                response.setContentType("text/xml");
                response.getWriter().print("<?xml version=\"1.0\" ?>");
                response.getWriter().print(
                        "<error><msj>ERROR AL FORMATEAR DOCUMENTO</msj><reason>" + ex.getMessage()
                                + "</reason></error>");
            }
        }
    }
    
	private void transform(final byte[] xml, final OutputStream out) throws Exception {
        final TransformerFactory factory = TransformerFactory.newInstance();
        final Transformer transformer = factory.newTransformer();
        final StreamSource source = new StreamSource(new ByteArrayInputStream(xml));
        final StreamResult result = new StreamResult(out);
        transformer.transform(source, result);
    }
    
}
