package cl.exe.exedoc.servlets.firma;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import cl.exe.exedoc.util.ServletUtils;

/**
 * Servlet encargado de guardar el documento firmado.
 * 
 * @author Jose Nova
 * @version 1.0
 */
public class GuardarDocumento extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 55527572446158407L;
	
	private static Log log = Logging.getLog(GuardarDocumento.class);

	/**
	 * Constructor.
	 */
	public GuardarDocumento() {
	}

	@Override
	protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
			throws ServletException, IOException {
		this.doGet(req, resp);
	}

	@Override
	protected void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException, IOException {
		final Long id = new Long(request.getParameter("documentoId"));
		final Long idFirmador = new Long(request.getParameter("firmadorId"));
		log.info("PutDocumento: " + id + " Firmador: " + idFirmador);
		final byte[] documento = new byte[4096];
		int cuantos;
		final ByteArrayOutputStream bstream = new ByteArrayOutputStream();
		while ((cuantos = request.getInputStream().read(documento)) > 0) {
			bstream.write(documento, 0, cuantos);
		}
		
		try {
			ServletUtils.guardaDocumentoFirmado(id, idFirmador, bstream.toByteArray());
		} catch (Throwable e) {
			throw new ServletException();
		}

	}

}
