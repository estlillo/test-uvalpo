package cl.exe.exedoc.servlets.firma;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jboss.seam.Component;

import cl.exe.exedoc.pojo.expediente.ExpedienteBandejaSalida;
import cl.exe.exedoc.reportes.Reporte;
import cl.exe.exedoc.reportes.ReporteInterface;

public class ObtenerDocumentoCierre extends HttpServlet implements Servlet {

	private static final long serialVersionUID = -2262951687751790622L;

	private static final Locale locale = new Locale("es", "CL");

	public ObtenerDocumentoCierre() {
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Long fi = new Long(request.getParameter("fechaInicio"));
		Long ft = new Long(request.getParameter("fechaTermino"));
		Long fc = new Long(request.getParameter("fechaCierre"));
		Date fechaInicio = new Date(fi);
		Date fechaTermino = new Date(ft);
		Date fechaCierre = new Date(fc);
		byte[] data = getDocumentoCierre(fechaInicio, fechaTermino, fechaCierre);
		try {
			if (data != null) {
				response.setContentType("text/xml");
				transform(data, response.getOutputStream());
				response.getOutputStream().flush();
			} else {
				response.setContentType("text/xml");
				response.getWriter().print("<?xml version=\"1.0\" ?>");
				response.getWriter().print(
						"<error>DOCUMENTO NO ENCONTRADO</error>");
			}
		} catch (Exception ex) {
			response.setContentType("text/xml");
			response.getWriter().print("<?xml version=\"1.0\" ?>");
			response.getWriter().print(
					"<error><msj>ERROR AL FORMATEAR DOCUMENTO</msj><reason>"
							+ ex.getMessage() + "</reason></error>");
		}
	}

	private byte[] getDocumentoCierre(Date fechaInicio, Date fechaTermino, Date fechaCierre) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss", locale);
		ReporteInterface reporte = (ReporteInterface) Component
				.getInstance(Reporte.class);
		List<ExpedienteBandejaSalida> documentos = reporte
				.buscarDocumentosCierre(fechaInicio, fechaTermino);
		StringBuilder sb = new StringBuilder();
		sb.append("<cierreDocumentos>\n");
		sb.append("<fechaCierre>" + simpleDateFormat.format(fechaCierre)
				+ "</fechaCierre>\n");
		sb.append("<documentos>\n");
		for (ExpedienteBandejaSalida ebs : documentos) {
			sb.append("<documento>\n");
			sb.append("<numeroDocumento>" + ebs.getNumeroDocumento()
					+ "</numeroDocumento>\n");
			sb.append("<fechaIngresoDocumento>" + ebs.getFechaIngreso()
					+ "</fechaIngresoDocumento>\n");
			sb.append("<emisor>" + ebs.getEmisor() + "</emisor>\n");
			sb.append("<destinatarios>" + ebs.getDestinatario()
					+ "</destinatarios>\n");
			sb.append("<tipo>" + ebs.getTipoDocumento() + "</tipo>\n");
			sb.append("<formato>" + ebs.getFormatoDocumento() + "</formato>\n");
			sb.append("</documento>\n");
		}
		sb.append("</documentos>\n");
		sb.append("</cierreDocumentos>");
		return sb.toString().getBytes();
	}

	private void transform(byte[] xml, OutputStream out) throws Exception {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();
		StreamSource source = new StreamSource(new ByteArrayInputStream(xml));
		StreamResult result = new StreamResult(out);
		transformer.transform(source, result);

	}

}
