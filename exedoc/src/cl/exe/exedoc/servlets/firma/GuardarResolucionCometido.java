package cl.exe.exedoc.servlets.firma;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.Component;

import cl.exe.exedoc.repositorio.Repositorio;
import cl.exe.exedoc.repositorio.RepositorioLocal;

public class GuardarResolucionCometido extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 6653523591583322344L;

	public GuardarResolucionCometido() {
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String nombreDocumento = new String(request
				.getParameter("nombreDocumento"));
		byte[] documento = new byte[4096];
		int cuantos;
		ByteArrayOutputStream bstream = new ByteArrayOutputStream();
		while ((cuantos = request.getInputStream().read(documento)) > 0) {
			bstream.write(documento, 0, cuantos);
		}
		guardaResolucionCometido(nombreDocumento, bstream.toByteArray());

	}

	private void guardaResolucionCometido(String nombreDocumento, byte[] archivo) {
		RepositorioLocal repositorio = (RepositorioLocal) Component
				.getInstance(Repositorio.class);
		repositorio.almacenarResolucionCometido(archivo, nombreDocumento);
	}

}
