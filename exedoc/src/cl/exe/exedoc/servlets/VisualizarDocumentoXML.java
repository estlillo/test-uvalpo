package cl.exe.exedoc.servlets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jboss.seam.Component;

import cl.exe.exedoc.entity.DocumentoElectronico;
import cl.exe.exedoc.session.Doc2Xml;
import cl.exe.exedoc.session.Doc2XmlBean;

/**
 * Este es un servlet para visualizar los xml a partir de los documentos
 * 
 * @author ediaz
 */
public class VisualizarDocumentoXML extends HttpServlet implements Servlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4321032061736897806L;

	/**
	 * Plantilla default estandar.
	 */
	private static final String XSLT_SOURCE = "xslt/documentoElectronico.xsl";

	public VisualizarDocumentoXML() {

	}

	/**
	 * La magia de recuperar desde el repositorio CMS Al Fresco la realiza Dox2XmlBean. Este servlet aplica el XSL
	 * estandar ubicado en /xlt
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id = new Long(request.getParameter("documentoId"));

		Doc2Xml doc2Xml = (Doc2Xml) Component.getInstance(Doc2XmlBean.class);

		DocumentoElectronico doc = (DocumentoElectronico) doc2Xml.getDocumento(id);

		byte[] data = doc2Xml.getXmlData(doc);

		try {
			if (data != null) {
				response.setContentType("text/html");
				transform(data, response.getOutputStream());
				response.getOutputStream().flush();
			} else {
				response.setContentType("text/xml");
				response.getWriter().print("<?xml version=\"1.0\" ?>");
				response.getWriter().print("<error>DOCUMENTO NO ENCONTRADO</error>");
			}
		} catch (Exception ex) {
			response.setContentType("text/xml");
			response.getWriter().print("<?xml version=\"1.0\" ?>");
			response.getWriter().print(
					"<error><msj>ERROR AL FORMATEAR DOCUMENTO</msj><reason>" + ex.getMessage() + "</reason></error>");
		}
	}

	/**
	 * Aplica la transformacion XSL estandar sobre los documentos electronicos
	 * 
	 * @param xml el documento como un arreglo de bytes
	 * @param out stream donde escribir
	 * @throws Exception
	 */
	private void transform(byte[] xml, OutputStream out) throws Exception {
		// out.write(xml);
		// return;

		// byte[] xslOut = null;
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(getTemplateSource());
		StreamSource source = new StreamSource(new ByteArrayInputStream(xml));
		StreamResult result = new StreamResult(out);
		transformer.transform(source, result);

	}

	/**
	 * Desde donde leer la plantilla XSL
	 * 
	 * @return la plantilla como un Source
	 */
	private Source getTemplateSource() {
		String templatePath = this.getServletContext().getRealPath(XSLT_SOURCE);

		return new StreamSource(new File(templatePath));
	}

}
