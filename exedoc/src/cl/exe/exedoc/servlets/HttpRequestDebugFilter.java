package cl.exe.exedoc.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.log.Logging;

public class HttpRequestDebugFilter implements Filter {
	org.jboss.seam.log.Log log = Logging.getLog(HttpRequestDebugFilter.class);

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {

		// log info
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			if (httpRequest.getRequestURI().endsWith(".seam")) {
				log.info("request: method=" + httpRequest.getMethod() + ", URL=" + httpRequest.getRequestURI());
				log.info("  contentType=" + request.getContentType() + ", characterEncoding="
						+ request.getCharacterEncoding() + ")");
				log.info("  header:");
				for (Enumeration<?> e = httpRequest.getHeaderNames(); e.hasMoreElements();) {
					String key = (String) e.nextElement();
					Object value = httpRequest.getHeader(key);
					log.info("    " + key + " = " + value);
				}
				if (request.getParameterMap().size() > 0) {
					log.info("  parameters:");

					// sort parameters by name
					List<String> paramNames = new ArrayList<String>(request.getParameterMap().keySet());
					Collections.sort(paramNames);

					for (String key : paramNames) {
						String[] values = request.getParameterValues(key);
						log.info("    " + key + " = " + Arrays.asList(values));
					}
				}
			}
		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig arg0) throws ServletException {
	}

}
