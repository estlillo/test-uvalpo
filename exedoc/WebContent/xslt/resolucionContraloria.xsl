<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:contraloria="http://www.contraloria.cl/2005/05/CGRDoc"
	xmlns:exedoc="http://www.exedoc.gov.cl/esquemas/resolucion.xsd" xmlns="http://www.w3.org/1999/xhtml">

	<xsl:output version="1.0" method="html" indent="yes" />

	<xsl:variable name="urlServletImagenFirma"
		select="'http://192.168.3.180:8080/exedoc/servlet/verImagenFirma?rut='" />

	<xsl:template match="/">
		<html>
			<xsl:call-template name="encabezado" />
			<body>
				<table width="98%" alignment="center" border="0" cellpadding="0"
					cellspacing="0">
					<xsl:apply-templates />
					<xsl:for-each select="contraloria:Documento">
						<xsl:for-each select="contraloria:Contenido">
							<tr>
								<td background="/exedoc/imagenes/login_alfa04.gif"></td>
								<td align="center" valign="top" bgcolor="#FFFFFF">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<tr>
														<td align="left" valign="top" class="txtcampos">
															<img src="/exedoc/imagenes/logoInstitucion.png"
																width="90" height="90" />
															<br />
															<div align="center">DOCUMENTO ELECTRONICO</div>
														</td>
														<td width="10" align="left" valign="top" class="txtcampos"></td>
														<td align="right" valign="top" class="txtcampos">
															<table width="320" border="0" cellpadding="0"
																cellspacing="2">
																<tr>
																	<xsl:for-each select="contraloria:Resolucion">
																		<td align="left" valign="top" class="txtcampos">
																			<xsl:value-of select="contraloria:Materia" />
																		</td>
																	</xsl:for-each>
																</tr>
																<tr>
																	<td align="left" valign="top" class="txtcampos">
																		RESOLUCION
																		<xsl:value-of select="tipoResolucion" />
																		Nº:
																		<xsl:for-each select="contraloria:Folio">
																			<xsl:value-of select="contraloria:Numero" />
																			/
																			<xsl:value-of select="contraloria:Agno" />
																		</xsl:for-each>
																	</td>
																</tr>
																<tr>
																	<xsl:for-each select="contraloria:Resolucion">
																		<td align="left" valign="top" class="txtcampos">
																			<xsl:value-of select="contraloria:Lugar" />
																			,
																			<xsl:call-template name="formato_fecha">
																				<xsl:with-param name="fecha_hora"
																					select="contraloria:Fecha" />
																			</xsl:call-template>
																		</td>
																	</xsl:for-each>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td></td>
										</tr>

										<tr>
											<td align="center">
												<table width="550" border="0" cellpadding="0"
													cellspacing="2">
													<tr>
														<td width="30" align="left" valign="top" class="txtcampos"></td>
														<td width="10" align="left" valign="top"></td>
														<br />
														<td colspan="3" width="520" align="left" valign="top"
															class="txtcampos">
															<br />
															<br />
															<br />
															<br />
														</td>
													</tr>

													<xsl:for-each select="contraloria:Resolucion">
														<xsl:for-each select="contraloria:Visto">
															<tr>
																<td width="30" align="left" valign="top" class="txtcampos"></td>
																<td width="10" align="left" valign="top"></td>
																<td align="left" valign="top" class="txtcampos">
																	VISTOS:
																</td>
															</tr>
															<xsl:for-each select="contraloria:Parrafo">
																<tr>
																	<td width="30" align="left" valign="top" class="txtcampos"></td>
																	<td width="10" align="left" valign="top"></td>
																	<td align="justify" valign="top" class="txtcampos">
																		<xsl:apply-templates />
																	</td>
																</tr>
															</xsl:for-each>
														</xsl:for-each>

														<xsl:for-each select="contraloria:Considerando">
															<tr>
																<td width="30" align="left" valign="top" class="txtcampos"></td>
																<td width="10" align="left" valign="top"></td>
																<td align="left" valign="top" class="txtcampos">
																	<br />
																	CONSIDERANDO:
																</td>
															</tr>
															<xsl:for-each select="contraloria:Parrafo">
																<tr>
																	<td width="30" align="left" valign="top" class="txtcampos"></td>
																	<td width="10" align="left" valign="top"></td>
																	<td align="justify" valign="top" class="txtcampos">
																		<xsl:apply-templates />
																	</td>
																</tr>
															</xsl:for-each>
														</xsl:for-each>
													</xsl:for-each>

													<xsl:for-each select="contraloria:Resolucion">
														<xsl:for-each select="exedoc:Resolucionexedoc">
															<xsl:for-each select="exedoc:Resuelvo">

																<tr>
																	<td width="30" align="left" valign="top" class="txtcampos"></td>
																	<td width="10" align="left" valign="top"></td>
																	<td align="left" valign="top" class="txtcampos">
																		<br />
																		RESUELVO:
																	</td>
																</tr>
																<xsl:for-each select="exedoc:Parrafo">
																	<tr>
																		<td width="30" align="left" valign="top" class="txtcampos"></td>
																		<td width="10" align="left" valign="top"></td>
																		<td align="justify" valign="top" class="txtcampos">
																			<xsl:apply-templates />
																		</td>
																	</tr>
																</xsl:for-each>
															</xsl:for-each>

															<tr>
																<td align="left" valign="top" class="txtcampos"></td>
																<td align="left" valign="top"></td>
																<td align="left" valign="top" class="txtcampos">
																	<br />
																	<xsl:value-of select="exedoc:Indicacion" />
																</td>
															</tr>

															<xsl:for-each select="exedoc:FirmadoPor">
																<xsl:for-each select="exedoc:Firma">
																	<xsl:for-each select="exedoc:Firmante">
																		<tr>
																			<td align="left" valign="top" class="txtcampos"></td>
																			<td align="left" valign="top"></td>
																			<td align="right" valign="top" class="txtcampos">
																				<br />
																				<br />
																				<br />
																				<br />
																				<br />
																				<table>
																					<tr>
																						<td>
																							<img>
																								<xsl:attribute name="src">
															<xsl:value-of
																									select="concat($urlServletImagenFirma,exedoc:Rut)" />
														</xsl:attribute>
																							</img>
																						</td>
																					</tr>
																					<tr>
																						<td align="center" valign="top" class="txtcampos">
																							<xsl:value-of select="exedoc:Nombre" />
																							<br />
																						</td>
																					</tr>
																					<tr>
																						<td align="center" valign="top" class="txtcampos">
																							<xsl:value-of select="exedoc:Cargo" />
																							<br />
																						</td>
																					</tr>
																					<tr>
																						<td align="center" valign="top" class="txtcampos">
																							<xsl:value-of select="exedoc:UnidadOrganizacional" />
																							<br />
																						</td>
																					</tr>
																				</table>
																			</td>
																		</tr>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:for-each>

															<tr>
																<td align="left" valign="top" class="txtcampos"></td>
																<td align="left" valign="top"></td>
																<td align="left" valign="top" class="txtcampos">
																	<br />
																	<br />
																	<p>
																		<xsl:for-each select="exedoc:FirmadoPor">
																			<xsl:for-each select="exedoc:Firma">

																				<xsl:for-each select="exedoc:Firmante">
																					<xsl:for-each select="exedoc:Iniciales">
																						<xsl:value-of select="." />
																						<xsl:if test="position()=last()">
																							/
																						</xsl:if>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>

																		<xsl:for-each select="exedoc:VisadoPor">
																			<xsl:for-each select="exedoc:Visacion">
																				<xsl:for-each select="exedoc:Visador">
																					<xsl:for-each select="exedoc:Iniciales">
																						<xsl:value-of select="." />
																						<xsl:if test="position()!=last()">
																							/
																						</xsl:if>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>

																		/

																		<xsl:for-each select="exedoc:Autor">
																			<xsl:for-each select="exedoc:Iniciales">
																				<span style="text-transform:lowercase">
																					<xsl:value-of select="." />
																				</span>

																				<xsl:if test="position()!=last()">
																					/
																				</xsl:if>
																			</xsl:for-each>
																		</xsl:for-each>
																	</p>
																	DISTRIBUCION:
																</td>
															</tr>
															<xsl:for-each select="exedoc:Distribucion">
																<xsl:for-each select="exedoc:Distribucion">
																	<tr>
																		<td align="left" valign="top" class="txtcampos"></td>
																		<td align="left" valign="top"></td>
																		<td align="left" valign="top" class="txtcampos">
																			<xsl:value-of select="." />
																		</td>
																	</tr>
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</xsl:for-each>
												</table>
											</td>
										</tr>



									</table>
								</td>

								<td background="/exedoc/imagenes/login_alfa02.gif"></td>

							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>

				<xsl:call-template name="footer" />
			</body>
		</html>
	</xsl:template>


	<xsl:template name="encabezado">
		<head>
			<title>exedoc exedoc</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<link href="/exedoc/css/general.css" rel="stylesheet" type="text/css" />
			<script language="JavaScript">
<![CDATA[
var message = "";
function clickIE(){
if (document.all){
(message);
return false;
}
}
function clickNS(e){
if (document.layers || verificaDoc()){
if (e.which == 2 || e.which == 3){
(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown = clickNS;
} else {
document.onmouseup = clickNS;
document.oncontextmenu = clickIE;
}
document.oncontextmenu = new Function("return false")

function verificaDoc(){
	if (document.getElementById)
	 if(!document.all){
	 	return true;
	}return false;
	
}
]]>
			</script>
		</head>
	</xsl:template>

	<xsl:template name="footer">
		<div align="center" class="txtcampos">
			<br />
			Firmado Electrónicamente en Conformidad con el Artículo 2º letra F y
			G de
			la Ley 19.799
		</div>
	</xsl:template>

	<xsl:template name="parrafo">
		<xsl:call-template name="CopyWithLineBreaks">
			<xsl:with-param name="string" select="." />
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="CopyWithLineBreaks">
		<xsl:param name="string" />
		<xsl:variable name="Result">
			<xsl:call-template name="lf2br">
				<xsl:with-param name="StringToTransform" select="$string" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:copy-of select="$Result" />
	</xsl:template>

	<xsl:template name="lf2br">
		<!-- import $StringToTransform -->
		<xsl:param name="StringToTransform" />
		<xsl:choose>
			<!-- string contains linefeed -->
			<xsl:when test="contains($StringToTransform,'&#xA;')">
				<!-- output substring that comes before the first linefeed -->
				<!-- note: use of substring-before() function means -->
				<!-- $StringToTransform will be treated as a string, -->
				<!-- even if it is a node-set or result tree fragment. -->
				<!-- So hopefully $StringToTransform is really a string! -->
				<xsl:value-of select="substring-before($StringToTransform,'&#xA;')" />
				<!-- by putting a 'br' element in the result tree instead -->
				<!-- of the linefeed character, a <br> will be output at -->
				<!-- that point in the HTML -->
				<br />
				<!-- repeat for the remainder of the original string -->
				<xsl:call-template name="lf2br">
					<xsl:with-param name="StringToTransform">
						<xsl:value-of select="substring-after($StringToTransform,'&#xA;')" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<!-- string does not contain newline, so just output it -->
			<xsl:otherwise>
				<xsl:value-of select="$StringToTransform" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="formato_fecha">
		<xsl:param name="fecha_hora" />

		<xsl:variable name="anho">
			<xsl:value-of select="substring($fecha_hora,1,4)" />
		</xsl:variable>

		<xsl:variable name="mes-temp">
			<xsl:value-of select="substring-after($fecha_hora,'-')" />
		</xsl:variable>

		<xsl:variable name="mes">
			<xsl:value-of select="substring-before($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia-temp">
			<xsl:value-of select="substring-after($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia">
			<xsl:value-of select="substring($dia-temp,1,2)" />
		</xsl:variable>

		<xsl:value-of select="$dia" />
		<xsl:value-of select="'/'" />
		<xsl:value-of select="$mes" />
		<xsl:value-of select="'/'" />
		<xsl:value-of select="$anho" />
	</xsl:template>

</xsl:stylesheet>