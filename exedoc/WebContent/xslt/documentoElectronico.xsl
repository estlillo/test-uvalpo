<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

	<xsl:output method="html" indent="yes" />

	<xsl:variable name="urlServletImagenFirma" select="'./../servlet/verImagenFirma?rut='" />

	<xsl:template match="/">
		<html>
			<xsl:call-template name="encabezado" />
			<body>
				<table width="98%" alignment="center" border="0" cellpadding="0"
					cellspacing="0">
					<xsl:apply-templates />
				</table>
				<xsl:call-template name="footer" />
			</body>
		</html>
	</xsl:template>

	<xsl:template name="encabezado">
		<head>
			<title>exedoc exedoc</title>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<link href="../css/estilos_exedoc.css" rel="stylesheet" type="text/css" />
			<script language="JavaScript">
<![CDATA[
var message = "";
function clickIE(){
if (document.all){
(message);
return false;
}
}
function clickNS(e){
if (document.layers || verificaDoc()){
if (e.which == 2 || e.which == 3){
(message);
return false;
}
}
}
if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown = clickNS;
} else {
document.onmouseup = clickNS;
document.oncontextmenu = clickIE;
}
document.oncontextmenu = new Function("return false")

function verificaDoc(){
	if (document.getElementById)
	 if(!document.all){
	 	return true;
	}return false;
	
}
]]>
			</script>
		</head>
	</xsl:template>
	<xsl:template name="footer">
		<div align="center" class="txtcampos">
			<br />
			Firmado Electrónicamente en Conformidad con el Artículo 2º letra F y
			G de
			la Ley 19.799
		</div>
	</xsl:template>


	<xsl:template match="actaAdjudicacion">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="320" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td align="left" valign="top" class="txtcampos">
													Acta de Adjudicacion Nº:
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													SANTIAGO,
													<xsl:value-of select="fecha" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td align="center">
							<div style="text-align:center;font-size:15px;font-weight:bold"
								class="txtcampos">
								ACTA DE ADJUDICACIÓN
								<br />
								<xsl:value-of select="materia" />
							</div>
						</td>
					</tr>

					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<br />
									<td colspan="3" width="520" align="justify" valign="top"
										class="txtcampos">
										<br />
										<br />
										<br />
										<br />
									</td>
								</tr>

								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										1. La Comisión Evaluadora se constituyó con fecha
										<xsl:value-of select="fecha" />
										, la integran, las siguientes personas:
									</td>
								</tr>
								<xsl:for-each select="comision/firmante">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="justify" valign="top" class="txtcampos">
											<xsl:value-of select="nombre" />

										</td>
									</tr>
								</xsl:for-each>

								<xsl:for-each select="cuerpoActa">
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<xsl:for-each select="comision/firmante">
											<table>
												<tr>
													<td>
														<img>
															<xsl:attribute name="src">
															<xsl:value-of select="concat($urlServletImagenFirma,RUT)" />
														</xsl:attribute>
														</img>
													</td>
												</tr>
												<tr>
													<td align="center" valign="top" class="txtcampos">
														<xsl:value-of select="nombre" />
														<br />
													</td>
												</tr>
												<tr>
													<td align="center" valign="top" class="txtcampos">
														<xsl:value-of select="cargo" />
														<br />
													</td>
												</tr>
												<tr>
													<td align="center" valign="top" class="txtcampos">
														<xsl:value-of select="unidadOrganizacional" />
													</td>
												</tr>
											</table>
										</xsl:for-each>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<p>
											<xsl:for-each select="//firmante/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()=last()">
													/
												</xsl:if>
											</xsl:for-each>

											<xsl:for-each select="//visador/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
											/
											<xsl:for-each select="//autor/iniciales">
												<span style="text-transform:lowercase">
													<xsl:value-of select="." />
												</span>
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
										</p>
										DISTRIBUCION:
									</td>
								</tr>

								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>
	</xsl:template>





	<xsl:template match="resolucion">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="320" border="0" cellpadding="0"
											cellspacing="2">
											<tr>

												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													RESOLUCION
													<xsl:value-of select="tipoResolucion" />
													Nº:
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="ciudad"/>, 
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<br />
									<td colspan="3" width="520" align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
									</td>
								</tr>

								<xsl:for-each select="vistos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											VISTOS:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />

											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<xsl:for-each select="considerandos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											CONSIDERANDO:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<xsl:for-each select="resuelvo">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											RESUELVO:
										</td>
									</tr>
									<xsl:for-each select="parrafo ">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<xsl:value-of select="indicacion" />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<p>
											<xsl:for-each select="//firmante/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()=last()">
													/
												</xsl:if>
											</xsl:for-each>

											<xsl:for-each select="//visador/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
											/
											<xsl:for-each select="//autor/iniciales">
												<span style="text-transform:lowercase">
													<xsl:value-of select="." />
												</span>
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
										</p>
										DISTRIBUCION:
									</td>
								</tr>

								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>


	</xsl:template>

	<xsl:template match="respuestaConsultaCiudadana">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="290" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td width="120" align="left" valign="top" class="txtcampos">Carta
													Respuesta Nº</td>
												<td width="40" align="left" valign="top" class="txtcampos">:</td>
												<td width="202" align="left" valign="top" class="txtcampos">
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td width="120" align="left" valign="top" class="txtcampos">
													SANTIAGO,
												</td>
												<td width="40" align="left" valign="top" class="txtcampos"></td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>

					<tr>
						<td align="center">
							<br />
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">Señor(a)
									</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<xsl:value-of select="//destinatario" />
									</td>
								</tr>
								<tr>
									<!-- <td width="30" align="left" valign="top" class="txtcampos"></td> -->
									<!-- <td align="left" valign="top" class="txtcampos"></td> -->
									<td class="txtcampos">
										<br />
										Presente:
									</td>
								</tr>

								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top" class="txtcampos"></td>
									<td class="txtcampos">
										<xsl:value-of select="//respuesta" />
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>
	</xsl:template>

	<xsl:template match="decreto">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="320" border="0" cellpadding="0"
											cellspacing="2">
											<tr>

												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													DECRETO
													<xsl:value-of select="tipoResolucion" />
													Nº:
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="ciudad"/>, 
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<br />
									<td colspan="3" width="520" align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
									</td>
								</tr>

								<xsl:for-each select="vistos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											VISTOS:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />

											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<xsl:for-each select="considerandos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											CONSIDERANDO:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<xsl:for-each select="resuelvo">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											DECRETO:
										</td>
									</tr>
									<xsl:for-each select="parrafo ">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<xsl:value-of select="indicacion" />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<p>
											<xsl:for-each select="//firmante/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()=last()">
													/
												</xsl:if>
											</xsl:for-each>

											<xsl:for-each select="//visador/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
											/
											<xsl:for-each select="//autor/iniciales">
												<span style="text-transform:lowercase">
													<xsl:value-of select="." />
												</span>
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
										</p>
										DISTRIBUCION:
									</td>
								</tr>

								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>


	</xsl:template>

	<xsl:template match="oficio">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="290" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">
													<span style="text-transform: uppercase;">
													<xsl:value-of select="tipoDocumento"/>
													</span>
													N.</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td width="202" align="left" valign="top" class="txtcampos">
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">ANT.
												</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="antecedentes" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">MAT.
												</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">SANTIAGO,
												</td>
												<td width="10" align="left" valign="top" class="txtcampos"></td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>

					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<br />
									<td width="30" align="left" valign="top" class="txtcampos">DE</td>
									<td width="10" align="left" valign="top" class="txtcampos">:</td>
									<td width="502" class="txtcampos">
										<xsl:value-of select="//emisor" />
									</td>
								</tr>
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">A</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<xsl:value-of select="//destinatarios" />
									</td>
								</tr>

								<xsl:for-each select="contenido">
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<br />
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										Saluda atte. a Usted
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<xsl:for-each select="//firmante/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>

										<xsl:for-each select="//visador/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="position()!=last()">
												/
											</xsl:if>
										</xsl:for-each>
										/
										<xsl:for-each select="//autor/iniciales">
											<span style="text-transform:lowercase">
												<xsl:value-of select="." />
											</span>
											<xsl:if test="position()!=last()">
												/
											</xsl:if>
										</xsl:for-each>
									</td>
									<td align="left" valign="top" class="txtcampos">
									</td>
									<td align="right" valign="top" class="txtcampos">
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										DISTRIBUCION:
									</td>
								</tr>
								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>

	</xsl:template>

	<xsl:template match="memorandum">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="290" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">MEMO
													N.</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td width="202" align="left" valign="top" class="txtcampos">
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">MAT.
												</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">SANTIAGO,
												</td>
												<td width="10" align="left" valign="top" class="txtcampos"></td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<br />
									<td width="30" align="left" valign="top" class="txtcampos">DE</td>
									<td width="10" align="left" valign="top" class="txtcampos">:</td>
									<td width="502" class="txtcampos">
										<xsl:value-of select="//emisor" />
									</td>
								</tr>
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">A</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<xsl:value-of select="//destinatarios" />
									</td>
								</tr>

								<xsl:for-each select="contenido">
									<xsl:for-each select="parrafo">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<br />
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos"></td>
									<br />
									<br />
									<br />
									<br />
									<br />
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>

											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<xsl:for-each select="//firmante/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										<xsl:for-each select="//visador/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										/
										<xsl:for-each select="//autor/iniciales">
											<span style="text-transform:lowercase">
												<xsl:value-of select="." />
											</span>
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>

									</td>
									<td align="left" valign="top" class="txtcampos">
									</td>
									<td align="right" valign="top" class="txtcampos">
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										DISTRIBUCION:
									</td>
								</tr>
								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>

	</xsl:template>

	<xsl:template match="providencia">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="290" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">PROV.
													N.</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td width="202" align="left" valign="top" class="txtcampos">
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>

											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">SANTIAGO,
												</td>
												<td width="10" align="left" valign="top" class="txtcampos"></td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
													<!-- xsl:value-of select="//plazo" / -->
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<br />
									<td width="30" align="left" valign="top" class="txtcampos">DE</td>
									<td width="10" align="left" valign="top" class="txtcampos">:</td>
									<td width="502" class="txtcampos">
										<xsl:value-of select="//emisor" />
									</td>
								</tr>
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">A</td>
									<td width="10" align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<xsl:value-of select="//destinatarios" />
										<br />
										<br />
									</td>
								</tr>

								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">OBJETIVOS
									</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<ul>
											<xsl:for-each select="//accion">
												<li>
													<xsl:value-of select="." />
												</li>
											</xsl:for-each>
										</ul>
										<br />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos">OBSERVACIONES
									</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td align="justify" valign="top" class="txtcampos">
										<xsl:for-each select="//observaciones">
											<xsl:call-template name="parrafo" />
										</xsl:for-each>
										<br />
										<br />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos">PLAZO</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td align="left" valign="top" class="txtcampos">
										<xsl:call-template name="formato_fecha_plazo">
											<xsl:with-param name="fecha_hora_plazo"
												select="//plazo" />
										</xsl:call-template>
										<!-- xsl:value-of select="//plazo" / -->
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<xsl:for-each select="//firmante/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										<xsl:for-each select="//visador/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										/
										<xsl:for-each select="//autor/iniciales">
											<span style="text-transform:lowercase">
												<xsl:value-of select="." />
											</span>
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>


									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>

	</xsl:template>


	<xsl:template match="convenio">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="320" border="0" cellpadding="0"
											cellspacing="2">
											<tr>

												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													CONVENIO Nº:
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													SANTIAGO,
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
													<!-- xsl:value-of select="fecha" / -->
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td align="center">
							<div style="text-align:center;font-size:15px;font-weight:bold"
								class="txtcampos">
								SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (exedoc)
								<br />
								Y
								<br />
								<xsl:value-of select="titulo" />
							</div>
						</td>
					</tr>

					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<br />
									<td colspan="3" width="520" align="justify" valign="top"
										class="txtcampos">
										<br />
										<br />
										<xsl:value-of select="prologo" />
										<br />
										<br />
										<br />
									</td>
								</tr>


								<xsl:for-each select="considerandos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											CONSIDERANDO:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />

											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<xsl:for-each select="teniendoPresente">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											TENIENDO PRESENTE:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<xsl:for-each select="convienen">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											CONVIENEN:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<p>
											<xsl:for-each select="//firmante/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()=last()">
													/
												</xsl:if>
											</xsl:for-each>

											<xsl:for-each select="//visador/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
											/
											<xsl:for-each select="//autor/iniciales">
												<span style="text-transform:lowercase">
													<xsl:value-of select="." />
												</span>
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
										</p>
										DISTRIBUCION:
									</td>
								</tr>

								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>
	</xsl:template>

	<xsl:template match="contrato">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="320" border="0" cellpadding="0"
											cellspacing="2">
											<tr>

												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													CONTRATO
													<xsl:value-of select="tipoResolucion" />
													Nº:
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top" class="txtcampos">
													SANTIAGO,
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
													<!-- xsl:value-of select="fecha" / -->
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td align="center">
							<div style="text-align:center;font-size:15px;font-weight:bold"
								class="txtcampos">
								CONTRATO DE PRESTACION DE SERVICIOS
								<br />
								SUBSECRETARIA DE DESARROLLO REGIONAL Y ADMINISTRATIVO (exedoc)
								<br />
								Y
								<br />
								<xsl:value-of select="titulo" />
							</div>
						</td>
					</tr>

					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos"></td>
									<td width="10" align="left" valign="top"></td>
									<br />
									<td colspan="3" width="520" align="justify" valign="top"
										class="txtcampos">
										<br />
										<br />
										<xsl:value-of select="prologo" />
										<br />
										<br />
										<br />
									</td>
								</tr>


								<xsl:for-each select="puntos">
									<tr>
										<td width="30" align="left" valign="top" class="txtcampos"></td>
										<td width="10" align="left" valign="top"></td>
										<td align="left" valign="top" class="txtcampos">
											<br />
											SE HA CONVENIDO LO SIGUIENTE:
										</td>
									</tr>
									<xsl:for-each select="parrafo">
										<tr>
											<td width="30" align="left" valign="top" class="txtcampos"></td>
											<td width="10" align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<br />
										<br />
										<br />
										<br />
										<br />
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<p>
											<xsl:for-each select="//firmante/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()=last()">
													/
												</xsl:if>
											</xsl:for-each>

											<xsl:for-each select="//visador/iniciales">
												<xsl:value-of select="." />
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
											/
											<xsl:for-each select="//autor/iniciales">
												<span style="text-transform:lowercase">
													<xsl:value-of select="." />
												</span>
												<xsl:if test="position()!=last()">
													/
												</xsl:if>
											</xsl:for-each>
										</p>
										DISTRIBUCION:
									</td>
								</tr>

								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>

							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>
	</xsl:template>

	<xsl:template match="carta">
		<tr>
			<td background="/exedoc/imagenes/login_alfa04.gif"></td>
			<td align="center" valign="top" bgcolor="#FFFFFF">

				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">

								<tr>
									<td align="left" valign="top" class="txtcampos">
										<img src="/exedoc/imagenes/logoInstitucion.png" width="90"
											height="90" />
										<br />
										<div align="center">DOCUMENTO ELECTRONICO</div>
									</td>
									<td width="10" align="left" valign="top" class="txtcampos"></td>
									<td align="right" valign="top" class="txtcampos">
										<table width="290" border="0" cellpadding="0"
											cellspacing="2">
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">CARTA
													N.</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td width="202" align="left" valign="top" class="txtcampos">
													<xsl:value-of select="numeroDocumento" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">MAT.
												</td>
												<td width="10" align="left" valign="top" class="txtcampos">:</td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:value-of select="materia" />
												</td>
											</tr>
											<tr>
												<td width="70" align="left" valign="top" class="txtcampos">SANTIAGO,
												</td>
												<td width="10" align="left" valign="top" class="txtcampos"></td>
												<td align="left" valign="top" class="txtcampos">
													<xsl:call-template name="formato_fecha">
														<xsl:with-param name="fecha_hora" select="fecha" />
													</xsl:call-template>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
					<tr>
						<td align="center">
							<table width="550" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<br />
									<td width="30" align="left" valign="top" class="txtcampos">DE</td>
									<td width="10" align="left" valign="top" class="txtcampos">:</td>
									<td width="502" class="txtcampos">
										<xsl:value-of select="//emisor" />
									</td>
								</tr>
								<tr>
									<td width="30" align="left" valign="top" class="txtcampos">A</td>
									<td align="left" valign="top" class="txtcampos">:</td>
									<td class="txtcampos">
										<xsl:value-of select="//destinatarios" />
									</td>
								</tr>

								<xsl:for-each select="contenido">
									<xsl:for-each select="parrafo">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="justify" valign="top" class="txtcampos">
												<br />
												<xsl:call-template name="parrafo" />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>


								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos"></td>
									<br />
									<br />
									<br />
									<br />
									<br />
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="right" valign="top" class="txtcampos">
										<table>
											<tr>
												<td>
													<img>
														<xsl:attribute name="src">
															<xsl:value-of
															select="concat($urlServletImagenFirma,//firmante/RUT)" />
														</xsl:attribute>
													</img>
												</td>
											</tr>

											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/nombre" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/cargo" />
													<br />
												</td>
											</tr>
											<tr>
												<td align="center" valign="top" class="txtcampos">
													<xsl:value-of select="//firmante/unidadOrganizacional" />
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										<br />
										<xsl:for-each select="//firmante/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										<xsl:for-each select="//visador/iniciales">
											<xsl:value-of select="." />
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>
										/
										<xsl:for-each select="//autor/iniciales">
											<span style="text-transform:lowercase">
												<xsl:value-of select="." />
											</span>
											<xsl:if test="not(position()=last())">
												/
											</xsl:if>
										</xsl:for-each>

									</td>
									<td align="left" valign="top" class="txtcampos">
									</td>
									<td align="right" valign="top" class="txtcampos">
									</td>
								</tr>
								<tr>
									<td align="left" valign="top" class="txtcampos"></td>
									<td align="left" valign="top"></td>
									<td align="left" valign="top" class="txtcampos">
										<br />
										DISTRIBUCION:
									</td>
								</tr>
								<xsl:for-each select="distribucion">
									<xsl:for-each select="distribucion">
										<tr>
											<td align="left" valign="top" class="txtcampos"></td>
											<td align="left" valign="top"></td>
											<td align="left" valign="top" class="txtcampos">
												<xsl:value-of select="." />
											</td>
										</tr>
									</xsl:for-each>
								</xsl:for-each>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td background="/exedoc/imagenes/login_alfa02.gif"></td>
		</tr>

	</xsl:template>

	<xsl:template name="parrafo">

		<xsl:call-template name="CopyWithLineBreaks">
			<xsl:with-param name="string" select="." />
		</xsl:call-template>

	</xsl:template>

	<xsl:template name="CopyWithLineBreaks">
		<xsl:param name="string" />
		<xsl:variable name="Result">
			<xsl:call-template name="lf2br">
				<xsl:with-param name="StringToTransform" select="$string" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:copy-of select="$Result" />
	</xsl:template>

	<xsl:template name="lf2br">
		<!-- import $StringToTransform -->
		<xsl:param name="StringToTransform" />
		<xsl:choose>
			<!-- string contains linefeed -->
			<xsl:when test="contains($StringToTransform,'&#xA;')">
				<!-- output substring that comes before the first linefeed -->
				<!-- note: use of substring-before() function means -->
				<!-- $StringToTransform will be treated as a string, -->
				<!-- even if it is a node-set or result tree fragment. -->
				<!-- So hopefully $StringToTransform is really a string! -->
				<xsl:value-of select="substring-before($StringToTransform,'&#xA;')" />
				<!-- by putting a 'br' element in the result tree instead -->
				<!-- of the linefeed character, a <br> will be output at -->
				<!-- that point in the HTML -->
				<br />
				<!-- repeat for the remainder of the original string -->
				<xsl:call-template name="lf2br">
					<xsl:with-param name="StringToTransform">
						<xsl:value-of select="substring-after($StringToTransform,'&#xA;')" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<!-- string does not contain newline, so just output it -->
			<xsl:otherwise>
				<xsl:value-of select="$StringToTransform" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="formato_fecha_ciudad">

		<xsl:param name="fecha_hora" />

		<!-- new date format 20/12/2008 -->
		<!-- former date format 2008-12-20 18:34:26.781 -->

		<!-- Se parsean los datos desde el string original de fecha -->

		<xsl:variable name="ciudad">
			<xsl:value-of select="substring-before($fecha_hora,',')" />
		</xsl:variable>

		<xsl:variable name="fecha_hora_temp">
			<xsl:value-of select="substring-after($fecha_hora,',')" />
		</xsl:variable>

		<xsl:variable name="anho">
			<xsl:value-of select="substring($fecha_hora_temp,1,4)" />
		</xsl:variable>

		<xsl:variable name="mes-temp">
			<xsl:value-of select="substring-after($fecha_hora_temp,'-')" />
		</xsl:variable>

		<xsl:variable name="mes">
			<xsl:value-of select="substring-before($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia-temp">
			<xsl:value-of select="substring-after($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia">
			<xsl:value-of select="substring($dia-temp,1,2)" />
		</xsl:variable>

		<!-- Se muestran con el nuevo formato -->

		<xsl:value-of select="$ciudad" />
		<xsl:value-of select="', '" />

		<xsl:value-of select="$dia" />
		<xsl:value-of select="'/'" />

		<xsl:value-of select="$mes" />
		<xsl:value-of select="'/'" />

		<xsl:value-of select="$anho" />
	</xsl:template>

	<xsl:template name="formato_fecha">

		<xsl:param name="fecha_hora" />

		<!-- new date format 20/12/2008 -->
		<!-- former date format 2008-12-20 18:34:26.781 -->

		<!-- Se parsean los datos desde el string original de fecha -->
		<xsl:variable name="anho">
			<xsl:value-of select="substring($fecha_hora,1,4)" />
		</xsl:variable>

		<xsl:variable name="mes-temp">
			<xsl:value-of select="substring-after($fecha_hora,'-')" />
		</xsl:variable>

		<xsl:variable name="mes">
			<xsl:value-of select="substring-before($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia-temp">
			<xsl:value-of select="substring-after($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia">
			<xsl:value-of select="substring($dia-temp,1,2)" />
		</xsl:variable>

		<!-- Se muestran con el nuevo formato -->
		<xsl:if test="string-length($fecha_hora) &gt; 0">
			<xsl:value-of select="$dia" />
			<xsl:value-of select="'/'" />
			<xsl:value-of select="$mes" />
			<xsl:value-of select="'/'" />
			<xsl:value-of select="$anho" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="formato_fecha_plazo">

		<xsl:param name="fecha_hora_plazo" />

		<!-- new date format 20/12/2008 18:34 -->
		<!-- former date format 2008-12-20 18:34:26.781 -->

		<!-- Se parsean los datos desde el string original de fecha -->
		<xsl:variable name="anho">
			<xsl:value-of select="substring($fecha_hora_plazo,1,4)" />
		</xsl:variable>

		<xsl:variable name="mes-temp">
			<xsl:value-of select="substring-after($fecha_hora_plazo,'-')" />
		</xsl:variable>

		<xsl:variable name="mes">
			<xsl:value-of select="substring-before($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia-temp">
			<xsl:value-of select="substring-after($mes-temp,'-')" />
		</xsl:variable>

		<xsl:variable name="dia">
			<xsl:value-of select="substring($dia-temp,1,2)" />
		</xsl:variable>

		<xsl:variable name="hora">
			<xsl:value-of select="substring-after($dia-temp,' ')" />
		</xsl:variable>

		<xsl:variable name="hh">
			<xsl:value-of select="substring($hora,1,2)" />
		</xsl:variable>

		<xsl:variable name="mm">
			<xsl:value-of select="substring($hora,4,2)" />
		</xsl:variable>

		<!-- Se muestran con el nuevo formato -->
		<xsl:value-of select="$dia" />
		<xsl:value-of select="'/'" />

		<xsl:value-of select="$mes" />
		<xsl:value-of select="'/'" />

		<xsl:value-of select="$anho" />

		<xsl:value-of select="' '" />
		<xsl:value-of select="$hh" />
		<xsl:value-of select="':'" />
		<xsl:value-of select="$mm" />
	</xsl:template>

</xsl:stylesheet>
