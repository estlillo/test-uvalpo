//Mensajes de Error
var errSinNumExpediente = "\n- Debe ingresar un valor en 'N� Expediente'";
var errSinDestinatario ='\n- Debe agregar al menos un destinatario';
var errSinDocumento ='\n- Debe agregar al menos un documento';

function submitGuardar(){
	validacionGuardar();
}

function submitDespachar(){
	validacionDespachar();
}

function isCampoNulo(idCampo){
	var retorno;
	var campoTexto;
	
	campoTexto = document.getElementById(idCampo);
	valorCampoTexto = campoTexto.value;
	if(valorCampoTexto == ''){
		retorno = true;
	}else{
		retorno = false;
	}
	return retorno;
}

function existeTabla(idTabla){
	var retorno;
	try{
		tabla = document.getElementById(idTabla);
		if (null == tabla){
			retorno = false;
		}else{
			retorno = true;
		} 
	}catch(e){
		alert(e.description);
	}
	return retorno;
}

function validacionGuardar(){
	var mensaje = 'No se puede guardar el documento :\n';
	try{
		var mensajeDeError = mensaje;
		
		if(!existeTabla('formDocIni:listaDocumentosIniciales')){
			mensajeDeError += errSinDocumento;
		}
		
		//SI NO HAN OCURRIDO ERRORES HACE SUBMIT
		if(mensajeDeError == mensaje){ 
			boton = document.getElementById('formBotones:registrarExpediente');
			boton.click();
		}else{ //SINO MUESTRA LOS ERRORES EN EL ALERT
			alert(mensajeDeError); 
		}
		
	}catch(e){
		alert('Error ' + e.description);
	}
}

function validacionDespachar(){
	var mensaje = 'No se puede despachar el documento :\n';
	try{
		var mensajeDeError = mensaje;
		
		if(!existeTabla('formDestinatarioExpediente:listaDestinatarios')){
			mensajeDeError += errSinDestinatario;
		}
		if(!existeTabla('formDocIni:listaDocumentosIniciales')){
			mensajeDeError += errSinDocumento;
		}
		
		//SI NO HAN OCURRIDO ERRORES HACE SUBMIT
		if(mensajeDeError == mensaje){ 
			boton = document.getElementById('formBotones:despachar');
			boton.click();
		}else{ //SINO MUESTRA LOS ERRORES EN EL ALERT
			alert(mensajeDeError); 
		}
	}catch(e){
		alert('Error ' + e.description);
	}	
}