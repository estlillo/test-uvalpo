//Mensajes de Error
var errSinDestinatario ='\n- Debe agregar al menos un destinatario';

function submitAgregarDocumentoDigital(){

 var mensaje = 'No se puede Agregar el documento :\n';
	try{
		var mensajeDeError = mensaje;

		if(!existeTabla('idIncludeDigitalizado:formAgregarDocumentoDigitalizado:ADocumentoDigitalizado:ddListDigitalizado')){
			mensajeDeError += errSinDestinatario; 
		}

		var fecha = document.getElementById('idIncludeDigitalizado:formAgregarDocumentoDigitalizado:idDecorateCalendarFechaDocumento:idCalendarFechaDocumentoInputDate').value;
		var agno = fecha.substring(6,10);
		if (fecha.length != 10){
			mensajeDeError += '\n- La Fecha debe estar en formato: dd/MM/yyyy';
		}else if (agno < 2000){
			mensajeDeError += '\n- El a�o debe ser mayor que 2000';
		}

		//SI NO HAN OCURRIDO ERRORES HACE SUBMIT
		if(mensajeDeError == mensaje){
			boton = document.getElementById('idIncludeDigitalizado:formAgregarDocumentoDigitalizado:idButtonAgregarDocumentoDigitalizado');
			boton.click();
		}else{ //SINO MUESTRA LOS ERRORES EN EL ALERT

			alert(mensajeDeError); 
		}
		
	}catch(e){
		alert('Error ' + e.description);
	}
}

function isCampoNulo(idCampo){
	var retorno;
	var campoTexto;
	
	campoTexto = document.getElementById(idCampo);
	valorCampoTexto = campoTexto.value;
	if(valorCampoTexto == ''){
		retorno = true;
	}else{
		retorno = false;
	}
	return retorno;
}

function existeTabla(idTabla){
	var retorno;
	try{
		tabla = document.getElementById(idTabla);
		if (null == tabla){
			retorno = false;
		}else{
			retorno = true;
		} 
	}catch(e){
		alert(e.description);
	}
	return retorno;
}