package cl.exe.email.utils.configuracion;

import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

/**
 * @author Ricardo Fuentes
 */
@Name(AbstractConfiguracion.NAME)
@Scope(ScopeType.APPLICATION)
@AutoCreate
@Install(precedence = Install.FRAMEWORK)
@Startup
public class Configuracion extends AbstractConfiguracion {

	/**
	 * Constructor.
	 */
	public Configuracion() {
		super();
	}

	/**
	 * @return {@link Configuracion}
	 */
	public static Configuracion getInstance() {
		return (Configuracion) Component.getInstance(AbstractConfiguracion.NAME);
	}

	/**
	 * Crea configuracion.
	 */
	@Create
	public void create() {
		this.configurar();
	}

}
