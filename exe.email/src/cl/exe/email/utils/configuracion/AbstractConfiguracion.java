package cl.exe.email.utils.configuracion;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.log.Log;

/**
 * Class CustomizacionSingleton.
 * 
 * @author Ricardo Fuentes
 */
public abstract class AbstractConfiguracion {

	/**
	 * Nombre de la clase.
	 */
	public static final String NAME = "configuracionEmail";

	/**
	 * HOST. valor: mail.smtp.host.
	 */
	public static final String HOST = "mail.smtp.host";

	/**
	 * STARTTLS. valor: mail.smtp.starttls.enable.
	 */
	public static final String STARTTLS = "mail.smtp.starttls.enable";

	/**
	 * PORT. valor: mail.smtp.port.
	 */
	public static final String PORT = "mail.smtp.port";

	/**
	 * USER. valor: mail.smtp.user.
	 */
	public static final String USER = "mail.smtp.user";

	/**
	 * AUTH. valor: mail.smtp.auth.
	 */
	public static final String AUTH = "mail.smtp.auth";

	/**
	 * CLAVE. valor: mail.smtp.clave.
	 */
	public static final String CLAVE = "mail.smtp.clave";
	
	/**
	 * PLANTILLA_FIRMADO.
	 */
	public static final String PLANTILLA_FIRMADO = "mail.plantilla.firmado";
	
	/**
	 * PLANTILLA_DESPACHO.
	 */
	public static final String PLANTILLA_DESPACHO = "mail.plantilla.despacho";
	
	/**
	 * PLANTILLA_DESPACHO CON ARCHIVO ADJUNTO.
	 */
	public static final String PLANTILLA_DESPACHO_BINARIO = "mail.plantilla.despachoBinario";
	
	/**
	 * PLANTILLA_ALERTA.
	 */
	public static final String PLANTILLA_ALERTA = "mail.plantilla.alerta";
	
	/**
	 * PLANTILLA_NOTIFICACION.
	 */
	public static final String PLANTILLA_NOTIFICACION = "mail.plantilla.reasignado";
	
	/**
	 * PLANTILLA_SOLICITUD_PAPEL.
	 */
	public static final String PLANTILLA_SOLICITUD_PAPEL = "mail.plantilla.solicitudPapel";
	
	/**
	 * PLANTILLA_SOLICITUD_BINARIO CON ARCHIVO ADJUNTO.
	 */
	public static final String PLANTILLA_SOLICITUD_BINARIO = "mail.plantilla.solicitudBinario";
	
	/**
	 * PLANTILLA_SOLICITUD_CUSTODIA_APROBADA.
	 */
	public static final String PLANTILLA_SOLICITUD_CUSTODIA_APROBADA = "mail.plantilla.solicitudCustodiaAprobada";
	
	/**
	 * PLANTILLA_SOLICITUD_CUSTODIA_RECHAZADA.
	 */
	public static final String PLANTILLA_SOLICITUD_CUSTODIA_RECHAZADA = "mail.plantilla.solicitudCustodiaRechazada";
	
	/**
	 * SMTP. value: mail.smtp.
	 */ 
	public static final String SMTP =  "mail.smtp";

	/**
	 * PROPERTIES.
	 */
	public static Properties properties;

	@Logger
	private Log log;

	/**
	 * Constructor.
	 */
	protected AbstractConfiguracion() {
		super();
	}

	/**
	 * Configurar properties para el sistema.
	 */
	public void configurar() {
		log.info("Configurando");

		// Resetear las propiedades
		properties = new Properties();
		log.info("inicio para leer propiedades");
		this.loadPropertyMail(properties);
		log.info("fin leer propiedades");
	}

	/**
	 * Metodo que lee las propiedades de Email.
	 * 
	 * @param propiedad {@link Properties}
	 */
	private void loadPropertyMail(final Properties propiedad) {
		try {
			final InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("exedoc/exedocMail.properties");
			propiedad.load(resourceAsStream);

			log.info("1, {0} = {1}", HOST, propiedad.getProperty(HOST));
			log.info("2, {0} = {1}", STARTTLS, propiedad.getProperty(STARTTLS, "false"));
			log.info("3, {0} = {1}", PORT, propiedad.getProperty(PORT));
			log.info("4, {0} = {1}", USER, propiedad.getProperty(USER));
			log.info("5, {0} = {1}", CLAVE, propiedad.getProperty(CLAVE));
			log.info("6, {0} = {1}", PLANTILLA_FIRMADO, propiedad.getProperty(PLANTILLA_FIRMADO));
			log.info("7, {0} = {1}", PLANTILLA_DESPACHO, propiedad.getProperty(PLANTILLA_DESPACHO));
			log.info("8, {0} = {1}", PLANTILLA_DESPACHO, propiedad.getProperty(PLANTILLA_DESPACHO));
			log.info("9, {0} = {1}", PLANTILLA_DESPACHO_BINARIO, propiedad.getProperty(PLANTILLA_DESPACHO_BINARIO));
			log.info("10, {0} = {1}", PLANTILLA_SOLICITUD_PAPEL, propiedad.getProperty(PLANTILLA_SOLICITUD_PAPEL));
			log.info("11, {0} = {1}", PLANTILLA_SOLICITUD_BINARIO, propiedad.getProperty(PLANTILLA_SOLICITUD_BINARIO));
			log.info("12, {0} = {1}", PLANTILLA_NOTIFICACION, propiedad.getProperty(PLANTILLA_NOTIFICACION));
			log.info("13, {0} = {1}", PLANTILLA_SOLICITUD_CUSTODIA_APROBADA, propiedad.getProperty(PLANTILLA_SOLICITUD_CUSTODIA_APROBADA));
			log.info("14, {0} = {1}", PLANTILLA_SOLICITUD_CUSTODIA_RECHAZADA, propiedad.getProperty(PLANTILLA_SOLICITUD_CUSTODIA_RECHAZADA));

		} catch (IOException e) {
			throw new RuntimeException("No se puede cargar archivo.", e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("Encoding incorrecto.", e);
		}
	}

	/**
	 * Searches for the property with the specified key in this property list. If the key is not found in this property
	 * list, the default property list, and its defaults, recursively, are then checked. The method returns null if the
	 * property is not found.
	 * 
	 * @param name - the property key.
	 * @return the value in this property list with the specified key value.
	 */
	public String getProperty(final String name) {
		return properties.getProperty(name);
	}

	/**
	 * @param name {@link String} nombre de la property.
	 * @return {@link Boolean}
	 */
	public Boolean getPropertyBoolean(final String name) {
		final String value = this.getProperty(name);
		return Boolean.parseBoolean(value);
	}

	/**
	 * @return {@link Properties};
	 */
	public static Properties getProperties() {
		return properties;
	}

	public void create() {
		// TODO Auto-generated method stub
		
	}

}
