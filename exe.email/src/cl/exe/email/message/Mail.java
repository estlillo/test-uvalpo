package cl.exe.email.message;

import javax.mail.internet.InternetAddress;

/**
 * Class Mail, contiene datos para enviar correo.
 * 
 * @author Ricardo Fuentes
 */
public class Mail {

	private InternetAddress form;
	private String subject;
	private String text;
	private String nombreArchivo;
	private byte[] data;

	/**
	 * Constructor.
	 */
	public Mail() {
		super();
	}

	/**
	 * Constrctor con parametros.
	 * 
	 * @param form {@link InternetAddress}
	 * @param subject {@link String}
	 * @param text {@link String}
	 */
	public Mail(final InternetAddress form, final String subject, final String text) {
		super();
		this.form = form;
		this.subject = subject;
		this.text = text;
	}
	
	public Mail(final InternetAddress form, final String subject, final String text, byte[] data ) {
		super();
		this.data = data;
		this.form = form;
		this.subject = subject;
		this.text = text;
	}

	/**
	 * @return {@link InternetAddress}
	 */
	public InternetAddress getForm() {
		return form;
	}

	/**
	 * @param form {@link InternetAddress}
	 */
	public void setForm(final InternetAddress form) {
		this.form = form;
	}

	/**
	 * @return {@link String}
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject {@link String}
	 */
	public void setSubject(final String subject) {
		this.subject = subject;
	}

	/**
	 * @return {@link String}
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text {@link String}
	 */
	public void setText(final String text) {
		this.text = text;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	
}
