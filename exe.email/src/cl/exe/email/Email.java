package cl.exe.email;

import static cl.exe.email.utils.configuracion.AbstractConfiguracion.CLAVE;
import static cl.exe.email.utils.configuracion.AbstractConfiguracion.SMTP;
import static cl.exe.email.utils.configuracion.AbstractConfiguracion.USER;
import static cl.exe.email.utils.configuracion.AbstractConfiguracion.properties;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import cl.exe.email.message.BufferedDataSource;
import cl.exe.email.message.Mail;
import cl.exe.email.utils.configuracion.Configuracion;

/**
 * Class Email.
 * 
 * @author Ricardo Fuentes
 */
public class Email implements Serializable {
	
	/**
	 * NAME, nombre de la clase.
	 */
	public static final String NAME = "Email";

	private static final long serialVersionUID = -2015162366346263932L;
	
	private static final Logger LOGGER = Logger.getLogger(Email.NAME);

	private Configuracion configuracionMail;

	/**
	 * Cosntructor.
	 */
	public Email() {
		super();
		configuracionMail = new Configuracion();
	}

	/**
	 * Envia mail.
	 * 
	 * @param mail {@link Mail}
	 */
	public void enviarMail(final Mail mail) {
		final Session session = Session.getDefaultInstance(properties);
		String msgLog = "";
		final MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(configuracionMail.getProperty(USER)));

			message.addRecipient(Message.RecipientType.TO, mail.getForm());
			message.setSubject(mail.getSubject());
			message.setText(mail.getText());
			
			// Lo enviamos.
			final Transport t = session.getTransport(configuracionMail.getProperty(SMTP));
			t.connect(configuracionMail.getProperty(USER), configuracionMail.getProperty(CLAVE));
			t.sendMessage(message, message.getAllRecipients());
			msgLog = MessageFormat.format("Correo despacho: {0}" , mail.getForm().getAddress());
			LOGGER.info(msgLog);

			// Cierre.
			t.close();
		} catch (AddressException e) {
			LOGGER.log(Level.SEVERE, "Error al intenter enviar mail....", e);
		} catch (MessagingException e) {
			LOGGER.log(Level.SEVERE, "Error al enviar mail..", e);
		}
	}

	public void enviarMailArchivoAdjunto(final Mail mail) {
		final Session session = Session.getDefaultInstance(properties);
		String msgLog = "";
		final MimeMessage message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(configuracionMail.getProperty(USER)));

			message.addRecipient(Message.RecipientType.TO, mail.getForm());
			message.setSubject(mail.getSubject());
			
			
			//Archivo adjunto
			
			// Se crea el objeto Multipart y se le añade el contenido 
			Multipart multiparte = new MimeMultipart(); 
			// Se adjunta el archivo 
	
			BodyPart adjunto = new MimeBodyPart(); 
			BufferedDataSource bds = new BufferedDataSource(mail.getData(), mail.getNombreArchivo()); 
			adjunto.setDataHandler(new DataHandler(bds)); 
			adjunto.setFileName(mail.getNombreArchivo()); 
		
			BodyPart texto = new MimeBodyPart();
			texto.setText(mail.getText());
			
			multiparte.addBodyPart(texto);
			multiparte.addBodyPart(adjunto);
			
			message.setContent(multiparte);
			// Lo enviamos.
			final Transport t = session.getTransport(configuracionMail.getProperty(SMTP));
			t.connect(configuracionMail.getProperty(USER), configuracionMail.getProperty(CLAVE));
			t.sendMessage(message, message.getAllRecipients());
			msgLog = MessageFormat.format("Correo despacho: {0}" , mail.getForm().getAddress());
			LOGGER.info(msgLog);

			// Cierre.
			t.close();
		} catch (AddressException e) {
			LOGGER.log(Level.SEVERE, "Error al intenter enviar mail....", e);
		} catch (MessagingException e) {
			LOGGER.log(Level.SEVERE, "Error al enviar mail..", e);
		}
	}

}
