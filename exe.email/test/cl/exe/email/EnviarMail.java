package cl.exe.email;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.junit.Test;

import cl.exe.email.message.Mail;

/**
 * @author Ricardo Fuentes
 */
public class EnviarMail {

	/**
	 * Constructor.
	 */
	protected EnviarMail() {
		super();
	}

	/**
	 * Metodo para probar si envia mail.
	 * 
	 * @throws AddressException si la direccion de correo no es valida.
	 */
	@Test
	public void enviarMail() throws AddressException {
		final Mail mail = new Mail();
		mail.setForm(new InternetAddress("pumana@exe.cl"));
		mail.setSubject("hola");
		mail.setText("prueba de envio de correo");

		final Email email = new Email();
		email.enviarMail(mail);
	}
}
